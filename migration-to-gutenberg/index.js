#!/usr/bin/env node

/**
 * Copyright (C) 2022, Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 *
 * This script bootstraps WP Gutenberg API under node.js to operate in batch
 * over blocks/blocks' markup.
 * convertStatisticBlock() and makeHistoryBlock() are two such examples.
 */
import minimist from 'minimist';
import {fileURLToPath} from 'url';
import fs from 'fs';
import path from 'path';
import m2j from 'markdown-to-json';
import {webcrypto as crypto} from 'node:crypto';
import camelCase from 'lodash.camelcase';
import {exec} from 'child_process';
import moment from 'moment';
import {parse} from 'csv-parse/sync';

/**
 * JSDOM/MutationObserver initialization
 * @wordpress/priority-queue needs requestidlecallback which needs mutationobserver which needs jsdom
 */
import '../globals.js';
import '../globals-mutation.js';
import '../globals-postload.js';

import coreBlocksLib from '@wordpress/block-library';
import '@animalequality/gutenberg-blocks/build/node/node.js';
import './sb-chart-block/index.js';

import {parseModular, yamlToGravDate, addTitlePattern, percySnap} from './utils.js';

import actionShare from './modular/actionShare.js';
import actionLinks from './modular/actionLinks.js';
import actionMsender from './modular/actionMsender.js';
import contentAccordeon from './modular/contentAccordeon.js';
import contentCitation from './modular/contentCitation.js';
import contentCitationHero from './modular/contentCitationHero.js';
import contentFullscreenHero from './modular/contentFullscreenHero.js';
import contentGallery from './modular/contentGallery.js';
import contentGalleryFlickr from './modular/contentGalleryFlickr.js';
import contentLogos from './modular/contentLogos.js';
import contentHeroImageOnly from './modular/contentHeroImageOnly.js';
import contentHero from './modular/contentHero.js';
import contentCallouts from './modular/contentCallouts.js';
import contentStaff from './modular/contentStaff.js';
import contentHeroBox from './modular/contentHeroBox.js';
import contentTeaser from './modular/contentTeaser.js';
import contentArrow from './modular/contentArrow.js';
import contentWave from './modular/contentWave.js';
import contentIcons from './modular/contentIcons.js';
import contentCards from './modular/contentCards.js';
import contentChartRow from './modular/contentChartRow.js';
import contentChart from './modular/contentChart.js';
import contentFoorm from './modular/contentFoorm.js';
import contentImageText from './modular/contentImageText.js';
import contentImageTextDouble from './modular/contentImageTextDouble.js';
import contentVideo from './modular/contentVideo.js';
import contentLatestUpdates from './modular/contentLatestUpdates.js';
import contentTwoColumns from './modular/contentTwoColumns.js';
import contentInvestigationPreview from './modular/contentInvestigationPreview.js';
import contentText from './modular/contentText.js';
import contentTimeline from './modular/contentTimeline.js';
import contentPressReleases from './modular/contentPressReleases.js';
import petitionImage from './modular/petitionImage.js';
import petitionHero from './modular/petitionHero.js';
moment.suppressDeprecationWarnings = true;

let window;
const default_attrs = {};
const basedir = process.env.WP_BASE_DIR || './';
const wpincludedir = basedir + '/web/wp/wp-includes';
const plugindir = basedir + '/web/app/plugins';
const themedir = basedir + '/web/app/themes';
const ACTION_POST_TYPE = 'ae-action';
const CAMPAIGN_POST_TYPE = 'campaign';

if (!global.crypto) {
  global.crypto = crypto;
}

const args = minimist(process.argv.slice(2));
const migrationFunctions = {
  actionShare,
  actionLinks,
  actionMsender,
  contentAccordeon,
  contentCitation,
  contentCitationHero,
  contentFullscreenHero,
  contentGallery,
  contentGalleryFlickr,
  contentLogos,
  contentHeroImageOnly,
  contentHero,
  contentStaff,
  contentHeroBox,
  contentIcons,
  contentCards,
  contentImageText,
  contentImageTextDouble,
  contentVideo,
  contentCallouts,
  contentTwoColumns,
  contentInvestigationPreview,
  contentText,
  contentLatestUpdates,
  contentTeaser,
  contentArrow,
  contentWave,
  contentChartRow,
  contentChart,
  contentFoorm,
  contentTimeline,
  contentPressReleases,
  petitionImage,
  petitionHero,
  petitionVideo: petitionImage,
  donationImage: petitionImage,
  donationVideo: petitionImage,
  donationHero: petitionHero,
  newsletterImage: petitionImage,
  newsletterVideo: petitionImage,
  newsletterHero: petitionHero
};

const countries = [
  {code: 'br', lang: 'pt_BR', campaign_slug: 'campanha', action_slug: ''},
  {code: 'de', lang: 'de_DE', campaign_slug: 'kampagne', action_slug: ''},
  {code: 'es', lang: 'es_ES', campaign_slug: 'campana', action_slug: 'actua'},
  {code: 'it', lang: 'it_IT', campaign_slug: 'campagna', action_slug: 'agisci'},
  {code: 'in', lang: 'en_GB', campaign_slug: 'campaign', action_slug: ''},
  {code: 'mx', lang: 'es_MX', campaign_slug: '', action_slug: 'actua'},
  {code: 'uk', lang: 'en_GB', campaign_slug: 'campaign', action_slug: 'act'},
  {code: 'us', lang: 'en_US', campaign_slug: 'campaign', action_slug: 'act'}
];

const country = countries.find(item => item.code === args.country),
    lvUrl = args.lv,
    donateUrl = args.donate,
    alias = args.alias,
    mapFile = args.map;

coreBlocksLib.registerCoreBlocks();

let map;
const cmds = [];
const system = args.system;

if (!system || !['grav', 'wordpress'].includes(system)) {
  console.error(`error: Invalid system defined "${system}". Accepted: "grav", "wordpress"`);
  process.exit(1);
}

if (!country) {
  console.error('error: Unsupported country defined');
  process.exit(1);
}

const destinationPath = args.dest || `./migration/${system}/posts.${country.code}`;

if (system === 'grav' && (!lvUrl || !donateUrl)) {
  console.error('error: Missing config lvUrl or donateUrl');
  process.exit(1);
}

if (mapFile) {
  if (!fs.lstatSync(mapFile)) {
    console.error(`error: map-file ${mapFile} unreadable/not found`);
    process.exit(1);
  }
  const text = fs.readFileSync(mapFile).toString();
  map = parse(text, {
    columns: true,
    encoding: 'utf8',
    skip_empty_lines: true
  });
  map = Object.fromEntries(map.filter(e => e.Country === country.code).map(e => [e.Campaign, e]));

  // The mapping contains nested path
  // but the directory we iterate on has already been flattened.
  // Eg: "2014" instead of "gadhimai/2014", so let's add basename
  // to the mapping to detected for these
  for (const [k, v] of Object.entries(map)) {
    if (k.includes('/')) {
      map[path.basename(k)] = v;
    }
  }
}

function mapDestinationToType(path) {
  if (!map) return ACTION_POST_TYPE;
  let dst = null,
      slug = path.replace(/\/[0-9]+\./g, '/');
  do {
    if (slug in map) {
      dst = map[slug].Posttype;
      break;
    }
    if (!slug.includes('/')) {
      break;
    }
  } while (slug = slug.replace(/^\/*.+?\//, ''));

  if (!dst) {
    console.warn(dst === null ? `Couldn't map "${slug}" to a post-type` : `No post-type defined for "${slug}"`);
  } else if (dst.includes('campaign')) {
    return CAMPAIGN_POST_TYPE;
  }
  return ACTION_POST_TYPE;
}

function fillInGravMeta(page, pageMd, meta) {
  // Stick to the value of AE_GRAV_PAGE_META_KEY
  meta._grav_original_page = path.dirname(page).replace(/.*(page|data)\//, '').replace(/(^|\/)[0-9]+\./g, '$1');
  if (pageMd.content) {
    meta._genesis_description = pageMd.content.replace(/^(\n)+/, '');
  }
  if (pageMd.title) {
    meta._genesis_title = pageMd.title;
  }
  if (pageMd['animal-equality-seo']) {
    const seo = pageMd['animal-equality-seo'];
    if (seo.facebookTitle) {
      meta._open_graph_title = seo.facebookTitle;
    }
    if (seo.twitterTitle) {
      meta._twitter_title = seo.twitterTitle;
    }
    if (seo.googleTitle) {
      meta._genesis_title = seo.googleTitle;
    }
    if (seo.facebookDescription) {
      meta._open_graph_description = seo.facebookDescription;
    }
    if (seo.twitterDescription) {
      meta._twitter_description = seo.twitterDescription;
    }
    if (seo.googleDescription) {
      meta._genesis_description = seo.googleDescription;
    }
  }
}

async function processPage(page, pageMd) {
  if (pageMd.redirect) {
    console.warn(`redirect "${pageMd.title}" redirects to "${pageMd.redirect}"`);
  }

  const modularContentPromises = [],
      pageDir = path.dirname(page),
      modularDirs = fs.readdirSync(pageDir, {withFileTypes: true}).filter(dirent => dirent.isDirectory()).map(dirent => path.join(pageDir, dirent.name));

  for (const modular of modularDirs) {
    const files = fs.readdirSync(modular);
    for (const file of files) {
      const fullPath = path.join(modular, file),
          modularType = file.split('.')[0];

      if (path.extname(file) !== '.md' || modularType.includes('form')) {
        continue;
      }

      const parsedModular = parseModular(fullPath, modularType);
      if (parsedModular.published !== false) {
        const migrationFunction = migrationFunctions[camelCase(modularType)];
        if (!migrationFunction) {
          console.warn(`${modularType}: no migration function found at ${fullPath}`);
          continue;
        }

        if (!pageMd.date) {
          pageMd.date = yamlToGravDate(parsedModular);
        }

        modularContentPromises.push(new Promise(async resolve => {
          const migrationFunctionResult = migrationFunction(parsedModular, parsedModular.color_scheme, country.lang, lvUrl, donateUrl);
          let migratedContent = null;
          if (migrationFunctionResult instanceof Promise) {
            migratedContent = await migrationFunctionResult;
          } else {
            migratedContent = migrationFunctionResult;
          }

          // Add arrow
          const colorArrow = parsedModular.color_scheme === 'light' ? '#ececec' : parsedModular.color_scheme === 'dark' ? '#3f3f3f' : '#ffffff';
          if (parsedModular.styling_arrow) {
            migratedContent += wp.blocks.serialize(wp.blocks.createBlocksFromInnerBlocksTemplate(
              [['animalequality/arrow', {colorArrow}]]
            ));
          }
          resolve(migratedContent);
        }));
      }
    }
  }

  return modularContentPromises;
}

async function createHtmlAndCommands(args, page, pageMd, modularContents) {
  const system = args.system;
  const pageBasename = path.basename(path.dirname(page));

  await Promise.all(modularContents).then(async content => {
    let cmd = null;

    content = content.reduce((prev, cur) => prev + cur, '');
    if (system === 'grav') {
      content = addTitlePattern(content, pageMd.title);
    }

    const data = {title: pageMd.title, meta: {}, content},
	  txtDest = path.join(destinationPath, pageBasename + '.txt'),
	  alias = args.alias || `ae${country.code}`;

    fs.writeFileSync(txtDest, data.content, {});

    if (system === 'grav' && !pageMd.date) {
      console.warn(`\tNo date found for post "${pageBasename}"`);
    }

    if (system === 'grav') {
      fillInGravMeta(page, pageMd, data.meta);
      const meta_str = JSON.stringify(data.meta).replace(/(["\\])/g, '\\$1'),
          post_slug = pageBasename.replace(/^[0-9]+\./, ''),
          dst_post_type = mapDestinationToType(path.dirname(page)),
          post_status = pageMd.published === false ? 'private' : 'draft';
      if (pageMd.published === false) {
        console.warn(`unpublished Grav page: "${pageMd.title}"`);
      }

      cmd = `cat ${txtDest} | wp @${alias} post create --post_date="${pageMd.date}" --post_type=${dst_post_type} --post_status=${post_status} --post_title="${data.title.replace(/(["\\])/g, '\\$1')}" --post_name="${post_slug}" --meta_input="${meta_str}" -`;
    } else if (system === 'wordpress') {
      cmd = `cat ${txtDest} | wp @${alias} post update ${pageBasename} -`;
    }

    cmds.push(cmd);

    if (args.execute) {
      exec(cmd, (error, stdout, stderr) => {
        if (error || stderr) {
          console.warn(error, stdout, stderr);
        }
      });
    }

    if (args.snapshot) {
      await percySnap(`https://${country.code}.animalequality.me?p=${pageBasename}`);
    }
  });
}

const getFiles = (path) => {
  if (fs.lstatSync(path).isDirectory()) { // is this a folder?
    fs.readdirSync(path).forEach(f => { // for everything in this folder
      getFiles(path + '/' + f); // process it recursively
    });
  } else if (path.endsWith('.ts')) { // is this a file we are searching for?
    files.push(path); // record it
  }
};

function pagesFromPath(sources, mdFiles) {
  for (const source of sources) {
    const isFile = fs.lstatSync(source).isFile(),
        isDir = fs.lstatSync(source).isDirectory();

    if (isFile && source.endsWith('.md')) {
      mdFiles.push(source);
      continue;
    }
    if (isDir) {
      if (fs.existsSync(path.join(source, 'modular_ae.md'))) {
        mdFiles.push(path.join(source, 'modular_ae.md'));
        continue;
      }

      const subDirs = fs.readdirSync(source, {withFileTypes: true})
        .filter(dirent => dirent.isDirectory())
        .map(dirent => path.join(source, dirent.name));
      pagesFromPath(subDirs, mdFiles);
    }
  }
}

let pages = [];
pagesFromPath(args._, pages);

if (system === 'wordpress') {
    pages.sort(function (a,b) {
	return a.localeCompare(b, undefined, { numeric: true, sensitivity: 'base' });
    });
}

if (!pages.length) {
  const __filename = fileURLToPath(import.meta.url);
  console.error(`error: No page to process.\nUsage: ${path.basename(__filename)} <modular-source>...`);
  process.exit(1);
}

// Uniqueness
pages = [...new Set(pages)];
console.log(`Detected ${pages.length} pages`);
if (fs.existsSync(destinationPath)) {
    console.error(`error: Output directory "${destinationPath}" already exists. Aborting`);
    process.exit(1);
}

fs.mkdirSync(destinationPath, {recursive: true});
for (const page of pages) {
  try {
    const pageMd = JSON.parse(m2j.parse([page], {content: true})).modular_ae;
    pageMd.date = yamlToGravDate(pageMd);
    const modularContents = await processPage(page, pageMd);
    await createHtmlAndCommands(args, page, pageMd, modularContents);
  } catch (error) {
    console.error('error', error);
  }
}

fs.writeFileSync(path.join(destinationPath, 'cmds.txt'), cmds.join('\n') + '\n', {}, (error) => { });
