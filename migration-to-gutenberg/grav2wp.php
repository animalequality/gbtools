<?php

error_reporting(E_ALL ^ E_DEPRECATED);

global $KNOWN_MEDIA_EXTENSIONS_RE, $UPLOADED_ASSETS, $UNREF_ASSETS, $DRY_RUN_LOG;
global $frontMatter;

define('DEFAULT_OUTDIR', './migration/grav/data');
define('TMPDIR', getcwd() . '/upload-queue');
define('AE_GRAV_FILE_META_KEY', '_grav_original_filepath');
define('AE_GRAV_PAGE_META_KEY', '_grav_original_page');
define('ACTION_POST_TYPE', 'ae-action');
define('CAMPAIGN_POST_TYPE', 'campaign');

$UNREF_ASSETS = [];
$UPLOADED_ASSETS = [];

$KNOWN_MEDIA_EXTENSIONS = ['gif', 'jpeg', 'jpg', 'mp4', 'ogv', 'pdf', 'png', 'svg', 'webm'];
$KNOWN_MEDIA_EXTENSIONS_RE = '!\.(' . implode('|', $KNOWN_MEDIA_EXTENSIONS) . ')$!i';

function title_filter($where, &$wp_query)
{
    // https://wordpress.stackexchange.com/a/96446
    global $wpdb;
    if ($search_term = $wp_query->get('title_startswith')) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'' . $wpdb->esc_like($search_term) . '%\'';
    }
    return $where;
}

function wp_builtin_basename_filter($arg)
{
    return sanitize_file_name(preg_replace('/\.[^.]+$/', '', wp_basename($arg)));
}

function gravPathToUrl($a)
{
    global $KNOWN_MEDIA_EXTENSIONS_RE;
    $sub = explode('/', preg_replace('/.*pages\//', '', $a));
    // The last part *may* be the extension
    for ($i = 0; $i < count($sub); $i++) {
        if ($i == count($sub) - 1 && preg_match($KNOWN_MEDIA_EXTENSIONS_RE, $sub[$i])) {
            continue;
        }
        $sub[$i] = preg_replace('/^([0-9]+\.)/', '', $sub[$i]);
    }
    return implode('/', $sub);
}

/**
 * Return the (possibly uploaded) attachement id and whether it was
 * actually a new file
 */
function uploadOneImage($filePath, &$firstEdit, $metas)
{
    global $DRY_RUN_LOG, $UPLOADED_ASSETS;

    if (isset($UPLOADED_ASSETS[$filePath])) {
        WP_CLI::debug("$filePath already uploaded during this run ID #{$UPLOADED_ASSETS[$filePath]}");
        return [$UPLOADED_ASSETS[$filePath], false];
    }

    // Most usual case
    $meta_query = [
        'key'     => AE_GRAV_FILE_META_KEY,
        'value'   => $metas[AE_GRAV_FILE_META_KEY]
    ];

    if (preg_match('/webm|ogv|mp4/i', $metas[AE_GRAV_FILE_META_KEY])) {
        // A video uploaded from a media_order and also reference from "video_mp4"
        $meta_query = [
            'key'     => AE_GRAV_FILE_META_KEY,
            'compare' => 'LIKE',
            'value'   => '/' . basename($metas[AE_GRAV_FILE_META_KEY])
        ];
    }

    $exists = get_posts([
        'post_type'              => 'attachment',
        'title_startswith'       => wp_builtin_basename_filter($filePath),
        'meta_query'             => [
            'relation' => 'AND',
            [
                'value'   => '/' . wp_builtin_basename_filter($filePath),
                'compare' => 'LIKE',
                'key'     => '_wp_attached_file',
            ],
            [
                'key'     => AE_GRAV_FILE_META_KEY,
                'compare' => 'EXISTS'
            ],
            $meta_query
        ],
        'post_status'            => 'all',
        'numberposts'            => 1,
        'update_post_term_cache' => false,
        'update_post_meta_cache' => false,
        'orderby'                => 'post_date ID',
        'order'                  => 'ASC',
    ]);
    if ($exists) {
        WP_CLI::debug("$filePath already exists under ID #{$exists[0]->ID}");
        return [$exists[0]->ID, false];
    }

    if (false && $DRY_RUN) {
        WP_CLI::log("[dry-run] Uploading $filePath");
        $id = rand();
        $UPLOADED_ASSETS[$filePath] = $id;
        return [$id, true];
    }

    $fileDate = getGitDate($filePath);
    if ($fileDate && (!$firstEdit || $fileDate < $firstEdit)) {
        $firstEdit = $fileDate;
    }

    $copy = tempnam(TMPDIR, 'xxxxx');
    copy($filePath, $copy);

    WP_CLI::log("Uploading $filePath (date=$fileDate)");
    $id = media_handle_sideload(['name' => basename($filePath), 'tmp_name' => $copy], 0, null, ['post_date' => $fileDate ?: $firstEdit]);
    if (is_wp_error($id)) {
        return [$id, false];
    }

    $DRY_RUN_LOG[] = get_attached_file($id);

    $UPLOADED_ASSETS[$filePath] = $id;
    foreach ($metas as $k => $v) {
        add_post_meta($id, $k, $v);
    }

    return [$id, true];
}

function getGitDate($filePath)
{
    // Only way to get a realistic file date is its first introduction inside the repository
    return trim(shell_exec(sprintf('git -C "%s" log --follow --format=%%ad --date iso "%s"|sed -n \'$s/ [+-].*//p\'', dirname($filePath), $filePath)));
}

function uploadImages($path, &$arr, &$firstEdit, &$uploadedPaths)
{
    global $KNOWN_MEDIA_EXTENSIONS_RE;
    $modularPageDir = gravPathToUrl(dirname($path));

    if (!$arr) {
        return;
    }

    foreach ($arr as $key => &$value) {
        if (is_array($value)) {
            uploadImages($path, $value, $firstEdit, $uploadedPaths);
            continue;
        }

        if (! preg_match($KNOWN_MEDIA_EXTENSIONS_RE, $value)) {
            continue;
        }
        if ($value === 'doi.jpg' || in_array($key, ['path', 'attachmentId', 'attachmentUrl'], true)) {
            continue;
        }

        if (str_starts_with($key, 'video_') || in_array($key, ['button_target', 'modal_success_cta_link2'], true)) {
            $splitPath = explode('/', $value);
            $oldurl = $value;
            $filePath = $path . '/' . urldecode($splitPath[count($splitPath) - 1]);
            if (!is_file($filePath)) {
                // try this?
                $filePath = str_replace(' ', '_',  $filePath);
            }
        }
        elseif (in_array($key, ['name', 'image', '_media_order'], true)) {
            $oldurl = preg_replace('!.*pages/*!', '', $path . '/' . $value);
            $filePath = $path . '/' . basename($value);
        }
        elseif (in_array($key, ['twitterImage', 'facebookImage'], true)) {
            $oldurl = preg_replace('!.*pages/*!', '', $path . '/' . $value);
            $p = explode('/', $value);
            array_shift($p);
            for ($i = 0; $i < count($p) - 1; $i++) {
                $p[$i] = '*' . $p[$i];
            }
            $src = $path . '/../' . implode('/', $p);
            $srcs = glob($src);
            if (!$srcs) {
                WP_CLI::warning("$key: No such file: \"$src\" in \"$path\"");
                continue;
            }
            $filePath = $srcs[0];
        }
        elseif ($key === 'media_order') {
            if (is_string($value)) {
                $value = array_map(fn ($e) => ['_media_order' => $e], explode(',', $value));
            }
            uploadImages($path, $value, $firstEdit, $uploadedPaths); // Will trigger the above "_media_order" case for each item in the "media_order" list
            continue;
        }
        elseif ($key === 'url' && str_contains($value, 'app/uploads/')) {
            continue;
        }
        else {
            WP_CLI::error(sprintf("YAML unknown key at %s: %s", $path, $key));
            die;
        }

        if (!is_file($filePath)) {
            WP_CLI::warning(sprintf("$key: No such file: \"%s\"", $filePath));
            continue;
        }

        // This doesn't remove the original file in the grav/ pages directory but doesn't allow to set custom time
        /*$id = WP_REST_Attachments_Controller::upload_from_data(file_get_contents($filePath),
            ['content_type' => mime_content_type($filePath), 'content_disposition' => sprintf('Content-Disposition: filename=%s', basename($filePath))]
        );*/

        // To avoid original file removal, media_handle_sideload() to operate on a copy
        // Date depends on git checkout
        // $fileDate = date('Y-m-d H:i:s', filemtime($filePath));

        $uploadedPaths[] = $filePath;
        [$id, $is_new] = uploadOneImage($filePath, $fileDate, [AE_GRAV_FILE_META_KEY => $oldurl, AE_GRAV_PAGE_META_KEY => $modularPageDir]);
        if (is_wp_error($id)) {
            WP_CLI::error(sprintf("Couldn't upload %s", $filePath));
            WP_CLI::error($id->get_error_message());
            continue;
        }

        $newurl = wp_get_attachment_image_url($id, 'full') ?: wp_get_attachment_url($id);
        // main output
        Wp_CLI::debug(implode("\t", [$key, $id, $filePath, $oldurl, $newurl]));;

        if (in_array($key, ['video_mp4', 'video_ogv', 'video_webm'], true)) {
            $arr[$key] = [$key => ['attachmentId' => $id, 'attachmentUrl' => $newurl]];
        } else {
            $arr += ['attachmentId' => $id, 'attachmentUrl' => $newurl];
        }
    }
}

class GravFolderProcess
{
    public static $frontMatter = null;
    private $destinationPath = null;

    public function __construct($frontMatter, $destinationPath)
    {
        self::$frontMatter = $frontMatter;
        $this->destinationPath = $destinationPath;
    }

    /**
     * $sourcePath: The /pages/ directory
     * Returns all the modular MD files (including nested ones)
     * This flatten possibly recursive pages directory (tree of nested modular pages)
     */
    public function processPagesFolder($sourcePath)
    {
        $directory = new RecursiveDirectoryIterator($sourcePath, RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($directory);
        // applies on (full) pathName
        $regex = new RegexIterator($iterator, '/modular_ae.+\.md$/', RecursiveRegexIterator::MATCH);
        $pages = array_map(fn ($e) => $e->getPathName(), iterator_to_array($regex, false));
        $pages = array_unique($pages);
        WP_CLI::log(sprintf("Processing %d pages into %s", count($pages), $this->destinationPath));
        WP_CLI::log("- " . implode("\n- ", $pages));
        foreach ($pages as $page) {
            $this->processModularPage($page);
        }
    }

    /**
     * $modularPageDir: The full path of a modular page, eg: ".../de/pages/15.rodung-in-brasilien"
     */
    public function processModularPage($sourcePageFile)
    {
        $modularPageDir = dirname($sourcePageFile);
        $modularPageName = basename($modularPageDir);
        $isDir = fn ($dir) => is_dir($modularPageDir . '/' . $dir) && !in_array($dir, ['.', '..']);
        $modulars = array_filter(scandir($modularPageDir), $isDir);
        if (!count($modulars)) {
            WP_CLI::error(sprintf("No subpages under $modularPageDir"));
            return;
        }

        // Process individual modular
        $this->processPages($modularPageDir, $modulars);


        // Finally process the main page itself
        // Instead of a simple `copy($sourcePageFile, $mainDestination);`
        $mainDestination = $this->destinationPath . '/' . $modularPageName . '/' . 'modular_ae.md';
        $document = self::$frontMatter->parse(file_get_contents($sourcePageFile));
        self::processPage($document, $modularPageDir);

        // Also add the date if absent
        $date = getGitDate($sourcePageFile);
        if (empty($data['date'])) {
            $data = $document->getData();
            $data['date'] = $date;
            $document->setData($data);
        }
        //

        file_put_contents($mainDestination, self::$frontMatter->dump($document));
    }

    /**
     * $modularPageDir: The full path of a modular page, eg: ".../de/pages/15.rodung-in-brasilien"
     */
    public function processPages($modularPageDir, $modulars)
    {
        // TODO separate image import and markdown generation from grav
        // 3 column file: old url, new url, wordpress attachment id
        foreach ($modulars as $modular) {
            $sourcePageDir = $modularPageDir . '/' . $modular;

            if (strpos($modular, '_') === false) {
                WP_CLI::warning(sprintf("## $sourcePageDir : Sounds like a modular page... skipping"));
                continue;
            }
            $sourcePages = array_filter(scandir($sourcePageDir), fn ($file) => pathinfo($file)['extension'] === 'md');
            $sourcePage = reset($sourcePages);
            $sourceModularFile = $sourcePageDir . '/' . $sourcePage;

            $destinationModularDir = $this->destinationPath . '/' . basename($modularPageDir) . '/' . $modular;
            $destinationModularFile = $destinationModularDir . '/' . preg_replace('/\.(pt|de|es|it|en)_(BR|DE|ES|IT|GB|MX|US)/', '', $sourcePage);

            if ($sourcePage) {
                $document = self::$frontMatter->parse(file_get_contents($sourceModularFile));
                self::processPage($document, $sourcePageDir);

                if (!is_dir($destinationModularDir)) {
                    mkdir($destinationModularDir, 0755, true);
                }
                // save with imported images
                file_put_contents($destinationModularFile, self::$frontMatter->dump($document));
            }
        }
    }

    /**
     * Collect all assets in this directory (to identify unreferenced ones)
     */
    public static function getSiblingMedias($sourcePageDir)
    {
        global $KNOWN_MEDIA_EXTENSIONS_RE;
        $iterator = new GlobIterator($sourcePageDir . '/*', FilesystemIterator::SKIP_DOTS | FilesystemIterator::KEY_AS_PATHNAME);
        $regex = new RegexIterator($iterator, $KNOWN_MEDIA_EXTENSIONS_RE, RecursiveRegexIterator::MATCH | RegexIterator::INVERT_MATCH | RegexIterator::USE_KEY);
        return array_keys(iterator_to_array($regex));
    }

    /**
     * Process one page:
     * Extract Grav frontmatter identify images in the fields and uploaded them
     * Then change the YAML to include the freshly upload WP attachmentID
     * @param $document parsed frontmatter
     * @param $sourcePageDir the directory the file lives in (to look for medias)
     */
    public static function processPage($document, $sourcePageDir)
    {
        global $UNREF_ASSETS;

        WP_CLI::debug("Analyzing YAML at $sourcePageDir");
        $sourceAssets = self::getSiblingMedias($sourcePageDir);
        $data = $document->getData();
        $firstEdit = '';
        $uploadedPaths = [];
        uploadImages($sourcePageDir, $data, $firstEdit, $uploadedPaths);
        $unrefs = array_diff($sourceAssets, $uploadedPaths);
        $unrefs_uploaded = 0;
        foreach ($unrefs as $unref) {
            $md5 = md5(file_get_contents($unref));
            if (isset($UNREF_ASSETS[$md5])) {
                // Don't upload identical unrefs assets twice to a given WP instance
                continue;
            }
            $oldurl = preg_replace('!.*pages/*!', '', $unref);
            [$id, $is_new] = uploadOneImage($unref, $firstEdit, [AE_GRAV_FILE_META_KEY => $oldurl, AE_GRAV_PAGE_META_KEY => gravPathToUrl($sourcePageDir), '_grav_unref' => true]);
            if (is_wp_error($id)) {
                WP_CLI::error(sprintf("Couldn't upload %s", $unref));
                WP_CLI::error($id->get_error_message());
            } elseif ($is_new) {
                $UNREF_ASSETS[$md5] = [$id, $unref];
                $unrefs_uploaded++;
            }
            // count($unrefs);
        }
        if (count($unrefs)) {
            WP_CLI::log(sprintf("%d unreferenced files identified for this page. %d uploaded", count($unrefs), $unrefs_uploaded));
        }

        if (empty($data['date']) && $firstEdit) {
            $data['date'] = date('d-m-Y H:i', strtotime($firstEdit));
        }

        $document->setData($data);
    }
}

function redirection_map($prefix = null)
{
    /**
     * @var wpdb $wpdb
     */
    global $wpdb;
    $req = $wpdb->prepare(
        'SELECT post_id, post_name, post_date, post_modified, post_title, post_type, meta_value'
        . " FROM {$wpdb->postmeta}"
        . ' LEFT JOIN wp_posts ON post_id = id'
        . ' WHERE (post_type = %s AND meta_key = %s) OR (post_type IN (%s, %s) AND meta_key = %s)',
        'attachment', AE_GRAV_FILE_META_KEY, // Files
        ACTION_POST_TYPE, CAMPAIGN_POST_TYPE, AE_GRAV_PAGE_META_KEY // Pages
    );

    foreach ($wpdb->get_results($req) as $r) {
        $source = gravPathToUrl($r->meta_value);
        if ($prefix) {
            $source = rtrim($prefix, '/') . '/' . $source;
        }
        $id = $r->post_id;
        if ($r->post_type === 'attachment') {
            $dest = wp_get_attachment_url($id);
        } else {
            $p = get_post($id);
            //$r->r = sanitize_title($r->post_name ? $r->post_name : $r->post_title, $r->ID);
            $p->post_status = 'publish';
            $dest = get_post_permalink($p);
        }
        WP_CLI::log("$source\t$dest # {$id}");
    }
}

function link_library($dry_run)
{
    /**
     * @var wpdb $wpdb
     */
    global $wpdb;
    // Get all attachments
    $req = $wpdb->prepare('SELECT post_id, post_name, post_date, post_modified, post_title, post_parent, meta_value'
                        . " FROM {$wpdb->postmeta}"
                        . ' LEFT JOIN wp_posts ON post_id = id'
                        . ' WHERE post_type = %s AND meta_key = %s', 'attachment', AE_GRAV_FILE_META_KEY);

    // For each of them
    foreach ($wpdb->get_results($req) as $r) {
        $asset_old_path = $r->meta_value; // something ending in ".jpg"
        // Look at the pages having meta's _grav_original_page containing the path
        $old_path = gravPathToUrl($asset_old_path);
        $old_path = preg_replace('!^https?://([^/]+/){2}!', '', $old_path);
        $page_req = $wpdb->prepare('SELECT post_id, post_name, post_date, post_modified, post_title, meta_value'
                                   . " FROM {$wpdb->postmeta}"
                                   . ' LEFT JOIN wp_posts ON post_id = id'
                                   . ' WHERE post_type != %s AND meta_key = %s AND INSTR(%s, CONCAT(meta_value, "/")) > 0'
                                   . ' ORDER BY LENGTH(meta_value) DESC',
                                   'attachment', AE_GRAV_PAGE_META_KEY, $old_path);
        $pages = $wpdb->get_results($page_req);
        WP_CLI::debug("Searching '{$old_path}' leads to " . count($pages) . " results");
        if (count($pages) !== 1) {
            WP_CLI::warning("Ambiguity searching for '{$old_path}'");
            WP_CLI::debug(print_r($r, true));
            WP_CLI::warning("Matches:\n=> " . implode("\n-  ", array_map(fn ($e) => sprintf('"%s" (%d [%s])', $e->post_name, $e->post_id, $e->meta_value), $pages)));
        }
        if (!count($pages)) {
            var_dump("No result, exit");
            die;
        }

        $post_parent_id = $pages[0]->post_id;
        if ($r->post_parent === $post_parent_id) {
            continue;
        }

        WP_CLI::success(sprintf("%s %sset parent to %d", $r->post_name, $r->post_parent > 0 ? 're' : '', $post_parent_id));
        if ($dry_run) {
            continue;
        }

        wp_update_post([
            'ID' => $r->post_id,
            'post_parent' => $post_parent_id
        ]);
    }
}

function main($sourcePath, $destinationPath)
{
    global $frontMatter;

    if (!is_dir(TMPDIR)) {
        mkdir(TMPDIR, 0755);
    }

    $frontMatter = new \Webuni\FrontMatter\FrontMatter();
    if (!is_dir($destinationPath)) {
        mkdir($destinationPath, 0755, true);
    }
    $proc = new GravFolderProcess($frontMatter, $destinationPath);
    $proc->processPagesFolder($sourcePath);
}


class Grav2Wp extends \WP_CLI_Command {

    public function map($args, $assoc_args)
    {
        $prefix = getenv('PREFIX');
        if (!$prefix) {
            WP_CLI::warning("Define a prefix to generate a map of redirection, eg: https://animalequality.it/agisci/");
        }
        redirection_map($prefix);
    }

    /**
     * Link attachment to the parent page they were originally associated with.
     *
     * ## OPTIONS
     *
     * [--dry-run]
     * : Only show what would have been done
     *
     */

    public function link($args, $assoc_args)
    {
        link_library($assoc_args['dry-run']);
    }

    /**
     * Insert attachments and generate Grav frontmatter linking to inserted images
     *
     * ## OPTIONS
     *
     * <directory>
     * : Grav pages directory or specific page directory
     *
     *
     * [--outdir=<path>]
     * : Destination path to create modified copy of the markdown files.
     * or use the <OUTDIR> environment variable
     *
     * [--dry-run]
     * : Rollback SQL transactions without committing
     *
     */
    public function migrate($args, $assoc_args)
    {
        require_once((is_dir('/tmp/vendor') ? '/tmp' : '..') . '/vendor/autoload.php');

        global $DRY_RUN_LOG, $wpdb, $UPLOADED_ASSETS, $UNREF_ASSETS;
        $dry_run = $assoc_args['dry-run'] ?? false;
        $sourcePath = rtrim($args[0], '/');
        $destinationPath = ($assoc_args['outdir'] ?? false) ?: getenv('OUTDIR') ?: DEFAULT_OUTDIR;

        add_filter('posts_where', 'title_filter', 10, 2);

        if ($dry_run) {
            $wpdb->query("START TRANSACTION;");
        }

        main($sourcePath, $destinationPath);

        WP_CLI::success(sprintf("Summary: %d total uploads (including %d unreferenced files):", count($UPLOADED_ASSETS), count($UNREF_ASSETS)));
        WP_CLI::log("Unref:\n" . print_r($UNREF_ASSETS, true));
        WP_CLI::log("Uploaded:\n" . print_r($UPLOADED_ASSETS, true));

        if ($dry_run) {
            WP_CLI::success("Dry-run mode, cleaning up SQL. Please remove manually the following files:");
            $wpdb->query("ROLLBACK;");
            WP_CLI::log(implode("\n", $DRY_RUN_LOG));
        }
    }
}

WP_CLI::add_command('grav2wp', Grav2wp::class);

// TODO:
// Changing animalequality.me inside wp_posts GUID


# snip
<<<'EOF'
  Verify:
  # list files (basename) on the Grav filesystem
  find de/pages -type f -not -name '*.md' -not -name 'doi.jpg' -ls|awk -F/ '{print $NF}'|sort -u
  # list files actually inserted
  mysql -N aede <<< "SELECT DISTINCT meta_value FROM wp_postmeta LEFT JOIN wp_posts ON post_id = id WHERE meta_key like '_grav_%'"|xargs -n1 basename|sort -u
  # => compare why some files are missing
EOF;
