import {createBlockFromTemplate, setInnerBlocks, getInnerBlock, convertMarkdownToHtml, removeInnerBlock, getImageData, setBlockAttribute, setColorSchemeOfBlock, addInnerBlocks} from '../utils.js';
import {rawHandler} from '@wordpress/blocks';

export default (modular, colorScheme) => {
  let pattern = null;

  if (modular.layout === 'content') {
    pattern = createBlockFromTemplate('animalequality/pattern-media-content');
    const group = getInnerBlock(pattern, 'core/group');
    const columns = getInnerBlock(group, 'core/columns');
    const columnMedia = getInnerBlock(columns, 0);
    const columnContent = getInnerBlock(columns, 1);
    const buttons = getInnerBlock(columnContent, 'core/buttons');
    const imageData = getImageData(modular, 'image');

    setColorSchemeOfBlock(group, colorScheme, 'light');

    if (imageData) {
      setBlockAttribute(getInnerBlock(columnMedia, 'core/image'), 'url', imageData.url);
      setBlockAttribute(getInnerBlock(columnMedia, 'core/image'), 'id', imageData.id);
    }

    if (modular.title) {
      setBlockAttribute(getInnerBlock(columnContent, 'core/heading'), 'content', modular.title);
    } else {
      removeInnerBlock(columnContent, 'core/heading');
    }

    if (modular.id) {
      setBlockAttribute(group, 'anchor', modular.id);
    }

    if (modular.content) {
      setBlockAttribute(getInnerBlock(columnContent, 'core/paragraph'), 'content', convertMarkdownToHtml(modular.content));
    } else {
      removeInnerBlock(columnContent, 'core/paragraph');
    }

    if (modular.button && modular.button.label && modular.button.url) {
      setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'text', modular.button.label);
      setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'url', modular.button.url);
    } else {
      removeInnerBlock(columnContent, 'core/buttons');
    }

    if (modular.image_alignment === 'right') {
      setInnerBlocks(columns, [columnContent, columnMedia]);
    }
  } else {
    pattern = createBlockFromTemplate(modular.isTwoThird ? 'animalequality/pattern-featured' : 'animalequality/pattern-featured-half');
    const mediaText = getInnerBlock(pattern, 'core/media-text');
    const group = getInnerBlock(mediaText, 'core/group');
    const buttons = getInnerBlock(group, 'core/buttons');
    const imageData = getImageData(modular, 'image');
    let blockToRemove = 0;

    setColorSchemeOfBlock(mediaText, colorScheme, 'dark');

    if (imageData) {
      setBlockAttribute(mediaText, 'mediaUrl', imageData.url);
      setBlockAttribute(mediaText, 'mediaId', imageData.id);
    }

    setBlockAttribute(getInnerBlock(group, 0), 'content', modular.pretitle);
    setBlockAttribute(getInnerBlock(group, 1), 'content', modular.title);

    if (modular.author) {
      addInnerBlocks(group, [['core/paragraph', {content: modular.author, fontSize: 'small'}]]);
    }

    if (modular.title_alignment) {
      setBlockAttribute(getInnerBlock(group, 1), 'align', modular.title_alignment);
    }

    if (!modular.pretitle) {
      removeInnerBlock(group, blockToRemove);
    } else {
      blockToRemove++;
    }

    if (!modular.title) {
      removeInnerBlock(group, blockToRemove);
    } else {
      blockToRemove++;
    }

    if (modular.id) {
      setBlockAttribute(mediaText, 'anchor', modular.id);
    }

    if (modular.content) {
      addInnerBlocks(group, rawHandler({HTML: modular.content}), false, getInnerBlock(group, blockToRemove));
    } else {
      removeInnerBlock(group, blockToRemove);
    }

    if (modular.button && modular.button.label && modular.button.url) {
      setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'text', modular.button.label);
      setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'url', modular.button.url);
    } else {
      removeInnerBlock(group, 'core/buttons');
    }

    if (modular.image_alignment === 'right') {
      setBlockAttribute(mediaText, 'mediaPosition', 'right');
    }
  }

  return wp.blocks.serialize(pattern);
};
