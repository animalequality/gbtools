import {createBlockFromTemplate, addInnerBlocks, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock, convertMarkdownToHtml} from '../utils.js';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-simple');
  const group = getInnerBlock(pattern, 'core/group');

  setColorSchemeOfBlock(group, colorScheme, 'white');

  if (modular.title) {
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  if (modular.id) {
    setBlockAttribute(group, 'anchor', modular.id);
  }

  if (modular.content) {
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', convertMarkdownToHtml(modular.content));
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'align', 'center');
  } else {
    removeInnerBlock(group, 'core/paragraph');
  }

  addInnerBlocks(group, [['core/shortcode', {text: `[gravityform id=${modular.id} title=false description=false ajax=true tabindex=49]`}]]);

  return wp.blocks.serialize(pattern);
};
