import {createBlockFromTemplate, convertMarkdownToHtml, adjustInnerBlockCount, getImageData, getInnerBlock, setBlockAttribute, setInnerBlocks, removeInnerBlock} from '../utils.js';
import chunk from 'lodash.chunk';
import {cloneBlock} from '@wordpress/blocks';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-logos');
  const cover = getInnerBlock(pattern, 'core/cover');
  const group = getInnerBlock(cover, 'core/group');
  const columns = getInnerBlock(group, 'core/columns');
  const columnsTemplate = cloneBlock(columns);

  if (!modular.items || !modular.items.length) {
    return '';
  }

  if (modular.background) {
    const bgImageData = getImageData(modular, 'background');

    setBlockAttribute(cover, 'url', bgImageData.url);
    setBlockAttribute(cover, 'id', bgImageData.id);
  } else {
    setBlockAttribute(cover, 'dimRatio', 0);
  }

  chunk(modular.items, 4).forEach((chunk, indexChunk) => {
    if (indexChunk > 0) {
      setInnerBlocks(group, group.innerBlocks.concat([columnsTemplate]));
    }

    const currentColumns = getInnerBlock(group, indexChunk + 2);

    adjustInnerBlockCount(currentColumns, chunk.length);
    setBlockAttribute(currentColumns, 'align', 'wide');

    chunk.forEach((logo, index) => {
      const imageBlock = getInnerBlock(currentColumns, [index, 0]);
      const imageData = getImageData(logo, 'image');

      setBlockAttribute(imageBlock, 'url', imageData.url);
      setBlockAttribute(imageBlock, 'id', imageData.id);
      setBlockAttribute(imageBlock, 'caption', '');
    });
  });

  if (modular.title) {
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  if (modular.id) {
    setBlockAttribute(cover, 'anchor', modular.id);
  }

  if (modular.content) {
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', convertMarkdownToHtml(modular.content));
  } else {
    removeInnerBlock(group, 'core/paragraph');
  }

  return wp.blocks.serialize(pattern);
};
