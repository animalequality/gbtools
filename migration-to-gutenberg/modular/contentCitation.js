import {createBlockFromTemplate, adjustInnerBlockCount, setColorSchemeOfBlock, setBlockAttribute, getImageData, setInnerBlocks, removeInnerBlock, getInnerBlock} from '../utils.js';
import util from 'util';
export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-quotes');
  const group = getInnerBlock(pattern, 'core/group');
  const columns = getInnerBlock(group, 'core/columns');

  if (!modular.citations || !modular.citations.length) {
    return '';
  }

  setColorSchemeOfBlock(group, colorScheme, 'dark');
  adjustInnerBlockCount(columns, modular.citations.length);
  setBlockAttribute(columns, 'align', modular.citations.length === 1 ? 'none' : 'wide');

  modular.citations.forEach((citation, index) => {
    const column = getInnerBlock(columns, index);
    const columnGroup = getInnerBlock(column, 'core/group');
    const citationBlock = getInnerBlock(columnGroup, 'core/quote');
    const imageData = getImageData(citation, 'image');

    if (imageData && imageData.url) {
      setBlockAttribute(getInnerBlock(columnGroup, 'core/image'), 'url', imageData.url);
      setBlockAttribute(getInnerBlock(columnGroup, 'core/image'), 'id', imageData.id);
    } else {
      removeInnerBlock(columnGroup, 'core/image');
    }

    setInnerBlocks(citationBlock, [['core/paragraph', {align: 'center', content: citation.text}]]);
    setBlockAttribute(citationBlock, 'textAlign', 'center');
    setBlockAttribute(citationBlock, 'align', 'center');
    setBlockAttribute(citationBlock, 'citation', citation.author);
  });

  if (modular.title) {
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  if (modular.id) {
    setBlockAttribute(group, 'anchor', modular.id);
  }

  removeInnerBlock(group, 'core/paragraph');

  return wp.blocks.serialize(pattern);
};
