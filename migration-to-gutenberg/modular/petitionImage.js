import {createBlockFromTemplate, setInnerBlocks, setBlockAttribute, getInnerBlock, getImageData, setColorSchemeOfBlock, getEmbedBlock} from '../utils.js';

export default (modular, colorScheme) => {
    let pattern = null;

    pattern = createBlockFromTemplate('animalequality/pattern-media-content');
    const group = getInnerBlock(pattern, 'core/group');
    const columns = getInnerBlock(group, 'core/columns');
    const columnMedia = getInnerBlock(columns, 0);
    const columnContent = getInnerBlock(columns, 1);
    const imageData = getImageData(modular, 'image');

    setColorSchemeOfBlock(group, colorScheme);

    setInnerBlocks(columnContent, []);

    if (modular['vimeo_id']) {
        setInnerBlocks(columnMedia, getEmbedBlock('vimeo', modular['vimeo_id']));
    } else if (modular['youtube_id']) {
        setInnerBlocks(columnMedia, getEmbedBlock('youtube', modular['youtube_id']));
    } else if(modular.image && imageData && imageData.url && imageData.id) {
        setBlockAttribute(getInnerBlock(columnMedia, 'core/image'), 'url', imageData.url);
        setBlockAttribute(getInnerBlock(columnMedia, 'core/image'), 'id', imageData.id);
    }

    return wp.blocks.serialize(pattern);
};
