import {stripHtml} from 'string-strip-html';
import {createBlockFromTemplate, addInnerBlocks, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-simple');
    const group = getInnerBlock(pattern, 'core/group');

    setColorSchemeOfBlock(group, colorScheme, 'white');

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(group, 'core/heading');
    }

    removeInnerBlock(group, 'core/paragraph');

    addInnerBlocks(group, [['animalequality/msender', {
        subject: modular.subject,
        message: stripHtml(modular.content).result,
        to: modular.to.map(contact => `${contact.name}|${contact.email}`).join('\n'),
        cc: modular.cc ? modular.cc.map(contact => `${contact.name}|${contact.email}`).join('\n') : null,
        bcc: modular.bcc ? modular.bcc.map(contact => `${contact.name}|${contact.email}`).join('\n') : null,
        newsletterEnabled: modular.newsletter !== '0',
        newsletterPreselected: modular.newsletter === '2',
        newsletterEndpoint: modular.newsletter_endpoint,
        newsletterLabel: modular.newsletter_text
    }]]);

    return wp.blocks.serialize(pattern);
};
