import {createBlockFromTemplate, removeInnerBlock, convertMarkdownToHtml, getImageData, getInnerBlock, setBlockAttribute, addInnerBlocks, shortcodes} from '../utils.js';
import {stripHtml} from 'string-strip-html';
import {rawHandler} from '@wordpress/blocks';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-hero');
  const cover = getInnerBlock(pattern, 'core/cover');
  const group = getInnerBlock(cover, 'core/group');
  const buttons = getInnerBlock(group, 'core/buttons');
  const imageData = getImageData(modular, 'background_image');

  if (imageData && imageData.url && imageData.id) {
    setBlockAttribute(cover, 'url', imageData.url);
    setBlockAttribute(cover, 'id', imageData.id);
  }

  if (!modular.title && !modular.text && !modular.button_text) {
    setBlockAttribute(cover, 'dimRatio', 0);
  }

  if (modular.title) {
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  if (modular.id) {
    setBlockAttribute(cover, 'anchor', modular.id);
  }

  if (modular.text) {
    const textContent = rawHandler({HTML: modular.text + (modular.author_text ? `<p>${modular.author_text}</p>` : '')});
    textContent.forEach(block => {
      if (block.name === 'core/heading') {
        setBlockAttribute(block, 'textAlign', modular.content_position);
      } else {
        setBlockAttribute(block, 'align', modular.content_position);
      }
    });

    addInnerBlocks(group, textContent, false, getInnerBlock(group, 'core/paragraph'));
  } else {
    removeInnerBlock(group, 'core/paragraph');
  }

  if (modular.embed) {
    const textContent = rawHandler({HTML: modular.embed});
    textContent.forEach(block => {
      if (block.name === 'core/heading') {
        setBlockAttribute(block, 'textAlign', modular.content_position);
      } else {
        setBlockAttribute(block, 'align', modular.content_position);
      }
    });

    addInnerBlocks(group, textContent, false);
  }

  if (modular.button_text && modular.button_target) {
    setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'text', modular.button_text);
    setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'url', modular.button_target);

    if (modular.button_color) {
      setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'backgroundColor', modular.button_color);
    }
  } else {
    removeInnerBlock(group, 'core/buttons');
  }

  if (modular.content_position) {
    setBlockAttribute(cover, 'contentPosition', `center ${modular.content_position}`);
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'textAlign', modular.content_position);
    setBlockAttribute(getInnerBlock(group, 'core/buttons'), 'layout', {type: 'flex', justifyContent: modular.content_position});
  }

  return wp.blocks.serialize(pattern);
};
