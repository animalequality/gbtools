import {stripHtml} from 'string-strip-html';
import {createBlockFromTemplate, addInnerBlocks, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock, convertMarkdownToHtml} from '../utils.js';
import {rawHandler} from '@wordpress/blocks';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-simple');
  const group = getInnerBlock(pattern, 'core/group');

  setColorSchemeOfBlock(group, colorScheme, 'white');

  if (modular.title) {
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  if (modular.id) {
    setBlockAttribute(group, 'anchor', modular.id);
  }

  removeInnerBlock(group, 'core/paragraph');

  if (modular.content) {
    const content = rawHandler({HTML: modular.content});
    content.forEach(block => {
      if (modular.alignment) {
        if (block.name === 'core/heading') {
          setBlockAttribute(block, 'textAlign', modular.alignment);
        } else if (block.name !== 'core/image') {
          setBlockAttribute(block, 'align', modular.alignment);
        }
      }
    });
    addInnerBlocks(group, content);
  }

  if (modular.button_label && modular.button_target) {
    addInnerBlocks(group, [['core/buttons', {
      layout: {type: 'flex', justifyContent: modular.alignment || 'left'}
    }, [['core/button', {
      url: modular.button_target,
      text: modular.button_label,
      backgroundColor: 'primary',
      linkTarget: modular.button_new_tab ? '_blank' : ''
    }]]]]);
  }

  return wp.blocks.serialize(pattern);
};
