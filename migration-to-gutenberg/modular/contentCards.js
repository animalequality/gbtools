import chunk from 'lodash.chunk';
import {addInnerBlocks, createBlockFromTemplate, adjustInnerBlockCount, setColorSchemeOfBlock, getInnerBlock, setBlockAttribute, setInnerBlocks, removeInnerBlock} from '../utils.js';
import {cloneBlock, rawHandler} from '@wordpress/blocks';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-cards-text');
  const group = getInnerBlock(pattern, 'core/group');
  const columns = getInnerBlock(group, 'core/columns');

  if (!modular.cards || !modular.cards.length) {
    return '';
  }

  setColorSchemeOfBlock(group, 'white');

  chunk(modular.cards, 3).forEach((chunk, indexChunk) => {
    if (indexChunk > 0) {
      setInnerBlocks(group, group.innerBlocks.concat([cloneBlock(columns)]));
    }

    const currentColumns = getInnerBlock(group, indexChunk + 2);

    adjustInnerBlockCount(currentColumns, chunk.length);
    setBlockAttribute(currentColumns, 'align', chunk.length === 1 ? 'none' : 'wide');

    chunk.forEach((card, index) => {
      const column = getInnerBlock(currentColumns, index);
      const columnGroup = getInnerBlock(column, 'core/group');
      const groupTitle = getInnerBlock(columnGroup, 0);
      const groupContent = getInnerBlock(columnGroup, 1);

      setBlockAttribute(getInnerBlock(groupTitle, 'core/paragraph'), 'content', `<strong>${card.title}</strong>`);

      if (card.content) {
        const textContent = rawHandler({HTML: card.content});

        textContent.forEach(block => {
          setBlockAttribute(block, 'align', 'center');
        });

        addInnerBlocks(groupContent, textContent, false, getInnerBlock(groupContent, 0));
      } else {
        removeInnerBlock(groupContent, 0);
      }

      setBlockAttribute(getInnerBlock(groupContent, 'core/paragraph'), 'align', 'center');
    });
  });

  if (modular.title) {
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  removeInnerBlock(group, 'core/paragraph');

  return wp.blocks.serialize(pattern);
};
