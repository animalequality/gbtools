import {createBlockFromTemplate, addInnerBlocks, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock, convertMarkdownToHtml} from '../utils.js';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-simple');
  const group = getInnerBlock(pattern, 'core/group');
  const firstElement = modular.elements[0];

  setColorSchemeOfBlock(group, colorScheme, 'white');

  if (modular.id) {
    setBlockAttribute(group, 'anchor', modular.id);
  }

  if (firstElement.title) {
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', firstElement.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  if (firstElement.teaser) {
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', firstElement.teaser);
  } else {
    removeInnerBlock(group, 'core/paragraph');
  }

  if (firstElement.content) {
    addInnerBlocks(group, [['core/html', {content: firstElement.content}]]);
  }

  return wp.blocks.serialize(pattern);
};
