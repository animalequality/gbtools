import {createBlockFromTemplate, adjustInnerBlockCount, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock} from '../utils.js';

export default (modular, colorScheme, lang, lvUrl, donateUrl) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-icons');
    const group = getInnerBlock(pattern, 'core/group');
    const columns = getInnerBlock(group, 'core/columns');

    if (!modular.items) {
        return '';
    }

    setColorSchemeOfBlock(group, colorScheme);
    adjustInnerBlockCount(columns, modular.items.length);

    modular.items.forEach((item, index) => {
        const column = getInnerBlock(columns, index);
        let link = item.link;

        if (item.type === 'handdrawn-loveveg') {
          link = lvUrl;
        } else if (item.type === 'handdrawn-sign' || item.type === 'handdrawn-signature') {
          link = '#petition';
        } else if (item.type === 'handdrawn-donate-euro' || item.type === 'handdrawn-donate') {
          link = donateUrl;
        }

        setBlockAttribute(getInnerBlock(column, 'animalequality/icon'), 'id', item.type);
        setBlockAttribute(getInnerBlock(column, 2), 'content', `<a href="${link}" ${!link.includes('#') ? 'target="_blank" rel="noopener"' : ''}>${item.label}</a>`);

        if (item.text) {
            setBlockAttribute(getInnerBlock(column, 1), 'content', item.text);
        } else {
            removeInnerBlock(column, 1);
        }
    });

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(group, 'core/heading');
    }

    if (modular.text) {
        setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', modular.content);
    } else {
        removeInnerBlock(group, 'core/paragraph');
    }

    return wp.blocks.serialize(pattern);
};
