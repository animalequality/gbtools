import {createBlockFromTemplate} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate(
        'animalequality/pattern-latest-updates',
        wp.blocks.getBlockType('animalequality/pattern-latest-updates')._template([{id: 2}], [{id: 3}])
    );

    return wp.blocks.serialize(pattern);
};
