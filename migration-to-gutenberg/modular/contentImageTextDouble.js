import {createBlockFromTemplate, setInnerBlocks, getInnerBlock, convertMarkdownToHtml, removeInnerBlock, getImageData, setBlockAttribute} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-featured-double');
    const columns = getInnerBlock(pattern, 'core/columns');
    const mediaText = getInnerBlock(columns, [1, 0]);
    const group = getInnerBlock(mediaText, 'core/group');
    const imageLeft = getInnerBlock(columns, [0, 0]);
    const imageDataLeft = getImageData(modular, 'image2');
    const imageDataRight = getImageData(modular, 'image');
    let hasHeadlineRemoved = false;

    setBlockAttribute(mediaText, 'mediaUrl', imageDataRight.url);
    setBlockAttribute(mediaText, 'mediaId', imageDataRight.id);

    setBlockAttribute(imageLeft, 'url', imageDataLeft.url);
    setBlockAttribute(imageLeft, 'id', imageDataLeft.id);

    removeInnerBlock(group, 0);
    removeInnerBlock(group, 'core/buttons');

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 0), 'content', modular.title);
    } else {
        removeInnerBlock(group, 0);
        hasHeadlineRemoved = true;
    }

    if (modular.content) {
        setBlockAttribute(getInnerBlock(group, 1), 'content', convertMarkdownToHtml(modular.content));
    } else {
        removeInnerBlock(group, hasHeadlineRemoved ? 0 : 1);
    }

    return wp.blocks.serialize(pattern);
};
