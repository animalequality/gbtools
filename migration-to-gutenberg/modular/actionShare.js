import {stripHtml} from 'string-strip-html';
import {createBlockFromTemplate, adjustInnerBlockCount, setColorSchemeOfBlock, setBlockAttribute, removeInnerBlock, getInnerBlock} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-share');
    const group = getInnerBlock(pattern, 'core/group');
    const columns = getInnerBlock(group, 'core/columns');

    setColorSchemeOfBlock(group, colorScheme, 'light');
    adjustInnerBlockCount(columns, modular.items.length);

    modular.items.forEach((share, index) => {
        const shareBlock = getInnerBlock(columns, [index, 0]);

        setBlockAttribute(shareBlock, 'type', share.type);
        setBlockAttribute(shareBlock, 'label', share.text && stripHtml(share.text).result);
        setBlockAttribute(shareBlock, 'message', share.message);
    });

    removeInnerBlock(group, 1);

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(group, 0);
    }

    return wp.blocks.serialize(pattern);
};
