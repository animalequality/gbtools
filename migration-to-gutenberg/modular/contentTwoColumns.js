import {createBlockFromTemplate, convertMarkdownToHtml, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-two-columns');
    const group = getInnerBlock(pattern, 'core/group');
    const columns = getInnerBlock(group, 'core/columns');
    const left = getInnerBlock(columns, 0);
    const right = getInnerBlock(columns, 1);

    setColorSchemeOfBlock(group, colorScheme, 'white');

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(group, 'core/heading');
    }

    setBlockAttribute(getInnerBlock(left, 'core/paragraph'), 'content', convertMarkdownToHtml(modular.content));
    setBlockAttribute(getInnerBlock(right, 'core/paragraph'), 'content', convertMarkdownToHtml(modular['content_right']));

    return wp.blocks.serialize(pattern);
};
