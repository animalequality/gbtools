import {createBlockFromTemplate, addInnerBlocks, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock, getEmbedBlock} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-simple');
    const group = getInnerBlock(pattern, 'core/group');

    setColorSchemeOfBlock(group, colorScheme);
    removeInnerBlock(group, 'core/paragraph');

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(group, 'core/heading');
    }

    if (modular['vimeo_id']) {
        addInnerBlocks(group, getEmbedBlock('vimeo', modular['vimeo_id']));
    } else if (modular['youtube_id']) {
        addInnerBlocks(group, getEmbedBlock('youtube', modular['youtube_id']));
    }

    return wp.blocks.serialize(pattern);
};
