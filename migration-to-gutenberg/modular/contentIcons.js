import chunk from 'lodash.chunk';
import {stripHtml} from 'string-strip-html';
import {createBlockFromTemplate, adjustInnerBlockCount, setBlockAttribute, removeInnerBlock, getInnerBlock, setInnerBlocks, setColorSchemeOfBlock} from '../utils.js';
import {cloneBlock} from '@wordpress/blocks';

export default (modular, colorScheme) => {
  const isIcons = (!modular.icons[0].number && !modular.icons[0].id) || (modular.icons[0].number && stripHtml(modular.icons[0].number).result !== modular.icons[0].number);
  const pattern = createBlockFromTemplate(isIcons ? 'animalequality/pattern-icons' : 'animalequality/pattern-statistics');
  const group = getInnerBlock(pattern, 'core/group');
  const columns = getInnerBlock(group, 'core/columns');

  setColorSchemeOfBlock(group, colorScheme);

  chunk(modular.icons, 3).forEach((chunk, indexChunk) => {
    if (indexChunk > 0) {
      setInnerBlocks(group, group.innerBlocks.concat([cloneBlock(columns)]));
    }

    const currentColumns = getInnerBlock(group, indexChunk + 2);

    adjustInnerBlockCount(currentColumns, chunk.length);
    setBlockAttribute(currentColumns, 'align', 'wide');

    chunk.forEach((item, index) => {
      if (isIcons) {
        const iconCol = getInnerBlock(currentColumns, index);
        const innerBlocks = [['animalequality/icon', {id: item.icon}]];

        if (item.number) {
          innerBlocks.push(['core/paragraph', {content: `<strong>${stripHtml(item.number).result}</strong>`, align: 'center', textColor: 'secondary', fontSize: 'extra-large'}]);
        }

        if (item.text) {
          innerBlocks.push(['core/paragraph', {content: item.text.replace(/data-tooltip="(.*?)"/, match => stripHtml(match).result), align: 'center'}]);
        }

        setInnerBlocks(iconCol, innerBlocks);
      } else {
        const statistic = getInnerBlock(currentColumns, [index, 0]);

        if (item.id) {
          setBlockAttribute(statistic, 'id', item.id);
        } else {
          setBlockAttribute(statistic, 'id', '');
          setBlockAttribute(statistic, 'label', item.text);
          setBlockAttribute(statistic, 'fmt', item.number);
          setBlockAttribute(statistic, 'icon', item.icon);
        }
      }
    });
  });

  if (modular.title) {
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  if (modular.content) {
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', modular.content);
  } else {
    removeInnerBlock(group, 'core/paragraph');
  }

  return wp.blocks.serialize(pattern);
};
