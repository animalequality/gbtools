import {createBlockFromTemplate, addInnerBlocks, setBlockAttribute, getInnerBlock, setColorSchemeOfBlock, removeInnerBlock} from '../utils.js';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-post-cards');
  const group = getInnerBlock(pattern, 'core/group');
  const title = getInnerBlock(group, 'core/heading');
  const query = getInnerBlock(group, 'core/query');
  const postTemplate = getInnerBlock(query, 'core/post-template');
  const postTemplateGroup = getInnerBlock(postTemplate, 'core/group');
  const postImage = getInnerBlock(postTemplate, 'core/post-featured-image');
  const postTitle = getInnerBlock(postTemplateGroup, 'core/post-title');

  addInnerBlocks(query, [['core/query-pagination', {}, [['core/query-pagination-previous'], ['core/query-pagination-numbers'], ['core/query-pagination-next']]]]);
  removeInnerBlock(postTemplate, 'animalequality/post-meta-button');

  const pagination = getInnerBlock(query, 'core/query-pagination');

  setBlockAttribute(pagination, 'paginationArrow', 'chevron');
  setBlockAttribute(pagination, 'showLabel', false);
  setBlockAttribute(pagination, 'align', 'center');

  setColorSchemeOfBlock(group, colorScheme, 'light-grey');

  if (modular.title) {
    setBlockAttribute(title, 'content', modular.title);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  setBlockAttribute(query, 'query', {...query.attributes.query, postType: 'press_release'});
  setBlockAttribute(query, 'align', 'wide');
  setBlockAttribute(postImage, 'isLink', true);
  setBlockAttribute(postTitle, 'isLink', true);

  return wp.blocks.serialize(pattern);
};
