import {createBlockFromTemplate, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock, adjustInnerBlockCount, getImageData} from '../utils.js';

const readMoreTranslations = {
  de_DE: 'Mehr lesen',
  it_IT: 'Per saperne di più',
  pt_BR: 'Consulte Mais informação',
  en_US: 'Read more',
  es_ES: 'Leer más',
  es_MX: 'Leer más',
  en_GB: 'Read more'
};
export default (modular, colorScheme, lang) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-timeline');
  const group = getInnerBlock(pattern, 'core/group');
  const heading = getInnerBlock(group, 'core/heading');
  const groupInner = getInnerBlock(group, 'core/group');
  const groupInnerInner = getInnerBlock(groupInner, 'core/group');

  adjustInnerBlockCount(groupInnerInner, modular.events.length);
  setColorSchemeOfBlock(group, colorScheme, 'white');

  if (modular.year) {
    setBlockAttribute(heading, 'content', modular.year);
  } else {
    removeInnerBlock(group, 'core/heading');
  }

  if (modular.events) {
    modular.events.forEach((event, index) => {
      const mediaText = getInnerBlock(groupInnerInner, index);
      const group = getInnerBlock(mediaText, 'core/group');
      const year = getInnerBlock(group, 0);
      const text = getInnerBlock(group, 1);
      const imageData = getImageData(event, 'image');

      setBlockAttribute(mediaText, 'mediaId', imageData.id);
      setBlockAttribute(mediaText, 'mediaUrl', imageData.url);
      setBlockAttribute(year, 'content', `<strong>${event.title}</strong>`);
      setBlockAttribute(text, 'content', `${event.text}${event.link ? `<br><br><a href="${event.link}">${readMoreTranslations[lang]}</a>` : ''}`);
    });
  }

  return wp.blocks.serialize(pattern);
};
