import {createBlockFromTemplate, addInnerBlocks, getImageData, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock, convertMarkdownToHtml} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-simple');
    const group = getInnerBlock(pattern, 'core/group');

    setColorSchemeOfBlock(group, colorScheme, 'white');

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(group, 'core/heading');
    }

    if (modular.content) {
        setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', convertMarkdownToHtml(modular.content));
    } else {
        removeInnerBlock(group, 'core/paragraph');
    }

    if (modular['button_label'] && modular['button_target']) {
        addInnerBlocks(group, [['core/buttons', {
            layout: {type: 'flex', justifyContent: 'center'},
            style: {spacing: {margin: {top: '2rem', bottom: "2rem"}}}
        }, [['core/button', {
            url: modular['button_target'],
            text: modular['button_label'],
            backgroundColor: 'primary'
        }]]]]);
    }

    if (modular.image) {
        addInnerBlocks(group, [['core/image', {
            url: getImageData(modular, 'image').url,
            id: getImageData(modular, 'image').id
        }]]);
    }

    return wp.blocks.serialize(pattern);
};
