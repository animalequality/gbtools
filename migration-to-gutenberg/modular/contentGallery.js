import {createBlockFromTemplate, setColorSchemeOfBlock, convertMarkdownToHtml, adjustInnerBlockCount, setBlockAttribute, getImageData, removeInnerBlock, getInnerBlock, addInnerBlocks} from '../utils.js';

export default (modular, colorScheme, lang, lvUrl, donateUrl, alignment = 'wide', innerLastBlock = null) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-gallery');
    const group = getInnerBlock(pattern, 'core/group');
    const gallery = getInnerBlock(group, 'core/gallery');

    if (!modular.items || !modular.items.length) {
        return '';
    }

    setColorSchemeOfBlock(group, colorScheme, 'light');
    adjustInnerBlockCount(gallery, modular.items.length);
    setBlockAttribute(gallery, 'align', alignment);

    for(const [index, item] of modular.items.entries()) {
        const imageBlock = getInnerBlock(gallery, index);
        const imageData = getImageData(item, 'image');

        if (imageData === null) {
          return;
        }

        setBlockAttribute(imageBlock, 'url', imageData.url);
        setBlockAttribute(imageBlock, 'id', imageData.id);
        setBlockAttribute(imageBlock, 'caption', convertMarkdownToHtml(item.description));
    };

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(group, 'core/heading');
    }

    if (modular.content) {
        setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', convertMarkdownToHtml(modular.content));
    } else {
        removeInnerBlock(group, 'core/paragraph');
    }

    if (innerLastBlock) {
        addInnerBlocks(group, innerLastBlock);
    }

    return wp.blocks.serialize(pattern);
};
