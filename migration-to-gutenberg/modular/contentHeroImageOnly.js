import {createBlockFromTemplate, removeInnerBlock, getInnerBlock, setBlockAttribute, getImageData} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-hero');
    const cover = getInnerBlock(pattern, 'core/cover');
    const group = getInnerBlock(cover, 'core/group');
    const imageData = getImageData(modular, 'image_landscape');

    if (modular['image_landscape']) {
        setBlockAttribute(cover, 'url', imageData.url);
        setBlockAttribute(cover, 'id', imageData.id);
        setBlockAttribute(cover, 'dimRatio', 0);
    }

    removeInnerBlock(group, 'core/heading');
    removeInnerBlock(group, 'core/paragraph');
    removeInnerBlock(group, 'core/buttons');

    return wp.blocks.serialize(pattern);
};
