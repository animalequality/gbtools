import {createBlockFromTemplate, setInnerBlocks, removeInnerBlock, getInnerBlock, setBlockAttribute, setColorSchemeOfBlock, addInnerBlocks} from '../utils.js';
import {rawHandler} from '@wordpress/blocks';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-media-content');
  const group = getInnerBlock(pattern, 'core/group');
  const columns = getInnerBlock(group, 'core/columns');
  const columnMedia = getInnerBlock(columns, 0);
  const columnContent = getInnerBlock(columns, 1);

  setColorSchemeOfBlock(group, colorScheme, 'light');
  setInnerBlocks(columnMedia, [['oik-sb/chart', {
    type: modular.type,
    content: modular.chartContent ? modular.chartContent.replace(/\|/g, '\n') : null,
    myChartId: `chart-${Math.ceil(Math.random() * 100000)}`
  }]]);

  if (modular.title) {
    setBlockAttribute(getInnerBlock(columnContent, 'core/heading'), 'content', modular.title);
  } else {
    removeInnerBlock(columnContent, 'core/heading');
  }

  if (modular.content) {
    const contentBlocks = rawHandler({HTML: modular.content});
    addInnerBlocks(columnContent, contentBlocks, false, getInnerBlock(columnContent, 'core/paragraph'));
  }

  if (modular.button && modular.button.label && modular.button.url) {
    const buttons = getInnerBlock(columnContent, 'core/buttons');
    const button = getInnerBlock(buttons, 'core/button');
    setBlockAttribute(button, 'text', modular.button.label);
    setBlockAttribute(button, 'url', modular.button.url);
  } else {
    removeInnerBlock(columnContent, 'core/buttons');
  }

  return wp.blocks.serialize(pattern);
};
