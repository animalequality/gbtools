import {createBlockFromTemplate, removeInnerBlock, getImageData, getInnerBlock, setBlockAttribute} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-hero');
    const cover = getInnerBlock(pattern, 'core/cover');
    const group = getInnerBlock(cover, 'core/group');
    const imageData = getImageData(modular, 'background_image');

    if (imageData && imageData.url && imageData.id) {
        setBlockAttribute(cover, 'url', imageData.url);
        setBlockAttribute(cover, 'id', imageData.id);
    }

    setBlockAttribute(cover, 'contentPosition', 'center center');

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'textAlign', 'center');
    } else {
        removeInnerBlock(group, 'core/heading');
    }

    removeInnerBlock(group, 'core/buttons');
    removeInnerBlock(group, 'core/paragraph');

    return wp.blocks.serialize(pattern);
};
