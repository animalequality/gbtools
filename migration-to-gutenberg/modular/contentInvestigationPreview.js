import contentGallery from './contentGallery.js';
import {translations, createBlockFromTemplate, addInnerBlocks, setBlockAttribute, removeInnerBlock, getInnerBlock, setColorSchemeOfBlock, getEmbedBlock} from '../utils.js';

export default (modular, colorScheme, lang) => {
    const patternSimple = createBlockFromTemplate('animalequality/pattern-simple');
    const groupSimple = getInnerBlock(patternSimple, 'core/group');
    let patternGallery = '';
    let moreButton = null;

    setColorSchemeOfBlock(groupSimple, colorScheme);

    if (modular.title) {
        setBlockAttribute(getInnerBlock(groupSimple, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(groupSimple, 'core/heading');
    }

    if (modular.content) {
        setBlockAttribute(getInnerBlock(groupSimple, 'core/paragraph'), 'content', modular.content);
    } else {
        removeInnerBlock(groupSimple, 'core/paragraph');
    }

    if (modular['youtube_id']) {
        addInnerBlocks(groupSimple, getEmbedBlock('youtube', modular['youtube_id']));
    }

    if (modular.gallery_link) {
        moreButton = wp.blocks.createBlocksFromInnerBlocksTemplate([['core/buttons', {
            layout: {type: 'flex', justifyContent: 'center'}
        }, [['core/button', {
            url: modular.gallery_link,
            text: translations[lang].more,
            backgroundColor: 'primary'
        }]]]]);
    }

    if (modular.gallery_images && modular.gallery_images.length) {
        patternGallery = contentGallery({title: modular['gallery_title'], items: modular.gallery_images}, colorScheme, lang, 'none', moreButton);
    } else {
        addInnerBlocks(groupSimple, [['core/spacer', {height: '20px'}]]);
        addInnerBlocks(groupSimple, moreButton);
    }

    return wp.blocks.serialize(patternSimple) + patternGallery;
};
