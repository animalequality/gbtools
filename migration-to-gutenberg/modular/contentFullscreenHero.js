import {translations, createBlockFromTemplate, setBlockAttribute, getImageData, getInnerBlock, removeInnerBlock} from '../utils.js';

export default (modular, colorScheme, lang) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-hero-fullscreen');
  const cover = getInnerBlock(pattern, 'core/cover');
  const group = getInnerBlock(cover, 'core/group');
  const buttons = getInnerBlock(group, 'core/buttons');
  let imageData = null;

  if (modular.video_mp4) {
    imageData = getImageData(modular, 'video_mp4');
  } else {
    imageData = getImageData(modular, 'image_background');
  }

  const imageDataPortrait = getImageData(modular, 'image_background_portrait');

  if (imageData && imageData.url && imageData.id) {
    setBlockAttribute(cover, 'url', imageData.url);
    setBlockAttribute(cover, 'id', imageData.id);

    if (modular.video_mp4) {
      setBlockAttribute(cover, 'backgroundType', 'video');
    }
  }

  if (imageDataPortrait && imageDataPortrait.url && imageDataPortrait.id) {
    setBlockAttribute(cover, 'portraitImageUrl', imageDataPortrait.url);
    setBlockAttribute(cover, 'portraitImageId', imageDataPortrait.id);

    if (modular.video_mp4) {
      setBlockAttribute(cover, 'backgroundType', 'video');
    }
  }

  if (modular.youtube_id) {
    setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'url', `https://www.youtube.com/watch?v=${modular.youtube_id}`);
    setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'text', translations[lang].watchVideo);
    setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'backgroundColor', 'white');
  } else if (modular.button) {
    setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'url', modular.button.url);
    setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'text', modular.button.text);
    setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'backgroundColor', 'white');

    if (modular.button.target) {
      setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'linkTarget', '_blank');
    }
  } else {
    removeInnerBlock(group, 'core/buttons');
  }

  setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
  setBlockAttribute(getInnerBlock(group, 'core/heading'), 'level', modular.isH2 ? 2 : 1);

  if (modular.subtitle) {
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', modular.subtitle);
  } else {
    removeInnerBlock(group, 'core/paragraph');
  }

  if (modular.id) {
    setBlockAttribute(cover, 'anchor', modular.id);
  }

  return wp.blocks.serialize(pattern);
};
