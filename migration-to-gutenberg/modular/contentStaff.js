import chunk from 'lodash.chunk';
import {stripHtml} from 'string-strip-html';
import {createBlockFromTemplate, setColorSchemeOfBlock, adjustInnerBlockCount, setBlockAttribute, getImageData, setInnerBlocks, removeInnerBlock, getInnerBlock, addInnerBlocks} from '../utils.js';
import {cloneBlock, rawHandler} from '@wordpress/blocks';

export default (modular, colorScheme) => {
  let pattern;

  if (modular.staff && modular.staff.length) {
    pattern = createBlockFromTemplate('animalequality/pattern-avatars');
    const group = getInnerBlock(pattern, 'core/group');
    const columns = getInnerBlock(group, 'core/columns');

    setColorSchemeOfBlock(group, colorScheme);

    chunk(modular.staff, 4).forEach((chunk, indexChunk) => {
      if (indexChunk > 0) {
        addInnerBlocks(group, [cloneBlock(columns)]);
      }

      const currentColumns = getInnerBlock(group, indexChunk + 2);

      adjustInnerBlockCount(currentColumns, chunk.length);
      setBlockAttribute(currentColumns, 'align', chunk.length === 1 ? 'none' : 'wide');

      chunk.forEach((person, index) => {
        const column = getInnerBlock(currentColumns, index);
        const group = getInnerBlock(column, 'core/group');
        const image = getInnerBlock(group, 0);
        const imageData = getImageData(person, 'image');
        let blockToRemove = 0;

        if (imageData && imageData.url) {
          setBlockAttribute(image, 'url', imageData.url);
          setBlockAttribute(image, 'id', imageData.id);
          blockToRemove++;
        } else {
          removeInnerBlock(group, blockToRemove);
        }

        if (person.name) {
          setBlockAttribute(getInnerBlock(group, blockToRemove), 'content', `<strong>${person.name}</strong>`);
          setBlockAttribute(getInnerBlock(group, blockToRemove), 'align', 'center');
          blockToRemove++;
        } else {
          removeInnerBlock(group, blockToRemove);
        }

        if (person.title) {
          setBlockAttribute(getInnerBlock(group, blockToRemove), 'content', person.title);
          setBlockAttribute(getInnerBlock(group, blockToRemove), 'align', 'center');
          blockToRemove++;
        } else {
          removeInnerBlock(group, blockToRemove);
        }

        if (person.email) {
          setBlockAttribute(getInnerBlock(group, blockToRemove), 'content', `<a href="mailto:${person.email}" data-type="mailto" data-id="mailto:${person.email}">${person.email}</a>`);
          setBlockAttribute(getInnerBlock(group, blockToRemove), 'align', 'center');
          blockToRemove++;
        } else {
          removeInnerBlock(group, blockToRemove);
        }

        if (person.notes) {
          const noteBlocks = rawHandler({HTML: person.notes});
          noteBlocks.forEach(block => setBlockAttribute(block, 'align', 'center'));
          addInnerBlocks(group, noteBlocks);
        }
      });
    });

    if (modular.title) {
      setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', stripHtml(modular.title).result);
    } else {
      removeInnerBlock(group, 'core/heading');
    }

    if (modular.content) {
      setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', stripHtml(modular.content).result);
      setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'align', 'center');
    } else {
      removeInnerBlock(group, 'core/paragraph');
    }
  } else {
    pattern = createBlockFromTemplate('animalequality/pattern-simple');
    const group = getInnerBlock(pattern, 'core/group');

    setColorSchemeOfBlock(group, colorScheme, 'white');

    if (modular.title) {
      setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', stripHtml(modular.title).result);
    } else {
      removeInnerBlock(group, 'core/heading');
    }
    if (modular.content) {
      setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', stripHtml(modular.content, {onlyStripTags: ['p']}).result);
      setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'align', 'center');
    } else {
      removeInnerBlock(group, 'core/paragraph');
    }
  }

  return wp.blocks.serialize(pattern);
};
