import {createBlockFromTemplate, setColorSchemeOfBlock, addInnerBlocks, adjustInnerBlockCount, setInnerBlocks, getInnerBlock, getEmbedBlock, setBlockAttribute, removeInnerBlock} from '../utils.js';
import fetch from 'node-fetch';
import chunk from 'lodash.chunk';
import {cloneBlock} from '@wordpress/blocks';
import migrateContentText from './contentText.js';

const FLICKR_API_KEY = 'b8946ac9ea4a7ad3fb399149ecc9e2b4';
const FLICKR_TAG = 'front';

function makeUrl({apiKey, photosetId, pExtras = [], pPerPage = null}) {
  const extras = ['url_m', 'url_o', 'urls', 'tags', 'path_alias', 'description'].concat(pExtras).filter((v, i, a) => a.indexOf(v) === i);
  const perPage = pPerPage || 100;
  return `https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=${apiKey}&photoset_id=${photosetId}&extras=${extras.join(',')}&per_page=${perPage}&format=json&nojsoncallback=1`;
}

function getImages(tags, set) {
  return set.photo.map(i => i.tags.includes(tags.join(',')) ? i : null).filter(i => i);
}

function is2xx(response) {
  if (response.status < 200 || response.status >= 300) {
    throw new Error(response.statusText);
  }
  return response;
}

async function flickrGallery(elements, tags) {
  const url = elements.url ? elements.url : makeUrl(elements);
  tags = tags && typeof tags === 'string' ? tags.split(',') : tags;
  try {
    const response = await fetch(url);
    is2xx(response);
    const json = await response.json();
    if (json.stat !== 'ok') {
      throw new Error(json.stat);
    }
    const images = getImages(tags, json.photoset);
    console.log(`Found photoset ${elements.photosetId} with ${json.photoset.total} photos. ${images.length} of them tagged "${tags.join(',')}"`);
    /**
         * Needs 'url_b', but flickr.photosets.getPhotos() doesn't provide it
         * Anyway, the photopage that the block needs is predictible, eg:
         * https://www.flickr.com/photos/animalequalityde/34229927031/
         * so there is no need to fetch each and every photo.
         */
    return images.map(e => ({url: `https://www.flickr.com/photos/${e.pathalias}/${e.id}/`, text: e.description._content}));
  } catch (e) {
    console.error(e);
  }
}

export default async(modular, colorScheme) => {
  console.log(`Found gallery modular "${modular.title}"`);
  const pattern = createBlockFromTemplate('animalequality/pattern-cards-media'),
      group = getInnerBlock(pattern, 'core/group'),
      columns = getInnerBlock(group, 'core/columns'),
      images = await flickrGallery({apiKey: FLICKR_API_KEY, photosetId: modular.photoset}, FLICKR_TAG),
      columnsTemplate = cloneBlock(columns);

  setColorSchemeOfBlock(group, colorScheme, 'light');

  if (!images.length) {
    return migrateContentText(modular, colorScheme);
  }

  chunk(images, 2).forEach((chunk, indexChunk) => {
    if (indexChunk > 0) {
      setInnerBlocks(group, group.innerBlocks.concat([columnsTemplate]));
    }

    const currentColumns = getInnerBlock(group, indexChunk + 2);

    adjustInnerBlockCount(currentColumns, chunk.length);
    setBlockAttribute(currentColumns, 'align', 'wide');

    chunk.forEach((image, index) => {
      const column = getInnerBlock(currentColumns, index);
      const columnGroup = getInnerBlock(column, 'core/group');
      const textGroup = getInnerBlock(columnGroup, 'core/group');

      removeInnerBlock(columnGroup, 'core/image');
      removeInnerBlock(textGroup, 0);
      removeInnerBlock(textGroup, 1);
      addInnerBlocks(columnGroup, getEmbedBlock('flickr', null, image.url), true);

      if (image.text) {
        setBlockAttribute(getInnerBlock(textGroup, 'core/paragraph'), 'content', image.text);
      } else {
        removeInnerBlock(columnGroup, 'core/group');
      }
    });
  });

  setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);

  if (modular.content) {
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', modular.content);
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'align', 'left');
  } else {
    removeInnerBlock(group, 'core/paragraph');
  }

  return wp.blocks.serialize(pattern);
};
