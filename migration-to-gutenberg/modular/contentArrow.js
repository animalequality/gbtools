export default (modular, colorScheme) => {
    let color = '#ffffff';

    if (modular.color === 'light') {
        color = '#ececec';
    } else if (modular.color === 'dark') {
        color = '#3f3f3f';
    }

    return wp.blocks.serialize(wp.blocks.createBlocksFromInnerBlocksTemplate([['animalequality/arrow', {colorArrow: color}]]));
};
