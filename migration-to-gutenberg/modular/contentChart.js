import {createBlockFromTemplate, setInnerBlocks, getInnerBlock, setColorSchemeOfBlock} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-simple');
    const group = getInnerBlock(pattern, 'core/group');

    setColorSchemeOfBlock(group, colorScheme, 'white');

    setInnerBlocks(group, [['oik-sb/chart', {
        'type': modular.type,
        'content': modular.chartContent.replace(/\|/g, '\n'),
        'myChartId': `chart-${Math.ceil(Math.random() * 1000)}`
    }]]);

    return wp.blocks.serialize(pattern);
};
