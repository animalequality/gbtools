import {createBlockFromTemplate, removeInnerBlock, getImageData, getInnerBlock, setBlockAttribute} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-hero');
    const cover = getInnerBlock(pattern, 'core/cover');
    const group = getInnerBlock(cover, 'core/group');
    const buttons = getInnerBlock(group, 'core/buttons');
    const imageData = getImageData(modular, 'background_image');

    setBlockAttribute(cover, 'url', imageData.url);
    setBlockAttribute(cover, 'id', imageData.id);

    if (modular.title) {
        setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', modular.title);
    } else {
        removeInnerBlock(group, 'core/heading');
    }

    if (modular.text) {
        setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'content', modular.text);
    } else {
        removeInnerBlock(group, 'core/paragraph');
    }

    if (modular.button && modular.button.label && modular.button.url) {
        setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'text', modular.button.label);
        setBlockAttribute(getInnerBlock(buttons, 'core/button'), 'url', modular.button.url);
    } else {
        removeInnerBlock(group, 'core/buttons');
    }

    setBlockAttribute(cover, 'contentPosition', 'center left');
    setBlockAttribute(getInnerBlock(group, 'core/heading'), 'textAlign', 'left');
    setBlockAttribute(getInnerBlock(group, 'core/paragraph'), 'align', 'left');
    setBlockAttribute(getInnerBlock(group, 'core/buttons'), 'layout', {type: 'flex', justifyContent: 'left'});

    return wp.blocks.serialize(pattern);
};
