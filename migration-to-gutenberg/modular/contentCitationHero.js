import {createBlockFromTemplate, setBlockAttribute, getImageData, setInnerBlocks, getInnerBlock} from '../utils.js';

export default (modular, colorScheme) => {
    const pattern = createBlockFromTemplate('animalequality/pattern-hero-quote');
    const cover = getInnerBlock(pattern, 'core/cover');
    const quote = getInnerBlock(cover, 'core/quote');
    const imageData = getImageData(modular, 'background_image');

    setBlockAttribute(cover, 'url', imageData.url);
    setBlockAttribute(cover, 'id', imageData.id);
    setBlockAttribute(cover, 'contentPosition', 'center left');
    setInnerBlocks(quote, [['core/paragraph', {content: modular.quote}]]);
    setBlockAttribute(quote, 'citation', `${modular.author_name}<br>${modular.author_profession}`);
    
    if (modular.alignment === 'right') {
        setBlockAttribute(cover, 'contentPosition', 'center right');
    }

    return wp.blocks.serialize(pattern);
};
