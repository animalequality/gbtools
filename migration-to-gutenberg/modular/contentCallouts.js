import {createBlockFromTemplate, adjustInnerBlockCount, getInnerBlock, setBlockAttribute, getImageData} from '../utils.js';

export default (modular, colorScheme) => {
  const pattern = createBlockFromTemplate('animalequality/pattern-callouts');
  const columns = getInnerBlock(pattern, 'core/columns');

  if (!modular.callouts || !modular.callouts.length) {
    return '';
  }

  adjustInnerBlockCount(columns, modular.callouts.length);
  setBlockAttribute(columns, 'align', modular.callouts.length === 1 ? 'none' : 'wide');

  if (modular.id) {
    setBlockAttribute(columns, 'anchor', modular.id);
  }

  modular.callouts.forEach((callout, index) => {
    const column = getInnerBlock(columns, index);
    const cover = getInnerBlock(column, 'core/cover');
    const heading = getInnerBlock(cover, 'core/paragraph');
    const imageData = getImageData(callout, 'image');

    setBlockAttribute(heading, 'content', `<a href="${callout.link}">${callout.title}</a>`);

    if (imageData && imageData.url) {
      setBlockAttribute(cover, 'url', imageData.url);
      setBlockAttribute(cover, 'id', imageData.id);
    }
  });

  return wp.blocks.serialize(pattern);
};
