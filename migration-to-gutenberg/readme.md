Scripts to migrate content from ACF and Grav to Gutenberg.

## Scripts

### From Grav to Wordpress; `grav2wp.php`
* Insert Grav medias into Wordpress (db and filesystem). Create a modified copy of the inside `migration/grav/data/` containing medias' WordPress ID/URL:
* `wp @aede --require=grav2wp.php grav2wp migrate /path/to/grav/pages --dry-run --outdir=/tmp/out

### preprocess wordpress
* Translate ACF to MD files
* `wp package install https://gitlab.com/animalequality/wp-acf-to-md.git`
* `wp @de acf-to-md`

### create content
* Take preprocessed MD files and generates Gutenberg markup stored in txt files and the corresponding `wp-cli` commands to insert these as posts.
* `--country` serves as the wp-cli alias and indicates translations' language

#### For Grav:
* Take `md` files from `migration/grav/data/` and create importable posts inside `migration/grav/posts`:
* `node index.js --system=grav --country=de [--execute] [--snapshot] path/to/modularDir /or/path/to/modular.md /or/path/nested/list/of/modularDir`


#### For WordPress:
* Take `md` files from `DIRECTORY` (eg `migration/wordpress/data/`) and create importable posts inside `migration/wordpress/posts`:
* `node index.js --system=wordpress --country=de [--execute] [--snapshot] migration/wordpress/data/`
* Note: pages are sort and compared (locale + numeric) before processing, ensuring that `cmds.txt` sorted too

### Additional parameters examples:
- `--lv=https://loveveg.de --donate=https://animalequality.de/spenden`
* `--alias=<alias` Overrides the wp-cli @alias (instead of using `ae{country}`)
* `for i in ae{us,in}; do node index.js --system=wordpress --country=$i --alias=prod-$i --dest ae$i.out ae$i.in/; done`
* Run example: `SLUG=igualdadanimalorg; sed -e "1iexport WP_CLI_BINARY=\"sudo -u $SLUG --preserve-env=WP_CLI_RUNTIME_ALIAS wp\"" -e 's! wp ! /wp-cli/bin/wp !' aees.out/cmds.txt`

## provide percy snapshots
1. set percy key: `export PERCY_TOKEN=key_of_project`
2. run without execute and with snapshot and original DB to create baseline: `node index.js --system=wordpress --country=de --snapshot migration/grav/wordpress/`
3. run with execute and snapshot to create comparison: `node index.js --system=wordpress --country=de --snapshot --execute migration/grav/wordpress/`

### Hints about importing back into WP:
Assuming a `~/.wp-cli/config.yml` like
```yaml
@docker:
  ssh: docker:animal-equality-website-feature-acf-to-gb-temporary-aeus-phpfpm-1/var/www
```
- and [wp-cli is patched](https://github.com/wp-cli/wp-cli/pull/5974)
- and `WP_CLI_DOCKER_NO_TTY=1` are needed
- then using `DOCKER_HOST=ssh://preprod` allow an import to be made directly into the (remote) docker container.


## Generate a map of assets redirections
`PREFIX=https://foo.bar/ wp @aede --require=grav2wp.php grav2wp map`

would generate listing like:
```
https://foo.bar/live-animal-markets/_gallery-for-markets/C0006.00_36_40_24.Imagen_fija048.jpg	https://animalequality.in/app/uploads/2020/03/C0006.00_36_40_24.Imagen_fija048-6.jpg
https://foo.bar/live-animal-markets/_content_citation/Ian_Linkin_copy.jpg	https://animalequality.in/app/uploads/2020/03/Ian_Linkin_copy-6.jpg
https://foo.bar/live-animal-markets/_content_citation/Andrew_Cunningham.jpg	https://animalequality.in/app/uploads/2020/03/Andrew_Cunningham-6.jpg
````

## Associate media library items with their parent page
`wp @aede --require=grav2wp.php grav2wp link --dry-run`

would output the association made between an attachment ID and a new WP campaign page ID, like:
```
buffalo_killing-7 set parent to 27256
15904124936_2de139b1a8_w-7 set parent to 27256
shutterstock_1616153245-23 set parent to 27256
web-4 set parent to 27256
thankyou-popup-image-2x-5 set parent to 27259
trampling-5 set parent to 27259
```
