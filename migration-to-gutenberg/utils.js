import childProcess from 'node:child_process';
import path from 'path';
import m2j from 'markdown-to-json';
import showdown from 'showdown';
import {executablePath} from 'puppeteer';
import puppeteer from 'puppeteer-core';
import percySnapshot from '@percy/puppeteer';

const translations = {
  de_DE: {
    watchVideo: 'Video ansehen',
    more: 'Mehr'
  },
  en_GB: {
    watchVideo: 'Watch the video',
    more: 'More'
  },
  en_US: {
    watchVideo: 'Watch the video',
    more: 'More'
  },
  es_ES: {
    watchVideo: 'Ver el video',
    more: 'Ver más'
  },
  es_MX: {
    watchVideo: 'Ver el video',
    more: 'Ver más'
  },
  it_IT: {
    watchVideo: 'Guarda il video',
    more: 'Di più'
  },
  pt_BR: {
    watchVideo: 'Assista ao vídeo',
    more: 'Mais'
  }
};

function addTitlePattern(content, title) {
  const pattern = createBlockFromTemplate('animalequality/pattern-simple');
  const group = getInnerBlock(pattern, 'core/group');

  setBlockAttribute(getInnerBlock(group, 'core/heading'), 'content', title);
  setBlockAttribute(getInnerBlock(group, 'core/heading'), 'level', 1);
  setBlockAttribute(getInnerBlock(group, 'core/heading'), 'textAlign', 'left');
  removeInnerBlock(group, 'core/paragraph');

  return wp.blocks.serialize(pattern) + content;
};

function createBlockFromTemplate(name, template = null) {
  if (!template) {
    template = wp.blocks.getBlockType(name)._template;
  }

  return wp.blocks.createBlock(
    name,
    {},
    wp.blocks.createBlocksFromInnerBlocksTemplate(template)
  );
}

function parseModular(path, key) {
  const parsed = JSON.parse(m2j.parse(
    [path],
    {content: true}
  ))[key];

  const trimmedContent = parsed.content && parsed.content.trim();

  parsed.content = trimmedContent.length ? trimmedContent : null;

  return parsed;
}

function yamlToGravDate(yml) {
  return (yml.date || yml.publish_date || yml.unpublish_date || '').replace(/(\d\d)-(\d\d)-(\d{4}) (\d\d:\d\d)/, '$3-$2-$1 $4:00');
}

function fileGitDate(filePath) {
  return childProcess.execSync(`git -C "${path.dirname(filePath)}" log --follow --format=%%ad --date iso "${filePath}"|sed -n '$s/ +.*//p'`).strip();
}

function getInnerBlockByName(block, name) {
  return block.innerBlocks.filter(innerBlock => innerBlock.name === name)[0];
}

function removeInnerBlockByIndex(block, index) {
  block.innerBlocks.splice(index, 1);
}

function removeInnerBlockByName(block, name) {
  block.innerBlocks.splice(block.innerBlocks.findIndex(innerBlock => innerBlock.name === name), 1);
}

function setBlockAttribute(block, attribute, value) {
  if (block && block.attributes) {
    block.attributes[attribute] = value;
  }
}

function setInnerBlocks(block, template) {
  block.innerBlocks = wp.blocks.createBlocksFromInnerBlocksTemplate(template);
}

function addInnerBlocks(block, template, prepend, replace) {
  const blocks = wp.blocks.createBlocksFromInnerBlocksTemplate(template);

  if (prepend) {
    block.innerBlocks = blocks.concat(block.innerBlocks);
  } else if (replace) {
    const indexOfBlock = block.innerBlocks.findIndex(block => block.clientId === replace.clientId);
    block.innerBlocks = [
      ...block.innerBlocks.slice(0, indexOfBlock),
      ...blocks,
      ...block.innerBlocks.slice(indexOfBlock + 1)
    ];
  } else {
    block.innerBlocks = block.innerBlocks.concat(blocks);
  }
}

function adjustInnerBlockCount(block, innerBlocksCount) {
  const currentCount = block.innerBlocks.length;
  const diff = innerBlocksCount - currentCount;

  block.innerBlocks = block.innerBlocks.slice(0, innerBlocksCount);

  if (diff > 0) {
    setInnerBlocks(block, block.innerBlocks.concat(Array(diff).fill(block.innerBlocks[0])));
  }
}

function setColorSchemeOfBlock(block, colorScheme, defaultScheme) {
  colorScheme = colorScheme || defaultScheme;

  if (colorScheme === 'white') {
    setBlockAttribute(block, 'backgroundColor', '');
  } else if (colorScheme === 'light') {
    setBlockAttribute(block, 'backgroundColor', 'light-grey');
  } else if (colorScheme === 'dark') {
    setBlockAttribute(block, 'backgroundColor', 'dark-grey');
    setBlockAttribute(block, 'textColor', 'white');
  }
}

function convertMarkdownToHtml(markdown) {
  const converter = new showdown.Converter({
    extensions: [{
      type: 'output',
      filter: (text, converter) => {
        text = text.replace(/<\p[^>]*>/ig, '<br /><br />');
        text = text.replace(/<\/p[^>]*>/ig, '');

        if (text.startsWith('<br /><br />')) {
          text = text.substring(12);
        }

        return text;
      }
    }]
  });

  return converter.makeHtml(markdown);
}

function getEmbedBlock(provider, id, url = null) {
  if (provider === 'vimeo') {
    return [['core/embed', {providerNameSlug: 'vimeo', type: 'video', className: 'wp-embed-aspect-16-9 wp-has-aspect-ratio', responsive: true, url: `https://player.vimeo.com/video/${id}`}]];
  } else if (provider === 'youtube') {
    return [['core/embed', {providerNameSlug: 'youtube', type: 'video', className: 'wp-embed-aspect-16-9 wp-has-aspect-ratio', responsive: true, url: `https://www.youtube.com/embed/${id}`}]];
  } else {
    return [['core/embed', {providerNameSlug: provider, responsive: true, url}]];
  }
}

function getImageData(data, key) {
  if (!data || !data[key] || !Object.values(data[key]).length) {
    return null;
  }

  return {
    url: Object.values(data[key])[0].attachmentUrl ? Object.values(data[key])[0].attachmentUrl.trimEnd() : null,
    id: parseInt(Object.values(data[key])[0].attachmentId, 10)
  };
}

function getInnerBlock(block, segments) {
  return innerBlockLoop(
    block,
    segments,
    (block, currentSegment) => block.innerBlocks[currentSegment],
    (block, currentSegment) => getInnerBlockByName(block, currentSegment)
  );
}

function removeInnerBlock(block, segments) {
  return innerBlockLoop(
    block,
    segments,
    (block, currentSegment) => removeInnerBlockByIndex(block, currentSegment),
    (block, currentSegment) => removeInnerBlockByName(block, currentSegment)
  );
}

function innerBlockLoop(block, segments, actionWhenIndex, actionWhenName) {
  const isLastSegment = !Array.isArray(segments) || segments.length === 1;
  const currentSegment = Array.isArray(segments) ? segments[0] : segments;
  const currentSegmentIsIndex = Number.isInteger(currentSegment);

  if (!block || !block.innerBlocks) {
    return null;
  }

  if (isLastSegment) {
    return currentSegmentIsIndex ? actionWhenIndex(block, currentSegment) : actionWhenName(block, currentSegment);
  }

  return getInnerBlock(
    currentSegmentIsIndex ? block.innerBlocks[currentSegment] : getInnerBlockByName(block, currentSegment),
    segments.slice(1)
  );
}

function shortcodes(content, block, alignment = 'right') {
  const shortcodeRegex = /(\[.*?\])/gs;
  const shortcodes = content.split(shortcodeRegex).filter(element => !!element.trim());

  shortcodes.forEach(element => {
    if (element.match(shortcodeRegex)) {
      addInnerBlocks(block, [['core/shortcode', {text: element}]]);
    } else {
      addInnerBlocks(block, [['core/paragraph', {content: element, align: alignment}]]);
    }
  });
}

async function percySnap(url) {
  const domain = new URL(url).hostname;
  const cookies = [
    {domain, name: 'ae_cookie_consent_facebook_pixel', value: 'false'},
    {domain, name: 'ae_cookie_consent_google_ads', value: 'false'},
    {domain, name: 'ae_cookie_consent_google_analytics', value: 'false'},
    {domain, name: 'ae_cookie_consent_google_tag_manager', value: 'false'},
    {domain, name: 'ae_cookie_consent_pardot', value: 'false'},
    {domain, name: 'ae_cookie_consent_vimeo', value: 'false'},
    {domain, name: 'ae_cookie_consent_youtube', value: 'false'}
  ];

  const puppet = await puppeteer.launch({executablePath: executablePath(), ignoreHTTPSErrors: true});
  const page = await puppet.newPage();
  await page.setCookie(...cookies);
  await page.goto(url, {waitUntil: 'networkidle2', timeout: 60000});
  await percySnapshot(page, url, {widths: [360, 1200]});
  puppet.close();
}

export {
  translations,
  addTitlePattern,
  createBlockFromTemplate,
  parseModular,
  yamlToGravDate,
  fileGitDate,
  setBlockAttribute,
  setInnerBlocks,
  addInnerBlocks,
  adjustInnerBlockCount,
  setColorSchemeOfBlock,
  convertMarkdownToHtml,
  getEmbedBlock,
  getImageData,
  getInnerBlock,
  removeInnerBlock,
  shortcodes,
  percySnap
};
