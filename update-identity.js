import { dryRun, epilog, inhibitParseWarnings } from './console-utils.js';

function action(markup) {
    let blocks = wp.blocks.parse(markup, { __unstableSkipMigrationLogs: true }),
        modified = wp.blocks.serialize(blocks) !== markup.trimEnd();

    return [blocks, modified];
}

async function apply(provider, options) {
    // start boiler-plate data-provider iterator-code
    const it = provider.it();
    while (true) {
        const { done, value } = await it.next();
        if (done) {
            break;
        } else if (done === undefined) {
            throw Error('Iterator error. done is undefined');
        }

        const { source, content } = value;
        // end boiler-plate data-provider iterator-code

        inhibitParseWarnings(1);
        const [blocks, modified] = action(content),
            warning_output = inhibitParseWarnings(0);

        if (!modified) {
            continue;
        }

        dryRun(source, content, blocks, options);
        if (!options.write) {
            console.log(warning_output);
        }

        await provider.write(source, blocks);
    }
    epilog();
}

export default apply;
