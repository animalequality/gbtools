# Converting ACF to Gutenberg.

## Intro
This set of PHP and JS scripts helps converting huge ACF pages to Gutenberg blocks.

- `acf-to-timeline-json.php` iterates over ACF converts it to a given block structure.
- `index.js` loads the block's javascript, iterates over the JSON blocks attributes and render the corresponding markup (html + metadata).

This script is addressing a specific ACF configuration and output attributes specific to the coolplugin timeline-block.

## Usage:
- `$ wp eval-file acf-to-timeline-json.php > data.json`
- Alternatively, on a remove host, `ssh ... wp eval-file - < acf-to-timeline-json.php > data.json` can also be used
- `$ WP_BASE_DIR=$HOME/ae/w/ae.me node index.js < data.json > data.html`
- Then copy/paste the markup in the Gutenberg (in code-editor mode).

## Known issues:
- Blocks built using `parcel` watch-mode have HMR (Hot Module Reload) code which may fail under node.js
- `self is undefined` : The webpack bundle of the Gutenberg block should be change to use output.globalObject = 'this'
- `Error: Automatic publicPath is not supported in this browser` : `defaultConfig.output.publicPath = '';` in the webpack config.
- Sample custom webpack config: `includes/cool-timeline-block/webpack.config.js`:
```js
let parse = await import('@wordpress/scripts/config/webpack.config.js'), defaultConfig = parse.default;
if (process.env.TARGET == 'node') {
    defaultConfig.output.globalObject = 'global';
    defaultConfig.output.publicPath = '';
}
export default defaultConfig;
```
- Building cool-timeline supposes `CXXFLAGS="--std=c++14" npm install --force` and `TARGET=node NODE_ENV=development npm run build`
- `Cannot unlock an object that was not locked before.` due to redundant copies of `@wordpress/private-apis`, solved by `node_modules/@wordpress/*/node_modules`

- `TypeError: _blockEditor.__experimentalLinkControl.DEFAULT_LINK_SETTINGS is not iterable` (https://github.com/WordPress/gutenberg/issues/68695)
```
sed -i '/^const LINK_SETTINGS/s!.*!const LINK_SETTINGS = [...(_blockEditor.__experimentalLinkControl.DEFAULT_LINK_SETTINGS || []), {!' node_modules/@wordpress/block-library/build/button/edit.js
```
