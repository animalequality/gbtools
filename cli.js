import process from 'process';
import {Argument, Command, Option} from 'commander';
import {loadModule, setUpLocale} from './utils.js';
import {setVerbosity} from './console-utils.js';
import {dataProvider, preCheck} from './providers.js';

import updateBlockAttribute from './update-block-attribute.js';
import youtubeToVimeo from './update-youtube-to-vimeo.js';
import updateIdentity from './update-identity.js';
import updateClassicBlock from './update-classic-block.js';
import updateEmptyHero from './update-empty-hero.js';
import migrateCoreHtml from './migrate-core-html.js';

let visualTesting = false;
try {
  visualTesting = await loadModule('./visual-testing.js');
} catch (e) {
  visualTesting = false;
}

function increaseVerbosity(dummyValue, previous) {
  return setVerbosity((previous || 0) + 1);
}

const optv = new Option(
      '-v, --verbose',
      'Increase verbosity (once for info, twice for debug)'
    ).argParser(increaseVerbosity),
    optw = new Option(
      '-w, --write',
      'If local files are specified, these are edited inline'
    ),
    optW = new Option(
      '-W, --force-write',
      'Force writing even if reserialization involves markup mutations'
    ).implies({write: true}),
    opt_check = new Option(
      '--no-check',
      'Inhibit safety checking (eg, like Gutenberg version consistency part of the MySQL provider)'
    ),
    opt_from = new Option('--from <uri>', 'Input (data provider)'),
    opt_to = new Option('--to <uri>', 'Output (write-mode)'),
    opt_mysqlwhere = new Option('--where <clause>', 'MySQL --where clause'),
    opt_mysqllim = new Option('--limit <num>', 'MySQL --limit clause'),
    // Default <sources> argument
    argsource = new Argument(
      '<sources...>',
      'markup files or post-ID. Could also be "*" or - depending on the data-provider'
    );

// Command-specific options (not enabled for all the commands)
const optl = new Option(
      '-l, --locale <string>',
      'locale that should be used when generating blocks',
      'auto'
    ),
    opts = new Option(
      '-s, --no-identity-check',
      'Skip prealable markup identity check'
    );

const program = new Command();

// Global options, inherited by all commands, mainly for data provider setup
// and common output (verbosity/debugging)
program
  .addOption(opt_mysqlwhere)
  .addOption(opt_mysqllim)
  .addOption(opt_from)
  .addOption(opt_to)
  .addOption(optv)
  .addOption(optw)
  .addOption(optW)
  .addOption(opt_check)
  .addHelpText(
    'after',
    `

The --from/--to options defines markup reading/post update modes aka "data provider" (fs, mysql, wpcli, api) which can be mixed.
If --from is omitted, <sources> are considered regular files to be updated in-place (if --write was set)
(stdin is used if a <source> is "-")
If --from is set to a valid  MySQL DB, <sources> can be set to "." or "-" in order to proceed with all posts ID

Examples:
* \`--from mysql;//user:pass@hostname/db\`: sources are post ID, MySQL SELECT() is used to retrieve the content.
* \`--from mysql;//db\`: can be used as a shortcut (.my.cnf is used)
* \`--from 'wp-cli://wp @prod-ae post get --field=post_content %d'\` : sources are post ID. wp-cli is used to retrieve the content.

* \`--to 'wp-cli://wp @prod-ae post update %d'\` : wp-cli is used to UPDATE the content. (New markup is passed as stdin of the command).

The following environment variable can be set with --from mysql:
* MySQL host (MYSQL_HOST)
* MySQL port (MYSQL_PORT)
* MySQL user (MYSQL_USER)
* MySQL database (MYSQL_DATABASE)
* MySQL password (MYSQL_PASS)
Eg, for -prod:
\`$ . <(ansible -m debug -a "var=websites[ae_slug].mysql" aeXXXX|sed '1d;2s/.*{/{/;$d'|jq -r '"export MYSQL_DATABASE=" + .db + " MYSQL_USER=" + .user + " MYSQL_PASS=\"" + .password + "\""')\`

Initial posts can be created using a \`--to /dev/null -s .\` run first and looking at impacted posts, then exporting these \`$ for i in posts-ids; do mysql -N DB -sre "SELECT post_content FROM wp_posts WHERE id = $i" >| data/DB/$i.txt; done\`. Where git-versioned \`data/\` directory can serves as a test environment for output transformation using \` --to data/DB/{source}.txt\` in later runs.


*WARNING* Operating this script on a remote -production database using mysql provider is VERY DANGEROUS: No post revision is being created.

ssh-tunnel and port-binding reminder:
$ ssh -N -L 3333:localhost:3306 root@aeus
(see also https://www.npmjs.com/package/tunnel-ssh ?)

Examples:
$ index.js -v vimeize --map yt-mapping3.json --from mysql://aeus --where "post_type != 'oembed_cache'" --to '{db}/{source}.txt' -s  .
# Apply the transform on a local MySQL database, to every non-oembed post and show which posts would be modified (no -w option passed)

$ index.js -v vimeize --map yt-mapping3.json --from mysql://localhost:3333/prod --to 'data/{db}/{source}.txt' -W 18573
# Same as above, but only for one specific post extracted from another DB (from a ssh-tunnel) and write the markup of the modified post to a local directory named after the database.

$ index.js -v vimeize --map yt-mapping3.json --from mysql://localhost:3333/prod --to "wp-cli://wp @prod-us post update %d -" -W 19493
# Same as above but use a wp-cli command to update the post.

`
  );

program.command('update-attribute')
  .description("Updates (or set) specific block's type attribute.")
  .addOption(optl).addOption(opts)
  .argument('<blockName>', 'block type to update')
  .argument('<attributeName>', 'attribute to update')
  .argument('<attributeValue>', 'value to set')
  .addArgument(argsource)
  .option(
    '-p, --parent <string>',
    'only update if the block has a parent of this type'
  )
  .option('-j, --json', 'interprete the value as a JSON')
  .action(
    async function(
      blockName,
      attributeName,
      attributeValue,
      sources,
      options
    ) {
      options = this.optsWithGlobals();
      const {readProvider, writeProvider} = dataProvider(sources, options);
      if (options.check) {
        await preCheck(readProvider, writeProvider);
      }
      await setUpLocale(readProvider, options.locale, sources, options);
      await updateBlockAttribute(
        readProvider,
        blockName,
        attributeName,
        attributeValue,
        options
      );
      process.exit();
    }
  );

program.command('vimeize')
  .addOption(optl).addOption(opts)
  .description(
    'Transform Youtube related blocks to Vimeo using a mapping of YT/Vimeo identifiers'
  )
  .option(
    '--map <file>',
    'A JSON object mapping Youtube ID keys with Vimeo ID values'
  )
  .addArgument(argsource)
  .action(async function(sources, options) {
    options = this.optsWithGlobals();
    const {readProvider, writeProvider} = dataProvider(sources, options);
    if (options.check) {
      await preCheck(readProvider, writeProvider);
    }
    await setUpLocale(readProvider, options.locale, sources, options);
    await youtubeToVimeo(readProvider, options);
    process.exit();
  });

program.command('update-identity')
  .description(
    'parse/reserialize Gutenberg post content without other modification'
  )
  .addOption(optl)
  .addArgument(argsource)
  .action(async function(sources, options) {
    options = this.optsWithGlobals();
    const {readProvider, writeProvider} = dataProvider(sources, options);
    if (options.check) {
      await preCheck(readProvider, writeProvider);
    }
    await setUpLocale(readProvider, options.locale, sources, options);
    await updateIdentity(readProvider, options);
    process.exit();
  });

program.command('update-empty-hero')
  .description(
    'Removes the group from the heros cover block if the group is empty to avoid rendering a blank container'
  )
  .addOption(opts)
  .addArgument(argsource)
  .action(async function(sources, options) {
    options = this.optsWithGlobals();
    const {readProvider, writeProvider} = dataProvider(sources, options);
    if (options.check) {
      await preCheck(readProvider, writeProvider);
    }
    await updateEmptyHero(readProvider, options);
    process.exit();
  });

program.command('update-classic-block')
  .description(
    'Converts the content of a classic block into separate core gutenberg blocks'
  )
  .addArgument(argsource)
  .action(async(sources, options) => {
    options = this.optsWithGlobals();
    const {readProvider, writeProvider} = dataProvider(sources, options);
    if (options.check) {
      await preCheck(readProvider, writeProvider);
    }
    await updateClassicBlock(readProvider, options);
    process.exit();
  });

program.command('migrate-core-html')
  .description('Migrates the core/html block')
  .addArgument(argsource)
  .action(async function(sources, options) {
    options = this.optsWithGlobals();
    const {readProvider, writeProvider} = dataProvider(sources, options);
    if (options.check) {
      await preCheck(readProvider, writeProvider);
    }
    await migrateCoreHtml(readProvider, options);
    process.exit();
  });

if (visualTesting) {
  program.command('visual-testing')
    .description(
      'Creates a post with all custom blocks and runs a visual comparison on it'
    )
    .action(() => {
      visualTesting();
    });
}

export default program;
