import { identityPreCheck } from './utils.js';
import { dryRun, epilog } from './console-utils.js';

let modified = null;

function updateAttributeRecursively(
    blocks,
    blockName,
    attributeName,
    value,
    parent = null,
    hasParent = false,
    attributeValueIsJson = false,
) {
    const valueToUse = attributeValueIsJson ? JSON.parse(value) : value;

    blocks.forEach((block) => {
        if (block.name === blockName && (!parent || hasParent)) {
            block.attributes
                ? block.attributes[attributeName] = valueToUse
                : block.attributes = { attributeName: valueToUse };
            modified = true;
        }

        if (block.innerBlocks) {
            updateAttributeRecursively(
                block.innerBlocks,
                blockName,
                attributeName,
                value,
                parent,
                block.name === parent,
                attributeValueIsJson,
            );
        }
    });
}

function action(
    source,
    blockName,
    attributeName,
    value,
    { parent, jsonValue },
) {
    const blocks = wp.blocks.parse(source, { __unstableSkipMigrationLogs: true });
    if (!blocks || !blocks.length) {
        throw Error('No blocks found in source');
    }

    modified = false;
    updateAttributeRecursively(
        blocks,
        blockName,
        attributeName,
        value,
        parent,
        false,
        jsonValue,
    );
    return [blocks, modified];
}

async function apply(provider, blockName, attributeName, newvalue, options) {
    // start boiler-plate data-provider iterator-code
    const it = provider.it();

    while (true) {
        const { done, value } = await it.next();
        if (done) {
            break;
        } else if (done === undefined) {
            throw Error('Itarator error. done is undefined');
        }

        const { source, content } = value;
        identityPreCheck(source, content, options);
        // end boiler-plate data-provider iterator-code

        const [blocks, modified] = action(
            content,
            blockName,
            attributeName,
            newvalue,
            options,
        );

        if (!modified) {
            continue;
        }

        if (wp.blocks.serialize(blocks) === content) {
            throw Error(`${source}: modified, but generate markup is identical`);
        }

        dryRun(source, content, blocks, options);
        await provider.write(source, blocks);
    }
    epilog();
}

export default apply;
