<?php

$p = get_post($args[0]);
if (! $p) {
  die("Can't get post {$args[0]}");
}
$content = get_fields($p)['page_content'];
$index = 1;

$full = [];
$new_year_tpl = [
    'animalequality/pattern-timeline',
    [],
    [
        [
            'core/group',
            [
                'templateLock' => false,
                'align' => 'full',
                'layout' => ['inherit' => true]
            ],
            [
                [
                    'core/heading',
                    [
                      'level' => 2,
                      'textAlign' => 'center',
                      'content' => null
                    ],
                    []
                ], [
                    'cp-timeline/content-timeline-block',
                    [
                        'contentAlignment' => 'left',
                        'lock' => ['move' => true, 'remove' => true],
                        'storyBorderColor' => '#ECECEC',
                        'BothsidedOrientation' => 'left',
                        'OrientationCheckBox' => true,
                        'block_id' => null
                    ],
                    []
                ]
            ]
        ]
    ]
];

foreach ($content as $field) {
    if ($field['acf_fc_layout'] != 'timeline') {
        continue;
    }

    $year = $field['timeline_year'];
    $new_year_data = $new_year_tpl + [];

    $new_year_data[2][0][2][0][1]['content'] = $year;

    foreach ($field['timeline_events'] as $e) {
        $time = $e['event_title'];
        $img_id = $e['event_image'];
        $desc = $e['event_text'];
        $title = $alt = '';
        $pos = $index % 2 == 1 ? 'left' : 'right';
        $img_url = wp_get_attachment_image_url($e['event_image'], 'full');
        $uuid = wp_generate_uuid4();

        $json = [
          'time_heading' => $title,
          'time_desc' => $desc,
          'time_image' => [
            'id' => (int)$img_id,
            'url' => $img_url,
            'size' => 'full'
          ],
          'timeLineImage' => $img_url,
          'block_id' => $uuid,
          't_date' => $time,
          'blockPosition' => $pos,
          'timelineDesign' => 'both-sided',
          'imageAlt' => $alt,
        ];

        $attr = [
          'time' => $time,
          'time_desc' => $desc,
          'time_image' => [
            'id' => (int)$img_id,
            'url' => $img_url,
          ],
          'timeLineImage' => $img_url,
          'block_id' => $uuid,
          't_date' => $time,
          'blockPosition' => $pos,
          'timelineDesign' => 'both-sided',
          'imageAlt' => $alt,
        ];

        $new_year_data[2][0][2][1][1]['block_id'] = wp_generate_uuid4();
        $new_year_data[2][0][2][1][2][] = ['cp-timeline/content-timeline-block-child', $json, []];
        $json = json_encode($json);
        $index++;
    }
    $full[] =  $new_year_data;
}

print(json_encode($full));
