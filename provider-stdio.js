import fs from 'fs';

class StdioProvider {
    #sources;

    constructor(sources, write, options) {
        this.type = 'fs';
        this.#sources = sources;
    }

    *it() {
        // ToDo line-by-line ?
        for (const source of this.#sources) {
            yield { source, content: fs.readFileSync('/dev/stdin').toString() };
        }
    }

    write(source, blocks) {
        const output = wp.blocks.serialize(blocks);
        console.log(output);
        return true;
    }

    detectLocale() {
        return null;
    }
}

export { StdioProvider };
