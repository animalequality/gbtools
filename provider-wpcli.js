import process from 'process';
import child_process from 'child_process';
import { computeIdQuery } from './gb-version-utils.js';
import { debug, info, isInfo } from './console-utils.js';

const slugToLocale = {
    de: 'de_DE',
    uk: 'en_GB',
    us: 'en_US',
    in: 'en_GB',
    es: 'es_ES',
    mx: 'es_MX',
    it: 'it_IT',
    br: 'pt_BR',
};

/**
 * Bedrock's dotenv createImmutable()
 * won't consider DB_* variables passed to the `env`
 * wp-cli must be working out-of-the-box.
 *
 * Kind of extension of the CommandProvider
 */
class WpCliProvider {
    #sources;
    #write;
    #readCommand = '{binary} {alias} post get --field=post_content {source}';
    #writeCommand = '{binary} {alias} post update {source} -';
    #binary;
    #alias;
    #reader;

    constructor(sources, write, { alias, reader }) {
        this.type = 'wp-cli';
        this.#sources = sources;
        this.#write = write;
        this.#reader = reader;
        this.#alias = alias.replace(/^@*/, '');
        this.#binary = process.env.WP_BIN || 'wp';
        if (!this.#alias) {
            throw new Error('No alias provided. Usage: wp-cli://@alias');
        }

        this.#alias = '@' + this.#alias;
    }

    computeId() {
        const cmd = [
            this.#binary,
            this.#alias,
            'sql',
            'query',
            computeIdQuery(),
            '--skip-column-names',
        ];
        const result = child_process.spawnSync(cmd[0], cmd.splice(1));
        return result.stdout.toString().trimEnd();
    }

    /**
     * Intended to avoid the following desastrous scenario:
     * --from mysql://localhost:3333/US-DATABASE --to wp-cli://@prod-ES
     * where modified US posts would be saved into the ES database through wp-cli.
     */
    async preCheck() {
        if (this.#reader && this.#reader.type === 'sql') {
            const sql_hash = await this.#reader.computeId(),
                wp_hash = this.computeId(),
                match = wp_hash === sql_hash;
            if (!match || !sql_hash || !wp_hash) {
                console.error(
                    `Read/Write databases version mismatch MySQL="${sql_hash} (${sql_hash.length}), WP-CLI=${wp_hash} (${wp_hash.length})`,
                );
                return false;
            }
        }

        debug('[wp-cli] DB version check passed');
        return true;
    }

    interpolate(str, source) {
        return str.replace('{binary}', this.#binary)
            .replace(/%alias%|\$?\{alias\}|\[alias\]/, this.#alias)
            .replace(/%source%|\$?\{source\}|\[source\]|%d/, source);
    }

    *it() {
        for (let source of this.#sources) {
            const cmd = this.interpolate(this.#readCommand, source).split(' '),
                result = child_process.spawnSync(cmd[0], cmd.splice(1));
            if (result.status != 0) {
                throw Error(result.stderr.toString());
            }
            yield { source: source, content: result.stdout.toString() };
        }
    }

    async write(source, blocks) {
        const markup = wp.blocks.serialize(blocks);
        let cmd = this.interpolate(this.#writeCommand, source);

        if (!cmd.includes(source)) {
            throw Error(
                `"${cmd}" doesn't seem to include "${source}" after placeholder replacement. Likely an error.`,
            );
        }

        if (!this.#write) {
            info(`[dry-run] wp-cli write command: ${cmd}`);
            return null;
        }

        info(`[wp-cli] Running ${cmd}`);
        cmd = cmd.split(' ');
        const result = child_process.spawnSync(cmd[0], cmd.splice(1), {
            input: markup,
        });
        if (result.status != 0) {
            throw result.error ? result.error : Error(result.stderr?.toString());
        }
        if (isInfo()) {
            info(`[wp-cli] post ${source} modified`);
            if (result.stdout) {
                debug('cmd stdout: ' + result.stdout.toString());
            }
        }

        return result.status === 0;
    }

    /**
     * Detect locale based on database's name.
     */
    detectLocale() {
        if (this.#alias.match(/(us|in|uk|mx|es|de|br|it)$/)) {
            let suffix = this.#alias.slice(-2);
            return slugToLocale[suffix];
        }
        return null;
    }
}

export { WpCliProvider };
