import {createBlockFromTemplate, adjustInnerBlockCount, removeInnerBlock, getInnerBlock, setBlockAttribute} from './migration-to-gutenberg/utils.js';
import process from "node:process";

function convertCoolTimelinePatternToPatternTimeline(blocks) {
  const resultBlocks = [];

  blocks.forEach(block => {
    if (block.name !== 'animalequality/pattern-timeline') {
        resultBlocks.push(block);
        return;
    }

    const pattern = createBlockFromTemplate('animalequality/pattern-timeline', wp.blocks.getBlockType(block.name)._template);
    const group = getInnerBlock(pattern, 'core/group');
    const heading = getInnerBlock(group, 'core/heading');
    const groupInner = getInnerBlock(group, ['core/group', 'core/group']);
    const coolTimeline = getInnerBlock(block, ['core/group', 'core/group', 'cp-timeline/content-timeline-block']);

    if (!coolTimeline.innerBlocks || !coolTimeline.innerBlocks.length) {
      return;
    }

    adjustInnerBlockCount(groupInner, coolTimeline.innerBlocks.length);

    if (getInnerBlock(block, ['core/group', 'core/heading'])) {
      setBlockAttribute(heading, 'content', getInnerBlock(block, ['core/group', 'core/heading']).attributes.content);
    } else {
      removeInnerBlock(group, 'core/heading');
    }

    coolTimeline.innerBlocks.forEach((event, index) => {
      const mediaText = getInnerBlock(groupInner, index)
      const group = getInnerBlock(mediaText, 'core/group');
      const year = getInnerBlock(group, 0);
      const text = getInnerBlock(group, 1);

      setBlockAttribute(mediaText, 'mediaId', event.attributes.time_image.id);
      setBlockAttribute(mediaText, 'mediaUrl', event.attributes.time_image.url);
      setBlockAttribute(year, 'content', `<strong>${event.attributes.t_date}</strong>`);
      setBlockAttribute(text, 'content', event.attributes.time_desc);
    });

    resultBlocks.push(pattern);
  });

  return resultBlocks;
}

export default async function convertCoolTimeline() {
  registerCoreBlocks();
  const wpr = {base: 'https://mx.animalequality.me', user: process.env.WP_USER, pass: process.env.WP_PASS};
  const content = await getRawContent(wpr, 'pages',  39244);
  const blocks = wp.blocks.parse(content);
  const convertedBlocks = convertCoolTimelinePatternToPatternTimeline(blocks);
  const convertedContent = wp.blocks.serialize(convertedBlocks);

  fs.writeFileSync('/tmp/test.txt', convertedContent, {}, () => {});

  return convertedContent;
}
