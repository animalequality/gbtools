import { identityPreCheck } from './utils.js';
import { dryRun, epilog, silentParse } from './console-utils.js';

function action(markup) {
    const blocks = silentParse(markup);
    let replacements = 0;

    for (const block of blocks) {
        if (block.name !== 'animalequality/pattern-hero') {
            continue;
        }

        if (!block.innerBlocks?.length) {
            console.log('Hero has no cover');
            continue;
        }

        if (!block.innerBlocks[0].innerBlocks?.length) {
            console.log('Cover has no group');
            continue;
        }

        if (!block.innerBlocks[0].innerBlocks[0].innerBlocks?.length) {
            replacements++;
            block.innerBlocks[0].innerBlocks = [];
        }
    }

    return [blocks, replacements];
}

async function apply(provider, options) {
    // start boiler-plate data-provider iterator-code
    const it = provider.it();

    while (true) {
        const { done, value } = await it.next();
        if (done) {
            break;
        } else if (done === undefined) {
            throw Error('Itarator error. done is undefined');
        }

        const { source, content } = value;
        identityPreCheck(source, content, options);
        // end boiler-plate data-provider iterator-code

        const [blocks, replacements] = action(content);

        if (!replacements) {
            continue;
        }

        dryRun(source, content, blocks, options);
        console.log(`${source} : ${replacements} replacements to be made`);
        await provider.write(source, blocks);
    }
    epilog();
}

export default apply;
