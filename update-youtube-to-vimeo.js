import { replace, RichTextData } from '@wordpress/rich-text';
import fs from 'fs';
import { identityPreCheck, silentParse, walkBlocks } from './utils.js';
import { debug, dryRun, epilog } from './console-utils.js';

function action(id, markup, mapping) {
    const [blocks, warnings] = silentParse(markup, {
            __unstableSkipMigrationLogs: true,
        }),
        iframe_re = new RegExp(
            `iframe src.*(?:youtube\.com|youtu\.be).*(${Object.keys(mapping).join('|')})`,
        ),
        url_re = new RegExp(
            `(?:youtube\.com|youtu\.be).*(${Object.keys(mapping).join('|')})`,
        );
    console.warn(`warning: parse() validation errors for ${id}`);
    debug(warnings);

    let replacements = 0;
    for (const [block, parents] of walkBlocks(blocks)) {
        if (
            block.name == 'core/embed' &&
            (
                !block.attributes.providerNameSlug ||
                [
                    'youtube',
                    'embed-handler',
                    'gestor-del-servicio',
                    'incorporar-manipulador',
                ].includes(block.attributes.providerNameSlug)
            ) &&
            (block.attributes.url || '').match(url_re)
        ) {
            const url = new URL(block.attributes.url);
            const ytid = (url.searchParams.get('v') || url.pathname.replace(/^.*\//, ''))
                    .replace(/\?.*/, ''),
                reg = new RegExp(
                    `https://(www\.)?(youtube\.com|youtu\.be)/(watch\\?v=)?${ytid}[^<"$]*`,
                    'ig',
                );
            let repl = mapping[ytid];
            // console.log(`embed block id ${ytid}`);
            if (!repl) {
                console.warn(
                    `${block.name} warning: no replacement for "${ytid}"`,
                    block,
                );
                continue;
            }

            if (!isNaN(repl)) repl = 'https://vimeo.com/' + repl;
            debug(`replacing ${block.name} ${block.attributes.url} by ${repl}`);
            // For compatibility
            if (block?.__unstableBlockSource?.blockName === 'core-embed/youtube') {
                Object.assign(
                    block,
                    wp.blocks.createBlock(
                        'core/embed',
                        {
                            url: repl,
                            type: 'video',
                            providerNameSlug: 'vimeo',
                            caption: block.attributes.caption.replace(reg, repl),
                            allowResponsive: block.attributes.allowResponsive,
                            responsive: block.attributes.responsive,
                            previewable: block.attributes.previewable,
                            className: block.attributes.className,
                        },
                    ),
                );
            } else { // main case
                block.attributes.url = repl;
                block.attributes.providerNameSlug = 'vimeo';
            }
            replacements++;
        } else if (
            block.name == 'core/button' && (block.attributes.url || '').match(url_re)
        ) {
            const url = new URL(block.attributes.url),
                ytid = url.searchParams.get('v') || url.pathname.replace(/^\//, '');
            let repl = mapping[ytid];
            // console.log(`embed block id ${ytid}`);
            if (!repl) {
                console.warn(
                    `${block.name} warning: no replacement for "${ytid}"`,
                    block,
                );
                continue;
            }

            if (!isNaN(repl)) repl = 'https://vimeo.com/' + repl;
            debug(`replacing ${block.name} ${block.attributes.url} by ${repl}`);
            block.attributes.url = repl;
            block.attributes.providerNameSlug = 'vimeo';
            replacements++;
        } else if (
            ['core/image', 'core/media-text'].includes(block.name) &&
            (block.attributes.href || '').match(url_re)
        ) {
            const url = new URL(block.attributes.href),
                ytid = url.searchParams.get('v') || url.pathname.replace(/^\//, ''),
                reg = new RegExp(
                    `https://(www\.)?(youtube\.com|youtu\.be)/(watch\\?v=)?${ytid}[^<"$]*`,
                    'ig',
                );
            let repl = mapping[ytid],
                parent = parents.slice(-1)[0];

            if (!repl) {
                console.warn(block);
                continue;
            }

            if (!isNaN(repl)) repl = 'https://vimeo.com/' + repl;
            debug(`replacing ${block.name} ${block.attributes.url} by ${repl}`);
            if (block.name === 'core/media-text') {
                block.attributes.href = repl;
                replacements++;
                continue;
            }

            // core/image
            if (parent.name === 'core/gallery') {
                // core/gallery aren't assumed to contain video. Don't transform these.
                console.warn(
                    'Following block was within a core/gallery, not changing to a core/embed',
                    block,
                );
                block.attributes.href = repl;
            } else {
                Object.assign(
                    block,
                    wp.blocks.createBlock(
                        'core/embed',
                        {
                            url: repl,
                            caption: block.attributes.caption.replace(reg, repl),
                            type: 'video',
                            providerNameSlug: 'vimeo',
                            allowResponsive: true,
                            responsive: true,
                            previewable: true,
                            className: 'wp-embed-aspect-16-9 wp-has-aspect-ratio',
                        },
                    ),
                );
            }

            replacements++;
        } else if (
            block.name == 'core/html' && block.attributes.content.match(iframe_re)
        ) {
            // block.attributes.content.replace(/<iframe src.*?<\/iframe>/, "");
            const ytid = block.attributes.content.replace(
                /.*youtube\.com\/embed\/([a-z0-9_-]{11}).*/i,
                '$1',
            );
            let repl = mapping[ytid];
            if (!repl) {
                console.warn(block);
                continue;
            }

            if (!isNaN(repl)) repl = 'https://vimeo.com/' + repl;
            debug(`replacing ${block.name} iframe(${ytid}) by ${repl}`);
            Object.assign(
                block,
                wp.blocks.createBlock(
                    'core/embed',
                    {
                        url: repl,
                        caption: block.attributes.caption,
                        type: 'video',
                        providerNameSlug: 'vimeo',
                        allowResponsive: true,
                        responsive: true,
                        previewable: true,
                        className: block.attributes.className,
                    },
                ),
            );
            replacements++;
        } else if (
            ['core/list-item', 'core/heading', 'core/paragraph'].includes(
                block.name,
            ) && block.attributes.content.match(url_re)
        ) {
            const re = new RegExp(
                    '(?:youtube\.com|youtu\.be)\/watch\\?v=([a-z0-9_-]{11})',
                    'gmis',
                ),
                ytids = block.attributes.content.matchAll(re);
            var block_shown = 0;
            for (let _y of ytids) {
                const ytid = _y[1],
                    reg = new RegExp(
                        `https://(www\.)?(youtube\.com|youtu\.be)/(watch\\?v=)?${ytid}[^<"$]*`,
                        'ig',
                    );

                if (ytid.length != 11) {
                    ytid = block.attributes.content.replace(
                        /.*(?:youtube\.com|youtu\.be)\/([a-z0-9_-]{11}).*/mis,
                        '$1',
                    );
                    if (ytid.length != 11) {
                        ytid = block.attributes.content.replace(
                            /.*youtube\.com\/embed\/([a-z0-9_-]{11}).*/mis,
                            '$1',
                        );
                        if (
                            ytid.length == 11 && block.attributes.content.includes('<iframe')
                        ) {
                            console.error(
                                `ToDo: <iframe> within a <${block.name}>: skipping`,
                            );
                            continue;
                        }
                    }
                }

                let repl = mapping[ytid];
                if (!repl) {
                    if (ytid.length != 11) {
                        console.warn(
                            `${block.name} warning: no replacement (#${++block_shown}/${ytids.length}) for ${ytid} in`,
                            !block_shown ? block : null,
                        );
                    }
                    continue;
                }

                if (!isNaN(repl)) repl = 'https://vimeo.com/' + repl;
                debug(`replacing ${block.name}(${ytid}) by ${repl}`);

                let content = RichTextData.fromHTMLString(
                    block.attributes.content.replace(reg, repl),
                );
                // wp.data.dispatch('core/block-editor').updateBlockAttributes(block.clientId, {content});
                block.attributes.content = content;
                if (block.attributes.content === content) {
                    replacements++;
                } else {
                    console.error(
                        "ToDo : Can't save ?? ",
                        block.attributes.content.toHTMLString(),
                    );
                }
            }
        }
    }

    return [blocks, replacements];
}

export default async function apply(provider, options) {
    const { map: mapping_file } = options,
        mapping = JSON.parse(fs.readFileSync(mapping_file));
    if (!Object.keys(mapping).length) {
        throw Error('Invalid mapping file');
    }
    const unvimeo = (e) => isNaN(e) && !e.includes('https://vimeo.com');
    if (Object.values(mapping).some(unvimeo)) {
        // exclude draft videos like "481855618/e13e060b31"
        console.warn(Object.values(mapping).filter(unvimeo));
        throw Error(
            'Mapping file contains invalid replacements (neither Vimeo ID nor Vimeo URL',
        );
    }

    // start boiler-plate data-provider iterator-code
    const it = provider.it();
    while (true) {
        const { done, value } = await it.next();
        if (done) {
            break;
        }
        if (done === undefined) {
            throw Error('Iterator error. done is undefined');
        }

        const { source, content } = value;
        identityPreCheck(source, content, options);
        // end boiler-plate data-provider iterator-code

        const [blocks, replacements] = action(source, content, mapping);

        if (replacements > 0) {
            if (wp.blocks.serialize(blocks) === content) {
                throw Error(
                    `${source}: ${replacements} replacements but generate markup is identical`,
                );
            }

            dryRun(source, content, blocks, { ...options, count: replacements });
            console.log(`${source} : ${replacements} replacements to be made`);
            await provider.write(source, blocks);
        }
    }
    epilog();
}
