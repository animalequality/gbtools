import fs from 'fs';
import { globSync } from 'glob';
import path from 'path';
import { __, setLocaleData } from '@wordpress/i18n';
import { debug, diffString, dryRun, inhibitParseWarnings, isDebug } from './console-utils.js';

class ImportError extends Error {}
const loadModule = async (modulePath) => {
    try {
        return await import(modulePath);
    } catch (e) {
        throw new ImportError(`Unable to import module ${modulePath}`);
    }
};

let HAS_PUPPETEER = false,
    executablePath,
    puppeteer,
    percySnapshot;
try {
    ({ executablePath } = await loadModule('puppeteer'));
    puppeteer = await loadModule('puppeteer-core');
    percySnapshot = await loadModule('@percy/puppeteer');
    HAS_PUPPETEER = true;
} catch (e) {
}

function mergeTranslations(target, source) {
    for (const key in source) {
        if (source.hasOwnProperty(key)) {
            if (typeof source[key] === 'object' && typeof target[key] === 'object') {
                mergeTranslations(target[key], source[key]);
            } else {
                target[key] = source[key];
            }
        }
    }
    return target;
}

/**
 * Look for the package i18n JSON files and load the ones matching a given locale
 */
async function setUpLocale(provider, locale, sources = []) {
    const domain = 'ae-gutenberg-blocks',
        package_pot_file = `@animalequality/gutenberg-blocks/languages/${domain}.pot`;

    if (locale === 'auto') {
        locale = provider.detectLocale();
        if (!locale) {
            throw Error("i18n: Can't guess a locale");
        }
        debug(`i18n: locale guessed: "${locale}"`);
    }

    let allTranslations = {};
    const base_path = import.meta.resolve(package_pot_file),
        base_dir = path.dirname(base_path.replace('file://', '')),
        i18n_resources = globSync(`${base_dir}/${domain}-${locale}-*.json`);

    for (const file of i18n_resources) {
        const rawData = fs.readFileSync(file, 'utf-8'),
            translationData = JSON.parse(rawData);
        allTranslations = mergeTranslations(
            allTranslations,
            translationData.locale_data.messages,
        );
    }

    setLocaleData(allTranslations, domain);
    debug(`i18n: "More Posts" = "${__('More Posts', domain)}"`);
}

function silentParse() {
    inhibitParseWarnings(1);
    const markup = wp.blocks.parse(...arguments),
        warning_output = inhibitParseWarnings(0);
    return [markup, warning_output];
}

/**
 * Test function
 * Should generally return <true>
 */
function identity(content, parseOptions) {
    return wp.blocks.serialize(wp.blocks.parse(content, parseOptions)) ===
        content.trimEnd();
}

function identityPreCheck(source, content, options) {
    const { identityCheck, write, forceWrite } = options;
    if (!identityCheck) {
        return;
    }
    const parseOptions = { __unstableSkipMigrationLogs: true };

    inhibitParseWarnings(1);
    const is_identical = identity(content, parseOptions),
        warning_output = inhibitParseWarnings(0);
    if (!is_identical) {
        if (isDebug()) {
            console.warn(warning_output);
            if (diffString) {
                dryRun(
                    source,
                    content,
                    wp.blocks.parse(content, parseOptions),
                    options,
                );
                var diff = diffString.diffString(
                    content.trimEnd(),
                    wp.blocks.serialize(
                        wp.blocks.parse(content, { __unstableSkipMigrationLogs: true }),
                    ),
                );
                console.warn(diff);
            }
        }
        if (write && !forceWrite) {
            throw Error(
                `Can't ensure identity of ${source} and --force-write not passed. Stopping.`,
            );
        }
    }
}

/**
 * A hookable version of wp.blocks.createBlocksFromInnerBlocksTemplate.
 */
function createBlocksFromInnerBlocksTemplateV2(blocks) {
    if (blocks.length === 0) {
        return [];
    }

    if (typeof blocks[0] === 'string') {
        const [
            name,
            attrs,
            innerBlocks = [],
            { variation: variation_name = null, patches = [] } = {},
        ] = blocks;
        const attributes = { ...(default_attrs[name] || {}), ...attrs };

        /**
         * A variation has been specified: Generate the structure of the pattern and apply structure's patches
         */
        if (variation_name || patches.length) {
            if (variation_name) {
                const variation = wp.blocks.getBlockType(name).variations.filter((v) =>
                    v.name === variation_name
                )[0];
                console.log(`found var ${variation_name} of ${name} = `, variation);
                const rawBlock = wp.blocks.createBlocksFromInnerBlocksTemplate([
                    { name, attributes, innerBlocks: variation.innerBlocks },
                ]);
            } else {
                /**
                 * A block pattern, but no variations. Try to extract the structure
                 * before patching.
                 *
                 * ToDo
                 */
                const blockType = wp.blocks.getBlockType(name);
                console.log(`found block for ${name} = `);
                // const Context = React.createContext(null);
                // React.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED.ReactCurrentDispatcher.current = null;
                // return wp.blocks.createBlocksFromInnerBlocksTemplate([{name, attributes, innerBlocks: []}]);
            }

            console.log(rawBlock);
            for (const [path, value] of Object.entries(patches)) {
                console.log(`0 ${path} = `, lodash.get(rawBlock, path));
                console.log(
                    '1 = ',
                    lodash.get(rawBlock, '[0][innerBlocks][0][innerBlocks][0]'),
                );
                const curval = lodash.get(rawBlock, path);
                if (!curval) {
                    continue;
                }
                console.log('set ' + path + ' ' + value);
                lodash.set(rawBlock, path, value);
            }

            return rawBlock;
        }

        /**
         * Call ourselves recursively
         */
        return wp.blocks.createBlock(
            name,
            attributes,
            createBlocksFromInnerBlocksTemplateV2(innerBlocks),
        );
    }

    /**
     * Iterate over blocks and call ourselves in a row.
     */
    const items = [];

    for (const block of blocks) {
        items.push(createBlocksFromInnerBlocksTemplateV2(block));
    }

    return items;
}

function* walkBlocks(blocks, parents = []) {
    for (const block of blocks) {
        yield [block, parents];
        if (block.innerBlocks) {
            yield* walkBlocks(block.innerBlocks, [...parents, block]);
        }
    }
}

function createBlockFromTemplate(name, template = null) {
    if (!template) {
        template = wp.blocks.getBlockType(name)._template;
    }

    return wp.blocks.createBlock(
        name,
        {},
        wp.blocks.createBlocksFromInnerBlocksTemplate(template),
    );
}

async function percySnap(url) {
    if (!HAS_PUPPETEER) {
        return false;
    }
    const puppet = await puppeteer.launch({
        executablePath: executablePath(),
        ignoreHTTPSErrors: true,
    });
    const page = await puppet.newPage();

    await page.goto(url, { waitUntil: 'networkidle2', timeout: 60000 });
    await percySnapshot(page, 'Gutenberg Blocks', { widths: [360, 1200] });
    puppet.close();
}

export {
    createBlockFromTemplate,
    HAS_PUPPETEER,
    identityPreCheck,
    loadModule,
    percySnap,
    setUpLocale,
    silentParse,
    walkBlocks,
};
