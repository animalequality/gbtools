import {identityPreCheck} from './utils.js';
import {dryRun, silentParse, epilog} from './console-utils.js';

function action(markup) {
  const blocks = silentParse(markup);
  let replacements = 0;

  const updatedBlocks = blocks.filter(block => {
    if (block.name !== 'core/html') {
      return true;
    }

    const markup = document.createElement('template');
    markup.innerHTML = block.attributes.content;
    const firstElement = markup.content.children[0];

    if (firstElement && ['div', 'center'].includes(firstElement.tagName.toLowerCase()) && firstElement.childNodes.length === 0) {
      replacements++;
      return false;
    }

    return true;
  });

  return [updatedBlocks, replacements];
}

async function apply(provider, options) {
  // start boiler-plate data-provider iterator-code
  const it = provider.it();

  while (true) {
    const {done, value} = await it.next();
    if (done) {
      break;
    } else if (done === undefined) {
      throw Error('Itarator error. done is undefined');
    }

    const {source, content} = value;
    identityPreCheck(source, content, options);
    // end boiler-plate data-provider iterator-code

    const [blocks, replacements] = action(content);

    if (!replacements) {
      continue;
    }

    dryRun(source, content, blocks, options);
    console.log(`${source} : ${replacements} replacements to be made`);
    await provider.write(source, blocks);
  }
  epilog();
};

export default apply;
