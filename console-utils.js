import process from 'process';

const INFO = 1,
    DEBUG = 2;
let verbosity;
let originalConsole = {
    warn: console.warn,
    error: console.error,
    NODE_ENV: process.env.NODE_ENV,
    output: '',
};
let diffString = false;

var modified_sources = [];

class ImportError extends Error {}
const loadModule = async (modulePath) => {
    try {
        return await import(modulePath);
    } catch (e) {
        throw new ImportError(`Unable to import module ${modulePath}`);
    }
};

try {
    diffString = await loadModule('word-diff');
    diffString = diffString.default;
} catch (e) {}

function silentParse(markup) {
    inhibitParseWarnings(1);
    const parsed = wp.blocks.parse(markup, { __unstableSkipMigrationLogs: true });
    inhibitParseWarnings(0);
    return parsed;
}

function lastParseWarnings() {
    return originalConsole.output;
}

function inhibitParseWarnings(action) {
    if (['true', 'yes', 'inhibit', 'silent', 1].includes(action)) {
        if (!originalConsole.warn) {
            originalConsole.warn = console.warn;
        }
        if (!originalConsole.error) {
            originalConsole.warn = console.error;
        }
        if (!originalConsole.NODE_ENV) {
            originalConsole.NODE_ENV = process.env.NODE_ENV;
        }
        // See NODE_ENV at packages/blocks/src/api/validation/logger.js
        process.env.NODE_ENV = 'test';
        originalConsole.output = '';
        console.warn = console.error = (chunk) => {
            if (typeof chunk === 'string') {
                originalConsole.output += chunk;
            }
        };
        return null;
    } else {
        if (originalConsole.warn) {
            console.warn = originalConsole.warn;
            originalConsole.warn = null;
        }
        if (originalConsole.error) {
            console.error = originalConsole.error;
            originalConsole.error = null;
        }
        process.env.NODE_ENV = originalConsole.NODE_ENV;
        return originalConsole.output;
    }
}

function isInfo() {
    return verbosity >= INFO;
}

function isDebug() {
    return verbosity >= DEBUG;
}

function setVerbosity(v) {
    verbosity = v || 0;
    return verbosity;
}

function info() {
    if (isInfo()) {
        console.log(...arguments);
    }
}

function debug() {
    if (isDebug() && arguments[0].length > 0) {
        console.log(...arguments);
    }
}

// ToDo: use the identity()-processed markup as a base for the diff to make it less noisy
function dryRun(source, markup, updated_blocks, { write, count }) {
    modified_sources.push(source);
    // Don't output dry-run info, if actually writing
    if (write) {
        return;
    }

    if (count) {
        console.log(`Post ${source} needs ${count} modifications`);
    } else {
        console.log(`Post ${source} needs modification`);
    }

    if (isInfo()) {
        showDiff(markup, updated_blocks);
    }
    // console.log(id, post_content);
}

function showDiff(orig, blocks) {
    if (diffString) {
        var diff = diffString.diffString(
            orig.trimEnd(),
            typeof blocks === 'string' ? blocks : wp.blocks.serialize(blocks),
        );
        console.warn(diff);
    }
}

function epilog() {
    info(
        `${modified_sources.length} sources impacted: ${modified_sources.join(' ')}`,
    );
}

export {
    debug,
    diffString,
    dryRun,
    epilog,
    info,
    inhibitParseWarnings,
    isDebug,
    isInfo,
    lastParseWarnings,
    setVerbosity,
    silentParse,
};
