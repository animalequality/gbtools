import { exec } from 'child_process';
import { createBlockFromTemplate } from './utils.js';
import { percySnap } from './migration-to-gutenberg/utils.js';

export default () => {
    const blocksToRender = wp.blocks.getBlockTypes().filter((blockType) => {
        return blockType.category === 'animalequality-patterns' &&
            blockType._template && blockType.name.indexOf('latest') === -1;
    });

    for (const block of blocksToRender) {
        const postTitle = block.name.replace('/', '-');
        const content = wp.blocks.serialize(createBlockFromTemplate(block.name));

        exec(
            `wp @aebr post create --post_title='${postTitle}' --post_type='page' --post_status='publish' --post_content='${content}'`,
            async (error, stdout, stderr) => {
                if (error || stderr) {
                    console.warn(error, stdout, stderr);
                }

                await percySnap(`https://br.animalequality.me/${postTitle}`);
            },
        );
    }
};
