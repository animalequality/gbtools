<div class="center"><img class="wp-image-1556" src="/app/uploads/2016/08/header_wbs_groß_860.png" /></div>
<p class="rtejustify">&quot;<em>Der Film, den wir euch gleich zeigen werden, ist nur schwer zu ertragen...</em>&quot; so beginnt <a href="https://www.facebook.com/annemendenofficial" target="_blank" rel="noopener noreferrer">Schauspielerin Anne Menden</a> ihren Prolog f&uuml;r Factory Farm, einer 360&deg;-Dokumentation: Zuschauer begleiten Animal Equalitys Rechercheleiter, Jose Valle, auf eine Reise hinter die Kulissen der Fleischindustrie. &quot;<em>Wenn Sie erst einmal drin sind, gibt es kein Zur&uuml;ck. Dies ist Ihre letzte Chance zu gehen</em>&quot;, warnt dieser die Zuschauer zu Beginn.</p>
<p class="rtejustify">Warum also nicht einfach umdrehen? Warum nicht einfach wegsehen? Weil wir verstehen m&uuml;ssen, welches Leid diese Tiere in der Massentierhaltung durchstehen m&uuml;ssen<em>.</em> Und weil wir alle die Macht haben, Zust&auml;nde wie diese zu &auml;ndern. Doch daf&uuml;r m&uuml;ssen wir uns ihrer erst bewusst werden:</p>
<p> </p>
<p class="rtecenter"><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/y-QHLBhvOT0" width="560"></iframe></p>
<p class="rtejustify">Bitte teilen Sie dieses Video in Ihrem Freundeskreis, mit Ihrer Familie und Bekannten! Nur gemeinsam k&ouml;nnen wir eine bessere Welt f&uuml;r Tiere erreichen.</p>
<p class="rtejustify">______________</p>
<p class="rtejustify">Tieren helfen: <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">www.LoveVeg.de</a></p>
<p class="rtejustify">Mehr &uuml;ber unser 360&deg;-Projekt: <a href="https://animalequality.de/" target="_blank" rel="noopener noreferrer">www.iAnimal360.de</a></p>
<p class="rtejustify">Unsere Arbeit f&uuml;r Tiere unterst&uuml;tzen: <a href="http://www.ae-mitglied.de" target="_blank" rel="noopener noreferrer">www.ae-mitglied.de</a></p>

