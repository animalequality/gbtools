<p class="rtejustify">Tierschutz-Aktivist*innen haben über zwei Jahre in 100 Milchbetrieben, elf Lebendtiermärkten und acht Schlachthöfen in Indien recherchiert. Ergebnis ist die von Animal Equality veröffentlichte Reportage <em><a href="https://www.youtube.com/watch?v=r2hURAApOyU" target="_blank" rel="noopener noreferrer">Verehrt und gequält - Das Leiden Indiens "heiliger" Kühe für Milch</a></em>, die bislang umfangreichste Recherche in der indischen Milchindustrie. Diese ist direkt mit der indischen Lederindustrie verbunden – und Deutschland gehört zu den wichtigsten Abnehmern dieses Leders aus Indien.</p>
<p class="rtejustify">Die Aufnahmen offenbaren Schockierendes:</p>

<blockquote>
<ul>
 	<li class="rtejustify">Kühe werden von unerfahrenen Arbeitern mit verunreinigten Geräten und <strong>unter unhygienischen Bedingungen besamt.</strong></li>
 	<li class="rtejustify">Durch den emotionalen Stress, den die Trennung von Kuh und Kalb mit sich bringt, hören einige Tiere auf, Milch zu produzieren; diese Kühe werden von den Arbeitern <strong>neben toten, mit Stroh ausgestopfte Kälbern platziert.</strong> So sollen sie glauben, es sei das eigene Kalb.</li>
 	<li class="rtejustify">Die Tiere werden von Arbeitern mit Stöcken und Ketten geschlagen, sie werden getreten und <strong>teilweise sogar sexuell missbraucht.</strong></li>
 	<li class="rtejustify"><strong>In keinem der untersuchten Betriebe werden die Kühe tierärztlich versorgt;</strong> sie bleiben entweder unbehandelt oder die Landwirte wenden eigenhändig Medikamente an, um Kosten zu senken.</li>
 	<li class="rtejustify">Auf dem Weg zum Schlachthof werden <strong>bis zu 30 Tiere auf einen Transporter</strong> verladen, der eigentlich nur für maximal sechs Tiere zulässig ist.</li>
 	<li class="rtejustify">Händler verdrehen oder brechen die Schwänze der Tiere, <strong>reiben ihnen Chili in die Augen und rammen Stöcke in ihre Genitalien,</strong> um sie in die Schlachttransporter zu zwingen.</li>
 	<li class="rtejustify">Im Schlachthof wird den Kühen und Rindern <strong>mehrfach die Kehle durchgeschnitten;</strong> kaum ein Arbeiter tötet die Tiere bereits mit dem ersten Schnitt.</li>
 	<li class="rtejustify">Viele Tiere sind während des Schlachtprozesses bei vollem Bewusstsein und <strong>erleben auch noch die anschließende Häutung mit.</strong></li>
</ul>
</blockquote>
<p class="rtejustify">Zahlreiche Medien berichteten über die Recherche, darunter <em>The Times of India</em>, <em>India Times</em> und <em>Hindustan Times</em>.</p>
<p class="rtecenter"><iframe src="https://www.youtube.com/embed/r2hURAApOyU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<p class="rtejustify">Bereits 2016 hat Animal Equality die Ergebnisse dieser Recherchen der indischen Regierung präsentiert und Empfehlungen ausgesprochen, wie ein höherer Tierschutzstandard in der Milch-, Fleisch- und Lederwirtschaft gewährleistet werden kann. Daraufhin hat Indiens Regierung zwar zunächst einige Verbote insbesondere für Lebendtiermärkte erlassen, um die Kühe und Büffel vor dem extremen, aber alltäglichen Leid zu schützen. Doch nach Widerstand aus einigen indischen Regionen sind die Tiermärkte zu einigen Praktiken wie dem Verkauf ausgedienter Milchkühe zu Schlachtzwecken zurückgekehrt. <strong>Auch in Milchbetrieben und Schlachthäusern herrschen noch immer tierquälerische Zustände vor.</strong></p>
<p class="rtejustify">Wir haben <a href="https://www.change.org/p/gequ%C3%A4lt-f%C3%BCr-milch-und-leder-hilf-indiens-heiligen-k%C3%BChen" target="_blank" rel="noopener noreferrer">eine Petition gestartet</a>, um die indische Regierung davon zu überzeugen, endlich die notwendigen Schritte einzuleiten, um die Kühe, Kälber und Büffel vor dieser Qual zu schützen. Unterstützen Sie unsere Forderungen noch heute <a href="https://www.change.org/p/gequ%C3%A4lt-f%C3%BCr-milch-und-leder-hilf-indiens-heiligen-k%C3%BChen" target="_blank" rel="noopener noreferrer">mit Ihrer Unterschrift.</a></p>
<p class="rtecenter"><img class="wp-image-1882" style="display: inline; text-align: center; width: 600px; margin: 3px 0px 3px 0px;" src="/app/uploads/2017/11/deadlydairy2.jpg" alt="" /></p>
<p class="rtecenter"><img class="wp-image-1883" style="display: inline; text-align: center; width: 600px; margin: 3px 0px 3px 0px;" src="/app/uploads/2017/11/deadlydairy3.jpg" alt="" /></p>
<p class="rtecenter"><img class="wp-image-1884" style="display: inline; text-align: center; width: 600px; margin: 3px 0px 3px 0px;" src="/app/uploads/2017/11/deadlydairy4.jpg" alt="" /></p>
<p class="rtecenter"><img class="wp-image-1886" style="display: inline; text-align: center; width: 600px; margin: 3px 0px 3px 0px;" src="/app/uploads/2017/11/deadlydairy6.jpg" alt="" /></p>
<p class="rtecenter"><img class="wp-image-1885" style="display: inline; text-align: center; width: 600px; margin: 3px 0px 3px 0px;" src="/app/uploads/2017/11/deadlydairy5.jpg" alt="" /></p>

<h5 class="rtecenter"><a href="https://www.change.org/p/gequ%C3%A4lt-f%C3%BCr-milch-und-leder-hilf-indiens-heiligen-k%C3%BChen" target="_blank" rel="noopener noreferrer"><strong>Jetzt zur Petition</strong></a></h5>
