<p align="justify">Animal Equality veröffentlicht erneut Aufnahmen von Tierschutz-Aktivist*innen aus der spanischen Stopfleberindustrie. Diesmal stammen die Aufnahmen aus einem Betrieb des zweitgrößten Stopfleberproduzenten Spaniens, Caracierzos. Dabei wurden dramatische Verstöße gegen geltende Tierschutzgesetze sowie entsetzliches Tierleid dokumentiert. Dies ist bereits die dritte Veröffentlichung aus der Stopfleberindustrie im Jahr 2012. Zuletzt wurden Aufnahmen aus Katalonien, dem Baskenland und aus Südfrankreich veröffentlicht. Der besuchte Betrieb liegt in der Stadt Santa Eulalia del Campo im Nordosten Spaniens. Er beliefert die Firmen Martiko und Collverd. Das Unternehmen Collverd wird von Jordi Terol geleitet, dem derzeitigen Vizepräsidenten der europäischen Vereinigung EURO FOIE GRAS.</p>
Während dieser Recherche dokumentierten Tierschutz-Aktivist*innen den Prozess der Ausbeutung der Enten für die Stopfleberherstellung im Betrieb Caracierzos vom Moment der Ankunft der gerade ein paar Tage alten Enten bis zu den Umständen, unter denen sie zwangsernährt und anschließend getötet werden. Die Tierschutz-Aktivist*innen haben mittels Video-und Fotoaufnahmen das Leid, das die Tiere in diesem Betrieb ertragen müssen, aufgezeichnet. Unter anderem wurde folgendes dokumentiert:
<ul>
 	<li>Eine in Blut getränkte Ente, deren Schnabel gebrochen war, blieb mehrere Tage ohne ärztliche Versorgung. Dies wurde an drei verschiedenen Tagen mit Fotos festgehalten.</li>
 	<li>Enten mit Augeninfektionen.</li>
 	<li>Enten, die versuchten aus den Einzelkäfigen zu entkommen, in die sie gesperrt waren und in denen sie große Schwierigkeiten hatten, sich zu bewegen.</li>
 	<li>Enten, die sich ständig wiederholende Verhaltensweisen in den Käfigen zeigten, was ein mögliches Anzeichen für Stress ist.</li>
 	<li>Enten mit Atembeschwerden.</li>
 	<li>Tote Enten innerhalb und außerhalb der Käfige und sogar eine tote Ente, deren Kopf in der Tränke lag.</li>
</ul>
<strong><strong>Warnung</strong>: Das folgende Video zeigt extreme Gewalt gegen Tiere</strong>

<strong>&nbsp;</strong>

<strong><iframe src="https://player.vimeo.com/video/54154731?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=c0e7f8" scrolling="no" allowfullscreen="allowfullscreen" width="580" height="325" frameborder="0">&amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;gt;</iframe></strong>

Überdies, stellen die Tierschutz-Aktivist*innen folgende Ordnungsverstöße fest, die bei den zuständigen Behörden angezeigt worden sind:
<ul>
 	<li>Hinweise darauf, dass die Tiere von tierärztlicher Seite vernachlässigt werden. Die Ermittler dokumentierten wiederholt verschiedene gesundheitliche Probleme der Tiere.</li>
 	<li>Mastanlagen, die kein Luftfiltersystem und/oder keine Fenster haben. Die Tiere werden in Plastikkonstruktionen gefangen gehalten, in die kein Tageslicht eintreten kann.</li>
 	<li>Mastanlagen, die keine Tröge und keine Tränken haben, die dafür ausgelegt sind, Krankheitsübertragungen zu verhindern.</li>
 	<li>Mastanlagen, die keine geeignete Ausrüstung und Kontrollen zur Reinigung und Desinfektion von Fahrzeugen, die auf die Anlagen fahren und diese verlassen, besitzen.</li>
</ul>
<p align="justify">Es ist außerdem wichtig zu unterstreichen, dass Caracierzos die Enten während der Mast in Einzelkäfigen gefangen hält, eine Praxis, die europäische gesetzliche Regelungen ausdrücklich verbieten. Diese Gesetzwidrigkeit wurde von Animal Equality im August 2012 bei den zuständigen Behörden angezeigt. Wir erinnern daran, dass am 17. Oktober acht EU- Abgeordnete in Brüssel das europaweite Verbot von Stopfleber verlangt haben. Diese Initiative stützte sich auf Material der Recherchen von Animal Equality in verschiedenen Stopfleberbetrieben in Spanien und Frankreich. Weiterhin hat aufgrund der Recherche von Animal Equality die größte Supermarktkette in Italien, COOP, die 1471 Verkaufsstellen betreibt, uns am 31. Oktober berichtet, dass sie beschlossen haben, den Verkauf von Stopfleber einzustellen. Wir unterstützen diese Initiativen und fordern ein europaweites Verbot von Stopfleber!</p>
