<p class="rtejustify">In britischen Schlachth&auml;usern werden Tiere <strong>im Schnitt sechs Mal am Tag so grausam behandelt</strong>, dass diese Praktiken als schwerwiegende Verst&ouml;&szlig;e gegen das Tierschutzgesetz eingestuft werden m&uuml;ssen.</p>
<p class="rtejustify">Das zeigen vor Kurzem ver&ouml;ffentlichte Berichte, die der zust&auml;ndigen Beh&ouml;rde, der Food Standards Agency (FSA) vorgelegt wurden. Die Berichte ber&uuml;cksichtigen sowohl Aufnahmen von <strong>versteckten Kameras</strong>, als auch von Veterin&auml;ren und Inspektoren dokumentierte Vorf&auml;lle in den Jahren 2014 bis 2016.</p>
<p class="rtejustify">Die Berichte beschreiben mehr als 4000 schwere Verst&ouml;&szlig;e gegen Tierschutzrichtlinien in den vergangenen zwei Jahren. Unter anderem wurde dokumentiert, dass H&uuml;hner noch lebend in kochendes Wasser getaucht wurden und Tiere durch Hitze oder K&auml;lte w&auml;hrend des Transports zu den Schlachth&auml;usern starben. Die Berichte zeigen, dass viele Tiere in schrecklichem Zustand in den Schlachth&auml;usern ankommen. Manche sind zu schwach, um stehen zu k&ouml;nnen; andere leiden an Krankheiten, Br&uuml;chen oder offenen Wunden. <strong>Diese grausamen Zust&auml;nde sind die Norm</strong> &ndash; <a href="https://animalequality.de/neuigkeiten/2016/03/31/2016-03-31-betaeubungsgeraet-im-schlachthaus-2-jahre-lang-defekt/" target="_blank" rel="noopener noreferrer">in allen Schlachth&auml;usern</a>.</p>
<p class="rtejustify" style=""><img alt="" class="wp-image-11959" src="/app/uploads/2016/09/bericht_england.jpg" style="width: 450px; float: left;" /></p>
<p class="rtejustify">Zwischen Juli 2014 und Juni 2016 haben Veterin&auml;re und Gesundheitsinspektoren der FSA <strong>insgesamt 9511 Gesetzesverst&ouml;&szlig;e</strong> dokumentiert. Von diesen wurden 4000 als schwerwiegend eingestuft. Au&szlig;erdem warnten sie, dass jeder dieser Vorf&auml;lle <strong>hunderte von Tieren betroffen</strong> haben k&ouml;nnte.</p>
<p class="rtejustify">Dies sind nur wenige Beispiele dieser Verst&ouml;&szlig;e:</p>
<blockquote>
	<ul>
		<li class="rtejustify">
			Tausende von Tieren werden nicht den Vorschriften entsprechend bet&auml;ubt. So sind H&uuml;hner und Schweine <strong>bei vollem Bewusstsein in kochendes Wasser getaucht worden</strong>.</li>
		<li class="rtejustify">
			In 600 F&auml;llen sind Tiere schon tot am Schlachthaus angekommen. In einem dieser F&auml;lle sind mehr als 500 H&uuml;hner <strong>in einem LKW an Hitze gestorben</strong>.</li>
		<li class="rtejustify">
			In einem anderen Fall sind 170 H&uuml;hner im LKW <strong>auf dem Weg zu Schlachthaus erfroren</strong>.</li>
		<li class="rtejustify">
			Eine Kuh ist mit einer <strong>Wunde im Gesicht</strong> im Schlachthaus angekommen, die voller W&uuml;rmer war. Die Wunde war durch Schl&auml;ge verursacht worden.</li>
	</ul>
</blockquote>
<p class="rtejustify">Der Bericht enth&uuml;llt ein weiteres Mal die andauernden <a href="https://animalequality.de/neuigkeiten/2016/06/01/undercover-recherche-britische-schweinebetriebe/" target="_blank" rel="noopener noreferrer">Tierqu&auml;lerei durch die Tierindustrie</a>. Solche und andere Grausamkeiten in Schlachth&auml;usern und Betrieben sind &uuml;ber die Jahre umfangreich dokumentiert worden. <strong>Nicht nur in Gro&szlig;britannien</strong>, sondern &uuml;berall dort, wo <a href="https://animalequality.de/undercover-recherchen/" target="_blank" rel="noopener noreferrer">Undercover-Recherchen</a> in der Tierindustrie durchgef&uuml;hrt worden sind.</p>
<p class="rtejustify">Millionen von Menschen weltweit haben sich bereits dazu entschlossen, Fleisch durch leckere tierfreundliche Alternativen zu ersetzen. Wenn auch Sie <strong>Zust&auml;nde wie diese nicht l&auml;nger unterst&uuml;tzen</strong> wollen, probieren Sie &ouml;fter tierfreundliche Gerichte! Entdecken Sie tolle Rezepte auf <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">www.LoveVeg.de</a>.</p>
<p> </p>
<p> </p>
<p> </p>
<h4>Quelle</h4>
<p class="rtejustify">The Bureau of Investigative Journalism: &quot;<a href="https://www.thebureauinvestigates.com/2016/08/28/severe-welfare-breaches-recorded-six-times-day-british-slaughterhouses/" target="_blank" rel="noopener noreferrer">Severe welfare breaches recorded six times a day in British slaughterhouses</a>&quot;</p>
<p class="rtejustify">Foto, klein: <a href="https://www.thebureauinvestigates.com/" target="_blank" rel="noopener noreferrer">The Bureau of Investigative Journalism</a></p>
<p class="rtejustify">Header: Animal Equality</p>
