<p> </p>
<p class="rtejustify">Die Aufnahmen zeigen, wie Arbeiter kleine K&auml;lber schlagen und K&uuml;hen brutal ins Gesicht treten. In diesem Betrieb wurden mehrere solcher F&auml;lle an nur einem Tag dokumentiert. Selbst wir sind von der rohen Gewalt gegen diese gutm&uuml;tigen Tieren zutiefst schockiert:</p>
<blockquote>
	<ul>
		<li class="rtejustify">
			Junge K&auml;lber werden durch Tritte und Schl&auml;ge zum Aufstehen gezwungen.</li>
		<li class="rtejustify">
			Mutterk&uuml;he werden durch das gewaltvolle Zuschlagen von Metallgattern verletzt.</li>
		<li class="rtejustify">
			Arbeiter dr&uuml;cken und werfen K&auml;lber grob auf den Boden. Dabei br&uuml;llen sie den ver&auml;ngstigten Tierkindern Obsz&ouml;nit&auml;ten ins Gesicht.</li>
		<li class="rtejustify">
			K&uuml;he, die erst vor Kurzem entbunden haben, werden wiederholt geschlagen und getreten.</li>
	</ul>
</blockquote>
<p class="rtejustify">In diesem Video k&ouml;nnen Sie sich selbst einen Eindruck der Zust&auml;nde vor Ort verschaffen. Das Video enth&auml;lt Szenen von extremer Gewalt an Tieren:</p>
<p class="rtecenter"><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/SjBtOQMM_UY" width="560"></iframe></p>
<p class="rtejustify">Auf Hinweis eines besorgten Anwohners, der Kontakt zu uns aufnahm, fuhren wir mehrmals zu besagtem Betrieb.</p>
<p class="rtejustify"><strong>Bereits beim ersten Betreten des Betriebs Ende Oktober dokumentierten unsere Ermittler, dass die Hinterbeine von mehr als einem halben Dutzend K&uuml;he mit Hilfe von Fu&szlig;fesseln zusammengebunden worden waren.</strong> Diese veraltete und qualvolle Methode wird bei K&uuml;hen angewandt, die w&auml;hrend der Entbindung eine Fraktur des Beckens erlitten haben. Das kommt heutzutage in der modernen Massentierhaltung leider h&auml;ufig vor. Denn die Z&uuml;chtung der K&uuml;he auf h&ouml;chstm&ouml;gliche Milchleistung geht oftmals zulasten ihrer eigenen Gesundheit. Auch in vielen deutschen Milchbetrieben ist der Einsatz von Fu&szlig;fesseln immer noch g&auml;ngige Praxis.</p>
<p class="rtejustify">Auf dem Aufnahmematerial ist au&szlig;erdem zu sehen, dass viele der Tiere lahmen und unter &auml;u&szlig;erst schmerzhaften Druckstellen leiden. Davon ist jedes Jahr rund die H&auml;lfte aller K&uuml;he in England betroffen.&nbsp;<strong>Besonders eine Kuh, gekennzeichnet mit der Nummer 1098, litt offenkundig unter heftigen Beschwerden und konnte kaum noch laufen. </strong>Wir meldeten diesen Fall umgehend den zust&auml;ndigen Beh&ouml;rden.</p>
<p class="rtejustify">Zwei tote K&auml;lber lagen auf dem Betonboden vor dem Kuhstall. Achtlos hatte man sie dort hingeworfen, sichtbar f&uuml;r K&uuml;he, die schwanger waren oder gerade erst entbunden hatten. Beide K&auml;lber hatte man erschossen, weil sie m&auml;nnlich und somit f&uuml;r die Milchproduktion unbrauchbar sind. <strong>Die T&ouml;tung von Tieren durch einen Schuss in den Kopf ist in den meisten L&auml;ndern der EU rechtlich zul&auml;ssig. In Deutschland ist diese Praxis nur bei Nott&ouml;tungen erlaubt.</strong></p>
<p class="rtejustify">Auch bei sp&auml;teren Besuchen in dem Betrieb waren jedes Mal K&uuml;he an den Hinterbeinen gefesselt. Einige der Tiere waren schon seit mehreren Wochen ununterbrochen in diesem Zustand belassen worden. Auch die Kuh mit der Nummer 1098 war immer noch da, und auch ihr qualvoller Zustand war unver&auml;ndert.</p>
<p class="rtejustify"><img alt="" class="wp-image-11964" src="/app/uploads/2016/12/coll_somerset.jpg" style="width: 850px; margin: 0px; float: left;" /></p>
<p> </p>
<p class="rtejustify">Wir beschlossen, den Betrieb genauer zu &uuml;berwachen, und installierten Anfang Dezember in einem der St&auml;lle eine versteckte Kamera. Die Aufzeichnungen &uuml;bertrafen unsere schlimmsten Erwartungen. <strong>Niemals h&auml;tten wir mit Szenen von solch unglaublicher Brutalit&auml;t gerechnet.</strong> Animal Equality hat das komplette Filmmaterial an die zust&auml;ndigen &ouml;rtlichen Beh&ouml;rden weitergeleitet und fordert nun, dass die Verantwortlichenn vor Gericht gestellt werden.</p>
<p class="rtejustify">Unsere Recherche zeigt leider einmal mehr, dass die tats&auml;chlichen Lebensbedingungen der Tiere oft nicht im Geringsten dem entsprechen, was uns die jeweiligen Betriebe und Unternehmen wei&szlig; machen wollen. Auch der von uns untersuchte Betrieb bezeichnet sich als &Ouml;ko-Hof und wirbt auf seiner Homepage mit Fotos von gl&uuml;cklichen Tieren auf gr&uuml;nen Weiden und einem eigenen Hof-Verkauf mit regionalen Produkten. Immer wieder zeigt sich im Zuge von Undercover-Ermittlungen in Betrieben wie diesem, dass das gerne bem&uuml;hte Bild der Bauernhofidylle mit der Realit&auml;t meist wenig zu tun hat. <strong>Stattdessen m&uuml;ssen die Tiere ein qualvolles Dasein unter unw&uuml;rdigen Bedingungen fristen - und das leider auch in Deutschland.</strong></p>
<p class="rtejustify" style=""><strong>____________________________</strong></p>
<p class="rtejustify" style=""><a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer"><strong>Love Veg</strong></a></p>
<p class="rtejustify" style="">Sie k&ouml;nnen dem schrecklichen Leid von K&auml;lbern und ihren M&uuml;ttern in den Milchbetrieben auf der ganzen Welt ganz einfach ein Ende setzen, indem Sie die zahlreichen M&ouml;glichkeiten einer pflanzlichen Ern&auml;hrung ausprobieren. Tipps hierzu finden Sie auf: <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">www.LoveVeg.de</a></p>
<p class="rtecenter">&nbsp;</p>

