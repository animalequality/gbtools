<p class="rtejustify">Animal Equality ver&ouml;ffentlicht erstmals Bilder, die die <strong>Standardpraktiken in der Milchindustrie in Mexiko</strong> zeigen. K&uuml;he und K&auml;lber sind diesen entsetztlichen Methoden ihr Leben lang ausgesetzt. Keiner anderen Organisation ist es bislang gelungen, Aufnahmen in der Milchindustrie in Mexiko zu machen.</p>
<p class="rtejustify"><a href="https://www.flickr.com/photos/animalequalityde/albums/72157678018057784" target="_blank" rel="noopener noreferrer">Das Video- und Bildmaterial</a> zeigt <strong>die nie endende Gewalt und das Leid der Tiere</strong>:</p>
<blockquote>
	<ul>
		<li class="rtejustify">
			K&uuml;he werden immer wieder <strong>zwangsgeschw&auml;ngert</strong>. Wie jedes S&auml;ugetier geben sie nur dann Milch, wenn sie ein Kind geboren haben.</li>
		<li class="rtejustify">
			K&auml;lber werden <strong>von ihren M&uuml;ttern getrennt</strong>, wenn sie nur wenige Stunden oder wenige Tage alt sind. Ihnen wird die Muttermilch verweigert &ndash; denn das ist die Milch, die die Milchindustrie verkauft.</li>
		<li class="rtejustify">
			K&auml;lber werden in enge, dreckige <strong>Einzelboxen</strong> gesperrt.</li>
		<li class="rtejustify">
			K&uuml;he werden <strong>enthornt</strong> &ndash; eine Praktik, die in anderen L&auml;ndern bereits verboten ist.</li>
		<li class="rtejustify">
			K&uuml;hen werden die <strong>Schw&auml;nze abgeschnitten</strong>, was in vielen anderen L&auml;ndern ebenfalls verboten ist.</li>
		<li class="rtejustify">
			Die Tiere werden mit <strong>brennenden Metallst&auml;ben</strong> &ldquo;markiert&rdquo;.</li>
		<li class="rtejustify">
			Beim maschinellen Melken werden die K&uuml;he immer wieder <strong>am Euter verletzt</strong>. Infektionen und Blutungen sind die Folge.</li>
		<li class="rtejustify">
			Infektionen und Verletzungen werden <strong>nicht tier&auml;rztlich behandelt.</strong></li>
		<li class="rtejustify">
			<strong>Sobald die Milchproduktion nachl&auml;sst</strong>, <a href="https://animalequality.de/neuigkeiten/2016/12/02/schlachthof-recherche-in-mexiko/" target="_blank" rel="noopener noreferrer">werden die K&uuml;he im Schlachthaus get&ouml;tet.</a></li>
	</ul>
</blockquote>
<p class="rtejustify"><a href="https://www.flickr.com/photos/animalequalityde/albums/72157678018057784" target="_blank" rel="noopener noreferrer"><img alt="" class="wp-image-1651" src="/app/uploads/2017/03/5.jpg" style="width: 400px; float: left;" /></a></p>
<p class="rtejustify">Derzeit sieht das mexikanische Recht <strong>keine Strafen</strong> f&uuml;r diejenigen vor, die sogenannte Nutztiere missbrauchen und grausam behandeln. Dulce Ram&iacute;rez, Vorsitzende von Animal Equality in Mexiko, erkl&auml;rt:</p>
<p class="rtejustify">&ldquo;<em>Es ist Zeit, die bestehenden Gesetze in Mexiko zu verst&auml;rken, um diesem Missbrauch entgegen zu treten. Tierqu&auml;lerei muss strafrechtlich verfolgt werden k&ouml;nnen. Die Tiere in der Nutztierindustrie werden wie Maschinen behandelt, geschlagen und in dreckige Buchten eingesperrt. Ihr Leid wird v&ouml;llig ignoriert</em>&rdquo;.</p>
<p class="rtejustify">Hier finden Sie alle Bilder der Recherche: <a href="https://www.flickr.com/photos/animalequalityde/albums/72157678018057784" target="_blank" rel="noopener noreferrer">Brutale Aufnahmen aus der Milchindustrie in Mexiko.</a></p>
<p> </p>
<p class="rtejustify">Der einfachste und effektivste Weg den Schweinen, K&uuml;hen und H&uuml;hnern in den Zucht-, Mast- und Schlachtbetrieben dieser Welt zu helfen, ist diese Tiere von der eigenen Speisekarte zu streichen. <strong>Helfen Sie Tieren</strong> und probieren Sie &ouml;fter pflanzliche Gerichte: <a href="https://LoveVeg.de/" target="_blank" rel="noopener noreferrer">www.LoveVeg.de</a></p>

