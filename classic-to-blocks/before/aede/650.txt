<p class="rtejustify">Die Haltung von Hennen in sogenannten Legebatterien ist in der EU seit dem 1. Januar 2012 verboten. Die Legehennenhaltung erfolgt seither in Kleingruppen-, Boden-, Freiland- sowie Biohaltung. Eine neue Kampagne von Animal Equality in Spanien macht deutlich, dass die Kleingruppenhaltung als Nachfolgemodell zur alten K&auml;fighaltung keine Verbesserung f&uuml;r die Hennen bedeutet &ndash; <strong>die Hennen leiden auch hierbei in Käfigen.</strong></p>
<p class="rtejustify">Das im Zuge der Kampagne ver&ouml;ffentlichte Video- und <a href="https://www.flickr.com/photos/animalequalityde/albums/72157678747834141" target="_blank" rel="noopener noreferrer">Bildmaterial</a> zeigt:</p>
<blockquote>
	<ul>
		<li class="rtejustify">
			Hennen, die stressbedingt fast alle Federn verloren haben.</li>
		<li class="rtejustify">
			tote und sterbende Tiere inmitten von noch lebenden Artgenossen.</li>
		<li class="rtejustify">
			&uuml;berquellende Leichencontainer.</li>
		<li class="rtejustify">
			Hennen mit gek&uuml;rzten Schn&auml;beln (noch immer eine g&auml;ngige Praxis in vielen Betrieben).</li>
		<li class="rtejustify">
			kranke Hennen, die zu schwach sind, um Nahrung oder Trinken zu erreichen.</li>
	</ul>
</blockquote>
<p class="rtejustify">Zahlreiche Tiere haben vom ununterbrochenen Stehen auf Gitterboden wunde, gebrochene und deformierte F&uuml;&szlig;e. <strong>Nicht selten bleiben die Tiere mit ihren F&uuml;&szlig;en zwischen den Gitterst&auml;ben h&auml;ngen.</strong> Ohne Wasser und Nahrung erreichen zu k&ouml;nnen, sterben die Hennen so langsam und qualvoll.</p>
<p class="rtejustify"><iframe allowfullscreen="" class="giphy-embed" frameborder="0" height="270" src="https://giphy.com/embed/5lnfwsnkczpzW" width="480"></iframe></p>
<p>&nbsp;</p>
<p class="rtejustify"><a href="https://www.flickr.com/photos/animalequalityde/albums/72157678747834141" target="_blank" rel="noopener noreferrer"><img alt="" class="wp-image-11940" src="/app/uploads/2017/02/Collage_Spain.jpg" style="width: 850px; margin: 0px; float: left;" /></a></p>
<p> </p>
<p class="rtejustify">&laquo;<em>Die Haltung von H&uuml;hnern in K&auml;figen z&auml;hlt zu den grausamsten Praktiken der Tierindustrie. Die Verbraucher haben ein Recht darauf zu erfahren, was Kleingruppenhaltung wirklich bedeutet und wie diese Tiere dort leben. Animal Equality wird alles daf&uuml;r tun diese Haltungsform zu beenden</em>&raquo;, so Javier Moreno, Vorsitzender von Animal Equality in Spanien.</p>
<p class="rtejustify">In Deutschland soll die Haltung von Hennen in Kleingruppen zwar bis 2025 abgeschafft werden, doch immer wieder haben <a href="https://www.youtube.com/watch?v=DYPWO0NEug4" target="_blank" rel="noopener noreferrer">Recherchen von Animal Equality</a> gezeigt, dass die Tiere auch in alternativen Haltungsformen &ndash; wie Boden-, Freiland- oder Biohaltung &ndash; leiden. <strong>Keine Haltungsform wird den nat&uuml;rlichen Bed&uuml;rfnissen der sensiblen, intelligenten V&ouml;gel gerecht.</strong></p>
<p class="rtejustify">Am einfachsten und sichersten k&ouml;nnen wir Tierleid mindern oder verhindern, wenn wir tierische Produkte durch pflanzliche ersetzen. Haben Sie <a href="http://www.animalequality.de/neuigkeiten/kochen-und-backen-ohne-eier" target="_blank" rel="noopener noreferrer">diese tollen Alternativen</a> zu herk&ouml;mmlichen Eierspeisen und Eiprodukten schon probiert?</p>

