<p class="rtejustify">Die bayerische Landtagsabgeordnete Rosi Steinberger erhielt auf Anfrage vom Staatsministerium f&uuml;r Umwelt und Verbraucherschutz eine Auflistung aller Verst&ouml;&szlig;e gegen das Tierschutzgesetz, die 2015 und 2016 bei der Anlieferung von Schweinen auf den Schlachthof Landshut dokumentiert wurden. <strong>Die Sprecherin der Landtags-Gr&uuml;nen fordert nun von der Umweltministerin Ulrike Scharf einen Tierschutzplan f&uuml;r Bayern.</strong></p>
<p class="rtejustify">&ldquo;<em>Seit Jahren fordern wir mehr Kontrollen und strengere Strafen, und seit Jahren greifen die Beh&ouml;rden nicht durch. Anstatt nur <strong>wirkungslose Ermahnungen</strong> auszusprechen, m&uuml;ssen die Missst&auml;nde mit h&auml;rteren Strafen konsequent abgestellt werden</em>&rdquo;, so Steinberger.</p>
<p class="rtejustify">Dies sind nur einige der Verst&ouml;&szlig;e gegen das Tierschutzgesetz, die vom 01.01.2015 bis zum 14.10.2016 bei der Ankunft der Schweine in Landshut dokumentiert wurden:</p>
<div class="rtejustify">
	&nbsp;</div>
<div class="rtejustify">
	&bull; 32 Schweine mit ausgepr&auml;gter Lahmheit.</div>
<div class="rtejustify">
	&bull; In f&uuml;nf F&auml;llen litten Schweine an erheblichen Verletzungen und Entz&uuml;ndungen, unter ihnen auch Muttertiere.</div>
<div class="rtejustify">
	&bull; 10 Schweine mit Umfangsvermehrungen.</div>
<div class="rtejustify">
	&bull; In drei F&auml;llen wiesen Schweine Kratzspuren auf, die ihnen bei einer Lebendschau mit einem Schlagstempel zugef&uuml;gt worden waren.</div>
<div class="rtejustify">
	&bull; Schweine mit Enddarmvorf&auml;llen, Klauenabszessen, Frakturen, Schwanznekrosen und aufgebl&auml;hten Abdomen.</div>
<div class="rtejustify">
	&bull; Schweine, die sich auf der Fahrt gegenseitig die Schw&auml;nze abgebissen hatten.</div>
<div class="rtejustify">
	&bull; Schweine, die nicht mehr laufen konnten.</div>
<div class="rtejustify">
	&bull; In drei F&auml;llen waren <a href="https://animalequality.de/blog/9-19-2015-der-weg-zum-schlachter/" target="_blank" rel="noopener noreferrer">Transportfahrzeuge</a> nicht ausreichend eingestreut.</div>
<div class="rtejustify">
	&bull; In elf F&auml;llen wurde die Ladedichte &uuml;berschritten.</div>
<div class="rtejustify">
	&nbsp;</div>
<p class="rtejustify">Das Fleischhygieneamt Passau sprach <strong>57 m&uuml;ndliche Verwarnungen</strong> aus. Andere F&auml;lle wurden an die Stadt, das Ordnungsamt und das Landratsamt Landshut weitergereicht. Anschlie&szlig;end erfolgten in einigen F&auml;llen Strafanzeigen und Ordnungswidrigkeitsverfahren, u. a.:</p>
<div class="rtejustify">
	&nbsp;</div>
<div class="rtejustify">
	&bull; Eine Person hatte beim Entladen Schweine mit einem elektrischen &ldquo;Viehtreiber&rdquo; gequ&auml;lt.</div>
<div class="rtejustify">
	&bull; Schweine wurden trotz erheblicher Verletzungen und Krankheiten geschlachtet.</div>
<div class="rtejustify">
	&bull; Ein Tierhalter hatte ein Schwein mit tiefen Schnittwunden abgeliefert, die bereits mit Maden befallen waren.</div>
<div class="rtejustify">
	&bull; <strong>Eine schwangere, abgemagerte Schweinemutter mit Lungenentz&uuml;ndung wurde geschlachtet.</strong></div>
<div class="rtejustify">
	&bull; &Uuml;berladung von Tiertransportern.</div>
<div class="rtejustify">
	&bull; <strong>Der Fahrer eines Transporters versuchte ein bereits bewegungsunf&auml;higes Schwein zu treiben und f&uuml;gte ihm dabei erhebliche Schmerzen zu.</strong></div>
<div class="rtejustify">
	&nbsp;</div>
<p class="rtejustify" style=""><img alt="" class="wp-image-11953" src="/app/uploads/2017/01/aeitalia_2015.jpg" style="width: 450px; margin: 10px; float: right;" /></p>
<p class="rtejustify">Grunds&auml;tzlich sind solche Verst&ouml;&szlig;e keine Seltenheit, sie werden jedoch nur selten zur Anzeige gebracht. <strong>Alle geschilderten Probleme treten regelm&auml;&szlig;ig und h&auml;ufig auf.</strong> Verletzte, lahme Schweine sind vor allem bei angelieferten Ferkeln eher die Regel als die Ausnahme.</p>
<p class="rtejustify">Der Schlachthof Landshut ist einer von zwei bayerischen Standorten des niederl&auml;ndischen Konzerns &ldquo;Vion Food Group&rdquo;, dem gr&ouml;&szlig;ten Schweinefleischproduzenten des Bundeslandes. <strong>Jede Woche werden in dem Schlachtbetrieb Landshut 16.500 Schweine get&ouml;tet</strong> &ndash; alle 10 Sekunden ein Schwein.</p>
<p class="rtejustify">Die Politikerin Rosi Steinberger f&uuml;rchtet, dass die lokalen Beh&ouml;rden eine zu offene Kommunikation mit Vion pflegen und aufgrund dieser N&auml;he zu dem Konzern befangen sein k&ouml;nnten.</p>
<p class="rtejustify">In Landshut werden die Schweine zun&auml;chst bet&auml;ubt, bevor ihnen mit einem Messer in den Hals gestochen wird, damit sie &ldquo;entbluten&rdquo;. <strong>Die Bet&auml;ubung durch Kohlenstoffdioxid ist in Deutschland Standard und gilt derzeit als die &ldquo;humanste&rdquo; und modernste Methode. </strong>Erfolgt anschlie&szlig;end kein &ldquo;sauberer&rdquo; Stich und damit keine &quot;richtige&quot; Entblutung, wacht das Schwein wieder auf und verblutet grausam.</p>
<p class="rtejustify"><strong>Allein 2016 wurden in Landshut sieben F&auml;lle dokumentiert, in denen Schweine nicht &quot;richtig&quot; entblutet wurden. Ein Schwein wurde gar nicht gestochen und verbr&uuml;hte qualvoll bei vollem Bewusstsein. </strong></p>
<p class="rtejustify">________________________________________</p>
<p class="rtejustify">+++<strong><span style="color:#027ac6;">Update 20.02.2017</span>:</strong> Die erste Sonderkontrolle des Jahres, durchgef&uuml;hrt vom Landesamt f&uuml;r Gesundheit und Lebensmittelsicherheit (LGL) hat ergeben, dass sich <strong>die Zust&auml;nde in bayerischen Schlachth&ouml;fen nicht verbessert</strong> haben. Auch bei der unangek&uuml;ndigten Sonderkontrolle gab es erhebliche M&auml;ngel bei der Bet&auml;ubung und Schlachtung der Tiere, die deutlich gegen das Tierschutzgesetz versto&szlig;en. Noch immer werden Schlachtarbeiten bei Tieren begonnen, die <strong>bei vollem Bewusstsein</strong> sind: Was die Tiere in diesen Minuten erleben m&uuml;ssen, ist unvorstellbar.+++</p>
<p class="rtejustify">________________________________________</p>
<p class="rtejustify">Es gibt keine humane Art ein Tier zu t&ouml;ten, das nicht sterben will. Bitte ziehen Sie es in Betracht, Tieren zu helfen, indem Sie sie von Ihrer Speisekarte streichen. Tolle Alternativen, spannende Tipps und leckere Rezepte f&uuml;r fleischfreies Kochen finden Sie bei <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">www.LoveVeg.de</a>.</p>
<p> </p>
<p class="rtecenter"><a href="https://LoveVeg.de/" target="_blank" rel="noopener noreferrer"><img alt="" class="wp-image-12052" src="/app/uploads/2017/01/lovevegcover.png" style="display: inline; text-align: center;width: 700px; margin: 0px;" /></a></p>
<p> </p>
<p> </p>
<p> </p>
<p class="rtejustify">Quellen:</p>
<p class="rtejustify">ARD: <a href="http://www.ardmediathek.de/tv/Kontrovers/Missst%C3%A4nde-am-Schlachthof-Landshut/BR-Fernsehen/Video?bcastId=14913688&amp;documentId=36811638" target="_blank" rel="noopener noreferrer">Missst&auml;nde am Schlachthof Landshut</a></p>
<p class="rtejustify">Bayerischer Rundfunk: <a href="http://www.br.de/nachrichten/schlachthof-landshut-gesetzesverstoesse-hygienemaengel-100.html" target="_blank" rel="noopener noreferrer">Jahrelang Gesetzesverst&ouml;&szlig;e und Hygienem&auml;ngel</a></p>
<p class="rtejustify"><a href="https://rosi-steinberger.de/start/expand/636235/nc/1/dn/1/" target="_blank" rel="noopener noreferrer">Rosi Steinberger fordert Tierschutzplan f&uuml;r Bayern</a></p>
<p class="rtejustify">Bayerischer Rundfunk: <a href="http://www.br.de/nachrichten/misstaende-schlachthoefe-bayern-100.html" target="_blank" rel="noopener noreferrer">Weiter Missst&auml;nde in Bayern</a></p>
<p class="rtejustify">Bildquellen: Animal Equality Recherche in der italienischen Schweinefleischindustrie (2015)</p>

