<span style="font-weight: 400;">Una iniciativa que insta a los gobiernos del mundo a implementar medidas inmediatas y realistas para frenar el cambio climático se ha convertido en un movimiento global. Mañana, viernes 15 de marzo, <strong>tendrá lugar la primera protesta masiva de #FridaysForFuture (Viernes por el Futuro) con 1.300 convocatorias en más de 100 países, incluyendo a#México.</strong> </span>

&nbsp;

<span style="font-weight: 400;">La impulsora del movimiento es <a href="https://twitter.com/GretaThunberg" target="_blank" rel="noopener">Greta Thunberg</a>, una adolescente sueca de 16 años quien desde agosto de 2018 decidió dejar de asistir a clases cada viernes para protestar frente a su escuela y exigir al gobierno que reduzca sus emisiones. Desde hace meses en </span><span style="font-weight: 400;">Alemania, Australia, Bélgica, Francia o Suiza<strong> los paros y protestas se han hecho masivos cada viernes.</strong> Salir de las aulas es, de acuerdo con Thunberg, una forma de simbolizar que no van a estudiar para un futuro que no tendrán. </span>

&nbsp;

<span style="font-weight: 400;">En relación con esto, el informe <a href="https://www.unep.org/news-and-stories/video/planet-focus-global-environment-outlook" target="_blank" rel="noopener">“Perspectiva Ambiental Global”</a> en el marco del Programa de las Naciones Unidas para el Medio Ambiente (PNUMA), que evalúa la situación ecológica global de los últimos 5 años, 250 científicos de más de 70 países concluyeron que sin voluntad política el mundo no podrá alcanzar un desarrollo sostenible en 2030 ni, mucho menos, para 2050. Para lograrlo, los expertos aconsejan que tanto en países desarrollados como en vías de desarrollo <strong>no solo se renuncie al plástico sino que se apliquen reducciones del consumo de carne</strong>, lo cual reduciría en un 50% el aumento necesario para producir alimentos para los 10 mil millones de personas que poblarán la Tierra en el 2050.</span>

&nbsp;
<h3><a href="https://loveveg.mx/#sign-up"><span style="font-weight: 400;">Consigue GRATIS un fantástico recetario con deliciosos platillos veggie</span></a><span style="font-weight: 400;">, además de una guía con consejos prácticos para una alimentación sostenible. </span></h3>
&nbsp;

<span style="font-weight: 400;">Todo esto es debido a la ineficiencia del sistema de producción de carne y que lo convierte en uno de los principales factores del cambio climático. Este requiere 16.000 litros de agua para producir un solo kilo de carne, emite el 15% del total de los gases de efecto invernadero del planeta y ocupa el 45% de las tierras habitables, entre otros males. </span><span style="font-weight: 400;"> </span>

&nbsp;

<span style="font-weight: 400;">Más de 12.000 científicos firmaron una carta que respalda al movimiento #FridaysForFuture y en ella afirman que lo que piden los estudiantes está justificado y que las medidas adoptadas hasta ahora han sido insuficientes.<strong> Es el momento de que los gobiernos del mundo hagan frente al impacto que el consumo de carne tiene en el planeta</strong> ya que las políticas gubernamentales actuales están casi todas a favor del crecimiento de la ganadería industrial y obstaculizan el desarrollo de alternativas a la carne que son sostenibles, más sanas y libres de crueldad hacia los animales.  </span>

&nbsp;
