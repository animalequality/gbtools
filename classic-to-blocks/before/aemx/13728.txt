Le Pain Quotidien se suma a la creciente lista de empresas que han decidido comprometerse a poner fin al cruel confinamiento de gallinas en México, al <a href="https://www1.lepainquotidien.com/mx/es/gallinas" target="_blank" rel="noopener noreferrer">anunciar&nbsp;hoy su compromiso de eliminar de su línea de suministro el huevo proveniente de jaula para 2025</a>.

<img class="wp-image-13335" style="float: left; margin-left: 10px; margin-right: 10px; width: 450px;" src="/app/uploads/2018/09/used_for_esselunga_1.jpg" alt="">

<strong>Le Pain Quotidien</strong> es una empresa de panadería y restaurantes de reconocimiento mundial, este compromiso tendrá un gran impacto a favor de las gallinas siendo miles las que ya no tendrán que vivir enjauladas.

La nueva política de Le Pain Quotidien se suma a una serie de medidas tomadas por diversas empresas &nbsp;en México como <a href="https://igualdadanimal.mx/noticia/2017/09/08/grupo-andersons-anuncia-su-compromiso-para-eliminar-los-huevos-de-gallinas-enjauladas/" target="_blank" rel="noopener noreferrer">Grupo Anderson’s</a>, <a href="https://igualdadanimal.mx/noticia/2018/01/30/pasteleria-fina-neufeld-anuncia-que-eliminara-las-jaulas-de-su-suministro-de-huevo/" target="_blank" rel="noopener noreferrer">Pastelería Fina Neufeld</a> y <a href="https://igualdadanimal.mx/noticia/2018/09/14/montparnasse-pasteleria-anuncia-que-eliminara-las-jaulas-de-su-suministro-de-huevo/" target="_blank" rel="noopener noreferrer">Montparnasse Pastelería </a>para deshacerse del uso de jaulas en respuesta a los consumidores, quienes cada día exigen una mayor transparencia así como mejoras en el trato animal dentro del sistema alimentario.

Esperamos que próximamente más empresas anuncien sus compromisos en beneficio de sus clientes y los animales.

En Igualdad Animal México sabemos que libre de jaula no significa libre de sufrimiento, pero consideramos que este compromiso es un avance hacia un mundo más compasivo. Seguiremos trabajando para eliminar este sistema cruel e innecesario.
