<div class="center"></div>
Desde que hace dos años lanzamos nuestro proyecto de realidad virtual <em>iAnimal,</em> hemos avanzado en nuestro objetivo de influir positivamente a la sociedad mostrando la realidad que viven los animales dentro de la ganadería industrial.

Tan solo en México, más de <strong>60.000 espectadores en más de 37 eventos multitudinarios</strong> han podido conocer la impresionante experiencia inmersiva que es <em>iAnimal</em>: entrar a una granja y matadero industrial y ser ese animal, estar en su piel y ver todo desde su perspectiva. Algunos de ellos son el Cosquín Rock México, el Punk Rock Fest, el Corona Wuf y el Festival Roxy.

Te puede interesar: <a href="https://igualdadanimal.mx/noticia/2018/09/19/lanzamos-love-veg-en-mexico-tu-herramienta-para-una-alimentacion-saludable-y-compasiva/">Lanzamos Love Veg en México: tu herramienta para una alimentación saludable y compasiva</a>

Entre el 27 y el 30 de septiembre de este año, y por tercer año consecutivo, se realizará en la Ciudad de México el <a href="http://vrfest.mx/">Festival Internacional de Realidad Virtual</a>, el primer festival de realidad virtual en México que reúne a desarrolladores, generadores de contenido y emprendedores de la realidad virtual, aumentada y mixta. <strong>Para nosotros fue la oportunidad perfecta para presentar iAnimal, ya que el VR Fest 2018 tenía por objetivo demostrar que la realidad virtual es mucho más que videojuegos, y que también abarca experiencias inmersivas con fines educativos y de alcance social</strong>.

<a href="https://loveveg.mx/#sign-up">Consigue GRATIS cuatro fantásticos recetarios</a> con deliciosos platillos veggie, ¡además de una guía con consejos prácticos para una alimentación sostenible!

El evento, que fue a la vez marco del tercer VR Challenge, un concurso internacional de contenidos VR, ofreció a los asistentes conferencias, talleres, exposiciones, una zona de negocios y espacios de capacitación, empatía y empoderamiento. Katya Ramírez, nuestra coordinadora de proyectos educativos en México, fue una de las ponentes y <strong>presentó la conferencia <em>iAnimal: la realidad virtual como poderosa herramienta para cambiar el mundo</em>.</strong>

<iframe src="https://www.youtube.com/embed/VoGOP6u6vlo" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Al respecto de <em>iAnimal</em>, Ramírez afirma: «Nuestro proyecto de realidad virtual convierte a los espectadores en testigos. Testigos de la cotidianidad dentro de una granja, de los procesos más brutales a los que son sometidos los animales para acelerar la producción y abastecer la gran demanda de carne. <em>iAnimal </em>es una herramienta poderosa para sensibilizar y lograr que las personas creen un lazo de empatía con los animales y decidan sacarlos de sus platos».

Hemos sido la primera organización en todo el mundo en usar la realidad virtual para mostrar la realidad de los animales de granja.
