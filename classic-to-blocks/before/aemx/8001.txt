<div class="center"></div>
Trabajamos para cambiar el mundo de los animales y hoy queremos compartir contigo que 2017 fue un año increíble de activismo efectivo para ayudar a los animales de granja. <strong>Con nuestros programas educativos llegamos a miles de personas</strong> y la posibilidad de un mundo compasivo y libre de maltrato animal se construye cada día con tres de nuestros programas más exitosos de educación en México.
<h2><strong>1. iAnimal</strong></h2>
Acudimos a 25 eventos masivos con nuestro proyecto de realidad virtual que te transporta a granjas y mataderos, <strong>fuimos parte de los festivales de música más importantes como el Cosquín Rock México, Festival Roxy,  Punk Rock Fest, Corona Wuf, Calavera Hip Hop, Corona Weekend, Festival Sucede, Revolution Fest y Campus Party</strong>, la concentración más grande de estudiantes Universitarios en México. Nadie ha podido quedar indiferente y más 4,450 espectadores pudieron ponerse en el lugar de los animales y se sumaron al compromiso de reducir y eliminar su consumo de carne, huevo y lácteos.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2016/10/20/la-realidad-virtual-que-te-transporta-mataderos-llega-mexico/">La realidad virtual que te transporta a mataderos llega a México</a></strong>

<span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: x-large;"><img class="wp-image-12763" style="margin-left: 20px; margin-right: 20px; float: left; width: 300px;" src="/app/uploads/2017/12/34993680351_65af9cf5b8_o.jpg" alt="" /></span></span></span></span>

<span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: x-large;"> <img class="wp-image-12759" style="width: 300px;" src="/app/uploads/2017/12/26794648449_97a2fab06a_o.jpg" alt="" /></span></span></span></span>

<span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: x-large;"> </span></span></span></span>
<h2><strong>2. Streetscreening</strong></h2>
<strong>Con nuestra campaña <a href="https://descubrirlacomida.com/">Descubrir la Comida</a> llegamos a 8.400 personas</strong> que descubrieron la realidad que nos oculta la ganadería industrial a través de nuestro video mostrado en iPads. Familias completas en plazas públicas, universitarios, estudiantes de bachillerato entre otros sectores de la población descubrieron el terrible sufrimiento que padecen los animales y cómo pueden actuar para ayudarlos.

<img class="wp-image-12767" style="width: 300px; margin-left: 20px; margin-right: 20px;" src="/app/uploads/2017/12/23487613588_158359a14f_o.jpg" alt="" /><img class="wp-image-12765" style="width: 313px;" src="/app/uploads/2017/12/35695366252_afc41ab819_o.jpg" alt="" />
<span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: x-large;"><span style="font-size: xx-small;"><span style="font-size: xx-small;">                                                </span></span></span></span></span></span>
<h2><strong>3. </strong>Guía vegetariana</h2>
<strong>Nuestro maravillosos equipo de voluntarios entregó en 2017, un total de 100 mil guías</strong> que muestran lo fácil y delicioso que es dejar a los animales fuera de tu menú y lo necesario que es que actuemos en favor de ellos, de nuestra salud y el planeta.  Estas guías motivan a miles de personas a suscribirse a nuestros <a href="https://descubrirlacomida.com/">boletines nutricionales</a> de LoveVeg para modificar sus hábitos de consumo, logrando una alimentación vegetal.

<span style="font-size: xx-small;"><img class="wp-image-12775" style="margin-left: 20px; margin-right: 20px; width: 300px;" src="/app/uploads/2017/12/38207543224_eea243881f_o.jpg" alt="" /><img class="wp-image-12773" style="width: 300px;" src="/app/uploads/2017/12/35968551094_8d5dca68bb_o.jpg" alt="" /></span>

<span style="font-size: xx-small;"> </span>

2017, fue un año increíble de activismo efectivo para los animales de granja gracias a tu maravilloso apoyo. <strong>¡Vamos por un 2018 mejor en el que llegaremos a más y más personas!</strong>

Si te interesa ayudar a los animales con estos programas de educación por favor escribe a <strong>info@igualdadanimal.mx</strong>
