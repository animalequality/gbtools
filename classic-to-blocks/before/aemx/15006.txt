<span style="font-weight: 400;">¡Terminamos febrero con grandes avances para los animales!</span>

<span style="font-weight: 400;">Si sigues el trabajo de Igualdad Animal seguramente has notado que cada día <strong>nos esforzamos por entregar los mejores resultados para los animales</strong> y medimos siempre el impacto de cada actividad que hacemos para asegurar que estamos haciendo lo más y mejor posible desde nuestras líneas de acción para cambiar la terrible realidad que padecen a consecuencia de la ganadería industrial.</span>

<span style="font-weight: 400;">Al paso de los años, he aprendido que como activista mi mayor responsabilidad es tener la capacidad de ver en dónde le sirvo más a los animales y sin reparo, ponerme ahí, también he visto cómo la convicción de ayudar a los animales se extiende más y más en nuestra sociedad gracias a las diversas formas en que podemos hacerlo. </span>

<span style="font-weight: 400;">Es justo por eso que hoy quiero compartirte cómo vivimos el mes de febrero en Igualdad Animal México.</span>
<div class="ae-video-container"><center><iframe src="https://www.youtube.com/embed/6ZrHEvKf0lg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></center></div>
<div></div>
Sin duda nada de esto sería posible sin toda la ayuda y apoyo que recibimos de ustedes y nuestro maravilloso equipo  de voluntariado. ¡Gracias!

<img class="alignnone  wp-image-14574" src="https://igualdadanimal.mx/app/uploads/2019/02/firmasola.jpg" alt="" width="205" height="184" />
