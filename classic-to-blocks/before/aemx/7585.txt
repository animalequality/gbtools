Acéptalo: los huevos que compramos en el supermercado no proceden de gallinas felices porque en la ganadería industrial no hay animales felices.

<strong>Igualdad Animal México</strong> acaba de lanzar la campaña «La vida en una jaula», exponiendo ante los consumidores <strong>la realidad de la producción de huevos</strong> en el país. En la nueva website de la campaña <a href="https://lavidaenunajaula.igualdadanimal.mx/" target="_blank" rel="noopener noreferrer">se puede firmar la petición</a> pidiendo el fin de las jaulas.

El 90% de los huevos puestos a la venta en México proceden de gallinas que <strong>viven toda su vida encerradas en minúsculas jaulas de alambre</strong>. ¿Puede ser alguien feliz viviendo en esas condiciones?

La campaña muestra a los consumidores investigaciones llevadas a cabo en granjas industriales de Jalisco, principal región productora de México y en la que se produce <strong>el 50% de todos los huevos comercializados en el país</strong>.

La industria del huevo mexicana <strong>no es diferente de la de otros países</strong>. Los métodos industrializados de producción están diseñados para producir el mayor número de huevos posible al mínimo coste. Por desgracia, quien acaba pagando el mayor coste son estas frágiles y sensibles aves, condenadas a vivir en jaulas tan pequeñas que ni siquiera pueden extender sus alas.

<img class="wp-image-11117" style="float: left; margin: 10px; width: 450px;" src="/app/uploads/2016/10/28770589393_6d86822bce_o.jpg" alt="">

El maltrato animal está omnipresente en la ganadería industrial. El motivo es que para abastecer la demanda de carne, lácteos o huevos de miles de millones de consumidores en todo el mundo <strong>los métodos de producción no tienen en cuenta las necesidades naturales de los animales</strong>.
<h4>«Los métodos industrializados de producción están diseñados para producir el mayor número de huevos posible al mínimo costo».</h4>
De esta forma se les condena a <strong>vidas miserables en granjas industriales</strong> en las que no pueden ni siquiera satisfacer sus instintos.

Puedes ver el vídeo de la campaña «La vida en una jaula» de Igualdad Animal México y <a href="https://lavidaenunajaula.igualdadanimal.mx/">firmar la petición aquí.</a>
