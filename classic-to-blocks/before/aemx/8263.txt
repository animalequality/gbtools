En Igualdad Animal trabajamos incansablemente para erradicar el maltrato hacia los animales en la industria ganadera. Por eso nos complace mucho poder decir que 2018 es otro año al que podemos mirar en retrospectiva y sentirnos satisfechos por los logros obtenidos pero, sobre todo, muy animados a seguir dedicando todos nuestros esfuerzos y recursos por la construcción de ese mundo que soñamos para los animales.

<img class="wp-image-13513" src="/app/uploads/2018/12/conoce-nuestros-logros-_por-los-animales-en-2018-igualdadanimal.org_.jpg" />

Nuestras investigaciones, que desde siempre han sido la piedra angular de nuestro trabajo, lograron un gran impacto mediático apareciendo en algunas de las publicaciones y medios televisivos más destacados del mundo. <strong>En total presentamos 18 investigaciones en 6 países</strong> que llegaron a millones de personas, permitiéndoles conocer lo que sufren los animales a mano de la industria. Nuestra investigación en granjas de cerdos proveedoras de El Pozo, en colaboración con el programa “Salvados”, hizo temblar a la industria cárnica en España y llegó a tres millones de personas. Las repercusiones traspasaron las fronteras consiguiendo más de 130.000 firmas que reclaman a El Pozo una estricta política de bienestar animal que proteja a los animales de abusos y maltratos.

Las impactantes escenas de sufrimiento que obtuvimos en una granja de gallinas enjauladas de Noble Foods en Dorset, Inglaterra, logró que esta empresa, que es<strong> la mayor productora de huevos en todo el Reino Unido</strong>, anunciara su compromiso de producir únicamente huevos libres de jaulas para el año 2025.

<img class="wp-image-13525" src="/app/uploads/2019/01/conoce-nuestros-logros-por-los-animales-en-2018-igualdadanimal.org-2.jpg" alt="" />

En México, nuestros investigadores se infiltraron por primera vez en la industria del pollo revelando la brutal crueldad con la que se somete cada año a más de 1.700 millones de estos animales cada año. Y en Reino Unido, tras haber hecho públicas impactantes imágenes de maltrato animal en una granja de cerdos propiedad de Elsham Linc, uno de los productores de carne de cerdo más grandes de Gran Bretaña, tres trabajadores de dicha granja<strong> fueron acusados de delitos contra el bienestar de los animales.</strong>

Gracias a nuestra estremecedora investigación que mostró la terrible agonía de los peces durante la pesca de arrastre en el mar mediterráneo pudimos mostrar a millones de personas el sufrimiento de estos sensibles animales tan olvidados y absolutamente desprotegidos por la ley.

Y en el continuo avance por dejar atrás la Era de las jaulas logramos 24 compromisos con renombradas empresas en México, España e Italia como Starbucks, Lidl, Ferrero y Marriott que dejarán de proveerse de huevos de gallinas que provengan del cruel sistema de jaulas. Persiguiendo este mismo fin, nos hemos unido a la <a href="https://igualdad-animal.endthecageage.eu/" target="_blank" rel="noopener noreferrer">Iniciativa Ciudadana Europea End The Cage Age</a>, la mayor coalición jamás antes formada por organizaciones de protección animal  y que tiene como objetivo conseguir un millón de firmas para pedir que se prohíba la cría de animales en jaulas en la ganadería.

Determinados a avanzar por el fin de la Era de las jaulas, también en México, donde hemos conseguido este año varios de estos compromisos,  lanzamos nuestra segunda investigación en la industria del huevo que tuvo un impresionante alcance mediático de 60 millones de personas y que nos acerca más a nuestro objetivo de erradicar este cruel sistema.

A mediados de año, junto a otras organizaciones, mostramos a más de un millón de personas por día la verdad sobre las granjas de pollo que proveen a McDonald's, a través de una poderosa campaña en video<strong> en Times Square en Nueva York</strong>, y en la que pedimos a la empresa que ponga fin a las crueles prácticas en su cadena de suministro.

Como parte de nuestros proyectos educativos lanzamos con gran éxito en España y México Love Veg, sitios web donde se puede encontrar todo lo relacionado con una alimentación vegetal desde recetas, consejos, información nutricional sobre proteínas vegetales hasta noticias de actualidad y que cuenta también con cuenta de Instagram y canales de Youtube.

<img class="wp-image-13524" src="/app/uploads/2019/01/conoce-nuestros-logros-por-los-animales-igualdadanimal.org_.jpg" alt="" />

Tan solo en México este año realizamos 1.723 screenings a través de 20 intervenciones en la vía pública y logrado que 244.671 personas se sumen a nuestro boletines nutricionales e informativos en los últimos tres años. Nuestro proyecto de realidad virtual iAnimal alcanzó las 3.760 vistas en los eventos masivos más importantes y universidades y otros centros educativos de Guadalajara, nuestra ciudad sede en México y repartimos 65.558 revistas.

Siendo 2018 un año de significativos logros por los animales, en Igualdad Animal miramos hacia adelante decididos a seguir cambiando el mundo para los animales, sacando a la luz el sufrimiento que la industria mantiene oculto y llevando adelante proyectos de educativos y de transformación social que permitan a los consumidores, y también a las empresas e instituciones, realizar cambios que beneficiaran a cada vez más animales.

¡Gracias por estar ahí, otro año más por los animales!

Te podría interesar: <a href="https://igualdadanimal.mx/noticia/2018/03/16/nueva-investigacion-en-mataderos-de-conejos-de-italia/">Nueva investigación en mataderos de conejos de italia</a>
