<div class="center"></div>
El pasado 27 de octubre celebramos en Los Angeles, Estados Unidos, por tercer año consecutivo, nuestra Inspiradora Gala de Acción Global, <strong>un evento que nos reunió con nuestros mejores amigos y defensores</strong> para celebrar y galardonar a todos aquellos que están cambiando el mundo para los animales.

Entre lo presentadores de la Gala se encontraron Moby y Evanna Lynch, y como  invitados especiales Kate y Rooney Mara, Jon Mack, Violett Beane, Richard Marx y Daysi Fuentes y la presentación especial de la cantante y actriz Renee Olstead.

<img class="wp-image-13413" src="/app/uploads/2018/10/moby-evanna_lynch-gala-igualdadanimal.org_.jpg" alt="" />

La artista del tatuaje y activista por los animales <strong>Kat Von D fue galardonada con el «Animal Heroe Award»</strong>, mientras que Nic Adler, emprendedor y cofundador del restaurante Monty´s recibió el «Vegan Trailblazer Award». El empresario Ethan Brown, fue premiado con el «Compassionate Company Award» por su gran contribución al desarrollo de los productos alternativos a la carne a través de su compañía Beyond Meat.

Sharon Núñez y José Valle, cofundadores de Igualdad Animal, estuvieron junto al equipo internacional de la organización mostrando el impresionante trabajo que en 12 años hemos llevado adelante en los 8 países donde estamos presentes. <strong>Los resultados son verdaderamente sorprendentes:</strong> 4.5 millones de seguidores, 100 mil donantes, 50 mil voluntarios, 200 mil acciones realizadas, 95 investigaciones presentadas y 800 instalaciones expuestas. Y tan solo este año, hemos lanzado 17 investigaciones con un alcance de 900 millones de vistas,14 iniciativas legales, 44 acuerdos de empresas para eliminar las jaulas en la industria del huevo y hemos ayudado a más de 21 millones de animales.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2017/12/08/estrellas-celebran-en-la-inspiradora-gala-de-accion-global-de-igualdad-animal/">Estrellas celebran en la inspiradora gala de acción global de Igualdad Animal </a></strong>

Fue una fantástica velada en las que <strong>las palabras de los invitados especiales y nuestros equipo internacional inspiraron a todos los presentes.</strong> Dulce Ramírez, directora ejecutiva en México, habló sobre lo que la inspira: «Gracias a las investigaciones de Igualdad Animal he aprendido que la rabia puede ser motor que nunca se apaga y que podemos transformarla en lucha y amor. Esta noche celebro su amor y esa rabia volcada en generosidad que cobija a los investigadores de Igualdad Animal en todo el mundo».

<img class="wp-image-13414" src="/app/uploads/2018/10/kat-von-d-gala-igualdadanimal.org_.jpg" alt="" />

Ha sido otro año de intenso trabajo y nos complace haber obtenido resultados significativos para los animales. Saber que contamos con el apoyo de tantas personas que están haciendo increíbles esfuerzos para cambiar el mundo para los animales nos anima e inspira a hacer siempre más.

¡Gracias!
