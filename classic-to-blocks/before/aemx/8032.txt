Seguro que has escuchado infinidad de veces la pregunta «¿qué fue primero, el huevo o la gallina?», pero, ¿conoces la respuesta?

Desde hace siglos muchos han intentando responder a esta interrogante. Hoy por hoy, aunque la expresión sigue siendo utilizada para referirse a situaciones en las que es difícil identificar la causa y la consecuencia, los científicos tienen una respuesta. Los huevos aparecieron mucho antes que las primeras aves, incluidas las gallinas. Asimismo, hoy también se sabe que las gallinas son animales muy especiales.

Contrario a lo que la industria y la publicidad suelen mostrar, <strong>las gallinas son seres sumamente complejos y emocionales </strong>y cada huevo que ponen implica al menos 26 horas de sufrimiento para ellas.

<strong>Noticia relacionada: &nbsp;<a href="https://igualdadanimal.mx/blog/no-lo-sabias-pero-las-gallinas-son-en-realidad-como-unos-gatos-o-perros-emplumados-muy/" target="_blank" rel="noopener noreferrer">No lo sabías pero las gallinas son en realidad como unos gatos o perros emplumados muy inteligentes</a></strong>

En México existen más de 200 millones de gallinas ponedoras y, desafortunadamente, <strong>la mayoría pasan su vida encerradas en jaulas de metal diminutas. </strong>Esto les impide desarrollar comportamientos tan básicos como extender sus alas, rascarse o anidar. Su sufrimiento es inmenso y se prolonga hasta su último suspiro, y muchas perecen antes de ser llevadas al matadero.

Hoy te presentamos tres formas en las que puedes ayudar a reducir su sufrimiento:

<strong>1.Sustituye el huevo en tu alimentación</strong>

<img class="wp-image-12841" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/02/sustituir_huevo_platano_1.jpg" alt="">

Si te gusta cocinar no dudes en probar <a href="http://www.igualdadanimal.org/7583/3-increibles-formas-de-sustituir-el-huevo" target="_blank" rel="noopener noreferrer">las creativas y maravillosas alternativas al huevo</a> en gran variedad de platillos o preparaciones.

<span style="font-size: x-large;"><span style="color: #808080;"><a href="https://descubrirlacomida.com/" target="_blank" rel="noopener noreferrer">Suscríbete gratuitamente</a> a nuestra serie de boletines con recetas, consejos y trucos para una alimentación veggie.</span></span>

<span style="font-size: x-large;">&nbsp;</span>

<strong>2.Exige a las empresas que cambien el destino de millones de gallinas</strong>

<span style="font-size: x-large;"><img class="wp-image-12842" style="margin-left: 10px; margin-right: 10px; float: left; width: 451px; " src="/app/uploads/2018/02/25026857028_ba09f18283_z.jpg" alt=""></span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

Firma las peticiones para pedir a las empresas del sector alimenticio que eliminen las jaulas en su cadena de suministro de huevo. <strong>Al hacerlo ayudarás a que millones de gallinas dejen de sufrir dentro de ellas.</strong> En México le estamos pidiendo a Mac’Ma, una empresa galletera y al supermercado Soriana, que se unan a este movimiento y ayuden a las gallinas.

Por favor, firma las peticiones en los siguientes enlaces:

Mac´Ma -&gt;<a href="https://igualdadanimal.mx/actua/macma-sin-jaulas" target="_blank" rel="noopener noreferrer"> https://igualdadanimal.mx/actua/macma-sin-jaulas</a>

Soriana -&gt; <a href="https://igualdadanimal.mx/actua/soriana-sin-jaulas" target="_blank" rel="noopener noreferrer">https://igualdadanimal.mx/actua/soriana-sin-jaulas</a>

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2017/09/08/grupo-andersons-anuncia-su-compromiso-para-eliminar-los-huevos-de-gallinas-enjauladas/" target="_blank" rel="noopener noreferrer">Grupo Anderson’s anuncia su compromiso para eliminar los huevos de gallinas enjauladas</a> &nbsp; </strong>

<strong>3. Date de alta en Protectores de Animales</strong>

<span style="font-size: x-large;"><img class="wp-image-12843" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/02/9814455046_2a399d5ecf_k.jpg" alt=""></span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

<span style="font-size: x-large;">&nbsp;</span>

Cada acción cuenta y las más sencillas pueden lograr mucho por los animales. Donde te encuentres puedes comenzar a cambiar la vida de millones de gallinas. Únete a los Protectores de Animales en Igualdad Animal México y detén el maltrato animal con un solo click <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank" rel="noopener noreferrer">www.ProtectoresAnimales.mx</a>
