En todo el mundo la industria de alimentos se está adaptando a la tendencia irreversible que el crecimiento de la demanda de productos alternativos a la carne está marcando en el mercado. <strong>Y algo que hace unos años parecía un sueño ya es posible: </strong>Burger King ya ofrece en su menú la “Impossible Whopper” una hamburguesa 100% vegetal.

Esta Whopper cuya carne es elaborada por la empresa <a href="https://impossiblefoods.com/" target="_blank" rel="noopener noreferrer">Impossible Foods</a> de la cual Bill Gates es inversionista, ha impactado de tal forma que Eric Bohl, un lobista de la industria cárnica, destacó lo impresionado que había quedado con el sabor de esta carne elaborada partir de ingredientes vegetales afirmando que es una “llamada de atención” para los ganaderos. "Si los agricultores y ganaderos piensan que podemos burlarnos y descartar estos productos como una moda pasajera, nos estamos engañando", declaró.

A pesar de que la Impossible Burger ya se sirve en 6.000 restaurantes de Estados Unidos, la alianza entre la empresa californiana y Burger King<strong> llevará esta excelente alternativa a la carne a conquistar un público mundial.</strong> Otro gigante que también se ha lanzado a esta conquista es Nestlé, la mayor compañía de alimentos del mundo, y que hace pocos días anunció el lanzamiento de una hamburguesa vegana que se comenzó a comercializar a principios de abril en Europa y próximamente en Estados Unidos.

Igualmente, Tyson Foods, el mayor productor de carne de Estados Unidos y segundo en todo el mundo hace más de dos años decidió invertir en Beyond Meat otra empresa que produce alternativas a la carne como la increíble Beyond Burger y que cuenta con Leonardo Dicaprio entre sus inversionistas.

Todas estas empresas saben que los consumidores <a href="https://loveveg.mx/por-que-las-alimentaciones-veggie-estan-a-la-alza/" target="_blank" rel="noopener noreferrer">están cada vez más interesados</a> por consumir alimentos que les permitan asegurar los aportes proteínicos que necesitan pero sin que eso implique dañar a los animales y, además, reduciendo su impacto en el planeta. Sin duda, están revolucionando la forma en que producimos lo que comemos y con eso construyendo un mundo más justo para los animales y más saludable y sostenible para todos.

Fuentes:

https://www.theguardian.com/business/2019/apr/08/burger-king-impossible-whopper-plant-based-review-meat-lobbyist

https://www.elperiodico.com/es/extra/20190405/impossible-whopper-burger-king-hamburgesa-vegana-7390078

https://www.estrategiaynegocios.net/newsletter/newsletter2015/1272787-330/nestl%C3%A9-anuncia-la-venta-de-su-primer-hamburguesa-vegana
