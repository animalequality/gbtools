<div class="center">&nbsp;</div>
<img class="wp-image-13591 alignleft" style="float: right; margin-right: 5px; margin-left: 5px; width: 337px;" src="/app/uploads/2015/03/portadamel.jpg" alt="">

La conocida obra de Melanie Joy "Por qué amamos a los perros, nos comemos a los cerdos y nos vestimos con las vacas", publicada en castellano por la Editorial Plaza y Valdés en colaboración con Igualdad Animal, ya está siendo distribuida en México a través del <a href="https://elfondoenlinea.com/Libreria.aspx"><strong>Fondo de Cultura Económica</strong></a>.

<em>¿Por qué comemos unos animales&nbsp;</em><em>y no otros? ¿Por qué elegimos comer unos animales y no otros? ¿Por qué eliges comer animales? </em>

La autora responde a estas y otras preguntas en esta obra revolucionaria en la que&nbsp;explica qué es el carnismo, <strong>el invisible sistema de creencias que compone nuestras percepciones sobre la carne que comemos haciendo que amemos a algunos animales y nos comamos a otros sin saber por qué</strong>.

Dando luz a estos mecanismos invisibles del carnismo, Joy hace que <strong>sus lectores puedan estar mejor informados como consumidores y que la ciudadanía se empodere para tomar sus propias decisiones</strong>.

Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2019/01/31/estas-son-las-librerias-que-en-mexico-venderan-el-libro-hacia-un-futuro-vegano/">Estas son las librerías que en México venderán el libro Hacia un futuro vegano</a>

Melanie Joy, psicóloga social, afirma que este fenómeno se explica por un proceso de negación. Hacemos caso omiso de los hechos: de la capacidad de conciencia de los animales, de su capacidad para sentir dolor, de las crueles prácticas ganaderas, de que no necesitamos comer carne y de que, por lo general, sin ella viviríamos más y mejor.

«<em>Una lectura obligada para todo el que esté interesado en saber más acerca de qué comemos y por qué</em>.» — Kathy Feston, autora de Veganist, éxito de ventas según The New York Times.

«<em>Creo que a Gandhi le hubiera encantado este libro</em> […] <em>Le llevará de la negación a la toma de conciencia, de la pasividad a la acción, de la resignación a la esperanza</em>.» — Del prólogo.

«<em>Se trata de un libro extraordinario que podría transformar lo que siente la sociedad respecto a comer animales. Está destinado a convertirse en un clásico</em>.» — Jeffrey Moussaieff Masson, autor de The Face on Your Plate

*En España está disponible en las principales librerías y acaba de salir a la luz la segunda edición.
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/gy7XqjvrVA4" width="885px" height="664px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
