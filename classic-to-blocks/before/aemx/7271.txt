Miradas que nos hablan de la más temeraria desolación. Animales sensibles, inteligentes, con interés en vivir y disfrutar de su vida padecen la más terrible e injusta discriminación.

<img class="wp-image-13580" style="margin: 5px; width: 885px;" src="/app/uploads/2015/07/CERDOme5.jpg" alt="" />

Igualdad Animal México accedió por primera vez a uno de tantos rastros municipales que operan en la ciudad de México donde las fotografías revelan, una vez más, que los cerdos se han convertido en máquinas de producir carne y sus vidas, bien sea en las explotaciones extensivas o intensivas, sólo conocerán el sufrimiento, las privaciones y finalmente la muerte.

<img class="wp-image-13586" style="margin: 5px; width: 885px;" src="/app/uploads/2015/07/cerdome3.jpg" alt="" />

Tras el aturdimiento, se les cuelga bocabajo de las piernas traseras en una guía anclada en el techo que los transporta donde los esperan para degollarlos.

<img class="wp-image-13584" style="margin: 5px 10px; width: 885px;" src="/app/uploads/2015/07/cerdome1.jpg" alt="" />

El matarife produce una incisión profunda en la papada del cerdo para alcanzar los grandes vasos sanguíneos poco antes de su llegada al corazón.

<img class="wp-image-13587" style="margin: 5px; width: 885px;" src="/app/uploads/2015/07/cerdome4.jpg" alt="" />

Finalmente mueren desangrados, pero muchos de ellos siguen vivos cuando son arrojados a las tinas de agua hirviendo, siguen conscientes.

En 2016 presentamos<a href="https://rastrosdemexico.igualdadanimal.mx/"> una investigación completa</a> de cómo operan los rastros mexicanos
<h4><a href="https://rastrosdemexico.igualdadanimal.mx/">Rastros de México: Ayúdanos a acabar con esta crueldad</a></h4>
La Comisión Federal para la Protección contra Riesgos Sanitarios (Cofepris) ha revelado que un alto porcentaje de los rastros y mataderos administrados por los ayuntamientos presentan incumplimiento a la normatividad sanitaria vigente. [1]
<p class="break-all">[1]: http://www.financierarural.gob.mx/informacionsectorrural/Panoramas/Panorama%20Porcino%20(may%202014).pdf</p>
