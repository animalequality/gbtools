¡Hola!

<strong>El pasado 10 de mayo asistimos nuevamente a la fábrica y oficinas de Pastelerías Marisa</strong> con un mensaje de amor y respeto por las gallinas.<strong> Intentamos mostrar a la dirigente de esta empresa que es posible reducir el sufrimiento de miles</strong> de sensibles, inteligentes y sociales aves como lo son las gallinas en el marco del Día de las Madres.

<a title="Sin título" href="https://www.flickr.com/photos/igualdadanimal/47030912324/in/album-72157691367421123/" data-flickr-embed="true"><img src="https://live.staticflickr.com/65535/47030912324_5c55def9c6_k.jpg" alt="Sin título" width="2048" height="1365"></a><script async="" src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Aunque las gallinas atrapadas en la industria del huevo no ponen huevos fértiles, sufren inmensamente los maltratos de una industria sin compasión que ha convertido sus frágiles cuerpos en máquinas para producir huevos. Permanecen encerradas en jaulas diminutas, entre heces, cadáveres o compañeras agonizantes y sufriendo heridas por el roce constante con los barrotes de una jaula. Simplemente, <strong>no podemos seguir permitiendo que exista tanto sufrimiento.</strong>

Que una empresa como Pastelerías Marisa se sume con una política pública para prohibir las jaulas es algo significativo y urgente. Por todo lo anterior, el apoyo de cada persona a la petición y a las acciones informativas como la que llevamos a cabo el viernes pasado son invaluables<strong> ¡Las gallinas nos necesitan y no podemos dejar de ayudarlas!</strong>

Si te gustaría hacer más, por favor, únete hoy a <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank" rel="noopener noreferrer">Protectores de Animales</a>, y si aún no lo has hecho, firma y comparte <a href="https://www.change.org/p/pastelesmarisa-%C3%BAnete-para-eliminar-este-maltrato-animal" target="_blank" rel="noopener noreferrer">esta petición para que Pastelerías Marisa</a> prohíba públicamente las jaulas en su cadena de suministro de huevo.

¡Gracias por tu apoyo!
