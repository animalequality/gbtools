Cientos de millones de pollos <strong>sufren inmensamente cada año en las granjas que crían pollos para McDonald’s.</strong> Han sido seleccionados genéticamente para que crezcan lo más rápido posible, tanto así que no pueden soportar su propio peso y muchos mueren luego de agonizar por días sin alcanzar el agua o el alimento.

Las terribles condiciones en las que pasan toda su vida estos sensibles e inteligentes animales <strong>son inaceptables</strong>. Es por esto que nos hemos unido a otras organizaciones de protección animal de Estados Unidos para lanzar <strong>una poderosa campaña publicitaria en vídeo en Times Square en Nueva York</strong>, pidiendo a McDonald's que ponga fin a las crueles prácticas en su cadena de  suministro de carne de pollo.

<iframe src="https://www.youtube.com/embed/8BvrPHonvlA" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El anuncio se basa en la <a href="https://www.youtube.com/watch?v=sHgMh2yJ2LQ&amp;feature=youtu.be">campaña y petición</a> en curso de la coalición que se lanzó en marzo de este año con una carta abierta en The New York Times. En la misma se pedía a McDonald's que adoptara normas significativas de protección animal que redujeran el sufrimiento de cientos de millones de pollos sacrificados para sus productos cárnicos.

El coordinador de campañas corporativas de Igualdad Animal en Estados Unidos, Ollie Davidson, explicó: «Casi 90 compañías -incluyendo a muchos de los competidores de McDonald's como Burger King, Subway y Jack in the Box- ya se han comprometido a hacer cambios significativos que mejorarán la vida de estos animales».

Añadió: «¿Cuándo dejará McDonald's de mirar a otro lado y se comprometerá a eliminar las prácticas estándar más crueles utilizadas por sus proveedores?».

El anuncio se muestra en 1500 Broadway Ave. y 43rd St. <strong>hasta el 31 de octubre de 2018 </strong>y se emitirá al menos 60 veces al día en un intento de llegar al mayor número posible del millón de visitantes estimados de Times Square cada día. La campaña también se está difundiendo en redes sociales. Puedes ver la galería de imágenes <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157697726016931/" target="_blank" rel="noopener noreferrer">aquí</a>.

La coalición de organizaciones detrás del anuncio está compuesta por <a href="https://animalequality.org/" target="_blank" rel="noopener noreferrer">Animal Equality</a> (Igualdad Animal en Estados Unidos), The Humane League, Mercy For Animals, Compassion In World Farming, Compassion Over Killing y World Animal Protection.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2018/04/06/lanzamos-una-campana-dirigida-mcdonalds-para-conseguir-mejoras-para-millones-de-pollos/">Lanzamos una campaña dirigida a McDonalds para conseguir mejoras para millones de pollos </a></strong>
