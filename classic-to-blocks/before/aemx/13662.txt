<div class="center"></div>
Cuando un camión que transportaba pollos hacia el matadero por la autopista 605 en California rebotó en una grieta no se informó ningún accidente, el camión no se detuvo y es posible que el conductor no supiera lo que sucedió o no le importara.

Lo que sí es cierto es que los cientos de pollos que iban en el camión apretujados en pequeñas cajas sintieron el rebote. Varias de las cajas se abrieron y 17 de ellos cayeron sobre el asfalto. <strong>Ochos de ellos murieron por el impacto o fueron atropellados por los automóviles.</strong>

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/consumir-carne-de-pollo-es-mucho-mas-perjudicial-de-lo-que-se-creia/">Consumir carne de pollo es mucho más perjudicial de lo que se creía</a></strong>

<img class="wp-image-12806" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/01/shutterstock_116932882_0.jpg" alt="">

Todos ellos <strong>tenían apenas 42 días de vida, es decir, eran en realidad unos bebés.</strong> Pero es en esta edad, debido a la hibridación a la cual han sido sometidos y que les hace crecer a una velocidad antinatural, que los pollos «broiler» alcanzan el peso reglamentario para ser llevados al matadero.

<strong>9 de los 17 pollos que cayeron a la autopista sobrevivieron y ahora viven en el refugio de animales de 26 acres Farm Sanctuary en California. </strong>Hoy, tres semanas después del accidente, esos nueve pollos tienen nombres: Bonnie, Clyde, Mulligan, Starla, Peggy, Eliza, Jessica, Davey y Miles.

<strong>Ahora sí que se puede decir que sus vidas están apenas comenzando.</strong> Lejos de acabar en el matadero y en el plato de alguien, acá vivirán sus vidas por 5 u 8 años, que es el tiempo de vida natural para ellos.

Fuente: <a href="https://www.pasadenastarnews.com/2018/01/22/heres-the-story-of-bonnie-clyde-and-the-7-other-chickens-that-survived-crossing-the-605-freeway/" target="_blank" rel="noopener noreferrer">https://www.pasadenastarnews.com/2018/01/22/heres-the-story-of-bonnie-clyde-and-the-7-other-chickens-that-survived-crossing-the-605-freeway/</a>
