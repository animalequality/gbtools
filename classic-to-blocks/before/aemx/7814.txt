<div class="center"></div>
En México podemos lograr el mayor avance en materia de bienestar animal que habría visto el país en años: <strong>el castigo penal a la crueldad que tiene lugar en los rastros (mataderos)</strong>.

Recientemente, Igualdad Animal <a href="https://rastrosdemexico.igualdadanimal.mx/" target="_blank" rel="noopener noreferrer">reveló a través de una impresionante investigación</a> que los rastros de México incurren en innumerables violaciones a la Ley Federal de Sanidad Animal.

<a href="https://igualdadanimal.mx/suscripcion/" target="_blank" rel="noopener noreferrer">Puedes suscribirte gratuitamente a nuestro boletín</a> para que recibas las mejores noticias de actualidad sobre los animales y conozcas más opciones de alimentación.

<strong>Los animales son matados a golpes, apuñalados sin aturdimiento previo y degollados o decapitados aún conscientes</strong>, es decir, no se cumple ninguno de los protocolos que estandarizan los procedimientos de manejo de animales en rastros.

<img class="wp-image-12116" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2017/06/30419705754_ccc472b19a_z.jpg" alt="">

Pero la situación podría estar a poco de dar un giro de 180 grados y en México podríamos acabar con la impunidad que tiene lugar en los rastros.

Y es que Igualdad Animal ha solicitado a través de una propuesta que contempla una modificación a la Ley Federal de Sanidad Animal y otra en el Código Penal Federal de México para la tipificación del delito de maltrato y crueldad animal que incluya a los animales de granja.

Se trata de una oportunidad histórica que permitirá que la crueldad y maltrato cometida hacia los animales de granja pueda ser tipificada como delito

<a href="https://rastrosdemexico.igualdadanimal.mx/#petition-form" target="_blank" rel="noopener noreferrer">Por favor ayúdanos a cambiar las leyes para los animales firmando la petición.</a>

Desde tu ordenador puedes combatir el peor maltrato animal conocido: el que ocurre dentro de las granjas y mataderos industriales.

Puedes enviar correos, hacer llamada y usar las redes sociales y ser parte del equipo de Igualdad Animal haciendo frente a las empresas de la ganadería industrial que maltratan a los animales.

<strong>Escribe ya a <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank" rel="noopener noreferrer">protectoresanimales.mx</a> o <a href="https://igualdadanimal.org/defensores-animales/" target="_blank" rel="noopener noreferrer">defensoresanimales.es</a></strong>

Te podría interesar: <a href="https://igualdadanimal.mx/blog/seguiremos-avanzando-por-el-fin-de-las-jaulas/">Seguiremos avanzando por el fin de las jaulas</a>
