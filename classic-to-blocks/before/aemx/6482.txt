<div class="center"></div>
Una nueva investigación encubierta de Igualdad Animal pone al descubierto a la industria cárnica británica al revelar una impactante brutalidad en las granjas de la compañía East Anglian Pig Company (EAP), tercera productora de carne de cerdo del Reino Unido y proveedora de los principales supermercados y marcas del país.

El periódico The Sunday Mirror ha dedicado esta mañana una página completa a la investigación.
Un investigador infiltrado trabajó en dos granjas diferentes durante un total de 29 días:

<strong>Unidad de Destete de Little Thorns:</strong>
Esta es la unidad de destete, con aproximadamente entre 8.000 y 10.000 cerdos. En Little Thorns, los cerdos son criados en seis cobertizos exteriores durante un periodo de entre 6 y 8 semanas antes de ser enviados a las granjas de engorde.

<strong>Didlington, The Piggery:</strong>
Esta es la unidad de cría con 3.000 cerdos y más de 700 cerdas. Los cerdos son criados en el exterior y engordados en el interior de las naves. Más de 300 cerdos son enviados al matadero cada semana desde esta granja.

Igualdad Animal prueba una vez más que, independientemente del sello de bienestar animal que tenga la granja, sigue existiendo dolor, sufrimiento y explotación a gran escala.

<strong><span style="color: #800000;">Advertencia: El vídeo que va a ver contiene escenas de violencia hacia los animales.</span></strong>
<div class="media_embed"><iframe src="https://www.youtube.com/embed/TUxg93ghBVA" width="560px" height="315px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<div></div>
<div></div>
La investigación de Igualdad Animal ha recibido el apoyo de expertos internacionales en comportamiento animal y veterinaria.

<strong>Para leer las declaraciones de los expertos</strong>, <a href="http://britishporkindustry.co.uk/es/east-anglian-pig-expertos.php">pulsa aquí</a>.
<blockquote>
<h4></h4>
</blockquote>
<strong>HALLAZGOS DE LA INVESTIGACIÓN:</strong>

<img style="margin-left: 8px; margin-right: 4px; float: right; width: 250px;" src="http://animalequality.net/sites/default/files/imagesimages/eap_photo_selection.jpg" alt="" />
<ul>
 	<li>Lechones enfermos que eran matados a golpes. Estos animales eran cogidos de sus extremidades traseras y brutalmente golpeados con la cabeza contra el suelo repetidas veces. En un caso en el que el lechón no había muerto tras el traumatismo craneal, un trabajador trató de asfixiar al animal agarrándole el hocico con su mano y pisándole la garganta.</li>
 	<li>Confinamiento extremo en las jaulas de gestación y parideras que impiden que los animales puedan siquiera darse la vuelta.</li>
 	<li>Estereotipias. Las cerdas fueron frecuentemente observadas mordiendo los barrotes u oscilando el cuerpo constantemente.</li>
 	<li>Cerdos que padecen graves heridas. Se observaron profundas heridas en los lomos de algunos animales y muchos lechones paralizados o parapléjicos, probablemente como resultado de un daño en su médula espinal.</li>
 	<li>Lechones con abrasiones y lesiones ulceradas en sus articulaciones. Estas heridas pueden llevar a infecciones.</li>
 	<li>Varios lechones muertos en las jaulas parideras, probablemente tras haber sido aplastados por sus madres.</li>
 	<li>Cerdos golpeados en la cabeza con una barra de hierro.</li>
 	<li>Un cerdo adulto fue observado amordazado por el hocico. El animal estaba claramente sufriendo.</li>
 	<li>Varios segmentos de colas amputadas como resultado de una mutilación que es llevada a cabo de forma rutinaria en las granjas. La amputación de las colas se realiza generalmente sin anestesia y puede ser un procedimiento altamente doloroso.</li>
 	<li>Trabajadores obligando a cerdos adultos a desplazarse dándoles patadas. Una cerda fue golpeada repetidas veces recibiendo varios puñetazos en su sensible hocico y en la cabeza.</li>
 	<li>Lechones que acaban de ser destetados y son lanzados con agresividad.</li>
 	<li>Un trabajador agarrando a un cerdo adulto por su cola para inmovilizarlo y su apuñalamiento con un cuchillo.</li>
</ul>
<a title="East Anglian Pig Company - Norfolk" href="https://www.flickr.com/photos/animalequalityuk/albums/72157629572561783" data-flickr-embed="true"><img src="https://farm8.staticflickr.com/7205/6989733085_e61fedcb1d_z.jpg" alt="East Anglian Pig Company - Norfolk" width="640" height="480" /></a><script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<strong>SUPERMERCADOS Y MARCAS INVOLUCRADOS</strong>

<strong>• </strong>East Anglian Pig es una compañía propiedad de <a href="https://cranswick.plc.uk/">Cranswick Plc </a>- un proveedor alimentario líder en el Reino Unido que produce para los principales supermercados del país tales como Morrisons, <a href="https://www.facebook.com/sainsburys">Sainsbury’s</a>, <a href="https://www.facebook.com/tesco">Tesco</a>, The Cooperative Food, Waitrose y <a href="https://www.facebook.com/Asda">ASDA</a>.

<strong>• </strong>La compañía produce una variedad de productos para marcas como <a href="https://www.facebook.com/jamieoliver">Jamie Oliver</a>, <a href="https://www.facebook.com/weightwatchers?ref=ts">Weight Watchers</a>, Richard Woodall, The Black Farmer, Red Lion Foods, <a href="http://www.reggae-reggae.co.uk/">Reggae Reggae</a>, <a href="http://www.simplysausages.co.uk/">Simply Sausages</a> y Yorkshire Baker.

• <a href="https://cranswick.plc.uk/">Cranswick Pork </a>es también quien fabrica famosos platos que son servidos en <a href="https://www.facebook.com/McDonaldsUK">McDonald's</a> como el McMuffin de salchicha y huevo. <a href="https://cranswick.plc.uk/">Cranswick </a>fue distinguido como proveedor del año por <a href="http://www.cranswick.plc.uk/plc_awards_recognition.html">McDonald's</a> el 4 de abril de 2011.

<strong>PARTICIPA</strong>

• ¡<a href="https://loveveg.mx/">Elige un estilo de vida vegano</a>!
La mayoría de nosotros pensamos que los animales no deben ser dañados si podemos evitarlo, sin embargo el consumo de productos animales impone a éstos una vida de miseria y sufrimiento que podríamos evitar fácilmente.

• <a href="https://igualdadanimal.mx/donar/">Por favor, considera hacer un donativo</a> para ayudar a Igualdad Animal a continuar con un trabajo de vital importancia en defensa de los animales.

• Puedes ayudar a que las imágenes tengan mayor repercusión publicando y compartiendo esta investigación en tu Facebook, Twitter o cuenta de Google +, subiendo nuestro vídeo a tu cuenta de YouTube o Vimeo, blog o sitio web.

• <a href="https://igualdadanimal.mx/protectores-de-animales/">¡Únete a nosotros! </a>Rellena el formulario para hacernos saber cómo te gustaría ayudar. Sé la voz de los sin voz.

<hr />
