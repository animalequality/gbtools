Nos fuimos al Congreso de Jalisco para mostrarle a todas y todos los jaliscienses quienes son las diputadas y diputados que apoyan la iniciativa <strong>#JaliscoSinCrueldad</strong> que presentamos el pasado 20 de febrero a través del diputado Salvador Caro Cabrera. Al día de hoy nuestra petición en <a href="https://www.change.org/p/firm%C3%A9-la-petici%C3%B3n-de-ia-mexico-porque-yo-tambi%C3%A9n-quiero-un-jaliscosincrueldad-t%C3%BA-tambi%C3%A9n-firma-y-demos-voz-a-los-animales-en-el-legislativojal?recruiter=694950671&amp;utm_source=share_petition&amp;utm_medium=copylink&amp;utm_campaign=share_petition">Change.Org</a> tiene más de 13 mil firmas y más de 400 personas han escrito a las Comisiones para que den lo antes posible un dictamen favorable.

¡Preparamos este video para ti!
<h4>Facebook</h4>
<p class="ae-video-container"><iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Figualdadanimalmexico%2Fvideos%2F2276351645750453%2F&amp;show_text=0&amp;width=560" width="1120" height="630" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe></p>
&nbsp;

&nbsp;

<center></center>&nbsp;
<h4><strong>Twitter</strong></h4>
<blockquote class="twitter-tweet" data-partner="tweetdeck">
<p dir="ltr" lang="es">¡Solo faltas tú! ¡Con tu apoyo lo lograremos!
🐷🐥🐔🐮🐶🐱
📣 Comparte RT
📸 Toma tu foto <a href="https://twitter.com/hashtag/JaliscoSinCrueldad?src=hash&amp;ref_src=twsrc%5Etfw">#JaliscoSinCrueldad</a>
🖊 Firma ---&gt; <a href="https://t.co/d14Az8Jt5b">https://t.co/d14Az8Jt5b</a> <a href="https://t.co/Iz3euzLXfq">pic.twitter.com/Iz3euzLXfq</a></p>
— Igualdad Animal Mex (@IA_Mexico) <a href="https://twitter.com/IA_Mexico/status/1108556332158717952?ref_src=twsrc%5Etfw">March 21, 2019</a></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

&nbsp;
<h3>&nbsp;</h3>
<h3>&nbsp;</h3>
<h3 style="text-align: center;">Te puede interesar: <a href="https://igualdadanimal.mx/noticia/2019/03/14/en-que-consiste-nuestra-iniciativa-jaliscosincrueldad/">Qué dice la iniciativa #JaliscoSinCrueldad</a></h3>
&nbsp;
<h4>32 de 38 diputados apoyan&nbsp;<strong>#JaliscoSinCrueldad:</strong></h4>
Caro Cabrera Salvador / Autor
Robles Villaseñor Mara Nadiezhda / Adhesion
Alcaraz Virgen Ma. Elizabeth / Adhesion
Cabrera Jiménez J. Jesús / Adhesion
Espanta Tejeda Ismael / Adhesion
Romo Mendoza Francisco Javier / Adhesion
Murguía Torres Claudia / Adhesion
Fernández Ramírez Mariana / Adhesion
González Arana Jorge Eduardo / Adhesion
González Fierros Adenawer / Adhesion
Franco Barba Priscilla / Adhesion
Sandoval García Ana Lidia / Adhesion
Cortés Berumen José Hernán / Adhesion
Munguía González Luis Ernesto / Adhesion
Hurtado Torres José de Jesús / Adhesion
Herrera Estrada Óscar Arturo / Adhesion
González Orozco Irma Verónica / Adhesion
Flores Gómez Mirza / Adhesion
Fregoso Franco Rosa Angélica / Adhesion
Sánchez Carrillo Carlos Eduardo / Adhesion
García Mosqueda Sofía Berenice / Adhesion
Velázquez Chávez Gerardo Quirino / Adhesion
Robles de León Daniel / Adhesion
Estrada Ramírez Esteban / Adhesion
Martínez García Jonadab / Adhesion
Pizano Ramos Héctor / Adhesion
Rivera Rodríguez Miriam Berenice / Adhesion
Rodríguez Jiménez Ricardo / Adhesion
Velázquez González Edgar Enrique / Adhesion
De Anda Licea Irma / Adhesion
Macías Zambrano Gustavo / Adhesion
Zúñiga Mendoza J. Jesús / Adhesion
