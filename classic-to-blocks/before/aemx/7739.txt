Como parte de las acciones para detener los abusos documentados por la organización internacional Igualdad Animal en los Rastros de México, la senadora Diva Gastélum, apoyada por las senadoras Cristina Díaz, Hilda Flores, Lilia Merodio, Itzel Ríos, Anabel Acosta y María Elena Barrera, presentó en Sesión Plenaria del Senado una iniciativa propuesta por la organización que contempla una modificación a la Ley Federal de Sanidad Animal y otra en el Código Penal Federal de México para la tipificación del delito de maltrato y crueldad animal que incluya a los animales de granja.

<img class="wp-image-11841" style="width: 400px; margin: 5px 10px; float: left;" src="/app/uploads/2017/03/igualdad_animal_senadora_diva_gastelum.jpg" alt="" />

La propuesta es apoyada por los siguientes grupos y asociaciones de protección animal en México; Vegan Outreach, Justicia y Dignidad Animal, AC., Proyecto Gran Simio, Animal Sonoro, Movimiento Ecológico por los Derechos Animales, Proyecto ARPA, Xalapa por la Vida Digna, A.C., Naturaleza Animal, A.C., Comunidad Gaia, Callejeritos de los Tuxtlas A.C. y Apasdem, A.C., una coalición integrada por 92 grupos que piden que el incumplimiento de las normas oficiales para operar los rastros del país sea un delito, sustentado en que las violaciones a estas normativas suponen la peor tortura y crueldad hacia los animales de granja.

La senadora Diva Gastélum asegura que “Estamos buscando instaurar sanciones dentro de la Ley Federal de Sanidad Animal y el Código Penal Federal para que quien maltrate o trate de manera cruel a los animales destinados para consumo humano evitar las situaciones como las que han sido documentadas por Igualdad Animal y siguen sucediendo y quedan totalmente impunes.”

Como se puede ver en la investigación realizada en 31 rastros municipales del país por la agrupación dedicada a prevenir la crueldad contra los animales, el incumplimiento de las normas deriva en que sean desollados vivos, golpeados, electrocutados, quemados y matados estando plenamente conscientes sin aturdimiento previo.

A decir de la directora ejecutiva de la organización, Dulce Ramírez, “la existencia de Normas Oficiales Mexicanas redactadas y autorizadas por la propia SAGARPA no sirven de nada si no se puede asegurar que se cumplan. Podemos ver que las sanciones actuales no son suficientes para lograrlo, es por eso que debemos endurecer la ley y convertir el incumplimiento en delito”.

Además agregó “confiamos que a través de la Senadora Gastélum y los miles de mexicanos que han firmado nuestra petición, el Senado muestre voluntad política para cambiar esta terrible situación. Es una forma de dejar claro que las Normas Oficiales Mexicanas son sólo para satisfacer convenios, acuerdos y exhortos internacionales en papel, si no para hacer crecer a México, en este caso, en materia de protección animal.”

Si tu también quieres que el maltrato y la crueldad animal sean un delito y que los animales de granja sean protegidos de terribles atrocidades por favor firma la petición que encontrarás en <a href="https://rastrosdemexico.igualdadanimal.mx/">RastrosDeMexico.com</a> para pedirle a los senadores que aprueben cuanto antes la medida.

<iframe src="https://www.youtube.com/embed/hxv7JTEgmjs" width="885" height="498" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
