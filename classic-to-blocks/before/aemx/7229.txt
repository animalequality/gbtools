<div class="center">&nbsp;</div>
<img class="wp-image-13583" style="float: right; margin-left: 5px; margin-right: 5px; width: 450px;" src="/app/uploads/2015/02/animalitos.jpg" alt="">La Procuraduría Federal de Protección al Ambiente (Profepa) ha decomisado un total de 101 animales a la Unidad de Manejo para la Conservación de la Vida Silvestre (UMA) denominada &nbsp;“Club de los Animalitos”, por hacinamiento y faltas al trato digno.

El aseguramiento se realizó tras una inspección efectuada a la UMA &nbsp;ubicada en Tehuacán, Puebla, y cuyo propietario es el Diputado de Acción Nacional (PAN), Sergio Emilio Gómez Olivier.

El personal de la Profepa acudió a las instalaciones de esta Unidad &nbsp;de Manejo con el propósito de verificar con los responsables de ese zoológico, las condiciones de confinamiento y la legal procedencia de los ejemplares, el registro de la UMA, así como el Plan de manejo &nbsp;de especies y su cumplimiento.

El pasado 29 de enero, la organización internacional Igualdad Animal (IA), con sede en México, <a href="https://igualdadanimal.mx/noticia/2015/02/05/asi-malviven-los-animales-en-el-zoo-del-diputado-sergio-gomez-olivier/">documentó con fotografías</a> y datos sobre la situación de más de 400 animales que habitan en el zoológico “El Club de los animalitos”.

En esa ocasión, el médico veterinario Carlos Eguibar Zenteno, dijo que el zoológico se deslindaba de cualquier comentario, ya que es el Diputado del PAN quien maneja toda esa información por tratarse de un asunto “político”.

“Tenemos 30 años de experiencia trabajando en zoológicos, no hay nada como maltrato animal, entonces es totalmente incierto. Como es año electoral, es un ataque político, por consiguiente los animales y nosotros no tenemos nada que ver”, dijo Eguibar Zenteno.

La Subprocuraduría de Recursos Naturales de la Profepa buscará canalizarlos &nbsp;a lugares que cuenten &nbsp;con las condiciones adecuadas para su recuperación, resguardo y manutención.
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/DeJXTkk2oO4" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<a title="Así malviven los animales en el zoo del diputado Sergio Gómez Olivier" href="https://www.flickr.com/photos/igualdadanimal/albums/72157650244456157" data-flickr-embed="true"><img src="https://farm8.staticflickr.com/7437/16425522506_5a0a2dcf06_z.jpg" alt="Así malviven los animales en el zoo del diputado Sergio Gómez Olivier" width="640" height="640"></a><script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
