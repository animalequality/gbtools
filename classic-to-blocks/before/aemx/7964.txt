<div class="center">&nbsp;</div>
La organización internacional Igualdad Animal ha presentado una &nbsp;nueva investigación para hacer público <strong>el maltrato y sufrimiento que ocurre en las granjas de cerdos del Reino Unido</strong>.

Entre septiembre y octubre de 2017 los investigadores de Igualdad Animal realizaron varia visitas a granjas de cerdos en Norfolk, Devon y el este de Yorkshire.

Los hallazgos han revelado:

▪ Hembras encerradas en pequeñas jaulas individuales (de gestación) durante días enteros sin recibir ninguna atención.

▪ Cerdos que viven constantemente encerrados por más de 5 días.

▪ Animales mordiendo los barrotes de las jaulas y agrediéndose debido al estrés provocado por el hacinamiento.

▪ Cerdos con grandes abscesos en la espalda, heridas abiertas en las orejas y heridas infectadas en las patas.

▪ Muchos cerdos con graves heridas y uno con un grave cojera permanecían abandonados.

▪ Las condiciones en que viven los animales les niegan cualquier comportamiento natural en ellos.

▪ Cerdos viviendo en instalaciones en ruinas, con goteras y cubiertas de estiércol, obligados a tumbarse sobre sus propios desechos.

▪ En todas las granjas investigadas a los cerdos se les niega el acceso a entretenimiento para prevenir el aburrimiento y el estrés.

▪ Todos los cerdos de todas las granjas investigadas tenían la cola cortada.
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/Pi1zkgBT9yE?rel=0" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
Los cerdos son <strong>los animales de granja más protegidos por las leyes en el Reino Unido</strong>. La legislación prohíbe que sean confinados en jaulas y jaulas de gestación, las mutilaciones como el corte de la cola y la privación del entretenimiento y espacio necesarios para poder comportarse de manera natural.

Sin embargo, la investigación de Igualdad Animal demostró que, <strong>a pesar de estar prohibidas, estas prácticas se siguen llevando a cabo en las granjas del país</strong>. Esto ocurre, incluso, en el caso de Cross Farm en Devon, una de las granjas investigadas y que fue premiada en 2016 y 2017 con el National Pig Award.

Dos de las tres granjas investigadas, Hall Farm y Poplar Far, ya habían sido expuestas públicamente por sus negligentes prácticas de bienestar animal en 2016 y 2015, respectivamente.

<img class="wp-image-12630" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px;" src="/app/uploads/2017/11/cerdasjaulasgestacion.jpg" alt="">

Toni Shephard, directora de Igualdad Animal en Reino unido declaró: «Los consumidores siempre dicen que hay que comprar a productores locales debido a los estándares de calidad y bienestar, sin embargo, las leyes sobre el bienestar de los cerdos (algunas con casi 20 años de antigüedad) son ignoradas a diario y ni el gobierno, ni los productores ni Red Tractor hacen lo necesario para detenerlo».

El catedrático de la Universidad de Ciencias de la Agricultura en Suecia Bo Algers, experto de la Unión Europea en leyes de bienestar para los cerdos, concluyó tras revisar el material de las 3 granjas investigadas:

«Los cerdos que se ven aquí no están siendo tratados según las leyes actuales de bienestar animal. Los cerdos en estas condiciones no tienen un nivel de bienestar aceptable».

Igualdad Animal ha enviado un dossier con el material grabado, fotografías y evidencias escritas a Animal and Plant Health Agency, departamento del gobierno británico que investiga las violaciones de las leyes de bienestar en granjas. La organización también ha lanzado una campaña pidiendo que se tomen medidas inmediatas a los responsables de proteger a los cerdos, desde el gobierno, las empresas y la organización de control de calidad de los alimentos Red Tractor.

<strong>Te podría interesar: <a href="https://igualdadanimal.mx/noticia/2018/08/27/impactante-filmamos-trabajadores-golpeando-y-alimentando-brutalmente-y-la-fuerza/">IMPACTANTE: filmamos a trabajadores golpeando y alimentando brutalmente y a la fuerza a becerros recién nacidos</a>&nbsp;</strong>
