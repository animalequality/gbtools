<div class="center"></div>
En diciembre de 2017, una vaca llamada Hermien <strong>logró escapar instantes antes de que la subieran al camión que la transportaría al matadero</strong>. Desde entonces, a pesar de los múltiples intentos, no han podido capturarla.

Desde su escape, Hermien se esconde en el bosque de Overijssel en Holanda y, en algunas oportunidades, se acerca a otras vacas de otras granjas para conseguir alimento. <strong>Su historia se ha hecho viral por haber logrado escapar de su trágico destino</strong> y también ha recibido miles de dólares en donaciones para que pueda llevar la vida que merece.
<h4>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/un-granjero-salva-todas-las-vacas-de-su-granja-enviandolas-un-refugio-en-lugar-del/">Un granjero salva a todas las vacas de su granja enviándolas a un refugio en lugar del matadero</a></h4>
<img class="wp-image-12836" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/02/shutterstock_678451807.jpg" alt="">

Muchos desconocen que las vacas pueden llegar a vivir 15 años en lugar de los 5 años que le permite la industria láctea antes de llevarlas al matadero y que son animales sociales y dotados de inteligencia. <strong>Pueden recordar acontecimientos durante un largo periodo de tiempo y tienen una gran memoria espacial</strong> [1] que les permite recordar rutas migratorias, lugares donde encontrar agua o las mejores zonas de pasto así como dónde dejaron a su ternero mientras pastaban.

El dueño de la granja donde vivía Hermien ha desistido en sus intentos por capturarla para no correr el riesgo de que se adentre más en el bosque y sea imposible sacarla de allí. Su plan es esperar que el clima mejore, poner más vacas a pastar cerca de ella y así lograr su confianza para que regrese junto al grupo.

<span style="font-size: x-large;"><span style="color: #808080;"><a href="https://igualdadanimal.mx/suscripcion/" target="_blank" rel="noopener noreferrer">Suscríbete gratuitamente a nuestro boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</span></span>

Es debido a esto último que miles de personas han creado una campaña que se ha vuelto viral, utilizando los hashtag #AyudenAHermien y #LiberenAHermien en Twitter para recolectar dinero para ayudar a Hermien. <strong>Todo el dinero que se recaude será destinado a lograr que ella pueda llevar una vida tranquila hasta su muerte natural en el refugio «De Leemweg»</strong>. Este lugar tan especial se sostiene únicamente gracias a las donaciones y se dedica a rescatar vacas que han logrado escapar del matadero.
