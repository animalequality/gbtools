Este jueves 20 de octubre a las 11:00 horas en la sala audiovisual de La Casa del Tiempo en Cuidad de México, la organización internacional Igualdad Animal lanzó en rueda de prensa <a href="https://ianimal.es/" target="_blank" rel="noopener noreferrer">iAnimal</a>, su proyecto de realidad virtual <strong>presentado y narrado por el famoso presentador de televisión Marco Antonio Regil</strong>, que "transporta" a los espectadores a granjas y mataderos industriales viviendo la experiencia a través de los ojos de un animal.

<img class="wp-image-11174" style="float: left; margin: 10px; width: 650px;" src="/app/uploads/2016/10/d648cdbf-af11-43ca-a171-e11ed55cf6a6.jpg" alt="" />

Entre los presentadores y narradores del corto documental para otros países se incluyen <strong>Tony Kanal</strong> de No Doubt (EE. UU.); el actor conocido por su papel en Downton Abbey, <strong>Peter Egan</strong> (Reino Unido); el rapero del grupo alemán de hip hop <strong>Die Fantastischen Vier</strong>, <strong>Thomas D</strong> (Alemania); y la actriz y cantante <strong>Angy Fernández</strong> (España).

Los animales explotados por la industria cárnica sufren desde el momento en el que nacen hasta que llegan al matadero. <strong>iAnimal permite al espectador acceder al maltrato diario que la industria ganadera oculta al público</strong>.

<iframe src="https://www.youtube.com/embed/BF8aiSZU6AQ" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Este proyecto incluye imágenes obtenidas durante <strong>más de 18 meses</strong> de investigaciones en granjas industriales y mataderos de España, México, Reino Unido, Alemania e Italia.

En la presentación los periodistas y personas del público se adentrarron en la realidad virtual iAnimal y conocieron de viva voz la experiencia de Marco Antonio Regil.

<img class="wp-image-11175" style="float: left; margin: 10px; width: 650px;" src="/app/uploads/2016/10/30453834885_a4aeea914a_k.jpg" alt="" />

Buscando generar debate respecto al maltrato animal dentro de la ganadería industrial, Igualdad Animal ha llevado su proyecto iAnimal a personas famosas y de gran talento, entre las cuales hay actores como <strong>Jon Mack</strong>,<strong> Diego Luna</strong> y <strong>Peter Egan</strong>; y músicos como <strong>Brian May</strong>, guitarrista de Queen, <strong>Dawn Richard</strong> de Danity Kane y <strong>Tony Kanal</strong> de No Doubt; periodistas del <strong>New York Times</strong>, <strong>The Guardian</strong> y otros periódicos; productores, directores y muchas personas del mundo de la tecnología han expresado su admiración por este proyecto.

<span style="font-size: x-large;"><span style="color: #808080;">"He visto muchos videos de crueldad animal pero esta experiencia es diferente porque estás desde el punto de vista del animal. Es muy triste verlo, pero es bueno verlo porque está sucediendo y si no lo veo, no me entero y si no me entero, no puedo hacer nada al respecto. Yo no quiero ser parte de esta crueldad ni hoy ni nunca".
</span>
<span style="font-size: small;"><span style="color: #808080;">- Marco Antonio Regil, presentador de televisión. </span></span></span>

<span style="font-size: x-large;"><span style="color: #808080;">"... algo tremendamente poderoso llega a través de la realidad virtual. La increíble cercanía visual provoca una sintonía emocional y, de este modo, se puede sentir la verdadera magnitud de la crueldad a la que se somete a cada animal".
</span>
<span style="font-size: small;"><span style="color: #808080;">- Barbara J. King, NPR.</span></span></span>

<span style="font-size: x-large;"><span style="color: #808080;">"La realidad virtual te traslada al lugar que la industria intenta mantener en secreto".
</span>
<span style="font-size: small;"><span style="color: #808080;">- Jane Velez-Mitchell, presentadora de televisión y escritora.</span></span></span>

<span style="font-size: x-large;"><span style="color: #808080;">"Por primera vez en nuestro país mostramos la crueldad de la industria cárnica como nunca antes se había hecho, con imágenes obtenidas en granjas y rastros de México la experiencia inmersiva en 360 no dejará indiferente a nadie"</span></span>

- Dulce Ramírez, directora ejecutiva de Igualdad Animal México.

Puedes ver el álbum de fotos de la presentación <a href="http://bit.ly/2el4kcz" target="_blank" rel="noopener noreferrer">haz clic aquí</a>.

<iframe src="https://www.youtube.com/embed/q_4RE4VM37w" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
