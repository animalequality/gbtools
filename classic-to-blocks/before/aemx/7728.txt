<div class="center"></div>
La organización internacional Igualdad Animal ha revelado <strong><a href="https://industriadelaleche.igualdadanimal.mx/">imágenes nunca vistas</a></strong> de las prácticas estándar en la producción de leche en México.

Ninguna otra organización de defensa de los animales había sacado a la luz una investigación de la industria láctea mexicana.

Lo que muestran en un <a href="https://www.youtube.com/watch?v=t8VNYMfI5L4"><strong>impactante vídeo</strong></a> es descrito como un interminable ciclo de crueldad que incluye:

• El nacimiento de crías y la separación nada más nacer para evitar que éstas consuman la leche de su madre.

• El encierro de los recién nacidos en jaulas.

• La dolorosa práctica conocida como el «descorne» que en México se realiza en vacas adultas, lo cual está prohibido en otros países.

• El corte de cola, también prohibido en otros países.

• El marcaje con hierros ardiendo sin ningún tipo de anestesia que provocan profundas quemaduras en los animales.

• La ingesta de imanes para recoger los metales en el estómago de las vacas por el consumo de comida contaminada.

• Lesiones, infecciones y sangrado de las ubres provocados por la extracción industrial de leche.

• La negligencia en tratamientos veterinarios para las heridas e infecciones en ojos u otras partes que no afectan la producción.

• El fin de las vacas con baja producción en los mataderos de México.

<img class="wp-image-11803 alignnone" src="/app/uploads/2017/03/vacas.jpg" alt="" />

Dulce Ramírez, directora ejecutiva de la organización en México, declara «Es momento de fortalecer la legislación en México para prevenir estos abusos hacia los animales. La crueldad animal debe ser delito».

Y añade «Los animales usados en la ganadería industrial son tratados como máquinas, golpeados, electrocutados, apuñalados brutalmente y confinados en sucias jaulas, ignorando por completo su sufrimiento».

Este informe demuestra que México se encuentra muy rezagado en leyes que protejan a los animales de granja.

Con esta nueva investigación Igualdad Animal pretende reforzar la petición que presentó en Ciudad de México en diciembre de 2016 al Gobierno Federal de México y al Senado de la República para modificar la legislación federal y convertir en delito la crueldad contra los animales de granja.

<strong><a href="https://industriadelaleche.igualdadanimal.mx/">Firma la petición ahora</a></strong>.
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/t8VNYMfI5L4" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
