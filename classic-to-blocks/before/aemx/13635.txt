La empresa SuKarne,<strong> la mayor productora de carne de México y Latinoamérica, ha dejado sin agua al poblado de Lucero</strong> por abastecer a las 300.000 vacas y becerros que cría.

Los habitantes de Lucero y otros poblados aledaños temen que la empresa acabe con el vital líquido en toda el área de Laguna. Actualmente sufren de sed porque la empresa ha secado todos los mantos acuíferos de la zona.

Según un productor local, los animales que cría la empresa son <strong>un peligro para la alimentación de millones de mexicanos</strong> porque los alimentan con un líquido que contiene grandes cantidades de arsénico.

<img class="wp-image-12339 aligncenter" src="/app/uploads/2017/08/granja_mexico.jpg">

Científicos y veterinarios coinciden en que el gas metano que producen las reses con sus excrementos y flatulencias son <strong>altamente contaminantes y perjudiciales para la población.</strong>

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2019/03/04/la-insostenible-y-destructora-huella-de-la-carne/">La insostenible y destructora huella de la carne</a></strong>

En un principio, Sukarne no pudo funcionar en Monterrey, Nuevo León. Les negaron los permisos al conocerse sus informes de contaminación y la cantidad de agua necesaria para los animales.

Se presume que su actual ubicación en el estado Durango se deba a que la administración estatal recibió dinero y aprobó la instalación de la planta ignorando dichos informes.

Los habitantes de Lucero han solicitado tanto al presidente Peña Nieto como al gobernador de Durango, José Rosas Aispuro, una investigación a fondo sobre esto.

Recientemente, la organización internacional Igualdad Animal denunció que en Cataluña, España, <strong>la contaminación por nitratos procedentes de purines</strong> supera el límite legal en el 41 % de los acuíferos y 142 municipios tienen problemas de acceso al agua potable.
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/RvTn6s6Yu6c" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
