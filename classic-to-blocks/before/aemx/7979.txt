<div class="center"></div>
En 2016 investigadores de Igualdad Animal revelaron la crueldad hacia los animales en los rastros de México. Luego de un año, <strong>se infiltraron por segunda vez y lo que descubrieron, inclusive para ellos que suelen presenciar escenas de insólita crueldad, fue terrible</strong>.
<div>Durante los meses de julio y agosto del 2017, los investigadores de Igualdad Animal obtuvieron <strong>nueva evidencia del incumplimineto e impunidad con la que operan los rastros municipales</strong>, lo que deriva en actos de crueldad hacia los animales.</div>
Los hallazgos muestran a cerdos y vacas desollados vivos, más de 14 minutos de agonía después del apuñalamiento, violencia sistemática en la movilización y manejo de animales y menores de edad presenciando la matanza. Un claro incumplimiento a las normas mexicanas.
<h4>Nota relacionada:&nbsp;<a href="https://igualdadanimal.mx/noticia/2017/08/16/de-ultima-hora-igualdad-animal-impulsa-en-jalisco-iniciativa-de-ley-para-que-la/">Igualdad Animal impulsa en Jalisco iniciativa de ley para que la crueldad contra los animales de granja sea un delito</a></h4>
<address>&nbsp;</address>Esta nueva investigación fue presentada en rueda de prensa en el Congreso de Jalisco por Dulce Ramírez, directora de la organización en México, el diputado Alejandro Hermosillo y la diputada Fela Pelayo.

<img class="wp-image-12663" style="width: 800px; " src="/app/uploads/2017/11/dsc01866_1.jpg" alt="">

Al respecto el diputado Alejandro Hermosillo comentó “Igualdad Animal ha hecho un trabajo excepcional, este no es un tema de fracciones partidistas y de algunos diputados en específico sino con todos, lamentamos que hoy no estén aquí más diputados y diputadas, invitamos también desde nuestra parte al Congreso a que atendamos este llamado de la sociedad civil”.

Igualdad Animal <strong>presenta esta nueva evidencia con el objetivo de hacer un llamado urgente al Congreso de Jalisco para que apruebe la iniciativa de reforma a los artículos 306 y 308 del Código Penal del Estado de Jalisco </strong>para asegurar que los rastros que operan en el Estado cumplan el marco normativo federal presentada por el Diputado Alejandro Hermosillo. Esta iniciativa tiene el respaldo de los integrantes de la Comisión de Medio Ambiente y Desarrollo Sustentable, quienes ya aprobaron un dictamen favorable.
<h4>Nota relacionada:&nbsp;<a href="https://igualdadanimal.mx/blog/5-razones-para-apoyar-la-iniciativa-contra-la-crueldad-en-rastros-de-jalisco/">5 razones para apoyar la iniciativa contra la crueldad en rastros de Jalisco</a></h4>
<address>&nbsp;</address>Por su parte la diputada Fela Pelayo enfatizó, “este tema no tendría por qué estarse discutiendo ni mucho menos rechanzándose en el Congreso de Jalisco, es lamentable, es penoso formar parte de algo que no respeta un derecho, por eso exigimos al resto de los diputados que se sumen a esta iniciativa, que la descongelen, que la saquen ya y que lo antes posible se estén aplicando las normas que tenemos en nuestro país".

“Seguimos sin poder creer que tras todas las evidencias y denuncias que hemos presentado, existan todavía legisladores que no sólo son indiferentes al tema sino que se oponen a que esta iniciativa se apruebe. Es inconcebible que Jalisco permita esta impunidad”, Dulce Ramírez, directora de Igualdad Animal en México.

De aprobarse esta iniciativa, 187.5 millones de animales por año que hoy se encuentran desamparados quedarán legalmente protegidos en la entidad. <strong>La organización convoca a todas y todos los jaliscienses a firmar la petición en <a href="https://rastrosdemexico.igualdadanimal.mx/">RastrosDeMexico.com</a></strong>

<iframe src="https://www.youtube.com/embed/MsapbJLrhr4" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
