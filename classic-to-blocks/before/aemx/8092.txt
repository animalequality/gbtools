No hay duda alguna que es dentro de la ganadería industrial donde tiene lugar <strong>el mayor maltrato animal conocido en la historia.</strong> Si alguien hiciera a nuestros perros o gatos lo que se les hace a los animales dentro de los granjas y mataderos, acabaría en la cárcel. Lo que pasa es que los animales que son víctimas de la ganadería industrial tienen la mala suerte de ser pollos, peces, gallinas, vacas, borregos, cerdos y corderos, entre otros.

Una de las investigaciones más impactantes realizadas por Igualdad Animal fue aquella que nos llevó al interior del horror de las incubadoras industriales en España, donde los pollitos son sometidos a las peores vejaciones en su primer día de vida.

<img class="wp-image-13046" src="/app/uploads/2018/04/22757853629_1baf671519_z_2.jpg">

<strong>Te interesará: &nbsp;<a href="https://pollohechoenmexico.igualdadanimal.mx">Nueva investigación Pollo Hecho en México</a></strong>

En el video de la investigación se ve cómo desde que nacen hasta que son llevados a las granjas de engorde un día después, los pollos <strong>son tratados como mercancías.</strong> Apenas saliendo del cascarón son tirados y arrastrados vivos dentro de contenedores donde muchos se asfixian y mueren, y también son aplastados con mazas si están enfermos o son inservibles.

<iframe src="https://www.youtube.com/embed/VGKQKGOyPtU" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Para la producción de carne de pollo se utilizan pollos de raza «broiler», hibridados y seleccionados genéticamente para aumentar de peso en poco tiempo. Aquellos considerados como «válidos» para ser llevados a las granjas de engorde, son forzados a crecer tan rápidamente que <strong>si un bebé engordase al ritmo al que lo hace un pollo, a los 2 meses pesaría 300 kilos. </strong>Debido a esto, para muchos caminar implica un dolor insoportable y otros sufren de parálisis y mueren por no poder alcanzar el agua y alimento luego de agonizar por días.

Los que sobreviven a este infierno <strong>son llevados a los 40 días de edad al matadero.</strong> Son manipulados con violencia al ser montados en los camiones y a muchos de ellos les rompen las patas y alas, según denuncia el vídeo, grabado en incubadoras de Lleida, Girona, Tarragona y Burgos.

Según datos del Ministerio de Agricultura, Alimentación y Medioambiente, en España se sacrifican más de 600 millones de pollos al año. Tan sólo en México cada año son matados más de 1700 millones de pollos.

El director internacional de Igualdad Animal, Javier Moreno, explicó que «Los pollos son los animales más maltratados por la industria cárnica, cada día más de un millón y medio son matados en España. Una vida que es un infierno desde el primer segundo».

La forma más efectiva de ayudar estos animales es reduciendo significativamente nuestro consumo de pollo o consumiendo en su lugar alternativas vegetales. Afortunadamente, cada vez son más los alimentos disponibles para sustituir el pollo y otras carnes en s<a href="https://loveveg.mx/supermercados-con-alimentos-veggie-en-mexico/">upermercados de México</a>&nbsp; y, además, cuentas con miles de <a href="https://www.youtube.com/channel/UCXKvyvMX3CrJkwcIgbtk7dg">deliciosas y fáciles recetas</a>.
