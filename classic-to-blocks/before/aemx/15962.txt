Tras 5 meses de solicitudes, Jerusa Rodríguez, senadora de la República por el partido Movimiento de Regeneración Nacional (Morena), logró que en el Senado de la República comiencen a ofrecer opciones para quienes siguen una alimentación vegana. La petición fue hecha para que se tuviera en cuenta que tanto ella como sus colaboradores siguen una alimentación vegana ya que el menú del recinto legislativo no ofrecía este tipo de opción.

La legisladora <strong>agradeció en un video a través de su cuenta de twitter el apoyo con el contó</strong> dentro del Senado para hacer realidad su petición con las siguientes palabras: “Quiero agradecer a todo el equipo que sirve la comida para los senadores porque, aunque nos tardamos cinco meses, ya hay opción vegana para los senadores y queremos que haya opción vegana en todos los restaurante en el Senado. Si no, ¿qué hace mi equipo, por ejemplo, que son veganos? ¿O cualquier otro que sea vegano? Así que muchas gracias, por fin comenzamos”.

[embed]https://twitter.com/jesusardgz/status/1121506190637305856[/embed]

Te podría interesar: <a href="https://igualdadanimal.mx/blog/presentamos-love-veg-mexico-para-promover-una-alimentacion-sana-y-sostenible/">Presentamos Love Veg México para promover una alimentación sana y sostenible</a>

Fuente: https://www.sdpnoticias.com/nacional/2019/04/25/jesusa-rodriguez-logra-que-el-senado-sirva-platillos-veganos
