<div class="center"></div>
A muchos de nosotros <strong>nos importan los animales y ya hemos comenzado a reducir nuestro consumo de carne </strong>para que vacas, pollos, cerdos, corderos y terneros queden fuera de nuestros platos.

Pero debido a que nuestros hogares pertenecen a medios tan diferentes <strong>conocemos muy poco sobre los peces</strong> y sobre el sufrimiento al cual se les somete para ser convertidos en comida.

<img class="wp-image-12393" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2017/09/shutterstock_497414386.jpg" alt="">

Los peces <strong>son animales tan sensibles como cualquier ave o mamífero</strong>, inteligentes y que forman complejas sociedades. Su capacidad de sentir dolor es igual y mayor que la de ellos. Su deseo de vivir también.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/6-practicas-crueles-que-la-pesca-industrial-y-las-piscifactorias-prefieren-que-no/">6 prácticas crueles que la pesca industrial y las piscifactorías prefieren que no conozcas</a></strong>

La pesca industrial avanza en sus enormes barcos y <strong>mata cada año a más peces que toda la industria ganadera junta</strong>. También los somete a la mayor crueldad y maltrato imaginables porque sencillamente <strong>no existe ley alguna que regule el bienestar de los peces </strong>y lo impida.

A los peces se les revientan sus órganos internos cuando son sacados del mar. Muchos <strong>mueren asfixiados y aplastados</strong> dentro de las gigantescas redes y en la piscifactorías (granjas de peces) los métodos de matanza son crueles: electrocución, asfixia o golpes.

La mayoría de las personas que consumen peces desconocen que las piscifactorías existen y que en ellas son criados cada año <strong>120.000 millones de peces</strong>. Y también ignoran que anualmente <strong>la pesca industrial captura globalmente a un número de peces equivalente a la población humana de 142 planetas Tierra.</strong>

<img class="wp-image-12394" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2017/09/shutterstock_630563330.jpg" alt="">

La solución a toda esta crueldad y violencia depende enteramente de cada uno de nosotros. Dejar de comer peces sería algo poco o para nada complicado si consideramos que para los peces esa decisión significa, nada más y nada menos que, la posibilidad de vivir.

Y es que aunque no podemos escuchar sus gritos, si pudiéramos seguramente entenderíamos que dicen: por favor, ¡sácame de tú plato!

<span style="font-size: xx-small;"><span style="color: #808080;">Piscifactoría o granja de peces en el mar.</span></span>

<span style="font-size: xx-small;">&nbsp;</span>
