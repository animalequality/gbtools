<div class="center"><img class="wp-image-2342" src="/app/uploads/2015/05/15244041929_51077ebf9a_m.jpg" /></div>
<strong>WARNING SOME PHOTOGRAPHS MAY BE DEEMED AS GRAPHIC.</strong>

The photos below are from our own investigative work into the fur industry. The fur farms supply these animal skins to global manufacturers and shops worldwide, reaching countries throughout Europe like the UK.

We continue to document and expose the current conditions of animals on fur farms which are hidden away from the public. Fur should have been banished long ago but the fur industry is, once again, trying to re-integrate its cruel garments into society by targeting youth and fashion followers in an attempt to increase their sales. All fur products are drenched in pain and death. We believe it is important for consumers and citizens to know the reality which has been hidden for far too long. Below are 5 photographs which exemplify the fur industry, in no specific order as the living hell for each animal is equally distressing:

<strong>1. With no veterinary care, animals on fur farms are often left to die with severe infections and diseases.<img class="wp-image-8226" style="width: 640px;" src="/app/uploads/2015/05/4935596180_f5c3f5bf38_z.jpg" alt="" /></strong>
<em>Photo by Jonás Amadeo Lucas for Animal Equality at a Spanish mink farm. An image you won't see on a fur garment.</em>

&nbsp;

&nbsp;

&nbsp;

<strong>2. They say the eyes are the windows to the soul. What do these eyes tell you?
<img class="wp-image-8219" style="width: 640px;" src="/app/uploads/2015/05/15406705316_7da44a61ae_z.jpg" alt="" /></strong>
<em>Raccoon dog at a Chinese fur farm in obvious pain and agony with, once again, severe wounds and a lack of care.</em>

&nbsp;

&nbsp;

&nbsp;

<strong>3. Surrounded by blood, this rabbit is aware of what is going to happen next, as she sees her family in the background:</strong><em><img class="wp-image-8217" style="width: 640px;" src="/app/uploads/2015/05/14935801629_1fdb2d84bf_z.jpg" alt="" />
Taken by Jo-Anne Mc Arthur for Animal Equality at a Spanish-based slaughterhouse. Large fashion houses which prevail throughout Europe and the rest of the globe were linked to these images.</em>

&nbsp;

&nbsp;

&nbsp;

<strong>4. Born to die: their fate is in the hands of their abusers.</strong>
<img class="wp-image-8224" style="width: 640px;" src="/app/uploads/2015/05/4022112476_26ab45acfe_z.jpg" alt="" />
<em>A mother guards her babies which will soon be ripped away from her and turned into fur coats. Taken at a Spanish mink farm.</em>

&nbsp;

&nbsp;

&nbsp;

<strong>5. The ruthlessness of the fur industry at its peak: animals violently turned into products.
<img class="wp-image-8218" style="width: 640px;" src="/app/uploads/2015/05/15243284078_ab182f28c7_z.jpg" alt="" /></strong>
<em>Raccoon dogs skinned for their fur as their bodies are discarded, an intrinsic part of the industry. Taken in China.</em>
If you are moved by these images we have obtained, <strong>please support our work now</strong> so we can continue documenting the truth behind the fur industry. With your support, we will be able to campaign against this barbaric practice by funding further investigations into this hidden reality and show society the truth.

We need your support but most of all, animals need your support.
<a href="http://bit.ly/SupportUsTodayUSA">Give today and save lives.</a> They depend on you.

&nbsp;
