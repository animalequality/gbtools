On the afternoon of 15th of June, six activists from Animal Equality carried out two separate actions in protest against bullfighting at the Barcelona Bullring “Monumental”.

As Jose Valle and Tomás Eugenio García abseiled from the main Balcony on the outside of the building displaying a Banner which read “ Tauromaquia Abolición - Derechos Para Todos Los Animales” (Bullfighting – Abolition – Rights now for all animals) , four more activists (Yahira Vázquez, Marta Morera, Iván Guijarro y Javier Moreno) jumped into the arena with banners reading “Abolición” whilst the second bull used in the event lay suffering on the sand having been tortured for the crowd.

These four activists were violently attacked by dozens of bullfighting supporters as well as police officers and one of them, Ivan Guijarro, after being repeatedly clubbed around the head with sticks, was later taken to a Barcelona hospital where he remained for several hours.

From the hospital he was taken to the police station where he was under arrest with the three other activists who jumped into the arena and where Ivan Guijarro was forced to remain until 9:30 a.m. the following morning suffering, alongside other activists, threats from officers.

Dozens of protestors appeared at the entrance of the police station to show their support for the detained activists. From Animal Equality we offer our heartfelt thanks for this support.

All six activists are currently due to be charged over the actions.
