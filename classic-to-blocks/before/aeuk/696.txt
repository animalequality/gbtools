<div class="center"><img class="wp-image-2339" src="/app/uploads/2015/04/Chimps.jpg" /></div>
<p>A court has granted &ldquo;Habeas Corpus&rdquo; to two chimpanzees in the USA, legally recognising them as non-human persons. Hercules and Leo are two chimpanzees which have been used in experiments and live in captivity at the State University of New York.</p>

<p>For the first time in history, a court has awarded the &ldquo;Habeas Corpus&rdquo;, a legal form used for cases of people unlawfully deprived of their, to two chimpanzees in the USA, legally recognising them as non-human persons.&nbsp;Hercules and Leo are two chimpanzees used in experiments and live in captivity at the State University of New York, currently being &quot;owned&quot; by the &ldquo;New Iberia&rdquo; Center for Scientific Research in Louisiana and whose release is being demanded by the organisation NonHuman Rights Project.</p>

<p><img alt="" class="wp-image-8260" src="/app/uploads/2015/04/Screen-Shot-2015-04-28-at-21_22_48.png" style="width:570px" /><br />
<em>Photo credit:&nbsp;E_Monk/Flickr (CC BY-NC-ND)</em></p>

<p>&nbsp;</p>

<p>On April 20, Judge Barbara Jaffe on behalf of the Supreme Court of the State of New York, signed a &ldquo;Habeas Corpus&rdquo; for chimpanzees Hercules and Leo, stating that there are sufficient reasons so that the responsible people for the imprisonment of both of these chimpanzees explain to the Court the reasons for their captivity.</p>

<p>Nonhuman Rights Project calls for their release and that they be transferred to a chimpanzee sanctuary in Florida where they could spend the rest of their lives in one of the 13 artificial islands built on a large lake by &rdquo;Save the Chimps&quot; where they&rsquo;d live amongst another 250 chimpanzees.</p>

<p><img alt="" class="wp-image-8304" src="/app/uploads/2015/04/hanging-out-on-the-island.jpeg" style="width:570px" /></p>

<p><em>Photo credit: Save The Chimps</em><br />
<br />
Similarly, in December 2014, another concession of this law took place at the Palermo Zoo in Buenos Aires, where Sandra, an orangutan, is deprived from her freedom. It was initially rejected by the judge but then accepted by the Criminal Appeals Court of Argentina.</p>

<p>On behalf of Animal Equality, we celebrate this historic event in which a law like habeas corpus is granted to two chimpanzees, recognising their right not to be deprived from their freedom and we strongly desire that they&rsquo;re granted their immediate release.</p>

<p><br />
<br />
<em>Cover Photograph: Martin Meissner/AP</em></p>

