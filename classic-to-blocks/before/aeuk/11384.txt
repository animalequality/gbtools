<span style="font-weight: 400;">Extreme suffering, abuse and cannibalism has been revealed on British chicken farms supplying Nando’s, Lidl and Asda.</span>

<span style="font-weight: 400;">The harrowing footage, which was filmed on three Red Tractor-certified chicken farms in Northamptonshire, shows:</span>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Dozens of chickens collapsed under the weight of their unnaturally large bodies and unable to stand, many flapping frantically in a desperate attempt to lift themselves up</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Dead birds left to rot among the living, leading to cannibalism on at least one farm</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Workers callously breaking birds’ necks and leaving them to convulse amidst the flock</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Dying birds thrown onto a pile and left to suffer for hours as workers cleared the shed for slaughter</span></li>
 	<li style="font-weight: 400;">Workers purposefully stepping on and kicking birds</li>
</ul>
<div class="ae-video-container">

<iframe src="https://www.youtube.com/embed/mETGcLPT5x8" width="600" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

</div>
<span style="font-weight: 400;">Animal Equality investigators visited Evenley Farm, Helmdon Farm and Pimlico Farm multiple times between January and March 2019, after receiving a tip-off from a whistleblower concerned about poor conditions. On every single visit they found lame chickens who were unable to stand and dead birds left to rot inside the overcrowded sheds. </span>

<span style="font-weight: 400;">All three farms produce chickens for Faccenda, which is one of the UK’s largest chicken companies. Faccenda supplies major UK supermarkets and popular high-street restaurant chains including Nando’s, Lidl and Asda.</span>

<span style="font-weight: 400;">After discovering bin bags full of rotting dead chickens inside one shed at Evenley Farm, our investigators installed a hidden camera to record how long the carcasses were left among living birds. It filmed workers deliberately kicking and stepping on birds, as well as callously breaking chickens’ necks and leaving them to convulse amongst their flockmates. Multiple dying birds, clearly in pain, were carelessly thrown onto a pile and left to suffer there for hours.</span>

<span style="font-weight: 400;"><img class="alignnone  wp-image-11390" src="https://animalequality.org.uk/app/uploads/2019/04/faccenda_lamechicken_web.jpg" alt="British Chicken Farm - Animal Equality" width="848" height="565"></span>

<span style="font-weight: 400;">Animal Equality has passed all of the footage to the RSPCA, Red Tractor and Defra’s Animal and Plant Health Agency. </span>

<span style="font-weight: 400;">Sensationalist headlines about American chlorinated chickens may lead consumers to believe that British birds live a life of luxury, but this is far from the truth. In reality, chickens on British factory farms are tightly packed inside crowded sheds and grow to such an unnaturally large size that their joints and hearts can’t cope with the strain. They suffer every minute of their lives.</span>

<span style="font-weight: 400;">Labels and certification schemes like Red Tractor don’t prevent animals from suffering in the meat industry, but consumers can. Delicious, plant-based alternatives to meat are now widely available in shops and restaurants across the UK. <a href="https://loveveg.uk/">It’s never been easier to choose compassionate options and leave chickens off your plate!</a></span>
