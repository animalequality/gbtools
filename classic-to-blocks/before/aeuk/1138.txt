Actor Dev Patel, famous for his roles in Slumdog Millionaire and Skins, and Emmy-award winning Westworld star Thandie Newton, have added their voices to a campaign calling for a #FoieGrasFreeGB. They join over 100,000 Brits who have signed a petition demanding an import ban on this extremely cruel product post-Brexit.

Animal Equality has documented the suffering of ducks and geese on foie gras farms across France and Spain where birds are force-fed grotesque amounts of food to produce engorged fatty livers. This process is so cruel that it is illegal in the UK, yet nearly 200 tonnes of foie gras is still imported every year for shops such as Harrods and Fortnum &amp; Mason as well as opulent restaurants.

<img class="wp-image-8150" src="/app/uploads/2018/09/ThandieNewton_FoieGrasQuote_web.jpg" alt="">

&nbsp;

After viewing footage of birds being force-fed for foie gras, Thandie Newton said: “<em>I am pleased to join Animal Equality UK and the overwhelming majority who support a #FoieGrasFreeGB. The UK’s departure from the European Union allows the unique opportunity for Parliament to meaningfully depart from the grotesque foie gras industry. The law should reflect that if it’s too cruel to produce here, we shouldn’t welcome cruelty from elsewhere</em>.”

<img class="wp-image-3079" src="/app/uploads/2018/09/DevPatel_FoieGrasQuote_web.jpg" alt="">

&nbsp;

Dev Patel echoed her thoughts, saying: “<em>As so many undercover investigations reveal, foie gras production causes prolonged suffering to birds who are repeatedly force-fed by having metal rods shoved down their throats until their livers swell to more than 10 times their natural size. &nbsp;The process is so exceptionally cruel that foie gras production was made illegal in the UK, yet the sale and importation of it is still allowed. We refuse to tolerate this loophole.</em>”

Comedian Ricky Gervais, naturalists Chris Packham and Bill Oddie, and actors Evanna Lynch, Peter Egan, Joanna Lumley and Steven Berkoff have also spoken out in support of the campaign.

You can add your name to the petition at&nbsp;<a href="https://animalequality.org.uk/act/ban-force-feeding">animalequality.org.uk/foie-gras</a>.
