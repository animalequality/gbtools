<iframe src="https://www.youtube.com/embed/TUxg93ghBVA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<p style="margin-top: 30px;"><strong>Target: Lee Grant, Chief Executive of the RSPCA's Freedom Food and Jim Paice Minister of State for Agriculture and Food, DEFRA</strong></p>
Animal Equality has released undercover footage from Little Thorns Weaner Unit and Didlington, The Piggery, owned by East Anglian Pig Company. East Anglian Pig Company is a member of the UK farm assurance scheme and food labelling system, ‘Freedom Food’, audited and monitored by the RSPCA. They are also licensed by Red Tractor Farm Assurance scheme.

Animal Equality's undercover footage shows pigs being hit on the head with iron bars, kicked and slapped by workers, stereotypies caused by extreme confinement, pigs being thrown over gates by their back legs, several injuries such as broken legs, prolapses, deep wounds and hernias.

Leading experts have reviewed and supported the findings, such as veterinarian, Andrew Knight, Dr. Katherine van Ekert, President, The Veterinary Institute for Animal Ethics, Lorelei Wakefield, VMD, Founder of the University of Pennsylvania Veterinary Animal Welfare Society, and Professor John Sorenson, Chair of the Department of Sociology, Brock University, Canada. The RSPCA have stated that they will NOT be prosecuting, even though Lee Grant, chief executive of the RSPCA's Freedom Food, was quoted saying that he was <em>'absolutely disgusted'</em> by what he saw on the footage and that it was <em>'absolutely unacceptable'</em>.

We urge the RSPCA and DEFRA not only to investigate the farms exposed by Animal Equality, but to prosecute those who are responsible.

<!-- iframe to https://web.archive.org/web/20120430034316/http://www.changeforanimals.org.uk/prosecute-eap/ -->
