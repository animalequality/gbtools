With so many amazing cruelty free places to eat out in the UK, it’s been difficult to pick just a handful, but these are our top picks for a vegan Christmas lunch, with all the trimmings.

<a href="https://www.facebook.com/blackcathackney/">Black Cat Café, Hackney</a>

<a href="https://www.mildreds.co.uk/2016/11/25/mildreds-christmas-menu-update/">Mildred’s, London</a>

<a href="https://www.samphirebrasserie.com/">Samphire Brasserie, Plymouth</a>

<a href="http://www.vbites.com/news/">VBites, Brighton</a>

<a href="https://www.anna-loka.com/">Anna Loka, Cardiff</a>

You can find many more recommendations on Veganuary’s website at <a href="https://veganuary.com/eating-guides/">https://veganuary.com/eating-guides/</a>

With all of the tasty alternatives on offer, it is so easy to cook an incredible plant based Christmas lunch. Our favourite centre pieces include <a href="http://www.fryfamilyfood.com/uk/soy-quinoa-country-roast/">Fry’s Soy and Quinoa Country Roast</a>,  Tofurky’s Veggie Roast &amp; Wild Rice Stuffing and Tesco’s Meat Free Festive Nut Roast.

And for those of you up for a Christmas challenge here are our tried and tested, gut busting Christmas lunch recipes.

Starters

<a href="https://www.bbcgoodfood.com/recipes/chestnut-shallot-tatins-mushroom-madeira-sauce">Chestnut and shallot tatins with mushroom and Madeira sauce</a> (replace the butter with vegan butter to make vegan)
<a href="http://www.vegkitchen.com/recipes/special-occasions-and-entertaining/vegan-christmas/white-bean-and-sun-dried-tomato-pate/">White bean and sundried tomato pate</a>
<a href="https://www.jamieoliver.com/recipes/vegetables-recipes/puy-lentil-parsnip-walnut-salad/">Puy Lentil, parsnip and walnut salad</a>

Mains

<a href="https://www.jamieoliver.com/recipes/vegetables-recipes/whole-roasted-cauliflower/">Whole roasted cauliflower</a>
<a href="https://ohsheglows.com/2012/10/05/glazed-lentil-walnut-apple-loaf-revisited/">Glazed Lentil, Walnut and Apple Loaf</a>

Sides

<a href="https://www.bbcgoodfood.com/recipes/vegan-yorkshire-puddings">Yorkshire Puddings</a>
<a href="https://www.jamieoliver.com/recipes/vegetables-recipes/lemon-roast-potatoes/">Lemon roast potatoes</a>
<a href="https://www.bbcgoodfood.com/recipes/fruity-red-cabbage">Fruity red cabbage</a>
<a href="https://www.food.com/recipe/easy-vegetarian-gravy-226004">Gravy</a>

<a href="https://www.jamieoliver.com/recipes/vegetables-recipes/mixed-mushroom-stuffing/">Mixed mushroom stuffing</a>

Desserts

<a href="http://www.veggieful.com/2012/11/vegan-fruit-mince-pie-recipe.html">Mince pies</a>

<a href="https://www.vegansociety.com/resources/recipes/special-occasions/christmas-pudding">Christmas Pudding</a>

<a href="https://thevietvegan.com/vegan-pecan-pie/">Pecan pie</a>

<a href="https://www.jamieoliver.com/recipes/chocolate-recipes/vegan-chocolate-pots/">Chocolate pots</a>

And to make life even easier, our friends at Fry’s have produced a fabulous <a href="https://t.co/HbeVyY6DSu">Christmas Cookbook</a>, using their delicious products to create mouth-watering plant based recipes. We promise you won’t regret trying a cruelty free Christmas this year!

From all of us here at Animal Equality, have a very Merry Christmas and a Happy New Year!

P.S. If you want to make eating healthier and more compassionately one of your resolutions for 2017, why not try <a href="https://veganuary.com/">Veganuary</a> this January?
