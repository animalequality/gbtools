<div class="center"><img class="wp-image-2347" src="/app/uploads/2015/05/140bfbbd-9674-402b-af01-e239b09dd041-620x372_0.jpeg" /></div>
<h3>1. A…. pizza! from <a href="http://www.doughpizzakitchen.co.uk">Dough Pizza</a>, Manchester</h3>
<img class="wp-image-8247" style="width: 580px;" src="/app/uploads/2015/05/Dairy-Free-veg-pizza-DP.jpg" alt="" />
Ask for dairy free cheese!
<h3>2. Indian street food at <a href="https://bundobust.com/">Bundobus</a><a href="https://bundobust.com/">t</a>, Leeds
<img class="wp-image-8223" style="width: 580px;" src="/app/uploads/2015/05/3715.jpg" alt="" /></h3>
<h3>3. A full vegan breakfast at <a href="http://13thnote.co.uk">13th Note</a>, Glasgow</h3>
<img class="wp-image-8208" style="width: 580px;" src="/app/uploads/2015/05/10953634_1409112556050138_106792192_a.jpg" alt="" />
<h3>4. Anything from <a href="http://www.thespotlessleopard.co.uk">The Spotless Leopard</a>, Bristol</h3>
<img class="wp-image-8238" style="width: 580px;" src="/app/uploads/2015/05/CE4R8jyWAAAD7dY_jpg-large.jpeg" alt="" />
<h3>5. Amazing Jackfruit tacos from <a href="https://www.clubmexicana.com/">Club Mexicana</a>, London</h3>
<img class="wp-image-8216" style="width: 580px;" src="/app/uploads/2015/05/140bfbbd-9674-402b-af01-e239b09dd041-620x372.jpeg" alt="" />
<h3>6. Udon noodles with Thai spices and home-smoked tofu at <a href="http://www.davidbann.com">David Bann Vegetarian Restaurant</a>, Edinburgh</h3>
<img class="wp-image-8269" style="width: 580px;" src="/app/uploads/2015/05/Udon-Noodles.jpg" alt="" />
<h3>7. Sesame tofu don at <a href="http://www.pompoko.co.uk">Pompoko</a>, Brighton</h3>
<img class="wp-image-8222" style="width: 580px;" src="/app/uploads/2015/05/348s.jpg" alt="" />
<h3>8. Sunday vegan roast at <a href="http://alleycafe.co.uk">Alley Café</a>, Nottingham</h3>
<img class="wp-image-8314" style="width: 580px;" src="/app/uploads/2015/05/nutroast1.jpg" alt="" />
<em>Photograph: Anthony Blake Joff Lee/Anthony Blake Photo Library</em>
