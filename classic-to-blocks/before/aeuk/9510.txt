Animal Equality is honoured to have once again been selected as a Top Charity by Animal Charity Evaluators (ACE), an independent research non-profit dedicated to finding and promoting the most effective ways to help animals.

ACE analyses charities on the <strong>basis of their effectiveness, efficiency and real impact on animals</strong>, and this year, out of a total of 300 analysed, ACE has awarded four organisations the title of "Top Charity".

<img src="https://www.animalequality.net/sites/default/files/ace_2018_web.jpg" alt="" />

As part of its review, ACE specifically highlighted our ability to use our resources effectively, particularly in regard to undercover investigations, and to continually monitor our programs for their effectiveness and impact.

According to ACE:

<em>“We think that Animal Equality does an exceptional job with the resources they have. They are able to produce and market undercover investigation videos at a low cost relative to other organisations, and their efforts to evaluate and improve their work are strong.”</em>

ACE also focused on our strong international programs and collaborations with other organisations as particular strengths.

In their review, ACE states:

<em>“Animal Equality is firmly established in eight countries, and because the exploitation of nonhuman animals is a global problem, international work seems likely to be an essential part to a global solution. Animal Equality is also highly collaborative with other organisations and plans to continue working with other organisations. Collaboration and sharing resources across the animal advocacy movement may increase its impact overall.”</em>

Animal Equality is making a broad and significant impact on the lives of farmed animals around the world. You are a part of that! And because we are a Top Charity, you can rest assured that your donations help even more animals. <strong><a href="https://animalequality.org.uk/donate/">Donate today to continue our vital work into the next year.</a> </strong>Thank you!
