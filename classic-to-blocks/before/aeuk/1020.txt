<div class="center"><img class="wp-image-2852" src="/app/uploads/2017/06/foisgrasimg.jpg" /></div>
Foie gras is a ‘delicacy’ made from the diseased livers of ducks or geese who have been repeatedly force-fed, a process known as ‘gavage’.

Animal Equality has documented the suffering birds endure on more than a dozen Spanish and French farms, including untreated injuries, dead birds in cages with the living, conditions that prevent any natural behaviour and animals in constant and extreme pain.

<iframe src="https://www.youtube.com/embed/-UEOEmHFkRE" width="600" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

It is illegal to produce foie gras in Britain because of this extreme suffering. Nevertheless, we still import almost 200 tonnes of foie gras from mainland Europe every year for sale in shops and delis, such as Harrods and Fortnum &amp; Mason. And to be served in high-end restaurants.

The UK’s departure from the European Union, and associated freedom to change trade regulations, provides a unique opportunity for us to ban the importation of this cruel product. A <a href="http://bit.ly/2rVPZsT">YouGov poll </a>conducted at the beginning of June* showed that less than 10% of Brits consume foie gras and an overwhelming majority support an import ban - 77% of those who expressed an opinion.

If you love animals and know this suffering is wrong, add your name to our petition calling for a ban on imports of foie gras following Brexit, and share using #FoieGrasFreeGB. Sign and share now at www.animalequality.org.uk/foie-gras/

* All figures, unless otherwise stated, are from YouGov Plc.  Total sample size was 2,079 adults. Fieldwork was undertaken between  6th - 7th June 2017.  The survey was carried out online. The figures have been weighted and are representative of all UK adults (aged 18+).
