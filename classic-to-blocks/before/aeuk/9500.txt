<em>Tate Modern has confirmed it will remove foie gras from its upcoming Christmas menu after a campaign by Animal Equality.</em>

Last week, the actor and activist Peter Egan joined campaigners outside the Tate Modern in central London, to demand that it takes foie gras off the Christmas menu. The demonstration coincided with the release of&nbsp;<a href="https://www.animalequality.net/node/1141">footage filmed on a public tour of a foie gras farm in France</a>.

<img class="aligncenter" src="https://www.animalequality.net/sites/default/files/tate_web_4_0.jpg" alt="">

Tate is a charity which receives a large proportion of its funding from the taxpayer. As a result, it is accountable to the public – most of whom would be shocked that a publicly funded art gallery was planning to serve such an unethical product. In fact, a recent YouGov poll found that almost 80% of British people support an import ban on foie gras.

<img class="aligncenter" src="https://www.animalequality.net/sites/default/files/tate_web_3_0.jpg" alt="">

Offering this barbaric product, which causes extreme suffering to ducks and geese, is completely at odds with the idea of a ‘modern’ institution. We are pleased to let our supporters know that Tate has now confirmed that it will remove foie gras from its menu.

<img class="aligncenter" src="https://www.animalequality.net/sites/default/files/tate_web_1_0.jpg" alt="">

We are continuing to call on restaurants to take action and impose their own ban on foie gras. If you see this cruel product being served anywhere this festive season,&nbsp;<a href="mailto:info@animalequality.net?subject=Foie%20Gras">please let us know</a>.
