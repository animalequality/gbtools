Britain’s best-known birder and national treasure, Bill Oddie OBE, joined MPs and Animal Equality campaigners to call on Michael Gove to ‘Make Britain Foie Gras-free’ as we handed over 70,000 signatures backing an import ban post-Brexit. The event was held on March 29th marking exactly one year until our departure from the EU.

<img class="wp-image-3030" src="/app/uploads/2018/04/Bill-Oddie-Foie-Gras-Petition-article.jpg" alt="" />

MPs Andrea Jenkyns, Sandy Martin, Sir David Amess, Kerry McCarthy and Baroness Jenny Jones joined the event to show their support for a ban.

<img class="wp-image-3028" src="/app/uploads/2018/04/jenny-jones_Web-Res.jpg" alt="" />

Animal Equality has documented the extreme suffering of ducks and geese on dozens of foie gras farms in France and Spain and launched the petition in June 2017 after a YouGov poll found overwhelming support for a UK import ban on foie gras - 77% of those who expressed an opinion*.

Bird enthusiast Bill Oddie OBE said: “Whenever Britons call themselves ‘a nation of animal lovers’ I have assumed this means that we prefer our creatures healthy, well cared for and – in most cases - alive. There are few more grotesquely cruel so called ‘traditions’ than the production of Foie Gras, which entails what amounts to prolonged torture of living geese and ducks. We may not produce it in Britain, but we certainly consume it. You can buy foie gras in Britain’s affluent food stores and expensive restaurants. It is often referred to as a ‘gourmet delicacy’. It is in fact an abomination. Definitely NOT the food of a nation of animal lovers.”

The campaign is also backed by naturalist Chris Packham who said: “To imagine so many ducks and geese enduring a lifetime of cruelty and misery so a few people can eat a pate made from their diseased liver is mind-blowing, and terribly sad. UK imports of foie gras must be banned.”

Last month actor Evanna Lynch urged her Instagram followers to sign the petition, saying: “Foie Gras is a brutally cruel practice that needs to be stopped!”

Comedian Ricky Gervais also encouraged his fans to support the campaign and generated nearly 25,000 signatures in 72 hours.

You can still add your voice to the call for a #FoieGrasFreeGB at <a href="https://animalequality.org.uk/act/ban-force-feeding">www.animalequality.org.uk/foie-gras</a>. When we reach 100,000 signatures we will deliver them directly to No. 10.

<img class="wp-image-8157" src="/app/uploads/2018/04/group_Web-Res.jpg" alt="" />

* All figures, unless otherwise stated, are from YouGov Plc. Total sample size was 2,079 adults. Fieldwork was undertaken between 6th - 7th June 2017.  The survey was carried out online. The figures have been weighted and are representative of all UK adults (aged 18+).
