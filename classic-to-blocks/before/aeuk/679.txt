Basilico was found when he was just two days old. He was feeling weak and hungry.

<img class="wp-image-8211" src="/app/uploads/2015/03/11037568_894446307261750_3532412028238952351_n.jpg" alt="" />

Basilico was found not very far from a farm. He was destined to be killed in over a month. Millions of babies like him are slaughtered daily when they are only a few weeks old. That is how the meat industry treats babies. They get away with abusing innocent animals. Lambs who are still drinking their mother's milk and have never grazed.

<img class="wp-image-8210" src="/app/uploads/2015/03/11017064_894446403928407_2603305315348973738_n.jpg" alt="" />

Basilico would have been kicked off a truck, he would have entered the killing floor of a slaughterhouse terrified. He desperately would have looked for a way out, he would have called for his mother. <a href="https://www.youtube.com/watch?v=WO6Wd1i7eoM">His throat would have been slit while he was fully conscious. </a>

But Basilico’s story has a happy ending.

<img class="wp-image-2322" src="/app/uploads/2015/03/10429235_894446693928378_8447978681624623791_n_0.jpg" />

Animal Equality intervened as soon as we saw him and saved him. In collaboration with the Italian sanctuary 'The Green Place', we stepped in, and Basilico was immediately fed, taken care of and brought to an animal sanctuary. He will now life his entire life surrounded by people who love him. He will never feel terror again.

Basilico is very gentle and playful. He loves being in the sun and hanging out with his human friends and other animals. Basilico deserves a life free from cruelty. So do all animals.

Help us to continue saving animals like Basilico by becoming an Animal Equality monthly supporter today.

&nbsp;

<a href="http://www.animalequality.net/donation-usa"><img class="wp-image-8299" src="/app/uploads/2015/03/donate-button_en.png" alt="" /></a>
