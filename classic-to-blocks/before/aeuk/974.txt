<div class="center"><img class="wp-image-2680" src="/app/uploads/2017/03/rabbitvoteA4.jpg" /></div>
The submission of this 'alternative resolution' is an unusual step, especially since the report by MEP Stefan Eck was already supported by a majority of votes in the Agricultural Committee (including votes from the EPP) back in January.

Eck's report aims to put an end to cruel rabbit cages and to introduce mandatory minimum standards in rabbit farming. The alternative motion by the EEP merely provides a recommendation for improved rabbit welfare without any binding legislation for the industry.

With this move the EPP is trying to prevent effective animal protection measures and make Eck's report obsolete. It is a desperate reaction to our campaign! Now more than ever we must show the MEPs that we demand serious animal protection and a ban on rabbit cages! <a href="http://www.animalequality.net/banrabbitcages">Please sign and share our petition TODAY</a>.
