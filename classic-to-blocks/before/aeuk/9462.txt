Animal Equality recently visited a foie gras farm to witness the production of the cruel ‘delicacy’ first hand. To our amazement, many farms in France offer public tours, open to families. We were welcomed onto one and filmed ducks and geese suffering severely in barren, wire cages and terrified birds struggling while being ruthlessly force fed with metal tubes.

<iframe src="https://www.youtube.com/embed/_8udctdnINM" width="600" height="315" frameborder="0" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe>

Actor and animal advocate, Peter Egan and TV veterinarian, Emma Milne joined Animal Equality on the visit to ‘La Ferme Turnac’ foie gras farm in France. They witnessed terrified, panting ducks crammed into tiny, filthy cages and geese struggling to escape as a vast amount of food was pumped down their throats using a large metal tube.

<img src="https://www.animalequality.net/sites/default/files/fg2.jpg" alt="">

Shocked that the farm was happy to put this horrific cruelty on display for families to watch, Peter Egan spoke out strongly against the practice: “It’s disgusting. It should be banned.”

Speaking about the force-feeding process, veterinarian and animal welfare campaigner Emma Milne said “The vocalisation was absolutely clear that it was an extremely unpleasant experience. If that’s what they’re happy to show you, I dread to think what some of the worse farms are like.”

<img src="https://www.animalequality.net/sites/default/files/fg3.jpg" alt="">

If the cruelty we filmed on this farm is the most humane way that foie gras can be produced, then it clearly has no place in a compassionate nation like the UK. Show your support for an import ban and&nbsp;<a href="https://animalequality.org.uk/act/ban-force-feeding">add your name to the 100,000+ people calling for a foie gras-free Great Britain.</a>
