<div class="center"><img class="wp-image-8429" src="/app/uploads/2018/03/close-up-beak-trim.jpg" /></div>
Noble Foods, the UK's biggest egg producer, has announced a commitment to producing solely cage-free egg production by 2025. The decision, which will reduce the suffering of millions of birds every year, comes just days after Animal Equality released shocking scenes of suffering filmed on a Noble Foods caged-hen farm in Dorset.

While cage-free doesn’t mean cruelty-free, Noble Foods is taking an important step toward improving conditions for animals who suffer tremendously on industrialised farms. Cramming birds in cages is a horrific practice. Studies have shown that birds are social and sensitive creatures with intelligence on par with a toddler. Yet they are forced to live in filthy wire cages with no semblance of a natural life nor the ability to engage in most natural behaviours.

<img class="wp-image-3017" src="/app/uploads/2018/03/Feet-on-wire.jpg" alt="" />

<em>Millions of hens will no longer spend their entire lives standing on painful wire floors thanks to this commitment from Noble.</em>

We are incredibly proud to have played a part in this landmark decision and <strong>congratulate our friends at The Humane League UK </strong>for their tireless efforts over the past six months to make this happen.

Of course, you can help end the suffering of hens today by choosing to leave eggs off your plate! Visit <strong><a href="https://loveveg.uk/">www.loveveg.uk</a></strong> for delicious egg-free recipes and products.
