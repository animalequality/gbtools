Animal Equality Italy, in collaboration with media outlet Tg2, has exposed deplorable conditions inside a pig farm in the Italian region of Lombardy. Our investigators found thousands of pigs living in filthy sheds, with discarded dead bodies left to decompose.

Our investigators visited the farm multiple times between February and April 2019. Despite Italy's animal welfare laws, we found distressing cruelty taking place, including:
<ul>
 	<li>Dead bodies of piglets abandoned in the corridors</li>
 	<li>Piles of decomposing bodies</li>
 	<li>Pigs forced into tiny cages, causing wounds which had become infected</li>
 	<li>Animals forced to live in overcrowded conditions</li>
 	<li>Feeders full of faeces</li>
 	<li>Infestations of cockroaches, mice and worms</li>
</ul>
More than 8 million pigs are bred every year in intensive factory farms in Italy, with almost 4 million in Lombardy alone. Of those, more than 500,000 female pigs are forced to spend weeks at a time between the bars of cages without the ability to move or look after their babies. The UK imports pig meat products from many EU countries, including Italy, to be sold here.
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/vcPA4M-J2HQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
Animal Equality Italy has reported its findings to the authorities and is calling for the farm to be shut down with immediate effect.

Once again our investigative work has exposed the extreme suffering taking place inside factory farms, destroying the idyllic image often portrayed by the meat industry. We need your support to continue to reveal these abuses – please consider <a href="https://animalequality.org.uk/donate/">donating today</a> to impact the lives of millions of animals.
