<div class="center"><img class="wp-image-3069" src="/app/uploads/2018/08/29277591127_b31ca6c3f8_z_0.jpg"></div>
Animal Equality has today released footage of calves being violently handled on an organic dairy farm in Somerset which supplies Waitrose. The shocking footage, which was filmed this summer after a tip-off about cruel handling practices, shows:
<ul>
 	<li>Newborn calves struggling as workers callously shove a tube down their throat</li>
 	<li>A calf thrown to the ground and slapped in the face during force-feeding</li>
 	<li>A worker standing on a calf with his full body weight while shouting “you f**king. . .sh*t”</li>
 	<li>Cows visibly agitated and trying to intervene as struggling calves call out in distress</li>
 	<li>Calves denied access to water for up to 29 hours on some of the hottest days of the year</li>
 	<li>A newborn calf dragged by its back legs into a separation pen</li>
 	<li>Calves routinely separated from their mothers less than 24 hours after birth</li>
 	<li>Several cows with their back legs chained together in shackles</li>
</ul>
<iframe src="https://www.youtube.com/embed/QxiYtq8X-ls" width="600" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Far from the idyllic image of organic dairy farming, such cruel treatment of newborn calves while their mothers look on helplessly will undoubtedly shock consumers. Yet the breaking of this strong maternal bond is an inherent part of the dairy industry, where mothers and babies are routinely separated so that their breast milk can be bottled and sold to us.

<img class="wp-image-3064" src="/app/uploads/2018/08/dairy-news-1.jpg" alt="">

&nbsp;

Our investigation reveals that Coombe Farm fails to meet even the minimum legal requirements for looking after calves and falls well below organic and RSPCA standards, despite being certified by the Soil Association and RSPCA Assured. We have passed our footage on to both of these organisations, as well as to Waitrose.

<img class="wp-image-3067" src="/app/uploads/2018/08/dairy-news-2.jpg" alt="">

&nbsp;

Labels and awards can’t protect animals from suffering in the dairy industry, but you can. Choose from the many delicious cruelty-free plant-based milks now widely available – <a href="https://loveveg.uk/" target="_blank" rel="noopener noreferrer">it’s never been easier to ditch dairy</a>!
