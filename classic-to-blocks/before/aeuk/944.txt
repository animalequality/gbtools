<div class="center"><img class="wp-image-2584" src="/app/uploads/2017/01/14019399498_081f395240_o.jpg" /></div>
Every year, <strong>320 million rabbits</strong> are slaughtered for meat in the European Union - and there are currently no legal provisions to protect them. This means that 99% of farmed rabbits are kept in small wire cages with little more space than an A4 sheet of paper.

<img class="wp-image-8215" src="/app/uploads/2017/01/14067611149_632e2c782c_o.png" alt="" />

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

<strong>Animal Equality has repeatedly documented the terrible suffering of rabbits in battery cages</strong> in Spain and Italy – where 60% of all rabbit farms in the EU are located. In Spain alone we have <a href="http://www.animalequality.net/news/623/new-undercover-investigation-reveals-cruelty-inside-rabbit-farms-linked-uk">investigated 70 rabbit farms and 4 slaughterhouses</a> and in each and every single one we have found evidence of extreme cruelty.

Our investigators have seen rabbits with open wounds, painful eye infections and even rabbits whose ears have been bitten off by their stressed companions. Due to the crowded and unnatural conditions, <strong>up to 30% of farmed rabbits die or are killed even before arriving at the slaughterhouse</strong> – that percentage is higher than with any other farmed animal.

<iframe src="https://www.youtube.com/embed/E2UHnOsfxhw" width="560px" height="315px" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

<strong>You can help end this cruel treatment of rabbits!</strong>

In June 2016 we <a href="http://www.animalequality.net/node/899">presented the evidence from our investigations in the European Parliament</a>, along with Compassion in World Farming, and now members of the Agriculture Committee are discussing a proposal for a <strong>Europe-wide ban on battery cages for rabbits</strong>. If they approve the report it will be voted on by all Members of the European Parliament later in the year.

<strong>Take action!</strong>

The Agriculture Committee will be voting on the proposal on January 25th, and we need to let our key country representatives know that we want them to support the ban. <strong>Please take a few moments to email these UK representatives</strong> and urge them to approve the report and support a ban on cages for rabbits:

<a href="mailto:richard.ashworth@europarl.europa.eu?subject=Please%20support%20the%20proposal%20to%20end%20battery-cages%20for%20rabbits">Richard Ashworth MEP</a>

<a href="mailto:diane.dodds@europarl.europa.eu ?subject=Please%20support%20the%20proposal%20to%20end%20battery-cages%20for%20rabbits">Dianne Dodds MEP</a>

<a href="mailto:jim.nicholson@uup.org?subject=Please%20support%20the%20proposal%20to%20end%20battery-cages%20for%20rabbits">Jim Nicholson MEP</a>

<strong>Please don't miss this opportunity to speak out against the factory farming of rabbits!</strong>
