<p align="justify">Animal Equality is proud to announce a new partnership with <strong>vegan chef Vanessa Almeida</strong>, the author behind the brand new <a href="http://essentialvegan.uk/cookbook/">Essential Vegan cookbook</a>.</p>
<img class="wp-image-4488" style="width: 600px; " src="/app/uploads/2013/09/book2.jpg" alt="" />

This mouth watering collection of plant-based recipes is perfect for both long term vegans and people looking for healthier meal ideas.

For every copy of Essential Vegan sold via her website, <a href="http://essentialvegan.uk/cookbook/">Vanessa will generously donate £2 to Animal Equality</a>.

<a href="http://essentialvegan.uk/cookbook/">Buy Essential Vegan today</a>. Not only will you be supporting an independent author, you will be securing yourself a collection of must-have vegan recipes while helping us continue our efforts to improve outcomes for animals.

<strong>Thank you in advance for your support!</strong>
