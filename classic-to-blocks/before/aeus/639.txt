The world’s largest sacrifice is almost here, and animals need your help to stop it! If we do nothing, hundreds of thousands of buffalo, goats, lambs, birds and more will be slaughtered at Nepal’s Gadhimai festival, filling an entire field with ankle-deep blood.

Our tireless team is working on the ground in India and Nepal but they need funding to carry out our plan. Here’s where your funds will go:
<ul>
 	<li>Monitoring of the Indo-Nepal border to ensure that officials do not allow animals across (most attendees come from India).</li>
 	<li>Transportation, accommodations, and equipment for our investigative team, who will document the festival.</li>
 	<li>A public awareness campaign to teach villagers alternatives to sacrifice.</li>
 	<li>A direct campaign to the Nepalese government urging them to outlaw animal sacrifice.</li>
</ul>
We need caring people like you now more than ever to stop the massacre!
