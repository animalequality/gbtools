<a href="https://animalequality.org/blog/2022/02/08/what-is-foie-gras/">Foie gras</a> means "fatty liver" in French. It’s a product made from a painfully enlarged, force-fed ducks' liver which has been pumped repeatedly with corn.

Despite the extreme levels of cruelty masked behind this product, it is considered a ‘delicacy’ in many countries and still remains widely available through out restaurants and stores. However, this is not the case in California.

Last Tuesday, the U.S Supreme Court authorized the State of California to sustain the ban on foie gras which bans state farmers from force-feeding birds with a tube. The aforementioned law also prohibits the sale of foie gras throughout California. Although challenged by New York producer Hudson Valley Foie Gras – among other groups – the law remains intact and in favor of animals.

Animal Equality was also delighted to announce on July 3, the notification issued by the <strong>Directorate General of Foreign Trade, stating that the import of foie gras</strong> was prohibited following our campaign in India.

Certainly, this victory is a promising and substantial step forward which we hope influences other states, as well as countries worldwide, to unite and take after this legal reform in order to protect animals from pain and inhumane treatment.

<strong>You can help ducks and geese by taking action today: <a href="https://animalequality.org/action/foie-gras">Sign our petition for a US ban on foie gras!</a></strong>
