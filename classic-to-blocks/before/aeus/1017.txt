Ten-year-old Genesis Butler, a lifelong animal rights activist, could take to task the most experienced vegans. In a recent TEDx talk she lays out the facts linking the cruel and deadly animal agriculture industry to the problems of climate change we currently face.

As one of the youngest TEDx speakers ever, she wisely spells out how the simple choice to adopt a plant-based lifestyle can mean the difference between destroying or saving our planet.

<iframe src="https://www.youtube.com/embed/E4ptaIDAIlY" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe>

<strong>Will you heed Genesis’ call by taking a stand for our future, our planet and by being an ally for animals?</strong>
