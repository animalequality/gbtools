145 NGOs from all over the European Union (EU), including Animal Equality, are calling for a ban on the use of cages for farmed rabbits in the EU.

<strong>THE DETAILS:</strong> In footage gathered by Animal Equality, Compassion in World Farming and others, rabbits are shown living in battery cages on wire mesh that can cause them injuries. They are kept in such overcrowded conditions that each rabbit has less space than a letter-sized piece of paper. They can’t stand upright, run, hop or express any of their natural behaviors, which has a detrimental impact on their welfare. In response to this cruelty and more, the “End the Cage Age” European Citizens’ Initiative (ECI) was created and calls for a ban on the use of cages for farmed animals throughout the EU.

<strong>THE BACKGROUND:</strong> In 2017, the European Parliament adopted a report that recognized barren battery cages as an inappropriate housing system for rabbits, agreeing with European citizens who expressed concern over the conditions in which these animals are farmed. Despite this report, new footage reveals that rabbits are still being kept in these cruel, caged conditions.

<strong>COMMENTARY:</strong> It’s time for the European Commission to sit up and take notice that EU citizens will not stand for such cruelty and suffering to go on behind closed doors any longer. Rabbits are the most caged farm animal in Europe. They are born in a cage and remain caged until they are slaughtered. A life behind bars with no access to sunlight, fresh air and the inability to move around naturally is no life at all. This appalling suffering where rabbits are confined in tiny wire cages happens throughout the EU. It needs to stop.

<strong>WHAT YOU CAN DO:</strong> In the EU alone, more than 300 million animals are farmed in cages every year. It’s time for that to change. To ensure that the European Commission states what steps it will take to introduce legislation to ban the use of cages throughout the EU, the coalition is trying to gather 1 million signatures by September 2019. Sign and share the EU petition today and add your voice for a cage-free future for farm animals.

<strong><a href="https://igualdad-animal.endthecageage.eu/en-INT/live">SIGN THE PETITION AND END CAGES FOR FARMED ANIMALS IN THE EU!</a></strong>

<strong>
</strong><strong></strong><em>- image courtesy of Compassion in World Farming</em>
