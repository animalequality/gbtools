On 7th May 2014, the Supreme Court in India, has passed a landmark judgment banning bullock cart races and all performances of bulls throughout the country including jallikattu (bull taming sports), bull fights, and other cruel sports thanks to the work of <a href="https://www.facebook.com/AnimalEqualityIndia?fref=ts"><strong>Animal Equality India</strong></a>, PETA India, and the Animal Welfare Board of India<strong>. </strong>The Animal Equality team has been successful at putting a stop to the cruel bullock cart races.

<img class="wp-image-4341" style="width: 600px; margin: 2px;" src="/app/uploads/2014/05/Biting-of-tails-3.jpg" alt="">

The honorable judges also mentioned that Parliament is expected to amend the Prevention of Cruelty to Animal Act, 1960 so that it proves an effective deterrent. They have also mentioned that the rights of animals should be elevated to Constitutional rights, to protect their dignity and honor.

Animal Equality in collaboration with activists, Gargi Gogoi, Ajay Marathe and Anil Katariya have been working tirelessly to stop these cruel bullock cart races for the past few years.

Bulls are beaten, whipped, electrocuted, force-fed alcohol, prodded with nails and spiked instruments, their tails are twisted and bitten and testicles squeezed and nose rope pulled by men, for these cruel races.
