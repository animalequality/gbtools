Each year, hundreds of millions of chickens suffer tremendously on farms that raise chickens for McDonald’s menu items. These birds are bred to be so unnaturally big their own legs can’t support their weight. They are crammed into dark sheds and trapped in filth, unable to engage in most natural behaviors. These horrific conditions are unacceptable.

<iframe src="https://www.youtube.com/embed/J4A38HscqfA?rel=0" allowfullscreen="allowfullscreen" width="100%" height="600" frameborder="0"></iframe>

The chickens raised and killed for McDonald’s live miserable lives. They grow so large so fast that their legs can’t support their own bodies. Neither can their organs - the birds often suffer heart attacks as a result of their unnatural rapid growth. These birds, bred for their parts such as “breasts” and “wings,” suffer leg deformities and constant pain so severe they can barely walk.Tens of thousands of oversized chickens are crammed together inside dark sheds without much room to move. Rarely able to engage in most natural behaviors such as perching, pecking or dust-bathing, these normally-curious baby birds rot in boredom.

<img class="wp-image-4159 alignleft" src="/app/uploads/2018/03/NewYorkTimesMcDAd.jpg" alt="">

The birds’ litter is rarely changed, forcing these chickens to sit, eat, and sleep in their own waste. Because the birds are too heavy to lift off the ground, the ammonia-laden litter burns the birds’ bellies, causing feather loss and boils. The putrid air stings the birds’ eyes.

As one of the world’s most notable brands, McDonald’s has the power to eliminate the cruelest farming practices used by its producers and reduce the suffering of millions of birds.

Urge McDonald's to improve the lives&nbsp;of chickens <a href="https://truthaboutmcdonaldschicken.com/">by signing this petition</a>.

Animal Equality has joined a coalition of groups to educate the public about the cruelty endured by the millions of chickens in McDonald’s supply chain. We will be organizing grassroots activities, online and on the ground, to show McDonald’s that its consumers overwhelmingly support the company eliminating the cruelest farming practices used by its producers to reduce the suffering of animals.

We invite anyone interested in helping these animals to <a href="https://animalequality.org/animal-protectors/">join our national network of volunteers called “Animal Protectors.”</a> Animal Protectors take small, easy online actions--such as sending emails or making social media posts--that add up to a big difference for animals.
