<span style="font-weight: 400;">For the first time in Italy, Animal Equality infiltrated two farms in the provinces of Cuneo and Mantua. Recorded footage released this week from these farms demonstrates the neglect and systematic mistreatment inflicted on pigs, including those certified for Parma Ham.</span>

<span style="font-weight: 400;">In the videos, you can clearly see the operators violently handling the piglets and adult pigs. The piglets are treated as if they were objects - taken by their legs and forcefully thrown, while their mothers are insulted, hit with sticks on their snouts and bodies, and dragged through the halls while debilitated by disease and horrific living conditions.</span>

<span style="font-weight: 400;">One of the cruelties recorded by investigators is the systematic procedure of cutting off the tails of baby pigs. In the US it is practiced routinely, in contrast to the directives of the European Union. </span><i><span style="font-weight: 400;">The EU has already sanctioned Italy, as almost all of their farms violate EU directives.</span></i>

<b>Some of the misconduct documented in the video:</b>
<ul>
 	<li><span style="font-weight: 400;">Operators who handle pigs without care</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Pigs abandoned to die in the breeding halls</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Operators who violently hit pigs on the face and on the head</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">An operator who repeatedly hits a sow violently on the vagina with a plastic tube</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Sows sick and covered with wounds</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Operators grabbing pigs by their hooves and violently throwing them</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Dead animals inside breeding farms</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Systematic cutting of piglets’ tails in violation of EU directives</span></li>
</ul>
<iframe src="https://www.youtube.com/embed/RCSQlL2N8wE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

<a href="https://animalequality.it/allevamenti-maiali/"><span style="font-weight: 400;">Sign our petition to demand better treatment for Italian farmed animals.</span></a>
