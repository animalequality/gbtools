1. In 2009, our research team rescued two little lambs and their mother from a farm where they were in harsh conditions. Their fate would’ve been the slaughterhouse. &nbsp;However, these little ones and their mother were lucky enough to meet our team&nbsp;who assisted them quickly. Jo-Anne McArthur, author of the book "We Animals" and star of the award-winning documentary The Ghosts In Our Machine, was with them. The rest of it was captured in this moving picture that has been seen all around the world.

2. In 2010, two Animal Equality activists burst onto Cibeles&nbsp;Runway when the fashion show displayed designers that used animal skins in their creations. On two different occasions during the live broadcast, our activists launched a message heard around the world: animal skins belong to animals and there is no need to wear them!

<img class="wp-image-4604" src="/app/uploads/2015/09/fotoviral2.jpg" alt="">

3. December 10th is&nbsp;International Animal Rights Day. Animal Equality has commemorated it since 2008 with a ceremony at Madrid’s Plaza del Sol. In 2011’s edition, 400 activists held dead animals from modern farms where they lived in terrible conditions. Jon Amad’s capturing photos of the event circulated fast!

<img class="wp-image-4605" src="/app/uploads/2015/09/fotoviral3.jpg" alt="">

4. In 2012, our research team conducted a pioneering report on the production of foie gras in Spain and France. The animal abuse involved in the production of foie gras is one of the most terrifying of all we have witnessed. It's no wonder why the production of foie gras has been banned in 17 countries. The scene captures the moment of “force-feeding”, in which a long tube is inserted into the duck’s esophagus and the food is being pumped directly into the stomach. For more information visit: foiegrasfarms.org

<img class="wp-image-4606" src="/app/uploads/2015/09/fotoviral4.jpg" alt="">

5. Our team of researchers traveled to China in 2013 to make a hidden camera article about the consumption of dog meat. In one of the slaughterhouses, our photographer captured the startling moment when the butcher chooses the next dog he will kill. The terrible scene went viral and has been watched by millions of people. For more information visit: voicelessfriends.org

<img class="wp-image-4607" src="/app/uploads/2015/09/fotoviral5.jpg" alt="">

6. In 2014, Animal Equality India made an article about&nbsp;the bloodiest religious ritual in the world, located in Nepal. To honor&nbsp;the goddess, Gadhimai, the ritual consists of&nbsp;violently ending&nbsp;half a million&nbsp;animal lives. Our Indian team&nbsp;filmed aerial scenes with drones to capture the magnitude of this cruel scene. The helplessness of this calf amongst the mutilated bodies of countless animals was plenty to make it go viral. Thanks to Animal Equality’s work and other animal protection organizations, people in charge of the Gadhimai temple have announced that they will no longer&nbsp;use animals in their rituals.

<img class="wp-image-4608" src="/app/uploads/2015/09/fotoviral6.jpg" alt="">
