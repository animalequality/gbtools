In the Central Valley of California, surviving birds from a fighting ring were taken by the&nbsp;Stanislaus&nbsp;County animal services, then rescued by an animal sanctuary. On Sunday February 7th,&nbsp;Stanislaus&nbsp;County sheriff's deputies were called to a home in Modesto, CA where they found a large-scale rooster fighting operation.

<img class="wp-image-4449" src="/app/uploads/2016/02/a195021f8f398ee1a8e620341461d2dd_L1.jpg" alt="">

Nearly 300 chickens were found, with over 100 dead. Those who survived endured incredible hardship by being confined in small cages and living through unsanitary conditions. Christine Morrissey of Harvest Home Animal Sanctuary in Stockton stated to Fox 40 News, "We saw animals, we saw dead animals and animals in the process of dying."

Cockfighting (which is illegal in most countries) is considered to be a blood sport where two roosters fight each other to the death – while spectators watch and place bets. The animals suffer from traumatic injuries and are treated like waste once the fight is over, even if they have "won."

According to The Humane Society, roosters are rarely violent toward&nbsp;one another, but those involved with cockfighting often wear razor-sharp blades attached to their legs which causes severe injuries. It is also common that the birds are given drugs to increase their aggression;&nbsp;It's wrong in so many ways!

Morrissey was able to rescue five birds that are "going to live their lives out naturally in peace and safety" at their animal sanctuary. Since the rescue, the animals have received proper veterinary&nbsp;care and have&nbsp;been given names – El Chapo the Mighty Rooster, Lindsey the Legendary Lady, Gina the Gangster Gallina, Sofia the Supreme Superhero, and Lori the Lionhearted Leader.

We hope no bird will ever have to experience this kind of pain. Thank you&nbsp;Harvest Home Animal Sanctuary for giving these beautiful creatures&nbsp;a second chance at life.

Sources:&nbsp;http://fox40.com/2016/02/08/20-arrested-in-modesto-rooster-fighting-sting/
