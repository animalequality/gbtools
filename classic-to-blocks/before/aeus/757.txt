If the name of our planet honored the most numerous animal, it should be called planet chicken. This gentle animal whose ancestors lived in India has colonized the planet. How? By becoming the most consumed meat worldwide. Unfortunately, this human preference for chicken meat is the cause of one of the greatest animal tragedies of history. Never before this amount of animals were killed. The price of chicken meat is the cheapest, but this results in higher animal abuse.

We explain some facts that many do not know:

<strong>1. The most unfortunate babies</strong>

Chickens arrive at factory farms at one day old. They are sent to slaughter when they are around 40-50 days of age, therefore, we eat chickens when they are still considered babies.

<strong>2. There are 3 chickens for every human on the planet</strong>

The chicken meat industry is the most widespread. At all times, there are 20 billion chickens in the world. They live in conditions with extreme overcrowding in large industrial buildings. It is sadly ironic that we may never see the most abundant being on the planet alive.

<strong>3. We eat chickens that weigh 6 times their natural weight</strong>

Through hybridization between breeds of chickens, humans have created an animal that grows at an unnatural speed. The goal is to quickly supply chicken to our supermarkets; more meat in less time. As a result, the chickens raised for meat, of the “broiler” race, suffer greatly from genetic diseases. These birds’ muscles grow so fast that their legs can’t hold it. If a human baby were to grow at the same rate, a two-month-old baby would weigh 300 kilos.

<img class="alignnone size-full wp-image-29605" src="https://animalequality.org/app/uploads/2022/08/2022-chicken-splay-leg-italian-investigation-600x400-1.jpg" alt="" width="600" height="400" />

<strong>4. Lives are drastically shortened</strong>

Chickens growing at their natural rhythm can live 6-8 years. Since the 20’s, the age in which they are sent to slaughter has been declining. The chickens in factory farms are sent to slaughter with 40-50 days of age. At that time, their muscles have developed enough to fill market requirements. Their short lives have been full of pain and suffering.

<strong>5. Can you imagine not having a mother?</strong>

Fragile chicks come to factory farms on their day of birth. Their mothers lay eggs in other farms; those eggs are collected and taken to “incubator” farms where eggs are artificially incubated. As soon as they hatch, they are sent to fattening farms where they spend their 40-50 days of life. They have never met their mother; they have never felt, nor will ever feel her love and protection.

<strong>6. The deaths on farms are immense, and there is no individualized veterinary care</strong>

<img class="wp-image-4730" src="/app/uploads/2015/10/pollobroiler-1.jpg" alt="" />

Thousands or even tens of thousands of chickens live on factory farms. Diseases abound when living in such extreme conditions. Many chickens suffer from genetic problems and diseases, but no veterinarian attends to them; it is too expensive and their lives are worth so little. The antibiotics they receive are provided to them through their food and water, but the sick ones can’t even reach food or water. They end up dying or agonizing in pain for hours or days. Others are still alive when they are thrown into large containers where they will ultimately die.

<strong>7. Chicken meat is full of antibiotics.</strong>

Due to weakness acquired after decades of hybridization, chickens are prone to illnesses. The extremely crowded conditions of the farms are also a risk factor. They are given a lot of antibiotics through food and water, and in some countries, antibiotics are even injected into the egg. Some studies have shown a direct relationship between the resistance of bacteria to antibiotics in humans with the consumption of chicken meat.
