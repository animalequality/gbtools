Animal Equality documented the situation through detailed notes, <a href="http://www.schwabenparkrecherche.com/en/photos/"><strong>350 photographs</strong></a>, and more than 500 minutes of video footage.

<a href="http://www.schwabenparkrecherche.com/en"><strong>Schwaben Park</strong></a> currently houses 44 chimpanzees, <strong>making it one of the largest private collections of chimpanzees in Europe</strong>. Other animals at the park include Siberian tigers, goats, sheep, alpacas, pigs, and birds. Many of the inhabitants perform in circus shows.

Animal Equality investigators found:
<ul>
 	<li><strong>Chimpanzees and other animals exploited in archaic circus shows </strong>- where they are humiliated and portrayed as clowns.</li>
 	<li><strong>Female chimpanzees used as breeding machines </strong>to ensure a continuous supply of infants for the shows. Infants are separated from their mothers at around a day old to be trained. One of the chimpanzees, ‘Chita’, has given birth to at least eight infants - all of whom were taken from her.</li>
 	<li><strong>Chimpanzees hired out for use in television shows and advertising.</strong></li>
 	<li>Animals are <strong>confined in small, barren enclosures and perform abnormal, repetitive and stereotypic behaviors.</strong> Chimpanzees were documented rocking back and forth, sucking their lips, salivating, and swaying against enclosure perimeters.</li>
 	<li><strong> Chimpanzees were observed with hair loss, wounds, ulcers </strong>and open sores on their bodies.</li>
 	<li><strong>Infants are separated from their mothers</strong> at around a day old to be trained. Hand-rearing is widely condemned, even amongst the zoo community. There are serious psychological effects to this.</li>
 	<li>The <strong>archaic circus-style shows are attended by families with children</strong> who are likely to leave believing that animals naturally behave like clowns</li>
</ul>
Despite the prohibition on the use of chimpanzees in travelling German circuses, during the investigation at least six chimpanzees were performing in shows. Chimpanzees wear human clothing, drive motorised quad bikes, and perform handstands. Movement is restricted by handlers using collars, leashes and chains.
<div class="media_embed"><iframe src="https://www.youtube.com/embed/i8A4T_Dl8sU" width="560px" height="315px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
Whilst the training of chimpanzees at Schwaben Park takes place behind closed doors, extensive research reveals that methods are often abusive and harsh.

Animal Equality’s findings are supported by<strong> <a href="http://www.schwabenparkrecherche.com/en/experts/">leading captive animal and chimpanzee rehabilitation experts</a></strong>, such as <strong>Dr. Lorraine Doherty</strong> (MONA Foundation), <strong>Prof. Marc Bekoff,</strong> <strong>Dr. Jonathan Balcombe</strong>, <strong>Dr. Theodora Capaldo</strong> (President of the New England Anti-vivisection Society) , and <strong>Dr. Andrew Knight</strong> (Veterinarian and Fellow of Oxford Centre for Animal Ethics).
<blockquote>"<em>Following this expose, Schwaben Park should be viewed not as an amusement park, but a prison for hundreds of exploited, innocent beings. In this day and age, we should reject facilities such as Schwaben Park. Animals are not objects of entertainment and I urge the public to boycott this dismal facility.</em>“ -&nbsp;Prof. Marc&nbsp;Bekoff&nbsp;-Emertus of Ecology and Evolutionary Biology, University of Colorado, Boulder, US</blockquote>
