According to new research by the animal protection group <a href="https://www.worldanimalprotection.us/blogs/fast-foods-love-plant-based-meat-saves-quarter-million-animals-year">World Animal Protection</a>, Impossible Foods and Beyond Meat additions to fast food menus are saving 250,000 animals a year. That’s nearly 700 animals a day!

<img class="aligncenter wp-image-14845 size-full" src="/app/uploads/2020/01/cows_in_field.jpg" alt="" width="600" height="410" />

<strong>THE DATA</strong>: Because businesses are often secretive about their sales information, World Animal Protection used a clever formula to estimate the amount of animals spared based on the average weight of typical farmed cows and pigs, and the pounds of plant-based beef and sausage the restaurants are serving. Research data shows that a majority of people who purchase the products are “flexitarian” or not fully vegetarian, so the organization assumes that 90% of the purchases were made by people who would’ve otherwise gotten a meat-based meal if the plant-based one wasn’t available.

[caption id="attachment_14846" align="aligncenter" width="600"]<img class="wp-image-14846 size-full" src="/app/uploads/2020/01/dunkin_beyond_sausage.jpg" alt="" width="600" height="410" /> Dunkin' Beyond Sausage Sandwich with hash browns[/caption]

<strong>WHAT THIS MEANS</strong>: World Animal Protection noted that although this number is just a fraction of the 120 million pigs and 30 million cows killed for food in the United States each year, it’s still a cause for major celebration. Whenever a customer chooses a plant-based option over a meat one, this represents an individual spared from cruel confinement and a horrific death in a slaughterhouse. The world is changing, one burger at a time!

[caption id="attachment_14848" align="aligncenter" width="600"]<img class="wp-image-14848 size-full" src="/app/uploads/2020/01/burger_king_impossible_whopper_for_blog.jpg" alt="" width="600" height="410" /> Burger King Impossible Whopper[/caption]

<strong>WHERE TO TRY IT:</strong> These are just some of the locations where you can try Impossible Foods and Beyond Meat.

<span style="font-weight: 400;"><strong>Beyond Meat: </strong></span>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Dennys</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Subway (select locations)</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Del Taco</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">TGI Fridays</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Carl’s Jr. </span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Hardee’s</span></li>
</ul>
<strong>Impossible Foods: </strong>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Fat Burger</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">White Castle</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Qdoba</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Red Robin</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Burger King</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">Hard Rock Cafe</span></li>
</ul>
[caption id="attachment_14847" align="aligncenter" width="600"]<img class="wp-image-14847 size-full" src="/app/uploads/2020/01/carls_jr_beyond_burger_california_classic.jpg" alt="" width="600" height="410" /> Carl's Jr. California Classic burger with Beyond Meat patties[/caption]

<strong>WHAT YOU CAN DO</strong>: The best way you can help spare more animals from suffering on factory farms and in slaughterhouses is simply by leaving them off your plate. Choosing these plant-based meats at restaurant chains is a great place to start! You’ll find that your local grocery store is also full of options. For tips, check out<a href="https://loveveg.com/"> loveveg.com</a>.
