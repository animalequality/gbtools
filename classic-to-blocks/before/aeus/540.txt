Spokeswoman Małgorzata Książyk says the public consultation period will last one week, following a Constitutional Court ruling late last year which said that ritualized slaughter of animals for religious purposes was inconsistent with Polish law.

The court concluded that a 2004 amendment that introduced exceptions to an animal protection law that forbade the slaughter of animals without prior stunning was “unconstitutional.”

Last week, the new draft law, which would make Polish law compatible with European Union regulations, was considered by the Standing Committee of the Council of Ministers, and the bill has now been passed on to social and trade union organizations for consultation.

The case was brought before the Constitutional Court last year by animal rights activists who claim the practice is cruel.

Ninety Polish scientists addressed a letter to Poland's prime minister in December saying that kosher and halal slaughtering methods are <em>“extremely cruel to animals”.</em>

<em>“Our position is not dictated by any dislike of religious practices and rituals but based solely on scientific knowledge and moral opposition to extreme forms of cruelty to animals,” </em>reads the letter written by Dr. Antoni Amirowicz at the Institute of Nature Conservation, Polish Academy of Sciences and Prof. Jerzy Bańbura of the Department of Experimental Zoology and Evolutionary Biology at the University of Lodz.

There are differences of opinion among legal experts and politicians as to whether Polish law needs re-writing, however.

Director of the Institute of Legal Sciences, Professor Wladyslaw Czaplinski said in January that <em>"EU Regulation have absolute priority over [Polish] law”, </em>though Agriculture Minister Stanislaw Kalemba believes that in order to legalize ritual slaughter it is necessary to amend the Protection of Animals Act.

Minister Kalemba has said the practice should be made legal again not just for religious reasons but also because the ritual slaughter for meat earns 1.2 to 1.5 billion zlotych (400 million euros0 per year for the Polish economy, and creates between 4 and 5,000 jobs.
