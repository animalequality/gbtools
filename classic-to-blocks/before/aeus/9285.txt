<span style="font-weight: 400;">The evening will feature fabulous cocktails, a gourmet, plant-based seated dinner, and the opportunity to mingle with fellow animal advocates, celebrity supporters, and our international team as we celebrate all that you have helped us accomplish for farmed animals around the world. </span>

<span style="font-weight: 400;">Presenters include Moby and Evanna Lynch and special guests include Kate Mara, Rooney Mara and Dave Navarro.</span>

<span style="font-weight: 400;">Held at the luxurious and iconic Beverly Hilton Hotel in Beverly Hills, California, </span><a href="https://animalequality.org/"><span style="font-weight: 400;">we’ll be honoring special individuals</span></a><span style="font-weight: 400;"> who have dedicated their lives to bringing about a more compassionate world for farmed animals.</span>

<span style="font-weight: 400;">Last year’s event sold out so, please, </span><a href="https://animalequality.us9.list-manage.com/track/click?u=ab553ae64c4718f9ed738dac9&amp;id=410a64737f&amp;e=2a4672a073"><span style="font-weight: 400;">purchase your tickets today!</span></a>

<span style="font-weight: 400;">Your support of this event will help us continue our life-saving work for farmed animals all over the world.</span> <span style="font-weight: 400;">We look forward to celebrating with you!</span>
