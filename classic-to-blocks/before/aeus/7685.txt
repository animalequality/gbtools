Animal Equality’s latest investigation reveals shocking scenes of cruelty and suffering on British pig farms – including female pigs caged in tiny ‘sow stalls’ for days on end, a practice banned in the UK since 1999.

https://www.youtube.com/watch?v=sjToGyWEk-A

In September and October 2017, Animal Equality investigators made multiple visits to&nbsp;<b>Hall Farm in Norfolk</b>. On each visit, they recorded female pigs in tiny cages, known as sow stalls. Hidden cameras placed by the investigators revealed one&nbsp;<b>group of pigs were continuously confined for at least 127 hours</b>&nbsp;(5+ days). Footage of bar biting and aggression demonstrates pigs were extremely distressed in these cages.

Further suffering found at Hall Farm, which&nbsp;<b>sells pigs to HG Blake – a Norfolk abattoir supplying London’s Smithfield Market and butchers in East Anglia</b>, included a pig with a major abscess on its back and another with an infected leg wound, both left in a pen with other pigs, and most pigs kept in barren, crowded pens that deny their natural behaviours. All pigs also had their tails amputated (docked), a painful mutilation performed without anesthetic that&nbsp;<b>should not be done routinely under UK law.</b>

<img class="alignnone wp-image-7692 size-full" src="https://animalequality.org/app/uploads/2018/06/news_hg_blake_large_abscess.jpg" alt="" width="600" height="410">

In September 2017, our investigators visited&nbsp;<b>Poplar Farm&nbsp;</b>in the East Riding of Yorkshire<b>, a Morrison's supplier</b>. They found multiple pigs with open, bleeding wounds on their ears; a severely lame pig left in a pen with other pigs; pregnant pigs kept in barren, concrete pens without any enrichment; several heavily scarred pigs and all pigs tail-docked. Two dead pigs were also left out in the farmyard overnight, an&nbsp;<b>imprisonable violation of legislation governing the disposal of dead animals.</b>

<img class="alignnone wp-image-7693 size-full" src="https://animalequality.org/app/uploads/2018/06/news_poplar_farm_ear-wounds.jpg" alt="" width="600" height="425">

We visited<b>&nbsp;Cross Farm in Devon</b>, owned by W J Watkins &amp; Sons –&nbsp;<b>National Pig Award winners in 2016 and 2017</b>, in October 2017 after a tip-off about poor conditions. Our investigators found pigs living in leaking, dilapidated buildings covered in the slurry and forced to lie in their own waste; two pigs with large hernias left in pens with other pigs; pregnant pigs kept in barren, concrete pens without any enrichment and all pigs tail-docked.

<img class="alignnone wp-image-7689 size-full" src="https://animalequality.org/app/uploads/2018/06/news_cross_farm_award-winning.jpg" alt="" width="600" height="458">

At&nbsp;<b>Grange Farm in Lincolnshire,&nbsp;</b>which our investigators also visited after a tip-off about welfare concerns, we found a decomposing deadpiglet left in a pen with its mother and siblings, young pigs kept in barren tiny pens barely bigger than a bathtub and all pigs tail-docked.

Pigs are the most protected farmed animals in Britain - on paper. UK law prohibits caging pigs in tiny sow stalls, routine mutilations like tail docking and keeping pigs in barren pens that deny their natural behaviors. Yet our investigation shows these prohibited practices are still happening on British farms.

<b>According to Professor Bo Algers, an expert on EU pig welfare legislation based at the Swedish University for Agricultural Sciences,&nbsp;</b>who reviewed footage from all four farms:

<i>“The pigs shown are not kept in accordance with the current animal welfare regulations. Pigs, kept under conditions such as those shown on the videos, do not have an acceptable level of welfare.”</i>

<strong>Chris Williamson, Labour MP for Derby North</strong>&nbsp;gave this response to viewing our report,&nbsp;"I encourage anyone to take a moment and think if&nbsp;this is really any way to treat these animals? Just look at the findings, especially the photos and videos in this report, and tell me that whether you think&nbsp;pigs should suffer like this? What I've read and seen in this report I find deeply upsetting. Much of this is illegal and is a human health risk as much as it's a matter of animal welfare.&nbsp;These conditions must end now.”

We have sent our evidence to the Animal and Plant Health Agency which is charged with investigating on-farm welfare violations. We are demanding they take long overdue action on pig farms who fail even to adhere to the limited protections that the law demands. We have also sent the evidence to Red Tractor which certifies all four farms.

<a href="https://animalequality.org.uk/act/protect-pigs">Sign our petition today</a>, Demanding Action for Britain’s Pigs!
