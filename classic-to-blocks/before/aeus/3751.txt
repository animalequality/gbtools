An extensive undercover investigation by Animal Equality inside 21 slaughterhouses in seven different Mexican states reveals extreme cruelty against animals.

In order to protect farmed animals from the abuse they are currently subjected to, <strong>we need legislation that protects them from suffering.</strong>

Our investigations team documented the horrible practices that are commonly used in slaughterhouses across Mexico, including in government-owned slaughterhouses in the states of Jalisco, Nuevo León, Aguascalientes, San Luis Potosí, Colima, Zacatecas, and Nayarit.

The images they captured show how these facilities constantly <strong>violate the Federal Animal Health Law</strong> without any legal repercussions.

<iframe src="https://www.youtube.com/embed/Q2qJIDOw-cU" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0" align="middle"></iframe>

Our investigators documented:
<ul>
 	<li>100% of the pigs are fully conscious while they are being stabbed. In most of the slaughterhouses, pigs are stabbed more than twice. As a result, pigs bleed to death on the ground.</li>
 	<li>Workers violently forcing pigs to move by beating and electrocuting them.</li>
 	<li>Workers killing more than three pigs at a time.</li>
 	<li>Some pigs step on others trying to escape the killing pen while workers are killing the other pigs.</li>
 	<li>None of the documented slaughterhouses used stunning methods for sheep and goats. These animals were violently slaughtered, with their legs tied up, dragged from one of their limbs across the floor, and beheaded.</li>
 	<li>Pigs being beaten with axes.</li>
 	<li>Cows being forced to move with electric prods, causing them excruciating pain and suffering.</li>
 	<li>Cows attempting to stand up while bleeding after waking up from stunning.</li>
 	<li>Underage children inside the slaughterhouses witnessing how animals are killed.</li>
 	<li>A worker violently tying a lamb, slitting his throat and ripping off his head while the animal is fully conscious.</li>
</ul>
<strong>This is what animals raised for human consumption face in Mexico every day.</strong>

The animal welfare laws that protect dogs and cats don’t include animals in farms and slaughterhouses. If someone did to a dog or a cat what factory farms do to pigs, cows, sheep, chickens, hens or goats, they would be imprisoned.

<strong>Unfortunately, there aren’t any laws that protect farmed animals from abuse</strong>. Although there are some regulations in Mexico, the law is not enforced and workers don’t face any legal consequences for torturing animals.

Currently, the Mexican Federal Criminal Code doesn’t include penalties for those who abuse or are cruel to animals, and the Federal Animal Health Law only applies for violations, comparable to a parking ticket, for slaughterhouses that don’t comply with the regulations. However, these laws don’t punish the person who commits acts of cruelty.

<strong>Please consider helping farmed animals by leaving them off your plate</strong>, and by <a href="https://www.animalequality.net/mexicanslaughterhouses/" target="_blank" rel="noopener"><strong>signing our petition</strong></a> demanding to punish those who abuse them.
