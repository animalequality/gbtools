Emma is another victim of animal exploitation for meat consumption. She was a mere few months old when she was hit and run over by a car in Utah, USA, right outside the ranch where she was being raised to then be slaughtered for her meat. She was left to die, which exemplifies the money-driven industry that is the meat industry. They didn’t care about her.

<img class="wp-image-4357" style="width: 560px;" src="/app/uploads/2015/04/CowPic41.gif" alt="" />
Now she's learning to walk on 3 legs as she unfortunately lost one of them in the aforementioned tragic accident. She was rescued by the rescue group at Farm Sanctuary as someone alerted them about the occurrence. She was rushed to a California-based hospital where she was given all the attention and care she required, thus leading to the amputation of her leg which suffered greatly in the accident.

<img class="wp-image-4358" style="width: 560px;" src="/app/uploads/2015/04/CowPic84.jpg" alt="" />

Emma reminds us that animals’ lives matter and that their thirst for life outweighs the tragedies that still enslave animals by commodifying them. The meat industry regarded Emma as waste, and hadn’t it been for a hero passing by, alerting Farm Sanctuary about this happening, she would have been yet another victim of the meat industry. Discarded and dumped.

<img class="wp-image-4359" style="width: 560px;" src="/app/uploads/2015/04/CowPic91.jpg" alt="" />

We would like to take the opportunity to remind our readers, that cows are calm and loving animals, capable of feeling pain and emotions. Emma’s life matters just as much as anyone else’s and it’s in our hands to ensure animals are respected as fellow friends.

<img class="wp-image-4356" style="width: 560px;" src="/app/uploads/2015/04/CowPic111.jpg" alt="" />
