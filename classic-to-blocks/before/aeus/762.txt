During today's&nbsp;plenary session&nbsp;the European Parliament&nbsp;agreed <strong>to ban subsidies to bullfighting&nbsp;with European funds. </strong>The amendment put forward by the parliamentary&nbsp;group;&nbsp;The Greens,&nbsp;passed&nbsp;on a vote of 438-199 with 50 abstentions. As a result, the 2016 European budgets will not include public aid for the breeding of bulls that will later be used in bullfights.

<img class="wp-image-4394 alignleft" src="/app/uploads/2015/10/La-crueldad-de-los-festejos-taurinos_17271486134_o2.png" alt="">

According to rough estimates, this cut is worth about £110 million&nbsp;(€150m, $168m) a year.

“A civilized and ethical Europe should&nbsp;not continue to promote and support&nbsp;bullfighting. Animal Equality congratulates the parliamentary group "The Greens" for the initiative and all the politicians who voted in favor of the amendment. This is an important step to end bullfighting in Spain.” said Amanda Romero, Director of Animal Equality in Spain.

For Javier Moreno, Executive Director of Animal Equality in Spain, “Bullfighting has been benefiting from these subsidies for many years. If they stop receiving public money, I have no doubt that their end will be very close. That is what most of the Spanish and European society want"

In June 2015, Animal Equality launched the international campaign&nbsp;<a href="http://www.animalequality.net/node/712">“Festejos Crueles" </a>(Cruel Celebrations)&nbsp;that showed the&nbsp;shocking cruelty behind&nbsp;bullfights. The campaign was presented in eight countries, gathering&nbsp;international attention. As a result,&nbsp;Animal Equality collected <a href="http://www.animalequality.net/node/709">over 120,000 signatures</a>&nbsp;in which tens of thousands of European citizens&nbsp;asked the EU Commission to immediately withdraw all funding from bullfighting.

Animal Equality has also carried out dozens of protests and events over the years demanding Spanish and European authorities to put an end to bullfighting.
