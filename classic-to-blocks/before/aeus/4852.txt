Each year at a village near to Hanoi, a pig is brutally chopped into two so that people can smear banknotes with the animal’s blood in the belief that it will bring them luck. On the sixth day of the first month of the lunar calendar, the festival is held at the Nem Thuong village in Bac Ninh, about 40 km north of Hanoi, to worship the village's deity Doan Thuong - an anti-royal military general who lived in the 13th century. This year, the festival was held on 28th January.

Each year, thousands of people from the village and from nearby villages gather to smear the blood of the pig on their bank notes in the belief that it would bring them luck in the New Year. Villagers play traditional music as they take part in a festival and a pig is carried around the village before being hacked in two with a sword by a participant. The festival is known as the most brutal in the country and is condemned by many, including some who have called upon the government to stop it.

There is also a petition to sign so that you can take action against this barbaric act of cruelty.

These festivals, rituals and other animal sacrifices, reinforce the wrong idea that humans have the right to use animals for legitimate needs. Animals are individual sentient beings like us, and we should all be able to choose our own lives and live in freedom, human or not.

Respect should be extended beyond our own species and incorporated into our daily lives, by not consuming any animal products.
