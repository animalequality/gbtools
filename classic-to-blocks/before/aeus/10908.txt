The vegan community is full of innovative and compassionate chefs who are using their immense culinary talents to create some of the most inspiring and delicious recipes that you can imagine.

In celebration of Black History Month, we’ve chosen to feature 5 vegan chefs who are serving up their own brand of compassionate cuisine.
<blockquote class="instagram-media" style="background: #FFF; border: 0; border-radius: 3px; box-shadow: 0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width: 540px; min-width: 326px; padding: 0; width: calc(100% - 2px);" data-instgrm-permalink="https://www.instagram.com/p/BsBuXsLF5aM/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12">
<div style="padding: 16px;">
<div style="display: flex; flex-direction: row; align-items: center;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div>
</div>
</div>
<div style="padding: 19% 0;"></div>
<div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;"></div>
<div style="padding-top: 8px;">
<div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;">View this post on Instagram</div>
</div>
<div style="padding: 12.5% 0;"></div>
<div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
<div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div>
<div style="background-color: #f4f4f4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
</div>
<div style="margin-left: 8px;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div>
<div style="width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg);"></div>
</div>
<div style="margin-left: auto;">
<div style="width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div>
<div style="background-color: #f4f4f4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div>
<div style="width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
</div>
</div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div>
</div>
<p style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; line-height: 17px; margin-bottom: 0; margin-top: 8px; overflow: hidden; padding: 8px 0 7px; text-align: center; text-overflow: ellipsis; white-space: nowrap;"><a style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px; text-decoration: none;" href="https://www.instagram.com/p/BsBuXsLF5aM/?utm_source=ig_embed&amp;utm_medium=loading" target="_blank" rel="noopener">A post shared by GREEDI VEGAN (@greedivegan)</a> on <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2018-12-30T21:42:01+00:00">Dec 30, 2018 at 1:42pm PST</time></p>

</div></blockquote>
<script async="" src="//www.instagram.com/embed.js"></script>
<a href="https://www.instagram.com/p/BsBuXsLF5aM/"><strong>LATISHA DARLING</strong></a>: A vegan chef and entrepreneur, Latisha is no stranger to self-made businesses, first creating Pieces (a boutique store in Prospect Heights, Brooklyn) and now <a href="https://www.greedivegan.com/">Greedi Vegan</a>, her wildly popular plant-based restaurant in Crown Heights. Latisha was raised vegan by her Rastafarian mother, and was inspired by her global travels and her family’s Southern roots to create the delicious food Greedi Vegan serves daily, food that she not only hopes excites vegans, but meat-eaters, too, “I want carnivores to come into my space, not feel intimidated, be curious about the food, love the food, and come back to explore the rest of the menu.”
<blockquote class="instagram-media" style="background: #FFF; border: 0; border-radius: 3px; box-shadow: 0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width: 540px; min-width: 326px; padding: 0; width: calc(100% - 2px);" data-instgrm-permalink="https://www.instagram.com/p/BC3M-GCL0Mf/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12">
<div style="padding: 16px;">
<div style="display: flex; flex-direction: row; align-items: center;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div>
</div>
</div>
<div style="padding: 19% 0;"></div>
<div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;"></div>
<div style="padding-top: 8px;">
<div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;">View this post on Instagram</div>
</div>
<div style="padding: 12.5% 0;"></div>
<div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
<div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div>
<div style="background-color: #f4f4f4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
</div>
<div style="margin-left: 8px;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div>
<div style="width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg);"></div>
</div>
<div style="margin-left: auto;">
<div style="width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div>
<div style="background-color: #f4f4f4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div>
<div style="width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
</div>
</div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div>
</div>
<p style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; line-height: 17px; margin-bottom: 0; margin-top: 8px; overflow: hidden; padding: 8px 0 7px; text-align: center; text-overflow: ellipsis; white-space: nowrap;"><a style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px; text-decoration: none;" href="https://www.instagram.com/p/BC3M-GCL0Mf/?utm_source=ig_embed&amp;utm_medium=loading" target="_blank" rel="noopener">A post shared by Bryant Terry (@bryantterry)</a> on <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2016-03-12T18:10:36+00:00">Mar 12, 2016 at 10:10am PST</time></p>

</div></blockquote>
<script async="" src="//www.instagram.com/embed.js"></script>
<a href="https://www.instagram.com/p/BC3M-GCL0Mf/"><strong>BRYANT TERRY</strong></a>: Activist, author, educator and chef - Bryant is an eco jack-of-all-trades and an amazing plant-based advocate whose numerous achievements have been tied to his compassionate lifestyle. Aside from being featured in <em>The New York Times</em>, <em>O: The Oprah Magazine</em> and <em>The Washington Post</em>, Bryant was named the inaugural Chef-in-Residence for the Museum of the African Diaspora in San Francisco in 2015. “If we move past the stereotypes of African American cuisine, the foundations are really healthful foods: nutrient-dense greens like mustards and turnips and kale and collards and dandelions, and butter beans and sugar snap peas and pole beans and black-eyed peas and sweet potatoes,” <a href="https://www.washingtonpost.com/lifestyle/food/bryant-terry-marries-vegan-cooking-and-african-food-traditions-with-delicious-results/2014/05/05/68092bea-d13a-11e3-937f-d3026234b51c_story.html?noredirect=on&amp;utm_term=.dd82f4dcc54a">Bryant explained to <em>The Washington Pos</em>t</a> on how he creates his cuisine. ”I just think we all can stand to eat more plant-strong foods.”
<blockquote class="instagram-media" style="background: #FFF; border: 0; border-radius: 3px; box-shadow: 0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width: 540px; min-width: 326px; padding: 0; width: calc(100% - 2px);" data-instgrm-permalink="https://www.instagram.com/p/5IOUeOvU45/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12">
<div style="padding: 16px;">
<div style="display: flex; flex-direction: row; align-items: center;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div>
</div>
</div>
<div style="padding: 19% 0;"></div>
<div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;"></div>
<div style="padding-top: 8px;">
<div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;">View this post on Instagram</div>
</div>
<div style="padding: 12.5% 0;"></div>
<div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
<div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div>
<div style="background-color: #f4f4f4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
</div>
<div style="margin-left: 8px;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div>
<div style="width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg);"></div>
</div>
<div style="margin-left: auto;">
<div style="width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div>
<div style="background-color: #f4f4f4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div>
<div style="width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
</div>
</div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div>
</div>
<p style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; line-height: 17px; margin-bottom: 0; margin-top: 8px; overflow: hidden; padding: 8px 0 7px; text-align: center; text-overflow: ellipsis; white-space: nowrap;"><a style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px; text-decoration: none;" href="https://www.instagram.com/p/5IOUeOvU45/?utm_source=ig_embed&amp;utm_medium=loading" target="_blank" rel="noopener">A post shared by Ama Opare (@foodforthesoul.opare)</a> on <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2015-07-14T19:35:32+00:00">Jul 14, 2015 at 12:35pm PDT</time></p>

</div></blockquote>
<script async="" src="//www.instagram.com/embed.js"></script>
<strong><a href="https://www.instagram.com/p/5IOUeOvU45/">AME OPARE</a></strong>: Ame is a vegan lifestyle coach and raw plant-based chef who founded <a href="https://foodforthesoul.opare.net/">Food for the Soul</a>, an online resource center for black vegans across the world. A lifetime educator and professional program developer, Ame partnered with her husband, Nana Kwaku Opare, MD, MPH, CA, to address growing health problems in their community. Ame also co-founded the Opare Institute, which provides help for people looking to adopt a plant-based lifestyle. In founding the Opare Institute, Ame says the reason is simple, “I’m a teacher at heart and I was eager to share what I had discovered to help other folks like me figure out how to make being plant-based work.”
<blockquote class="instagram-media" style="background: #FFF; border: 0; border-radius: 3px; box-shadow: 0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width: 540px; min-width: 326px; padding: 0; width: calc(100% - 2px);" data-instgrm-permalink="https://www.instagram.com/p/BTjxAx6lbnk/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12">
<div style="padding: 16px;">
<div style="display: flex; flex-direction: row; align-items: center;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div>
</div>
</div>
<div style="padding: 19% 0;"></div>
<div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;"></div>
<div style="padding-top: 8px;">
<div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;">View this post on Instagram</div>
</div>
<div style="padding: 12.5% 0;"></div>
<div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
<div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div>
<div style="background-color: #f4f4f4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
</div>
<div style="margin-left: 8px;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div>
<div style="width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg);"></div>
</div>
<div style="margin-left: auto;">
<div style="width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div>
<div style="background-color: #f4f4f4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div>
<div style="width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
</div>
</div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div>
</div>
<p style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; line-height: 17px; margin-bottom: 0; margin-top: 8px; overflow: hidden; padding: 8px 0 7px; text-align: center; text-overflow: ellipsis; white-space: nowrap;"><a style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px; text-decoration: none;" href="https://www.instagram.com/p/BTjxAx6lbnk/?utm_source=ig_embed&amp;utm_medium=loading" target="_blank" rel="noopener">A post shared by Aris LaTham (@arislife_)</a> on <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2017-05-01T17:54:26+00:00">May 1, 2017 at 10:54am PDT</time></p>

</div></blockquote>
<script async="" src="//www.instagram.com/embed.js"></script>
<a href="https://www.instagram.com/p/BTjxAx6lbnk/"><strong>Dr. ARIS LATHAM</strong></a>: Dr. Aris LaTham, who has been eating raw and vegan for 43 years, is considered by many to be the founder of gourmet ethical raw food cuisine in America. In 1979 in Harlem, New York, Dr. LaTham debuted his raw food creations when he started <a href="https://sunfired.com/">Sunfired Foods</a>, a live food company. In the years since the company’s founding, he has trained thousands of raw food chefs and created numerous delicious recipes. In explaining his diet, Dr. LaTham says, “The main thing that I want to always share with people is to be conscious about life. You cannot get life from death.”
<blockquote class="instagram-media" style="background: #FFF; border: 0; border-radius: 3px; box-shadow: 0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width: 540px; min-width: 326px; padding: 0; width: calc(100% - 2px);" data-instgrm-permalink="https://www.instagram.com/p/Bok9HIUHQ1O/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12">
<div style="padding: 16px;">
<div style="display: flex; flex-direction: row; align-items: center;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div>
</div>
</div>
<div style="padding: 19% 0;"></div>
<div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;"></div>
<div style="padding-top: 8px;">
<div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;">View this post on Instagram</div>
</div>
<div style="padding: 12.5% 0;"></div>
<div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
<div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div>
<div style="background-color: #f4f4f4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
</div>
<div style="margin-left: 8px;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div>
<div style="width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg);"></div>
</div>
<div style="margin-left: auto;">
<div style="width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div>
<div style="background-color: #f4f4f4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div>
<div style="width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
</div>
</div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div>
</div>
<p style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; line-height: 17px; margin-bottom: 0; margin-top: 8px; overflow: hidden; padding: 8px 0 7px; text-align: center; text-overflow: ellipsis; white-space: nowrap;"><a style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px; text-decoration: none;" href="https://www.instagram.com/p/Bok9HIUHQ1O/?utm_source=ig_embed&amp;utm_medium=loading" target="_blank" rel="noopener">A post shared by Babette Davis (@chefbabette)</a> on <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2018-10-06T03:58:40+00:00">Oct 5, 2018 at 8:58pm PDT</time></p>

</div></blockquote>
<script async="" src="//www.instagram.com/embed.js"></script>
<a href="https://www.instagram.com/p/Bok9HIUHQ1O/"><strong>CHEF BABETTE</strong></a>: 67-year-old Chef Babette Davis is a vegan comfort food specialist who serves up tasty food at her acclaimed plant-based restaurant, <a href="https://www.stuffieat.com/">Stuff I Eat</a>. A vegan and raw food chef for over two decades, Chef Babette is a firm believer that people should eat raw food and live consciously. She is also a committed and active health advocate, co-founding the Love Ur Age Project which provides health and wellness tips to people who may not have access to the tools that would allow them to live a healthier lifestyle. “Ever since I began sharing my cooking with the public at my restaurant, there has been an overwhelming response from my customers,” explains Chef Babette. “I am a firm believer that a quality life is the key to successful aging.”

HUNGRY YET?: These amazing chefs were just a sampling of the many, wonderfully creative plant-based cooks and tastemakers who are changing the world, one plate at a time. Want to learn more about living a vegan lifestyle?

<strong><a href="https://loveveg.com/">HEAD OVER TO LOVE VEG FOR GREAT TIPS AND RECIPES!</a></strong>

<em>Want to learn about other amazing black vegan activists? Visit <a href="https://www.instagram.com/black_vegans_rock/">Black Vegans Rock on Instagram</a>.</em>
