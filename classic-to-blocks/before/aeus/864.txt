Animal activists from Animal Liberation secretly captured footage of workers tormenting animals at a South Australian piggery. According to the <em>Daily Mail,</em> workers can be seen beating, kicking and riding pigs as they inseminate them.

<img class="wp-image-4256" src="/app/uploads/2016/02/316F4DD000000578-3457639-image-a-74_1456111396251.jpg" alt="" />

Filth, torture, anguish, helpless, inhumane – these words are often used to describe the images of animal abuse. These images you're about to see are no different! The footage taken at the Yelmah Piggery in Hamley Bridge shows firsthand the cruelties these creatures endure on a daily basis.

The workers treat these animals as if they were nothing; it's heartbreaking to watch, but it's even worse knowing that this happens across the world.

<a href="http://www.dailymail.co.uk/news/article-3457639/Brutal-footage-secretly-filmed-Australian-piggery-feral-cats-tear-apart-piglets-workers-beat-ride-sows.html#v-2431343928793648117">Click here to watch the shocking and disturbing video. </a>

&nbsp;

<img class="wp-image-4260" src="/app/uploads/2016/02/316F519600000578-3457639-image-a-65_1456111191974.jpg" alt="" />

The squeals alone are haunting and the brutality is difficult to watch – but it's important to know the truth!

<img class="wp-image-4258" src="/app/uploads/2016/02/316F4ED700000578-3457639-image-a-71_1456111314752.jpg" alt="" />

The animals in this piggery are often stuck in feces and live in unsanitary conditions which greatly affects their well-being.

<img class="wp-image-4257" src="/app/uploads/2016/02/316F4E0000000578-3457639-image-a-75_1456111417471.jpg" alt="" />

<img class="wp-image-4262" src="/app/uploads/2016/02/316FB2BA00000578-3457639-image-a-78_1456113516401.jpg" alt="" />

It was noted that a large group of feral cats would feast on newborn piglets. Bodies of partly devoured piglets were discovered and left to rot.

<img class="wp-image-4263" src="/app/uploads/2016/02/3170308900000578-3457639-image-a-95_1456121251958.jpg" alt="" />

Those who fell ill were killed and put into a carcass pit filled with other dead pigs and piglets.

Emma Hurst – Animal Liberation campaign director – was shocked by the number of pigs and piglets that were left to die. She stated, "There is no respect shown to these intelligent animals."

In a study conducted at the University of Cambridge, <a href="http://www.animalequality.net/node/744">it was found that pigs are highly intelligent beings that can gain knowledge to solve complex problems.</a> But when trapped in factory farms, they go through many physical and psychological changes.

<img class="wp-image-4259" src="/app/uploads/2016/02/316F4EF300000578-3457639-image-a-87_1456113692377.jpg" alt="" />

Chris Delforce who runs the website <a href="https://www.farmtransparency.org/campaigns/aussie-pigs">Aussie Pigs </a>told <em>Daily Mail Australia</em>, “Australia is touted as a global “leader” in animal welfare, but I think most people who see the conditions and practices of farming and slaughter would feel there’s nothing humane or ethical about it,” he continues, “For this reason these realities of modern farming are intentionally hidden from the public view.”

This type of abuse and practices are far too common in factory farming. By exposing the truth, we can strive for change.
