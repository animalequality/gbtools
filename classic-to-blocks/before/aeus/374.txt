Despite international protests and sudden death of hundreds of seals as a result of the warm weather and its effect on ice. The federal Department of Fisheries and Oceans announced late Tuesday that the commercial sealers could take to their boats this Thursday instead of next Monday as originally planned.

The hunt in the Gulf accounts for about 28 percent of the total quota of 400,000 seals that DFO said could be harvested year. Another two-thirds is allotted to the northeast coast of Newfoundland.

But even those sealers are expected to be fewer in number this year because the market for the pelts has gone dry. “We don’t know yet,” a spokesman for the Newfoundland government said Wednesday when asked if the hunt will go ahead in that province.

Trade in seal fur has plummeted in recent years as the U.S. all countries of the European Union, including more recently Russia, which was one of the largest importers of seal skins (up to 90% of total), have banned the trade of products derived from these animals. Even some members of the Parliament of Canada are publicly questioning whether to continue this annual slaughter.

&nbsp;

&nbsp;
