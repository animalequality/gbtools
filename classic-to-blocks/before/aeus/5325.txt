Perry Smith and his family took advantage of the intense snowfall by spending the day skiing at Whitetail ski resort in Pennsylvania. As they were on their way back to their hotel, they spotted an animal lying still on the side of the road.

According to Smith, the animal turned out to be a baby pig that was visibly suffering from the freezing conditions. After asking local homeowners if they knew where the pig came from, Smith decided to rescue him. He told ABC News, "I told the kids, 'Listen we don't have any choice but to take this pig in. We're gonna see if we can warm him up and we'll deal with it when we get to the hotel.'"

The family wrapped the adorable pig in a sweatshirt and snuck him into their hotel to help him recover from the cold. The pig – which they have named Wee Wee – was placed comfortably in the bathtub where he eventually was able to eat, drink, and get warm. He seems to be recovering well.

<img class="wp-image-4375 alignleft" src="/app/uploads/2016/01/HT_smith_family_piglet_wee_wee_4_jt_160124_4x3_992.jpg" alt="" />

Poplar Springs Animal Sanctuary in Maryland has agreed to adopt the pig where he will get another chance at life. We applaud the Smith family for showing compassion and doing this tremendous act of kindness.
