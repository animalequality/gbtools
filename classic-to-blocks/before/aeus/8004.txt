Actress and animal advocate Kate Mara, most widely known for her roles in House of Cards, 24, and American Horror Story: Murder House, features on the front cover of our <a href="https://animalequality.org/app/uploads/2018/06/TheirVoice_6_AnimalEquality_US.pdf">latest donor magazine</a>.

<a href="https://animalequality.org/app/uploads/2018/06/TheirVoice_6_AnimalEquality_US.pdf"><img class="alignleft wp-image-8010 size-full" style="float: left; margin-right: 20px; margin-bottom: 20px;" src="https://animalequality.org/app/uploads/2018/06/their_voice_6_thumbnail.jpg" alt="" width="612" height="792" /></a>Warm, positive and generous with her time in support of Animal Equality’s work, Kate proudly modeled her sister Rooney Mara’s vegan clothing brand Hiraeth, for the cover of Their Voice.

In addition to talking about her starring role in Pose, Ryan Murphy’s new high-profile series for FX, Kate shared her vegan journey, go-to foods and top tips for new vegans.

<strong>Who or what inspired you to go vegan and were you able to make the transition immediately, or was it a more gradual process?</strong>
I have always been an animal lover and was a vegetarian for a while off and on. When I read my friend Kimberly Snyder’s book called ‘The Beauty Detox Solution’ it solidified my belief that we aren’t meant to consume animal products (whether you’re an animal lover, activist or not). Learning about the process that our bodies go through to digest meat and dairy was eye-opening for me. My love of animals and animal activism has grown over the years so it was a no-brainer for me to become a vegan. It took me a little while to fully give up cheese; otherwise, it was easy.

<strong>When did you first connect with animals?</strong>
I grew up with dogs and lived on my grandparents' farm for a short time as a child, so I’ve always had a passion for animals. I was a horseback rider and my mom has had therapy dogs throughout my life so I actually feel uneasy when there aren’t animals around.

Read the rest of <a href="https://animalequality.org/app/uploads/2018/06/TheirVoice_6_AnimalEquality_US.pdf">Kate Mara's interview in our magazine</a>.
<p style="font-size: 0.8rem;">Photo: Don Flood
Hair: Mara Roszak for Starworks Artists
Makeup: Coleen Campbell-Olwell for Exclusive Artists
Wardrobe: Hiraeth</p>
