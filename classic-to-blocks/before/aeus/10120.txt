Our recent demonstrations have compelled hundreds of activists to join our campaign, and our message has had an estimated reach of millions of concerned and compassionate people.

In Los Angeles, 250 protesters stood silently in front of a McDonald’s restaurant to ask for basic chicken protections. In New York City, actor and activist Edie Falco joined our campaign along with her son to call on the company to end chicken abuse. And in Chicago we participated in many weeks of action, protesting at McDonald’s headquarters and marching through the city’s streets asking for change. Those are just some examples of the reach of our campaign. <strong>Watch the video below to see our impact in 2018!</strong>
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/PqAOInpeiQ0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"><span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span><span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span><span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span></iframe></div>
<strong>To make sure McDonald’s ends the suffering of millions of chickens, we need your support today.</strong>

<a href="https://animalequality.org/donate/"><strong>STOP #MCCHICKENCRUELTY</strong></a>

Your donation today will allow us to keep the pressure on McDonald’s and will help to improve the lives chickens everywhere. With your support we will make a difference and end this abuse once and for all.

<a href="https://animalequality.org/donate/"><strong>BONUS: All contributions made from now until the end of the year will be DOUBLED by a generous donor!</strong></a>

Aside from financial support, we also need more dedicated volunteers to help our campaign. With just a few minutes a week, you can make a big difference in the lives of farmed animals by taking part in easy, online actions that can change the world. <a href="https://animalequality.org/animal-protectors/"><strong>Join our Animal Protectors team today and use your voice for good!</strong></a>

Thank you for everything you are doing to make the world a better place for farmed animals.
