Animal Equality has captured <strong>never before seen</strong> drone images of pig factory farms in Catalonia, which are responsible for serious environmental problems due to the pollution of aquifers and rivers throughout the region.

<img class="wp-image-4128" style="width: 550px; " src="/app/uploads/2017/07/es-1.jpeg" alt="">

Catalonia is the region with the largest concentration of pig farms in Spain. It’s also the region with the largest amount of soil that has been officially declared vulnerable due to the excess of nitrates coming from pig waste. Police and environmental agencies in Catalonia frequently report intentional dumping of waste into rivers and sewage.

The images show “waste lagoons” which are giant outdoor pools located near factory farms where animal waste is accumulated. The overflow of these pools and the over-fertilization of agricultural soils with purine are the cause of a serious growing environmental and public health problem due to the <strong>nitrate pollution of drinking water.</strong>

A March 2017 report by Food &amp; Water Europe highlights the seriousness of a problem that challenges the economic interests of the powerful Catalan pig meat industry with environmental and public health. In fact, the Catalan government <strong>invests more than 6 million euros</strong> a year to supply drinking water to affected residents.

He adds, “The high concentration of pig farms in certain regions, especially in Catalonia, poses a serious environmental and public health issue due to the pollution of aquifers with nitrates and phosphates caused by the over-fertilization of agricultural soils.”
