Animal Equality has just launched a new campaign exposing the cruelty inside caged hen farms.

Some of the most shocking conditions include:
<ul>
 	<li>Hens living inside cages among<strong> decomposing bodies.</strong></li>
 	<li>Containers full of dead and dying hens.</li>
 	<li>Hens with mutilated beaks (standard practice on many farms).</li>
 	<li>Sick hens, abandoned on the ground, <strong>struggling to access food and water.</strong></li>
 	<li>Featherless hens as a result of stress due to the crowded conditions.</li>
</ul>
<iframe src="https://www.youtube.com/embed/G7Jh_Uk9rgM" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe>

This type&nbsp;of cruelty is common on caged egg farms that keep tens of thousands of hens caged in gigantic industrial sheds. The consequences are terrible for birds.

They can not express their natural instincts. <strong>They are forced to step on metal bars throughout their life</strong>, which causes them pain. In some cases, the hens become trapped between the bars, slowly dying without access to food or water. Due to the stress of living in a packed cage, hens tend to peck at each other, causing wounds that <strong>don't receive any veterinary care.</strong>

"Raising hens in cages is a system that causes immense suffering to the animals. Consumers have the right to know this information, and Animal Equality will work tirelessly to end this cruel system," said Javier Moreno, international director of Animal Equality.

And with all the <a href="https://loveveg.com/compassionate-options/eggs/">delicious cruelty-free egg alternatives</a> available it's easier than ever to ditch eggs, and this cruelty, from your diet.
