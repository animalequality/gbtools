You may know Danny Trejo from the “Machete” movies and the hit television show Breaking Bad, but now he is adding something new to his resume – restaurant owner. He is opening up a new taco restaurant along with film producer Ash Shah and Jeff Georgino called Trejo’s Tacos.

<img class="wp-image-4671" src="/app/uploads/2016/01/la-trejo-s-tacos-20160111-0011.jpg" alt="" />

Located on La Brea Ave. in Los Angeles, Trejo’s Tacos will have a fantastic vegan and gluten-free menu. Shah stated in the LA Times, “We try to be as vegan and vegetarian-friendly as possible, considering it’s such a big movement now.”

The menu will include black pepper tofu tacos served with serrano chiles, Austin-style fried avocado tacos with Rancho Gordo black beans, organic chopped kale salad, and organic “super rice” which is made with quinoa and brown rice. Yum!

<img class="wp-image-4672" src="/app/uploads/2016/01/la-trejo-s-tacos-20160111-007.jpg" alt="" />

Trejo’s Tacos is scheduled to have a soft opening early next week, then officially open the following week. If you’re in the Los Angeles area, be sure to stop by to try some tasty cruelty-free tacos.

Trejo’s Tacos is located at 1048 S. La Brea Ave., Los Angeles, <a href="http://www.trejostacos.com/">www.trejostacos.com</a>

Photos: Los Angeles Times
