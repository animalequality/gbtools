Animal Equality investigators visited<strong> 4 ‘traditional’ farms located in the south-west of France </strong>and<strong> 5 farms in Catalunya, Spain</strong>.<strong>  </strong>

One of the farms investigated supplies the company <strong>Collverd</strong>, which is owned by the<strong> President of the Spanish foie gras industry (Interpalm).</strong>

In Europe, <a href="https://animalequality.org/blog/2022/02/08/what-is-foie-gras/">foie gras</a> is produced in only five countries: <strong>France, Bulgaria, Spain, Hungary, and Belgium.</strong> These countries have formed the European Foie Gras Federation since 2008.

Ducks and geese exploited to produce foie gras are subjected to the agony of routine force-feeding so that farmers can obtain from them diseased, fatty livers (hepatic lipidosis).

Dozens of hours of video and audio, and more than <strong>500 photographs</strong> obtained by Animal Equality document the shocking reality of life for ducks and geese confined and force-fed for foie gras production in France and Spain.
<ul>
 	<li><strong>Animals confined</strong> to small cages in which the animals could not even turn around.</li>
 	<li>Animals with clear <strong>signs of stress and depression.</strong></li>
 	<li>Evidence of trauma and inflammation of the esophagus - recognized by <strong>blood stains on force-feeding tubes.</strong></li>
 	<li>Animals with obvious <strong>respiratory problems.</strong></li>
 	<li>Ducks, who appeared fully aware of their situation at the time of slaughter. These<strong> animals were flapping, kicking, and bleeding incessantly.</strong></li>
 	<li>Workers <strong>roughly handling animals.</strong></li>
 	<li>Ducks <strong>moving with difficulty</strong> due to the size of their livers.</li>
</ul>
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/SzwV2ZwV1g4" width="800px" height="450px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<strong>France is the largest producer</strong> and exporter of foie gras. Over 20,000 tons are produced and approximately 700,000 geese and 37 million ducks are slaughtered by the French foie gras industry each year.
<div class="ae-video-container"><iframe src="https://player.vimeo.com/video/45216589?title=0&amp;byline=0&amp;portrait=0&amp;color=c0e7f8" width="800px" height="450px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<strong>Over 4,200 tons are consumed in Spain</strong>, and 850 tons are produced. Over 1,150,000 ducks are slaughtered by the Spanish foie gras industry each year.

Appallingly, whilst foie gras production was banned in the United Kingdom during 2000, it is still legal to import foie gras products into the country. The UK has imported a total of 6,388,543 kg between 2007 and 2011, and also exported 2,547 kg during 2011.

<strong>Animal Equality is committed to ending foie gras everywhere. You can help ducks and geese by <a href="https://animalequality.org/action/foie-gras">signing our petition to ban foie gras in the US</a>.</strong>
