<h3><strong>They feel pain</strong></h3>
Research has shown that fish feel pain and stress and fishing practices usually involve evisceration, starvation, and asphyxiation. Delicate sensitive animals don’t belong on our plates.

https://www.youtube.com/watch?v=GPlDrCOJUCg
<h3><strong>They’re smart</strong></h3>
Fish are intelligent animals and capable of forming special relationships with others. They’ve even been observed playing games and using tools.
<h3><strong>Overfishing</strong></h3>
The fishing industry is causing a global crisis and is also perhaps the single greatest threat to these delicate animals. Overfishing is responsible for some of the greatest environmental degradation on Earth and causes irreversible damage to all sorts of creatures.

A 2006 study found that if current fishing rates continue apace, the planet’s fisheries will most likely collapse by 2048.
<h3><strong>They’re loaded with contaminants</strong></h3>
Fish are on factory fish farms are fed a diet of mostly chicken feces, their bellies can become filled with plastics and other debris,&nbsp;and according to multiple reports, many types of fish are treated with illegal antibiotics.

Perhaps most alarming though are the dangerous levels of mercury that are found in some species of fish. Researchers have found that bluefin tuna contain up to 10 million times more mercury than the seawater they’re from!
<h3><strong>Bycatch</strong></h3>
The fishing industry is so indiscriminate and careless that bycatch is a global issue. It leads to more than 300,000 whales, dolphins, and porpoises dying after being ensnared in giant fishing nets. A statistic that really illustrates this:

For every one pound of shrimp that ends up on people’s dinner tables, 26 pounds of other sea animals were killed and tossed back into the sea.

&nbsp;
<h3><strong>People suffer too</strong></h3>
Workers in the industry are often exploited as well. Some work 20 hour days for less than $1 per day while others are forced into slave labor and live aboard ships for months at a time.

As a consumer, you&nbsp;can remove your support for the cruel and damaging fishing industry simply by leaving fish and other marine animals off your plates.
