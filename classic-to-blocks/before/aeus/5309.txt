Fish have been known to be the healthy alternative to beef and chicken. While there may be some health benefits from consuming fish, the negative effects could have a greater impact on your health, the environment, and of course, the animals.

<strong>The Animals</strong>

Fish are highly intelligent creatures with an excellent memory and individual personalities. They each have a nervous system that can feel pain similar to other animals. Now more than ever, these animals are being raised on fish farms where they are trapped in small, crowded tanks. Here, they endure filthy conditions, suffer from diseases, predation, and other forms of cruelty. According to NBC News, a frightening estimate showed that our oceans could be emptied of fish by the year 2050. Prevent this from occurring and stop eating these beautiful creatures.

<strong>Your Health</strong>

Many believe fish are beneficial to our health, but please note, the fish you consume have been swimming with toxins mostly due to human pollution. Mercury, arsenic, and lead are some of the poisons found in ocean animals. Fish can have high levels of chemical residues that can be extremely harmful to your body.

Our oceans are also filled with bacteria and polychlorinated biphenyls (PCBs), which can cause cancer, affect the immune system, neurological development, and reproduction. It's the safest choice to leave fish off your plate.

<strong>The Environment</strong>

Commercial fishing and fish farms are taking a dramatic toll on our environment by essentially creating an underwater wasteland. These practices create a ton of pollution in the ocean (more so than humans), including chemicals, feces, antibiotics, and garbage. It's ruining the coral reefs and the rest of the aquatic ecosystem.

The fish industry is destroying the marine life, our environment, and our overall health! To help change that, choose cruelty-free foods.
