Today April 24, coinciding with <a href="https://en.wikipedia.org/wiki/World_Day_for_Laboratory_Animals">World Day for Animals in Laboratories</a>, Animal Equality releases new undercover footage of the shocking experiments animals are subjected to in Spain.

Animal Equality’s investigators have documented the practices carried out on mice at the Medical School of the Complutense University of Madrid, Spain. Undercover investigators also filmed the experiments on pigs at the Gregorio Marañón Hospital.

Mice, one of the animals most commonly used in experiments, are often subjected to genetic mutation studies. As these animals reproduce so quickly, many of them are discarded. One of the methods used to kill them is by breaking their neck.

In most universities, the idea that animals are objects is ingrained early on. There is virtually no information about the possibility of objecting to these practices. Contrastingly, most medical schools in the United States, including acclaimed colleges such as Harvard, Yale or Stanford, have already replaced the use of animals in courses such as pharmacology, and physiology, as well as in surgical procedures.

<strong>WARNING GRAPHIC CONTENT</strong>
<div class="media_embed"><iframe src="https://www.youtube.com/embed/kNInsgG6eDg?rel=0&amp;controls=0" width="580px" height="326px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
Footage also shows students dissecting an alive pig and splitting his sternum with a hammer in order to observe various internal organs as well as learning how to stitch it. After this procedure, the animal was killed.

The video includes groundbreaking images of a facility in Camarles, a primate breeding and supply center in Tarragona, Spain, as well as the rescue of 36 beagles bred for experiments by Animal Equality supporters from Sant Feliu de Codines.

Camarles can hold around 3,500 long-tailed macaques. These macaques are flown to laboratories across the world in the cargo of companies such as Air France.

Laura Gough, campaign manager at Animal Equality UK stated <em>“It’s paramount we invest in the development of non-animal techniques as every time more citizens and scientific bodies demand more humane alternatives”</em>

Each year, 11,5 million animals are used in experiments in the European Union. In Spain, According to the Ministry of Agriculture, Food and Environment, this figure is 1.5 million. More than 4 million animals are used in experiments in England every year. In the US, the numbers are much higher, more than 100 million animals are tested on each year.

Animal Equality believes that the public has a right to know what is happening behind the laboratory walls and the debate on animal testing is necessary and urgent.

Next month at the European Parliament in Brussels, there will be a Public Hearing of the<a href="http://www.stopvivisection.eu/"> “Stop Vivisection”</a> Initiative in which Animal Equality actively participated in, amongst other organizations. This European Citizens’ Initiative has collected over one million signatures and we are calling for the repeal of the current Directive 2010/63 / EU, thus potentially leading to an end to animal testing in Europe.

World Day For Animals In Laboratories was established in 1979 by the British National Anti-Vivisection Society (NAVS). The date 24 April was chosen as it commemorates the birthday of former NAVS president Hugh Dowding, 1st Baron Dowding.
