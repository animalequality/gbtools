Men's singles tennis champion, Novak Djokovic, opens a 100% plant-based restaurant with his wife Jelena Djokovic called Eqvita.

The two share a passion for healthy food which resulted in a restaurant that nourishes the mind, body, and soul. "Eqvita is more than a restaurant. It's a concept. A story," Djokovic stated. "A love story to be precise."

Located in Monaco, the menu will include hearty salads, wraps, lasagna, cold pressed juices, and delicious desserts – all cruelty-free!

While the restaurant officially opens in a few weeks, a pre-opening was held this past Sunday that featured fellow tennis players Andy Murray, Tomas Berdych, and Grigor Dimitrov.

According to its Facebook page, "Eqvita was created out of love, gratitude and deep esteem towards the nature, its effects on human body and mind, and the way they flawlessly interconnect to create a healthy, happy person we can all be."

We have no doubt that Eqvita will be a smashing success and inspire many to live a healthy, animal-free life.

For more information, visit their <a href="https://www.facebook.com/eqvita.restaurant/timeline">Facebook page. </a>
