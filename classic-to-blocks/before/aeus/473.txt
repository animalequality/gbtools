The activists were joined by Holocaust survivor Alex Hershaft, who also founded NGO Farm Animal Rights Movement (FARM).

The event, part of World Farm Animals Day on 2nd October, was meant to honor '<em>the 10 billion land animals killed in the U.S. each year, which animal advocates call Holocaust of the Animals',</em> a FARM press release said.

Hershaft continued to compare the plant to Nazi death camp Auschwitz in a press release.

<em>“I see a striking parallel between the deceptive bucolic images of pigs cavorting in green meadows on Farmer John’s murals and the cynical inscription ‘Work makes you free’ over the gate to Auschwitz,” </em>Hershaft said in the release.

<em>"Obviously, I am not equating the millions of my fellow Jews slain tragically in the 1940s and the millions of pigs slaughtered every week for U.S. dinner tables, for we differ in many ways. Yet,
we all share a love of life and our ability to experience many emotions, including affection, joy, sadness, and fear,"</em> Hershaft said.

<em>"I do see a striking parallel in the mindsets of both sets of oppressors: their self-image as upstanding members of their communities, their abject objectification of their victims, their callous use of
cattle cars for transport, their continuous refinement of killing line technology, their preoccupation with record keeping and cost-effectiveness, their eagerness to hide and masquerade their horrendous deeds,”</em> Hershaft said.

Hershaft traveled to L.A. from Washington, D.C. for the protest.

The L.A. Farmer John facility is the largest pig slaughterhouse on the West Coast, killing more than 6,000 pigs a day and 1.5 million pigs per year, according to FARM.

The trucks carrying pigs had to be rerouted to a holding facility.

World Farm Animals Day is on 2nd October each year, the birthday of Mahatma Gandhi in India and it began in 1983.

No matter where in the world it exists, animal farming industry is based on exploitation and suffering. Watch videos of Animal Equality's groundbreaking investigations into the British pig industry:
<div class="media_embed"></div>
<div class="media_embed"><iframe src="https://player.vimeo.com/video/35604854?title=0&amp;byline=0&amp;portrait=0&amp;color=00c4ff" width="500px" height="281px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
&nbsp;
