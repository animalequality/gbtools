Animal Equality is delighted to learn that Paraguay has banned using wild animals in circuses! Resolution 2002/12 was passed this week by the Secretary of the Ministry of the Environment in Paraguay. In South America, bans are already in place in Bolivia and Peru. In fact, in 2009, Bolivia became the first country in the world to ban wild and domestic animals in circuses.

The South American country's environment secretary has stated that “<em>wild animals must be protected and can no longer be used in entertainment shows, even If they're tamed”.</em>
