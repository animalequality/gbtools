Wednesday morning, the Hayward Police Department received an unusual call when someone reported a stranded animal flopping around in the bushes near a business in Hayward, California.&nbsp;At first, it was believed to be a cat or possum but ended up being a male Northern Fur Seal pup. Apparently, the seal pup managed to waddle across Interstate 880 before ending up on the eastern side of Hayward – a town&nbsp;in Northern California.

The Marine Mammal Center in Sausalito responded to the located and rescued the seal pup.&nbsp;He appeared to have suffered from dehydration and is slightly&nbsp;malnourished, but is expected to recover. They will release the pup –&nbsp;named Pipester –&nbsp;back&nbsp;into the wild once he is fully healthy.

<img class="wp-image-5321 alignleft" src="/app/uploads/2016/01/1_2.jpg" alt="">

Laura Sherr, a spokesperson for the center, told KTVU, "It looks like he had a hard time finding food and then lost his way."&nbsp;According to experts, warmer ocean water has led fish to move to colder areas&nbsp;which is making it difficult for seals and sea lions to find food.

This isn't the first time the center has rescued Pipester. The pup was rescued back in November and was treated for malnourishment after he was found at Moss Landing Harbor. He spent several weeks recovering at the center&nbsp;before he was released&nbsp;into the ocean.

We hope Pipester makes a full recovery and will live a fulfilling life in his natural habitat.
