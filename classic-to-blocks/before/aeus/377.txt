On Friday, the district named Casablanca in Zaragoza issued a statement in favor of banning wild animals from circuses. The proposal was approved by the majority of parties and had a wide public support. Leticia Crespo, the District Board President, stated that "it is irrational to allow the use of animals in circuses, especially when we are aware of the suffering to which they are subjected."

"The District Board facilitates direct citizen participation, a way to facilitate hearing and considering citizen voices. This way we are able to make the best decisions taking into account citizens opinions. There were no votes against, and no group or organization expressed its opposition to this new measure." Leticia Crespo stated.

"We hope the City Hall also takes note of this decision. We want a new concept of circuses that maintain their spirit but without mistreating or causing pain towards any animal."
