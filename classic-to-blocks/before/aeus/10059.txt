In August 2017 and March and August 2018, Animal Equality investigators re-visited Mexican egg production facilities in the state of Jalisco. The newly captured footage, narrated by actor Eugenio Derbez, shows the cruel reality caged hens have to endure during their short lives.

Warning: disturbing content. Viewer discretion is advised.

https://www.youtube.com/watch?v=hdyWm5jMyP8

"Eliminating cages is a first step that helps to significantly improve the lives of animals that suffer in caged confinement all their lives. Hens are intelligent and have complex emotions. Through our investigations we can help millions of them by exposing the abuse that results from consumers and businesses perpetuating this cruel system," said Dulce Ramírez, Executive Director of Animal Equality in Mexico.

Once again, this<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157702788103155"> new investigation</a> uncovers the disturbing caged system, reaffirming the cruelty captured in<a href="https://www.youtube.com/watch?v=A2H1GeEHEEE"> Animal Equality’s first investigation</a> into Mexican egg facilities launched in 2016.

The<a href="https://www.youtube.com/watch?v=hdyWm5jMyP8"> devastating footage</a> shows multiple hens crammed together inside cages, many of whom are sick and dying. You can see hens enduring forced molting, a hen being crushed to death under other hens and decomposing corpses inside and outside of the facilities. Currently, more than 200 million hens are living inside cages in Mexico.

Please<a href="https://lavidaenunajaula.igualdadanimal.mx/"> sign our petition</a> to get hens out of cages in Mexico.
