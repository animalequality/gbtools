New undercover footage shot inside a gestation-crate facility that supplies to Tyson - one of the world's largest meat processors, has been released by&nbsp;<em>The Humane Society of the United States.</em>

<strong>The shocking investigation reveals:</strong>
<ul>
 	<li>Workers punching and kicking mother pigs and piglets, and smashing them into the concrete floor.</li>
 	<li>Employees throwing piglets' testicles at each other and feeding them back to the mother pigs for "fun."</li>
 	<li>Pregnant pigs suffering from severe rectal and uterine prolapses without proper veterinary treatment.</li>
 	<li>Dead and decaying piglets - including some who were mummified - littering the facility, including in piles next to mother pigs in crates.</li>
 	<li>Dead pigs left to rot in their gestation crates for days. One dead pig was half-buried in grain from an automatic feeder.</li>
 	<li>Mother pigs confined in tiny metal gestation crates, unable to turn around for nearly their entire lives.</li>
</ul>
&nbsp;
<div class="media_embed"><object width="560px" height="315px"><param name="movie" value="https://www.youtube.com/v/bNY4Fjsdft4?version=3&amp;hl=en_US"><param name="allowFullScreen" value="true"><param name="allowscriptaccess" value="always"><embed allowfullscreen="allowfullscreen" allowscriptaccess="always" height="315px" src="https://www.youtube.com/v/bNY4Fjsdft4?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="560px"></object></div>
Animal Equality's <a href="http://www.britishporkindustry.co.uk/">investigations into pig farms</a> have revealed similarly horrific scenes. Our last undercover investigation exposed two 'Freedom Food' farms at&nbsp;<a href="https://www.youtube.com/watch?feature=player_embedded&amp;v=TUxg93ghBVA"><strong>East Anglian&nbsp;Pig Company</strong></a>, the third-largest pig producer, in the UK, which supplies <strong>Cranswick Plc</strong>. - a leading UK food supplier that manufactures and supplies products to the food service sector, food producers and grocery retailers within the UK, such as <strong>Morrisons, Sainsbury’s, Tesco, The Cooperative Food, Waitrose</strong>, and <strong>ASDA</strong>.

Even the most extreme cruelty documented in these facilities, such as killing piglets by force blunt trauma is considered standard and acceptable within the industry.

After publishing the investigation and findings none of the farms or workers will be prosecuted.

Animal Equality shows with these investigations, that regardless of whether animal farming is labeled as ‘High-Quality Assured’ or ‘Freedom Food’, there exists pain, suffering, and exploitation on a huge scale. We do not need to contribute to the institutionalized deprivation, humiliation, imprisonment, and suffering that occurs each day to billions of sentient beings. Without the public demand for animal products, a great amount of misery and exploitation can be avoided.

Consider removing your financial support from an industry that abuses animals for profit by adopting a vegan diet! Visit <strong><a href="https://loveveg.com">ChooseVeganism.org</a></strong> for more information and delicious recipes to get started on your journey toward cruelty-free living.

&nbsp;
