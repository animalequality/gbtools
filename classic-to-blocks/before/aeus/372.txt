Animal Equality is pleased to announce that this week Switzerland banned not only the keeping of dolphins in zoos and marine park, but also their import into the country!

On Tuesday, the keeping of dolphins and whales in zoos and waterparks was prohibited, and on Wednesday, the Senate then opted for a ban on the import of all cetaceans. This is a major victory for dolphins, and came following a public outcry over the death of two dolphins last summer at the Conny Land amusement park. Their deaths followed the deaths of eight others at the facility during just three years.

Sadly, three bottlenose dolphins (Tursiops truncatus) remain incarcerated in Switzerland, and it is reported that this park will operate as normal this summer. Following this recent decision by the Senate however, these animals now cannot be replaced with other dolphins.

In many countries around the world, dolphins are treated as slaves to the entertainment industry. Yet their capture from the wild and incarceration in tanks is now widely considered as unnacceptable. In the wild, dolphins have home ranges as large as 300 kilometers, and can swim up to 1076 km over 20 days. Orcas can dive as deep as 60 metres and travel as far as 160 km in a single day. Both dolphins and whales have complex social structures and are supreme hunters.

A life within the stagnent, chlorinated water of a small, blue-painted tank can be described as deprived at best. Captivity induces depression, sickness, and an early death.

<a href="http://www.spanishzoos.org">Animal Equality’s 2011 'Caged Lives' investigation</a> revealed a disturbing pattern of dolphins being bred and transferred between different Spanish zoos and marine parks seemingly to replace individuals who had died. We documented the deep abrasions on the rostrums of dolphins at Madrid Zoo, likely to be a consequence of trainers standing on the animals’ faces during training sessions and performances, or the animals hitting their faces against tank walls.
&nbsp;
