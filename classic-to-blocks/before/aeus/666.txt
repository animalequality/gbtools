This footage exposes the Chinese fur farms where a distant relation to dogs are skinned alive en-masse and turned into luxury coats sold in the West. Animal Equality activists carried out an undercover investigation into several farms and markets near Beijing, for over the period of three weeks.
<div class="media_embed"><iframe src="https://www.flickr.com/photos/animalequalityuk/14947484693/in/set-72157648430167259/player/" width="500px" height="333px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
Raccoon dogs are a type of canine but the breed is described as 'Asiatic raccoon' on luxury coat labels, partly due to off-putting comparisons with domestic pets.

Animal Equality investigated two farms, each holding 300 or 400 raccoon dogs, animals were:
<ul>
 	<li>Confined to feces-filled cages barely larger than their bodies.</li>
 	<li>Dead raccoon dogs were fed to those still alive.</li>
 	<li>Animals showed signs of 'mental and physical illness'.</li>
 	<li>Multiple raccoon dogs crammed into single cages and one with a wound to its leg.</li>
 	<li>Raccoon dogs stunned by two electrified metal rods hooked up to a truck engine's battery.</li>
</ul>
Animals were then skinned by workers, by ripping off their pelt around the head while its legs remained hooked to the back of a truck.

<iframe src="https://www.youtube.com/embed/frPNUCbRco0" width="560px" height="315px" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Co-founder Jose Valle, one of the two undercover activists who infiltrated the farms, explains the consequences could have been dire if he was caught. '<em>There was a stench coming from the cages. The animals had no access to water and when they were skinned, the corpses were left lying in the open air.</em>' He said.

<em>'Several of them showed clear signs of psychological stress and suffered stereotypical behavior such as pacing, biting bars and self-mutilation. Most of the animals were killed at the farm and skinned. Many times the bodies were then fed to other animals.'</em>

<em>'The majority are killed when they are a few months old but others are kept for breeding. Some of these animals spend four or five years in a tiny cage going round in circles going crazy.'</em>

<em>'Workers told us they were selling all over the world, including Europe, and he has several customers in Europe and the U.S.'</em>

Mr. Valle added: '<em>They know there's a growing outrage against the cruelty of the fur trade, so as westerners it's difficult for us to access these places. They know animal activists are interested so they're reluctant to say much to outsiders.</em>

<em>'One activist ended up in a car chase when he tried to film a fur market. He was taken to the police station and they confiscated the tapes of what he filmed there.'</em>

Animal Equality is calling for more countries to ban fur farming, which has boomed in China. China is now the world's biggest importer of fur despite already producing a quarter of the world's pelts, according to the International Fur Trade Federation.

A U.S. report in 2010 said 30 million mink, 25 million foxes, and 15 million raccoon dogs were set to be slaughtered for fashion in China that year - up to 15 million on the year before. Most of the country's fur farms - most of which have been built since 1995 - were small family firms, making it harder to carry out checks on the number of animals or their welfare. Europe remains by far the biggest fur farming region, producing 60 percent of the world's supply.

Fur farming was banned in Britain in 2000 amid a public outcry, and the country's last remaining mink farmers were paid compensation by the government. A year later Britain banned imports of dog and cat fur, though<em> raccoon dogs</em> do not come under the legislation because the animals are so distantly related to domestic pets.

Many U.S. and UK websites list coats and collars made from Chinese '<em>Asiatic raccoon</em>', and the use of the name means buyers do not always realize they are buying the fur of canines. These furs are shipped to countries across the globe. They line coats at stores throughout the U.S.
