Animal Equality and a coalition of animal protection organizations have launched a digital video advertisement in New York City’s famed Times Square to draw attention to the cruel farming practices on farms that raise chickens for McDonald’s meat.

<a href="https://www.facebook.com/AnimalEquality/videos/10155755902794077/" target="_blank" rel="noopener">The 10-second ad</a>, showing at 1500 Broadway Ave and 43rd St. from now until October 31, 2018, will air at least 60 times a day and will intend to reach the estimated 1 million people who pass through Times Square every day.

The ad aims to promote <a href="https://mcchickencruelty.com/" target="_blank" rel="noopener">Animal Equality’s campaign and a petition</a> calling on McDonald’s to adopt meaningful animal protection standards that would reduce the suffering of hundreds of millions of chickens killed for its menu items.

“Nearly 90 companies - including many of McDonald’s competitors such as Burger King, Subway, and Jack in the Box - have already committed to making meaningful changes that will improve the lives of chickens,” said Animal Equality’s Corporate Campaigns Coordinator, Ollie Davidson. “These smart and social birds deserve better. When will McDonald’s stop dragging its feet and commit to eliminating some of the cruelest standard practices used by its suppliers? The time for change is now.”

The coalition behind the ad is comprised of Animal Equality, The Humane League, Mercy For Animals, Compassion In World Farming, Compassion Over Killing and World Animal Protection.
