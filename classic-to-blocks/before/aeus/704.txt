Animal Equality has carried out several investigations into farms that&nbsp;sell their products&nbsp;as organic or supposedly having high welfare standards. In all&nbsp;cases we found animals suffering&nbsp;psychologically and physically, major injuries and alarming deficiencies in hygiene. Here are some of our most important findings.

<strong>Organic egg farms in Germany:&nbsp;</strong>
<img class="wp-image-4223" style="width: 580px;" src="/app/uploads/2015/05/17373559066_dce2328f46_z.jpg" alt="">

Animal Equality investigators&nbsp;<a href="http://www.animalequality.net/node/701">visited two organic farms in Northern Germany on March 2015. </a>Both of the farms investigated held up to 30,000 egg-laying hens.

One of the farms supplies eggs to “Deutsche Frühstücksei GmbH”, Germany’s largest egg producer’s and also one of Europe’s biggest egg producers. “Deutsche Frühstücksei GmbH” supplies all of Germany’s leading retail chains and supermarkets.

<strong>Findings:&nbsp;</strong>

- Over ten thousand animals living in a barn amongst dirt, dust and excrement.
- Dead animals laying between others who are still alive
- The majority of the hens have only a few feathers left due to stress-induced feather-plucking
- Lack of hygiene leading to daily inflammation and other injuries.

<strong>The truth behind Quality Assured meat in the UK:</strong>

<strong><img class="wp-image-4301" style="width: 580px;" src="/app/uploads/2015/05/6588436255_154b13f92d_z.jpg" alt=""></strong>

In 2012, Animal Equality carried out a <a href="https://www.youtube.com/watch?v=pQK4261GXyg">ground-breaking two-month undercover investigation into a ‘Quality Assured’ </a>(Red Tractor) farm in Norfolk - Harling Farm, belonging to A. J. Edwards &amp; Son.

<strong>Findings:&nbsp;</strong>

- Frantically&nbsp;squealing&nbsp;piglets who had limbs that were being crushed by their mothers.
- The routine clipping of teeth&nbsp;on piglets between 24 and 48 hours after their birth. Tooth clipping causes pain
and leaves piglets open to potential infection
- Sustained beatings&nbsp;that resulted in pigs with facial and other bodily abrasions. A
- Piglets&nbsp;who were left dead or dying within pens, and inside feed.
- Dead bodies&nbsp;that were dumped in an illegal shallow and uncovered pit to save the farm money.
<strong>What we found in pig farms with transparency&nbsp;labels in&nbsp;Germany:</strong>

<img class="wp-image-4219" style="width: 580px;" src="/app/uploads/2015/05/14851056325_946d379627_z.jpg" alt="">

The well-known German supermarket chain Edeka advertises its own branded meat program "Gutfleisch" with words like transparency, local origin as well as high quality and hygiene levels which supposedly ensure the health of animals. "Gutfleisch" has won several awards and exceeds standards stipulated by industry according to their own statements. <a href="http://www.animalequality.net/news/633/edeka-gutfleisch-%E2%80%93-german-pig-industry-exposed">In an&nbsp;investigation carried out in 2014,&nbsp;our team found major deficiencies in hygiene, alarming conditions and animal suffering.</a>

<strong>Findings:</strong>

• Dying, considerably suffering animals without veterinary treatment
• Animals with extreme breathing difficulties, coughing heavily (too many animals crammed in confined spaces with inadequate ventilation often lead to deadly diseases)
• Untreated and extreme umbilical hernias with bowels protruding through wounds
• Weeks animals that can no longer stand or sit upright and keep on falling at the attempt to get up
• Injuries due to the conditions in which animals are kept, including abscesses, claw injuries, bloody wounds, untreated hematomas etc.
• Cannibalism: bitten tails and ears with bleeding wounds
• Extreme hygiene defects, no clean drinking water, often no clean spaces for the animals to lie down

<strong>Organic pig farms in Spain:</strong>

Watch this video that shows what we found in organic pig farms in Spain.
<div class="media_embed"><iframe src="https://player.vimeo.com/video/11891585?color=00c4ff&amp;title=0&amp;byline=0&amp;portrait=0" allowfullscreen="allowfullscreen" width="560px" height="448px" frameborder="0"></iframe></div>
