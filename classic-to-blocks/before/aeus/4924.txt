The consumption of cows in the United States has fallen from 28.1 billion pounds per year in 2007 to 25.6 billion pounds in 2011, according to the U.S. Cattle and Beef Industry.&nbsp; The latest USDA report predicts that in 2013 Americans will eat a half-billion fewer chickens and 400,000 fewer cows compared to 2006, as well as 12 million fewer pigs compared to 2007, and 22 million fewer turkeys than in 2008.

While the USDA would like us to believe the sharp decline in meat production is mainly due to increased feed costs and rising meat prices, Kimberly Budziak of VegNewsDaily writes that&nbsp; a statistical analysis of meat demand bycountinganimals.com strongly suggests the trend is also due to the fact that U.S. residents are making the choice that they just don’t want to eat as much meat!

The Counting Animals’ study suggests that 70 percent of the waning cow-consumption and 93 percent of the chicken-consumption decline is not due to external factors, such as cost increase, but is the result of the increased awareness and reduced desire to eat animals by a generation knowledgeable about the health risks and appalled at the cruelty of factory farming and mass animal-slaughter operations.

<strong>DECLINE IN FOOD-ANIMAL VETERINARIANS</strong>

A predictable but overlooked consequence of Americans turning away from eating other sentient beings is the diminished number of veterinarians entering the food-animal field in the United States.

In the early 1900’s almost all veterinarians were treating farm animals which were destined to be slaughtered for food.&nbsp; Although there is no admission of the influence of animal-rights efforts to dissuade humans from eating meat and consider a vegan diet, according to the American Veterinary Medical Association (AVMA), “<em>a critical shortage of food-animal veterinarians is threatening the safety of the U.S. food supply</em>.”

American Veterinary Medical Association statistics for 2009 show that, of the approximately 61,000 veterinarians in private clinical practice, only 1,103 exclusively treated food animals, while 41,117 exclusively treated “companion animals,” according to foodsafetynews.com.&nbsp; Many private clinics treat both food animals and companion animals. Some also treat horses.

AVMA Veterinarian/CEO Ron DeHaven said that leaves only about 8,500 veterinarians caring for more than 9.4 billion head of livestock, and this shortage of food-animal veterinarians poses a risk to the safety of the nation’s food supply. He also explained concerns that there aren’t enough federal veterinarians to assure a safe food supply and effectively deal with zoonotic diseases–diseases that can be transmitted from animals to humans.

DeHaven, former administrator for USDA’s Animal and Plant Health Inspection Service, described the situation as “<em>dangerous</em>.” He told federal legislators that among the major reasons for this shortage were veterinary student debt loads and the population shift away from rural areas to suburban and urban areas. He omits the changing trend toward vegetarian and vegan diets.

Many suburban and urban students going into veterinary school today have had little if any, exposure to food animals nor do they feel inclined to pursue a career in which the orientation is production medicine and interventions aimed at improving the financial health of farming operations.

For most, the realities of factory farming and animal slaughter are not what motivated them to pursue a career in veterinary medicine. The idea of treating an animal that’s being raised to be butchered is what Veterinarian Eric Barchas, who specializes in small animals in San Francisco, describes on his blog as “unsavory.
