<strong>Cruelty&nbsp;is standard practice on egg farms that use cages</strong>. Animal Equality has highlighted the suffering of these delicate and sensitive hens across three different countries in just one week.&nbsp;Our latest investigation exposes the cruelty of the Italian egg industry and demonstrates that <strong>cruelty to hens occurs all around the world.</strong>

<img class="wp-image-4122" src="/app/uploads/2017/02/italy-hen-1.jpg" alt="">

Inside the filthy sheds&nbsp;investigators witnessed a disturbing scene:
<ul>
 	<li>Hens crammed into spaces so small they <strong>can’t even </strong><strong>fully</strong><strong> spread their wings</strong></li>
 	<li>Hens suffering from serious diseases and the loss of feathers</li>
 	<li><strong>Dead rats decomposing nearby the hens </strong></li>
 	<li>Hens living among dead and decomposing companions</li>
 	<li>Eggs that completely <strong>infested with larvae and insects</strong></li>
</ul>
<img class="wp-image-4108" src="/app/uploads/2017/02/italy-hen-2.jpg" alt="">

In Italy alone, <strong>64 million hens live their entire lives in tiny metal cages</strong> where they cannot express even the&nbsp;simplest of natural behaviors. Deprived of sunlight and forced to stand on wire mesh flooring, the hens suffer terribly in these inhumane conditions. Once their egg production decreases, they are deemed useless&nbsp;and sent to slaughter.

As part of our in-depth investigation, <strong>Animal Equality has taken steps to hold the egg industry accountable</strong>. We've notified the local police and also filed a complaint before the Ministry of the Environment and the Ministry of Health, as this is also <strong>a public health issue</strong>. In addition, the case has been reported on by both <em>Corriere</em>&nbsp;and <em>TG1</em>, <strong>two of Italy's largest TV networks</strong>, educating millions of people on the cruel practices in the egg industry.

Thankfully, more and more people are realizing that treating hens this way is cruel and that the best way to help them is to <a href="https://loveveg.com/compassionate-options/eggs/">replace eggs with plant-based alternatives</a>.

With your help we can make a difference for animals.
