On one covert video, farm workers illegally burn the ankles of Tennessee walking horses with chemicals. Another captures workers in Wyoming punching and kicking pigs and flinging piglets into the air. And at one of the country’s largest egg suppliers, a video shows hens caged alongside rotting bird corpses, while workers burn and snap off the beaks of young chicks.

Each video — all shot in the last two years by undercover animal rights activists — drew a swift response: Federal prosecutors in Tennessee charged the horse trainer and other workers, who have pleaded guilty, with violating the Horse Protection Act. Local authorities in Wyoming charged nine farm employees with cruelty to animals. And the egg supplier, which operates in Iowa and other states, lost one of its biggest customers, McDonald’s, which said the video played a part in its decision.

But a dozen or so state legislatures have had a different reaction: They proposed or enacted bills that would make it illegal to covertly videotape livestock farms, or apply for a job at one without disclosing ties to animal rights groups. They have also drafted measures to require such videos to be given to the authorities almost immediately, which activists say would thwart any meaningful undercover investigation of large factory farms. Critics call them <em>“Ag-Gag”</em> bills.

Some of the legislation appears inspired by the American Legislative Exchange Council, a business advocacy group with hundreds of state representatives from farm states as members. The group creates model bills, drafted by lobbyists and lawmakers, that in the past have included such things as <em>“stand your ground”</em> gun laws and tighter voter identification rules.

One of the group’s model bills, <em>“The Animal and Ecological Terrorism Act,”</em> prohibits filming or taking pictures on livestock farms to <em>“defame the facility or its owner.”</em> Violators would be placed on a <em>“terrorist registry.”</em>

Officials from the group did not respond to a request for comment.

Animal rights activists say they have not seen legislation that would require them to register as terrorists, but they say other measures — including laws passed last year in Iowa, Utah, and Missouri — make it nearly impossible to produce similar undercover exposés. Some groups say that they have curtailed activism in those states.

<em>“It definitely has had a chilling effect on our ability to conduct undercover investigations,”</em> said Vandhana Bala, general counsel for Mercy for Animals, which has shot many videos, including the egg-farm investigation in 2011. (McDonald’s said that video showed “disturbing and completely unacceptable” behavior, but that none of the online clips were from the Iowa farm that supplied its eggs. Ms. Bala, though, said that some video showing bird carcasses in cages did come from that facility.)

The American Farm Bureau Federation, which lobbies for the agricultural and meat industries, criticized the mistreatment seen on some videos. But the group cautions that some methods represent best practices endorsed by animal-care experts.

The videos may seem troubling to someone unfamiliar with farming, said Kelli Ludlum, the group’s director of Congressional relations, but they can be like seeing open-heart surgery for the first time.

“<em>They could be performing a perfect procedure, but you would consider it abhorrent that they were cutting a person open,</em>” she said.

In coming weeks, Indiana and Tennessee are expected to vote on similar measures, while states from California to Pennsylvania continue to debate them.

Opponents have scored some recent victories, as a handful of bills have died, including those in New Mexico and New Hampshire. In Wyoming, the legislation stalled after loud opposition from animal rights advocates, including Bob Barker, former host of <em>“The Price is Right.”</em>

In Indiana, an expansive bill became one of the most controversial of the state legislative session, drawing heated opposition from labor groups and the state press association, which said the measure violated the First Amendment.

After numerous constitutional objections, the bill was redrafted and will be unveiled Monday, said Greg Steuerwald, a Republican state representative and chairman of the Judiciary Committee.

The new bill would require job applicants to disclose material information or face criminal penalties, a provision that opponents say would prevent undercover operatives from obtaining employment. And employees who do something beyond the scope of their jobs could be charged with criminal trespass.

An employee who took a video on a livestock farm with his phone and gave it to someone else would<em> “probably” </em>run afoul of the proposed law, Mr. Steuerwald said. The bill will apply not just to farms, but to all employers, he added.

Nancy J. Guyott, the president of the Indiana chapter of the A.F.L.-C.I.O., said she feared that the legislation would punish whistle-blowers.

Nationally, animal rights advocates fear that they will lose a valuable tool that fills the void of what they say is weak or nonexistent regulation.

Livestock companies say that their businesses have suffered financially from unfair videos that are less about protecting animals than persuading consumers to stop eating meat.

Don Lehe, a Republican state representative from a rural district in Indiana, said online videos can cast farmers in a false light and give them little opportunity to correct the record.

As for whistle-blowers, advocates for the meat industry say that they are protected from prosecution by provisions in some bills that give them 24 to 48 hours to turn over videos to legal authorities.

<em>“If an abuse has occurred and they have evidence of it, why are they holding on to it?”</em> said Dale Moore, executive director of public policy for the American Farm Bureau Federation.

But animal rights groups say investigations take months to complete.

Undercover workers cannot document a pattern of abuse, gather enough evidence to force a government investigation and determine whether managers condone the abuse within one to two days, said Matt Dominguez, who works on farm animal protection at the Humane Society of the United States.

<em>“Instead of working to prevent future abuses, the factory farms want to silence them,”</em> he said. <em>“What they really want is for the whistle to be blown on the whistle-blower.”</em>

The Humane Society was responsible for a number of undercover investigations, including the videos of the Wyoming pig farm and the Tennessee walking horses.

Video shot in 2011 showed workers dripping caustic chemicals onto the horses’ ankles and clasping metal chains onto the injured tissue. This illegal and excruciating technique, known as <em>“soring,”</em> forces the horse to thrust its front legs forward after every painful step to exaggerate the distinctive high-stepping gait favored by breeders. The video also showed a worker hitting a horse in the head with a large piece of wood.

The Humane Society first voluntarily turned over the video to law enforcement. By the time the video was publicly disclosed, federal prosecutors had filed charges. A week later, they announced guilty pleas from the horse trainer and other workers.

Prosecutors later credited the Humane Society with prompting the federal investigation and establishing <em>“evidence instrumental to the case.”</em>

That aid to prosecutors shows the importance of lengthy undercover investigations that would be prevented by laws requiring video to be turned over within one or two days, Mr. Dominguez said.

<em>“At the first sign of animal cruelty, we’d have to pull our investigator out, and we wouldn’t be able to build a case that leads to charges.” </em>

<em>“That property owner is essentially guilty before they had the chance to address the issue,” </em>Mr. Lehe said.
