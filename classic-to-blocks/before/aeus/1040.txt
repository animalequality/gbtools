<em>Okja</em>, a big-budget movie with a <strong>pro-vegan message</strong>, received a four-minute standing ovation after its recent debut at the Cannes Film Festival.

The film combines the excitement of a family-friendly sci-fi adventure with a nightmarish look at the <strong>fast food industry.</strong>

While the shocking imagery involves the death of imaginary animals, it has clear parallels with the grotesque images of frightened cows, pigs, chickens, and others who meet grisly ends in <strong>real slaughterhouses</strong> around the world.

<iframe src="https://www.youtube.com/embed/AjCebKn4iic" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Jake Gyllenhaal, Lily Collins, Tilda Swinton, and Paul Dano bring plenty of star power.

Paul Dano and Lilly Collins play members of an <strong>animal rights activist group</strong> - the movie’s message is not at all subtle. The metaphors in <em>Okja</em> are clear yet fascinating.

<em>Okja</em> proves that movies with strong messages can be thought-provoking and <strong>inspire great change</strong>, yet they can also be entertaining.

<em>Okja</em> is available on <a href="https://www.netflix.com/title/80091936">Netflix</a> and is screening at select theatres.
