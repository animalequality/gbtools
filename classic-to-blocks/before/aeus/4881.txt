Spokesman Michal Stastny said all attempts to revive the animal failed. There were no cameras in the room and it was not clear exactly what happened. But it is believed Tatu unbraided one of the dozens of ropes the gorillas use in their pavilion for climbing and put a strand around his neck before tragically hanging himself.

Mammals curator Pavel Brandl said: '<em>It was an accident</em>.' The ropes are checked daily, he added.

He revealed that another gorilla, Kamba, was by Tatu's side and was apparently&nbsp; trying to help the animal -&nbsp; but '<em>it's hard to say what exactly she was doing</em>.' Director Miroslav Bobek said the death was the most tragic event at the zoo since flooding in 2002 killed more than 100 animals.

The zoo still has six gorillas, and they are among the most popular animals there. Tens of thousands of people watched Tatu's birth online on May 30, 2007. Prague Zoo was opened in the Czech Republic in 1931 with the goal to 'advance the study of zoology, protect wildlife, and educate the public' in the district of Troja in the north of the city. It stands on 111 acres and houses about 4,400 animals that represent 670 species from all around the world. In 2007, Forbes Traveler Magazine listed Prague Zoo among the world's best zoos.
