Thanks to the years of work of dozens of organizations and activists worldwide bullfighting is seeing its last days. Here are some examples of the actions Animal Equality has undertaken at the heart of this cruel spectacle.

<strong>1. Lowerings</strong>

Animal Equality's brave activists have lowered themselves from major bullrings and bullfighting symbols to send a clear message to the authorities. Bullfighting abolition!

&nbsp;

<img class="wp-image-4682" src="/app/uploads/2015/10/lowering.jpg" alt="">

<img class="wp-image-4683" src="/app/uploads/2015/10/lowering2.jpg" alt="Lowering from the world's most famous bullring, Las Ventas. ">

<strong>2. Jumping into the bullring</strong>

Over the years,&nbsp;Animal Equality has jumped into the rings of major bullrings in Spain demanding the end of the cruelty. In many cases our activists suffered from the violence of those who want to continue torturing animals for fun. These protests had a huge media attention reaching millions of people internationally.

<img class="wp-image-4758" src="/app/uploads/2015/10/salto.jpg" alt="">

&nbsp;

<img class="wp-image-4844" src="/app/uploads/2015/10/zzpress.jpg" alt="">

<strong>3. Investigations</strong>

Animal Equality's extensive investigations into bullfighting have revealed extreme cruelty. Our investigators have infiltrated these popular events for several years, obtaining footage that exposes the cruelty and brutality of these festivities. In some events animals are beaten, stoned, kicked and chased by vehicles until collapsing exhausted whilst small calves, less than two years old, suffer an agonizing and painful death.

<img class="wp-image-4843" src="/app/uploads/2015/10/zinvestigation1.jpg" alt="">

<img class="wp-image-4845" src="/app/uploads/2015/10/zzzzz.jpg" alt="">

Today's ruling by&nbsp;the European Parliament&nbsp;is a big step forward for animals.&nbsp;<a href="https://animalequality.org/donate/">Please, support our work against cruelty today by becoming an Animal Equality monthly supporter.&nbsp;</a>
