While campaigning for his wife in Las Vegas, Bill Clinton credited veganism for keeping him healthy after leaving office. The former president decided to change his eating habits in 2010 after undergoing emergency surgery for having chest pains.

According to<em> Politico</em>, he stated, "It changed my life. I might not be around if I hadn't become a vegan. It's great."

Consuming animal products may cause many health issues including certain cancers, obesity, and heart disease. <em>The Huffington Post</em> reported that high amounts of saturated fat and cholesterol in red meat increases people's risk for heart disease.

The 69-year old continued, "The vegan diet is what I like the best. I have more energy. I never clog. For me, the no dairy thing, because I had an allergy, has really helped a lot. And I feel good."

While he admitted he isn't <em>always</em> strictly vegan, he is a big fan of meat alternatives which he uses in lasagna and chili enchiladas.

With the wonderful alternatives, it's easier than ever to adopt a plant-based diet and – like Mr. Clinton – it can change your life.

&nbsp;
<p style="font-size: 0.7rem; color: #666;">Photo:&nbsp;Gage Skidmore</p>
