As part of our efforts to help end the horrific abuses we documented during <a href="https://www.youtube.com/watch?v=Q2qJIDOw-cU">our investigation</a>, Senator Diva Gastélum, supported by Senators Cristina Díaz, Hilda Flores, Lilia Merodio, Anabel Acosta, Itzel Ríos, and Maria Elena Barrera, <a href="https://www.youtube.com/watch?v=hxv7JTEgmjs&amp;feature=youtu.be">presented an initiative</a> proposed by Animal Equality that would modify the Federal Animal Health Law and the Federal Penal Code. <strong>The modifications would make abuse and cruelty against farmed animals a crime</strong>.

Our proposal is supported by eleven animal protection groups and an organization made up of 92 protection groups throughout the country. It pushes for violations, just like the ones we discovered in our Rastros de Mexico investigation, to be considered criminal acts.

As evidenced by <a href="https://www.flickr.com/photos/animalequalityuk/albums/72157675738505780">our investigation</a> carried out on 31 municipal facilities across Mexico, non-compliance with existing regulations results in animals being <strong>skinned alive, beaten, electrocuted, burned, and killed while fully conscious</strong> without previously being stunned.

<img class="wp-image-4136" src="/app/uploads/2017/03/33664927736_3770367952_k_0.jpg" alt="">

Senator Diva Gastélum says that "We are working to establish sanctions within the Federal Animal Health Law and the Federal Penal Code so that anyone who mistreats or abuses animals, such as those instances documented by Animal Equality, will be held accountable for their actions under the law.”

She then added that the measure "seeks to ensure compliance with official Mexican regulations and protect animals from abuse and cruelty. This change is a fundamental step for Mexico’s progress in terms of animal protection."

https://www.youtube.com/watch?v=Q2qJIDOw-cU

According to Dulce Ramirez, Animal Equality’s Executive Director in Mexico, “the existence of official standards drafted and authorized by SAGARPA (Mexico’s agricultural department) are of no use if they can't be enforced. We can see that the sanctions currently in place are not enough, that is why we must tighten the law and criminalize violations.

She added: “We trust that through Senator Gasélum and the thousands of concerned people who have signed our petition,<strong> the Senate will find the political will to fix this terrible situation</strong>. It is a way of making sure that the official regulations not only satisfy international conventions but also improve Mexico’s image with regard to animal welfare.”

<strong>It is not too late to act. </strong>You too can help make sure that animal abuse and cruelty is a crime and that farmed animals will be protected.

<strong>Please sign <a href="https://animalequality.org/action/mexican-slaughterhouses">our </a><a href="https://animalequality.org/action/mexican-slaughterhouses">petition&nbsp;</a>asking the senate to approve the measure as soon as possible. </strong>And remember that the best way to protect farmed animals is to leave them off our plates.
