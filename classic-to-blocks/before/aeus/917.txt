The&nbsp;<strong><a href="http://www.openphilanthropy.org/blog/initial-grants-support-corporate-cage-free-reforms">Open Philanthropy Project</a> has awarded Animal Equality a grant of $500,000</strong>&nbsp;spread over the next two years. This exciting&nbsp;initiative enables us to work even harder at pushing corporations to transition away from using eggs sourced from caged hens.

<strong>Cages cause severe suffering to hens</strong>. They are denied everything that is natural to them and are forced to live in cramped wire cages for nearly their entire lives. The cages are so small that the hens cannot even spread their wings.

<img class="wp-image-4699" src="/app/uploads/2016/10/mex-chicken-11.jpg" alt="">

The horrific reality these hens face is the sole reason why the&nbsp;<strong>elimination of cages</strong> is an absolute priority for Animal Equality.

In the US, since 2014, advocacy organizations have secured <strong>cage-free pledges from over 200 major companies</strong>, including McDonald’s, Costco, Subway, and Kroger. From these pledges alone, it is estimated that more than 100&nbsp;million hens annually will be spared from battery cage confinement.

<img class="wp-image-4408" src="/app/uploads/2016/10/Mex-Hen-Cage-21.gif" alt="">

This is why this is such&nbsp;<strong>huge news</strong>! Animal Equality has recently launched its Corporate Outreach Department, headed by International Director&nbsp;Jaya&nbsp;Bhumitra, and together with this generous grant from the Open Philanthropy Project enables us to double down on our efforts to persuade companies to phase out the use of cruel cages.

Our international presence enables us to effect change for animals around the world and with this grant, we will be focused on&nbsp;<strong>companies in Mexico, Brazil, India, Spain, and Italy</strong>. These countries represent nearly a quarter of the world’s population and are some of the largest egg consumers in the world. Brazil, Mexico, and India, in particular, are developing economies and millions of new consumers are developing shopping habits. We are aiming to inform these new consumers so that they can make compassionate choices at the supermarket and restaurants.

As an organization, we pride ourselves on our&nbsp;<strong>efficacy and professionalism</strong>. For two years in a row, we have been&nbsp;<strong>recognized by&nbsp;<a href="http://www.animalcharityevaluators.org/recommendations/list-organizations/">Animal Charity Evaluators</a>&nbsp;as a Top Charity</strong>. We know how to make the most of our donors’ dollars. In fact, this is one of the many reasons that Animal Equality was selected for this grant from the Open Philanthropy Project. Its mission is to identify outstanding giving opportunities and to give as effectively as it can.

Eliminating cages in egg production is the next step in our strategy to promote animal protection. While reducing egg consumption is a large component of that strategy, encouraging these changes at major companies reduces the suffering of millions of hens now. This defined top-down strategy is the most effective way to help these vulnerable animals and we are incredibly grateful and excited to continue the progress we’ve already made.
