Animals are great, right? These wonderful beings who are cruelly treated on farms and in slaughterhouses establish bonds with us as quickly as our beloved cats and dogs.

Pigs, for example, are as intelligent as dolphins,&nbsp;and they can reach an&nbsp;IQ of that of a 3-year-old child! Cows just may be&nbsp;the most protective and devoted mothers that exist on this planet. And the fragile hens begin to communicate with their chicks even before they have hatched.

When the eggs hatch, chicks begin to emit&nbsp;their characteristic chirping sounds to make sure their mother is beside them.

Chickens, animals we rarely see outside a supermarket refrigerator, can recognize and remember up to 100 members of their group. They also have up to 30 vocalizations to communicate.

Anyone who has had the chance to live with chickens and hens know that personalities among them are as varied as our precious dogs and cats. Each hen and chicken show unique personality traits. And just as our dogs and cats, they establish bonds with us quickly.

Chicks are among the most fragile creatures of nature. When they don’t have their mother, they seek out&nbsp;warmth and protection, just as any other baby does.

Cows are peaceful and sociable beings. They have their best friends of&nbsp;the group, and they establish hierarchies and follow the most experienced cow, as elephants do. &nbsp;There have been cases where cows have escaped from the slaughterhouse and have returned to wherever they were separated from their calves. These majestic animals radiate love and peace, and, they're as curious as cats!

<iframe src="https://www.youtube.com/embed/kAI-PFWZ95I" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe>

<em>Cows are very curious and perceptive beings. Aren’t they great?</em>

Pigs have become real stars in social networks. Not surprisingly, these animals are so adorable that even the surliest cats cannot resist their charms.

<iframe src="https://www.youtube.com/embed/Dd2CWnM1zSs" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe>

<em>Pigs</em><em> and cats…purrfect combination!</em>

So this is what we know: farm animals are adorable and can prove it if we give them the opportunity. We have to keep them far away from the cruelties of the&nbsp;meat industry! Want to know how? <a href="https://loveveg.com">Try compassionate and delicious food choices!</a>
