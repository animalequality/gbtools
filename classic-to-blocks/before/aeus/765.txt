The Finnish animal rights organization <a href="http://www.oikeuttaelaimille.net">Oikeutta eläimille (Justice for Animals)</a> launched an investigation with video footage obtained by using hidden cameras inside four slaughterhouses. For the first time in Finland, images of the common practices in slaughterhouses are now being exposed.

The <a href="https://www.elaintehtaat.fi/">video footage</a> obtained is shocking. Animals are routinely being beaten, and electrified bars are even used on their faces. They are subjected to electric shocks when animals are unable to&nbsp;move due to lack of space, and&nbsp;even if they are able to move as well.

<em>“The fundamental problem in the act of killing animals is that they don’t want to die. The butchers have to use electrified bars and hit them because animals are not going to walk voluntarily to their deaths,” </em>said Kristo Muurimaa of Oikeutta&nbsp;eläimille.<em> “This type of violence is a direct consequence of how our society sees animals as mere products. As long as we continue eating millions of animals a year, the violence will continue,”</em> he added.

One of the investigated slaughterhouses belongs to the company HKScan, one of the largest Nordic meat companies. This company exports meat to 50 countries, including the United States.
