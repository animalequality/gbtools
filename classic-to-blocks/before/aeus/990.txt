A scandal involving Brazil’s meat supply has spread across the country and recently the globe.

Investigators are looking into claims that <strong>meatpackers bribed inspectors</strong> to overlook unsanitary conditions. Some of the most egregious violations included pigs’ heads crushed and mixed into sausages, <strong>cardboard mixed with chicken meat</strong>, and c<strong>hemicals injected into meat</strong> to hide the smell of rot.

Brazilian police said last Friday that some of the meat ended up in <strong>lunches at public schools</strong>, and evidence showed some meat was headed for Italy and Spain. The massive raid operation included 1,100 agents, over 300 court orders, and nearly 200 search warrants.

The news has had global consequences and the nation’s economy has taken a nosedive. <strong>Brazil’s daily meat exports have fallen from an average of $63 million to just $74,000</strong>, <a href="https://www.meatingplace.com/Industry/News/Details/72312?allowguest=true">MeatingPlace.com</a> reports. Countries including China, Hong Kong, Egypt, and Chile were quick to ban beef imports from Brazil.

Yet few are speaking of the cruel practices that occur on these facilities every day of the year.

Animals, including chickens, pigs, and cows, are raised in unnatural conditions and forced to live miserable lives on factory farms and then face a horrific end. They’re slaughtered in the cheapest way possible which means that many are still fully conscious as they have their throats slit.

Government corruption and unsanitary conditions on factory farms are indeed scandalous&nbsp;but remember you can <strong>stand up against this corruption and an industry that tortures animals simply by trying a plant-based diet.</strong>

<strong>Take the <a href="https://ianimal360.com/">pledge</a> today to try vegan!</strong>
