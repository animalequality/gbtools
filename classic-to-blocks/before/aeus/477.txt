Donkeys carry rice and jaggery from Pamba and Kedar and Bad­rinath, the animals are used to transport people to the hilltop and they undertake treacherous journeys.

A S Sandhya, secretary of Society for Prevention of Cruelty to Animals (SPCA) in Pathanamthitta district has been fighting for the cause for a long period of time. Following her petition before the Kerala High Court five years ago, the court directed that only healthy animals should be used for transportation. “But it was not implemented by the authorities concerned,” she said. Heavy loads have continued to be tied to the back of those animals using thick ropes, said Sandhya. <em>"To force them to move they are beaten.</em> <em>Hot iron boxes are pressed on their thighs by 'owners', causing deep wounds. When beaten on the wounds, they move fast”.</em>

Other cruel methods are also employed to make donkeys do impossible tasks, Sandhya said. <em>"One can find donkeys with ears cut and wounds on their bodies. But the sad plight of these animals seldom attracts public attention".</em>

Shashi Tharoor, MP took up the matter with Minister V.S. Sivakumar in the wake of a report in a national newspaper on the inhumane treatment of donkeys in Sabarimala.

As a result, the Devaswom Board has been directed not to use donkeys to carry materials from now on.

The MP said<em> “The whole system of carriage of materials by animals can be dispensed with and alternative methods such as the use of ropeway or elevators may be put in place. This will not only bring an end to the torture of animals but also improve the hygienic conditions at the sacred pilgrim site”.</em>
