Animal Equality is raising awareness around the fate of farmed animals. The organization has <strong>launched an exhibition</strong> at the European Parliament titled "Factory Farming: Greatest Crime of Our Time?"

The exhibition, the <strong>first of its kind installed at the European Parliament</strong>, highlights images and research from our numerous investigations across farms in Spain, the UK, and Germany.  It also offers viewers the possibility of experiencing life as a factory farmed animal with iAnimal - Animal Equality’s virtual reality project.

<img class="alignnone wp-image-7678 size-full" src="https://animalequality.org/app/uploads/2018/06/news_500x420_european_parliament_exhibition_2017.jpg" alt="" width="500" height="420" />

Several Members of the European Parliament (MEPs) visited the exhibition and watched <strong>iAnimal</strong>for themselves, including Dimitrios Papadimoulis, Vice President of the EU Parliament.

MEP Stefan Eck of Germany, who last year lobbied parliament to introduce regulations for rabbit farming in the EU, including banning cages, said this of iAnimal: “Animal Equality’s virtual reality experience <strong>gives an extraordinary look</strong> into the lives of so called farmed animals...It shows that the <strong>European animal welfare laws aren’t worth the paper they’re written on</strong>.” He then added: “iAnimal awakens and shocks...all you can feel is a deep shame about what we are doing to animals in our society.”

<img class="alignleft wp-image-7677 size-full" src="https://animalequality.org/app/uploads/2018/06/news_500x420_european_parliament_talk_toni_2017.jpg" alt="" width="500" height="420" />

Toni Shephard, our Executive Director in the UK, also gave an impassioned speech before lawmakers and other important dignitaries. She said: “If we dare to act with bravery and urgency, I know that <strong>we will live to see the end of factory farming</strong>. And we will be able to look our grandchildren in the eye and say, "Yes, we did wrong, but we were unwavering in our efforts to put it right."

Members of government and their staff have the ability to affect change for hundreds of millions of animals. Exhibitions such as this one are vital in raising awareness and ending animal suffering.
