<div class="center"><img class="wp-image-1619" src="/app/uploads/2016/07/incubatore-polli_fb.jpg" /></div>
<p>Rosie &egrave; nata in un <strong>incubatoio industriale</strong>, all&#39;interno del settore delle uova. Non ha mai conosciuto sua madre e dal primo minuto di&nbsp;vita le &egrave; stato&nbsp;negato&nbsp;qualsiasi stimolo naturale. Se fosse stata un maschio sarebbe diventata cibo per bestiame, attraverso un <a href="https://www.youtube.com/watch?v=w75rbsjc2N8"><strong>metodo molto cruento di cui vi abbiamo gi&agrave; parlato</strong></a>.</p>

<p>Poche ore dopo la nascita &egrave; stato inviata ad un <strong>allevamento per l&#39;ingrasso e la crescita</strong> e qui ha trascorso i successivi <strong>cinque mesi</strong> necessari per l&#39;aumento del suo peso.&nbsp;<br />
<br />
Le condizioni dell&#39;allevamento erano terribili: Rosie non poteva soddisfare i suoi istinti, le era concesso solo di mangiare e ingrassare.&nbsp;</p>

<p><img alt="" class="wp-image-1620" src="/app/uploads/2016/07/rosie.jpg" style="width: 850px; " /></p>

<p>Ma il peggio doveva ancora venire.</p>

<p>Dopo aver raggiunto il giusto peso per l&#39;industria delle uova, Rosie &egrave; stata inviata nell&#39;azienda avicola dove avrebbe dovuto passare<strong> il resto dei suoi giorni</strong>, vivendo con pi&ugrave; di cinque altri volatili in una piccola gabbia sporca con pavimento in griglia metallica.</p>

<p><img alt="" class="wp-image-1621" src="/app/uploads/2016/07/galline_2.jpg" style="width: 850px; " /></p>

<p>Qui Rosie non poteva fare nulla di quello che &egrave; nella sua natura.&nbsp;<br />
Lo spazio era cosi piccolo da&nbsp;<strong>non poter nemmeno distendere le ali.</strong><br />
<br />
Le condizioni di vita all&#39;interno di questi allevamenti sono&nbsp;cos&igrave; gravi, che Rosie &egrave; morta di stenti&nbsp;dopo poco&nbsp;tempo.</p>

<p>Ha sofferto veramente&nbsp;<em>da morire</em>.</p>

<p>L&#39;incuria umana&nbsp;ha permesso che il suo corpo&nbsp;in decomposizione rimanesse per giorni all&#39;interno della gabbia dove, nel frattempo, altre sue sorelle continuavano a soffrire.&nbsp;&nbsp;</p>

<p>Rosie, come il resto delle galline ovaiole negli allevamenti intensivi, apparteneva a una razza che ha sub&igrave;to la <strong>selezione artificiale</strong> nel corso dei decenni passati. Il<strong>&nbsp;tasso di deposizione</strong> delle uova di questa razza &egrave; del tutto&nbsp;<strong>innaturale:</strong>&nbsp;depongono circa 300 uova&nbsp;all&#39;anno.&nbsp;</p>

<p>Se una gallina vivesse libera in natura questo numero sarebbe sensibilmente pi&ugrave; basso.</p>

<p><img alt="" class="wp-image-1623" src="/app/uploads/2016/07/rosie_2_0.jpg" style="width: 850px; " /></p>

<p>Queste galline passano la loro<strong>&nbsp;vita intera</strong> all&#39;interno di capannoni per la produzione di uova, e dopo circa <strong>un anno e mezzo</strong>&nbsp;le galline come Rosie vengono inviate al <strong>macello,</strong>&nbsp;quando ormai non sono pi&ugrave; utili al reddito delle aziende avicole.</p>

<p>La storia di Rosie&nbsp;&egrave; la stessa di altri milioni di animali: una storia che nessuno conoscer&agrave; mai perch&eacute; volutamente tenuta&nbsp;al riparo dall&#39;opinione pubblica.<br />
<br />
<img alt="" class="wp-image-1618" src="/app/uploads/2016/07/dead_poultry.jpg" style="width: 850px; " /></p>

<p>L&#39;industria delle uova miete vittime tanto quanto l&#39;industria della carne.</p>

<p><br />
Hai mai pensato di iniziare a lasciare le uova fuori dal tuo piatto?<br />
<strong><a href="http://www.vegolosi.it/glossario/sostituire-le-uova-consigli-pratici/">In questo articolo</a></strong> trovi un sacco di consigli pratici per <strong>sostituire le uova</strong>!</p>

