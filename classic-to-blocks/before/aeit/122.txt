C'è voluto un anno prima di poter liberare questi animali, la 'proprietaria', che acquistava pulcini e anatroccoli su internet per poi rivenderli online, li teneva tutti rinchiusi in un ambiente comune privo di ripari, senza alcuna cura, in condizioni di sovraffollamento e senza tener conto dei bisogni etologici differenti di ognuna delle specie.

Alla fine gli sforzi congiunti del Woodstock Sanctuary di New Yourk e dell'Ulster County SPCA, hanno portato al sequestro degli animali. Le sofferenze inflitte a questi uccelli sono state molte: gli ambienti erano piccoli e sporchi, sovraffollati, pieni di escrementi mai rimossi, polvere, odore di ammoniaca causato dalle esalazioni. Molti uccelli presentavano ferite e traumi, e non solo di natura fisica ma anche psicologica.

<strong>Ora sono tutti liberi, ospiti del rifugio che li sta curando offrendo loro la possibilità di una nuova vita</strong>. Quello che condividiamo con voi è un video in cui le anatre incontrano per la prima volta nella loro vita l'acqua. Basta guardare il filmato per capire. Un animale, anche se allevato e costretto alla cattività, ricorda la sua natura, ricorda di essere un animale e sa come si vive.

Noi dobbiamo solo dargli una possibilità. Loro faranno tutto il resto e torneranno ad essere degli individui liberi.

<iframe src="https://www.youtube.com/embed/PrPajlIsKd0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
