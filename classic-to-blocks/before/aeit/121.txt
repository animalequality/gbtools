<div class="center"><img class="wp-image-1090" src="/app/uploads/2012/10/7626444050_03c9239312_c.jpg" /></div>
<p class="rtejustify">La societ&agrave; per azioni inglese Creek Project Investments ha annunciato che a partire dal mese di aprile 2013 interromper&agrave; il sostegno al controverso progetto riguardante un gigantesco impianto per la produzione di foie gras nella regione cinese di Jianxi, destinato a diventare l&rsquo;allevamento di oche ed anatre pi&ugrave; grande del mondo.<br />
	<br />
	<img alt="" class="wp-image-10656" src="/app/uploads/2012/10/fg_francia2.jpg" style="width: 200px; border-width: 0pt; border-style: solid; margin: 10px 5px; float: left;" />Xu Qingchu direttore di una filiale cinese, la Nanchang Sangha Economic and Technology Development Area, ne ha confermato la cancellazione sul National Business Daily.<br />
	Il progetto che pianificava investimenti per 106 miliardi di yuan (pari a circa 16.8 milioni di dollari USA) avrebbe portato<strong> il numero di anatre a 2 milioni e a 8 milioni quello delle oche, tutti animali destinati all&rsquo;alimentazione forzata e alla macellazione per la produzione di foie gras, in un allevamento che sarebbe stato il pi&ugrave; grande del mondo con una produzione pari a circa una tonnellata annuale</strong>, secondo il sito web della Creek Project.<br />
	<br />
	Considerato una delicatezza specie in Francia, il foie gras deriva dall&rsquo;ingrossamento fuori misura del fegato di anatre ed oche attraverso la pratica dell&rsquo;alimentazione forzata.<br />
	Tale pratica &egrave; da considerarsi disumana.<br />
	<br />
	Il Green Beagle Environment Institute, una organizzazione cinese per la difesa degli animali e dell&rsquo;ambiente, ha tenuto una conferenza nel marzo scorso nel Bejin, dichiarandosi contraria al progetto rivolgendosi anche ad organizzazioni governative per avere sostegno.<br />
	Durante la conferenza il GBEI ha paragonato l&rsquo;alimentazione forzata alla stessa tortura inflitta agli orsi a cui si estrae la bile, pratica conosciuta in Cina, dal momento che ancora oggi &egrave; usata per produrre presunti medicamenti.<br />
	<br />
	Nonostante ci&ograve; diversi imprenditori a Jianxi sono ancora ottimisti riguardo al progetto iniziale: i nuovi ricchi cinesi considerano il consumo di foie gras una sofisticata novit&agrave; dal momento che in Francia il pat&eacute; di fegato d&rsquo;oca &egrave; considerato una delicatezza.<br />
	<br />
	Per questo motivo gli attivisti cinesi invitano a continuare a firmare la petizione contro il mega progetto finch&eacute; non ci sar&agrave; la certezza del suo abbandono definitivo e allo stesso tempo di inviare mail ai responsabili dell&rsquo;azienda. Qui potete trovare la petizione da firmare, della quale riportiamo la traduzione in Italiano.</p>
<div class="rtejustify" style="padding: 8px; border: 1px none; background: none repeat scroll 0% 0% rgb(237, 237, 237); border-radius: 5px 5px 5px 5px;">
	All&rsquo;attenzione del Creek Project Investments - Bryan Cook, Swee Mok, Wai Weng, Swee Lead.<br />
	<br />
	<em>Spett.le MP di Huntingdon, Jonathan Djanogly,<br />
	&egrave; a conoscenza di quello che sta avvenendo?<br />
	<br />
	In Inghilterra il foie gras &egrave; stato vietato. Quindi se questo finanziamento non &egrave; un atto di vera ipocrisia dovr&ograve; rivedere il mio dizionario.<br />
	<br />
	 Sono venuto/a a conoscenza del vostro progetto di finanziamento per quello che dovrebbe diventare il pi&ugrave; grande allevamento nel mondo per la produzione di foie gras, questo nonostante la produzione sia vietata in UK e in altri paesi.<br />
	<br />
	 Riguardo al GeeseProject,sul vostro sito web si legge che l&rsquo;obiettivo del progetto &egrave; quello di conciliare cambiamento e alti standard di vita. Tuttavia, e credo di parlare a nome di tutte le persone compassionevoli, non vi sembra che le vostre azioni contraddicano le vostre dichiarazioni?<br />
	<br />
	 Come disse Gandhi: &rdquo;Il valore e la grandezza di una nazione di misurano osservando il modo in cui vengono trattati gli animali&rdquo;.  Queste parole piene di verit&agrave; dovrebbero ispirare una societ&agrave; come la vostra.  Nelle vostre intenzioni a azioni a sostegno della produzione di foie gras non vi &egrave; in alcuna modalit&agrave; che possa considerarsi etica n&eacute; legale. <br />
	<br />
	Vi chiedo pertanto di agire immediatamente per fermare un investimento che ha finalit&agrave; tanto atroci.  Un atto ipocrita non raccoglie consenso n&eacute; rispetto persino nel mondo degli affari. <br />
	Cordiali saluti,<br />
	<br />
	[Nome]</em></div>
<p class="rtejustify">Gli attivisti invitano anche ad inviare una lettera cartacea (la trovate aprendo la petizione) agli indirizzi indicati. E&rsquo; possibile, inoltre, riempire un formulario web dedicato al &lsquo;Geeseproject&rsquo; (sempre aprendo la petizione di cui sopra) esprimendo la vostra contrariet&agrave;.</p>

