<div class="center"><img class="wp-image-1881" src="/app/uploads/2016/12/ae_10years_6_0.jpg" /></div>
<p>Si trattava di un momento importante per la nostra organizzazione per una lunga serie di motivi ed &egrave; stato magnifico condividere questo anniversario con chi ogni giorno ci supporta e ci aiuta quotidianamente a portare avanti il nostro lavoro in difesa degli animali.</p>

<p>A Serra Lorenzini siete intervenuti in tanti e ci avete riempito il cuore, dimostrandoci ancora una volta come Animal Equality vada ben oltre le persone che ogni giorno animano le nostre scrivanie o sono sul campo ad investigare, dialogare o pi&ugrave; in generale salvare vite.</p>

<p>&nbsp;</p>

<center><img alt="" class="wp-image-1888" src="/app/uploads/2016/12/1_0.jpg" style="width: 850px; " /></center>

<p>&nbsp;</p>

<p>L&#39;impressione che stesse accadendo qualcosa di magico che avrebbe lasciato un segno dentro di noi ronzava nelle nostre teste gi&agrave; durante l&#39;introduzione all&#39;evento di <strong>Kris Reichert</strong>., celebre volto televisivo italiano ed una delle migliori voci di Radio Monte Carlo che negli anni si &egrave; fatta pi&ugrave; volte portavoce della sofferenza degli animali all&#39;interno dell&#39;industria della carne.<br />
Kris ci ha emozionato con le sue parole dimostrando profonda ammirazione e gratitudine per il lavoro che svolgiamo, ed ha chiuso il suo discorso dicendo qualcosa che non dimenticheremo:</p>

<h4>&ldquo;non importa quanto gli animali siano nascosti, negletti, o dimenticati; finch&eacute; ci saranno organizzazioni come quella che stiamo celebrando stasera, essi non saranno mai soli, ed avranno sempre una speranza. La speranza di poter contare su esseri umani compassionevoli, coraggiosi, organizzati, e soprattutto pronti a dedicare la propria vita ad essere la loro voce&rdquo;.</h4>

<p><img alt="" class="wp-image-1889" src="/app/uploads/2016/12/3_0.jpg" style="width: 850px; " /></p>

<p>&nbsp;</p>

<p>Da li in poi, al microfono, si sono alternate tante tante persone importanti per noi: la nostra fondatrice <strong>Sharon</strong>&nbsp;<strong style="line-height: 1.6;">N&uacute;&ntilde;ez</strong> che ha raccontato come un&#39;organizzazione fondata da tre ragazzi a Madrid sia diventatata in soli dieci anni uno dei punti di riferimento per chiunque voglia difendere gli animali utilizzando un<strong style="line-height: 1.6;"> approccio inclusivo ma il pi&ugrave; efficace possibile;</strong>&nbsp;il nostro direttore esecutivo&nbsp;<strong>Matteo Cupi</strong>, che ha raccontato ai presenti la storia della nostra organizzazione in Italia;&nbsp;il nostro responsabile dello sviluppo&nbsp;<strong>Fabio Pasiani&nbsp;</strong>che ha scaldato il cuore dei presenti con uno degli appelli a non lasciar soli gli animali fra i pi&ugrave; commoventi di quelli a cui abbiamo assistito negli ultimi tempi.</p>

<p>A chiudere gli interventi infine, ancora una persona speciale per la nostra organizzazione: una persona la cui voce ha narrato buona parte dei video italiani di Animal Equality:&nbsp;<strong>Stefania Depeppe</strong>.</p>

<p>Stefania &egrave; una&nbsp;doppiatrice italiana da anni dedicata alla difesa degli animali nonch&egrave; la firma dietro al blog <a href="https://ethical-code.com/">Ethical Code</a>.&nbsp;Ci ha regalato un momento di pura magia con la lettura di un brano tratto dal libro &ldquo;<em>Se niente importa. Perch&eacute; mangiamo gli animali?</em>&quot; di <strong>Jonathan Safran Foer</strong>.<br />
<br />
La sua splendida voce e la sua straordinaria bravura hanno commosso tutte le persone presenti in sala: non ci sarebbe stata&nbsp;migliore chiusura possibile per una serata cos&igrave; speciale.</p>

<p>&nbsp;</p>

<center><img alt="" class="wp-image-1883" src="/app/uploads/2016/12/ae_10years_8.jpg" style="width: 500px; " /></center>

<p>&nbsp;</p>

<p>Per delle persone che ogni giorno sono impegnate in un lavoro senza tregua ne sosta in difesa degli animali, spesso scontandosi con difficolt&agrave; snervanti, tutte&nbsp;quelle belle parole, unite all&#39;immagine di <strong>una sala piena di persone cos&igrave; speciali</strong>, diventano&nbsp;<strong>carburante dal valore inestimabile</strong> per tutto il lavoro ancora da compiere.&nbsp;</p>

<p>Ecco dunque perch&egrave; poi, finiti gli interventi non abbiamo potuto fare altro che incontrarvi e ringraziarvi uno per uno.</p>

<p>Quello che avete fatto per noi venerd&igrave; scorso non ha prezzo.</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-1884" src="/app/uploads/2016/12/ae_10years_9.jpg" style="line-height: 1.6; width: 850px; " /></p>

<p>&nbsp;</p>


<h4>In dieci anni, Animal Equality &egrave; passata dall&#39;essere un&#39;organizzazione fondata da tre ragazzi spagnoli ad essere presente in otto differenti nazioni, sparse su ben tre continenti ed essere riconosciuta a livello internazionale come una delle pi&ugrave; efficenti fra le tante che operano per i diritti animali.</h4>

<p>&nbsp;</p>


<p>&nbsp;</p>

<p><img alt="" class="wp-image-1887" src="/app/uploads/2016/12/2.jpg" style="width: 850px; " /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>I <strong>risultati raggiunti</strong> in questo decennio di duro lavoro <strong>sono stati tanti</strong>, ma la strada che ci porter&agrave; ad un <strong>mondo senza crudelt&agrave; nei confronti degli animali </strong>&egrave; ancora molto lunga: proprio per questo &egrave; nostra intenzione non fermarci qui. Siamo determinati ad andare avanti ed a vincere tutte le sfide necessarie&nbsp;affinch&egrave; gli animali possano un giorno&nbsp;essere completamente fuori dalla portata delle sofferenze causate dall&#39;uomo.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-1876" src="/app/uploads/2016/12/ae_10years_2.jpg" /></p>

<p>&nbsp;</p>

<p>Per questo, proprio venerd&igrave; sera, assieme ai nostri fondatori <strong>Sharon N&uacute;&ntilde;ez, Javier Moreno e Jose Valle</strong> vi abbiamo presentato la <strong>nostra nuova squadra italiana</strong>, un team fatto di persone giovani, tremendamente competenti ed irriducibilmente visionarie.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-1875" src="/app/uploads/2016/12/ae_10years_1.jpg" /></p>

<p>&nbsp;</p>

<p>Persone che non hanno paura di <strong>insistere su quello che tutti gli altri considerano impossibile</strong>. Persone che insomma condividono quella<strong> visione di mondo</strong> che siamo sicuri essere anche<strong> la vostra</strong>.</p>

<p>Non dobbiamo dimenticarci infatti che <strong>VOI</strong> siete la linfa che alimenta ogni nostra azione. Tutto quello che siamo riusciti a raggiungere in questi primi dieci anni &egrave; stato possibile solo grazie a voi.</p>

<p>Per questo motivo venerd&igrave; sera eravamo cos&igrave; contenti della vostra partecipazione.</p>

<p>Per questo motivo ogni giorno siamo orgogliosi ed incredibilmente riconoscenti per il vostro supporto.</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-1880" src="/app/uploads/2016/12/ae_10years_5.jpg" /></p>

<h4>Il capitolo dei prossimi dieci anni di Animal Equality &egrave; ancora tutto da scrivere, ma siamo sicuri&nbsp;che proprio grazie a voi, saranno degli anni pieni di vittorie e risultati concreti per tutti gli animali.</h4>

<p>&nbsp;</p>

<p>Rimane quindi solo un&#39;ultima cosa da dire: <strong>grazie infinite per essere sempre al nostro fianco</strong>.</p>

<p>Questa festa &egrave; stata la vostra festa.</p>

<p><a href="https://www.flickr.com/photos/animalequalityitalia/sets/72157673978031624/">Guarda l&#39;intera gallery dell&#39;evento</a></p>

