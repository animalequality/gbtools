<div class="center"><img class="wp-image-1653" src="/app/uploads/2016/08/jose-valle-animal-equality-investigator.jpg" /></div>
<p>Quando avevo 8 anni ho assistito alla macellazione di una scrofa: aveva speso una vita intera dentro una gabbia in un villaggio dove ero solito trascorrere l&#39;estate quando ero piccolo. I miei vicini e alcuni membri della mia famiglia la uccisero proprio di fronte a me. L&#39;ho vista lottare per la vita, per quanto fosse stata una vita infelice. L&#39;ho sentita urlare e poi ho visto il suo sangue iniziare a scorrere.</p>

<p>Volevo fare qualcosa per fermare quella scena, ma alla fine non ho potuto far nulla. Mi sono sentito impotente e l&#39;unica cosa che ho potuto fare &egrave; stato tentare di razionalizzare rapidamente quello che stavo vedendo. Tentai di spiegarmelo come una specie di male necessario di cui, in quel momento, non riuscivo a comprenderne il motivo.</p>

<p><img alt="" class="wp-image-1649" src="/app/uploads/2016/08/JOSE_VALLE_ANIMAL_EQUALITY_0.png" /><br />
&nbsp;</p>

<p>Molte persone mi chiedono come riesca ad infiltrarmi nei macelli e soprattutto come riesca a filmare tutto quell&#39;orrore a cui assistito. Io di solito rispondo che ho sviluppato una certa capacit&agrave; nel mettere momentaneamente in &quot;stand-by&quot; le mie emozioni per poter portare avanti il mio lavoro investigativo, fingendomi impassibile a quello che sta succedendo agli animali, facendo buon viso a cattivo gioco per continuare a documentare quei tragici momenti.</p>

<p>Credo che, in parte, lo sviluppo di questa abilit&agrave; abbia a che fare con la macellazione di quella scrofa avvenuta 29 anni fa.</p>

<p><img alt="" class="wp-image-1651" src="/app/uploads/2016/08/animal-equality-investigators.png" /></p>

<p>Ora, quando sono in questi luoghi, cerco di ottenere il massimo numero di immagini e video, come se questo potesse dare un senso all&rsquo;insensatezza di questi momenti, come se fosse un modo per tirare fuori qualcosa di utile per porre fine a tutto questo orrore.</p>

<p><img alt="" class="wp-image-1650" src="/app/uploads/2016/08/MAIALE_MACELLO_OCCHI.png" /></p>

<p>Queste immagini sono in un certo senso tutto ci&ograve; che rimane di questi animali e raccontare al mondo come hanno vissuto e come sono stati uccisi &egrave; il mio modo per dare un senso ad una morte che un senso proprio non ce l&#39;ha: quelle immagini che si imprimono nella mia videocamera e nella mia memoria, potranno poi imprimersi anche nella memoria di altre persone.</p>

<p>Dimenticare la disperazione che si legge negli occhi degli animali durante i loro ultimi attimi in un mattatoio &egrave; impossibile.</p>

<p><img alt="" class="wp-image-1652" src="/app/uploads/2016/08/pig_slaughterhouse_camera.png" /></p>

<p>Cos&igrave; come &egrave; impossibile dimenticarsi di come continuino a lottare per una via d&#39;uscita che non c&#39;&egrave;, utilizzando tutta la loro voce o piangendo, spesso in mezzo all&#39;indifferenza dei loro carnefici. &Egrave; un qualcosa che non posso dimenticare. &Egrave; qualcosa che non voglio dimenticare. L&#39;immensit&agrave; di questo problema, ed il conto delle vittime brutalizzate costantemente in aumento&hellip; possono farci crollare.</p>

<p>E se riesco a non crollare &egrave; solo perch&eacute; ho visto tutti questi animali combattere per la propria vita fino alla fine&hellip; ed anche perch&eacute; ho incontrato molte persone come voi, che giorno dopo giorno si battono senza tregua dando una voce a chi non ce l&rsquo;ha.</p>

<p>Grazie per essere una fonte d&#39;ispirazione costante e per continuare a ricordarmi che vinceremo noi.</p>

<p>Grazie per ricordarmi che stiamo gi&agrave; vincendo questa battaglia e che ogni passo, ogni animale salvato, costituisce gi&agrave; di per s&eacute; una vittoria.</p>

<p>Firmato: Jose Valle.<br />
<br />
- - -</p>

<p><em>Nel 2006 Jose Valle ha fondato a Madrid Animal Equality assieme a Sharon N&uacute;&ntilde;ez e Javier Moreno. Da quel giorno, la nostra organizzazione lotta a fianco degli animali in ben 3 continenti.<br />
Jose ad oggi &egrave; direttore internazionale delle investigazioni e coordina il lavoro sul campo dei nostri investigatori.</em><br />
&nbsp;</p>

