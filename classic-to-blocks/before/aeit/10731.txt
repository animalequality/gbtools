<div class="center"><img class="wp-image-1063" src="/app/uploads/2012/08/a1.jpeg" /></div>
Geoffrey Towell, 54 anni, di East Harlink, Norfolk, è stato condannato a 18 settimane di reclusione e non potrà lavorare in nessun allevamento di animali per i prossimi 10 anni.

James Dove, 27 anni, di Wymondham, Norfolk, ha ricevuto una pena di 8 settimane e sospeso dal lavoro per cinque anni, dalla Norwich Magistrates'Court.

Il Giuduce Distrettuale Peter Veits ha definito questo caso uno dei peggiori atti di deliberata crudeltà verso gli animali che abbia mai visto.

Jonathan Eales ha affermato che l'accusa più grave rivolta a Towell è quella di aver colpito tre maiali alla testa, con una sbarra di metallo, fino ad ucciderli. Più volte è stato ripreso mentre sollevava violentemente dei maialini per le orecchie, e li tirava, scuoteva e sbatteva ripetutamente sul muro o per terra.

La corte ha ascoltato Dove, l'autista del trattore; anche lui si è reso colpevole di aver preso a calci gli animali, di averli colpiti con ferocia, di aver sollevato e scosso molti maialini prendendoli per le orecchie o per le gambe, per poi sbatterli al suolo. Sia Towell che Dove, erano soliti colpire i maiali con tubi di plastica.

Towell e Dove sono stati accusati di aver provocato inutile sofferenze agli animali, in contrasto con quanto previsto dall'Animal Welfar Act che, nel Regno Unito, dovrebbe tutelare il benessere degli animali negli allevamenti.

Ian Fisher, l'avvocato di Dove ha affermato : "<em>Ha agito d'impulso, in maniera istintiva, o imitando gli altri lavoratori dell'allevamento</em>". Ha aggiunto: "<em>l'umiliazione pubblica che sta vivendo è già una forma di punizione</em>".

Alcuni giorni dopo la diffusione dell'investigazione di Animal Equality, il proprietario dell'allevamento Stephen Brown, 52 anni, è stato trovato morto con una ferita di arma da fuoco alla testa. Il medico legale ha stabilito che si è trattato di suicidio.
L'avvocato di Towell, Jamieson Plummer, ha detto: "<em>Il mio assistito ha dovuto sopportare la morte del suo capo e questo ha causato in lui uno stato depressivo</em>". A seguito di queste considerazioni a quindi chiesto al giudice di revocare la clausola della condanna che prevede il dievieto per Towell di lavorare, per un anno, in qualsiasi allevamento di animali; questo causerebbe ulteriore stress emotivo, la perdita del lavoro e della casa, attaccata all'allevamento.

Il Giudice Veits ha invece ribadito la condanno per i due uomini, affermando: "<em>Questi crimini sono terribili, è facile dare la colpa a chi non c'è più, a chi non può più pagare per quello che è stato commesso. Towell ha dimostrato di non provare nessuna empatia nei confronti degli animali e di non essere capace di accudirli. Ha continuamente a colpire i maiali, a volte fino alla morte</em>".

Dove ha avuto un ruole 'minore' ma ha comunque dimostrato di non esser stato in grado di trattare gli animali come esseri viventi. Come conseguenza, dovrà svolgere 180 ore di lavoro non retribuito.

Per entrambi gli uomini è scattato il divieto di lavorare in qualunque allevamento - Towell per 10 anni, e Dove per 5.

<strong>Guarda il video della nostra investigazione all'interno di Harling Farm</strong>

<iframe src="https://www.youtube.com/embed/pQK4261GXyg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
