<div class="center"></div>
Amy Meyer, 25 anni, ha voluto vedere un macello con i suoi occhi. Come le è stato detto, non era possibile entrare per vedere gli animali e così si è incamminata per la via che costeggia la recinzione del mattatoio. Una strada pubblica.

Una scena in particolare ha rapito il suo interesse: <strong><em>"Una mucca, che sembrava malata o ferita, non riusciva nemmeno a stare in piedi e per questa veniva trascinata da un trattore come fosse un cumulo di macerie"</em></strong>.

Di fronte a questa terribile scena, Amy ha afferrato il suo cellulare e ha registrato quello che stava accadendo. Quando il direttore del macello è uscito per chiederle di smettere, Amy ha risposto che si trovava in una strada pubblica e che aveva il diritto di riprendere. Questo è stato anche ciò che la polizia ha affermato una volta sul posto.
Tuttavia, secondo il rapporto della polizia, il direttore ha accusato Amy di aver invaso la proprietà privata del macello, attraversando il filo spinato, anche se il funzionario ha detto che "non ci sono danni alla recinzione".

È curioso notare come il proprietario del macello in questione sia Darrell H. Smith, sindaco della città. I legami tra le autorità pubbliche e l'industria dello sfruttamento animale, sono molto comuni.

<strong>Attenzione: questo video contiene scene di esplicita violenza</strong>
<iframe src="https://www.youtube.com/embed/zhlhSQ5z4V4" width="640" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

<span style="font-size: 10px;">Il video. della <a href="http://www.humanesociety.org/news/news/2008/01/undercover_investigation_013008.html">Humane Society</a>, contiene immagini simili a quelle riprese da Amy Meyer nello Utah.</span>

<strong>Questo è il primo processo, in America, in cui viene applicata la legge Ag-Gag progettata per mettere a tacere gli investigatori sotto copertura che denunciano gli abusi sugli animali</strong>, diffusi negli allevamenti di tutto il paese. La normativa è una risposta diretta e repressiva nei confronti delle molteplici organizzazioni per i diritti animali che hanno lavorato in questi anni per sensibilizzare l'opinione pubblica. Le investigazioni hanno spesso determinato la chiusura degli allevamenti sotto inchiesta e hanno generato un enorme indignazione pubblica, arrivando anche a formalizzare accuse penali nei confronti dei lavoratori, per maltrattamenti verso gli animali.

Amy Meyer si è dichiarata 'non colpevole'. La sua prossima udienza è prevista per il 23 maggio.
