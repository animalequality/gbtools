<div class="center"><img class="wp-image-1715" src="/app/uploads/2016/10/FB_BB.jpg" /></div>
<strong>Tyson Foods</strong>, una delle più grandi aziende di lavorazione della carne degli Stati Uniti, ha appena deciso di investire in un settore che, tra le altre cose, <strong>punta anche a ridurre il consumo di carne di pollo, manzo e maiale, promuovendo proteine vegetali alternative</strong>.

Lo ha annunciato settimana scorsa, dichiarando pubblicamente un investimento in una quota non inferiore al 5% in <strong>Beyond Meat</strong>, una società Californiana che si occupa di produrre <em>'carni'</em> da proteine vegetali come quelle della soia e dei piselli.

Beyond Meat ha lanciato quest'anno il suo Beyond Burger, un burger che <em>trasuda e sfrigola grassi durante la cottura su piastra proprio come farebbe un vero hamburger. </em>La differenza? Beh… il Beyond Burger <em>è </em><strong>100% vegetale</strong>!

<img class="wp-image-1711" style="width: 850px;" src="/app/uploads/2016/10/carne_obesita_3_1.jpg" alt="" />

Per <strong>Whole Foods Market</strong> il Beyond Burger è così vicino ad un hamburger di carne che, nella sua catena di supermercati, può tranquillamente permettersi di <strong>venderlo nel reparto macelleria</strong>.
<h4><em>"La qualità del Beyond Burger è incredibile," ammette Monica McGurk, ex dirigente della Coca-Cola e vice presidente del reparto strategie ed investimenti di Tyson da questa primavera. "Pensiamo che sia un prodotto che farà di sicuro la differenza in questo nuovo settore del food business"</em></h4>
I termini dell'accordo non sono stati divulgati ma il giro di finanziamenti comprende altri investitori, come la<strong> Humane Society of the United States</strong>, che aveva già investito precedentemente in Beyond Meat.

Gli americani stanno mangiando molti più cibi a base vegetale e le aziende alimentari agiscono di conseguenza. La <strong>Plant Based Foods Association</strong> dichiara come questo settore sia lievitato fino a <strong>4.9 miliardi di dollari in soli 12 mesi</strong>, diventando la fetta di mercato con la crescita più elevata e veloce dell'intero settore alimentare. Alcuni esempi? Beyond Meat, Califia Farms o Heidi Ho.

<img class="wp-image-1545" style="width: 861px;" src="/app/uploads/2016/07/mangimi-animaili_0.jpg" alt="" />

Anche la sezione investimenti di General Mills sta investendo in Beyond Meat, così come in Kite Hill, un'azienda che utilizza noci ed altre proteine da fonti vegetali per sostituire quelle di origine animale in formaggi e derivati del latte.

Secondo Michele Simon, direttrice esecutiva della Plant Based Foods Association, l'investimento di Tyson segna una svolta storica: è infatti la prima volta in cui una delle più grandi aziende produttrici di carne del mondo investe in una società che produce alternative vegetali alla carne.
<h4><em>"Continuo a chiedermi come mai queste acquisizioni abbiano luogo" continua la signora Simon. "La visione più positiva potrebbe essere che l'industria della carne stia iniziando a spostarsi dalla carne animale alle sue alternative vegetali. Ma credo che non abbiamo ancora abbastanza elementi in mano per affermarlo con sicurezza. Potrebbe anche essere un modo per distogliere l'attenzione dalla loro produzione tradizionale".</em></h4>
<img class="wp-image-1713" style="width: 850px;" src="/app/uploads/2016/10/bybr.jpg" alt="" />

<em>Un burger Beyond Burger, in un'immagine delle campagne di Beyond Meat.</em>

Ethan Brown, fondatore e amministratore delegato di Beyond Meat, si aspettava che l'investimento avrebbe lasciato dubbiosi parecchie persone, in particolare tra vegani e vegetariani.
<h4><em>"Spero che questo passo venga però visto come parte di un percorso intenzionale che toglierà i prodotti cruelty free dall'angolino delle 'alternative vegetali' del supermercato e per ricollocarli in un posto in cui abbiano veramente una possibilità con il consumatore" ha detto Brown.</em></h4>
Una ricerca congiunta di NPD Group, Midan Marketing e Meatingplace, ha rilevato come il 70% dei consumatori di carne utilizzi alternative vegetali al posto ​​della carne almeno una volta alla settimana. E il 22% sostiene di utilizzarne di più rispetto all'anno prima.

Beyond Meat ha raccolto 17 milioni di dollari di investimenti lo scorso anno. Questo era il terzo round di finanziamento che CB Insights, l'unico per il quale è stato divulgato un importo.

Stiamo vivendo un momento particolare.

I cibi a base vegetale <a href="https://www.facebook.com/AnimalEqualityItalia/videos/1184560981583613/" target="_blank" rel="noopener"><strong>tolgono sempre più mercato</strong></a> ai loro antenati fatti con carne animale e raccolgono investimenti per un totale che qualche anno fa sarebbe stato semplicemente inimmaginabile.

Le aziende 'veg' possono permettersi di assumere figure come <strong>Don Thompson</strong>, un ex amministratore delegato di McDonald.

Gruppi di investitori realizzano rapporti come l'ultimo appena pubblicato dal <a href="https://www.fairr.org/"><strong>FAIRR</strong></a>, nel quale si <strong><a href="http://fairr.org/resources/reports/factory-farming-assessing-investment-risks" target="_blank" rel="noopener">mettono in guardia i colleghi investitori dal finanziamento di aziende legate all'industria della carne</a></strong>.

<img class="wp-image-1714" style="width: 850px;" src="/app/uploads/2016/10/vegburger.jpg" alt="" />

Insomma, appare evidente come quella che un tempo era una nicchia ristretta di persone, sia oggi un <strong>mercato solido ed in espansione</strong>.

E se questo è possibile, è perché sempre più persone considerano la scelta di alternative vegetali come la scelta migliore dal punto di vista etico, salutare ed ambientale.
<h4><a href="https://it.loveveg.com/"><strong>Scopri quanto è facile smettere fin da subito di fare del male agli animali e al pianeta.</strong></a></h4>
