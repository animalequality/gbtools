<div class="center"><img class="wp-image-1581" src="/app/uploads/2016/07/quanti_animali_salvati_1.jpg" /></div>
<p>Hai mai pensato di calcolare quanta carne hai evitato adottando una dieta a base vegetale?</p>

<p>Ti sei mai chiesto quante<strong> vite animali </strong>hai<strong> risparmiato </strong>grazie a questa tua scelta?</p>

<p>E se dovessimo parlarare di anidride carbonica, ti sei mai chiesto quanta <strong>CO2</strong> hai <strong>risparmiato al pianeta</strong> grazie alle tue abitudini alimentari compassionevoli?</p>

<p>&nbsp;</p>

<p>Probabilmente no, o forse si, te lo sarai anche chiesto, ma come tutti non sei riuscito a darti una risposta precisa.</p>

<p>Nessun problema, c&#39;&egrave; una soluzione!</p>

<p><img alt="" class="wp-image-1580" src="/app/uploads/2016/07/quanti_animali_salvati.jpg" style="width: 850px; " /></p>

<p><br />
Esiste un sito, <strong><a href="https://vegetariancalculator.com/">V</a><a href="http://vegetariancalculator.com/">egetarianCalculator.com</a></strong>, che ti permette di scoprire tutto questo. (<strong><a href="http://www.thevegancalculator.com/">Qui la versione vegana</a></strong>)</p>

<p>&nbsp;</p>

<p>Il sito &egrave; in inglese, ma anche se non hai dimestichezza con la lingua &egrave; comunque molto inguitivo e&nbsp;far&agrave; al caso vostro sia che siate vegetariani di vecchia data o relativamente nuovi all&#39;argomento.</p>

<p>Utilizzarlo &egrave; semplicissimo:</p>

<p>&bull; se avete abbandonato la carne<strong> da anni</strong>, scegliete il <a href="https://vegetariancalculator.com/"><strong>calcolatore annuale</strong></a>&nbsp;ed inserite il numero di <strong>anni</strong> vissuti senza prodotti animali</p>

<p>&bull; se invece avete abbandonato la carne <strong>da qualche mese</strong>, scegliete il <strong><a href="https://vegetariancalculator.com/">calcolatore mensile</a></strong>, ed inserite questa volta il numero di <strong>mesi</strong> passati da quando avete preso questa scelta.</p>

<p>&nbsp;</p>

<p>Cliccate su &quot;<em>Calculate</em>&quot; e vi compariranno tre risultati:</p>

<p>Il primo &egrave; il numero di <strong>animali risparmiati</strong>, il secondo sono le <strong>libbre di carne che avete evitato</strong> di ingerire ed il terzo le <strong>libbre di CO2 che avete risparmiato all&#39;atmosfera</strong>.</p>

<p>&nbsp;</p>

<p>1 libbra equivale a 0.45 kg circa, quindi per ottenere i vostri risultati in kg vi baster&agrave; moltiplicare il tutto per 0.45.<br />
Se invece volete rifare il calcolo, vi baster&agrave; cliccare su &quot;<em>Reset</em>&quot;, inserire un nuovo numero di anni o di mesi e&nbsp;poi cliccare nuovamente su &quot;Calculate&quot;.&nbsp;</p>

<p>&nbsp;</p>

<p>A volte un numero ha la capacit&agrave; di <strong>rendere molto pi&ugrave; definita un&#39;idea</strong>.<br />
<br />
Sei contento di aver salvato tutte queste vite ed aver risparmiato al mondo tutti questi kg di CO2?</p>

<h4>E per i vegani?</h4>

<p>Non preoccuparti, eccoti anche il tuo calcolatore:&nbsp;<a href="http://www.thevegancalculator.com/">www.thevegancalculator.com/</a></p>

