<div class="center"><img class="wp-image-1205" src="/app/uploads/2013/09/9814455046_0772243740_c.jpg" /></div>
Abbiamo potuto salvare 6 galline da un destino terribile. Queste ora possono vivere una vita in libertà in un santuario, accudite da persone che conoscono la compassione.

Questa liberazione viene chiamata <strong>“riscatto aperto”</strong> il che significa che <strong>gli attivisti hanno mostrato apertamente i loro volti e hanno rivelato le loro identità</strong>, assumendo così la piena responsabilità di quanto accaduto, <strong>ritengono che quanto fatto sia legittimo e desiderano ispirare una presa di coscienza nell’opinione pubblica che porti alla fine della schiavitù animale nella nostra società</strong>.

<strong>Guarda il video del riscatto aperto di 6 galline ovaiole da un allevamento all'aperto, in Germania.</strong>
<iframe width="640" height="480" src="https://www.youtube.com/embed/bInGyqAxXF4" frameborder="0" allowfullscreen></iframe>

Nel luogo in questione la sofferenza delle galline è evidente. Le situazioni documentate sono:

• <strong>Animali feriti</strong> (es. infezioni, infezioni purulente agli occhi, lesioni infette e infestate da acari)

• <strong>Mancanza di piumaggio</strong> (le piume vengono pizzicate dalle altre galline, segno di grave stress emotivo)

• <strong>Galline morte</strong>

Le galline vivono meno di 2 anni in queste condizioni, giunte a quell'età gli allevatori considerano insufficiente la loro produzione di uova e le mandano al macello.

Uno dei nostri attivisti di Animal Equality in Germania spiega: <cite>"il momento più difficile arriva quando bisogna lasciare l'impianto, sapendo di dover lasciare migliaia di animali dietro. Continueranno a soffrire per la loro prigionia, moriranno"</cite> e continua: <cite>"a mio avviso semplicemente non c'è motivo per non liberare questi animali, avendo la possibilità di farlo. Non cambierà il mondo, ma forse questo porterà qualcuno a pensare in modo diverso. E poi per sei di questi animali il mondo è davvero cambiato!"</cite>

<strong>Guarda la galleria fotografica del riscatto aperto di 6 galline ovaiole da parte degli attivisti di Animal Equality in Germania.</strong>
<object width="400" height="300"> <param name="flashvars" value="offsite=true&lang=it-it&page_show_url=%2Fphotos%2Fanimalequalityitalia%2Fsets%2F72157635642461885%2Fshow%2Fwith%2F9814455046%2F&page_show_back_url=%2Fphotos%2Fanimalequalityitalia%2Fsets%2F72157635642461885%2Fwith%2F9814455046%2F&set_id=72157635642461885&jump_to=9814455046"></param> <param name="movie" value="https://www.flickr.com/apps/slideshow/show.swf?v=124984"></param> <param name="allowFullScreen" value="true"></param><embed type="application/x-shockwave-flash" src="https://www.flickr.com/apps/slideshow/show.swf?v=124984" allowFullScreen="true" flashvars="offsite=true&lang=it-it&page_show_url=%2Fphotos%2Fanimalequalityitalia%2Fsets%2F72157635642461885%2Fshow%2Fwith%2F9814455046%2F&page_show_back_url=%2Fphotos%2Fanimalequalityitalia%2Fsets%2F72157635642461885%2Fwith%2F9814455046%2F&set_id=72157635642461885&jump_to=9814455046" width="400" height="300"></embed></object>
