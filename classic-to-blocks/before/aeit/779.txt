<div class="center"><img class="wp-image-2503" src="/app/uploads/2019/02/M6NTHS-2-copyright-Eline-Helena-Schellekens.jpg" /></div>
<p><b>M6NTHS (6 mesi)</b> &egrave; un cortometraggio della regista olandese Eline Helena Schellekens e mostra la vita di un suinetto che cresce in un allevamento intensivo. Il film &egrave; vincitore di un Panda Award, il pi&ugrave; importante riconoscimento per i film sulla natura.&nbsp;</p>

<p><strong>M6NTHS supporta l&rsquo;Iniziativa dei Cittadini Europei &nbsp;&ldquo;End the cage age&rdquo;</strong> contro l&rsquo;uso delle gabbie negli allevamenti con lo streaming in esclusiva, dal 5 al 18 febbraio, su questo sito web e quelli delle ONG aderenti all&rsquo;Iniziativa, del film in versione completa.&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p class="rtecenter"><iframe allowfullscreen="" frameborder="0" height="360" mozallowfullscreen="" src="https://player.vimeo.com/video/310773230" webkitallowfullscreen="" width="640"></iframe></p>

<p>&nbsp;</p>

<p class="rtecenter"><b>Anche tu pensi che gli animali negli allevamenti siano degni di una vita migliore?</b></p>

<p class="rtecenter"><b>Entra in azione e <a href="https://eci.endthecageage.eu/?rescountry=it&amp;shareURL=https%3A%2F%2Fanimal-equality-it.endthecageage.eu%2Fit-IT%2Flive&amp;lang=it&amp;country=it&amp;shareURLHMAC=215c3f59aebc6cf75f452f974838ae6adb6a21e04c89cae6b1ae02e3021faee6&amp;nationality=it&amp;channel=channel026">FIRMA ANCHE TU</a> la petizione contro l&rsquo;uso delle gabbie negli allevamenti</b></p>

<p class="rtecenter">&nbsp;</p>

<center><a href="https://eci.endthecageage.eu/?rescountry=it&amp;shareURL=https%3A%2F%2Fanimal-equality-it.endthecageage.eu%2Fit-IT%2Flive&amp;lang=it&amp;country=it&amp;shareURLHMAC=215c3f59aebc6cf75f452f974838ae6adb6a21e04c89cae6b1ae02e3021faee6&amp;nationality=it&amp;channel=channel026"><img alt="" class="wp-image-2479" src="/app/uploads/2018/10/fima-ora-1.jpg" /></a></center>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><b>ULTERIORI INFORMAZIONI SULL&#39;INIZIATIVA DEI CITTADINI EUROPEI</b></p>

<p>La petizione non &egrave; una semplice richiesta di sostegno online. Si tratta di <b>una vera e propria iniziativa dei cittadini europei, una forma di democrazia diretta prevista dall&#39;Unione Europea</b>.</p>

<p>Raccogliendo <b>ALMENO UN MILIONE di firme in tutta Europa</b>, possiamo chiedere alla Commissione di esprimersi sul tema delle gabbie e avviare cos&igrave; un processo per l&#39;eliminazione definitiva di questa tortura dal nostro continente.</p>

<p>Perch&eacute; la firma sia valida &egrave; necessario:&nbsp;</p>

<ul>
	<li>Inserire un numero di documento di identit&agrave; (passaporto o carta di identit&agrave;)</li>
	<li>Inserire i propri dati anagrafici</li>
</ul>

<p><b>Questo perch&eacute; si tratta di una petizione CERTIFICATA</b>: vogliamo e dobbiamo essere sicuri che nessuno falsifichi la vostra firma e che si tratti di adesioni autentiche.</p>

<p>I vostri dati sono protetti in base alle normative vigenti e <b>NON verranno usati per alcun altro scopo</b>, se non per aderire all&#39;iniziativa.</p>

<p>&nbsp;</p>

<center><a href="https://eci.endthecageage.eu/?rescountry=it&amp;shareURL=https%3A%2F%2Fanimal-equality-it.endthecageage.eu%2Fit-IT%2Flive&amp;lang=it&amp;country=it&amp;shareURLHMAC=215c3f59aebc6cf75f452f974838ae6adb6a21e04c89cae6b1ae02e3021faee6&amp;nationality=it&amp;channel=channel026"><img alt="" class="wp-image-2479" src="/app/uploads/2018/10/fima-ora-1.jpg" /></a></center>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

