<p>Quello che avviene nei macelli &egrave; una crudelt&agrave; senza fine. Ed &egrave; per questo che <a href="https://animalequality.it/video-macello-conigli/">abbiamo lanciato una nuova petizione per mettere fine alle torture atroci</a> che gli animali sono costretti a subire&nbsp;a causa dei mancati controlli e della poca attenzione delle persone che lavorano nell&rsquo;industria della carne.&nbsp;</p>

<p>Abbiamo cominciato a dicembre dell&rsquo;anno scorso a rilasciare video che dimostrano le condizioni in cui sono costretti gli animali, che siano <a href="https://www.youtube.com/watch?v=GsUMaPEYS0w">piccoli agnellini&nbsp;uccisi ancora coscienti</a> o <a href="https://www.youtube.com/watch?v=Me6Fvnhf8Hc">maiali che non vengono storditi</a> e cercano disperatamente di sfuggire a questo destino osceno.&nbsp;</p>

<p>Questa volta invece abbiamo deciso di portarvi in un macello di conigli, grazie al lavoro di un nostro investigatore che, operando sotto copertura, &egrave; riuscito a filmare le condizioni in allevamento e la fine brutale di questi animali in un macello vicino a Brescia.&nbsp;</p>

<center>
<p><img alt="" class="wp-image-2352" src="/app/uploads/2018/03/38994862060_2fda17a207_k.jpg" style="width: 840px; " /></p>
</center>

<p>I conigli passano tutta la vita in una gabbia&nbsp;e, a sole 12 settimane, sono avviati a un destino doloroso e spesso compiuto senza il pieno rispetto delle norme, in particolare quelle relative allo stordimento.&nbsp;</p>

<p>Nel video si vede chiaramente che:&nbsp;</p>

<ul>
	<li>Gli operatori non stordiscono adeguatamente gli animali&nbsp;</li>
	<li>Gli operatori sgozzano in modo sistematico conigli coscienti</li>
	<li>Gli operatori spezzano il collo degli animali a mani nude&nbsp;</li>
	<li>Gli animali sono costretti ad attendere la propria fine ammassati in gabbie&nbsp;</li>
	<li>I conigli urlano di paura e terrore quando vengono estratti dalle gabbie</li>
	<li>Gli animali sono costretti a vivere e ad essere trasportati in gabbie dove non possono esprimere i propri comportamenti naturali&nbsp;</li>
</ul>

<center>
<p><iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="405" src="https://www.youtube.com/embed/02T8XEVwMqs" width="720"></iframe></p>
</center>

<p>Lo stordimento &egrave; obbligatorio per tutti gli animali prima dell&rsquo;abbattimento, ma - secondo la legge italiana - &nbsp;sono previste solo sanzioni amministrative per chi viola le prescrizioni sulla procedura di macellazione, sanzioni che nei casi pi&ugrave; gravi prevedono la pena pecuniaria di &euro;6.000.&nbsp;</p>

<p>Questa mancanza di conseguenze penali concrete, insieme alla totale assenza di controlli severi, comporta una maggiore leggerezza nella gestione di questa fase da parte degli operatori che lavorano nei macelli e, come dimostrano le nostre inchieste, spesso costringe gli animali a subire atroci torture.</p>

<p><img alt="" class="wp-image-2354" src="/app/uploads/2018/03/40762805422_bcf07bfe3d_k.jpg" style="width: 840px; " /></p>

<p>&laquo;Questa inchiesta dimostra che &egrave; necessario introdurre delle norme specifiche per la tutela degli animali che vengono macellati nel nostro paese&raquo; spiega Matteo Cupi, direttore esecutivo di Animal Equality Italia.&nbsp;</p>

<p>&laquo;L&rsquo;Europa si &egrave; gi&agrave; espressa per una maggiore tutela dei conigli allevati per la carne, che non potranno pi&ugrave; essere allevati in gabbia grazie a un voto del Parlamento europeo nel marzo 2017, un risultato storico ottenuto grazie a un&rsquo;intensa campagna di pressione politica da parte di Animal Equality. Ovviamente per&ograve; tutto questo non &egrave; la soluzione alle sofferenze che gli animali vivono nei macelli. Infatti i consumatori possono aiutare per primi i conigli facendo scelte alimentari mirate, come ridurre o evitare il consumo di carne&raquo;.&nbsp;</p>

<p>Noi lavoriamo da anni per ridurre la sofferenza degli animali durante la macellazione. &nbsp;</p>

<p>Ed &egrave; per questo che abbiamo lanciato una petizione rivolta al Parlamento per ottenere, fra le varie richieste, l&rsquo;installazione di telecamere a circuito chiuso nei macelli e il rispetto dello stordimento senza deroghe ed eccezioni.&nbsp;</p>

<p>Sappiamo che non &egrave; la soluzione definitiva che vorremmo, ma siamo disposti a fare di tutto per migliorare la vita degli animali, nella speranza che la situazione possa cambiare al pi&ugrave; presto.&nbsp;</p>

<p>Tutto questo &egrave; possibile anche perch&eacute; ci sono cittadini impegnati in questa battaglia al nostro fianco. <a href="https://animalequality.it/video-macello-conigli/">Sono pi&ugrave; di 60.000 le persone che hanno gi&agrave; firmato la nostra petizione, unisciti a loro</a>!</p>

