<div class="center"><img class="wp-image-1391" src="/app/uploads/2016/02/Vegan_Gardein_Tofu_Foods_Display_cropped1.jpg" /></div>
<p>Nel 2009 appena l&rsquo;1% della popolazione degli Stati Uniti aveva eliminato la carne o i prodotti di origine animale dalla propria alimentazione. Adesso <strong>il 5% degli americani &egrave; vegetariano e la met&agrave; di queste persone ha eliminato totalmente i prodotti animali</strong>. Negli ultimi anni, quindi, i dati hanno subito un&rsquo;impennata e il numero&nbsp;sembra&nbsp;destinato&nbsp;ad aumentare. La ragione di questo incremento&nbsp;risiede in diversi fattori, ma quello che ha avuto<strong> l&rsquo;impatto pi&ugrave; forte sembra essere la maggiore&nbsp;informazione sugli allevamenti intensivi e sul maltrattamento degli animali.</strong></p>

<p>Sono ben <strong>16 milioni le persone che hanno eliminato ogni alimento di origine animale dalla propria dieta negli Stati Uniti</strong>. &Egrave; un numero notevole, considerando quanto sia forte la presenza della carne nella cultura americana. Quasi la met&agrave; di queste persone (42%) ha dichiarato di aver fatto questa scelta in seguito alla visione di un film educativo sull&rsquo;argomento; in moltissimi (69%) l&rsquo;hanno fatto perch&eacute; gli animali smettano di essere maltrattati e pi&ugrave; della met&agrave; (52%) di coloro che hanno compiuto questa scelta l&rsquo;hanno fatto meno di 10 anni fa. Tutti questi dati indicano che <strong>negli ultimi anni la popolazione americana &egrave; molto pi&ugrave; informata sulla provenienza del proprio cibo</strong>.</p>

<p><img alt="" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Vegan_Gardein_Tofu_Foods_Display_%28cropped1%29.jpg/1024px-Vegan_Gardein_Tofu_Foods_Display_%28cropped1%29.jpg" style="width: 800px; " /></p>

<p>Il consumo di carne &egrave; calato del 12,2% rispetto al 2007 e, secondo i dati di Google, <strong>la parola &ldquo;vegan&rdquo; viene digitata sempre pi&ugrave; frequentemente sul motore di ricerca</strong>. I risultati, d&rsquo;altro canto, sono passati dai 1.600 del 2007 agli oltre 24.000 attuali.&nbsp;<strong>Un numero sempre maggiore di celebrit&agrave; e di persone influenti sceglie veg</strong>, si pensi soltanto a personaggi come <strong>Bill Clinton, Natalie Portman, Ellen DeGeneres, Mike Tyson, Woody Harrelson o Alicia Silverstone</strong>. I ristoranti e i supermercati offrono <strong>sempre pi&ugrave; opzioni alternative alla carne e agli altri prodotti di origine animale</strong>.</p>

<p><img alt="" src="https://41.media.tumblr.com/tumblr_m02zcctf0o1rnom7fo1_1280.jpg" style="width: 800px; " /></p>

<p>Insomma, andando avanti cos&igrave; <strong>il cambiamento potrebbe essere inarrestabile!</strong></p>

<p>Ricordiamoci che,<strong> oltre a salvare milioni di animali, la riduzione o l&rsquo;eliminazione degli alimenti di origine animale &egrave; una scelta di vita salutare e sostenibile, che pu&ograve; diminuire il nostro impatto sull&rsquo;ambiente e al tempo stesso offrirci grandi vantaggi dal punto di vista della salute.</strong></p>

<p>&nbsp;</p>

