<div class="center"><img class="wp-image-2147" src="/app/uploads/2017/05/33828807854_7be3941ba9_z.jpg" /></div>
Le telecamere nascoste di Animal Equality hanno ripreso immagini di estrema violenza in un allevamento nel nord della Germania, tra cui: diversi <strong>pulcini morti o in fin di vita ritrovati in mezzo a quelli ancora vivi</strong>; un<strong> operatore che afferra un pulcino con una pinza e lo calpesta con violenza</strong>, presumibilmente con l’intento di ucciderlo; <strong>animali deboli o feriti gettati all’interno di secchi e carriole</strong> insieme a quelli morti, rimanendo soffocati sotto i corpi dei compagni.
<center><iframe src="https://www.youtube.com/embed/iVafxWG84kI" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe></center>

<center> </center>

<img class="wp-image-2141" style="width: 500px; margin-left: 7px; margin-right: 7px; float: right;" src="/app/uploads/2017/05/recherche-in-einem-masthhnchenbetrieb-in-niedersachsen_34257121071_o.jpg" alt="" />

Particolarmente agghiacciante la scena in cui <strong>un operatore colpisce ripetutamente un pulcino con l’estremità di un badile</strong>, causandogli profonde ferite. L’uomo <strong>getta poi l’animale, ancora vivo, in un cassonetto della spazzatura</strong>.

Dodici ore più tardi, il pulcino è stato ritrovato e soccorso dai nostri investigatori...

<strong>Solo in questo allevamento sono presenti oltre 224,000 polli chiusi in 16 capannoni. Nessuno di questi animali vedrà mai la luce del sole né potrà mai respirare aria fresca.</strong>

Per massimizzare il profitto, nel corso degli anni l’industria ha selezionato geneticamente i polli al punto che, oggi, <strong>essi raggiungono il peso di macellazione a sole 6 settimane di vita</strong>. A causa del ritmo accelerato di crescita cui sono costretti, la maggior parte di essi ha difficoltà di locomozione e soffre di gravi problemi cardiovascolari e respiratori. Milioni di polli presentano deformazioni alle zampe e moltissimi tra questi rimangono completamente paralizzati a poche settimane dalla nascita.

<img class="wp-image-2137" style="float: left; margin-left: 7px; margin-right: 7px; width: 500px;" src="/app/uploads/2017/05/recherche-in-einem-masthhnchenbetrieb-in-niedersachsen_33549785483_o.jpg" alt="" />

Di conseguenza, moltissimi pulcini cadono sulla propria schiena e non riescono più a rialzarsi, e poiché sono impossibilitati a raggiungere gli abbeveratoi sono condannati a morire lentamente di sete. Altri, come mostrato nel video, vengono raccolti dagli operatori e buttati via senza alcuna pietà.

<strong>Animal Equality ha provveduto immediatamente a sporgere denuncia contro l’allevamento in questione per chiedere che i responsabili siano giudicati per le proprie azioni. </strong>

<strong>Vogliamo che queste pratiche brutali siano messe al bando al più presto.</strong>

Scopri di più:

<a href="https://www.youtube.com/watch?v=JplSLx5BzrE"><strong>Guarda la storia e lieto fine di Gloria, salvata dall'industria della carne</strong></a>

<strong><a href="https://ianimal.it/">iAnimal: l'ultima video-inchiesta filmata a 360 gradi sugli allevamenti intensivi di polli</a></strong>

<strong><a href="https://animalequality.it/blog/7-cose-da-sapere-sulla-carne-di-pollo/">7 cose da sapere sulla carne di pollo</a></strong>

<strong><a href="https://www.youtube.com/watch?v=w75rbsjc2N8&amp;t=2s">Indagine sulla crudeltà degli incubatoi industriali di pollo</a></strong>
