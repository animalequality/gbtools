<div class="center"><img class="wp-image-1938" src="/app/uploads/2017/01/animali_difesa_ragazza_mucca__.jpg" /></div>
<p>Alcune dei propositi per il nuovo anno pi&ugrave; comuni sono mangiare meglio, perdere peso, adottare uno stile di vita pi&ugrave; salutare o contribuire a rendere il mondo un posto migliore. E sapete qual &egrave; la buona notizia? C&#39;&egrave; un piccolo passo che vi porter&agrave; pi&ugrave; vicini a tutto questo: lasciare carne e derivati fuori dal vostro piatto.</p>

<p>&nbsp;</p>

<p>Ecco 17 motivi per provare una dieta a base vegetale nel 2017:</p>

<p>&nbsp;</p>

<h4>17. Sentirsi meno stanchi.</h4>

<p>Mangiare sano &egrave; un modo sicuro per sentirsi sano e pieno di energia. Affaticherete meno il vostro corpo ed i risultati saranno subito visibili!</p>

<p>&nbsp;</p>

<h4>16. Risparmiare</h4>

<p>Cereali, frutta e verdura sono elementi fondamentali di una dieta sana, pensate a non dover pi&ugrave; spendere soldi in carne, pesce e formaggi. Come ne risentirebbe il vostro portafoglio?!</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-10479" src="/app/uploads/2017/01/vegani.jpg" style="width: 850px; " /></p>

<h4>15. Provare nuovi piatti</h4>

<p>Lasciare carne e derivati fuori dal vostro piatto vi aiuter&agrave; a scoprire nuovi piatti! Quasi ogni tipo di cucina ha ricette completamente a base vegetale, o facilmente adattabili. Non disperate: potrete andare avanti a mangiare lasagne, tacos, burger e fritti anche senza fare del male ad animali indifesi ;)</p>

<p>&nbsp;</p>

<h4>14. Perdere peso</h4>

<p>Se nella vostra lista avete inserito anche questo punto, bene, sappiate che una dieta senza prodotti di origine animale vi aiuter&agrave; in questo percorso! Uno studio della Cornell University ha dimostrato come chi segue diete 100% vegetali tenda a pesare in media 10, 20 kg in meno rispetto a chi ancora si nutre di carne!</p>

<p>&nbsp;</p>

<h4>13. Stringere nuove amicizie</h4>

<p>Entrare per la prima volta in ristoranti che non servono cibi animali o iniziare a seguire hashtag come #veganfoodporn su Instagram, vi porter&agrave; nel centro di un vortice di nuove amicizie fatte di splendide persone compassionevoli! Provateci!</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-1940" src="/app/uploads/2017/01/vegani_vecchi.jpg" style="width: 850px; " /></p>

<h4>12. Vivere pi&ugrave; a lungo</h4>

<p>Meno carne mangi, pi&ugrave; a lungo vivi. &Egrave; una teoria su cui concordano sempre pi&ugrave; autorit&agrave; scientifiche.</p>

<p>&nbsp;</p>

<h4>11. Abbandonare quello che si nasconde dietro (o dentro) al latte</h4>

<p>L&#39;industria lattiero-casearia &egrave; crudele allo stesso livello di quella della carne e sempre pi&ugrave; studi mostrano come il latte animale non faccia poi cos&igrave; bene al corpo umano. Sapevi che nel latte compaiono spesso anche tracce di sangue e di quello che &egrave; conosciuto come pus? Un motivo in pi&ugrave; per rimanerne alla larga, no?</p>

<p>&nbsp;</p>

<h4>10. Abbandonare quello che si nasconde dietro (o dentro) alla carne</h4>

<p>Non &egrave; raro che carne venga spesso contaminata con le feci, sangue e altri fluidi corporei degli animali a cui apparteneva. Lo sapevi che, proprio per questo, &egrave; la prima fonte di intossicazione alimentare negli Stati Uniti?</p>

<p>&nbsp;</p>

<h4>9. Prevenire le malattie</h4>

<p>Una dieta a base vegetale &egrave; la cosa migliore che il tuo corpo possa chiedere! Secondo l&#39;Academy of Nutrition and Dietetics, chi ha completamente eliminato carne e derivati ha meno probabilit&agrave; di sviluppare malattie cardiache, cancro, diabete, o pressione alta rispetto a chi ancora non ha compiuto questa scelta!</p>

<p>&nbsp;</p>

<h4>8. Essere in ottima compagnia</h4>

<p>Joaquin Phoenix, Natalie Portman, Ariana Grande, Al Gore, Flo Rida, Moby, e Kat Von D sono solo alcune delle personalit&agrave; che hanno abbandonato la carne e si stanno impegnando a diffondere una dieta a base vegetale.</p>

<p>&nbsp;</p>

<p>&nbsp;<img alt="" class="wp-image-10478" src="/app/uploads/2017/01/animali_difesa_acqua_ridistribuzione_idrica_sete.jpg" style="width: 850px; " />&nbsp;</p>


<h4>7. Risparmiare acqua!</h4>

<p>Lo sapevate che ci vogliono quasi 1.800 litri d&#39;acqua per produrre un solo chilo di carne di manzo?! Evitare alimenti a base di carne e derivati per un anno risparmia l&#39;equivalente di un anno di docce!</p>

<p>&nbsp;</p>

<h4>6. Salvare la foresta pluviale</h4>

<p>La maggior parte dei disboscamenti delle foreste pluviali avvengono per far spazio ai pascoli di bestiame o per coltivare i mangimi per gli animali d&#39;allevamento.</p>

<p>&nbsp;</p>

<h4>5. Aiutare la lotta alla fame nel mondo</h4>

<p>Mangiare carne non solo fa male agli animali: fa male anche alle persone pi&ugrave; povere. Ci vogliono tonnellate di mangimi e acqua per allevare gli animali da allevamento. Come potremmo ridistribuire&nbsp; quei terreni e e di quelle risorse idriche se non avessimo la necessit&agrave; di alimentare un sistema che da 30kg di grano riesce a produrre un solo kg di carne?</p>

<p>&nbsp;</p>

<h4>4. Migliorare la vostra salute</h4>

<p>Una dieta 100% vegetale vi permette di recuperare tutti i nutrienti che il vostro corpo ha bisogno per vivere sano. Proteine, fibre, minerali, vitamine etc&hellip; senza tutto quello che invece nella carne fa male: dite addio a colesterolo e grassi animali saturi!</p>

<p><img alt="" class="wp-image-10477" src="/app/uploads/2017/01/animali_difesa_ragazza_mucca___0.jpg" style="width: 850px; " /></p>

<h4>3. Aiutare a rimettere in sesto il pianeta</h4>

<p>L&#39;adozione di una dieta 100% &egrave; il passo pi&ugrave; efficace per passare ad un paradigma produttivo pi&ugrave; rispettoso del pianeta, nonch&eacute; una delle azioni pi&ugrave; impattanti contro il cambiamento climatico.</p>

<p>&nbsp;</p>

<h4>2. Combattere il maltrattamento di animali innocenti ed indifesi</h4>

<p>Semplicemente non esiste modo pi&ugrave; semplice per aiutare gli animali e prevenire le loro sofferenze che la scelta di cibi a base vegetale. Lasciate i prodotti a base di carne, di pesce, di uova e di latticini fuori dal vostro carrello ed avrete lasciato fuori dalla vostra spesa anche una quantit&agrave; enorme di sofferenza e crudelt&agrave;.</p>

<p><img alt="" class="wp-image-10481" src="/app/uploads/2017/01/animali_difesa_bambini_pulcini.jpg" style="width: 850px; " /></p>

<h4>1. Aumentare il bene ed il bello nel mondo</h4>

<p>Scegliete la compassione al posto della sofferenza ed avrete fatto il primo passo per creare un mondo migliore.</p>

<p>&nbsp;</p>

