<div class="center"><img class="wp-image-1289" src="/app/uploads/2015/05/389069_4248425133563_218977623_n.jpg" /></div>
<p class="p1">Dopo giorni di indagini eravamo esausti. Quella mattina ci siamo trovati circondati da alcuni allevatori di animali da pelliccia che avevano cominciato ad avere sospetti sul motivo della nostra presenza in quel luogo. Ci trovavamo nel bel mezzo di uno dei mercati di pelliccia più noti della Cina, il mercato di Shangcun.</p>
<p class="p2">Non sapevano che stavamo filmando tutto con la nostra piccola videocamera nascosta. E' stato questo il modo in cui siamo riusciti a documentare il crudele commercio di pellicce in Cina.</p>
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/frPNUCbRco0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<p class="p1">Quando abbiamo deciso di andare in Cina lo abbiamo fatto con un unico obiettivo: filmare l'uccisione e la scuoiatura dei procioni e delle volpi allevati per le pellicce. Le condizioni di vita che questi animali devono sopportare sono terrificanti e volevamo portare la nostra testimonianza a tutto il mondo.</p>
<p class="p2">Nel corso delle indagini abbiamo visto procioni (un tipo di canide) e volpi intrappolati ognuno all'interno di una piccola gabbia. Le gabbie erano appena più grandi dei loro stessi corpi. Questi poveri animali vivevano tra le proprie feci. Non avevano abbastanza cibo né acqua.</p>
<p class="p2">La loro morte è terribile quanto la loro vita. Abbiamo filmato una femmina di cane procione mentre veniva uccisa mediante elettrocuzione con delle barre di metallo collegate a una batteria per auto. Paralizzata dalla scossa elettrica, venne scuoiata viva. Il mio cuore batteva all'impazzata per il dolore che provavo alla vista del trattamento che stava subendo quella bella e innocente creatura. Ma l'unica cosa che potevo fare era continuare a filmare per portare la mia testimonianza al mondo. Sperando che milioni di persone come voi mi avrebbero aiutato a diffonderla.</p>

<blockquote>
<p class="p2"><em>Alcune di quelle immagini ancora mi perseguitano. Dopo aver assistito a tanta crudeltà sono stato malissimo per giorni. Ma sapevo che qualcosa si poteva fare. Qualcosa doveva essere fatto.</em></p>
</blockquote>
La pelliccia di questi animali viene spedita in tutto il mondo. Guarnisce cappotti e altri indumenti in molti negozi. Marchi internazionali come Fendi, Alice e Olivia, o Adrienne Landau utilizzano pelliccia di cane procione proveniente dalla Cina. Ma non vogliono che tu lo sappia così la etichettano come “procione asiatico”.

La nostra investigazione <a href="http://www.dailymail.co.uk/news/article-2867219/Inside-Chinese-fur-farms-breed-raccoon-dogs-tiny-cages-skin-alive-make-luxury-coats-sold-West.html">è stata pubblicata sul quotidiano inglese Daily Mail</a> e in Italia su <a href="http://www.video.mediaset.it/video/striscialanotizia/servizio/496331/cani-da-pelliccia.html">Striscia La Notizia</a> e il <a href="https://www.corriere.it/animali/14_novembre_17/scuoiati-vivi-la-pelliccia-cosi-cina-si-uccidono-cani-procione-d7b03d26-6e8d-11e4-8e96-e05d8d48a732.shtml">Corriere della Sera</a>, grazie a ciò milioni di persone sono venute a conoscenza della crudeltà insita nel commercio di pellicce.
<p class="p2">Grazie al supporto di persone come voi abbiamo già conseguito importantissimi risultati in Cina. <a href="https://www.senzavoce.org/vittoria-chiusi-un-macello-e-33-rivenditori-di-carne-di-cane-e-gatto/">Siamo riusciti a far chiudere 33 mercati che vendevano cani e gatti destinati a diventare carne e pellicce e un macello</a>. <a href="https://www.senzavoce.org/blog/vita/">Abbiamo anche salvato Vita</a>, una cagnetta destinata alla morte, in uno di questi macelli.</p>
<p class="p2"><strong>Ma molto di più deve essere ancora fatto. </strong><a href="https://animalequality.it/diventa-sostenitore">Diventa sostenitore mensile di Animal Equality</a>, aiutaci a continuare a conseguire vittorie per gli animali in Cina e in tutto il mondo.</p>
<p class="p2"><strong>Con il tuo aiuto lavoreremo per porre fine al crudele commercio di pellicce e a salvare tanti animali mediante:</strong></p>

<ul>
 	<li class="p2"><em>L'esposizione della verità attraverso indagini innovative nel settore della pelliccia.</em></li>
 	<li class="p2"><em>​L'educazione dell'opinione pubblica attraverso volantinaggio, servizi sui media e campagne di informazione sui social networks.</em></li>
 	<li class="li1"><em>Il dialogo con le aziende che continuano ad usare le pellicce.</em><a href="https://animalequality.it/diventa-sostenitore"><img class="wp-image-10624" src="/app/uploads/2015/05/banner_sostegno1.jpg" alt="" /></a></li>
</ul>
<p class="p2"></p>
