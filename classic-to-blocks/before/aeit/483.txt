<p>Siamo a un passo dal fare la storia!&nbsp;Stiamo presentando al <a href="http://www.sundance.org/festivals/sundance-film-festival">Sundance Film Festival</a> un cortometraggio davvero innovativo, che sfrutta le<strong> tecnologie della realt&agrave; virtuale per mostrare la vita degli animali dagli&nbsp;allevamenti ai&nbsp;macelli</strong>. L&rsquo;abbiamo prodotto in collaborazione con <a href="http://conditionone.com/">Condition One</a>, una societ&agrave; specializzata in tecnologia.</p>

<p><img alt="" class="wp-image-10606" src="/app/uploads/2016/01/EQ_1464.jpg" style="width: 800px; " /></p>

<p>Il nostro direttore delle investigazioni,<strong> Jose Valle</strong>,&nbsp;si &egrave; infiltrato nei luoghi dell&#39;industria che maltratta gli animali per offrire al pubblico un&rsquo;esperienza rivoluzionaria. Come tante altre volte, <strong>ha vissuto l&#39;orrore della crudelt&agrave;&nbsp;sugli animali in prima persona&nbsp;per poter accompagnare gli spettatori in un viaggio che potrebbe&nbsp;cambiare&nbsp;la loro vita</strong>.</p>

<p><img alt="" class="wp-image-10608" src="/app/uploads/2016/01/EQ_15211.jpg" style="width: 800px; " /></p>

<p>La tecnologia della realt&agrave; immersiva sta <strong>scuotendo il mondo dell&#39;audiovisivo</strong>.&nbsp;La partecipazione sensoriale&nbsp;lascia senza fiato ed <strong>alimenta l&#39;empatia nei confronti degli animali</strong>. &Egrave; praticamente impossibile non rifletterci sopra. <strong>Tutti sentono l&#39;urgenza di raccontare la loro esperienza</strong>.</p>

<p><img alt="" class="wp-image-10607" src="/app/uploads/2016/01/EQ_1477.jpg" style="width: 800px; " /></p>

<p><strong>Gli animali</strong>,&nbsp;segregati nei capannoni delle periferie, <strong>sono sempre pi&ugrave; vicini, visibili</strong>, nonostante l&#39;industria faccia di tutto per tenerceli nascosti. <strong>Grazie alla tecnologia della realt&agrave; immersiva, siamo noi ad andare da loro. A stare insieme a loro.</strong></p>

