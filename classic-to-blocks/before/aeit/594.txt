<div class="center"><img class="wp-image-1762" src="/app/uploads/2016/10/disastro_ambientale_fb.jpg" /></div>
Spesso ci dimentichiamo di come i piccoli cambiamenti collettivi alle nostre routine possano avere effetti di vasta portata.

È stato calcolato che se solo la popolazione degli Stati Uniti decidesse di rinunciare a carne e derivati per un solo giorno alla settimana, in un anno, <a href="https://shrinkthatfootprint.com/food-carbon-footprint-diet/">risparmieremmo alla nostra atmosfera l'inquinamento prodotto da 7.6 milioni di automobili</a>.

Benché nel dibattito sui gas serra il monossido di carbonio catalizzi sempre tutta l'attenzione, <a href="http://www.unep.org/pdf/unep-geas_oct_2012.pdf">esso rappresenta solo il 9% delle emissioni del settore agricolo</a>. Il <strong>metano</strong> (CH4), è un gas serra <strong>25 volte più pericoloso</strong>, occupa una fetta che va dal <strong>35% a 45%</strong> mentre l'<strong>ossido nitroso</strong> (N2O), <strong>300 volte più pericoloso</strong>, oscilla ogni anno fra il <strong>45% ed il 55%</strong>.

<img class="wp-image-1761" style="width: 850px;" src="/app/uploads/2016/10/manzo_carne_2.jpg" alt="" />
<h4>Cosa c'entra il metano con gli allevamenti?</h4>
I ruminanti (bovini ed ovini) producono metano come effetto secondario dei propri <strong>processi digestivi</strong> e lo <strong>rilasciano in atmosfera</strong> proprio con questi processi digestivi o con le <strong>esalazioni derivanti dal loro letame in decomposizione</strong>. Se considerate che solo gli animali allevati negli Stati Uniti producono <strong>500 milioni di tonnellate di letame ogni anno</strong>, ossia <a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.164.5187&amp;rep=rep1&amp;type=pdf">3 volte la quantità di rifiuti prodotti dalla popolazione statunitense nello stesso arco di tempo</a>, inizierete a farvi un'idea di <a href="https://www.ipcc-nggip.iges.or.jp/public/gp/bgp/4_2_CH4_and_N2O_Livestock_Manure.pdf">cosa significhi per l'ambiente il nostro insaziabile appetito per la carne</a>.

Facciamo presente che stiamo parlando di <strong>un unico gas nocivo</strong> e solamente del suo effetto in atmosfera. Ma il letame che si decompone, parlando di quantità così grosse, va a contaminare anche la nostra <strong>falda acquifera</strong>.

E poi esistono anche altre sostanze, come ad esempio il <strong>protossido di azoto</strong>.

<img class="wp-image-1748" src="/app/uploads/2016/10/manzo_carne.jpg" alt="" />
<h4>Cosa c'entra il protossido di azoto con gli allevamenti?</h4>
Il <a href="http://www.unep.org/pdf/unep-geas_oct_2012.pdf">protossido di azoto è anch'esso un prodotto secondario della decomposizione del letame dei ruminanti</a>, ma viene immesso in atmosfera in quantità maggiore con la <strong>produzione</strong> e l'<strong>applicazione </strong>di <strong>fertilizzanti azotati</strong>, così come durante il deterioramento di un terreno ricco di carbonio,<strong> appena disboscato per creare spazio ad uso agricolo</strong>.

Continuiamo con l'espio statunitense per farci un idea: <a href="http://static.ewg.org/reports/2011/meateaters/pdf/report_ewg_meat_eaters_guide_to_health_and_climate_2011.pdf?_ga=1.109160837.1881602438.1473890083">60 milioni di ettari di terre coltivate, 167 milioni di kg di pesticidi e 19 miliardi di euro</a> vengono spesi ogni anno per nutrire la popolazione di ruminanti di cui parlavamo prima.

Oppure, visto dal punto di vista del consumo del suolo:<a href="http://www.europarl.europa.eu/climatechange/doc/FAO%20report%20executive%20summary.pdf"> secondo la FAO, gli <strong>allevamenti equivalgono al 26% di tutte terre emerse</strong></a>, ghiacciai inclusi. A questo dato possiamo aggiungere che l'area totale dei terreni in cui si coltiva il cibo per gli allevamenti equivale al<strong> 33% di tutta la terra arabile del pianeta</strong>. Riassumendo, <a href="http://www.europarl.europa.eu/climatechange/doc/FAO%20report%20executive%20summary.pdf"><strong>sempre secondo la FAO</strong></a> l'allevamento occupa il 70% di tutti i terreni agricoli presenti nel mondo ed il 30% della superficie del pianeta. E stiamo continuando ad abbattere foreste per far spazio ad allevamenti e terreni per mangimi.

<img class="wp-image-1542" src="/app/uploads/2016/07/deforestazione.jpg" alt="" />

A questo punto, probabilmente starai iniziando a realizzare come tutte quelle costine, quegli hamburger o quei formaggi che finiscono ogni giorno nel nostri piatti non facciano poi così bene all'ambiente.

Ma non prendetevela con gli allevamenti, con i macelli, i supermercati o con i ristoranti. Queste attività commerciali rispondono ad una domanda. E quella domanda… beh la creiamo noi, noi che divoriamo carne a prezzi mai visti. Dal 1971 al 2010, a fronte di una crescita della popolazione globale del 81%, <strong>la produzione di carne mondiale è triplicata</strong> raggiungendo una cifra di circa 670 miliardi di euro.

<img class="wp-image-1751" src="/app/uploads/2016/10/carne_obesita_1_1.jpg" alt="" />
<h4>Quindi, quali alimenti sono più dannosi per l'ambiente?</h4>
Esiste un'analisi dettagliata della <em><strong>Carbon Footprint</strong></em> totale delle<strong> fonti di proteine più comunemente diffuse</strong>, siano esse carne, formaggi o vegetali.

Per <a href="https://shrinkthatfootprint.com/food-carbon-footprint-diet/">Carbon Footprint</a> si intende l'<em>emissione di gas nocivi attribuibile ad un prodotto, un'organizzazione o un individuo</em> e serve a misurare l'<strong>impatto</strong> che tali emissioni hanno sui cambiamenti climatici legati all'attività dell'uomo.

Secondo questa lista, <strong>agnello, manzo</strong> e <strong>formaggio</strong> occupano in questo ordine le tre posizioni più inquinanti. La lista procede poi includendo <strong>carne di maiale, salmone d'allevamento, tacchino, pollo, tonno in scatola, uova</strong> per poi terminare con patate, riso, burro di arachidi, noci, broccoli, tofu, fagioli secchi, latte e le lenticchie.

<img class="wp-image-1759" style="width: 850px;" src="/app/uploads/2016/10/manzo_carne_0.jpg" alt="" />

È una lista che ingloba tutti i momenti della vita di un cibo, trasporti inclusi. Secondo questa analisi ogni kg di cibo consumato produce circa <a href="http://static.ewg.org/reports/2011/meateaters/pdf/report_ewg_meat_eaters_guide_to_health_and_climate_2011.pdf?_ga=1.109160837.1881602438.1473890083"><strong>86.4 kg di CO2 se si tratta di carne di agnello</strong></a>, 59.6 kg se si tratta di manzo, 29.7 kg se si tratta di formaggio. I broccoli generano circa 1.9 kg di CO2 contro le lenticchie che ne producono solo 0.89. Va inoltre notato che per le fonti vegetali, la maggior parte delle emissioni nocive è legata al trasporto, alla cottura ed allo smaltimento dei rifiuti.

<img class="wp-image-1543" src="/app/uploads/2016/07/inaridimento.jpg" alt="" />
<h4>E l'impatto sulla salute?</h4>
Va prima di tutto fatta una precisazione: <strong>l'impatto ambientale ha già di per se' effetti sulla nostra salute</strong>. E questo anche se siamo lontani centinaia di km dalle fonti di inquinamento: acque ed aria non conoscono confini,<strong> il problema ormai è globale</strong>.

Ma se l'impatto ambientale delle nostre diete non fosse sufficiente a farci ripensare le nostre routine alimentari, forse potrebbe farcela l'<a href="https://pubmed.ncbi.nlm.nih.gov/19808637/"><strong>impatto del cibo sulla nostra salute</strong></a>.

L'<a href="https://pubmed.ncbi.nlm.nih.gov/18469286/">Istituto Nazionale per il Cancro statunitense ha studiato ben 500.000 cittadini americani nel 2009</a> ed è arrivato a conclusioni pesanti: nella parte di questo campione che si è cibata maggiormente di carne rossa si riscontra un +<strong>20% di probabilità di morte per cancro</strong> e + <strong>27% per infarto</strong>.

Per le donne, i risultati sono ancora meno confortanti: fra le consumatrici di alte quantità di carne rossa, il <strong>rischio di morte per malattie cardiovascolari è più alto del 50%</strong>.

<img class="wp-image-1760" style="width: 850px;" src="/app/uploads/2016/10/vegan_health.jpg" alt="" />
<h4>Ma abbandonare la carne è difficile!</h4>
<a href="https://it.loveveg.com"><strong>Diventare vegetariani, o semplicemente diminuire il nostro consumo di carne, non è così drastico come si può pensare.</strong></a>

L'analisi dei dati dell'<a href="https://shrinkthatfootprint.com/food-carbon-footprint-diet/">Economic Research Service del Dipartimento dell'Agricoltura degli Stati Uniti</a> (ERS / USDA) ha evidenziato come tutte le nostre <strong>diete varino fra di loro per solo il 40% del loro totale</strong>, indipendentemente che siano consumatori di carne o vegan.

Benché le emissioni di CO2 delle diete vegetariane siano <strong>inferiori di un terzo rispetto</strong> a quelle di un occidentale medio e quasi la metà di un <strong>"amante della carne"</strong>, la stessa analisi dell'ERS ha contemporaneamente dimostrato che solo la sostituizione della carne di manzo con quella di pollo abbatterebbe di un quarto le emissioni di CO2.
<em>[NDR: Animal Equality incoraggia il lettore a lasciare fuori dalla propria dieta tutti i tipi di carne.]</em>

Gli effetti di un piccolo cambiamento cominciano ad avere ancora più senso se si considera come <strong>i prodotti di origine animale rappresentino solo il 25% del nostro apporto calorico</strong> ed allo stesso tempo costituiscano <strong>il 60% delle emissioni legate alle nostre routine alimentari</strong>.

<img class="wp-image-1757" src="/app/uploads/2016/10/VEGAN.jpg" alt="" />
<h4>Ok, ma le proteine?</h4>
A coloro i quali si stanno preoccupando di non riuscire a reperire abbastanza proteine, suggeriamo l'ultimo studio del Centro Nazionale per l'Informazione Biotecnologica svolto su un campione di uomini fra i 17 ed i 70 anni e con un apporto di proteine consigliato di 56g giornalieri.

Questo studio ha dimostrato che la maggior parte di questi individui consumi normalmente il quasi<strong> il doppio delle proteine consigliate</strong>, in quote giornaliere fra gli 88.3 ed i 109.2 grammi giornalieri.

Tendo presente questi risultati, consigliamo anche quelli di un altro studio: <strong>solo l'1% dei bambini ed il 4% degli adulti consuma la quantità consigliata di frutta e verdura</strong>.

<img class="wp-image-1756" src="/app/uploads/2016/10/proteine_vegetali.jpg" alt="" />
<h4>Ed oltre a non mangiare carne ne derivati, cosa posso fare per aiutare il pianeta?</h4>
Le risposte sono sotto il nostro naso da secoli, e sicuramente ne hai sentito parlare anche tu.

Acquista prodotti locali: ridurrai le emissioni in percentuali che vanno dal 20% al 25%.

Se possibile concentrati su prodotti provenienti da agricolture biologiche: questo tipo di coltivazioni hanno un impatto ambientale molto più ridotto rispetto alle agricolture convenzionali poiché alle aziende biologiche è richiesto di utilizzare fertilizzanti naturali e di evitare i pesticidi chimici.

<img class="wp-image-1758" src="/app/uploads/2016/10/VEGAN2.jpg" alt="" />

In conclusione, se vogliamo fare sul serio la differenza non dobbiamo sconvolgerci la vita: <a href="https://it.loveveg.com/"><strong>basta lasciare la carne ed i derivati fuori dalla nostra dieta</strong></a>.

Se una famiglia media di 4 persone riuscisse a farlo anche solo per un giorno alla settimana, in un anno, <strong>risparmierebbe al pianeta l'equivalente di 3 mesi di inquinamento della propria auto</strong>.

Ragioniamoci su.
