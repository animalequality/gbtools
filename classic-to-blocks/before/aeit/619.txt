Per moltissimo tempo le persone che amano gli animali sono rimaste all’oscuro di qualcosa che si scontra in modo fondamentale con i loro valori: il maltrattamento verso gli animali da parte dell’industria della carne.

Non si tratta di un caso, né del fatto che cercassero di distogliere lo sguardo dalla verità. Niente di tutto ciò. La verità è che le industrie della carne, delle uova e dei latticini <strong>stavano nascondendo quello che succede all’interno degli allevamenti e dei mattatoi.&nbsp;</strong>

E continuano a farlo. Però adesso il movimento per la protezione degli animali ha concentrato i suoi sforzi nell’esporre gli abusi che vengono commessi contro gli animali che vengono allevati per scopo alimentare.

<img class="wp-image-1866" style="width: 850px;" src="/app/uploads/2016/12/OCCHI_19_MAIALINO.jpg" alt="">

Le investigazioni condotte dalle organizzazioni per la protezione degli animali di tutto il mondo hanno mostrato a milioni di consumatori la vita degli animali negli allevamenti e la loro morte nei mattatoi.
<h4>“È&nbsp;possibile che in futuro si parli delle&nbsp;persone dietro a queste investigazioni&nbsp;come di coloro che grazie al loro amore per gli animali, hanno cambiato il mondo”.</h4>
Animal Equality è specializzata in questo tipo di investigazioni che aprono gli occhi e rendono visibile una realtà sconosciuta. Questo tipo di lavoro di investigazione ha dato<strong> una nuova dimensione al concetto di maltrattamento degli animali</strong>.

Adesso il consumatore è più cosciente di ciò che <strong><a href="https://animalequality.it/blog/quello-che-nessuno-ti-vuole-raccontare/">milioni di animali devono subire</a></strong> per quella vaschetta di carne, per quel pezzo di formaggio, per quelle uova. <strong><a href="http://www.scopriltuocibo.com" data-wplink-edit="true">E saperlo non gli fa piacere</a>.</strong>

<img class="wp-image-1863" style="width: 850px;" src="/app/uploads/2016/12/OCCHI_17_ANATROCCOLI.jpg" alt="">

La legge consente a queste industrie di realizzare <strong><a href="https://animalequality.it/blog/quello-che-nessuno-ti-vuole-raccontare/">pratiche aberranti</a>&nbsp;</strong>che sono ormai comuni&nbsp;in ogni allevamento ed in ogni macello.

Un esempio di questo sono le <strong>piccolissime gabbie</strong> in cui vengono stipate <a href="https://animalequality.it/news/2016/10/19/gli-allevamenti-di-galline-ovaiole-da-dentro-una-nuova-investigazione-di-animal-equality/">le galline ovaiole</a>&nbsp;o le scrofe destinate alla riproduzione. Nei mattatoi molti animali arrivano al loro ultimo momento ancora completamente coscienti, perfino nel momento in cui&nbsp;vengono sgozzati.

<img class="wp-image-1907" style="width: 850px;" src="/app/uploads/2016/12/allevamento_galline_uova.png" alt="">

Per questi motivi, le persone che amano gli animali si trovano di fronte ad una&nbsp;<strong>sfida</strong>. Una sfida che è destinata a <strong>scuotere dalle fondamenta queste industrie multimilionarie,&nbsp;</strong>a partire dai consumatori che decidono di smettere di comprare i loro prodotti.

La logica è semplicissima e si può <strong>mettere in pratica nella vita quotidiana,</strong> giorno dopo giorno: ridurre il consumo di carne, introdurre sempre più alimenti di origine vegetale ed alternative alla carne nella nostra alimentazione. E per molti non finisce qui.
Grazie alla quantità di prodotti attualmente disponibile, milioni di persone&nbsp;in tutto il mondo <strong><a href="https://it.loveveg.com/">hanno sostituito completamente la carne con opzioni più compassionevoli</a></strong>.

Gli studi ci rivelano che nei paesi occidentali il consumo di carne si sta riducendo allo stesso ritmo con il quale sta esponenzialmente&nbsp;aumentando il consumo di alimenti a base vegetale.

<img class="wp-image-1908" style="width: 850px;" src="/app/uploads/2016/12/2_sfida_animalismo_21secolo.jpg" alt="">

Abbiamo una sfida importante davanti a noi ed in molti l‘hanno già accettata.. iniziando a&nbsp;fare&nbsp;la storia. E’ possibile che in futuro si parli di queste persone come di coloro che, grazie al loro amore per gli animali, hanno cambiato il mondo.
<h4>Gli animali hanno bisogno che tu ti unisca a questo gruppo di persone che ambiscono ad un futuro migliore.</h4>
