<p>Animal Equality rende pubblica un&rsquo;investigazione sotto copertura all&rsquo;interno di una delle circa 200 strutture italiane in cui &egrave; permessa la macellazione rituale. Gli investigatori hanno avuto l&rsquo;opportunit&agrave; di riprendere cosa &egrave; accaduto durante le fasi di carico e scarico di centinaia di animali, esponendo successivamente il terribile trattamento riservato durante la macellazione rituale in una delle circa 200 strutture italiane in cui &egrave; permessa. <strong>Le immagini raccolte denunciano la violenza inferta nelle fasi del rito &lsquo;halal&rsquo; a centinaia di montoni, durante la Festa del Sacrificio o di Aid al Ahda, mostrando il cruento trattamento rivolto agli animali, continuamente percossi, sbattuti contro delle grate, legati e costretti a vedere i propri simili barbaramente uccisi.</strong></p>
<div>
	<img alt="Macellazione rituale Halal - macelli.info/halal" class="wp-image-1260" src="/app/uploads/2014/10/halal_rituale.jpg" style="width: 378px; border-width: 0px; border-style: solid; margin: 5px 10px; float: right;" />I responsabili delle strutture come quella investigata acquistano gli animali dagli allevamenti, li contrassegnano con un numero progressivo messo sull&rsquo;orecchio che corrisponde ad un talloncino rilasciato all&rsquo;acquirente di fede musulmana, che si presenter&agrave; poi il giorno della macellazione per ritirare la sua carcassa. Questo tipo di prassi avviene in Italia da alcuni anni, rispondendo all&rsquo;esigenza di evitare spargimenti di sangue di fatto illegali, poich&eacute; tradizionalmente la Festa del Sacrificio viene celebrata collettivamente con l&rsquo;uccisione diretta dell&rsquo;animale da parte dei fedeli e il successivo consumo della carne. Pertanto la violenza che si consuma ogni anno, anche in Italia, in un&rsquo;occasione del genere, viene &lsquo;istituzionalizzata&rsquo; e permessa all&rsquo;interno dei circa 200 macelli che hanno ottenuto la deroga per effettuare macellazioni rituali.</div>
<div>
	&nbsp;</div>
<div>
	<strong>Ci&ograve; che il nostro team investigativo ha raccolto supera ogni immaginazione e ci porta all&rsquo;interno di una realt&agrave; mai documentata prima in Italia, dove gli animali, in questo caso centinaia di montoni, vengono uccisi barbaramente dopo ripetute violenze</strong> e dove &egrave; stato riscontrato quanto segue:</div>
<div>
	&nbsp;</div>
<div>
	&bull; <em>Nel momento in cui &egrave; effettuato il carico dai recinti dove sono radunati in attesa del trasporto al macello, gli animali vengono terrorizzati e colpiti violentemente.</em></div>
<div>
	&nbsp;</div>
<div>
	&bull; <em>Per forzarli a salire sul camion, gli animali sono calpestati e presi a calci, in ripetuti casi vengono maneggiati con assurda violenza.</em></div>
<div>
	&nbsp;</div>
<div>
	&bull; <em>I montoni vengono lasciati in un cortile completamente legati per diversi minuti, in preda al terrore e forzati a vedere i simili che vengono sgozzati.</em></div>
<div>
	&nbsp;</div>
<div>
	&bull; <em>Gli operatori del macello gestiscono in maniera difficoltosa gli animali che nel frattempo, terrorizzati, si dimenano in mezzo al sangue e ai resti dei compagni.</em></div>
<div>
	&nbsp;</div>
<div>
	&bull; <em>Diversi montoni aspirano sangue nei loro polmoni durante la macellazione.&nbsp;</em></div>
<div>
	&nbsp;</div>
<div>
	&bull; <em>Risulta evidente dai filmati raccolti che il taglio della gola e dell&#39;esofago lascia gli animali in agonia e dolore per diversi minuti, con una sofferenza inaudita e percepibile a chiunque veda le immagini.</em></div>
<div>
	&nbsp;</div>
<div>
	&bull; <em>Quando vengono effettuate le operazioni della separazione delle pelli realizzata con un compressore, gli animali sono ancora vivi. Il tessuto connettivo che ne previene la separazione &egrave; molto innervato, quindi una pressione del genere per separarlo mentre si &egrave; ancora vivi crea un intenso e insopportabile dolore fisico. &nbsp;</em></div>
<div>
	&nbsp;</div>
<div>
	<strong>Paura, angoscia, sofferenza, dolore: quello che prova un animale durante la macellazione va oltre ogni immaginazione. L&rsquo;investigazione di Animal Equality dimostra quanta brutalit&agrave; si cela dietro l&rsquo;uccisione di ogni singolo individuo. Le immagini rese pubbliche mostrano come l&rsquo;assenza di stordimento possa amplificare il dolore provato dall&rsquo;animale e prolungarne, anche di molto, l&rsquo;agonia.</strong> Questo &egrave; permesso da una deroga alle cosiddette leggi sul benessere animale, che invece prevederebbero, appunto, lo stordimento prima della macellazione vera e propria. L&rsquo;Unione Europea lascia facolt&agrave; ad ogni singolo paese membro di decidere se eliminare o meno la possibilit&agrave; di derogare a quest&rsquo;obbligo. Cos&igrave; alcuni Stati, come Polonia e Danimarca, hanno gi&agrave; scelto di vietare la macellazione rituale ed &egrave; possibile che ci&ograve; avvenga anche in Italia. Per quanto un risultato del genere non possa considerarsi neanche lontanamente risolutivo, significherebbe evitare che i piccoli passi in avanti fatti nel percorso per il riconoscimento dei diritti animali siano vanificati da altrettanti passi indietro. <strong>Per questo ti chiediamo di attivarti e di fare la tua parte, diffondendo la campagna &ldquo;Fermiamo la crudelt&agrave; rituale&rdquo; e firmando la petizione!</strong></div>

