<div>
	Abbiamo una bellissima notizia da condividere con voi! La storia di Vita, la cagnetta che Animal Equality ha salvato da un macello per cani in Cina, verr&agrave; raccontata ad oltre 700.000 persone.&nbsp;</div>
<div>
	<br />
	<img alt="Jose Valle &amp; Vita" class="wp-image-1237" src="/app/uploads/2014/03/12104789225_35b3bc5caf_b.jpg" style="width: 326px; border-width: 0px; border-style: solid; margin: 10px 5px; float: right;" />Fabrizia Angelini, portavoce di Animal Equality in Italia, apparir&agrave; all&#39;interno del programma <strong><a href="http://www.cronacheanimali.rai.it/" target="_blank" rel="noopener noreferrer">Cronache Animali</a></strong> dove parler&agrave; del salvataggio di Vita e della terribile storia di cani, gatti e molti altri animali che stanno soffrendo in Cina. Solo per il commercio della carne di cane sono pi&ugrave; di 10 milioni gli animali uccisi.</div>
<div>
	&nbsp;</div>
<div>
	Questo nuovo traguardo &egrave; stato possibile grazie al sostegno delle oltre <strong>320.000</strong> persone che hanno firmato la petizione per porre fine al commercio di carne di cane in Cina. Tutto questo ci ha permesso di ottenere visibilit&agrave; su diversi e importanti canali mediatici. Grazie alle persone che hanno firmato, alla copertura mediatica e alla collaborazione con diversi gruppi cinesi per i diritti animali, abbiamo ottenuto anche la <strong>chiusura di ben 33 rivenditori ed un macello</strong>.</div>
<div>
	&nbsp;</div>
<div>
	Fabrizia Angelini parler&agrave; di questo ed altro ancora, all&#39;interno del programma Cronache Animali. Verr&agrave; mostrato il video del salvataggio di Vita e diverse altre significative immagini.&nbsp;Vi invitiamo a guardarlo domani (<strong>sabato 15 marzo</strong>), a partire dalle&nbsp;<strong>10.40</strong> su <strong>RAIDUE</strong>.</div>

