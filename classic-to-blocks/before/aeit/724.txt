<div class="center"><img class="wp-image-2285" src="/app/uploads/2017/12/2_appeso_2_1.jpg" /></div>
<p>Il Comando carabinieri per la salute pubblica, comunemente noto come Nas &egrave; un distaccamento dell&rsquo;arma dei Carabinieri posto alle dipendenze funzionali del Ministero della salute.</p>

<p>Il suo compito &egrave; vigilare sulla disciplina igienica della produzione, commercializzazione e vendita delle sostanze alimentari e delle bevande, a tutela della salute pubblica.</p>

<p>Negli ultimi 9 mesi i NAS hanno effettuato diversi controlli su allevamenti e macelli, per un totale di <b>2148 strutture</b> e quello che emerge dai verbali &egrave; un quadro preoccupante:</p>

<h4><b>pi&ugrave; di una struttura su tre &egrave; risultata non conforme alla normativa vigente</b>.&nbsp;</h4>


<p><img alt="" class="wp-image-2284" src="/app/uploads/2017/12/2_appeso_2_0.jpg" style="width: 850px; " /></p>

<p>&nbsp;</p>

<h4>Ecco i numeri nel dettaglio:</h4>

<p>&nbsp;</p>

<strong>763 strutture su 2148 sono risultate non conformi (35.52%)</strong>

<h4>530 persone sono state segnalate alle Autorit&agrave; amministrative</h4>

<h4>156 persone sono state segnalate alle Autorit&agrave; giudiziarie</h4>

<h4>392 sanzioni penali evidenziate</h4>

<h4>1.017 sanzioni amministrative evidenziate per un importo complessivo di 1.267.600 euro</h4>

<h4>887.826 animali da reddito sono stati sequestrati</h4>

<h4>108 le strutture chiuse o sequestrate per un valore complessivo di 67.344.000 Euro</h4>

<p>&nbsp;</p>

<p>Sempre secondo il sito del Ministero della salute, gli illeciti pi&ugrave; ricorrenti sono stati: <strong>carenze igienico-strutturali</strong>,&nbsp;inosservanza corretta identificazione degli animali, <strong>inosservanza delle norme sull&rsquo;uso dei farmaci veterinari</strong>,&nbsp;irregolarit&agrave; dei criteri di protezione negli allevamenti e nei trasporti, inosservanza delle norme sulla produzione, inosservanza delle norme riguardanti l&rsquo;igiene dei mangimi.</p>

<p>Animal Equality si augura che questi controlli diventino una routine, che si inaspriscano le pene per chi maltratta gli animali all&rsquo;interno delle strutture dislocate sul nostro territorio e che allo stesso tempo migliorino gli standard minimi garantiti dalla legge italiana.<br />
<br />
&nbsp;</p>

<p>FONTE: dati dal <a href="http://www.salute.gov.it/portale/news/p3_2_1_2_1.jsp?lingua=italiano&amp;menu=notizie&amp;p=nas&amp;id=701">sito web del Ministero della Salute</a></p>

