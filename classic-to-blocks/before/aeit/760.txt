<p>Dopo tante investigazioni, la vita dei polli italiani non &egrave; ancora cambiata. Oggi rendiamo pubblici alcuni filmati che mostrano le condizioni di vita di questi animali, a partire dalla terribile fase dell&rsquo;incubatoio.&nbsp;</p>

<p>&nbsp;</p>

<p>In questi luoghi, milioni di pulcini vengono fatti nascere con una sola destinazione, il macello. Ed &egrave; per questo che durante l&rsquo;intero processo vengono maneggiati come fossero oggetti, lanciati sui nastri trasportatori senza alcuna cura e, in alcuni casi, lasciati morire sul pavimento dell&rsquo;incubatoio.&nbsp;</p>

<p>&nbsp;</p>

<center>
<p><iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="405" src="https://www.youtube.com/watch?v=KrY3d_rCFV8" width="720"></iframe></p>
</center>

<p>&nbsp;</p>

<p>Ma anche in allevamento i giovani polli sono sottoposti a maltrattamenti e violenze. Qui, gli animali malati o feriti vengono semplicemente abbandonati a morire di fame e sete, ma anche quelli che sopravvivono non sono destinati a una sorte migliore. Prima del trasporto infatti, vengono sottoposti alla fase di cattura, spesso realizzata attraverso macchine che sono in grado di prendere centinaia di polli in pochissimo tempo. In questa fase, gli operatori maneggiano gli animali che riescono a sfuggire alla macchina con violenza, causando immensi dolori alle zampe e alle ali, mentre in alcuni casi i polli vengono addirittura presi a calci e lanciati nella macchina per la cattura.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-2440" src="/app/uploads/2018/06/39769441450_36d8084955_z.jpg" style="width: 840px; " /></p>

<p>&nbsp;</p>

<p>Il loro viaggio purtroppo prosegue verso il macello, destinazione finale nella quale persistono alcune gravi inadempienze, come il mancato stordimento. La legge italiana infatti prevede che prima della macellazione gli animali vengano storditi, ma spesso questo non avviene, perch&eacute; i metodi risultano inefficaci e costringono gli animali a una fine crudele e terribile: la morte pienamente coscienti.&nbsp;</p>

<p>&nbsp;</p>

<p>Le immagini, ottenute grazie al lavoro di vari investigatori sotto copertura, mostrano tutto quello che avviene durante la breve vita di questi delicati animali. I polli infatti vivono in media sei settimane e fin dal primo giorno sono sottoposti a maltrattamenti e incredibili violenze.&nbsp;</p>

<p>&nbsp;</p>

<p><b>Alcuni degli atti documentati nel video:</b></p>

<p>&nbsp;</p>

<ul>
	<li>Operatori che maneggiano senza alcuna cura i pulcini</li>
	<li>Pulcini abbandonati a morire nei corridoi dell&rsquo;incubatoio</li>
	<li>Operatori che maltrattano gli animali colpendoli con violenza</li>
	<li>Giovani polli malati e coperti di ferite &nbsp;</li>
	<li>Operatori che afferrano i polli per le ali e li lanciano nelle macchine per la cattura</li>
	<li>Carcasse abbandonate all&rsquo;interno degli allevamenti</li>
	<li>Metodi di stordimento inefficaci</li>
	<li>Animali macellati pienamente o parzialmente coscienti</li>
</ul>

<p>&nbsp;</p>

<p>Solo in Italia sono pi&ugrave; di 500 milioni i polli che vengono macellati ogni anno, e in Europa si tratta di miliardi di animali che vengono sistematicamente sottoposti a violenze e maltrattamenti.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-2441" src="/app/uploads/2018/06/27688560358_dddd2806ec_z.jpg" style="width: 840px; " /></p>

<p>&nbsp;</p>

<p>Tutto questo accade in un contesto in cui l&rsquo;Italia gioca un ruolo molto rilevante. Il nostro paese infatti &egrave; uno dei maggiori produttori di carne di pollo d&rsquo;Europa, contando quasi il 10% della produzione totale e con un fatturato di settore pari a 4,6 miliardi di euro nel 2016. &nbsp;</p>

<p>&nbsp;</p>

<p>Da tempo per&ograve; stiamo lavorando per ridurre la sofferenza di questi animali.&nbsp;</p>

<p>&nbsp;</p>

<p>&Egrave; per questo che abbiamo lanciato una petizione su<a href="https://campaigns.animalequality.it/pollo-100-italiano/"> polloitaliano.it</a> rivolta a Unaitalia - associazione di categoria che rappresenta i maggiori produttori di carne avicola e uova - proprio per ottenere, fra le varie richieste, un maggior controllo delle condizioni degli animali, la sospensione dei metodi di stordimento inefficaci e la cessazione dell&rsquo;utilizzo di razze a crescita rapida, una delle principali cause delle sofferenze dei polli.&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p class="rtecenter"><a href="https://campaigns.animalequality.it/pollo-100-italiano/"><span style="font-size:28px;"><b>FIRMA ANCHE TU PER METTERE FINE A TUTTO QUESTO</b></span></a></p>

