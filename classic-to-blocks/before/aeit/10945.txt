<p>Animal Equality ha catturato immagini che sono rimaste impresse nella mente di milioni di persone in tutto il mondo. Scattate durante il nostro costante lavoro in difesa degli animali, queste fotografie sono state diffuse in rete a macchia d&#39;olio. Quando le avrete viste, capirete perch&eacute;.</p>

<p><strong>1.</strong> Nel 2009, la nostra squadra investigativa salva, insieme alla madre, due piccoli agnelli da una fattoria nella quale vivono in condizioni terribili. Il loro destino sarebbe stato il macello. Fortunatamente incontrano il nostro team che provvede rapidamente ad assisterli e a metterli in salvo. La famosa foto-reporter Jo-Anne McArthur, autrice del libro &quot;We Animals&quot; e protagonista del pluripremiato documentario &ldquo;The Ghosts In our Machine&rdquo; &egrave; con loro e documenta il salvataggio. Questa immagine fa il giro del mondo.</p>

<p><img alt="" class="wp-image-10664" src="/app/uploads/2016/06/fotoviral1.jpg" style="width: 800px; " /></p>

<p><strong>2.</strong> Nel 2010, due attiviste di Animal Equality irrompono sulla passerella di Cibeles Runaway nel momento in cui sfilano i capi in pelliccia. In due diverse occasioni, durante la trasmissione in diretta, le nostre attiviste lanciano un messaggio a tutto il mondo: Le pelli degli animali appartengono agli animali e non vi &egrave; alcuna necessit&agrave; di indossarli!</p>

<p><img alt="" class="wp-image-1344" src="/app/uploads/2015/10/fotoviral2.jpg" style="width: 800px; " /></p>

<p><strong>3</strong>.&nbsp;Il 10 dicembre &egrave; la Giornata Internazionale per i Diritti degli Animali. Animal Equality la celebra dal 2008 con una dimostrazione nella famosa Plaza del Sol di Madrid. Nell&#39;edizione 2011, 400 attivisti tengono tra le braccia i corpi di animali morti provenienti dagli allevamenti intensivi per denunciare le devastanti condizioni nelle quali sono costretti a vivere. La foto dell&#39;evento, scattata da Jon Amad, diventa virale e viene pubblicata su testate giornalistiche e social di tutto il mondo!</p>

<p><img alt="" class="wp-image-10665" src="/app/uploads/2016/06/fotoviral3.jpg" style="width: 800px; " /></p>

<p><strong>4. </strong>Nel 2012, il nostro team investigativo conduce un&#39;indagine pionieristica sulla produzione di foie gras in Spagna e in Francia. Il maltrattamento degli animali utilizzati per la produzione del foie gras &egrave; uno dei pi&ugrave; terrificanti che abbiamo mai visto, non c&#39;&egrave; da meravigliarsi quindi se la produzione del foie gras &egrave; vietata in 17 Paesi. L&#39;immagine coglie il momento dell&#39;alimentazione &quot;forzata&quot;, nel quale un lungo tubo viene inserito nell&#39;esofago dell&#39;anatra e il cibo pompato direttamente nello stomaco.</p>

<p><img alt="" class="wp-image-10666" src="/app/uploads/2016/06/fotoviral4.jpg" style="width: 800px; " /></p>

<p><strong>5.</strong> Nel 2013 la nostra squadra investigativa si reca in Cina per documentare ed esporre, con una telecamera nascosta, il mercato del consumo di carne di cane. In uno dei macelli, il nostro fotografo coglie l&#39;attimo in cui il macellaio sceglie il prossimo cane che uccider&agrave;. La terribile scena diventa virale ed &egrave; vista da milioni di persone. Per ulteriori informazioni visita: senzavoce.org</p>

<p><img alt="" class="wp-image-10667" src="/app/uploads/2016/06/fotoviral5.jpg" style="width: 800px; " /></p>

<p><strong>6. </strong>Nel 2014, Animal Equality India gira un reportage sul pi&ugrave; sanguinoso rito religioso del mondo, che si celebra in Nepal. Per onorare la dea Gadhimai, durante il rito si uccidono persino mezzo milione di animali. Il nostro team indiano riprende, tramite l&#39;utilizzo di droni, delle scene aeree per catturare la grandezza di questo crudele scenario. L&#39;impotenza di questo vitello fra i corpi mutilati di innumerevoli animali emoziona milioni di persone e questa immagine diventa virale. Grazie al lavoro di Animal Equality e di altre organizzazioni per la protezione degli animali, i responsabili del tempio di Gadhimai hanno annunciato che non sar&agrave; pi&ugrave; consentito utilizzare animali nel corso dei loro rituali.</p>

<p><img alt="" class="wp-image-10668" src="/app/uploads/2016/06/fotoviral6.jpg" style="width: 800px; " /></p>

