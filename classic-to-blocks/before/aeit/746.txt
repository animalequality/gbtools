<p>L&rsquo;industria della carne non vuole che il pubblico conosca quello che avviene all&rsquo;interno di macelli e allevamenti intensivi, ma per fortuna i giudici si stanno esprimendo sempre pi&ugrave; in favore del lavoro fatto dai nostri investigatori e da quelli delle altre organizzazioni. L&rsquo;ultima buona notizia arriva dalla Germania, dove &egrave; stato sancito il diritto di mostrare immagini raccolte all&rsquo;interno di questi luoghi, una decisione importante che difende la libert&agrave; di stampa e dei cittadini ad essere informati correttamente.&nbsp;<br />
&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-2386" src="/app/uploads/2018/04/16033603226_beda2f77b8_z.jpg" style="width: 840px; " /></p>

<p>&nbsp;</p>

<p>Tutto ha inizio con la denuncia da parte di <i>F&uuml;rstenhof GmbH</i> - una delle aziende di uova biologiche pi&ugrave; importanti della Germania - contro la TV tedesca <i>MDR</i>, &ldquo;colpevole&rdquo; di aver pubblicato immagini tratte da un&rsquo;investigazione che mostrava <b>la realt&agrave; che si cela dietro l&rsquo;immagine idilliaca spesso diffusa dalle aziende del settore alimentare, incluso quello biologico.&nbsp;</b></p>

<p>&nbsp;</p>

<p>Infatti, nonostante l&rsquo;azienda fosse certificata come bio,<b> le immagini mostrate dai giornalisti tedeschi sono devastanti</b>: galline ridotte pelle e ossa e coperte da gravi infezioni - tutti segni di enorme sofferenza chiaramente visibili agli occhi degli investigatori perch&eacute; i volatili erano quasi privi di piume, un altro segno di malessere e incuria.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-2387" src="/app/uploads/2018/04/16708142937_48004ac191_z.jpg" style="width: 840px; " /></p>

<p>&nbsp;</p>

<p>E come se non bastasse, gli investigatori si sono trovati di fronte a quella che &egrave; un&rsquo;altra scena che ricorre spesso all&rsquo;interno degli allevamenti: migliaia di galline ovaiole costrette a condividere gli spazi con i cadaveri delle loro compagne che non ce l&rsquo;hanno fatta a sopravvivere.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-2388" src="/app/uploads/2018/04/16914200432_26dd7c1f30_z.jpg" style="width: 840px; " /></p>

<p>&nbsp;</p>

<p><b>La battaglia legale&nbsp;</b></p>

<p>&nbsp;</p>

<p>La vicenda comincia nel 2012, quando il canale TV tedesco <i>MDR </i>pubblic&ograve; le immagini ottenute grazie al lavoro dell&rsquo;organizzazione animalista tedesca <i>Animal Rights Watch (ARIWA)</i>, che si era infiltrata all&rsquo;interno di un allevamento di galline ovaiole a marchio biologico facente parte del gruppo <i>F&uuml;rstenhof GmbH.&nbsp;</i></p>

<p>&nbsp;</p>

<p>Il verdetto finale per&ograve; &egrave; arrivato solo in questi giorni, con la decisione della corte federale tedesca di Karlsruhe, che ha emesso <b>una sentenza in favore della libert&agrave; di stampa</b> <b>e del diritto dei consumatori di conoscere la verit&agrave; che si cela dietro all&rsquo;industria della carne</b> e delle etichette commerciali che si fregiano di rispettare in toto il benessere animale.&nbsp;</p>

<p>&nbsp;</p>

<p>Dopo la pubblicazione dell&rsquo;investigazione, <b><i>F&uuml;rstenhof </i>ha cercato in tutti i modi di bloccare la diffusione del servizio</b>: ha fatto causa a <i>MDR</i> per &ldquo;omissione&rdquo;, e cio&egrave; - secondo il loro punto di vista - di aver diffuso informazioni false e omesso altre rilevanti.&nbsp;</p>

<p>&nbsp;</p>

<p>Nelle prime fasi del processo, l&rsquo;azienda sembrava averla avuta vinta.&nbsp;</p>

<p>&nbsp;</p>

<p>Le immagini infatti erano state ottenute tramite investigazione e mostravano una situazione che era, di fatto, considerata in linea con la legislazione europea - nonostante l&rsquo;evidente sofferenza degli animali coinvolti e le terribili condizioni in cui le galline ovaiole erano costrette a vivere all&rsquo;interno degli allevamenti dell&rsquo;azienda. &nbsp;</p>

<p>&nbsp;</p>

<p><b>Ma per fortuna, durante l&rsquo;appello presso la corte federale, &egrave; emerso chiaramente che le immagini ottenute tramite investigazioni sotto copertura sono giustificate e necessarie</b>, secondo quanto stabilito dai giudici.&nbsp;</p>

<p>&nbsp;</p>

<p>Le condizioni degli allevamenti intensivi e dei macelli rivelate dagli investigatori di ARIWA e da tanti investigatori di Animal Equality in tutto il mondo dimostrano proprio questo:<b> l&rsquo;immagine idilliaca mostrata nelle pubblicit&agrave; dell&rsquo;industria della carne &egrave; quanto di pi&ugrave; lontano possa esistere dalla realt&agrave;.&nbsp;</b></p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-2389" src="/app/uploads/2018/04/16293083664_b716c3ab36_z-1.jpg" style="width: 840px; " /></p>

<p>&nbsp;</p>

<p>Inoltre, il giudizio dei magistrati tedeschi ha messo in chiaro anche un altro punto importante, e cio&egrave; che pubblicare queste immagini risponde a un interesse pubblico generale e al rispetto della libert&agrave; di stampa e di espressione, due dei valori pi&ugrave; importanti per la salvaguardia della democrazia.&nbsp;</p>

<p>&nbsp;</p>

<p>Ma i magistrati tedeschi non si sono fermati qui.&nbsp;</p>

<p>&nbsp;</p>

<p>I componenti della corte infatti hanno messo nero su bianco che le registrazioni fatte durante l&rsquo;investigazione &ldquo;informano correttamente il pubblico&rdquo;, anche perch&eacute; &ldquo;la linea editoriale del programma voleva mettere proprio in discussione la produzione massiccia di prodotti animali a marchio bio e la veridicit&agrave; delle immagini diffuse dalle aziende, che, come mostra il servizio giornalistico, sono in contrasto con la realt&agrave;.&rdquo;</p>

<p>&nbsp;</p>

<p>Gi&agrave; a febbraio di quest&rsquo;anno <a href="https://animalequality.it/blog/decisione-senza-precedenti-corte-regionale-tedesca-conferma-la-legalita-delle/">i giudici tedeschi si erano espressi a favore delle investigazioni condotte dalle organizzazioni per la protezione degli animali da reddito.&nbsp;</a></p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-2390" src="/app/uploads/2018/04/9814455046_0772243740_z.jpg" style="width: 840px; " /></p>

<p>&nbsp;</p>

<p>In quella circostanza, un giudice dell&rsquo;alta corte regionale di Naumburg conferm&ograve; la legalit&agrave; delle investigazioni sotto copertura, perch&eacute; sono l&rsquo;unico modo per mostrare al pubblico tutte le crudelt&agrave; che vengono perpetrate nei confronti degli animali all&rsquo;interno di allevamenti intensivi e nei macelli.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;Oggi &egrave; un bel giorno per la libert&agrave; di stampa,&rdquo; ha dichiarato Joachim Pohl, il consulente legale della tv pubblica tedesca <i>ZDF</i>.&nbsp;</p>

<p>&nbsp;</p>

<p><b>Ed &egrave; un bel giorno per tutti i nostri investigatori</b>, che lavorano senza sosta per mostrare quello che avviene in questi luoghi di grande sofferenza.&nbsp;</p>

<p>&nbsp;</p>

<p><b>Tutto quello che puoi&nbsp;fare per sostenere le investigazioni&nbsp;</b></p>

<p>&nbsp;</p>

<p>&Egrave; difficile avere il coraggio di guardare in faccia la realt&agrave;: gli animali allevati a scopo alimentare sono costretti a vivere una lunghissima agonia, passando tutta la vita in allevamenti sovraffollati e sporchi, per poi finire poi sui ganci dei macelli, dove verranno uccisi brutalmente.&nbsp;</p>

<p>&nbsp;</p>

<p>Ma senza immagini che mostrino ci&ograve; che avviene davvero in questi luoghi, le sofferenze di questi animali rimarrebbero nascoste a tutti e non ci sarebbe soluzione.&nbsp;Per questo e molti altri motivi, il lavoro degli investigatori sotto copertura &egrave; fondamentale.&nbsp;</p>

<p>&nbsp;</p>

<p>Accogliamo positivamente la sentenza tedesca e speriamo che serva a far capire chiaramente che avere a cuore il benessere degli animali non &egrave; - e non pu&ograve; essere considerato - un crimine.&nbsp;</p>

<p>&nbsp;</p>

<p>La crudelt&agrave; e gli abusi sistematici a cui sono sottoposti gli animali in allevamenti intensivi e macelli devono essere mostrati a tutti: per questo i nostri investigatori sono sul campo anche in questo momento, assumendosi personalmente i rischi e le conseguenze di un lavoro cos&igrave; duro e pericoloso.</p>

<p>&nbsp;</p>

<h1 class="rtecenter"><a href="https://animalequality.it/infiltrato-industria-carne/"><b>GUARDA ORA L&rsquo;INTERVISTA A UN INVESTIGATORE INFILTRATO DI ANIMAL EQUALITY</b></a></h1>

