La siccità in California sta facendo notizia da mesi e l'opinione pubblica è molto attenta alle decisioni che le istituzioni hanno intenzione di prendere per limitare l'uso dell'acqua. Come previsto, si sono aperti ampi dibattiti sull'impatto che le nostre scelte - e soprattutto la nostra dieta - hanno sul consumo di acqua.

<a href="http://www.nytimes.com/2015/05/31/opinion/sunday/nicholas-kristof-our-water-guzzling-food-factory.html?smid=tw-nytopinion&amp;_r=2">Un nuovo studio pubblicato sul New York Times</a> ha confrontato la quantità di acqua utilizzata per la produzione di vari alimenti e gli hamburger sono in cima alla lista.

L'autore dello studio, Nicholas Kristof, spiega: <em>"La crisi in California è soltanto un'anticipazione della carenza idrica che si avrà in gran parte del mondo. E mentre noi associamo lo sperpero di acqua al suo uso per riempire piscine o annaffiare prati verdeggianti, i più grandi consumi di acqua sono dovuti invece all'allevamento e all'agricoltura. Infatti, in California, l'80 per cento dell'acqua utilizzata dagli esseri umani è destinata all'agricoltura e all'allevamento".</em>

Sicuramente gli hamburger sono in cima alla lista degli alimenti ad alto consumo di acqua, ma anche altri prodotti di origine animale, come uova e formaggi, utilizzano una quantità incredibile di acqua e di altre risorse che potrebbero essere meglio utilizzate.

<em>"I vegetali si trasformano in maniera piuttosto inefficiente in proteine animali. Infatti la produzione di un solo uovo necessita di circa 200 litri d'acqua; un chilo di pollo, 4000 litri; un litro di latte, 1000 litri. E un chilo di carne di manzo, 15.000 litri di acqua” </em>spiega Kristof.

Dal consumo diretto di acqua degli animali, all'acqua utilizzata per annaffiare le colture destinate a diventare mangime, l'allevamento è il settore che richiede la più grande quantità di acqua.

Mentre i sostenitori degli allevamenti intensivi si vantano dei cambiamenti che hanno permesso di diminuire il prezzo della carne, Kristof afferma che a fronte di ciò le grandi aziende di allevamento impongono elevatissimi costi sociali, dati dall'uso inefficiente delle nostre risorse naturali.

<img class="wp-image-10521" style="width: 800px;" src="/app/uploads/2015/07/2015-04-07-1428448186-1408575-huffpo-thumb.png" alt="" />

Nell'allevamento oltretutto si utilizzano troppi antibiotici e le malattie umane sono sempre più resistenti ad essi, con grave pericolo per la salute pubblica. <a href="https://www.nytimes.com/2012/09/04/health/use-of-antibiotics-in-animals-raised-for-food-defies-scrutiny.html">Circa quattro quinti degli antibiotici venduti negli Stati Uniti</a>  vengono utilizzati per il bestiame e il pollame e ogni anno in America, <a href="http://www.cdc.gov/drugresistance/threat-report-2013/">secondo il Centro di controllo e prevenzione delle Malattie</a>, 23.000 persone muoiono per infezioni resistenti agli antibiotici.

L'agricoltura utilizza troppi pesticidi, <a href="https://www.nytimes.com/2009/06/28/opinion/28kristof.html">alcuni dei quali potenti distruttori endocrini</a> che sono stati collegati a cancro, obesità e disordini riproduttivi.

L'allevamento industriale è spesso basato sul maltrattamento degli animali, <a href="https://www.nytimes.com/2014/12/04/opinion/nicholas-kristof-abusing-chickens-we-eat.html">in particolare il pollame</a>, che viene condotto con <a href="https://www.nytimes.com/2014/12/04/opinion/nicholas-kristof-abusing-chickens-we-eat.html">spietata crudeltà</a>.

<img class="wp-image-10602" style="width: 800px;" src="/app/uploads/2015/07/Sick-and-deformed-chickens-suffering-inside-a-chicken-factory-farm.jpg" alt="" />

Ma noi consumatori possiamo fare una scelta importante. Con l'adozione di una dieta a base vegetale, sana e sostenibile, abbiamo il potere di preservare preziose risorse naturali e proteggere gli animali, che purtroppo pagano il prezzo più basso delle loro carni o derivati con una vita di sofferenze e torture.

<a href="https://it.loveveg.com/">Clicca qui</a> per conoscere tante ottime ricette sostenibili e senza crudeltà.
