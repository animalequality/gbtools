<strong>Moby</strong>, al secolo Richard Melville Hall è di sicuro una delle maggiori <strong>icone</strong> della musica elettronica degli ultimi <strong>25 anni</strong>. Ma i suoi dischi e le sue canzoni non sono l'unico motivo per cui Moby è famoso nel mondo.

Moby è infatti un'icona anche per quello che riguarda la <strong>causa animale</strong> nel mondo, ed anche quest'estate non ha perso l'occasione per dimostrarcelo.

Il video clip del suo ultimo singolo registrato con i <strong>the Void Pacific Choir </strong>è stato infatti realizzato utilizzando le immagini di alcune investigazioni di <strong>Animal Equality</strong> e <strong>Mercy For Animals</strong>.
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/jtL3Jsl2ieE?rel=0&amp;showinfo=0" width="850" height="478" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
Le liriche del pezzo, assieme al videoclip stesso, lasciano pochi dubbi: frasi come "<em>fight for me</em>" (combatti per me) o "<em>don't leave me alone</em>" (non abbandonarmi) faranno amare questa canzone a chiunque abbia a cuore gli animali.

O no? :)
<h4>Circle V</h4>
La pubblicazione di "Don't Leave Me" è solo una parte però della news di oggi: Moby ha infatti annunciato parallelamente le date del festival <strong>Circle V</strong>, un festival <strong>100% cruelty free</strong> i cui ricavati andranno tutti a sostegno delle associazioni per la protezione animale.

Si terrà il prossimo 23 ottobre, al <strong>Fonda Teathre</strong> di <strong>Los Angeles</strong> e vedrà il live di Moby proprio come show principale.

Citando lo stesso Moby a riguardo: "<em>con Circle V è una nuova tappa del mio lavoro, sia come musicista che come attivista per i diritti animali. Non potrei essere più entusiasta di questo evento e non potrei essere più fiero di esserne l'headliner.</em>"

Moby crede che un futuro migliore con diritti basici garantiti anche per gli animali sia possibile se tutti facciamo qualcosa <strong>ora</strong>, e proprio in questo spirito ha appena pubblicato il video di "Don't Leave Me" ed il comunicato stampa con il quale annunciava il Circle V Festival.
<h4>Non solo musica</h4>
Ma l'impegno di Moby per i diritti degli animali non si limita solo alla musica. Nel 2010 ha pubblicato una raccolta di citazioni di persone interne all'industria alimentare dal titolo<em> Gristle: From Factory Farms to Food Safety (Thinking Twice About the Meat We Eat).</em> Letteralmente <em>Gristle: dall'allevamento intensivo alla sicurezza alimentare (pensiamoci due volte alla carne che mangiamo).</em>

In più lo scorso novembre ha inaugurato a <strong>Los Angeles</strong> il suo ristorante 100% cruelty free: <strong>Little Pine</strong>.

L'apertura di un ristorante ispirato alla dieta mediterrane e completamente a base vegetale di per se non è una grande notizia... ma il fatto che<strong> tutti i suoi ricavati</strong> siano destinati al <strong>sostegno</strong> delle associazioni per la <strong>protezione animale</strong> è invece abbastanza meritevole di almeno una nota, no?
