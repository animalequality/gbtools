<p><strong>ANTIPASTO:&nbsp;Treccia di Castagna con Ripieno di Spinaci (per 6 persone)</strong></p>

<p><img alt="" class="wp-image-10486" src="/app/uploads/2015/12/1-Antipasto-rid.jpg" style="width: 800px; " /></p>

<p>Ingredienti:</p>

<p>- 250 g&nbsp;di latte di soia al naturale;<br />
- 1 cubetto di lievito di birra;<br />
- 400 g&nbsp;di farina integrale bio;<br />
- 150 g&nbsp;di farina di castagne bio;<br />
- 50 g di&nbsp;olio EVO;<br />
- 300 g&nbsp;di spinaci freschi;<br />
- 1 spicchio d&#39;aglio.</p>

<p>Procedimento:<br />
Versare il latte in una scodella e unirlo con l&#39;olio e il lievito, farlo sciogliere bene ed aggiungere la farina di castagna e poi la farina integrale. Salare a piacere. Se l&#39;impasto risulta appiccicoso, aggiungere ancora un cucchiaio di farina integrale.&nbsp;<br />
Mettere l&#39;impasto in una ciotola, coprire&nbsp;e lasciare lievitare per&nbsp;circa un&#39;ora.&nbsp;</p>

<p>Per&nbsp;il ripieno:&nbsp;mettere gli&nbsp;spinaci direttamente in padella con un filo d&#39;olio e uno spicchio d&#39;aglio pulito. Cuocere a fuoco basso per pochi minuti senza coperchio.</p>

<p>Quando l&#39;impasto &egrave; raddoppiato di volume, stenderlo con il mattarello&nbsp;formando un rettangolo da dividere in tre parti uguali.&nbsp;<br />
Stendere su ciascuna delle parti il ripieno, poi arrotolare la pasta formando un cilindro per ognuna.&nbsp;<br />
Formare la treccia con i tre cilindri facendo attenzione perch&eacute; sono molto delicati.&nbsp;<br />
Lasciare lievitare ancora per 30 minuti e mettere nel forno caldo per&nbsp;30 minuti a&nbsp;200&deg;.<br />
Servire tiepido&nbsp;e... buon appetito!</p>

<p><strong>PRIMO:&nbsp;Maltagliati al Radicchio con Crema di Noci (per 6 persone)</strong></p>

<p><strong><img alt="" class="wp-image-10520" src="/app/uploads/2015/12/2-Primo-rid.jpg" style="width: 800px; " /></strong></p>

<p>Ingredienti:</p>

<p>- 500 g&nbsp;di Maltagliati (ottenuti da sfoglie di semola di grano duro per lasagne senza uova);<br />
- 400 g&nbsp;di radicchio;<br />
- 350 g di&nbsp;tofu al naturale;<br />
- 250 g&nbsp;di noci;<br />
- 2 cipolle;<br />
- 1 bicchiere d&#39;acqua;<br />
- 2 cucchiaini di amido di mais;<br />
- olio EVO;<br />
- noce moscata;<br />
- salvia.</p>

<p>Procedimento:<br />
Tagliare il radicchio e la cipolla e farli scottare in padella <span style="line-height: 20.8px;">a fuoco alto&nbsp;</span>con un po&#39; di olio EVO. Aggiungere sale e pepe a piacimento. Cuocere per 5/6 minuti. Far raffreddare e impastare con 250 g&nbsp;di tofu precedentemente sbriciolato.<br />
A parte, mettere le noci nel frullatore e ridurle in piccoli pezzi. Lasciare da parte qualche cucchiaio di noci.<br />
Versare un bicchiere d&#39;acqua e mettere 100 g&nbsp;di tofu nel frullatore, insieme ad un cucchiaino di noce moscata e un pizzico di sale. Frullare fino ad ottenere un bel mix.<br />
Versare il composto in un pentolino e cuocere a fuoco basso. Mescolare in modo continuo aggiungendo due cucchiaini di amido di mais, cercando di non formare grumi.<br />
Quando il composto si addensa spegnere il fuoco continuando a mescolare.<br />
Mettere a bollire i maltagliati in acqua salata. Una volta cotti, scolarli e versarli nella padella con il radicchio. Mescolare bene e versare un po&#39; di crema di noci. Impiattare come in foto aggiungendo un po&#39; di granella di noci e di salvia. Aggiungere la crema di noci a piacimento.</p>

<p><strong>SECONDO:&nbsp;Funghi ripieni con contorno di Carciofi alla Romana (per 6 persone)</strong></p>

<p><img alt="" class="wp-image-1377" src="/app/uploads/2015/12/3-Secondo-rid.jpg" style="width: 800px; " /></p>

<p>Ingredienti per i funghi:</p>

<p>- 12 funghi champignon grandi;<br />
- 1 porro;<br />
- 4 pomodori;<br />
- 50 g&nbsp;di pisellini surgelati;<br />
- 50 g&nbsp;di pomodorini;<br />
- pangrattato di riso.</p>

<p>Procedimento:<br />
Pulire i funghi, togliere il gambo e metterlo da parte, posizionare le teste dei funghi su&nbsp;una teglia con&nbsp;carta da forno. Accendere il forno ventilato a 180&deg;.<br />
Tritare il porro e rosolarlo con olio in una padella antiaderente.&nbsp;Aggiungere poi i gambi dei funghi, anch&#39;essi tritati, e qualche pomodorino a pezzi.&nbsp;Far&nbsp;rosolare il tutto per qualche minuto.<br />
A fine cottura, aggiungere il pangrattato di riso (si pu&ograve; utilizzare&nbsp;anche la mollica di pane) sale e pepe qb.<br />
Mettere il composto sui funghi e aggiungere&nbsp;un cucchiaino di piselli precedentemente bolliti sopra ognuno di essi. Dare un&#39;ultima spolverata di pangrattato, un filo d&#39;olio e infornare. Dopo circa 20 minuti, controllare la cottura e sfornare quando i funghi sono leggermente rosolati.</p>

<p>Ingredienti per i carciofi alla romana:</p>

<p>- 12 carciofi;<br />
- prezzemolo;<br />
- aglio;<br />
- 2 limoni.</p>

<p>Procedimento:<br />
Pulire bene i carciofi&nbsp;dalle foglie pi&ugrave; dure, immergerli in acqua con qualche fetta di limone per alcuni&nbsp;minuti.<br />
Metterli in una casseruola a testa in gi&ugrave; con&nbsp;olio abbondante, sale,&nbsp;prezzemolo e&nbsp;poca acqua.<br />
Bagnare e strizzare un foglio di&nbsp;carta da forno e coprire i carciofi.&nbsp;Aggiungere un coperchio e cuocere&nbsp;a fuoco vivo. Quando l&#39;acqua raggiunge l&#39;ebollizione,&nbsp;abbassare la fiamma.<br />
Dopo circa 20 minuti, togliere la carta da forno e controllare la cottura.&nbsp;Qualora vi fosse ancora dell&#39;acqua, alzare la fiamma fino a completa evaporazione.</p>

<p><strong>DESSERT:&nbsp;Crema di Mandorla all&#39;Arancia su letto di Mousse al Cioccolato (per 6 persone)</strong></p>

<p><strong><img alt="" class="wp-image-10533" src="/app/uploads/2015/12/4-Dolce-rid.jpg" style="width: 800px; " /></strong></p>

<p>Ingredienti per&nbsp;la crema:</p>

<p>- 100 g di&nbsp;zucchero;<br />
- 50 g di&nbsp;farina;<br />
- scorza di 1/2 arancia&nbsp;biologica;<br />
- 20 g&nbsp;di amido di mais;<br />
- 30 g&nbsp;di succo d&#39;arancia biologico;<br />
- 1/2 litro di latte di mandorla.</p>

<p>Procedimento:<br />
Mettere in un pentolino lo zucchero, la farina,&nbsp;la scorza&nbsp;d&#39;arancia <span style="line-height: 20.8px;">e il succo</span>. Cuocere a&nbsp;fuoco basso aggiungendo il latte di mandorla, mescolare facendo attenzione a non portare la crema ad ebollizione. Una volta addensata la crema,&nbsp;lasciar&nbsp;raffreddare e rimuovere la scorza d&#39;arancia.</p>

<p>Ingredienti per la mousse:</p>

<p>- 400 g di&nbsp;cioccolato fondente al 70%;<br />
- 450 g&nbsp;d&#39;acqua.</p>

<p>Procedimento:<br />
Sciogliere il cioccolato in un pentolino a bagnomaria. Una volta fuso il cioccolato, aggiungere l&#39;acqua.<br />
Mescolare bene&nbsp;fino a quando il cioccolato &egrave; completamente emulsionato.&nbsp;Versare il composto in una scodella raffreddata <span style="line-height: 20.8px;">esternamente</span>&nbsp;con del ghiaccio&nbsp;e usare una frusta elettrica per ottenere un composto perfetto.</p>

<p>Composizione:<br />
Versare prima la mousse fino a met&agrave; bicchiere e poi la crema di mandorla fino all&#39;estremit&agrave;.<br />
Ornare con pezzettini di mandorla, cioccolato&nbsp;e scorza d&#39;arancia.</p>

<p><strong>A MEZZANOTTE: Coveghino e Lenticchie</strong></p>

<p><img alt="" class="wp-image-10539" src="/app/uploads/2015/12/5-mezzanotte-rid.jpg" style="width: 800px; " /></p>

<p>Ingredienti per il coveghino:</p>

<p>- 450 g&nbsp;di preparato per seitan;<br />
- 100 g&nbsp;di tofu&nbsp;affumicato;<br />
- 100 g&nbsp;di patate;<br />
- noce moscata;<br />
- cannella;<br />
- 1 cucchiaio di paprica;<br />
- 1 cucchiaio di pepe nero;<br />
- 40 g&nbsp;di amido di mais;<br />
- 20 cl di vino rosso;<br />
- 20 cl di Marsala.</p>

<p>Procedimento:<br />
Mettere nel mixer il seitan con le&nbsp;patate, il vino, il marsala, il sale, l&#39;amido di mais e le spezie.&nbsp;Macinare il tutto pi&ugrave; finemente possibile.<br />
Mettere il composto in un contenitore e aggiungere il tofu sbriciolato.&nbsp;Amalgamare il tutto.<br />
Modellare l&#39;impasto a formare un salsicciotto&nbsp;e arrotolarlo in un panno (o in carta da forno).&nbsp;Cuocere a vapore per circa 30 minuti.<br />
Una volta cotto, lasciarlo raffreddare prima di toglierlo dal suo involucro.</p>

<p>Ingredienti per le lenticchie:</p>

<p>- 500 ml di brodo vegetale;<br />
- 100 g&nbsp;di pomodori pelati in scatola;<br />
- 1 cipolla grande;<br />
- 3 carote;<br />
<span style="line-height: 20.8px;">- 2 spicchi d&#39;aglio;</span><br />
- prezzemolo;<br />
- Olio EVO.</p>

<p>Procedimento:<br />
Lavare il prezzemolo e tritarlo.<br />
Mettere in una pentola l&#39;olio&nbsp;assieme alla cipolla tritata finemente e all&#39;aglio intero in camicia. Portare sul fuoco e far dorare a fiamma media. Unire un paio di cucchiai di brodo e proseguire la cottura per 4-5 minuti.<br />
Aggiungere il pomodoro e&nbsp;mescolare.&nbsp;Aggiungere le lenticchie, un cucchiaio di prezzemolo tritato, mescolare e lasciare che riprenda a bollire.<br />
Aggiungere il brodo caldo, le carote tagliate a pezzettoni e&nbsp;un pizzico di sale.&nbsp;Abbassare la fiamma e cuocere per 40 minuti con il coperchio, mescolando di tanto in tanto.<br />
Eliminare l&#39;aglio a met&agrave; cottura.&nbsp;Trascorso il tempo di cottura, le lenticchie dovranno essere tenere. In caso contrario, proseguire la cottura, anche fino ad un&#39;ora.</p>

<p>Composizione:<br />
Su un letto di lenticchie calde,&nbsp;adagiare&nbsp;3 o 4 fettine di coveghino - anch&#39;esso caldo.<br />
<br />
<strong>Noi abbiamo gi&agrave; l&#39;acquolina, e voi? Buon appetito!</strong></p>

