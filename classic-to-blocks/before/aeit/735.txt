<div class="center"><img class="wp-image-2341" src="/app/uploads/2018/03/SENTENZA_INVESTIGATORI_GERMANIA_1.jpg" /></div>
<p>Molto del lavoro che riusciamo a fare in Animal Equality &egrave; basato sulle investigazioni sotto copertura. Solo nel 2017 Animal Equality ha presentato 21 investigazioni, e grazie alle scoperte che abbiamo fatto abbiamo potuto mostrare le terribili sofferenze a cui sono sottoposti gli animali negli allevamenti intensivi e nei macelli.&nbsp;</p>

<p>Per fortuna non siamo gli unici ad adottare queste tecniche. La nostra battaglia per aiutare gli animali &egrave; condivisa a livello internazionale con tanti coraggiosi colleghi di altre organizzazioni, che come noi documentano ogni giorno quello che avviene in questi luoghi.&nbsp;</p>

<p><img alt="" class="wp-image-2340" src="/app/uploads/2018/03/SENTENZA_INVESTIGATORI_GERMANIA_0.jpg" style="width: 840px;" /></p>

<p>Purtroppo per&ograve; - nonostante i nostri sforzi - non ci sono mai grosse conseguenze per gli operatori degli allevamenti e dei macelli oggetto delle nostre investigazioni, anche nei casi in cui vengano svelate crudelt&agrave; estreme e violazioni molto gravi alle norme vigenti. Al contrario, sono le persone che lavorano per mostrare quello che avviene all&rsquo;interno di questi luoghi che spesso vengono messe sotto accusa e talvolta portate in tribunale.</p>

<p><b>Ma a febbraio di quest&rsquo;anno, per la prima volta, una corte regionale tedesca ha confermato che compiere investigazioni sotto copertura &egrave; legale.&nbsp;</b></p>

<center>

<h4><b>Che cosa &egrave; successo</b></h4>
</center>

<p>Nel 2013 l&rsquo;organizzazione tedesca <i>Animal Rights Watch</i> (ARIWA) ha pubblicato un&rsquo;investigazione sotto copertura contenente rivelazioni sconvolgenti sulle condizioni di vita dei maiali allevati in una delle pi&ugrave; grandi aziende tedesche del settore, la &ldquo;van Gennip Tierzuchtanlagen GmbH&quot;, con sede in Sassonia.&nbsp;</p>

<p><a href="https://www.flickr.com/photos/animalrightswatch/sets/72157671354550422/with/28812668661/">Le immagini mostravano scene di estrema crudelt&agrave; e le gravissime violazioni del benessere animale</a> a cui erano sottoposti i maiali allevati nei capannoni dell&rsquo;azienda: ammassati nella sporcizia, coperti da parassiti e colpiti da ulcere e cisti enormi.&nbsp;</p>

<p>Quando le immagini sono state pubblicate c&rsquo;&egrave; stato un grande scalpore, ma il trattamento riservato ad attivisti e all&rsquo;azienda di fronte alla legge &egrave; stato molto diverso e davvero impari; le indagini delle autorit&agrave; sull&rsquo;azienda si sono fermate nel 2015, mentre quelle sugli investigatori sono sfociate in un&rsquo;accusa di violazione della propriet&agrave; privata a settembre 2016.&nbsp;</p>

<p>Per fortuna per&ograve; la situazione si &egrave; evoluta positivamente per gli attivisti, i quali sono stati proclamati innocenti sia in primo grado sia in appello e, oggi, sono stati definitivamente assolti anche nell&rsquo;ultimo grado di giudizio. Il verdetto &egrave; chiaro: l&rsquo;Alta Corte Regionale della Sassonia in Germania ha assolto i tre accusati, confermando quindi le due sentenze pronunciate in precedenza di fronte alla corte locale e presso il tribunale regionale.&nbsp;</p>

<p><img alt="" class="wp-image-2342" src="/app/uploads/2018/03/sheep.jpg" style="width: 840px;" /></p>

<p>&nbsp;</p>

<center>
<h4><b>Perch&eacute; le investigazioni sono cos&igrave; importanti&nbsp;</b></h4>
</center>

<p>&Egrave; difficile avere il coraggio di guardare in faccia la realt&agrave;: gli animali allevati a scopo alimentare sono costretti a vivere una lunghissima agonia, passando tutta la vita in allevamenti sovraffollati e sporchi, per poi finire poi sui ganci dei macelli, dove verranno uccisi brutalmente.&nbsp;</p>

<p>Si tratta di una realt&agrave; difficile da raccontare, ma senza immagini che mostrino ci&ograve; che avviene davvero in questi luoghi, le loro sofferenze rimarrebbero nascoste a tutti e senza soluzione. Per questo e molti altri motivi, il lavoro degli investigatori sotto copertura &egrave; fondamentale.&nbsp;</p>

<p>Accogliamo positivamente la sentenza tedesca e speriamo che serva a far capire chiaramente che avere a cuore il benessere degli animali non &egrave; - e non pu&ograve; essere considerato - un crimine.&nbsp;</p>

<p>La crudelt&agrave; e gli abusi sistematici a cui sono sottoposti gli animali in allevamenti intensivi e macelli devono essere mostrati a tutti: per questo i nostri investigatori sono sul campo anche in questo momento, assumendosi personalmente i rischi e le conseguenze di un lavoro cos&igrave; duro e pericoloso.</p>


<center>
<strong><a href="https://animalequality.it/infiltrato-industria-carne/">GUARDA ORA L&rsquo;INTERVISTA A&nbsp;</a></strong><strong><a href="https://animalequality.it/infiltrato-industria-carne/">UN INVESTIGATORE INFILTRATO</a></strong>

<strong><a href="https://animalequality.it/infiltrato-industria-carne/">DI ANIMAL EQUALITY</a></strong>
</center>



<p>&nbsp;</p>

<p dir="ltr">&nbsp;</p>

<p>&nbsp;</p>

