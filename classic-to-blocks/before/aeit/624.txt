Le immagini che seguono sono finite su tutti i giornali ed hanno sconvolto milioni di persone eppure risulta ancora difficile per la maggior parte di noi <strong>collegare le nostre abitudini alimentari alle vite spezzate dall'industria della carne</strong>.

Sono immagini forti, immagini che non avremmo voluto mostrarvi, ma la situazione, come tutti saprete, è drammatica: <strong>bisogna fare qualcosa e bisogna farlo il prima possibile</strong>.

Speriamo che tutto questo possa servire a smuovere delle coscienze.

1. Questa donna è rimasta molto amareggiata scoprendo praticamente a fine pasto che il pollo che aveva quasi terminato, fosse <strong>un animale in grado di camminare</strong>.

Guarda qui: <a href="https://ibankcoin.com/flyblog/2016/05/23/woman-goes-ape-on-twitter-after-finding-chicken-foot-in-chipotle-burrito-bowl/" target="_blank" rel="noopener noreferrer">ibankcoin.com</a>

2. Quando questo cliente di KFC ha messo in bocca la propria crocchetta di pollo, ha dovuto affrontare l'amara scoperta che questi animali, un tempo, <strong>erano in grado di respirare</strong>. Avvolto nella pastella fritta ha infatti trovato il polmone di un pollo.

Guarda qui: <a href="https://metro.co.uk/2016/02/04/man-finds-lung-attached-to-kfc-chicken-breast-5661391/">metro.co.uk</a>

3. Il bacon è un salume di maiale che si ottiene dalla <strong>pancia dell'animale</strong> e, purtroppo, non c'è immagine più adatta per raccontarlo di quella che trovate qui sotto. È stata scattata da un'utente di Reddit ancora sconvolta dopo l'amara scoperta.

Guarda qui: <a href="http://www.dailymail.co.uk/femail/food/article-3734327/Bacon-NIPPLE-left-sends-Reddit-social-media-meltdown.html">dailymail.co.uk</a>

4. Questo cliente di un fast food francese voleva semplicemente mangiare delle ali di pollo fritte. Quello che ha ritrovato nel proprio piatto gli ha ricordato come le ali di pollo siano gli arti di esseri viventi, con un volto.

<center><iframe src="https://www.youtube.com/embed/7Zr3th8-6AA?rel=0&amp;showinfo=0" width="850" height="478" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

</center>fonte: <a href="https://www.liberation.fr/france/2016/04/24/chez-quick-une-tete-de-poulet-dans-les-chicken-wings_1448289/">liberation.fr</a>
<h4>Gli animali all'interno dell'industria della carne vivono vite brevi e piene di sofferenza, subendo crudeltà che i più, fra di noi, ignorano.</h4>
<h4>Ricordiamoci da dove viene il cibo che finisce sulle nostre tavole.</h4>
<h4>Ricordiamoci della connessione che c'è fra la sofferenza di miliardi di animali e quello che l'industria della carne vuole venderci ogni giorno.</h4>
<h4>Ricordiamoci di avere compassione.</h4>
