<div class="center"></div>
La libertà, nel mondo come lo conosciamo oggi, sembra essere un qualcosa di sfuggevole, destinato a svanirci tra le mani. Lo sanno bene gli animali selvatici, che pur essendo liberi sulla carta, diciamo per definizione, non lo sono poi all'atto pratico. Anche qui c'è qualcuno, da qualche parte, che decide per la loro vita e se esiste qualcuno che sceglie per noi, allora significa che non siamo liberi.
<div>Quando la fauna selvatica stride con le necessità della civiltà allora c'è qualcuno che decide cosa è 'meglio' per la sopravvivenza di quest'ultima, e quali misure adottare per riportare la situazione alla normalità.</div>
<div></div>
<div><strong>È quello che sta per avvenire in Australia, dove circa 10.000 cavalli selvatici stanno per essere abbattuti in quella che sembra essere una <em>ovvia</em> risposta ad una crisi ambientale.</strong></div>
<div></div>
<div>Il Central Land Council (CLC) ha stabilito che da mercoledì si potrà procedere con il piano di sfoltimento, a 300 chilometri a sud est di Alice Springs, nello Stato federale del Northern Territory.
<div></div>
<div>I cavalli saranno colpiti da quattro elicotteri che pattuglieranno la zona fino alla metà di giugno.</div>
<div></div>
<div>
<div>Il motivo che ha spinto gli organi predisposti a prendere questa decisione è la mancanza di cibo e acqua destinati al consumo umano, beni largamente utilizzati anche dagli animali che, come tutti, devono nutrirsi e abbeverarsi. Molti animali stanno già morendo a causa dell'inadeguatezza delle risorse disponibili; <strong>l'abbattimento preventivo e in larga scala sembra essere la miglior risposta umana possibile, o semplicemente quella preferibile sotto molti punti di vista</strong>.</div>
<div></div>
<div>Il direttore del CLC ha affermato <em>"Nessuno vuole vedere la sofferenza che provano questi animali, soprattutto gli abitanti originari della zona che amano i cavalli ma sono ben consapevoli delle terribili conseguenze che potrebbero essere causate dalla popolazione fuori controllo"</em>.</div>
<div></div>
<div>Ancora una volta, quella che a tutti gli effetti rimane una violenza terribile perpetrata ai danni degli animali, viene presentata all'opinione pubblica in una veste diversa, come una manovra obbligata per tutelare la collettività. Uccidere 10.000 animali rimane un massacro, a prescindere da quali siano le motivazioni che lo sostengono. Si possono trovare misure precauzionali per monitorare le popolazioni di animali selvatici, così come della manovre contenitive che permettono di ridurre il numero degli individui che ne fanno parte nel tempo. Tuttavia, ogni programma teso a risolvere il problema, nel rispetto di tutte le parti coinvolte, ha un costo maggiore, e quindi per ragioni prevalentemente economiche si sceglie la via più cruenta (e ovviamente meno costosa).</div>
<div></div>
<div>Quelle terre non sono nostre ma di tutta la vita che le abita.</div>
<div></div>
<div>Puoi scegliere di attivarti e manifestare la tua posizione di dissenso nei confronti di questa scelta, <a href="http://www.thepetitionsite.com/takeaction/586/961/380/">firmando una petizione</a> per chiedere al governo australiano che questo massacro venga annullato, in virtù di una diversa azione risolutiva basata sul rispetto.</div>
</div>
</div>
