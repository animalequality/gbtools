<div class="center"><img class="wp-image-2063" src="/app/uploads/2017/03/fb_eurospin.jpg" /></div>
<p>Animal Equality ha iniziato ieri una <a href="https://animalequality.it/news/"><strong>campagna di informazione</strong></a> rivolta ad una delle maggiori catene di discount in Italia, criticando l&rsquo;azienda per la vendita di uova provenienti da galline allevate in gabbia e sollecitando l&rsquo;<strong>Amministratore Delegato Romano Mion</strong> ad adottare una politica volta a cessare questa pratica.&nbsp;</p>

<center>
<p>&nbsp;</p>
<iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/lY6J7WnQioM" width="850"></iframe>

<p>&nbsp;</p>

<p><a href="https://animalequality.it/news/"><img alt="" class="wp-image-2062" src="/app/uploads/2017/03/PETIZIONE.png" style="width: 400px;" /></a><br />
&nbsp;</p>
</center>

<p>Nell&rsquo;arco della sola prima giornata, la campagna ha gi&agrave; ricevuto un enorme supporto:<strong> in meno di 24 ore quasi 3000</strong> persone hanno firmato la petizione rivolta a Romano Mion e numerosissimi sono stati i commenti lasciati sulla pagina Facebook di Eurospin dai <strong><a href="https://animalequality.it/difensori-animali/">Difensori degli Animali</a></strong>, la<strong> nuova squadra di volontari di Animal Equality Italia dedicata a combattere la crudelt&agrave; negli allevamenti</strong>.&nbsp;</p>

<p>&nbsp;</p>

<p>La campagna, che include <a href="https://animalequality.it/news/">un sito web dedicato all&rsquo;azienda</a>, prevede numerose attivit&agrave; fra cui <strong>azioni sui social media</strong>, <strong>proteste</strong> ed il<strong> coinvolgimento dei mezzi di comunicazione</strong>, e non cesser&agrave; fino a quando Romano Mion non si impegner&agrave; pubblicamente ad eliminare da tutti i punti vendita&nbsp;Eurospin il 100% delle uova provenienti da galline allevate in gabbia.</p>

<p>&nbsp;</p>

<center>
<p><a href="https://animalequality.it/difensori-animali/"><img alt="" class="wp-image-2064" src="/app/uploads/2017/03/difensore.png" style="width: 506px; " /></a></p>
</center>

