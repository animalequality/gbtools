La realtà è che, sebbene i nostri antenati si siano cibati anche di carne per più di due milioni di anni, il consumo di carne è un'abitudine che nei secoli è stata costantemente <em>sconvolta</em> da cambiamenti non indifferenti.

Per portare questa abitudine alimentare verso il suo prossimo passo, ossia il suo<a href="https://it.loveveg.com/"><strong> abbandono in favore di alternative che non implichino sofferenza</strong></a>, ci è sembrato utile raccontare la storia di 2.5 milioni di anni di consumo di carne in piccole pillole, piccoli avvenimenti chiave che messi assieme ne riassumano il percorso nella nostra storia.
<img class="wp-image-1730" style="width: 850px;" src="/app/uploads/2016/10/macello_1.png" alt="" />

Se è vero che l'uomo si ciba di carne da più di due milioni di anni, <strong>non è altrettanto vero che questa routine alimentare debba necessariamente proseguire per sempre</strong>.

Riconoscerne il peso etico, salutare ed ambientale è un passo nel segno del progresso della nostra società.

Un passo possibile e molto più sostenibile di tutti quelli fatti in precedenza.

<img class="wp-image-1704" style="width: 850px;" src="/app/uploads/2016/10/PULCINO.jpg" alt="" />

<strong>2.600.000 A.C.</strong>

Primati pre-umani iniziano a creare utensili di pietra per cibarsi delle carcasse degli animali.

<strong>2.000.000 A.C.</strong>

Primati pre-umani iniziano a cacciare animali selvaggi tendendo imboscate.

<strong>250.000 A.C.</strong>

Primati pre-umani iniziano ad utilizzare il fuoco per cuocere i propri cibi.

<strong>8000 A.C.</strong>

Il maiale è l'unica altra specie addomesticata dopo il cane. Bovini e polli seguiranno a breve.

<strong>7600 A.C.</strong>

Si estingue il Mammoth: finisce l'era della caccia <em>semplice</em> per i primati pre-umani.

<strong>1700 A.C.</strong>

Viene costruita la prima ghiacciaia per preservare la carne ed altri cibi.

<strong>1493 D.C.</strong>

Cristoforo Colombo importa i primi bovini d'allevamento in Nord America.

<strong>1876 D.C.</strong>

Negli Stati Uniti viene utilizzato per la prima volta un recinto per l'ingrasso dei bovini al posto del pascolo.

<strong>1885 D.C.</strong>

Wilhelm Roux riproduce in laboratorio la prima cellula. Sopravviverà pochi giorni.

<strong>1894 D.C.</strong>

Il chimico francese Pierre Eugene Marcellin Berthelot predice la produzione di carne in vitro.

<strong>1910</strong>

Il consumo medio pro capite di carne in Italia è di <strong>15.5kg annui.</strong>

<strong>1914 D.C.</strong>

Negli Stati Uniti si riscontra la prima epidemia di Afta Epizootica come risultato delle condizioni di sovraffollamento crescente negli allevamenti.

<strong>1916 D.C.</strong>

Apre il primo fast food della storia: il White Castle a Witchita, U.S.A.

<strong>1931 D.C.</strong>

Wiston Churchill dichiara pubblicamente il suo supporto alla crescita di carne in laboratorio in un discorso.

<strong>1944 D.C.</strong>

Donald Watson conia la parola "<em>vegan</em>". Prima di allora, una dieta vegana era semplicemente denominata come "<em>Pitagorica</em>".

<img class="wp-image-1726" style="width: 850px;" src="/app/uploads/2016/10/donald_whatson_2.jpg" alt="" />

<strong>1950 D.C.</strong>

Buona parte dell'industria ad alleva i propri animali al coperto per incrementare la propria produzione.

<strong>1961 D.C. </strong>

Il consumo medio pro capite di carne in Italia è di 27 kg annui.

<strong>1982 D.C.</strong>

<strong>Gregory Simms</strong> commercializza per la prima volta un veggie-burger

<img class="wp-image-1727" style="width: 850px;" src="/app/uploads/2016/10/vegburger_0.jpg" alt="" />

<em>La prima pagina del Meat Trades Journal del 12 aprile 1984. Pic by <a href="https://www.gregorysams.com/">Gregory Sams</a>.</em>

<strong>1988 D.C.</strong>

Si registra una delle peggiori epidemie del morbo di mucca pazza: è il risultato dell'utilizzare i resti animali nel mangime dei bovini.

<strong>1994 D.C.</strong>

McDonalds vende il suo 100miliardesimo hamburger e smette di contarli.

<img class="wp-image-1644" style="width: 850px;" src="/app/uploads/2016/08/carne_obesita_3.jpg" alt="" />

<strong>1995 D.C.</strong>

L'Agenzia per gli Alimenti ed i Medicinali Statunitense approva l'utilizzo delle tecnologie in-vitro per la produzione di carne da inserire sui mercati.

<strong>1999 D.C. </strong>

Willem Van Eelen registra il primo brevetto per la produzione di carne sviluppata in laboratorio.

<strong>2003 D.C.</strong>

Oron Catts e Ionat Zurr assaggiano la loro prima bistecca sviluppata in laboratorio.

<strong>2005 D.C.</strong>

Il consumo medio pro capite di carne in italia è di 81.1 kg

<img class="wp-image-1734" style="width: 850px;" src="/app/uploads/2016/10/allevamento-suini.png" alt="" />

<strong>2013 D.C.</strong>

Il professor <strong>Mark Post</strong> presenta il risultato delle sue ricerche: il primo hamburger prodotto da carne cresciuta in laboratorio.

Viene assaggiato di fronte al pubblico a Londra.
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/jJrSdKk3YVY?rel=0&amp;showinfo=0" width="850" height="478" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
