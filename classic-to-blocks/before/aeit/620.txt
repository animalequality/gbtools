<div class="center"><img class="wp-image-1915" src="/app/uploads/2016/12/OCCHI_8_MAIALE_1.jpg" /></div>
<p>Siamo tutti contrari al maltrattamento nei confronti degli animali, per&ograve; mangiamo carne proveniente da quell&rsquo;industria responsabile delle pi&ugrave; grande crudelt&agrave; sistematica&nbsp;della storia. Com&#39;&egrave; possibile? Ce lo spiegano gli psicologi con molta semplicit&agrave;.</p>

<h4>Il trucco &egrave; presentare la carne al consumatore nel modo che gli ricordi il meno possibile l&rsquo;animale da cui &egrave; stata ricavata.</h4>

<p>Pensate alle campagne pubblicitarie o anche solo alle confezioni di hamburger, salsicce, salumi, pancetta&hellip; carne trita, carne a bocconcini, carne a filetti, carne in vassoio. Di quegli animali in grado di provare emozioni e di sentire dolore, nemmeno l&rsquo;ombra.</p>

<p>L&#39;industria della carne sa che gli animali hanno un forte impatto nel processo decisionale del consumatore. Per quanto possa piacerci mangiare carne,&nbsp;a nessuno va completamente gi&ugrave; l&#39;idea di cibarsi di un animale sapendo che ha sofferto per una vita intera prima di finire nel nostro piatto.</p>

<p><img alt="" class="wp-image-1867" src="/app/uploads/2016/12/OCCHI_14_MAIALI.jpg" style="width: 850px; " /></p>

<p>Gli studiosi dell&rsquo;Universit&agrave; di Oslo hanno chiamato questo fenomeno <strong><em>il paradosso della carne</em></strong>. Hanno condotto cinque differenti studi in Norvegia ed hanno hanno analizzato il modo in cui consumiamo il cibo e quanto ci risulti sgradevole l&rsquo;idea di mangiare animali.</p>

<p><strong>Jonas R. Kunst</strong>, un ricercatore dell&#39;Istituto di Psicologia dell&#39;Universit&agrave; di Oslo si &egrave; cos&igrave;&nbsp;espresso:</p>

<h4>&ldquo;Il modo in cui l&rsquo;industria ci presenta la carne influisce sul nostro stato d&rsquo;animo quando la mangiamo. Il nostro appetito viene influenzato sia dal modo in cui chiamiamo ci&ograve; che stiamo mangiando, che dal modo in cui ci viene presentato&rdquo;.</h4>

<p><img alt="" class="wp-image-1911" src="/app/uploads/2016/12/OCCHI_7_MAIALE_0.jpg" style="width: 850px; " /></p>

<p>Il paradosso diventa ancora pi&ugrave; evidente nelle&nbsp;<a href="https://animalequality.it/blog/lorganizzazione-mondiale-della-sanita-dichiara-che-la-carne-lavorata-e-cancerogena/"><strong>societ&agrave; occidentali, dove le autorit&agrave; raccomandano la riduzione del consumo di carne perch&eacute; esso &egrave; di fatto un problema per la salute pubblica</strong></a>. Mangiamo cos&igrave; tanta carne da ammalarci,&nbsp;nonostante&nbsp;nella nostra societ&agrave; <strong><a href="https://www.facebook.com/AnimalEqualityItalia/videos/1184560981583613/">le alternative al consumo di carne sono sempre pi&ugrave; facili da trovare in tutti i supermercati</a>.</strong>&nbsp;&Egrave;&nbsp;logico aspettarsi che con il passare del tempo questi prodotti risultino competitivi agli occhi dei consumatori tanto per il loro prezzo quanto per il loro sapore.&nbsp;</p>

<p>Nel frattempo, l&rsquo;industria della carne approfitta del fenomeno psicologico del &#39;paradosso della carne&#39;.&nbsp;Tutta la carne processata o insaccata aiuta i consumatori a prendere distanza <strong>dall&rsquo;animale da cui essa proviene</strong>. Se serve poi, sono pronte campagne di marketing fra le pi&ugrave; sofisticate mai elaborate con le quali vengono ritratti degli animali felici assolutamente diversi da quello che mostrano le <a href="https://www.youtube.com/watch?v=FNWZsSp9YSk">immagini raccolte durante le investigazioni negli allevamenti intensivi</a> da cui provengono.</p>

<p><img alt="" class="wp-image-1912" src="/app/uploads/2016/12/OCCHI_8_MAIALE_0.jpg" style="width: 850px; " /></p>

<p><br />
In uno degli studi ad esempio, Kunst e gli altri investigatori hanno dimostrato come i partecipanti <strong>provino fastidio</strong> nel mangiare un maiale arrosto con ancora la testa attaccata al resto del corpo, mentre il fastidio e la resistenza a mangiarlo diminuivano quando la testa veniva staccata.</p>

<p>Viviamo, insomma, un paradosso complesso: <strong><a href="https://it.loveveg.com/">amiamo gli animali, ma siamo disposti a cibarcene</a></strong>. La maggior parte di noi non avrebbe mai il coraggio di infliggere in maniera diretta della violenza nei loro confronti.&nbsp;Tuttavia in pochi riescono a collegare una salsiccia alle immagini di un <a href="https://www.youtube.com/watch?v=NupGMf0z_Vs"><strong>maiale sofferente</strong></a>&nbsp;o del latte ad <strong><a href="https://animalequality.it/alimentazione/latte">una mucca privata del proprio vitello subito dopo il parto</a></strong>.</p>

<p>La maggior parte di noi riesce a provare empatia solo per gli animali interi&nbsp;poich&eacute;, una volta a pezzi, l&rsquo;industria della carne ed il suo marketing fanno leva su dei meccanismi psicologici che ci ingannano o semplicemente distolgono l&rsquo;attenzione dall&rsquo;animale da cui quella carne proviene.</p>

<p>Tutto questo &egrave; frutto di studi di marketing realizzati coscientemente, il cui preciso scopo &egrave; farci sentire il meno vicini possibile alla sofferenza a cui milioni di animali vengono sottoposti ogni anno per via delle nostre routine alimentari.</p>

<p><img alt="" class="wp-image-1913" src="/app/uploads/2016/12/animal_equality_empatia.jpg" style="width: 850px; " /></p>

<h4>Cercare e trovare la verit&agrave; &egrave; molto pi&ugrave; semplice di quello che si pensi, e quando la si trova non si pu&ograve; pi&ugrave; tornare a far finta di niente.</h4>

<p>Ed &egrave; proprio per questo motivo che esistono interi dipartimenti di marketing <strong>pronti a vendervi una storia che &egrave; tutto tranne che reale</strong> e persone che invece sono disposte a rischiare tutto quello che hanno per mostrarvi la vera storia del cibo che consumiamo.</p>

<p>&nbsp;</p>

<h4><img alt="" class="wp-image-1914" src="/app/uploads/2016/12/1_sfida_animalismo_21secolo.jpg" style="width: 850px; " /></h4>


<h4>La prossima volta che vi troverete in un supermercato la scelta sar&agrave; nelle vostre mani.</h4>

<p>La vostra lista della spesa &egrave; un&rsquo;arma molto potente: &egrave; la <strong>vostra dichiarazione in merito al mondo che desiderate</strong>.</p>

<p>Scegliete di dare ascolto al vostro cuore.&nbsp;Ne beneficeranno gli animali, l&rsquo;ecosistema e perfino la vostra salute.</p>

