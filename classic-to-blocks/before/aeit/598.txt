<div class="center"><img class="wp-image-1773" src="/app/uploads/2016/10/12_ae_img.jpg" /></div>
Per questo, ogni giorno gli <strong>investigatori di Animal Equality</strong> fanno quello che avrebbero preferito evitare: entrano in quel mondo fatto di sofferenza e crudeltà e documentano attraverso le telecamere (ed i propri occhi) quello a cui preferirebbero non assistere.

Non è una cosa semplice, sia dal punto di vista pratico che, soprattutto, dal punto di vista <strong>emotivo</strong>.

<strong>Ma qualcuno lo deve fare</strong>: mostrare la sofferenza costante di queste creature è il modo più diretto che abbiamo per <strong>smascherare l'industria della carne</strong>.

Parallelamente, è anche il più potente mezzo per <strong>scatenare domande</strong> e <strong>riflessioni</strong> ed iniziare a far <strong>germogliare empatia verso gli animali</strong>.

Di fronte a queste immagini, <strong>rimanere indifferenti è impossibile</strong>. Tu ad esempio, ti ricordi qual è stata la scintilla che ha acceso in te il fuoco della compassione?

Ecco perchè importante mostrare quello che nessuno vuole far vedere: <strong>perchè tutta questa violenza cessi il prima possibile.</strong>

Rimani al fianco degli investigatori di Animal Equality e non lasciare da soli gli animali.

Come ti renderai conto dalla lista qui sotto, questi animali hanno bisogno di aiuto. Il tuo.

Ogni vita va tutelata e tu puoi aiutarci a fare la differenza.
<h4>Quello che nessuno ti vuole raccontare</h4>
Quello a cui stai per assistere è la routine all'interno degli allevamenti.

Benchè i nostri investigatori abbiano documentato violenze di ogni tipo, abbiamo volutamente scelto le immagini meno cruente.
<h4>Taglio della coda (senza anestesia)</h4>
<img class="wp-image-1763" src="/app/uploads/2016/10/taglio-coda.jpg" alt="" />

Benché illegale in molti stati, il taglio della coda rimane una pratica ancora molto diffusa nella maggioranza degli allevamenti. È un'operazione che viene eseguita <strong>senza alcun tipo di anestetico</strong>, con un taglio che va a <strong>recidere nervi, pelle ed ossa</strong> di animali ancora giovanissimi.
<h4>Decornazione o rimozione delle corna</h4>
<img class="wp-image-1764" src="/app/uploads/2016/10/dehorning.jpg" alt="" />

<em>Screenshot tratto da un filmato PETA.</em>

La decornazione è una pratica comune negli allevamenti dell'industria casearia: gli allevatori rimuovono le corna dell'animale bruciandole o tagliandole. Viene giustificata da motivi di sicurezza per gli addetti e dalla riduzione dei rischi di lesione fra gli animali. Viene effettuata in circa l'<strong>80% degli allevamenti,</strong> mentre la rimozione degli abbozzi cornei nei vitelli, effettuata generalmente intorno al trentaduesimo giorno di vita, è effettuata praticamente in ogni allevamento. La cauterizzazione della ferita viene effettuata con un <strong>ferro arroventato in 9 casi su 10</strong>.*
<h4>Alimentazione forzata</h4>
<h4><img class="wp-image-1765" src="/app/uploads/2016/10/alimentazione-forzata.jpg" alt="" /></h4>
L'alimentazione forzata è una crudele pratica che avviene più volte al giorno nell'industria del foie gras. Avviene tramite un tubo metallico inserito in bocca agli animali e che a volte raggiunge direttamente lo stomaco delle oche o delle anatre. Serve ad indurre la <strong>crescita innaturale ed abnorme del fegato</strong> e l'<strong>aumento di grassi nelle cellule epatiche noto come steatosi, </strong>pratiche<strong> </strong>necessarie per la produzione del foie gras.
<div align="center">

<a href="https://campaigns.animalequality.it/sto-gli-investigatori-animal-equality/"><img class="alignnone wp-image-1781" style="width: 487px; " src="/app/uploads/2016/11/SOSTIENI_INVESTIGATORI.jpg" alt="" width="487" height="67" /></a>

</div>
<h4>Rimozione dei denti</h4>
<img class="wp-image-1767" src="/app/uploads/2016/10/taglio-denti.jpg" alt="" />

È pratica diffusa all'interno degli allevamenti di maiali quella della rimozione dei denti incisivi. Questa rimozione avviene nella maggior parte dei casi <strong>senza l'utilizzo di alcun tipo di anestetico</strong>.
<h4>Uccisioni cruente</h4>
<h4><img class="wp-image-1766" src="/app/uploads/2016/10/uccisioni-violente.jpg" alt="" /></h4>
I maiali malati o che non crescono abbastanza in fretta vengono uccisi con metodi cruenti. Spesso, ad esempio, gli individui adulti vengono<strong> percossi con oggetti contundenti</strong> provocando loro forti traumi cranici, mentre i cuccioli vengono ripetutamente e violentemente sbattuti di testa contro il suolo. Gli animali che non muoiono sul colpo vengono lasciati agonizzare.
<h4>Debeccaggio</h4>
<img class="wp-image-1768" src="/app/uploads/2016/10/taglio-del-becco.jpg" alt="" />

<em>Debeccaggio effettuato con lama arroventata. Fonte GaryTV</em>

Per debeccaggio si intende la rimozione della punta del becco per mezzo di diversi sistemi. Il becco è un <strong>organo estremamente sensibile</strong> del volatile, ricco di <strong>terminazioni nervose e recettori sensoriali</strong>. Questa pratica viene effettuata con i metodi più differenti, da un alto voltaggio elettrico al raggio laser, ma la procedura più utilizzata è una lama arroventata tra i 650° ed i 750°.

Il debeccaggio viene giustificato dal rischio di cannibalismo, di lesioni ed autolesioni a cui gli animali sono soggetti a causa delle forti condizioni di stress a cui sono sottoposti negli allevamenti intensivi.
<div align="center">

<a href="https://campaigns.animalequality.it/sto-gli-investigatori-animal-equality/"><img class="alignnone wp-image-1781" style="width: 487px; " src="/app/uploads/2016/11/SOSTIENI_INVESTIGATORI.jpg" alt="" width="487" height="67" /></a>

</div>
<h4>Castrazione</h4>
<img class="wp-image-1769" src="/app/uploads/2016/10/castrazione_senza_anestesia.jpg" alt="" />

Poco dopo la nascita, i cuccioli di maiale vengono castrati dagli allevatori <strong>senza alcun tipo di anestesia</strong>. L'operazione avviene tramite un coltello o un bisturi con il quale l'allevatore estrae i testicoli del piccolo maiale. Secondo la normativa europea, quella quindi in vigore anche in Italia, se eseguita entro il settimo giorno di vita dell'animale, questa pratica può essere effettuata da un semplice operaio e non richiede nemmeno la presenza di un veterinario.
<h4>Separazione forzata dai propri figli</h4>
<h4><img class="wp-image-1778" style="width: 850px; " src="/app/uploads/2016/11/separazione_famiglie_0.jpg" alt="" /></h4>
Negli allevamenti odierni, i cuccioli vengono separati dalla madre <strong>poco dopo il parto</strong>. Questo comporta un enorme carico di dolore sia per la madre che per il cucciolo.
<h4>Spazi vitali insufficienti</h4>
<h4><img class="wp-image-1780" style="width: 850px; " src="/app/uploads/2016/11/extreme-confinement_0.jpg" alt="" /></h4>
Al fine di massimizzare i propri profitti, l'industria concede agli animali il <strong>minor spazio fisico possibile</strong>. Gli animali vengono privati dello spazio necessario per soddisfare bisogni primari come quello di muoversi, e privati spesso di qualsiasi possibilità di movimento, probabilmente vedranno la luce del sole solamente il giorno in cui verranno trasportati al macello.
<h4>Triturati vivi</h4>
<h4><img class="wp-image-1779" style="width: 850px; " src="/app/uploads/2016/11/macerators_0.jpg" alt="" /></h4>
<em>Pulcini lanciati a mano all'interno dei macchinari che li triteranno vivi. Immagine di M.F.A.</em>

Poiché i pulcini maschi non depongono uova e non cresceranno abbastanza per fornire un quantitativo di carne che ne gistifichi l'investimento economico, la maggior parte di loro viene uccisa <strong>nel giro di un'ora dalla nascita</strong>. Quando non vengono uccisi con il gas, è molto probabile che questi animali vengano lanciati in macchinari che li <strong>triteranno vivi</strong>.
<div align="center">

<a href="https://campaigns.animalequality.it/sto-gli-investigatori-animal-equality/"><img class="alignnone wp-image-1781" style="width: 487px; " src="/app/uploads/2016/11/SOSTIENI_INVESTIGATORI.jpg" alt="" width="487" height="67" /></a>

</div>
NOTE:
* In un <a href="https://vetjournal.it/it/item/409-decornazione-dei-vitelli-pratiche-e-opinioni-in-italia.html">sondaggio italiano somministrato a più di 600 allevatori del Nord-Est</a>, il 74% ha dichiarato di non avere nessun tipo di difficoltà a gestire animali ancora in possesso delle proprie corna.
