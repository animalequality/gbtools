L’operatore di un allevamento industriale di suini <strong>prende tra le mani un maialino</strong>. Sua madre <strong>non può far altro che guardare impotente, dalla gabbia</strong>. L’operatore apre la bocca del cucciolo e <strong>gli taglia i denti</strong> con delle pinze. Lui <strong>grida di dolore</strong>. Questo accade ad <strong>ogni maialino</strong> appena nato nell’allevamento.

Senza dubbio <strong>crudele</strong>, ma del tutto <strong>legale</strong>.

<strong>L’industria della carne</strong> ha una politica poco chiara sulle pratiche sistematiche come questa. È consapevole che la società non accetterebbe la crudeltà sugli animali e quindi <strong>li mostra tutti felici su prati verdi e soleggiati.</strong> Niente di più lontano dalla realtà.

Ecco cosa ci viene nascosto:

<strong>Il taglio dei denti senza anestesia</strong>

<img class="wp-image-1415" style="width: 800px;" src="/app/uploads/2016/02/3785099854_64b4d84dba_b.jpg" alt="" />

<strong>Il taglio della coda senza anestesia</strong>

<img class="wp-image-1416" style="width: 800px;" src="/app/uploads/2016/02/3784969718_6e1e2d7f41_b.jpg" alt="" />

<strong>La castrazione senza anestesia</strong>

<img class="wp-image-1417" style="width: 800px;" src="/app/uploads/2016/02/3786954643_741645077f_b.jpg" alt="" />

<strong>Gabbie in cui le madri non riescono nemmeno a girarsi</strong>

<strong>La separazione dei vitelli dalle loro madri a poche ore dalla nascita</strong>

<strong><img style="width: 800px;" src="https://weanimalsmedia.org/wp-content/gallery/factory-farming-dairy-and-veal/DairyandVealFarm_Spain_JMcArthur_2010-0152.jpg" alt="" /></strong>

<strong>Vitelli tenuti in piccoli capanni e poi inviati al macello</strong>

<strong>Polli geneticamente selezionati per crescere in modo così rapido da non reggersi in piedi</strong>

<strong><img class="wp-image-1419" style="width: 800px;" src="/app/uploads/2016/02/pollo.jpg" alt="" /></strong>

<strong>Morti diffuse per mancanza di cure veterinarie individuali</strong>

<img style="width: 800px;" src="https://www.mirror.co.uk/incoming/article1338609.ece/ALTERNATES/s1227b/Sick%20and%20deformed%20chickens%20suffering%20inside%20a%20chicken%20factory%20farm" alt="" />

<strong>Il trasporto disumano fino al mattatoio senza cibo né acqua</strong>

<img class="wp-image-1420" style="width: 800px;" src="/app/uploads/2016/02/4618240360_2477d96ba1_b.jpg" alt="" />

<strong>Lo sgozzamento a testa in giù al macello</strong>
