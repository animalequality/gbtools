<div class="center"><img class="wp-image-2094" src="/app/uploads/2017/04/Carrefour_news.jpg" /></div>
<p>Carrefour Italia fa chiarezza sulla propria fornitura di uova e <a href="http://www.carrefour.it/carrefour-italia-stop-alla-vendita-di-uova-da-galline-gabbia#.WPWzi1OGOcZ">pubblica</a> un chiaro impegno ad abbandonare quelle prodotte in gabbia in tutti i propri punti vendita, inclusi quelli in franchising. Questa decisione arriva in seguito del dialogo con Animal Equality ed altre organizzazioni, in particolare una campagna diretta dall&rsquo;organizzazione Compassion In World Farming (CIWF), e avr&agrave; un impatto su centinaia di migliaia di galline ogni anno.&nbsp;</p>

<p>Nel corso del mese di marzo, Carrefour aveva <a href="https://www.ansa.it/canale_terraegusto/notizie/mondo_agricolo/2017/03/01/carrefour-italia-stop-a-vendita-uova-da-galline-in-gabbia_ccc1b33f-d1b1-4db4-979c-1a4f1fb5d31a.html">annunciato</a> che avrebbe smesso di commercializzare uova provenienti da allevamenti in gabbia, ma senza chiarire se questo impegno coinvolgesse anche i punti vendita non a gestione diretta. La crudelt&agrave; subita dalle galline negli allevamenti in gabbia, documentata nella nostra scioccante <a href="https://animalequality.it/news/2017/02/28/il-vero-prezzo-delle-uova-la-nuova-scioccante-investigazione-di-animal-equality-italia/">investigazione</a>, ha portato le organizzazioni impegnate con l&rsquo;azienda a non accettare l&rsquo;ambiguit&agrave; della politica, chiedendo a gran voce pi&ugrave; chiarezza e trasparenza.&nbsp;</p>

<p><img alt="" class="wp-image-2095" src="/app/uploads/2017/04/Carrefour_news_0.jpg" style="width: 850px; " /></p>

<p>La chiarezza arriva con la pubblicazione di suddetta politica sul sito web di Carrefour Italia e l&rsquo;esplicita dichiarazione che &ldquo;dal 1&deg; marzo 2017 tutti i 477 punti vendita a gestione diretta (con le insegne Carrefour, Market, Express, DocksMarket e GrossIper) non commercializzano pi&ugrave; uova prodotte da galline allevate in gabbia&rdquo;, e che &ldquo;Carrefour Italia prevede, nell&#39;arco di 30 mesi, che tutti i punti vendita in franchising siano completamente allineati&rdquo;.&nbsp;</p>

<p>&nbsp;</p>

<p>Non tutte le aziende si dimostrano disponibili a far uscire le galline dalle gabbie, e per convincere le pi&ugrave; reticenti ci serve il tuo aiuto: iscriviti al nostro team su <a href="https://animalequality.it/difensori-animali/">iodifendoglianimali.it</a> e in pochi minuti a settimana potrai fare la differenza per moltissimi animali!</p>

<p>&nbsp;</p>

<h4>Sia chiaro, abbandonare le gabbie non significa abbandonare la crudelt&agrave;. Il modo migliore per aiutare le galline &egrave; eliminare le uova dalla tua dieta: clicca <a href="https://www.vegolosi.it/glossario/come-sostituire-le-uova/">qui</a> per scoprire quanto &egrave; facile.&nbsp;</h4>

