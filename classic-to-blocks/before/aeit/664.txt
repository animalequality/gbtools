<p>Anche quest&rsquo;anno Animal Equality Italia d&agrave; il via alla campagna <a href="http://www.salvaunagnello.com"><b>#SalvaunAgnello</b></a> che, come di consueto, invita a non consumare carne di agnello e di altri cuccioli in occasione della Pasqua, suggerendo allo stesso tempo di considerare un tipo di alimentazione cruelty-free anche per il resto dell&rsquo;anno.&nbsp;Testimonial d&rsquo;eccezione - e eccezionale - per il video ufficiale della campagna 2017 &egrave; l&rsquo;attore <strong>Tullio Solenghi</strong> che negli ultimi anni &egrave; impegnato personalmente, insieme alla moglie <strong>Laura Fiandra</strong>, nel sostenere iniziative volte a diffondere abitudini di vita pi&ugrave; compassionevoli.&nbsp;<br />
<br />
<iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/Px1qzfz20dM" width="850"></iframe><br />
&nbsp;</p>

<p><br />
La prima campagna di sensibilizzazione di Animal Equality su questo tema &egrave; partita nel 2013 con lo slogan &quot;<b>Questa Pasqua Salva un Agnello</b>&quot;, accompagnato sui social dall&#39;hashtag <b>#Salvaunagnello</b>, arrivando al quinto anno consecutivo per <strong><a href="https://animalequality.it/blog/buone-notizie-gli-agnelli/">contrastare la terribile uccisione di agnelli e capretti che caratterizza purtroppo la festivit&agrave; religiosa della Pasqua</a></strong>.&nbsp;</p>

<p>Negli ultimi anni, anche per rafforzare questa campagna e rendere pi&ugrave; efficace la risposta dell&rsquo;opinione pubblica, <b>Animal Equality Italia ha reso noti</b> i risultati sconvolgenti di <b>diverse investigazioni svolte all&rsquo;interno del mercato della carne di agnello</b>, negli allevamenti e nei mattatoi, compreso il trasporto di milioni di cuccioli che si svolge in un lasso di tempo produttivo molto concentrato. Le immagini e i video arrivati attraverso i media nazionali &ndash; e internazionali - nelle case degli italiani, hanno mostrato agnelli strappati alle loro madri al momento della nascita, il loro trasporto che spesso dura migliaia di chilometri, i maltrattamenti subiti durante il tragitto e all&rsquo;arrivo al mattatoio, la loro macellazione che troppo spesso avviene con gli animali ancora coscienti.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-2075" src="/app/uploads/2017/03/Tullio-ritoccata-orizz_site.jpg" /></p>

<p><br />
Nel 2013 si macellavano circa 4 milioni di agnelli, per passare nel <a href="https://animalequality.it/blog/buone-notizie-gli-agnelli/">2016 a poco pi&ugrave; di 2 milioni di capi</a>, risultato dei cambiamenti di abitudini di 7 famiglie su 10 che hanno scelto di non includere la carne di agnello nella loro tavola in occasione della Pasqua.&nbsp;</p>

<p>Per continuare l&rsquo;azione di sensibilizzazione, <b>Animal Equality Italia ha prodotto per il 2017, in collaborazione con <a href="https://www.veggiechannel.com/">Veggie Channel</a>, lo studio di posa <a href="https://www.instudio.org/">Instudio</a> e il rifugio <a href="https://www.facebook.com/thegreenplace.org/">The Greenplace</a>, un video ufficiale della campagna pi&ugrave; corale del solito.</b> Insieme a <b>Tullio Solenghi e Cesarina (l&rsquo;agnellina nelle sue braccia)</b>, infatti, sono sfilati sotto la telecamera e i riflettori <b>bambini e bambine che</b>, abbracciando l&#39;agnellina, <b>hanno voluto inviare dei messaggi semplici ma fondamentali</b>. Ve li lasciamo scoprire direttamente cliccando play sul video.&nbsp;<br />
<br />
Il commento di&nbsp;<b>Tullio Solenghi </b>sulla campagna 2017?&nbsp;Eccolo:</p>

<h4>&ldquo;<i>Ringrazio Animal Equality per avermi dato l&#39;opportunit&agrave; di tenere in braccio un cucciolo, con tutte le emozioni e le tenerezze che questo atto genera. Dico non a caso cucciolo, anche se so bene che si tratta di un agnellino, ad evitare discriminazioni che rendono alcuni cuccioli degni del nostro affetto e della nostra attenzione, altri invece brutale carne da macello. Per me &egrave; stato come se tenessi ancora in braccio quel cucciolone del mio labrador Jocker, che ha colorato con la sua felicit&agrave; tanti anni della mia vita&rdquo;</i><b><i>.</i></b></h4>

<p>&nbsp;</p>

<p>Cesarina &egrave; invece un&rsquo;agnellina ospite del rifugio di Nepi per animali salvati dai mattatoi ed &egrave; stata intrattenuta nello studio per pochi minuti, il tempo strettamente necessario per le riprese e gli scatti.</p>

<p><br />
<br />
<br />
Ci teniamo a ringraziare quanti tutti coloro che hanno collaborato alla realizzazione del video:</p>

<p>Sceneggiatura:&nbsp; Massimo Leopardi</p>

<p>Regia:&nbsp; Massimo Leopardi e Julia Ovchinnikova</p>

<p>Testi:&nbsp; Luca de Bei</p>

<p>Operatore:&nbsp; Alessandro Iafulla</p>

<p>Editing:&nbsp; Micaela Della Siria</p>

<p>Musiche:&nbsp; Rosario Di Bella</p>

<p>Pubbliche relazioni:&nbsp; Mauro Cristiano</p>

<p>Assistenti: Marina Kodros, Arianna fioravanti, Giada Prada, Giada Carlucci,&nbsp;</p>

<p>Studio di posa: Instudio</p>

<p>Credito ph. Gianluca Roselli/Instudio</p>

