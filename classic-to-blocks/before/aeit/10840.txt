<div class="center"><img class="wp-image-1306" src="/app/uploads/2015/06/thumbnail_lili.jpg" /></div>
<p class="p1"><strong>Lili &egrave; stanca e sa di non avere nessuna speranza... </strong>la vita di una&#39;anatra&nbsp;rinchiusa in uno dei tanti, troppi, capannoni da ingrasso degli allevamenti intensivi &egrave; piena di abusi e di abbandono. Questo &egrave; il motivo per il quale le storie di Lili e di tanti altri milioni di animali hanno bisogno di essere ascoltate.</p>

<div class="media_embed" height="478px" width="850px"><iframe allowfullscreen="" frameborder="0" height="478px" src="https://www.youtube.com/embed/yKZPFkKb9YY" width="850px"></iframe></div>

<p><strong>Lili &egrave; nata in un mondo in cui animali dolci e adorabili come lei sono visti solo come prodotti</strong>. A Lili &egrave; stato negato ogni minimo desiderio o bisogno. Lei non potr&agrave; mai nuotare in un lago o avere una famiglia. Lili non potr&agrave; mai immergere le sue piccole zampette in acqua o circondare i suoi figli con le sue ali protettive.&nbsp;</p>

<p class="p2"><strong>L&#39;innocente Lili &egrave; nata per essere torturata</strong>. L&#39;industria della carne vuole che prenda pi&ugrave; peso nel minor tempo possibile. La sua carne sar&agrave; poi venduta nei supermercati.</p>

<p class="p2"><strong>Il tuo aiuto &egrave; fondamentale</strong> per porre fine alle pratiche crudeli che avvengono nel segreto di quelle porte chiuse. Aiutala ad essere l&#39;ambasciatrice di tutti gli animali che soffrono negli allevamenti moderni, condividendo oggi la sua storia.</p>

