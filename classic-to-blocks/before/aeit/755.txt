<div class="center"><img class="wp-image-2433" src="/app/uploads/2018/05/butterfly-1976737_640.jpg" /></div>
La biodiversità è definita “la ricchezza della vita”. Sì, perché con questo termine si intende la varietà degli esseri viventi che abitano la Terra, che essi siano batteri o grandi carnivori, tutti fanno parte della “biodiversità”.

La biodiversità - celebrata proprio oggi - si misura partendo dai geni, dalle specie, dalle popolazioni e dagli ecosistemi, tutti elementi correlati tra di loro che danno origine alla varietà di esseri viventi presenti sulla Terra e che rendono possibile la vita come la conosciamo.

È grazie alla biodiversità infatti che anche l’uomo è in grado di rifornirsi di cibo, acqua, energia e risorse per la vita quotidiana, interagendo quindi costantemente con tutti gli altri esseri viventi presenti sul nostro Pianeta.

<img class="wp-image-2429" style="width: 840px; " src="/app/uploads/2018/05/H1KSJ04BVD.jpg" alt="" />

<a href="http://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001127#s2">Secondo le stime scientifiche più recenti</a> e accurate, sarebbero oltre 7,7 milioni le specie animali sul pianeta terra. Finora ne sono state classificate solo 1,2 milioni. Questo significa che, persino dopo un secolo di sforzi, l'86% delle specie terrestri resta ancora da classificare!

Inoltre, vi sono alcuni ambienti che sono particolarmente ricchi di biodiversità, come le barriere coralline, le foreste tropicali e gli estuari dei fiumi, che ospitano circa la metà degli esseri viventi del Pianeta, anche se ricoprono solo il 6% della superficie terrestre.

<b>Ma queste enormi risorse stanno scomparendo, e tra le cause principali c’è proprio l’allevamento intensivo</b>.

Consumo del suolo, cambiamenti climatici e inquinamento infatti sono tutti elementi direttamente correlati al consumo vertiginoso di carne e quindi all’impatto che questi enormi agglomerati di animali da reddito hanno sul nostro ecosistema.

<b>Per quanto riguarda il consumo del suolo, il problema è da imputare alla coltivazione di cereali destinati al consumo animale</b>. Da un lato infatti vi è il tragico disboscamento delle foreste - come sta accadendo in Sud America, in Brasile in particolare, con le immense piantagioni di soia destinate agli allevamenti intensivi - e dall’altro la modalità iper-performante con cui sono state selezionate tutte le nuove varietà per la produzione di mangimi. Queste coltivazioni sono <a href="https://www.lifegate.it/persone/stile-di-vita/biodiversita">rese sempre più resistenti all’uso massiccio di pesticidi chimici che finora hanno causato la sparizione di circa 300.000 varietà tradizionali</a>.

<img class="wp-image-2434" style="width: 840px; " src="/app/uploads/2018/05/deforestation-405749_1920.jpg" alt="" />

<b>Altro discorso invece è quello che lega l’inquinamento agli allevamenti </b>e quindi alla perdita di biodiversità.

Negli Stati Uniti in particolare si è osservato un fenomeno molto comune, e cioè la dispersione di tossine e componenti chimici che arrivano dagli allevamenti e in generale dai terreni agricoli in torrenti, fiumi e oceani. Questa dispersione porta alla crescita di alghe che sono in grado di soffocare le altre specie marine, perché causano un fenomeno chiamato ”ipossia”, ovvero mancanza di ossigeno, portando la fauna marina alla morte o spingendola a cercare nuovi ecosistemi più accoglienti.

Secondo alcune ong americane, come Mighty Earth, <a href="https://www.theguardian.com/environment/2017/aug/01/meat-industry-dead-zone-gulf-of-mexico-environment-pollution">questo fenomeno avrebbe causato la formazione delle cosiddette “zone morte” nel Golfo del Messico</a>, aree in cui infatti flora e fauna sono scomparse.

<img class="wp-image-2431" style="width: 840px; " src="/app/uploads/2018/05/water1.jpg" alt="" />

E sempre secondo Mighty Earth,<b> </b><a href="https://www.businessinsider.com/eating-meat-affects-environment-dead-zone-2018-4?IR=T"><b>le più grosse aziende produttrici di carne sarebbero responsabili anche di altre forme di inquinamento</b></a><b> che distrugge la biodiversità</b>, come la produzione di letame e di inquinanti collegati alla produzione di nitrato, che deriva proprio dalla dispersione del letame all’interno delle acque.

<b>Tutto questo avviene anche in Italia, dove</b> - secondo Ispra, l’istituto superiore per la protezione e la ricerca ambientale - <a href="https://guidominciotti.blog.ilsole24ore.com/2018/05/22/oggi-giornata-della-biodiversita-ma-la-sesta-estinzione-di-massa-e-gia-iniziata/"><b>il 31% degli animali vertebrati e la metà delle specie vegetali sono a rischio estinzione</b></a>, perché gli sforzi internazionali di tutela “stanno fallendo miseramente”.

Entro il 2020, secondo il Piano strategico sulla biodiversità, vanno centrati 20 obiettivi – dal dimezzamento della perdita di habitat alla gestione sostenibile di pesca e agricoltura, fino alla creazione di aree protette dedicate ad animali e piante.

Ovviamente, l’impatto dell’essere umano e l’aumento demografico hanno un peso notevole in questo processo, ma se da un lato si può fare poco per diminuire la crescita della popolazione, è anche vero che qualcosa si può fare agendo sulla propria alimentazione.

Spesso l’aumento demografico è associato all’aumento dei consumi di carne, che dopo una flessione degli ultimi anni sta registrando una nuova crescita dovuta all’aumento della richiesta in paesi altamente popolati come l’India e la Cina.

Ma scegliendo di tenere fuori dal proprio piatto carne e derivati si aiuta l’ambiente e si può evitare di contribuire alla distruzione di questo preziosissimo patrimonio: la biodiversità del nostro pianeta Terra.

<img class="wp-image-2435" style="width: 840px; " src="/app/uploads/2018/05/bambino.jpg" alt="" />
