Dopo aver catturato l'attenzione pubblica per aver fondato la <a href="https://www.happyhippies.org/#our-work">Happy Hippie Foundation</a>, in aiuto della comunità di giovani senzatetto LGBT, Miley Cyrus amplia il suo cerchio di compassione e affronta la questione del maltrattamento degli animali su un <a href="http://www.papermag.com/2015/06/miley_cyrus_happy_hippie_foundation.php">numero della rivista Paper</a>.

<em>"Cyrus inizia quasi subito a raccontarci di come, lo scorso anno, abbia deciso di diventare vegan”</em>, si legge nell'articolo. <em>"Stava facendo un giro del mondo a sostegno del suo album “Bangerz”</em>, disco di platino nel 2013, quando il suo amato cane, Floyd, un Alaskan Klee Kai, venne sbranato da un coyote.

Smise quasi immediatamente di consumare prodotti di origine animale. Non ha parlato molto di quale riflessione l'abbia condotta a questa scelta, ma ha affermato di voler essere un esempio per tutti i suoi fans, per fare del mondo un posto migliore”.

La vediamo qui, sulla copertina di Paper, mentre abbraccia un maiale.

La scelta di Miley è stata ispirata anche dall'artista e amica Joan Jett che come <em>"lei ama gli animali e non vuole mangiarli"</em>.

La cantante è sempre molto generosa nel condividere la sua vita e le sue scelte su internet, così spesso pubblicizza i suoi pasti vegetali su piattaforme come Instagram con didascalie simili a questa: <em>“Con così tanti vegetali, frutti e alimenti vegan, non abbiamo bisogno di mangiare animali morti! Ogni alimento che entra nel mio corpo è vivo! E mi tiene viva! Amo questa scelta!”</em>

<img class="wp-image-10599" style="width: 800px;" src="/app/uploads/2015/08/Screen-Shot-2015-06-11-at-12_54_58.png" alt="" />

Miley, 22 anni, è l'esempio di una tendenza crescente che porterà sempre più persone a intraprendere la scelta Vegan, l'unica strada vincente verso un mondo migliore! Siamo convinti che queste sue dichiarazioni indurranno molti altri ad adottare scelte alimentari e di vita più etiche, compassionevoli e di origine vegetale.
