<div class="center"><img class="wp-image-2016" src="/app/uploads/2017/02/LOGHI_fb.jpg" /></div>
<p>Il piano di <strong>#run4animals</strong> &egrave; semplice: ci alleniamo, ci divertiamo e raccogliamo fondi per un mondo senza gabbie partecipando alla <strong>Milano Marathon 2017 domenica 2 aprile</strong>.</p>

<p>Obiettivo di quest&#39;anno &egrave; raccogliere fondi destinati alla campagna per <strong>liberare gli animali dalle gabbie</strong>. Lo faremo attraverso <strong>nuove investigazioni</strong>, <strong>pressione alle aziende attraverso petizioni pubbliche</strong>, campagne di <strong>informazione pubblica</strong>, azioni di <strong>pressione sui decisori politici</strong>.</p>

<p><iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/lIIb38jdprM" width="850"></iframe></p>

<p>&nbsp;</p>

<p>Solo pochi giorni fa abbiamo gi&agrave; ottenuto questo importante risultato: <a href="https://animalequality.it/news/2017/01/26/allevamenti-di-conigli-gabbia-ue-abolizione-sempre-piu-vicina/"><strong>&egrave; sempre pi&ugrave; vicina nell&#39;UE l&#39;abolizione delle gabbie negli allevamenti di conigli</strong></a>. Un passo in avanti ma certamente non l&#39;ultimo. Per questo dobbiamo correre per gli animali!&nbsp;</p>

<p>La squadra di Animal Equality quest&#39;anno sar&agrave; di <strong>circa 40 persone</strong> che correranno la staffetta 4x10km: la foto di gruppo sar&agrave; molto pi&ugrave; affollata di quella dello scorso anno :).</p>

<h2>Abbiamo appena lanciato la campagna e abbiamo raccolto circa la met&agrave; dei <strong>Runners4Animals</strong>!<br />
<br />
Ma ci sono ancora dei posti liberi perci&ograve; se non ti spaventa correre per 10 km e <strong>hai a cuore gli animali</strong> <a href="http://run4animals.animalequality.it"><strong>contattaci attraverso la pagina della campagna: abbiamo bisogno di te</strong></a>!</h2>

<p><br />
Se vuoi ti possiamo<strong> inserire una delle squadre</strong> oppure<strong> puoi coinvolgere altre tre persone e fare la tua squadra!</strong></p>

<p>&nbsp;</p>

<p><a href="http://run4animals.animalequality.it"><img alt="" class="wp-image-2014" src="/app/uploads/2017/02/LOGHI_850.jpg" style="width: 850px; " /></a></p>

<p>&nbsp;</p>

<p>Partecipare &egrave; facile: ti manderemo tutte le informazioni via mail.</p>

<p>La partecipazione alla staffetta quest&#39;anno prevede un contributo a persona a partire da 40 euro che comprende:</p>

<ul>
	<li>
	<p>l&#39;iscrizione alla staffetta (pettorale e cronometraggio)&nbsp;</p>
	</li>
	<li>
	<p>il pacco gara della Milano Marathon (gadget vari e i vari prodotti degli sponsor dell&#39;evento)</p>
	</li>
	<li>
	<p>il ristoro durante la corsa e a fine gara</p>
	</li>
	<li>
	<p>la maglietta della Milano Marathon&nbsp;</p>
	</li>
	<li>
	<p>la splendida maglietta di #Run4Animals (stiamo definendo il lay out in questi giorni)</p>
	</li>
	<li>
	<p>e alcuni allenamenti insieme per prepararci al meglio<br />
	&nbsp;</p>
	</li>
</ul>

<p>La partecipazione di Animal Equality alla maratona &egrave; un&#39;importante occasione per raccogliere fondi per la <strong>campagna contro le gabbie</strong>, per questo ognuno di noi sar&agrave; testimonial degli animali e si far&agrave; promotore della raccolta.</p>

<p>Ogni Staffetta avr&agrave; come obiettivo di raggiungere i 1000 euro, circa 250 euro a testa.<br />
&nbsp;</p>

<p><img alt="" class="wp-image-2015" src="/app/uploads/2017/02/maratona2015.jpg" style="width: 850px; " /></p>

<p>Alcuni dei partecipanti alla Milano Maraton 2015 per Animal Equality</p>

<p>&nbsp;</p>


<h4>Non farti spaventare dai numeri&nbsp;lo scorso anno anche chi era pi&ugrave; restio &egrave; riuscito a raccogliere molto di pi&ugrave; perch&eacute; &egrave; stupefacente quante persone vogliano <strong>contribuire a dare un futuro diverso agli animali</strong>, un futuro <strong>libero dalle gabbie</strong>.&nbsp;</h4>

<p>&nbsp;</p>

<p>Se deciderai di partecipare a<strong> #run4animals</strong> ti accompagneremo passo passo e ti dar&ograve; tutti i contenuti, i consigli e le idee pi&ugrave; semplici ed efficaci per raggiungere insieme il nostro obiettivo: non rimarrai da solo!&nbsp;</p>

<p>Basta aprire un account (personale o della squadra) su Retedeldono alla pagina del nostro progetto, scrivere qualche riga di presentazione e poi condividere la <strong>pagina sui social</strong>, via <strong>mail</strong> e con i mezzi in cui lo ritieni pi&ugrave; opportuno!&nbsp;Nessuna preoccupazione: contribuire alla raccolta fondi <strong>&egrave; pi&ugrave; facile che prepararsi alla corsa</strong> :)</p>

<p><br />
Presto ti faremo sapere i primi appuntamenti per allenarci insieme in vista della Maratona.<br />
Ovviamente il team italiano&nbsp;di Animal Equality parteciper&agrave; al completo!&nbsp;:)</p>

<h2><a href="http://run4animals.animalequality.it">Il 2 aprile sar&agrave; un grande giorno perch&eacute; correremo insieme per dare voce agli animali!<br />
Pi&ugrave; saremo, pi&ugrave; persone riusciremo a coinvolgere, pi&ugrave; forte sar&agrave; la loro voce.</a></h2>

