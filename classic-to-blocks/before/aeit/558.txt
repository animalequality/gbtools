<div class="center"><img class="wp-image-1593" src="/app/uploads/2016/07/RIA_ANIMAL_EQUALITY_GERMANY_BEST_WOMAN_FB.jpg" /></div>
<p>Ria ha letteralmente sbaragliato una concorrenza fatta di circa<strong> altre 700 candidate</strong>, e grazie al <a href="https://editionf.com/Die-25-Frauen-die-unsere-Welt-besser-machen/">voto congiunto di community e giuria di Edition F</a>, il suo nome figura nella rosa delle 25 attiviste pi&ugrave; autorevoli ed ammirate in terra tedesca.<br />
<br />
<img alt="" class="wp-image-1590" src="/app/uploads/2016/07/RIA_ANIMAL_EQUALITY_GERMANY_BEST_WOMAN_2.jpg" style="width: 850px; " /></p>

<p>L&#39;<strong>impegno</strong> e la <strong>dedizione</strong>&nbsp;sono stati una costante nel suo lavoro in tutti questi anni.<br />
<br />
Scontrarsi tutti i giorni con la crudelt&agrave; dell&#39;industria della carne e la sofferenza che genera sugli animali non &egrave; affatto semplice, e per dover fronteggiare tutto questo sono necessari una&nbsp;<strong>forza</strong> ed un&nbsp;<strong>carattere </strong>non indifferenti.<br />
Due caratteristiche, appunto, che le lettrici tedesche hanno riconosciuto alla co-direttrice di Animal Equality Germania e per le quali l&#39;hanno voluta premiare.</p>

<p><img alt="" class="wp-image-1591" src="/app/uploads/2016/07/RIA_ANIMAL_EQUALITY_GERMANY_BEST_WOMAN_1.jpg" style="width: 850px; " /></p>

<p>In merito al premio della testata&nbsp;Edition F, Ria Rehberg ha affermato:</p>

<h4><em>&quot;Sono onorata di ricevere questo premio assieme ad un&#39;altra attivista per i diritti animali come <strong>Hilal Sezgin</strong> ed il fatto che le lettrici abbiano messo un cos&igrave; forte accento sulla <strong>questione animale</strong> mi fa molto piacere. Sempre pi&ugrave; donne anche in Germania scelgono la <strong>compassione</strong> e riducono - se non addirittura eliminano - il proprio consumo di carne a causa delle <strong>insopportabili condizioni </strong>degli animali all&#39;interno dell&#39;<strong>industria della carne</strong>.&quot;</em></h4>

<p><img alt="" class="wp-image-1592" src="/app/uploads/2016/07/RIA_ANIMAL_EQUALITY_GERMANY_BEST_WOMAN_3.jpg" style="width: 850px; " /></p>

<p>Un cambiamento molto evidente ora: un&#39;<a href="http://ec.europa.eu/public_opinion/archives/ebs/ebs_270_en.pdf"><strong>indagine statistica europea (pag.34)</strong></a>&nbsp;pubblicata di recente afferma infatti che <strong>il 94% dei cittadini Europei ritiene che il benessere animale sia una questione importante</strong>.</p>

<p>Sempre pi&ugrave; persone stanno riducendo il proprio consumo di carne, arrivando spesso ad tagliare fuori dalla propria vita qualsiasi prodotto di origine animale.<br />
Parallelamente, sugli scaffali dei supermercati troviamo sempre pi&ugrave; alternative alla carne, al pesce, alle uova ed ai latticini.</p>

<p>Cambiare le abitudini della societ&agrave; &egrave; possibile: ce lo dimostra la storia di Ria ed il fatto che un&#39;attivista per i diritti animali possa essere considerata fra le migliori 25 donne che, traducendo letteralmente Edition F,&nbsp;&quot;<em><strong>stanno migliorando il nostro mondo</strong></em>&quot;&hellip; ma il tutto parte dai piccoli gesti come scegliere cosa finisce nel nostro carrello o nel nostro piatto.<br />
&nbsp;</p>

