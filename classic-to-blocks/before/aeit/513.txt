<p>Come possiamo produrre cibo a sufficienza per <strong>nutrire una popolazione in crescita esponenziale</strong>? Come possiamo farlo <strong>senza distruggere il pianeta</strong>?<br />
Per scoprirlo, <a href="http://www.nature.com/ncomms/2016/160419/ncomms11382/full/ncomms11382.html">alcuni ricercatori hanno condotto centinaia di simulazioni</a>. Hanno confrontato, per esempio, la produzione biologica con quella ad alta resa, la produzione per un&rsquo;alimentazione a base vegetale rispetto ad una con alti consumi di carne.</p>

<p>Lo <a href="http://www.nature.com/ncomms/2016/160419/ncomms11382/full/ncomms11382.html">studio</a> ha misurato la sostenibilit&agrave; di varie combinazioni di alimentazione e strategie produttive. In altre parole, <strong>ha indagato se fosse possibile produrre abbastanza cibo per gli oltre 9 miliardi di persone sul pianeta</strong> stimati per il 2050 senza aumentare le aree coltivabili e quindi <strong>senza abbattere foreste</strong>.</p>

<p><img alt="" class="wp-image-1475" src="/app/uploads/2016/04/3274214937_5c456b5259_o.jpg" style="width: 800px;" /></p>

<p>Il team di ricercatori ha riportato che il tipo di alimentazione &egrave; stato il fattore che ha cambiato maggiormente le carte in tavola: <strong>qualsiasi modalit&agrave; di produzione &egrave; risultata sostenibile con una popolazione nutrita totalmente da alimenti vegetali. </strong><br />
Lo stesso non si pu&ograve; dire delle diete occidentali tipicamente ricche di carne: soltanto il 15% di loro &egrave; risultata sostenibile a causa della quantit&agrave; di terre che servono ad allevare gli animali per la carne.&nbsp;<strong>Nessun altro fattore ha avuto un impatto tanto grande quanto l&rsquo;alimentazione veg, nemmeno modificare le pratiche agricole e d&rsquo;allevamento.</strong></p>

<p><img alt="" src="https://static.pexels.com/photos/534/animal-countryside-agriculture-farm.jpg" style="width: 800px;" /><br />
Lo studio presuppone un commercio totalmente libero tra i vari paesi, e quindi non affronta il problema della distribuzione del cibo a livello di mercato.<br />
Tuttavia dimostra che in teoria &egrave; davvero possibile continuare a nutrire il pianeta senza che vengano distrutte foreste per fare spazio alle coltivazioni per gli animali da carne, soprattutto scegliendo un&#39;alimentazione a base vegetale.</p>

