<div class="center"><img class="wp-image-1236" src="/app/uploads/2014/03/8563657864_09c5c50c42.jpg" /></div>
<p>Rilanciamo la nostra campagna sapendo di poter contare su una sempre pi&ugrave; crescente opposizione alla crudelt&agrave; che colpisce questi cuccioli indifesi. Attraverso le nostre investigazioni sotto copertura abbiamo mostrato scene di violenza diffuse e non casi isolati.<br />
	<br />
	<strong><img alt="Immagine dell'investiazione sulla carne di agnello - 2013" src="https://farm9.staticflickr.com/8237/8581394359_f7c7c1cb84_n.jpg" style="width: 320px; border-width: 0px; border-style: solid; margin: 10px 5px; float: right;" />Agnelli lasciati morti</strong> per giorni all&#39;aperto, altri senza cure veterinarie e in stato di ipotermia. Abbiamo documentato la &#39;<strong>pesatura</strong>&#39;, una pratica ritenuta illegale; gli agnelli, terrorizzati, vengono legati, issati per i carpi (polsi) e pesati in gruppi.<br />
	<br />
	<strong>Nei macelli abbiamo trovato cuccioli stressati e spaventati</strong>, che si ammassano l&#39;uno sull&#39;altro urlando e rimanendo spesso impigliati nei recinti, ferendosi, nell&#39;attesa di essere uccisi.<br />
	<br />
	<strong>Tutta questa violenza pu&ograve; finire</strong>. In prossimit&agrave; della Pasqua dello scorso anno la riduzione degli ordini di carne d&#39;agnello &egrave; arrivata al 40% (secondo fonti della stessa industria ovina). &Eacute; oramai evidente quanto la crudelt&agrave; verso questi cuccioli stia colpendo l&rsquo;opinione pubblica.<br />
	<br />
	<strong>Durante le prossime settimane la nostra campagna &#39;Salva Un Agnello&#39; permetter&agrave; a sempre pi&ugrave; persone di conoscere cosa accade a questi cuccioli</strong>. Attraverso azioni pubbliche, campagne on line, immagini e filmati ci impegneremo insieme per ottenere anche di pi&ugrave; di quanto abbiamo potuto fare lo scorso anno. La possibilit&agrave; di cambiare il destino di questi animali &egrave; nelle nostre mani, si manifesta attraverso le nostre scelte.<br />
	<br />
	<strong>Durante le festivit&agrave; tradizionali come la Pasqua possiamo fare la differenza.</strong></p>

