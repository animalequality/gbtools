<div class="center"><img class="wp-image-1972" src="/app/uploads/2017/01/TRIPLICATO_VEGANI_ITALIA_EURISPES_3_RAPPORTO_1.jpg" /></div>
<p>&Egrave; appena stato pubblicato il <a href="http://www.eurispes.eu/content/eurispes-rapporto-italia-2017-tavola-gli-italiani-amano-il-made-italy-741-e-i-prodotti-di"><strong>Rapporto Eurispes 2017</strong></a> e contiene una bellissima notizia: il numero di persone che sono passate ad una dieta completamente a base vegetale &egrave; <strong>triplicato</strong> in un solo anno.</p>

<p>S&igrave;, avete letto bene, la fetta di popolazione che ha deciso di<strong> abbandonare i prodotti derivanti dallo sfruttamento animale</strong> &egrave; passata in un solo anno,&nbsp;nel nostro paese, dall&#39;1% al <strong>3%</strong>.</p>

<p>&nbsp;</p>

<h4>Significa che circa 1.800.000 italiani non mangiano n&egrave; carne n&egrave; derivati.</h4>


<p><img alt="" class="wp-image-1970" src="/app/uploads/2017/01/TRIPLICATO_VEGANI_ITALIA_EURISPES_3_RAPPORTO.jpg" /></p>

<p>&nbsp;</p>

<h4>&quot;Il 7,6% del campione segue una dieta vegetariana o vegana. In particolare, il 4,6% degli intervistati si dichiara vegetariano (-2,5% rispetto al 2016) mentre i vegani giungono il <strong>3%</strong> (erano l&rsquo;1%).&quot;</h4>

<p>Questo quanto si legge proprio sul sito del pi&ugrave; accreditati istituti di ricerca statistica del mondo.</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-1973" src="/app/uploads/2017/01/TRIPLICATO_VEGANI_ITALIA_EURISPES_3_RAPPORTO_1_0.jpg" style="width: 850px; " /></p>

<p>&nbsp;</p>

<p>Come possiamo interpretare questi numeri?</p>

<p>Se il numero dei vegani &egrave; in aumento, ma quello dei vegetariani &egrave; in calo, &egrave; molto probabile che buona parte di questi vegetariani abbiano deciso di <strong>abbandonare completamente</strong> i prodotti di origine animale.</p>

<p>Rimane comunque un dato positivo,&nbsp;soprattuto se si pensa al peso specifico che una dieta completamente a base vegetale ha sul pianeta e soprattutto sugli animali rinchiusi negli allevamenti intensivi.</p>

<p>&nbsp;</p>

<p>L&#39;Italia si riconferma uno dei paesi con le percentuali di vegetariani e vegani pi&ugrave; alte&nbsp;del mondo e questo non pu&ograve; far altro che motivarci ulteriormente nel nostro lavoro quotidiano.</p>

<p>&nbsp;</p>

