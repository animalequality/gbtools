<div class="center"><img class="wp-image-11302" src="/app/uploads/2016/11/little-miss-sunshine5.jpg" /></div>
<p>&iquest;Imaginas c&oacute;mo ser&iacute;a poder extender tus alas y recibir la luz directa del sol por primera vez? &iquest;Caminar libremente y vivir en un lugar donde seas respetado y cuidado como mereces hasta el final de tus d&iacute;as? Estos sencillos placeres que parecen tan comunes para nosotros resultan imposibles para los <a href="http://www.igualdadanimal.org/noticias/7616/el-es-el-animal-mas-abusado-del-planeta" target="_blank">pollos</a> y <a href="https://igualdadanimal.org/blog/la-historia-de-rosie-una-gallina-ponedora/" target="_blank">gallinas</a> que permanecen dentro de las granjas y mataderos.</p>

<p>Algunos de estos animales tuvieron la fortuna de ser liberados del sufrimiento que implica ser un producto m&aacute;s de <a href="https://igualdadanimal.org/noticia/2016/10/25/por-que-ganaderia-industrial-equivale-maltrato-animal/" target="_blank">la industria</a>. <strong>Personas compasivas los reconocieron como los&nbsp;maravillosos seres que son y decidieron darles la vida digna que merecen</strong>.</p>

<p>A continuaci&oacute;n te presentamos algunas de las historias de aves de granja que pudieron vivir en libertad por el resto de su vida.</p>

<p>&iexcl;Te encantar&aacute; conocerlas!</p>

<p>&nbsp;</p>

<strong>1. Las afortunadas gallinas y gallos de California</strong>

<p>8 pollos (cuatros gallinas y cuatro gallos) del tipo que la industria llama &ldquo;broilers&rdquo;, provenientes de una de las granjas que suministra pollos al mayor productor del estado de California fueron seleccionados para ser llevados a una competencia de campo.</p>

<p>Miembros de la organizaci&oacute;n <a href="https://harvesthomesanctuary.org/" target="_blank">Harvest Home Animal Sanctuary</a> lograron asegurar la liberaci&oacute;n de las aves una vez finalizara el evento y confirmaron su colocaci&oacute;n con el refugio para animales de granja <a href="http://www.farmsanctuary.org/" target="_blank">Farm Sanctuary</a>.</p>

<p>Todos eran muy j&oacute;venes (apenas 9 semanas y media de edad) pero ya hab&iacute;an superado la edad en la que hubiesen sido llevados al matadero.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Los pollos criados por la industria son matados a las seis semanas de edad, es decir, cuando a&uacute;n son unos beb&eacute;s. Son tan j&oacute;venes que a&uacute;n emiten el sonido de los pollitos y no los de un pollo adulto&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>Ahora cada uno de ellos recibe atenci&oacute;n y cuidados personalizados, algo que nunca hubiese ocurrido en el entorno de la industria. A diferencia de los 9 mil millones de pollos que son consumidos cada a&ntilde;o en Estados Unidos ellos pasar&aacute;n el resto de sus d&iacute;as rodeados de personas que los proteger&aacute;n y amaran como merecen. &nbsp;</p>

<p><img alt="" class="wp-image-11303" src="/app/uploads/2016/11/gerald.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><br />
<FONT SIZE=1><FONT COLOR=#808080>Gerald, uno de los gallos que ahora vive feliz en Wildwood Farm Sanctuary &copy; Farm Sanctuary</p></FONT>

<p>&nbsp;</p>

<strong>2. Bella y Stella, una amistad incondicional</strong>

<p><img alt="" class="wp-image-11304" src="/app/uploads/2016/11/bella_y_stella.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><br />
<FONT SIZE=1><FONT COLOR=#808080>&copy; Farm Sanctuary</p></FONT>

<p>&nbsp;</p>

<p>Bella es una gallina ameraucana que naci&oacute; con un severa deformidad en su pico que le imped&iacute;a crecer al mismo ritmo que otras gallinas con quienes viv&iacute;a en una granja de producci&oacute;n de huevos.</p>

<p>Como para los granjeros el tiempo es dinero, cuidar de una gallina deformada no vale la pena. Bella estaba abandona a su suerte pero uno de los trabajadores de la granja se ofreci&oacute; a llev&aacute;rsela a su casa. Stella (la gallina color durazno) le fue entregada para que fuera su compa&ntilde;era.</p>

<p>Bella estaba tan d&eacute;bil que apenas soport&oacute; el viaje hasta Farm Sanctuary. No pod&iacute;a mantener su cabeza erguida y por ello fue atendida y restablecida. A las ocho horas levant&oacute; su cabeza y comenz&oacute; a responder a los gritos desesperados de su mejor amiga Stella.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;A diferencia de la mayor&iacute;a de las gallinas que pasan sus vidas siendo explotadas por sus huevos y teniendo que soportar toda clase de privaciones y maltratos, Bella y Stella se encuentran ahora en un lugar donde jam&aacute;s le faltar&aacute;n los cuidados y el cari&ntilde;o&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>Bella y Stella son verdaderas amigas, se acompa&ntilde;an, duermen juntas, lo cual ayuda a mantener caliente a la fr&aacute;gil Bella. A Stella le encanta limpiar a Bella que siempre tiene buen apetito pero es algo desastrosa al comer debido a la deformaci&oacute;n de su pico. Tambi&eacute;n practican sus habilidad de vuelo, les encanta tomar el aire fresco, picotear la tierra y jugar como lo har&iacute;a cualquier beb&eacute;.</p>

<p>&nbsp;</p>

<strong>3. Little Miss Sunshine o c&oacute;mo la compasi&oacute;n logra cambiar los corazones</strong>

<p>Un d&iacute;a el due&ntilde;o de una granja de gallinas enjauladas tuvo un cambio de coraz&oacute;n: se dio cuenta que <strong>no era justo mantener confinadas dentro de jaulas a estos sensibles y maravillosos seres como si fueran &laquo;unidades de producci&oacute;n&raquo; y de que no pod&iacute;a seguir lucr&aacute;ndose de su sufrimiento</strong>. A continuaci&oacute;n tom&oacute; una importante decisi&oacute;n: darles una vida digna y para eso pidi&oacute; ayuda al refugio de animales <a href="https://edgarsmission.org.au/" target="_blank">Edgard&acute;s Mission</a>.</p>

<p>Las gallinas ponedoras pasan toda su vida confinadas en sucias jaulas en bater&iacute;a con un espacio para cada una tan peque&ntilde;o como un iPad donde ni siquiera pueden extender sus alas. A fin de que la producci&oacute;n de huevos sea mayor se les somete a la dolorosa mutilaci&oacute;n de su pico y son forzadas a poner huevos a un ritmo antinatural alterando los ciclos de luz y oscuridad y oblig&aacute;ndoles a permanecer largas horas despiertas.</p>

<p>Tambi&eacute;n se practica la muda forzada que consiste en privar a las gallinas de agua y comida por 3 d&iacute;as a fin de reducir su peso corporal en un 20 % y acelerar un segundo ciclo de puestas de huevo, lo cual provoca que muchas de ella mueran durante o despu&eacute;s de dicha pr&aacute;ctica.</p>

<p><img alt="" class="wp-image-11305" src="/app/uploads/2016/11/little-miss-sunshine.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>&copy; Edgard&acute;s Mission</p></FONT>

<p>&nbsp;</p>

<p>La brutal explotaci&oacute;n que sufren d&iacute;a tras d&iacute;a hace que su salud se resienta y muchas de ellas mueran en las jaulas incluso antes de ser llevadas al matadero.</p>

<p>Luego de recibir la llamada del granjero, el equipo de rescate de Edgard&acute;s Mission se prepar&oacute; para transportar con delicadeza a todas y cada una de las 1081 gallinas hacia una nueva vida donde conocer&iacute;an por primera vez la libertad.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Little Miss Sunshine era una de estas gallinas y su destino iba a ser el mismo que el resto de las 1080 gallinas que viv&iacute;an con ella: ser&iacute;a enviada al matadero cuando su producci&oacute;n de huevos mermara luego de una triste vida en la que solo conoci&oacute; el sufrimiento&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>Entre las rescatadas se encontraba una peque&ntilde;a gallina que desde su llegada al refugio conquist&oacute; el coraz&oacute;n de todos. Un d&iacute;a, poco despu&eacute;s de llegar, estaba echada sobre la hierba con sus alas extendidas en una de sus primeras oportunidades de recibir la luz de sol. Uno de los trabajadores dijo &laquo;m&iacute;rala, le encanta el sol pero nunca lo hab&iacute;a visto antes. Vamos a llamarla Peque&ntilde;a Miss Sunshine&raquo;.</p>

<p>Alegre y resplandeciente, Little Miss Sunshine se ha convertido en la embajadora de miles de millones de gallinas. Ella es la esperanza de un mundo mejor para todos los animales, porque no solo es posible sino que de forma imparable lo estamos construyendo.</p>

<p><strong>Si quieres estar al tanto de c&oacute;mo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n</strong>.</p>

