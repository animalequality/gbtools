<div class="center"><img class="wp-image-11715" src="/app/uploads/2017/02/cherihabibi.jpg" /></div>
<p><strong>Si trabajas en una granja de cr&iacute;a de animales sabr&aacute;s que todo se maneja en t&eacute;rminos de rentabilidad</strong>. Si un animal enferma o si desciende su nivel de producci&oacute;n (por ejemplo, de leche) implica un gasto innecesario y para &eacute;l s&oacute;lo hay un destino posible: el matadero.</p>

<p>Esto es lo que Chery Ezzel presenci&oacute; d&iacute;a tras d&iacute;a en la granja de leche de cabras y vacas que administraba junto a su esposo Jim, hasta que un d&iacute;a su visi&oacute;n de lo que hac&iacute;a para ganarse la vida cambi&oacute; por completo.</p>

<p>Un d&iacute;a, en plena faena de orde&ntilde;o, se dio cuenta de que un becerro estaba muy enfermo. <strong>Al preguntarle a Jim qu&eacute;&nbsp;pasar&iacute;a con &eacute;l, este le dijo que ser&iacute;a vendido inmediatamente a alg&uacute;n comerciante que le har&iacute;a matar por su carne</strong>.</p>

<p>Cheryl hab&iacute;a ya presenciado c&oacute;mo los terneros eran separados de sus madres para ser vendidos sin siquiera tomar el calostro, la primera leche que es crucial para su supervivencia.</p>

<p><strong>Tambi&eacute;n fue testigo de la desesperaci&oacute;n de las vacas que llaman a sus beb&eacute;s por d&iacute;as luego de que son separadas de ellos</strong>. &iquest;C&oacute;mo algo as&iacute; puede calificarse como &laquo;humano&raquo;?, se pregunta ahora Cheryl.</p>

<p><img alt="" class="wp-image-11714" src="/app/uploads/2017/02/15181304_10153925571457461_172895788061072586_n.jpg" style="float:right; margin:10px; width:450px" />Conscientes del sufrimiento que estaban provocando, Cheryl y Jim decidieron dejar la producci&oacute;n de leche de vaca para dedicarse &uacute;nicamente a la producci&oacute;n de leche de cabra.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>

<p><br />
Pensaron que podr&iacute;an vender la leche y tener a los beb&eacute;s viviendo con ellos para que no terminaran en el matadero, pero los costos de manutenci&oacute;n eran muy elevados. <strong>Eran muchos beb&eacute;s y cada a&ntilde;o se sumaban m&aacute;s para que pudieran obtener suficiente leche para vender</strong>.</p>

<p>Como en algunas comunidades se acostumbra a comer la carne de estos beb&eacute;s en Pascua, comenzaron a vender a los chivitos. <strong>Un d&iacute;a observaron c&oacute;mo se llevaban en un cami&oacute;n a uno de ellos mientras este lloraba desesperadamente camino al matadero</strong>.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Fue en ese momento terror&iacute;fico cuando Jim y yo nos miramos con l&aacute;grimas en los ojos y comenzamos nuestro camino hacia una vida sin matar&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>Desde entonces, <strong>Jim y Cheryl abandonaron la producci&oacute;n de leche de cabra y convirtieron su granja en un refugio de animales de granja</strong>, animales silvestres y de perros y gatos.</p>

<p>&nbsp;</p>

<p>Con la seguridad que ha ganado al hacer lo que considera correcto Cheryl nos dice: <strong>&laquo;Para Jim y para mi no existe algo como una cr&iacute;a de animales humana. Una agricultura humana es aquella que cultiva para la producci&oacute;n de una dieta basada en vegetales. La agricultura inhumana es la que cr&iacute;a seres que sienten y sufren para su producci&oacute;n y consumo&raquo;.</strong></p>

