Hola, soy Basílico y quiero contar mi historia porque quiero que llegue muy lejos. Tal vez así pueda ayudar a mis hermanos y hermanas que no tienen la suerte que yo tuve…

La historia de mis hermanos y hermanas suele ser muy triste y la mía iba a ser igual, <strong>pero alguien muy importante para mí se cruzó en mi camino y me salvó: tú</strong>.

Al igual que a ellos, nada más nacer <strong>me separaron de mi madre</strong>. Recuerdo cómo me seguía desesperada mientras el granjero me arrastraba hacia el camión que me llevó a la granja de engorde.

Hizo todo lo que pudo porque no nos separaran, pero el granjero era más fuerte.

Durante el transporte en el camión todos teníamos mucho miedo. <strong>Éramos sólo bebés</strong>. Llamábamos a nuestras madres porque aún teníamos esperanza.

En la granja nuestra vida fue a peor. Nos maltrataban y golpeaban. Algunos no consiguieron sobrevivir.

<strong>Fue entonces cuando tomé una decisión: escapar.</strong>

Una noche cuando los granjeros no estaban conseguí colarme entre los barrotes de la horrible granja. Salí corriendo sin mirar atrás. <strong>No sabía lo que me esperaba</strong>, pero estaba seguro que sería mejor que quedarme en aquel lugar en el que tanto nos hacían sufrir.

La noche era fría y yo sentía <strong>mi corazón latiendo fuertemente</strong> entre el miedo a ser descubierto y atrapado y el nerviosismo por mi huida hacia ninguna parte.

Se hizo de día y fui a parar al lado de una carretera. Me sentía muy débil. Se me acababan las esperanzas.

Y entonces sucedió. Al principio sólo pude ver a personas acercándose a mí. Pensé que serían los granjeros. Estaba aterrado. Pero no eran granjeros. Eran amigos. Mis salvadores: los investigadores de Igualdad Animal.

<img class="wp-image-13814" style="float: right; margin: 10px; width: 450px;" src="/app/uploads/2015/12/basilico2.jpg" alt="" />Me cogieron en brazos y me taparon con mantas. <strong>Me hablaban con cariño y me acariciaban</strong>. Uno de ellos empezó a llorar de alegría mientras me acariciaba y abrazaba.

<strong>Entonces lo supe: estaba salvado</strong>. Gracias, a ti que estás leyendo esto y apoyas a Igualdad Animal. Gracias a ti, hoy en día vivo en un maravilloso refugio para animales en Italia, donde me cuidan y me quieren.

Gracias a ti que compartes posts y que dejas comentarios de apoyo y <a href="https://igualdadanimal.org/hazte-socio/">que donas</a> para que los investigadores de Igualdad Animal sigan contando las historias de los que como yo, nacimos sólo para sufrir. <strong>Así pondrán a mucha gente de nuestro lado y mis hermanos y hermanas ya no sufrirán más</strong>.

Sé que he tenido mucha suerte. La mayoría de los animales no tienen tanta , pero en mí hay una gran esperanza de que las cosas cambien.

<strong>Contigo de mi lado, mi esperanza nunca se apagará</strong>.
