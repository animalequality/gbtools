En Río de Janeiro ha quedado prohibida la vivisección desde el martes 6 de noviembre tras la aprobación de una nueva ley. Esta ciudad es el segundo lugar de Brasil donde se realizan más vivisecciones. 

Sin embargo, el alcalde afirmó que anulará la ley: "La Cámara [municipal] afirmó que esa ley es nula, pues la enmienda aprobada no fue incluida. Por lo tanto, elevarán el texto correcto de inmediato".

Asimismo, en respuesta a dicha prohibición, el gobierno brasileño se ha planteado presionar al Congreso para que se apruebe una nueva ley, la cual anularía la prohibición de Río de Janeiro.
