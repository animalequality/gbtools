<div class="center"><img class="wp-image-9772" src="/app/uploads/2012/11/aan_orangutan_b.jpg" /></div>
<p align="justify">Un orangut&aacute;n, al que han llamado &quot;Aan&quot;, &nbsp;fue rescatado en Indonesia por el equipo veterinario de la Orangutan Foundation tras encontrarle un total de 104 casquillos en sus &oacute;rganos vitales, ojos y o&iacute;dos.<br />
	<br />
	Aan fue sometido a tres horas de cirug&iacute;a para extraerle los 37 balines que albergaba en la cabeza y los 67 del resto del cuerpo. Las radiograf&iacute;as tomadas dejan patente el ensa&ntilde;amiento con el que se dispar&oacute; al animal.</p>
<p><img alt="" class="wp-image-13805" src="/app/uploads/2012/11/balines1.jpg" style="width: 260px; float: left; " title="Radiografía de Aan en la que pueden apreciarse los balines" /><img alt="" class="wp-image-13806" src="/app/uploads/2012/11/balines2.jpg" style="width: 260px; float: left; " title="Balines dispersos por toda la cabeza de Aan" /></p>
<p align="justify">&nbsp;</p>
<p align="justify">Aan viv&iacute;a cerca de una <strong>planta agr&iacute;cola productora de aceite </strong>en la isla de Borneo, por lo que todo apunta a que <strong>los agresores sean trabajadores de la empres</strong>a, que intentaron mantener al orangut&aacute;n alejado de la cosecha.</p>
<p align="justify"><br />
	Aunque qued&oacute; ciego del ojo izquierdo debido a los disparos, Aan est&aacute; logrando recuperarse muy favorablemente. Seg&uacute;n los m&eacute;dicos que lo atienden,&nbsp;ya est&aacute; en condiciones de comer y tomar agua, aunque a&uacute;n debe ser asistido para acceder a los alimentos, debido a su ceguera.</p>
<p align="justify"><br />
	<img alt="" class="wp-image-13747" src="/app/uploads/2012/11/aan_orangutan.jpg" style="width: 580px; " title="Aan durante la operación" /></p>
<p>&nbsp;</p>
<p align="justify">Aan tien a&uacute;n casquillos de bala alrededor de los ojos y jam&aacute;s podr&aacute; volver a vivir en la naturaleza, ya que ser&iacute;a un blanco f&aacute;cil para los cazadores y agricultores, que tanto desprecian su vida.</p>

