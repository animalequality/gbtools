<br />
Organiza: Igualdad Animal<br />
D&iacute;a: S&aacute;bado 11 de Diciembre 2010<br />
Hora: 12 h.<br />
Lugar: Puerta del Sol (junto a la estatua de El Oso y el Madro&ntilde;o) - Madrid<br />
<br />
Con motivo del D&iacute;a Internacional de los Derechos Animales la organizaci&oacute;n Igualdad Animal convoca el pr&oacute;ximo s&aacute;bado 11 de Diciembre a las 12 del mediod&iacute;a en la Puerta del Sol de Madrid una impactante protesta en la que 250 activistas provenientes de varios pa&iacute;ses sostendr&aacute;n en sus manos 250 cad&aacute;veres de animales que han sido v&iacute;ctimas del consumo para reivindicar los derechos animales.<br />
<br />
Zorros, gatos, pollos, cerdos, gallinas, corderos&hellip; son algunos de los 250 animales que formar&aacute;n parte de esta protesta. Recogidos de los contenedores de las granjas donde permanec&iacute;an apilados o sacados del interior de dichas granjas donde hab&iacute;an muerto, estos animales pasar&aacute;n de haber sido explotados en vida y finalmente desechados como basura a ser los protagonistas de una reivindicaci&oacute;n solemne mediante la que exigimos que los derechos animales sean respetados.<br />
<br />
Junto a otras organizaciones en todo el mundo, Igualdad Animal celebra desde 2006 el D&iacute;a Internacional por los Derechos Animales coincidiendo con el D&iacute;a por los Derechos Humanos para reivindicar que el mismo principio igualdad que nos lleva a respetar a otros seres humanos debe ser extendido para proteger a los dem&aacute;s animales.<br />
<br />
Los animales nohumanos -no olvidemos que los humanos tambi&eacute;n somos animales&ndash; son igualmente capaces de sentir y tienen intereses propios que merecen el mismo respeto que acordamos entre humanos. La desconsideraci&oacute;n de sus intereses o especismo es una discriminaci&oacute;n arbitraria, un prejuicio irracional an&aacute;logo al racismo y al sexismo que debe ser superado. Igualdad Animal quiere animar a todos a respetar a los animales haci&eacute;ndonos veganos y dejando de utilizar animales ya sea en nuestra alimentaci&oacute;n, vestimenta o otros &aacute;mbitos de nuestra vida.<br />
<br />
Igualdad Animal es una organizaci&oacute;n internacional de derechos animales actualmente presente en Espa&ntilde;a, Inglaterra, Polonia, Venezuela y Colombia dedicada a la educaci&oacute;n y concienciaci&oacute;n social, la realizaci&oacute;n de investigaciones sobre la explotaci&oacute;n animal y los rescates abiertos de animales.<br />
<br />
Galer&iacute;a fotogr&aacute;fica de la protesta de Igualdad Animal en 2009:<br />
http://www.flickr.com/photos/igualdadanimal/sets/72157622823030485/<br />
<br />
<br />
CONTACTO DE MEDIOS<br />
<br />
Sharon N&uacute;&ntilde;ez | Portavoz de Igualdad Animal<br />
Tel. 609 980 196<br />
Tel. 691 054 172<br />
Correo electr&oacute;nico: sharonn@igualdadanimal.org<br />
<br />
<img alt="" class="wp-image-13652" src="/app/uploads/2010/12/4168898835_aef82c4f7f_z.jpg" style="width: 640px; " /><br />

