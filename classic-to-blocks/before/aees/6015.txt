<div class="center"><img class="wp-image-9041" src="/app/uploads/2011/05/estrella4.jpg" /></div>
<p>
	El descubrimiento de una perra que fue enterrada viva hasta su nariz despu&eacute;s de recibir 40 disparos, ha conmocionado a Malta y a muchos usuarios de la red social Facebook. Llamada Estrella por su milagroso rescate, fue encontrada cerca de la ciudad de Birzebbuga por agentes de protecci&oacute;n de los animales, que investigaban un caso no relacionado. Los agentes escucharon gemidos procedentes de debajo de una tabla de madera, con un tronco pesado colocado en la parte superior.</p>
<p>
	Tras retirar la madera, encontraron con la terrible escena de la cara de un perro enterrado en la tierra. Espantosa crueldad, pues Estrella fue enterrada viva, con todas sus piernas atadas y con 40 disparos de pistola de perdigones en el cr&aacute;neo.</p>
<p>
	Estrella sobrevivido milagrosamente, despu&eacute;s de que los veterinarios le extirparan los 40 perdigones de su cr&aacute;neo durante una cirug&iacute;a de emergencia en el hospital de Ta &#39;Qali.</p>
<p>
	El caso ha causado indignaci&oacute;n en Malta donde, independientemente de la naturaleza del acto, la pena m&aacute;xima puede hacer frente a la crueldad animal es una sentencia de c&aacute;rcel de un a&ntilde;o o una multa m&aacute;xima de 46.500 euros.</p>
<p>
	Con el agresor en libertad, las personas han expresado su apoyo con p&aacute;ginas dedicadas en Facebook, la mayor de las cuales tiene 7.000 seguidores. M&aacute;s de 1.000 personas han firmado una petici&oacute;n en l&iacute;nea para ayudar a atrapar al agresor y para generar fondos para pagar las facturas del veterinario. Se han realizado donaciones incluso desde Canad&aacute; y cartas de preocupaci&oacute;n de la Brigada de Bomberos de Amsterdam.</p>
<p>
	Despu&eacute;s de haber pasado por este calvario atroz, Estrella tiene m&aacute;s de 20 peticiones de adopci&oacute;n, sin embargo, debido al trauma se ha decidido esperar a que Estrella se recupere f&iacute;sica y psicol&oacute;gicamente antes de ser acogida en un nuevo hogar.</p>

