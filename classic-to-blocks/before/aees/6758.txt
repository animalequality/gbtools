<div class="center"><img class="wp-image-9743" src="/app/uploads/2012/10/duke_perro_avisa.jpg" /></div>
<p align="justify">Un perro llamado <strong>Duke salv&oacute; la vida de una ni&ntilde;a de 9 meses</strong> en Conecticut, Estados Unidos, avisando a los padres de &eacute;sta para que se fueran hasta su habitaci&oacute;n.</p>
<p align="justify">Duke salt&oacute; a la cama del matrimonio, mientras dorm&iacute;an, y comenz&oacute; a temblar y gemir. Extra&ntilde;ados por su comportamiento, acudieron a la habitaci&oacute;n de Harper, su beb&eacute; de tan solo 9 meses, y <strong>descubrieron que la peque&ntilde;a hab&iacute;a dejado de respirar</strong>. Inmediatamente fue trasladada a un hospital, donde se recupera.&nbsp;</p>
<p align="justify"><img alt="" class="wp-image-13695" src="/app/uploads/2012/10/Duke_perro_avisa.jpg" style="width: 580px; " title="Duke y la pequeña Harper" /></p>
<p align="justify">&nbsp;</p>
<p align="justify">La familia asegura que sin la alerta de Duke, adoptado hace 6 a&ntilde;os por la familia,&nbsp; no habr&iacute;an podido salvar la vida de su bebe: <em>&laquo;<strong>Si Duke no hubiera estado tan asustado, nos habr&iacute;amos dormido.</strong>&raquo;</em></p>
<p align="justify">Los m&eacute;dicos no est&aacute;n seguros de qu&eacute; caus&oacute; que Harper dejara de respirar, pero creen que podr&iacute;a haber sido a causa del reflujo g&aacute;strico.</p>
<p><iframe allowfullscreen="" frameborder="0" height="330" scrolling="no" src="https://www.youtube.com/embed/BQvzKk9Tn0s" width="580"></iframe></p>

