Pingüinos, lobos marinos y gaviotas están siendo atendidos en la costa este uruguaya, después de que alcanzara el litoral parte del combustibles vertido la pasada semana en el Río de la Plata, tras el choque de dos cargueros.

Richard Tesore, una de las personas que participa en el rescate, ha afirmado que el accidente podría convertirse en "un desastre de grandes dimensiones".

La Intendencia (Ayuntamiento) del departamento de Maldonado puso a disposición de las organizaciones que están participando en el rescate un vehículo municipal para que puedan recoger a los animales que están apareciendo afectados.

Hace una semana el buque carguero griego 'Syros' y el maltés 'Sea Bird' colisionaron a unos 30 kilómetros de las costas uruguayas y, según las primeras informaciones, unos 14.000 litros de combustible cayeron a las aguas del Río de la Plata.

Para la Intendencia de Maldonado, la magnitud del vertido es mucho mayor ya que aseguran que fueron expulsados al Río de la Plata 14.000 metros cúbico de fuel oil, unos 14 millones de litros.

El presidente del Comité de Emergencia de Maldonado, Enrique Pérez Morad, aseguró que 'todavía no salió del mar todo el combustible vertido por el barco'.

'La propia experta contratada por la empresa aseguradora nos confirmó que a su juicio el 95 por ciento del combustible ya salió, pero el remanente que queda nos obliga a estar preparados', añadió.

Las labores de limpieza de las playas podrían prolongarse entre dos días y una semana, según la Dirección Nacional de Medio Ambiente (Dinama).

La Armada puso en marcha el pasado martes un plan de contención y limpieza para contrarrestar los efectos de las manchas, en el que participan efectivos de la Prefectura Nacional Naval.

Las empresas aseguradoras de los buques y la agencia marítima Cristophersen, responsable del 'Syros', enviaron rastrillos, bolsas para recoger los restos de combustible, vestimenta especial y material para la limpieza, además de 50.000 pesos uruguayos (unos 2.500 dólares) para costear los gastos de alimentación de los trabajadores.

Los datos del caso serán elevados a la Comisión de Accidentes Marítimos, que deberá definir responsabilidades y aplicar las eventuales multas que correspondan.

Fuente: Terra Actualidad - EFE
