<div class="center"><img class="wp-image-11453" src="/app/uploads/2016/12/30873467830_2a8118baaf_z.jpg" /></div>
<p>La industria c&aacute;rnica es a los animales de granja lo que las peleas de perros a nuestros compa&ntilde;eros de vida o la tauromaquia a los toros: violencia y maltrato.</p>

<p>Por desgracia para los inteligentes, sensibles y desprotegidos animales de granja, la diferencia es que <strong>ellos est&aacute;n en manos de una industria que no es vista a&uacute;n como lo son la tauromaquia o las peleas de perros</strong>.</p>

<p>&iquest;Por qu&eacute;?</p>

<p>Porque esta industria est&aacute; tan institucionalizada y subvencionada que nos ha hecho creer que necesitamos comer carne a diario para sobrevivir; de hecho, nos ha hecho creer que <a href="https://www.abc.es/sociedad/abci-igualdad-animal-pide-reducir-5242805119001-20161210015305_video.html">no hay ning&uacute;n problema &eacute;tico con enviar al matadero a 68.000 millones de animales cada a&ntilde;o a nivel mundial</a>.</p>

<p>68.000 millones de animales al a&ntilde;o: la poblaci&oacute;n humana de 9 planetas Tierra.</p>

<blockquote>
<h4>&laquo;A la industria c&aacute;rnica solo hay una cosa que le importe menos que las vidas de estos desprotegidos animales: tu salud&raquo;.</h4>
</blockquote>

<p>Y lo que resulta incluso m&aacute;s perturbador es que la abrumadora mayor&iacute;a de la carne proveniente de estos animales <strong>es consumida en las sociedades occidentales</strong>. Sociedades en las que las autoridades sanitarias <a href="https://igualdadanimal.org/noticia/2016/04/05/el-gobierno-holandes-recomienda-reducir-el-consumo-de-carne-dos-raciones-semanales/">llevan a&ntilde;os recomendando que se reduzca el consumo de carne</a> y se aumente el de verduras, legumbres, cereales y fruta.</p>

<p><img alt="" class="wp-image-11452" src="/app/uploads/2016/12/30873479890_ac41541748_z.jpg" style="float:left; margin:10px; width:300px" />&iquest;Quieres saber la verdad? <strong>A la industria c&aacute;rnica solo hay una cosa que le importe menos que las vidas de estos desprotegidos animales: tu salud</strong>. Una sociedad que tiende a ver cualquier plato sin carne como un plato nutricionalmente incompleto es una sociedad enga&ntilde;ada por la publicidad de una industria sin escr&uacute;pulos.</p>

<p>De hecho, como acaba de avalar un a&ntilde;o m&aacute;s <a href="https://igualdadanimal.org/noticia/2016/11/25/la-alimentacion-vegetariana-respaldada-por-la-academia-de-nutricion-mas-importante-del/">la academia de nutricionistas m&aacute;s influyente del mundo, las alimentaciones vegetarianas (vegana incluida), son nutricionalmente saludables y ecol&oacute;gicamente preferibles</a>. Esta academia, adem&aacute;s, se&ntilde;ala que lo son en todas las etapas de la vida del ser humano, infancia y embarazo incluido.</p>

<p>&iquest;Cu&aacute;nto tiempo m&aacute;s continuaremos negando la evidencia?</p>

<p>Los animales de granja, los millones de animales de granja, <strong>viven en un continuo ciclo de violencia, desamparo y sufrimiento</strong>. Las hembras son seleccionadas para seguir concibiendo m&aacute;s y m&aacute;s animales que nos abastezcan de carne que no necesitamos comer. Son inseminadas artificialmente una y otra vez para traer al mundo a cr&iacute;as que no deber&iacute;an nacer. Sus cr&iacute;as son separadas de ellas al poco tiempo de nacer para ser enviadas a <a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/">granjas industriales de engorde que no deber&iacute;an existir</a>.</p>

<p>El nombre de estos lugares es ya de por s&iacute; perturbadoramente descriptivo: engordamos a los animales con el solo prop&oacute;sito de com&eacute;rnoslos.</p>

<p>En nuestra manera de ver la alimentaci&oacute;n, intencionadamente manipulada por la industria c&aacute;rnica, si no comemos carne, enfermaremos porque nos faltar&aacute; prote&iacute;na. <strong>Al igual que hace a&ntilde;os se nos dec&iacute;a que si no beb&iacute;amos leche de vaca nos quedar&iacute;amos sin calcio</strong>. Y, sin embargo, el consumo de leche de vaca no para de descender a la vez que no para de aumentar el de leches vegetales. &iquest;D&oacute;nde est&aacute;n esas hordas humanas con carencia de calcio?</p>

<p>Tal vez, est&eacute;n en el mismo lugar que todos esas personas que han decidido decir no a la industria c&aacute;rnica: <strong>viviendo vidas saludables y satisfactorias</strong> aliment&aacute;ndose con alternativas a la carne.</p>

<p><strong>Si quieres saber c&oacute;mo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4">suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n</strong>.</p>

