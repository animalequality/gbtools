<div class="center"><img class="wp-image-12087" src="/app/uploads/2017/06/shutterstock_157322306.jpg" /></div>
<p>Miley Cyrus ha hecho p&uacute;blico en m&uacute;ltiples ocasiones el amor que siente hacia los animales que conviven con ella y c&oacute;mo han llegado a ser indispensables en su vida.</p>

<p>Uno de ellos, en particular, le hizo cuestionarse un d&iacute;a si era correcto para ella seguir comiendo carne. Fue Pablow, <strong>un pez globo que seg&uacute;n la cantante le reconoc&iacute;a al llegar a casa, mostr&aacute;ndose verdaderamente emocionado</strong>.</p>

<p>&nbsp;</p>

<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Puedes suscribirte gratuitamente a nuestro e-bolet&iacute;n</a> para que recibas las mejores noticias de actualidad sobre los animales y conozcas m&aacute;s opciones de alimentaci&oacute;n.</font></font></p>

<p><font size="5">&nbsp;</font></p>

<p>En sus propias palabras: &laquo;la raz&oacute;n por la cual comenc&eacute; con esto es porque yo ten&iacute;a un pez muy inteligente. &Eacute;l realmente sab&iacute;a qui&eacute;n era yo, se emocionaba cuando yo estaba en casa. Un d&iacute;a fui a un restaurante de sushi con algunos amigos y estaban sirviendo pez globo. Y pens&eacute;, este es un animal inteligente&raquo;.</p>

<p><font size="5"><img alt="" class="wp-image-12088" src="/app/uploads/2017/06/miley-cyrus-bubba-sue-collage.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></font></p>

<p>En efecto, <a href="https://www.abc.es/ciencia/20140618/abci-peces-tambien-sienten-dolor-201406181047.html" target="_blank">estudios cient&iacute;ficos</a> han demostrado que las capacidades cognitivas y sensoriales de los peces est&aacute;n, por lo general, a la par con la de otros animales. Y sus sentidos primarios son tan buenos, e inclusive, mejores, que los de los humanos.</p>

<p>Los peces han sido err&oacute;neamente subestimados de m&uacute;ltiples formas. Son mucho m&aacute;s inteligentes de lo que se cree, tienen excelente memoria y pueden formar complejas relaciones sociales. Tambi&eacute;n se reconocen a s&iacute; mismos y a otros y de desarrollan t&eacute;cnicas de manipulaci&oacute;n y castigo que les permiten colaborar con otros peces para evitar depredadores o buscar comida.</p>

<p>Lamentablemente, debido a que viven en un medio diferente al nuestro, poco sabemos sobre ellos y err&oacute;neamente subestimamos su capacidad de sentir y <a href="http://www.igualdadanimal.org/noticias/7797/6-hechos-por-los-cuales-podrias-no-querer-volver-comer-mas-nunca-pescado" target="_blank">los condenamos a terribles sufrimientos</a>.</p>

<p>Los peces no solo son los animales m&aacute;s consumidos en el planeta sino tambi&eacute;n los m&aacute;s maltratados. Anualmente la pesca industrial captura a un cantidad de peces <strong>equivalente a la poblaci&oacute;n humana de 142 planetas Tierra. Todos sufren muertes terribles y no hay legislaci&oacute;n alguna que los proteja</strong>.</p>

<p><font size="5">&nbsp;</font></p>

<p><font size="5">&nbsp;</font><font size="1"><font color="#808080">Miley junto a su cerdita Bubba Sue y uno de sus perros.</font></font></p>

<p>Resulta maravilloso que celebridades como Miley Cyrus cuyas acciones pueden tener un gran impacto en millones de personas, alcen la voz para hablar a favor de los animales y evitar su sufrimiento.</p>

<p>T&uacute; tambi&eacute;n puedes hacerlo y tambi&eacute;n sustituir el pescado en tu alimentaci&oacute;n por deliciosas opciones vegetales. <a href="http://www.igualdadanimal.org/noticias/7797/6-hechos-por-los-cuales-podrias-no-querer-volver-comer-mas-nunca-pescado" target="_blank">&iexcl;Ac&aacute; te decimos lo f&aacute;cil que es!</a></p>

