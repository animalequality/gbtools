Ribeira-Según informa El Correo Gallego, 230 cerdos murieron calcinados en un incendio ocurrido la pasada madrugada en una nave de la localidad coruñesa de Ribeira. La policía apunta a que el incendio pudo ser provocado.

La voz de alarma la dio un vecino poco después de las dos de la madrugada al ver como las llamas devoraban las instalaciones del centro de explotación de animales “A Cañoteira”, sita en el lugar de A Carballa. El vecino avisó al dueño de la planta, Pedro Manuel Regueira Iglesias, que enseguida llamó a la Policía Local.

Varios bomberos y efectivos de Protección Civil trabajaron en las labores de extinción hasta que las llamas quedaron sofocadas a las 04.30 horas. Fueron los propios bomberos los que pusieron en conocimiento de la Policía Nacional de Ribeira sus sospechas acerca de la posibilidad de que el incendio pudiese haber sido provocado, pues se detectaron hasta cinco focos de origen del fuego.

