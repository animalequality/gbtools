El próximo Miercoles día 11 de Octubre, el pleno del Parlamento Europeo debatirá sobre un proyecto de informe en el que solicita a la UE que se dote de normas que prohíban "la lucha de perros, toros y gallos". La propuesta se votará el Jueves día 12. Dicha propuesta podría acabar con las corridas de toros y otros "espectáculos taurinos" en nuestro país. 

Esta iniciativa parte de la eurodiputada alemana Elisabeth Jeggle (Grupo Popular Europeo) dentro del Plan de la UE para la Protección y el Bienestar de los Animales 2006-2010, quien elevó la propuesta a la Eurocámara y fue aprobado de forma unánime en la Comisión de Agricultura el pasado septiembre.

Los eurodiputados, que manifestaron su preocupación por el sufrimiento de los animales de lucha, pretenden así que los organismos estatales "adopten normas legales nacionales o comunitarias, según los casos, y que se asegure de que las personas implicadas no reciben subvenciones nacionales o estatales para organizar tales eventos".


