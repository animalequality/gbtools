<p><img alt="" class="wp-image-13917" src="/app/uploads/2015/11/cowspy.jpg" style="float:right; margin:10px; width:450px" />Corr&iacute;a el a&ntilde;o 2006 y nada hac&iacute;a pensar que las industrias c&aacute;rnicas estuvieran pasando por un mal momento: el mundo com&iacute;a cada vez m&aacute;s hamburguesas, salchichas y alitas de pollo, las personas segu&iacute;an pensando que la &uacute;nica <a href="http://www.agenciasinc.es/Agenda/Simposio-Proteinas-vegetales-para-el-futuro">fuente de prote&iacute;na</a> era la carne, los gobiernos promocionaban su consumo y las organizaciones ecologistas no dec&iacute;an una palabra del &ldquo;peque&ntilde;o&rdquo; problema que la ganader&iacute;a industrial estaba generando: destrucci&oacute;n de selvas, contaminaci&oacute;n de acu&iacute;feros, aceleraci&oacute;n del efecto invernadero y la brutal matanza de m&aacute;s animales que nunca antes en la historia de la humanidad.</p>

<p>Y entonces sucedi&oacute; algo. Algo que las multimillonarias industrias c&aacute;rnicas no esperaban. Algo que ni sus <a href="https://countinganimals.com/meat-industry-advertising/">todopoderosos departamentos de marketing</a> ni sus sombr&iacute;os lobbys hab&iacute;an llegado a prever: un inesperado informe sal&iacute;a a la luz haciendo m&aacute;xima responsable a la ganader&iacute;a industrial de los desastres medioambientales globales de nuestra era. En sus poltronas, los millonarios directores de las gigantescas industrias c&aacute;rnicas hicieron que saliera humo de sus tel&eacute;fonos: el informe se llamaba &ldquo;<a href="https://www.fao.org/3/a0701s/a0701s00.htm">La Alargada Sombra del Ganado</a>&rdquo;. &iquest;Qui&eacute;n demonios hab&iacute;a escrito aquello?, &iquest;Greenpeace?, &iquest;La WWF?, &iquest;PETA? No, ni en sus peores pesadillas hab&iacute;an imaginado qui&eacute;n estaba detr&aacute;s del demoledor informe: La ONU, a trav&eacute;s de su <a href="https://www.fao.org/home/es/">Organizaci&oacute;n para la Agricultura y la Alimentaci&oacute;n (FAO)</a>.</p>

<p>En el hist&oacute;rico informe de la FAO, se hac&iacute;a a la industria de la carne uno de los principales responsables de la contaminaci&oacute;n de la tierra, el cambio clim&aacute;tico y la poluci&oacute;n del aire, la escasez global de agua y la contaminaci&oacute;n de acu&iacute;feros y de la p&eacute;rdida irreparable de biodiversidad.</p>

<p>Han pasado 9 a&ntilde;os desde aquel informe y la situaci&oacute;n se ha agravado incluso m&aacute;s para las todopoderosas industrias de la hamburguesa y carne a la parrilla. Hoy en d&iacute;a las voces que claman por el cese de la ganader&iacute;a industrial se han multiplicado. Desde las organizaciones medioambientales internacionales <a href="http://www.worldwatch.org/node/6294">World Watch</a>, &nbsp;la <a href="http://www3.epa.gov/region9/animalwaste/problem.html">Agencia de Protecci&oacute;n Medioambiental de Estados Unidos,</a> la Organizaci&oacute;n Mundial del Agua o el <a href="http://www.takeextinctionoffyourplate.com/meat_and_wildlife.html">Centro para la Diversidad Biol&oacute;gica</a> hasta prestigiosas revistas como Science Magazine, Nature, International Business Times y National Geographic.</p>

<p>Lo que en 2006 era cierto en 2015 lo sigue siendo. Y ahora sabemos m&aacute;s datos. Muchos m&aacute;s. La comunidad cient&iacute;fica ha llevado a cabo nuevos estudios. &iquest;Quieres saber algunos?:</p>

<p>- La ganader&iacute;a y los subproductos que genera son responsables del <a href="http://www.worldwatch.org/node/6294">51% de las emisiones</a> de gases de efecto invernadero.</p>

<p>- El gas metano tiene un potencial de calentamiento global <a href="http://www.sciencemag.org/content/326/5953/716.figures-only">86 veces mayor</a> que el del di&oacute;xido de carbono. La ganader&iacute;a industrial es la responsable de emitir diariamente a la atm&oacute;sfera 600.000 millones de litros de metano. Una sola vaca puede producir entre 250 y 500 litros de metano al d&iacute;a. El tanque de gas de un coche impulsado por gas suele contener unos 60 litros.</p>

<p>- La ganader&iacute;a es responsable del <a href="ftp://ftp.fao.org/docrep/fao/011/a0701s/a0701s03.pdf">65% de emisiones</a> del gas &oacute;xido nitroso, un gas de efecto invernadero con un potencial 296 mayor que el di&oacute;xido de carbono.</p>

<p>- Se necesitan <a href="http://www2.worldwater.org/data20082009/Table19.pdf">9.400 litros de agua</a> para producir 500 gramos de carne de vaca.</p>

<p>- Se necesitan <a href="https://www.ewg.org/consumer-guides/ewgs-quick-tips-reducing-your-diets-climate-footprint">1.800 litros de agua</a> para producir 500 gramos de huevos.</p>

<p>- Se necesitan <a href="https://waterfootprint.org/media/downloads/Hoekstra-2008-WaterfootprintFood.pdf">3.700 litros de agua</a> para producir 3,7 litros de leche.</p>

<p>- La ganader&iacute;a es la responsable del consumo de entre <a href="http://www.sciencedirect.com/science/article/pii/S2212371713000024">20% y 33% de toda el agua potable del mundo</a>.</p>

<p>- La ganader&iacute;a industrial y los pastos para alimentar a los animales ocupan el <a href="https://cgspace.cgiar.org/bitstream/handle/10568/10601/IssueBrief3.pdf">45% de la tierra habitable en el planeta.</a></p>

<p>- La ganader&iacute;a industrial es una causa principal de la extinci&oacute;n de especies, zonas oce&aacute;nicas muertas, poluci&oacute;n del agua y destrucci&oacute;n de h&aacute;bitats.</p>

<p>- ⅓ del planeta est&aacute; desertificado con la ganader&iacute;a industrial como un factor principal.</p>

<p>- La ganader&iacute;a industrial es la responsable de &nbsp;hasta un <a href="https://www.wdronline.worldbank.org/bitstream/handle/10986/15060/277150PAPER0wbwp0no1022.pdf?sequence=1">91% de la deforestaci&oacute;n del Amazonas</a>.</p>

<p>- M&aacute;s de <a href="https://awellfedworld.org/factory-farms/">70.000 millones de animales</a> mueren al a&ntilde;o para consumo humano. Esto significa m&aacute;s de 6 millones por hora.</p>

