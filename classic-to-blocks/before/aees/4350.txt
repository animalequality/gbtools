El medio centenar de embarcaciones andaluzas que se dedican a la observación turística de cetáceos -la mayoría en el Estrecho de Gibraltar- aplica en la nueva temporada el reciente Real Decreto que restringe la observación turística de cetáceos e impide acercarse a ellos a menos de 60 metros.

Este Real Decreto regula las condiciones de trabajo y el número de embarcaciones que pueden dedicarse a esta actividad para evitar molestias y daños a estos animales pues, según el texto legal, 'diversos estudios nacionales e internacionales demuestran que estas actividades alteran los patrones de conducta de los cetáceos'. Además, el transporte marítimo y la excesiva presencia de embarcaciones pueden tener efectos adversos sobre las poblaciones de cetáceos y su hábitat, tanto por colisión con individuos, especialmente las embarcaciones rápidas o las dedicadas al turismo de observación, como por afectar a su comunicación y dañar su sistema auditivo.

La norma crea el denominado Espacio Móvil de Protección de Cetáceos, un cilindro virtual cuya base sería una circunferencia de mil metros de diámetro y su centro sería el grupo de cetáceos, una altura en la zona aérea de 500 metros y que una zona submarina de 60 metros de profundidad.

Dentro del Espacio Móvil se distinguen 5 zonas según la distancia a la que se encuentren los cetáceos:

1. Zona de Exclusión, que tendrá un radio no inferior a 60 m medidos en la superficie del agua a partir del cetáceo o grupo de cetáceos (se entiende por grupo el constituido por 2 o más ejemplares que se encuentren a una distancia no mayor de 50 m unos de otros).

2. Zona de Permanencia Restringida, que comprende la superficie entre el límite de la Zona de Exclusión (60 m) y el límite de la Zona de Aproximación (300 m).

3. Zona de Aproximación, que comprende la superficie entre los 300 m del límite de la Zona de Permanencia Restringida y los 500 m del contorno exterior del Espacio Móvil.

4. Zona Aérea, que comprende el espacio aéreo dentro de los 500 m de radio del cilindro imaginario en vertical, y en horizontal, a partir del cetáceo o grupo de cetáceos.

5. Zona Submarina, que comprende el espacio submarino dentro de los 500 m de radio del cilindro imaginario en horizontal y los 60 m en profundidad a partir del cetáceo o grupo de cetáceos.

Entre las actividades que deben evitarse en el Espacio Móvil de Protección de Cetáceos figuran el contacto físico de embarcaciones o personas; alimentar a los cetáceos; tirar alimentos, bebidas, basuras o cualquier otro objeto o sustancia que sea perjudicial para ellos; impedir su libre movimiento; interceptar su trayectoria; separar o dispersar a un grupo de cetáceos, especialmente, interponerse entre un adulto y su cría; producir ruidos y sonidos fuertes o estridentes para intentar atraerlos o alejarlos, incluyendo la emisión de sonidos bajo el agua, y bañarse o bucear en la Zona de Exclusión del Espacio Móvil.

Durante la realización de actividades recreativas de observación de cetáceos en el Espacio Móvil se prohíbe el uso de sistemas de sónar y/o acústicos para emitir ruidos con objeto de detectar cetáceos o conducirlos a la superficie.

Asimismo, las embarcaciones deberán moverse a una velocidad constante y no superior a 4 nudos y, en su caso, no más rápida que el animal más lento del grupo, a excepción de la Zona de Exclusión.
Además, está prohibida la aproximación a los cetáceos de frente, por detrás o perpendicularmente a su trayectoria. La aproximación se hará de forma suave y convergente con la dirección y el sentido de la natación de los animales, en un ángulo de aproximadamente 30º. Tampoco se navegará en círculo en torno a los animales, ni se dará nunca marcha atrás. Los buceadores tendrán prohibido interactuar con los cetáceos.

El ámbito de aplicación de las disposiciones de este Real Decreto son las aguas sometidas a soberanía, derechos soberanos o jurisdicción española, que comprenden aguas interiores, el mar territorial, la zona contigua y la zona económica exclusiva.

Por su parte, las actividades de protección civil, salvamento marítimo y lucha contra la contaminación, seguridad pública marítima y aérea y defensa nacional se regirán por su normativa específica. Asimismo, los derechos de libre navegación y de paso inocente se ejercerán en los términos previstos en el derecho internacional, si bien los buques extranjeros deberán cumplir con las medidas españolas de protección de cetáceos.

El Consejo de Ministros aprobó en diciembre la norma, para proteger a los cetáceos del impacto negativo de las actividades humanas. 

Fuente: Terra Actualidad - EFE, Ministerio de Medio Ambiente


