Vivimos en una sociedad en la que la inmensa mayoría de las personas se declaran en contra del maltrato animal. ¿Cuántas veces nos hemos indignado al ver imágenes de una corrida de toros? Nos entristece y hasta llena de rabia saber que las peleas de perros son un negocio en el que se lucran a costa de su sufrimiento, y cada vez que conocemos una historia de crueldad animal celebramos que haya tenido un final feliz.

Te puede interesar: <a href="https://igualdadanimal.mx/blog/la-persona-que-puede-ayudar-mas-los-animales-eres-tu/">La persona que puede ayudar más a los animales eres tú</a>

Pero existe una realidad en la que el mayor maltrato animal que ha visto la historia se repite de forma sistematizada día tras día, sin que nadie a lo detenga y sin que nada lo cuestione. Y es así porque mientras que a nuestros queridos perros y gatos los consideramos familia, a aquellos animales que son víctimas de esta inmensa crueldad los consideramos comida.

<span style="font-size: x-large;">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación? Suscríbete gratuitamente <a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">a nuestro e-boletín</a>!</span>

Si eres una animal de granja apenas naces eres separado de tu madre. Aislado y desorientado la buscarás incansablemente, la llamarás pero ella nunca podrá venir a encontrarte. Durante el tiempo que pases dentro de una granja, nadie en absoluto tendrá un solo gesto de compasión hacia ti. Cada día solo conocerás las  privaciones, el sometimiento y el maltrato. Porque allí todo es tristeza, terror y desolación.

Dentro de las granjas los animales son sometidos a las más crueles prácticas: a las gallinas les cortan el pico con cuchillas incandescentes, a los cerditos les cortan los testículos sin anestesia para que el sabor de su carne sea más suave y a los pollitos machos los trituran vivos con apenas horas de vida porque no sirven para producir carne ni huevos. Cualquiera persona que hiciera lo mismo a animales como perros o gatos iría preso y, sin embargo, dentro de la industria ganadera, todas estas prácticas son legales.

Los productos de origen animal están presentes en todas partes, acaparando las estanterías del súper y nuestras neveras. La publicidad de la multimillonaria industria ganadera nos ha dicho desde siempre que necesitamos consumirlos para estar sanos y así lo hemos creído.

Sobre esto último han mentido siempre. Cada vez más estudios científicos dan pruebas de la incidencia que el consumo de carne tiene en nuestra salud y el planeta, y recomiendan seguir una dieta basada en vegetales para prevenir enfermedades como diabetes, hipertensión y cáncer y contribuir a reducir los efectos del cambio climático.

El cambio hacia un modelo alimentario que sea libre de crueldad hacia los animales, sostenible y que beneficie nuestra salud está a las puertas. Si los productos de origen animal nos rodean a donde quiera que vayamos, también los vegetales, frutas legumbres y semillas siempre han estado ahí disponibles. Solo debemos recordarlo a la hora de la compra y hacer nuestra parte y apostar por un futuro sostenible en el que la crueldad hacia los animales sea solo un triste recuerdo.
