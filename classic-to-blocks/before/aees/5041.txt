
HUESCA -  El Servicio de Protección de la Naturaleza de la Guardia Civil (Seprona) de Fraga y Huesca han localizado un total de 224 animales disecados, de los que 111 son especies catalogadas como amenazadas.

   La Guardia Civil de Fraga y Huesca investiga desde el mes de marzo, en el marco de la Operación Alondra, una presunta actividad ilícita de comercio y depósito de especies amenazadas naturalizadas en la zona de Fraga,  dichas investigaciones dieron como resultado la ubicación del inmueble donde se encontraban los animales disecados.

   Tras la inspección de una nave en la comarca del Bajo Cinca fueron localizados en su interior un total de 224 animales disecados, de los cuales 111 son especies catalogadas, inmersas en el catálogo de especies amenazadas, bajo algún tipo de protección (interés especial, sensibles a la alteración de su hábitat o vulnerables).

   Los 111 animales disecados fueron decomisados con el objetivo de proceder a su completa identificación, y quedaron a disposición de la autoridad competente.

   De los hechos se han instruido diligencias administrativas, por una supuesta infracción grave a la Ley 42/07 de 13 de diciembre de Patrimonio Natural y de la Biodiversidad, no obstante en el transcurso de la investigación los hechos podrían ser constitutivos de un delito contra la fauna.

Fuente: EUROPA PRESS

Nota de Igualdad Animal: El comercio de animales es una más de las actividades en las que los seres humanos estamos explotando al resto de animales. Considerados mercancía, estos animales son capturados muchas veces en sus hábitat salvajes, son separados de sus familias, vendidos, comprados, explotados y también matados para acabar siendo convertidos en objetos decorativos. Todos los animales merecen respeto, independientemente del número de individuos que conformen cada especie.
