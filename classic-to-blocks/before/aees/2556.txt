Buenos Aires - Alejandra Moreyra, portavoz de la APAEQ (Asociación Protectora de Animales y Ecología de Quilmes), denunció el preocupante estado en el que se encuentran los caballos que tiran de carritos en la ciudad, y la proliferación de su uso, "Antes veíamos algún carro tirado por caballo, pero hoy estamos viendo animales en malísimo estado sanitario, que están heridos, que están débiles, y lo peor de todo es que cuando hacemos la denuncia ante la Policía, ellos no actúan", afirma Moreyra.

También indicó qué "la mayoría, por no decir todos estos animales son robados, o comprados a alguien que los robó, y ninguno de los que los usa puede presentar papeles acreditando la procedencia del animal. Y lo que más indigna, es que más allá de todo, la persona que está usando a este animal, tampoco quiere detenerse a escuchar cuando les decimos algo, o aparecen los que nos plantean que lo dejemos, porque está trabajando".

La asociación reclama más presencia por parte del Estado, para aplicar con más firmeza las leyes vigentes de protección animal, ya que comprueban desolados, que la educación "hasta ahora no viene siendo suficiente, y no nos da resultados".

Fuente: www.perspectivasur.com

Los caballos que pasan sus vidas atados a carros de los que tirar tanto en Buenos Aires como en otros lugares son utilizados como medio de tracción y padecen por ello enormemente. Desde Igualdad Animal pensamos que la mejor forma de ayudar a estos animales es dejar de utilizarles y promover el boicot a esta forma de explotación animal, pues la aplicación de las leyes de protección animal con mayor firmeza no implican que estos animales sean respetados y centran su atención únicamente en el modo en que son utilizados.
