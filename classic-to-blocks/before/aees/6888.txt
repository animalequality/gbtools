<p><img alt="" class="wp-image-9872" src="/app/uploads/2013/01/lobeznos.jpg" style="width: 300px; float: left; margin-left: 10px; margin-right: 10px;" />La organizaci&oacute;n &quot;<a href="http://lobomarley.org/" target="_blank">Ciudadanos por el Lobo y por el Mundo Rural</a>&quot; ha lanzado una campa&ntilde;a de recogida de firmas por internet para evitar el exterminio de lobos en la provincia de &Aacute;vila. Esta iniciativa surge en respuesta a la petici&oacute;n enviada por la organizaci&oacute;n agroganadera &quot;Alianza por la Unidad del Campo&quot; a 22 ayuntamientos de la provincia para declarar sus t&eacute;rminos municipales como &ldquo;territorio libre de lobos&rdquo;.</p>

<p>La campa&ntilde;a busca cien mil firmas que se quieren entregar no s&oacute;lo al presidente de la Junta de Castilla y Le&oacute;n, Juan Vicente Herrera, sino tambi&eacute;n al ministro de Agricultura, Alimentaci&oacute;n y Medio Ambiente, Miguel Arias Ca&ntilde;ete, y al alcalde de &Aacute;vila, Miguel &Aacute;ngel Garc&iacute;a Nieto.</p>

<p>&ldquo;Ciudadanos por el Lobo y por el Mundo Rural&rdquo; ha considerado un acto ilegal e inmoral que se quieran exterminar los lobos de este territorio. Esta asociaci&oacute;n ha afirmado que ese exterminio es un crimen y ha recordado que el lobo es una especie protegida al sur del r&iacute;o Duero, como es el caso de &Aacute;vila.</p>

<p>Para esta organizaci&oacute;n, &quot;que una especie, de forma natural, recupere territorio de su h&aacute;bitat natural es una muestra de que las cosas no van mal&quot;. Tambi&eacute;n han a&ntilde;adido que &ldquo;Si esa expansi&oacute;n natural genera conflictos hay que buscar el equilibrio, pero pretender eliminar todos los lobos de una provincia es una irresponsabilidad y un disparate&quot;.</p>

<p><a href="http://www.change.org/es/peticiones/presidente-de-la-junta-de-castilla-y-le%C3%B3n-evita-el-exterminio-del-lobo-en-%C3%A1vila?utm_source=share_petition&amp;utm_medium=url_share&amp;utm_campaign=url_share_before_sign" target="_blank">Firma para evitar el extermino del lobo en &Aacute;vila&nbsp;</a><br />
&nbsp;</p>

