La forma en que nos relacionamos con los animales que conviven con nosotros es estrecha y muy personal. Reconocemos que <strong>en cada uno de ellos hay un individuo que siente </strong>y que puede ser feliz o desdichado según lo que le ocurra y cómo sea tratado, exactamente igual que nosotros. Protegemos a nuestros perros y gatos de enfermedades, peligros y no querríamos ni imaginar verlos sufrir.

Y aunque sabemos que existen personas que verían a nuestros perros y gatos como comida, la sola idea nos repugna y nos preguntamos cómo alguien no podría reconocer en sus miradas y actitudes que son individuos únicos que desean vivir. Si algo tenemos muy claro es que <strong>ellos son nuestra familia y jamás terminarían en nuestros platos</strong>.

No obstante todo lo anterior, la verdad salta a la vista cuestionando todo lo que sostiene a esta sesgada visión que mantenemos de los animales: <strong>los cerdos son animales dotados de una inteligencia</strong> superior a las de nuestros perros y gatos y comparable con la de chimpancés y delfines. Las gallinas protegen a sus hijos y a los de otras especies (perros y gatos) del frío y de cualquier peligro y se comunican con sus pollitos antes de que salgan del cascarón, y ellos al nacer aprenden a emitir un piar que solo su madre puede reconocer.

&nbsp;

<img class="wp-image-13352" style="margin-left: 10px; margin-right: 10px; float: left; width: 424px; " src="/app/uploads/2018/10/shutterstock_386881855_1.jpg" alt="" />

Las vacas pueden pasar días mugiendo desconsoladas, parto tras parto, cada vez que sus hijos, los terneros que nos abastecen de carne, les son arrebatados. Los pollos, animales dotados de gran inteligencia y sensibilidad, son solo después de los peces los animales más maltratados en el mundo y a quienes se mata siendo bebés de apenas 42 días de vida.

Todas estas evidencias indican que detrás de las miradas de los animales considerados de granja como las vacas, pollos, cerdos, gallinas o corderos, hay individuos que sienten y <strong>quieren vivir tanto como aquellos animales a quienes consideramos nuestra familia</strong>. Y es entonces cuando nos formularnos una pregunta básica: en un mundo donde podemos alimentarnos perfectamente sin dañar a los animales, ¿por qué no hacerlo?

Hoy es el Día Mundial de los Animales y hace apenas dos día fue el Día Mundial de los Animales de Granja. La diferenciación entre una celebración y otra es la confirmación de que aún nos queda por hacer en cuanto a reevaluar nuestra visión y relación con todos los animales. Independientemente de que ocurra en una calle a la vista de todos o en una granja y matadero industrial el maltrato hacia los animales es el mismo.

&nbsp;

&nbsp;

&nbsp;
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</h4>
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;">¡Suscríbete gratuitamente a nuestro e-boletín!</span></a></h4>
&nbsp;

Nuestro trabajo de investigación tiene como objetivo arrojar luz sobre esta realidad. Mostramos lo que ocurre a los animales dentro de la ganadería precisamente <strong>para darles la voz que la industria ha querido silenciar </strong>y para que así su sufrimiento no quede en el olvido. Creemos que los consumidores tienen derecho a saber lo que ocurre a los animales antes de que la carne llegue a sus platos para que puedan tomar decisiones compasivas que los ayuden.

&nbsp;

<img class="wp-image-13353" style="margin-left: 10px; margin-right: 10px; float: left; width: 416px; " src="/app/uploads/2018/10/43102809842_37d72e2e9f_z.jpg" alt="" />

A poco kilómetros de nuestros hogares todos los días los animales son sometidos a los más terribles maltratos en las granjas y mataderos y, de igual manera, muy cerca de nosotros podemos comprar productos que fácilmente sustituyen a la carne y que evitan toda esta crueldad. <strong>Solo tenemos que decidir qué echaremos en el carrito del supermercado</strong>.

Recientemente lanzamos <a href="https://loveveg.mx/">Love Veg México</a>, una plataforma donde encontrarás toda la información necesaria para comenzar a probar más platillos vegetales. Cada semana podrás encontrar nuevas recetas vegetarianas, información nutricional sobre proteínas vegetales, testimonios y tips de personas que siguen una alimentación vegana, noticias sobre productos y restaurantes y consejos sobre la transición a una dieta saludable, sostenible y libre de crueldad. Ya se puede acceder a <a href="https://es.loveveg.com/" target="_blank" rel="noopener">Love Veg</a>, a su <a href="https://www.youtube.com/channel/UCm71ueLKEd06FXW6nmBLCGw" target="_blank" rel="noopener">canal de YouTube</a> y también a su cuenta en <a href="https://www.instagram.com/loveveg_es/" target="_blank" rel="noopener">Instragram</a>.

&nbsp;
