San Francisco - La pesca y la contaminación ambiental está llevando a la muerte de un buen número de nutrias de mar de California.

Según el investigador David Jessup, "la mortalidad de las nutrias constituye la señal de un medioambiente marino degradado". Su índice de mortalidad es muy elevado.

Actualmente las nutrias de mar, quienes antes vivían en toda la costa del Estado (más de 1.000 kilómetros), se encuentran confinadas a un espacio entre Santa Bárbara (al noroeste de Los Angeles) y la bahía Half Moon (al sur de San Francisco), lo que constituye unos 400 kilómetros.
