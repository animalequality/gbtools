<div class="center"><img class="wp-image-10650" src="/app/uploads/2016/05/ham.jpg" /></div>
<p><img alt="" class="wp-image-10649" src="/app/uploads/2016/05/noticiajueves_0.jpg" style="float:left; margin:10px; width:450px" />El laboratorio estadounidense de an&aacute;lisis alimentarios Clear Labs ha realizado un an&aacute;lisis de ADN a 258 muestras de carne de hamburguesas. El objeto del an&aacute;lisis era hacerse una idea general de la higiene tanto en grandes cadenas de hamburguesas como en peque&ntilde;os minoristas.</p>

<p>El m&eacute;todo de an&aacute;lisis empleado buscaba hallar <strong>agentes pat&oacute;genos, contaminaci&oacute;n de ADN, ingredientes desaparecidos y a&ntilde;adidos</strong>, entre otras variables.</p>

<p>El resultado del an&aacute;lisis da una imagen de las cadenas de hamburguesas <a href="https://igualdadanimal.org/noticia/2016/04/06/es-la-carne-el-nuevo-tabaco/">bien alejada</a> de la que estas empresas ofrecen en su publicidad.</p>

<p>De las 258 muestras analizadas, el an&aacute;lisis encontr&oacute; problemas de sustituci&oacute;n de ingredientes (ingredientes que no constaban en el etiquetado) en 16 casos, contaminaci&oacute;n pat&oacute;gena en el 13,6% de las muestras.</p>

<p>En dos casos de los analizados, el laboratorio encontr&oacute; <strong>restos de carne en hamburguesas vegetarianas</strong>. En <strong>14 muestras</strong> Clear Labs encontr&oacute; &laquo;ingredientes desaparecidos&raquo;, productos que constaban en el etiquetado pero no en la carne de hamburguesa.</p>

<p>En cuanto al ADN, el an&aacute;lisis apunta que <strong>en tres casos se encontr&oacute; ADN de rata</strong> (uno en una hamburguesa vegetariana) y <strong>en uno de ellos ADN humano</strong>.</p>

<p>Por &uacute;ltimo, <strong>en 11 muestras se hallaron pat&oacute;genos</strong> (microorganismos que pueden originar enfermedades), Yersinia pseudotuberculosis como el m&aacute;s abundante.</p>

<p>&nbsp;</p>

