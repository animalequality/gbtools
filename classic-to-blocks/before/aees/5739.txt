<p><img alt="" height="189" class="wp-image-13990" src="/app/uploads/2010/10/gafas.jpg" style="margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 223px; " width="223" />Igualdad Animal ha enviado esta ma&ntilde;ana dos pares de gafas graduadas para miop&iacute;a a los inspectores de la Consejer&iacute;a de Medio Rural de la Xunta de Galicia.<br />
	<br />
	Esperamos que estas gafas sirvan a los inspectores que revisan las granjas de visones gallegas para reconocer la forma que tiene un vis&oacute;n con sus dos patas traseras y su cola mutiladas y en carne viva o el rostro destrozado de los animales que hemos documentado en el transcurso de nuestra investigaci&oacute;n y cuyas im&aacute;genes pueden ver en nuestro sitio web <a href="http://www.granjasdevisones.es">GranjasdeVisones.es</a><br />
	<br />
	Habida cuenta de las declaraciones de Jos&eacute; Miguel Mart&iacute;n Manzanares de la Organizaci&oacute;n Empresarial Espa&ntilde;ola de la Peleter&iacute;a en un medio impreso nacional en el que afirmaba &quot;que se resiste a creer&quot; que las im&aacute;genes difundidas correspondan a la realidad ya que &quot;si esto fuera as&iacute;, los inspectores tendr&iacute;an que haberlo detectado&quot; afirmando adem&aacute;s que &eacute;stos &quot;las revisan peri&oacute;dicamente&quot;, Igualdad Animal considera entonces que la &uacute;nica explicaci&oacute;n a la paradoja expuesta por el se&ntilde;or Mart&iacute;n es que dichos inspectores adolecen de un alto grado de miop&iacute;a que les impide detectar en sus visitas lo que cualquiera podr&iacute;a ver f&aacute;cilmente.<br />
	<br />
	No consideramos que aumentar las inspecciones ni sancionar a una granja en particular sea la soluci&oacute;n al problema de ra&iacute;z que plantea la existencia de estas granjas. Mientras existan lugares en los que se confina y mata a cientos de miles de animales para la confecci&oacute;n de prendas de vestir, la injusticia de las mismas ser&aacute; evidente para todos, a excepci&oacute;n quiz&aacute;s de los inspectores de la Xunta y los propios peleteros.<br />
	<br />
	Pensamos que la &uacute;nica soluci&oacute;n al problema sobre la injusticia que plantea la existencia de las granjas de visones consiste en la prohibici&oacute;n de dichas granjas tal y como ya han hecho en Inglaterra y Austria.<br />
	<br />
	Por &uacute;ltimo queremos hacer p&uacute;blica nuestra intenci&oacute;n de continuar con esta campa&ntilde;a hasta el cierre de la &uacute;ltima de estas granjas incrementando en los pr&oacute;ximos d&iacute;as nuestras acciones para tal fin.<br />
	<br />
	<strong>Contacto de medios</strong><br />
	Sharon N&uacute;&ntilde;ez<br />
	Presidenta y portavoz de Igualdad Animal</p>

