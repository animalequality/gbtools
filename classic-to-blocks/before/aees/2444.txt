Caracas - Dentro de los actos y protestas que Igualdad Animal realizará en diferentes países en contra los zoos, ayer Sábado día 7 de Abril se realizó la primera protesta de este tipo en la ciudad de Caracas (Venezuela).

El acto que congregó a una decena de activistas de la organización en dicho país, duro unas horas y diarios como "El Universal" cubrieron la noticia. Andrés Lizardo, representante de Igualdad Animal en Venezuela, informó que "todo lo que queremos es que la gente sepa que los animales merecen su hábitat, desarrollar su vida junto a sus familias y no estar encerrados".

El mismo acto se realizó ayer con éxito en el zoo de Madrid y se llevará a cabo en diversas ciudades durante todo el mes de Abril.
