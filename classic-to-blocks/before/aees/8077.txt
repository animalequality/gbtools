Con el objetivo de reducir su huella medioambiental, contribuir con productores locales y fomentar mejores hábitos alimenticios, 4 ciudades de Brasil se han comprometido a ofrecer menús basados en proteínas vegetales en las cafeterías de sus escuela públicas a partir de 2019.

Las ciudades son Serrinha, Barroca, Teofilandia y Biritinga en el estado nororiental de Bahía, y, de acuerdo a los organizadores de este programa, serán las primeras en todo el mundo en sumarse a un compromiso de este tipo.

El proyecto «Escola Sustentável» («Escuela Sostenible»), liderado por  la organización Humane Society International y la Fiscalía Loca logrará la reducción del consumo de carne, lácteos y huevos en un estimado del 25% por semestre, llegará a 30.000 estudiantes y tendrá un impacto en 23 millones de comidas cada año.

Pero esta no es el primera vez que las escuelas brasileñas cambian a menús basados en proteínas vegetales. Desde 2009, el distrito escolar de la ciudad de São Paulo, el más grande del país, ha participado en Meatless Mondays sirviendo más de medio millón de comidas vegetales cada dos semanas, según comentaron los organizadores del programa.

Además del terrible sufrimiento que causa a los animales, la industria ganadera consume grandes cantidades de agua, tierra y energía. También es uno de los mayores contribuyentes a las emisiones de gases de efecto invernadero causadas por el ser humano y que impulsan el cambio climático, generando un 15% por ciento de las emisiones totales. Este tanto por ciento representa más emisiones que todo el sector transportes mundial junto, incluidos coches, barcos y aviones.

El programa «Escuela Sostenible» comenzó hace dos semanas y cuenta con su propio chef, André Vieland, quien durante cuatro días estuvo liderando los entrenamientos culinarios para los cocineros de las escuelas y comparte recetas veganas hechas con productos locales en sus redes sociales. Las comidas que solían contener proteínas animales como carne de vaca, cordero, pollo, pescado, huevos, leche y mantequilla, ahora consistirán en soja, leche de arroz, mantequilla de maní (en lugar de mantequilla), verduras, tubérculos, granos y pan de trigo integral.

Diversos estudios científicos comprueban que la reducción en el consumo de proteínas de origen animal y el aumento de la ingesta de verduras, frutas, legumbres y cereales, reduce los niveles de colesterol, la presión arterial y los riesgos de sufrir enfermedades cardíacas, diabetes y cáncer.

En 2014, el Ministerio de Salud brasileño anunció: «Optar por consumir varios tipos de alimentos de origen vegetal y un consumo limitado de productos animales provoca indirectamente un sistema alimentario más justo y menos estresante para el medio ambiente, los animales y la biodiversidad en general».

Los organizadores del programa señalaron que la mitad de la población de Brasil tiene sobrepeso u obesidad, incluido uno de cada tres niños de entre 5 y 9 años. Los estudiantes que se incorporen al programa se harán exámenes periódicos que evalúen la ferritina, la vitamina B12, el colesterol total, los triglicéridos y los niveles de glucosa en sangre. El peso, la altura y la composición corporal también se medirán.

Según Leticia Bair, directora del proyecto «Escuela Sostenible», «proporcionar a nuestros distritos escolares comidas basadas en proteínas vegetales ayudará a ahorrar recursos ambientales y financieros públicos, permitirá un futuro de adultos sanos y construirá un mundo justo para los animales».
