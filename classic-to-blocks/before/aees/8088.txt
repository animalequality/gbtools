La empresa emergente de tecnología alimentaria Terramino Foods desarrolló recientemente una hamburguesa vegetal que se ve, sabe, huele y tiene el perfil nutricional del salmón. Kimberlie Le y Joshua Nixon, cofundadores de la empresa, comenzaron a trabajar en el proyecto cuando cursaban un programa de aceleración dirigido por IndieBio en la Universidad de California.

«Nos preguntamos: ¿existe algo en la naturaleza (que no sea una planta) que tenga el mismo tamaño de fibra que la fibra muscular animal?», comentó Le. Terminaron descubriendo que los hongos tienen una textura muy parecida a la carne y no tienen ese sabor fuerte que suelen tener las plantas ricas en proteínas. Y cuando se le añaden algas, la hamburguesa toma el mismo sabor y valor nutricional del salmón.

De acuerdo con Fast Company, la hamburguesa de Terramino no solo se ve, huele y sabe como el salmón, sino que, al igual que el pescado, es una completa fuente de proteínas con ácidos grasos omega-3.

Terramino planea lanzar sus primeros productos veganos, que son un filete y una hamburguesa, a finales de este mismo año. En 2019, el equipo planea aumentar su producción, y para esto incluirá otras alternativas a productos marinos y carne elaborados a partir de hongos.
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales?</h4>
<h4 style="text-align: center;">¡Suscríbete gratuitamente a <a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958"><span style="color: #0000ff;">nuestro e-boletín</span></a>!</h4>
Cada año se pescan en todo el mundo casi 3 billones de peces. Según <span style="color: #0000ff;"> <a style="color: #0000ff;" href="https://www.wwf.es/informate/actualidad_y_prensa/?36100/El-colapso-de-las-pesqueras-y-la-mala-salud-del-ocano-amenazan-el-suministro-de-alimentos" target="_blank" rel="noopener">el informe “Living Blue Planet”</a></span> (Planeta azul vivo) de la organización internacional <span style="color: #333333;">WWF</span> «en una sola generación, la actividad humana ha dañado gravemente los océanos  capturando peces más rápido de lo que pueden reproducirse, mientras se destruyen sus zonas de alimentación...». Se estima que para el año 2050 los océanos podrían no tener vida animal.

Entre las ventajas de la hamburguesa y filete de pescado de Terramino Foods se encuentran:
<ol>
 	<li>El pescado de origen vegetal no conlleva ningún riesgo de contaminación por microplásticos, antibióticos o mercurio.</li>
 	<li>Es más sostenible desde el punto de vista ambiental que el pescado. Debido a la pesca masiva y al cambio climático las poblaciones marinas han disminuido casi un 50% desde 1970. Las poblaciones de algunas especies han disminuido hasta un 75%.</li>
 	<li>No hay ningún misterio en torno a su fuente o cadena de suministro, como suele ser el caso de los productos del mar comprados en la tienda o en el restaurante.</li>
</ol>
Mientras que el mercado de las proteínas vegetales que sustituyen la carne de pollo y vaca sigue creciendo aceleradamente con gigantes de empresas cárnicas como principales inversionistas, Terramino Foods apuesta por la creación de sustitutos vegetales al pescado. Y si logran posicionar su productos sería una ventaja sobre un área muy lucrativa y menos explotada que tiene, además, un impacto muy positivo para los animales y el planeta.

&nbsp;
