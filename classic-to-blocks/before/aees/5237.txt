
<p>
	<a href="https://www.flickr.com/photos/igualdadanimal/4231973767/" title="2009 - Madrid - Pancarta Nochevieja - New Year's Eve Banner por Igualdad Animal  |  Animal Equality, en Flickr"><img align="left" alt="2009 - Madrid - Pancarta Nochevieja - New Year's Eve Banner" height="180" hspace="10" src="http://farm3.static.flickr.com/2722/4231973767_2a6d671415_m.jpg" width="240" /></a></p>
<p>
	Durante las campanadas de la Nochevieja del 2009 y mientras millones de personas se encontraban frente al televisor para celebrar la llegada del 2010, varios activistas de Igualdad Animal han desplegado una pancarta en la Puerta del Sol con el lema &quot;Tauromaquia Abolici&oacute;n&quot;.</p>
<p>
	Esta acci&oacute;n ha sido el punto y final a un a&ntilde;o lleno de trabajo y esfuerzo por parte de los activistas de Igualdad Animal, activistas que deseamos que el lema de la pancarta que desplegamos esta noche se haga realidad y que el 2010 suponga tambi&eacute;n un enorme avance para los Derechos de los Animales.</p>
<p>
	Ay&uacute;danos a que este a&ntilde;o sigamos defendiendo a los animales y &iexcl;Feliz 2010!</p>
