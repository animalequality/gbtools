<div class="center"><img class="wp-image-9549" src="/app/uploads/2012/05/najera_pajaros.jpg" /></div>
<p>Jilgueros, herrerillos, verderones, carboneros, verdecillas... multitud de p&aacute;jaros llegan volando desde las grandes choperas de los alrededores y se topan con las cristaleras de las pistas de p&aacute;del, que no cuentan con ning&uacute;n elemento visual que muestre su ubicaci&oacute;n. El impacto contra ellas es tal que <strong>la mayor&iacute;a de estas aves caen muertas o malheridas al suelo</strong>.<br />
	<br />
	La aparici&oacute;n de p&aacute;jaros muertos en las instalaciones motiv&oacute; la denuncia de los vecinos, pero al parecer las autoridades locales han tardado en mostrar su inter&eacute;s por el problema.<br />
	<br />
	Responsables de Medio Ambiente pidieron al Ayuntamiento de N&aacute;jera que pusiera alg&uacute;n elemento, como <strong>vinilo</strong>, para ahuyentar a los p&aacute;jaros, ya que estos no ven las cristaleras o ven s&oacute;lo los reflejos de otros &aacute;rboles. Pero las medidas todav&iacute;a no se han llevado a cabo.</p>
<p>Desde la Concejal&iacute;a de Deportes se ha propuesto la colocaci&oacute;n de una pantalla de &aacute;rboles de crecimiento r&aacute;pido, como chopos, con los que las aves podr&iacute;an orientarse y encontrar a su vez refugio, evitando as&iacute; el encontronazo con las cristaleras.<img alt="" class="wp-image-14156" src="/app/uploads/2012/05/padel_najera.jpg" style="width: 300px; float: right;" /><br />
	<br />
	Mientras llega la soluci&oacute;n, <strong>pr&aacute;cticamente todos los d&iacute;as muere alg&uacute;n p&aacute;jaro, en ocasiones varios</strong>. Y es que, aunque la zona no es un parque natural, tanto las enormes choperas como el r&iacute;o Najerilla y los riscos que la bordean son un espacio de gran calidad medioambiental, donde las aves pueden criar sin ser molestadas.</p>

