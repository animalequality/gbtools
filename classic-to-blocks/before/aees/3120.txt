Sagunto, Valencia - En las fiestas de este pueblo se empleará a 150 patos, que serán lanzados al mar.

El ayuntamiento entregará regalos a los vecinos que devuelvan a los patos a sus dueños (actualmente los animales no-humanos son considerados propiedades), quienes volverán a ser encerrados posteriormente en la granja de Cuenca de donde vienen. En el momento en que su existencia deje de ser rentable para los propietarios de la granja, esos patos serán asesinados.

