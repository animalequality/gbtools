En este vídeo podrás conocer la vida de Lili, una pata explotada en una granja de engorde. Las imágenes pertenecen a nuestra reciente investigación en una de las mayores granjas de producción de patos de Alemania, que ha conmocionado a la sociedad alemana apareciendo en los principales medios de comunicación del país.

La triste historia de Lili representa la vida de millones de animales que cada día son maltratados por la industria ganadera en formas que ni siquiera podemos imaginar. A continuación puedes leer el testimonio de Kathrin, investigadora de Igualdad Animal que estuvo grabando dentro de la granja:

"<em>Después de estar documentando las condiciones de los animales dentro de la granja, me llamó la atención una pata que estaba boca arriba sin poder moverse. Cuando me acerqué a ella, me miró con sus brillantes ojos azules. Con cuidado, deslicé mi mano por debajo de su espalda para ayudarla. Al intentar caminar puso una pata en frente de la otra pero se tropezó y volvió a caer en mis brazos… La sostuve con cuidado y la llevé hasta el dispensador de agua.</em>

<em>Apenas reaccionó. Me pregunté si ya se había rendido. Le ofrecí un poco de agua. Solo entonces empezó a beber, utilizando el pico para beber el agua de mi mano... Podía sentir el latido de su corazón, el calor de su cuerpo mientras estaba apoyada en mí y no pude evitar empezar a llorar.</em>

<em>El destino de esta pata me afectó muchísimo a pesar de que había miles como ella a nuestro alrededor. Quería ayudarles a todos en ese mismo momento</em>".

&nbsp;
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/71tym6DgitY" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
&nbsp;
