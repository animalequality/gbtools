<div class="center"><img class="wp-image-10655" src="/app/uploads/2016/05/peces1.jpg" /></div>
<p><img alt="" class="wp-image-10653" src="/app/uploads/2016/05/peces1_0.jpg" style="float:left; margin:10px; width:450px" />No cabe duda: un pez siente tanto como lo pueda hacer tu perro o tu gato. La pregunta es, y ahora, &iquest;qu&eacute;?</p>

<p>A pesar de que los peces piensan y sienten como los mam&iacute;feros, <strong>no existe ninguna ley</strong> que proteja a los pobladores de mares y r&iacute;os de la crueldad de la pesca industrial y las piscifactor&iacute;as.</p>

<p>De esta forma, ayudar a estos sensibles animales <a href="https://igualdadanimal.org/noticia/2015/12/10/dejaremos-los-peces-fuera-de-nuestro-amor-por-los-animales/">queda de nuestra mano</a>, <strong>las personas a quienes nos importan los animales</strong>.</p>

<p>Seg&uacute;n bi&oacute;logos y expertos en comportamiento animal, <strong>tendemos a infravalorar enormemente</strong> la diversidad y capacidades de los vertebrados marinos. Para ellos no cabe duda: los peces tienen ricas y diversas vidas interiores y complejas relaciones sociales.</p>

<p>Algunas especies marinas tienen un cerebro que proporcionalmente es equivalente en tama&ntilde;o a la de muchos mam&iacute;feros; otros son capaces de memorizar a la perfecci&oacute;n el fondo marino donde se encuentran y recordarlo hasta 40 d&iacute;as despu&eacute;s; diversas especies de peces son capaces de <strong>utilizar herramientas de forma planeada</strong> para conseguir sus prop&oacute;sitos. La lista de evidencias es interminable.</p>

<p>Sin embargo estos animales viven en vastos y oscuros h&aacute;bitats de los que poco sabemos. Tal vez por eso hemos llevado al borde de la extinci&oacute;n a especies enteras sin pararnos a considerar el sufrimiento que padecen. <a href="https://igualdadanimal.org/noticia/2016/03/30/si-hablamos-de-maltrato-animal-tenemos-que-mirar-mares-y-oceanos/">La pesca industrial y las piscifactor&iacute;as</a> son responsables <strong>del mayor n&uacute;mero de v&iacute;ctimas animales</strong> de todas las industrias de alimentaci&oacute;n.</p>

<p>Se estima que cada a&ntilde;o se arranca de su h&aacute;bitat marino a 500.000 millones de peces para ser consumidos. <strong>Si se pusieran uno detr&aacute;s de otro la gigantesca fila alcanzar&iacute;a el Sol</strong>.</p>

<p><img alt="" class="wp-image-10654" src="/app/uploads/2016/05/peces2_0.jpg" style="float:right; margin:10px; width:450px" />Sufren <strong>terribles e inhumanas muertes</strong> por asfixia o aplastamiento para proveernos de comida a nosotros, a nuestros animales de compa&ntilde;&iacute;a, a los animales de granja e incluso a los peces que son criados en piscifactor&iacute;as.</p>

<p>En esas horribles granjas industriales de peces, las piscifactor&iacute;as, <strong>literalmente no hay ninguna ley que proteja a los animales</strong>. Viven hacinados en enormes tanques donde son engordados hasta alcanzar el peso comercial adecuado. Una vez lo han alcanzado son sacrificados brutalmente.</p>

<p>A pesar de todo lo que sabemos a d&iacute;a de hoy sobre las capacidades de estos vertebrados marinos <strong>las leyes de protecci&oacute;n animal los excluyen sistem&aacute;ticamente</strong>. Ni siquiera los mares y r&iacute;os son ya <a href="https://igualdadanimal.org/noticia/2015/10/22/estamos-vaciando-los-oceanos-de-vida/">un lugar seguro para ellos</a>. Las gigantescas redes de la pesca industrial los acechan.</p>

<p>Queda en nuestras manos hacer algo por ellos. Reducir o sustituir el pescado en nuestra alimentaci&oacute;n es la medida m&aacute;s efectiva de proteger a los peces. Generar conciencia sobre su terrible sufrimiento en redes y tanques de piscifactor&iacute;as es labor de organizaciones de protecci&oacute;n animal, pero <strong>en &uacute;ltima instancia la decisi&oacute;n es del consumidor</strong>.</p>

<p>La pr&oacute;xima vez que est&eacute;s en el supermercado ante una lata de at&uacute;n o una bandeja de pescado, piensa en ello. Millones de peces te lo agradecer&aacute;n.<br />
&nbsp;</p>

