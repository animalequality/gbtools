No existe manera más sencilla y práctica: nuestra alimentación puede ayudar a acabar con el maltrato animal y detener el calentamiento global. ¿Qué más se puede pedir?, ¿que sea deliciosa y buena para nuestra salud? ¡Pues también!

A continuación te damos algunos consejos prácticos que te ayudarán <strong>a reducir o sustituir la carne en tu alimentación</strong>. ¡Enhorabuena, estarás cambiando el mundo con cada nuevo plato!

&nbsp;

<strong>1. Paso a paso</strong>

Comienza con pequeños cambios de hábitos de alimentación. <strong>Cada semana, intenta incluir un nuevo cambio que no te cueste demasiado</strong> dentro de tu rutina a las horas de las comidas. Por ejemplo, comer una hamburguesa vegetal en vez de un plato con carne.

&nbsp;

<strong>2. Comienza por la carne de pollo, luego, todo será más fácil</strong>

Por ejemplo, podrías comenzar <strong>reduciendo o sustituyendo la carne de pollo durante la primera semana</strong>. Para ello puedes usar sabrosas alternativas como garbanzos, lentejas, alubias, avena, tofu… ¡Hay muchas posibilidades!

Para inspirarte, <a href="http://www.descubrirlacomida.es/" target="_blank" rel="noopener">en este enlace puedes descargarte nuestro eBook gratuito con más de 50 recetas para ti</a>.

&nbsp;

<strong>3. Autorecordatorios</strong>

También resulta de ayuda tener <strong>pequeños recordatorios para los cambios que estás introduciendo en tu alimentación</strong>. Por ejemplo puedes poner en la puerta de tu nevera o de fondo de tu móvil una bonita foto de tu animal de granja favorito o del planeta Tierra. <strong>Este sencillo truco es muy útil porque te ayuda a recordar por qué te estás sumando al cambio</strong>.

&nbsp;

<strong>4. ¿Dudas?</strong>

¿Dudas nutricionales? ¿De dónde sacar la proteína?, ¿el hierro? ¿calcio? <strong>¡sin problemas!</strong> <a href="https://dimequecomes.com/" target="_blank" rel="noopener">En este blog</a> de la dietista-nutricionista <strong>Lucía Martínez</strong>, miembro del  grupo de nutrición de la Unión Vegetariana Española obtendrás respuestas.

&nbsp;

<strong>5. Descubre alternativas cuando vayas de compra</strong>

¿Conoces ya algunos de los nuevos productos alimenticios alternativos a la carne? ¡Cada vez están en más supermercados!<strong> Son deliciosos, versátiles y ricos en proteínas y otros nutrientes</strong>. Tofu, hamburguesas y salchichas vegetales, embutido vegetal, hummus… ¡Pruébalos y sorpréndete!

&nbsp;

<strong>6. Cuando comas fuera de casa</strong>

¿Hoy cenas fuera? ¿O te apetece comer algo rápido pero no sabes dónde hay opciones sin carne? No te preocupes, te ayudamos: <a href="https://www.happycow.net/mobile" target="_blank" rel="noopener">descarga en tu móvil</a> la aplicación HappyCow que <strong>te informará de todas las opciones vegetarianas y veganas más próximas</strong>. ¡Estés donde estés!

&nbsp;

<strong>7. No eres solo tú, hay muchas personas que están haciendo lo mismo</strong>

Estás dando <a href="https://igualdadanimal.org/noticia/2016/07/29/9-alternativas-la-carne-mas-saludables-y-llenas-de-proteinas/" target="_blank" rel="noopener">un paso muy importante para el planeta, los animales y para tu propia salud</a>. ¡No te sientas solo, <strong>hay muchas personas que también lo están haciendo</strong>! Procura conocer a grupos de personas que hayan dado el mismo paso y comparte con ellos tu experiencia. En redes sociales como Facebook es fácil encontrar grupos de personas que te apoyen. Te sentirás genial sabiendo que <strong>cada vez somos más quienes estamos cambiando el mundo</strong> con nuestras decisiones cotidianas de alimentación.

&nbsp;

<strong>8. ¡Fíjate en todo lo que estás consiguiendo!</strong>

<strong>Lo que estás haciendo es genial</strong>, no lo olvides. Cada vez que comes un plato sin carne consigues todo esto:

- Ahorras una cantidad de agua equivalente a <strong>8 días de uso personal de agua</strong>.

- Reducir el terrible sufrimiento que los animales padecen <strong>en granjas industriales y mataderos</strong>.

- Ayudando a <strong>reducir las emisiones de gases de efecto invernadero</strong> que producen el temible calentamiento global.

- Favoreciendo que las empresas usen más productos alternativos a la carne que faciliten dar los pasos a más y más personas como tú.

&nbsp;

<strong>9. Si a veces no puedes, no te culpes</strong>

Nuestra alimentación forma parte integral de nosotros. A veces podemos vernos en situaciones en las que sea difícil elegir una opción alternativa a la carne. Otras, puede que sientas el deseo de comer algo que no sea vegetal.<strong> ¡No te sientas mal!</strong> Es normal y <strong>lo importante es que sigas dando pasos </strong>que favorezcan tu cambio cuando puedas. ¡Ánimo!
