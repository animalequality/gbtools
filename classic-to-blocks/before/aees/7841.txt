<div class="center"><img class="wp-image-12213" src="/app/uploads/2017/07/beaddc8906e9894c16248eca43ce0df7.jpg" /></div>
<p>En la conferencia del Instituto de tecnolog&iacute;a alimentaria (IFT por sus siglas en ingl&eacute;s) que tuvo lugar la semana pasada en Las Vegas hubo una estrella por encima de las dem&aacute;s: <strong>las prote&iacute;nas vegetales</strong>.</p>

<p>Expertos en nuevas tendencias de la industria alimentaria apuntan a un creciente inter&eacute;s de los consumidores por <strong>alimentos elaborados con vegetales</strong>. Los productos presentados por las empresas reunidas en la conferencia ofrec&iacute;an <strong>una soluci&oacute;n</strong> vegetal para todas las necesidades alimenticias de los consumidores.</p>

<p>Empresarios, cient&iacute;ficos y activistas reunidos por el IFT estaban de acuerdo en que existen dos tendencias principales que est&aacute;n dirigiendo a los consumidores: la b&uacute;squeda de prote&iacute;nas y una alimentaci&oacute;n m&aacute;s saludable.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://cadenaser.com/ser/2017/02/08/gastro/1486576548_993325.html" target="_blank">Espa&ntilde;a, cada vez m&aacute;s vegetariana</a></strong>

<p>&nbsp;</p>

<p>La relaci&oacute;n entre consumidores y los productos vegetales est&aacute; cambiando. La percepci&oacute;n del consumidor es que estos alimentos <strong>son m&aacute;s fiables, dejan una huella ecol&oacute;gica menor y ayudan a llevar un alimentaci&oacute;n m&aacute;s sana</strong>.</p>

<p>&laquo;El consumo de productos vegetales no es solo un nicho de mercado, es una tendencia poderosa&raquo;, declaraba Steven Walton, Director de la firma de investigaci&oacute;n HealthFocus International. &laquo;Una vez que los consumidores dan el paso, pocos se vuelven atr&aacute;s&raquo;, a&ntilde;ad&iacute;a.</p>

<p><img alt="" class="wp-image-12214" src="/app/uploads/2017/07/heura_bocados_originales_delante.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>De forma interesante, Walton apuntaba que la industria alimentaria frecuentemente se pierde en t&eacute;rminos como vegano, vegetariano o flexitariano al intentar comprender la evoluci&oacute;n de los h&aacute;bitos de los consumidores. Su punto de vista es que este nuevo movimiento <strong>va m&aacute;s all&aacute; de esas definicione</strong>s y denota un cambio de relaci&oacute;n entre consumidores y productos vegetales.</p>

<p>&laquo;La mayor&iacute;a de los consumidores todav&iacute;a no son capaces de conceptualizar las alimentaciones ricas en vegetales, pero sus intereses y h&aacute;bitos de consumo est&aacute;n alineados con esta tendencia&raquo;, explicaba Walton.</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>Bocados originales Heura, de Foods For Tomorrow, prote&iacute;na vegetal de tercera generaci&oacute;n, ya a la venta. Foto cortes&iacute;a de Foods For Tomorrow.</p></FONT>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://elpais.com/elpais/2017/04/18/ciencia/1492533970_022074.html" target="_blank">&iquest;Por qu&eacute; nos preguntamos si es segura una dieta vegana?</a></strong>

<p>&nbsp;</p>

<p>Seg&uacute;n el experto analista, existe una diferencia entre las percepciones de los consumidores, sus creencias y las motivaciones que les mueven a llevar alimentaciones ricas en vegetales o vegetarianas.</p>

<p>La producci&oacute;n de nuevas generaciones de productos vegetales ricos en prote&iacute;na vegetal se ve beneficiada adem&aacute;s, <strong>por su menor huella ecol&oacute;gica</strong>. Los consumidores se identifican con marcas m&aacute;s limpias y medioambientalmente responsables.</p>

<p>Los abanderados en este cambio de h&aacute;bitos de consumo son los consumidores pertenecientes a las generaciones x y los millennials. Para estas personas, la alimentaci&oacute;n <strong>forma parte de sus valores hacia el mundo</strong>, y est&aacute; fuertemente ligada a c&oacute;mo se ven a s&iacute; mismos ante las crisis alimentarias y medioambientales que vivimos.</p>

<p><br />
Fuente: <a href="https://www.fooddive.com/news/whats-driving-consumer-desire-for-plant-based-foods/446183/" target="_blank">https://www.fooddive.com/news/whats-driving-consumer-desire-for-plant-based-foods/446183/</a></p>

