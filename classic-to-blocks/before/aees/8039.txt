Jo-Anne McArthur es una fotógrafa canadiense que <strong>ha dedicado su vida a recorrer el mundo para documentar a los animales en el entorno humano:</strong> granjas, laboratorios, circos, acuarios y mercados; así como en santuarios y su hábitat natural. Su proyecto <a href="https://weanimalsmedia.org/" target="_blank" rel="noopener">«We Animals»</a> comenzó en 1998 y, como su nombre lo indica, es intencionalmente amplio, siendo su objetivo derribar las barreras que los humanos hemos construido y que hacen que tratemos a los animales como objetos y no como los seres sensibles que son.
<h4 style="text-align: center;"><strong>¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja? </strong></h4>
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;"><strong>¡Suscríbete gratuitamente a nuestro e-boletín!</strong></span></a></h4>
Su trabajo ha sido reconocido por distintas asociaciones como el Institute for Critical Animal Studies y la Toronto Vegetarian Association, y hace unos días <strong>fue galardonada con el Premio del Público al Fotógrafo de Vida Salvaje del Año</strong>, organizado por Museo de Historia Natural de Londres.

La fotografía que le ha hecho merecedora de tal reconocimiento y que ha conmovido al mundo entero es la de Pikin, una gorila que fue rescatada por la organización Ape Action Africa de Camerún. <strong>La imagen muestra a Pikin sonriendo junto a su cuidador Appolinaire Ndohoudou mientras él la sostiene en sus brazos</strong>. Fue elegida por 20.000 personas entre 24 fotos seleccionadas de casi 50.000 que se presentaron al concurso.

Jo-Anne tomó esta fotografía cuando la gorila se trasladaba desde un refugio forestal seguro en Camerún a uno nuevo y más grande, junto con otro grupo de gorilas. Había sido sedada pero afortunadamente despertó durante el viaje <strong>para que pudiera inmortalizar tan hermoso y significativo momento que ha dado a conocer su historia en todo el mundo.</strong>

«Estoy tan agradecida de que esta imagen haya resonado en la gente y espero que pueda inspirarnos a todos a preocuparnos un poco más por los animales», aseguró la fotógrafa, y afirmó que «ningún acto de compasión hacia ellos es demasiado pequeño».

Y aunque su trabajo la lleva principalmente a documentar escenas de crueldad y <strong>su premisa es que podamos encontrar un nuevo significado a esas situaciones cotidianas, a menudo desapercibidas</strong>, también ha sido testigo, y nosotros gracias a sus increíbles proyectos, de historias de rescate y esperanza como la de Pikin que nos recuerdan que un mundo justo para los animales es posible y lo seguimos construyendo.

Fuentes:

<a href="https://www.bbc.com/mundo/noticias-43051557" target="_blank" rel="noopener">https://www.bbc.com/mundo/noticias-43051557</a>

<a href="http://www.milenio.com/cultura/foto-vida-salvaje-museo-historia-natural-londres-ganadora-imagenes-premio_0_1121887884.html" target="_blank" rel="noopener">http://www.milenio.com/cultura/foto-vida-salvaje-museo-historia-natural-londres-ganadora-imagenes-premio_0_1121887884.html</a>
