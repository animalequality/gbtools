<div class="center"><img class="wp-image-10418" src="/app/uploads/2016/01/pigfatbree.jpg" /></div>
<p>El fot&oacute;grafo alem&aacute;n, Timo Stammberger, combina su pasi&oacute;n por la fotograf&iacute;a con su pasi&oacute;n por la justicia social para contarnos una historia. <strong>Mediante im&aacute;genes visuales, estas historias pretenden dar informaci&oacute;n al p&uacute;blico que provoque una respuesta emocional, con la esperanza de crear un mundo mejor para todos</strong>. Su tem&aacute;tica incluye desde escenas espeluznantes de migraci&oacute;n clandestina hasta historias personales de refugiados.</p>

<p><img alt="" src="http://www.animalequality.net/sites/default/files/imagesimages/pigfatbreed65000.jpg" style="float:left; margin:10px 20px; width:550px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>- Fotograf&iacute;a 1: <em>Granja de engorde y cr&iacute;a de cerdos - 65.000 cerdos</em></p>

<p>La serie de fotograf&iacute;as m&aacute;s reciente de Stammberger, <a href="http://www.timostammberger.com/making-the-connection">Making the Connection</a> (Hacer la Conexi&oacute;n), <strong>muestra una vista a&eacute;rea y terrestre del mundo de las granjas industriales</strong>. Pretende hacernos ver c&oacute;mo estas granjas aisladas parecen f&aacute;bricas de procesamiento industrial, cuya finalidad es <strong>que no sospechemos nunca que en su interior habitan multitud de animales hacinados</strong>. Este secretismo de la industria consigue que los consumidores se desvinculen de la carne envasada que hay en los supermercados o en la t&iacute;pica hamburgueser&iacute;a frecuentada habitualmente.</p>

<p><img alt="" src="http://www.animalequality.net/sites/default/files/imagesimages/chickenfattening922318.jpg" style="float:left; margin:10px 20px; width:550px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>- Fotograf&iacute;a 2:<em> Granja de engorde de pollos - 922. 318 pollos</em></p>

<p>A trav&eacute;s de estas fotograf&iacute;as, Stammberger espera que <strong>empecemos a cuestionarnos la realidad que hay detr&aacute;s de la ganader&iacute;a industrial</strong> y que cambiemos nuestro modo de pensar sobre los animales a los que denominamos comida.</p>

<p><img alt="" src="http://www.animalequality.net/sites/default/files/imagesimages/poultrySH27000perhour(2).jpg" style="float:left; margin:10px 20px; width:550px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>- Fotograf&iacute;a 3:<em> Matadero de aves - 27.000 aves matadas cada hora.</em></p>

<p>Puedes ver toda la colecci&oacute;n de fotograf&iacute;as de Stammberger <a href="https://www.timostammberger.com/">aqu&iacute;</a>.</p>

