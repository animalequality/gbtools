<div class="center"><img class="wp-image-9212" src="/app/uploads/2011/11/dsc_1134.jpg" /></div>
<p>
	Equanimal e Igualdad Animal convierten la entrada del Palacio de Congresos de Valencia en un Cementerio Animal en protesta contra la experimentaci&oacute;n con animales.</p>
<p>
	Galer&iacute;a fotogr&aacute;fica: http://www.flickr.com/photos/igualdadanimal/sets/72157628146566473</p>
<p>
	Hoy viernes 25 de noviembre, a las 11 horas, las organizaciones en defensa de los animales Equanimal e Igualdad Animal convirtieron la entrada del Palacio de los Congresos de Valencia en un &quot;cementerio animal&quot; instalando cien cruces en protesta contra la experimentaci&oacute;n animal. Este acto coincidi&oacute; con la clausura del XI Congreso Nacional de la Sociedad Espa&ntilde;ola para las Ciencias del Animal de Laboratorio (SECAL) que se ha venido desarrollando estos d&iacute;as en el Palacio de Congresos de Valencia.</p>
<p>
	Cada cruz llevaba la foto de un animal v&iacute;ctima de experimentos y represent&oacute; a las millones de v&iacute;ctimas de la experimentaci&oacute;n animal. Con este acto se quiere provocar un debate en la sociedad sobre la explotaci&oacute;n de los animales en general y sobre la experimentaci&oacute;n con animales en particular. Seg&uacute;n datos del Ministerio de Medio Ambiente y Medio Rural y Marino, un total de 1,40 millones de animales murieron en los laboratorios espa&ntilde;oles en 2009, lo que representa un aumento del 56,3 por ciento respecto al a&ntilde;o anterior. Esta cifra no refleja el n&uacute;mero real de v&iacute;ctimas de la experimentaci&oacute;n con animales en Espa&ntilde;a, dado que en dicho informe no se contabiliza a los animales invertebrados que son tambi&eacute;n capaces de sentir y con intereses propios, por lo que la cifra exacta ha de ser necesariamente mucho mayor.</p>
<p>
	Pensamos que el avance de la ciencia no puede estar justificado a toda costa, y apostamos por un avance de la &eacute;tica. Por una ciencia sin v&iacute;ctimas. Se debe invertir en las alternativas a la experimentaci&oacute;n con animales, y cuestionar un avance cient&iacute;fico que discrimina a los animales por el simple hecho de no pertenecer a nuestra especie. Un prejuicio denominado especismo, an&aacute;logo a otras discriminaciones arbitrarias como el sexismo o el racismo.</p>
<p>
	Igualdad Animal y Equanimal somos dos organizaciones por los derechos animales que trabajamos para denunciar las injusticias que padecen los animales y luchar por sus derechos.</p>
<p>
	Contacto de Medios</p>
<p>
	Alba Garc&iacute;a Bernal&nbsp; -&nbsp; Tel: 646 250 649 &nbsp;</p>

