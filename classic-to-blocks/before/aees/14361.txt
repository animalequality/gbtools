Colonia - Un zoológico alemán anunció esta semana que contratará los servicios de un payaso profesional para mantener ocupados a gorilas, chimpancés y orangutanes y de este modo evitar los comportamientos derivados del aburrimiento, producto de su cautiverio.

Según fuentes del zoológico, el aburrimiento es el causante de enfermedades y comportamientos agresivos estos animales, por lo cual acudieron a las muecas y trucos de Peters, una payasa profesional, quien "tendrá la responsabilidad de evitar que decaiga el ánimo de los chimpancés".

"Se ponen como locos cuando ven qué estoy llegando porque saben que van a divertirse", dijo Peters al diario alemán Express.

Esta triste noticia debería alertarnos de las terribles vidas que padeden los animales en los zoos de todo el mundo. Los zoos son prisiones en las que viven encarcelados cientos de animales con el objetivo de hacer disfrutar a quienes les visitan. Las jaulas, celdas y otros espacios más o menos reducidos son las "casas" donde se obliga a vivir encerrados a muchos animales, lejos de sus hábitats naturales e igual de alejados de una vida satisfactoria.

El hecho de encerrar animales es injusto en sí mismo, independientemente de las condiciones de su cautiverio. Desde Igualdad Animal nos oponemos a que seres que desean ser libres se vean privados de serlo.
