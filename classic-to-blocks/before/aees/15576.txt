<p><strong><a href="http://www.lateral.com/noticias/transparencia-lateral/">La cadena de restaurantes Lateral ha confirmado </a></strong>a Igualdad Animal que todos los huevos que utiliza en sus establecimientos son libres de jaula.&nbsp;</p>

<p><img alt="" class="wp-image-12607" src="/app/uploads/2017/11/3_2_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>Es uno de los primeros restaurantes en Espa&ntilde;a en hacer p&uacute;blico este compromiso y muestra c&oacute;mo la tendencia en el sector alimentario en Espa&ntilde;a avanza a toda velocidad hacia modelos libres de la jaula en la producci&oacute;n de huevos.&nbsp;</p>

<p>El pasado mi&eacute;rcoles 17 de enero, <a href="https://igualdadanimal.org/noticia/2018/01/17/lidl-espana-ya-no-vende-huevos-de-gallinas-enjauladas/">Lidl Espa&ntilde;a anunci&oacute; </a>que desde el 1 de enero ha dejado de vender huevos de jaula en todos sus supermercados.&nbsp;Una tendencia mundial que ha llegado a Espa&ntilde;a y est&aacute; acelerando el cambio hacia sistemas libres de jaula.&nbsp;</p>

<p>Estos acuerdos son <strong>una clara respuesta a la creciente demanda de los consumidores</strong> para una mayor transparencia y mejor trato para los animales en el sistema alimentario.</p>

<p>&laquo;Al hacer p&uacute;blica esta &nbsp;informaci&oacute;n, en la que confirma que todos los huevos que utliza en sus establecimientos son libres de jaula, Restaurantes Lateral forma parte de el importante grupo de empresas que en todo el mundo est&aacute;n apostando por la mejora de las condiciones de los animales que sufren en las granjas industriales&raquo;, declara Javier Moreno, cofundador y director internacional de Igualdad Animal. Y a&ntilde;ade: &laquo;Aunque libre de jaula no implica libre de maltrato, este sistema en la ganader&iacute;a industrial es uno de los m&aacute;s crueles con los animales. Es indispensable que las empresas respondan a las demandas crecientes de la ciudadan&iacute;a y se comprometan a dejar de apoyarlo&raquo;.</p>

