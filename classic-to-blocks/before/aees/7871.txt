<div class="center"><img class="wp-image-12325" src="/app/uploads/2017/08/vegano_montanista_koisher_everest_2.jpg" /></div>
<p>Cuando Kuntal Joisher descubri&oacute; su afici&oacute;n por la naturaleza hace 8 a&ntilde;os, ni en sus sue&ntilde;os m&aacute;s descabellados se hubiese imaginado a s&Iacute; mismo escalando monta&ntilde;as.</p>

<p>Entre 2009 y 2014 escal&oacute; con &eacute;xito varios picos en Las Gaths Occidentales, Nepal y Patagonia. Y tras m&aacute;s de 15 expediciones de trekking en la India y un tramo de las cumbres m&aacute;s altas del mundo en la cordillera del Himalaya, <strong>se convirti&oacute; en el primer vegano en llegar a la cumbre del monte Manaslu</strong>, la octava monta&ntilde;a m&aacute;s alta del mundo.</p>

<p>Pero Kuntal aspiraba a mucho m&aacute;s: quer&iacute;a ser el primer vegano en alcanzar la cumbre del Everest. Y <strong>quer&iacute;a desmentir la idea de que la dieta sin carne hab&iacute;a sido el motivo de las muertes de monta&ntilde;istas veganos y vegetarianos</strong> que intentaron alcanzar la cumbre del mundo.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>Previamente al primer ascenso de Joisher, Maria Strydom, una monta&ntilde;ista australiana experimentada y vegana muri&oacute; en su intento por subir este monte. Los tabloides, se centraron sin reparo alguno en el hecho de que hab&iacute;a muerto por ser vegana y obviaron que la gente muere constantemente en el Everest.</p>

<p>De hecho, en el Everest han muerto 275 personas en la &uacute;ltima d&eacute;cada, y la mayor&iacute;a de los medios de comunicaci&oacute;n suelen informar de sus muertes con respeto y sin hacer referencia a sus dietas.</p>

<p><img alt="" class="wp-image-12326" src="/app/uploads/2017/08/vegano_montanista_koisher_everest.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>Despu&eacute;s de tres a&ntilde;os de intenso entrenamiento, el primer intento de Joisher en escalar el monte en 2014 se vio frustrado cuando una avalancha le quit&oacute; la vida a 16 gu&iacute;as sherpa y escaladores. Fue el evento m&aacute;s mort&iacute;fero en la historia del Everest.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2016/08/30/8-deportistas-que-prueban-que-comer-carne-no-es-necesario/" target="_blank">8 deportistas que prueban que comer carne no es necesario</a></strong>

<p>&nbsp;</p>

<p>Y tras un segundo intento tambi&eacute;n fallido (un terremoto que lo detuvo), y sacudido por el hecho de haber escapado dos veces de la muerte, <strong>en un tercer intento alcanz&oacute; a los 36 a&ntilde;os la c&uacute;spide m&aacute;s alta del planeta luego de 60 d&iacute;as de expedici&oacute;n.</strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Estar en la cima del mundo con una bandera vegana fue una gran manera de retribuirle a esta causa que cambi&oacute; mi vida entera&raquo;, confiesa Joisher. &laquo;Si un individuo de 110 kg que vive una vida sedentaria, sin ning&uacute;n tipo de antecedentes en resistencia o escalada de monta&ntilde;a puede transformar su vida y subir a la cima del Monte Everest, no hay nada imposible&raquo;.</p></FONT>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-12327" src="/app/uploads/2017/08/vegano_montanista_everest_koisher.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>


<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/02/20/los-atletas-que-siguen-una-alimentacion-veggie-son-tan-o-mas-fuertes-que-aquellos-que/" target="_blank">Los atletas que siguen una alimentaci&oacute;n veggie son tan o m&aacute;s fuertes que aquellos que comen carne</a> &nbsp;</strong>

<p>&nbsp;</p>

<p>Kuntal ha derribado todos los mitos. Tras 60 d&iacute;as de ascenso <strong>demostr&oacute; al mundo que practicar alpinismo de alto nivel siguiendo una dieta vegetal es perfectamente posible.</strong></p>

<p>&Eacute;l asegura que cada uno de sus ascensos han sido una oportunidad de demostrar que somos capaces de escalar grandes monta&ntilde;as y de romper los mitos y estereotipos en torno a las alimentaciuones basadas en vegetales.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://www.latimes.com/socal/daily-pilot/tn-dpt-me-vegan-mountaineer-20161027-story.html" target="_blank">https://www.latimes.com/socal/daily-pilot/tn-dpt-me-vegan-mountaineer-20161027-story.html</a></p>

