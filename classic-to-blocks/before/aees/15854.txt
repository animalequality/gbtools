2019 será otro año de importantes logros para los animales. Gracias a tu invaluable apoyo, en 2018 conseguimos importantes avances que nos acercan más nuestro objetivo de acabar con la crueldad hacia los animales en la ganadería industrial.

Y este año, a través del trabajo de nuestros equipos de trabajo, presentes en diferentes puntos del mundo, <strong>seguiremos llevando adelante nuestras campañas para lograr cambios</strong> en nuestra sociedad que impacten en el bienestar de millones de animales.

Y los cinco puntos sobre los cuales estaremos dedicando nuestros esfuerzos serán:

<strong>1. Investigaciones</strong>

Son nuestro sello de identidad y es gracias a ellas que seguiremos mostrando al mundo todo lo que la industria de la carne, huevos y leche ocultan para dar una voz a cada animal que sufre dentro de ellas.

<strong>2. Compromisos con empresas</strong>

En total, desde 2017 hemos conseguido 100 compromisos con empresas que dejarán de proveerse de huevos de gallinas que provengan del cruel sistema de jaulas, y en 2018 también nos unimos a la <a href="https://igualdad-animal.endthecageage.eu/it-IT/live">Iniciativa Ciudadana Europea End The Cage Age</a> para pedir que se prohíba en Europa la cría de animales en jaulas en la ganadería. Y para este 2019 seguiremos avanzando hacia el logro de todos estos objetivos.

<strong>3. Love Veg</strong>

Nuestra plataforma con todo lo relacionado con un alimentación vegetal es también<strong> la herramienta para hacer frente a algunos de los retos más importantes de nuestros tiempos</strong> mientras cambiamos la vida de millones de animales. En <a href="https://es.loveveg.com/">Love Veg España</a> y en <a href="https://loveveg.mx/">Love Veg México</a> está todo para comenzar: información nutricional, consejos sobre dónde comprar y qué restaurantes visitar, ademá de que estamos listos y encantados de responder a todas tus inquietudes.

<strong>4. Trabajo legislativo</strong>

Las terribles escenas de violencia que hemos registrado en nuestras investigaciones en rastros de México no pueden quedar impunes y, por eso, este 2019 seguiremos impulsando una iniciativa legal que hará que los actos de crueldad cometidos contra animales destinados a consumo sean un delito.

<strong>5. Nuestros proyectos educativos</strong>

2018 fue otro año increíble para nuestros proyectos de concienciación social. Realizamos 2.848 screenings y logramos que 433.100 personas se sumaran a nuestro boletines nutricionales e informativos, y nuestro proyecto de realidad virtual <strong>iAnimal alcanzó las 45.193 vistas en más de 1000 eventos</strong> en diversas partes del mundo. Y para 2019 esperamos incrementar el alcance de nuestros proyectos y llevar la voz de los animales aún más lejos.
