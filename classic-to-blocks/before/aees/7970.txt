<p>&iquest;Has escuchado eso de que un vegetariano en una camioneta contamina menos que una persona que come carne en una bicicleta? Suena a chiste, pero la verdad es que es absolutamente cierto. La industria ganadera <a href="https://igualdadanimal.org/blog/7-datos-de-la-produccion-y-consumo-de-carne-que-estan-acabando-con-los-recursos-del/">es una maquinaria devastadora en t&eacute;rminos de contaminaci&oacute;n y derroche de recursos naturales</a>. &nbsp;</p>

<p>Ya en 2006 la FAO (Organizaci&oacute;n de las Naciones Unidas para la Alimentaci&oacute;n y la Agricultura) p&uacute;blico su informe &laquo;<a href="https://www.fao.org/3/a0701s/a0701s00.htm">La sombra del ganado</a>&raquo;, y alert&oacute; a los gobiernos sobre el impacto que el consumo de carne tiene en el planeta. <strong>La ganader&iacute;a industrial produce m&aacute;s gases de efecto invernadero que todos los autom&oacute;viles, barcos y aviones que hay en el mundo</strong>. Esto equivale al 15% de las emisiones totales de gases en el planeta. &nbsp;&nbsp;</p>

<p>&nbsp;</p>

<h4>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/04/20/la-cadena-de-noticias-cnn-nos-dice-hazte-vegano-salva-al-planeta/">La cadena de noticias CNN nos dice &laquo;Hazte vegano, salva al planeta&raquo;</a></h4>

<p>&nbsp;</p>

<p>La industria ganadera y la producci&oacute;n de alimentos para los animales que esta cr&iacute;a ocupa el 30% de la superficie del planeta. Pero seg&uacute;n el Instituto Internacional para la Investigaci&oacute;n Ganadera (ILRI, por sus siglas en ingl&eacute;s), si a esta cifra a&ntilde;adimos la superficie utilizada para pastos, entonces es el <strong>50% de toda la superficie del planeta la que est&aacute; ocupada para la producci&oacute;n de carne </strong>y otros productos animales.</p>

<p><img alt="" class="wp-image-12643" src="/app/uploads/2017/11/shutterstock_285980522_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Y, aunque lo anterior ya es suficientemente sorprendente, a&uacute;n hay m&aacute;s. El sistema de producci&oacute;n de carne es ineficiente. Para producir un kilo de carne (8 hamburguesas) se necesitan 16 mil litros de agua. Es much&iacute;simo, pero cuando piensas que <strong>tendr&iacute;as que beber un litro de agua al d&iacute;a durante 44 a&ntilde;os para consumir esa misma cantidad de agua</strong>, resulta absolutamente alarmante.</p>

<p>C&oacute;mo ves el planeta podr&iacute;a estar dirigi&eacute;ndose hacia la cat&aacute;strofe debido al excesivo consumo de carne. Esta situaci&oacute;n se ha empeorado debido al incremento en la capacidad de consumo en varios pa&iacute;ses como China y la India, seg&uacute;n revel&oacute; una filtraci&oacute;n de Wikileaks de un informe llevado a cabo en 2009 por la mayor empresa mundial de alimentaci&oacute;n, Nestl&eacute;.</p>

<p>&nbsp;</p>

<p>Seg&uacute;n el informe, ejecutivos de Nestl&eacute; se reunieron en secreto con autoridades de Estados Unidos para alertar sobre los siguientes puntos:</p>

<p>Nestl&eacute; considera que un tercio de la poblaci&oacute;n mundial se ver&aacute; afectada por la escasez de agua tan pronto como para el a&ntilde;o 2025. La situaci&oacute;n se volver&aacute; cr&iacute;tica en 2050. &nbsp;</p>

<p>No hay suficiente agua potable para seguir abasteciendo a una poblaci&oacute;n que se espera supere los 9 mil millones para mediados de este siglo.</p>

<p>La dieta del estadounidense promedio proporciona aproximadamente 3600 calor&iacute;as al d&iacute;a con un consumo sustancial de carne. Si en el planeta todos comi&eacute;ramos como lo hacen ellos las fuentes de agua se habr&iacute;an agotado en el a&ntilde;o 2000.</p>

<p>Para hacerte una idea m&aacute;s clara de la eficiencia y el ahorro energ&eacute;tico de un sistema de producci&oacute;n de alimentos basado en vegetales, piensa que cuando comes una hamburguesa vegetal ahorras el equivalente a 28 km de emisiones de gases conduciendo, 25 d&iacute;as de suministro de agua para beber y 7 metros cuadrados de tierra deforestada.</p>

<p>La soluci&oacute;n est&aacute; en nuestras manos. El futuro de nuestra alimentaci&oacute;n deber&aacute; estar basado en vegetales si queremos que la Tierra siga siendo un lugar habitable. Considera alimentarte con alternativas a la carne. <a href="http://www.igualdadanimal.org/noticias/7933/como-comer-menos-carne-en-5-sencillos-pasos">Ac&aacute; te decimos c&oacute;mo</a>. Tambi&eacute;n te invitamos a visitar las incre&iacute;bles webs de <a href="https://danzadefogones.com/">Danza de fogones</a> y <a href="https://reinasyrepollos.com/">Reinas y repollos</a> y los geniales canales de youtube de <a href="https://www.youtube.com/channel/UCrN5l3YrBp8CiYNv7_xf2AA/videos">Cocina Vegan F&aacute;cil</a> y <a href="https://www.youtube.com/channel/UC_XE7-_GigBlY_s4N1__Jsw/videos">Comer Vegan</a>.</p>

