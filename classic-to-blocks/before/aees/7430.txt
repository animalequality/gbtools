<p><img alt="" class="wp-image-10436" src="/app/uploads/2016/02/meatoperation.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" />La Agencia Tributaria ha lanzado &nbsp;la macrooperaci&oacute;n &lsquo;Presunto&rsquo; contra posibles fraudes que afectan a toda la cadena de producci&oacute;n y comercio mayorista de <a href="http://www.igualdadanimal.org/noticias/7427/no-vas-creer-que-estas-practicas-de-la-industria-carnica-sean-legales">la industria c&aacute;rnica</a>.</p>

<p>Se han llevado a cabo <strong>75 registros</strong> en sociedades del sector ubicadas en <strong>15 provincias espa&ntilde;olas</strong> y se est&aacute; inspeccionando a <strong>25 personas</strong> asociadas a dichas sociedades.</p>

<p>El dispositivo ha contado con <strong>330 funcionarios de la Agencia as&iacute; como 45 efectivos policiales</strong>. La serie de registros en las sociedades de <a href="https://igualdadanimal.org/blog/por-que-la-ganaderia-industrial-es-la-mayor-causante-de-maltrato-animal-de-la-historia/">la industria c&aacute;rnica</a> comenzaron el pasado martes y concluyeron ayer. Ahora se abre la fase de inspecci&oacute;n de evidencias.</p>

<p>Durante los registros se han hallado <strong>softwares de contabilidad de doble uso que permit&iacute;an a las empresas llevar la contabilidad b </strong>y ocultar as&iacute; los negocios en negro.</p>

<p>La operaci&oacute;n &lsquo;Presunto&rsquo; cont&oacute; con una primera fase de investigaci&oacute;n que, una vez concluida, ha reunido evidencias concretas que afectan a empresarios del negocio c&aacute;rnico. Se han hallado <strong>ingresos de grandes sumas de dinero</strong> tanto en las cuentas corrientes de las sociedades y de los mismos empresarios, <strong>posesi&oacute;n de bienes en el extranjero, altos niveles de vida no justificables con los sueldos obtenidos, etc</strong>.</p>

<p>La Agencia Tributaria estima que las empresas bajo sospecha de fraude ingresan al a&ntilde;o la cantidad de <strong>650 millones de euros, lo que no imped&iacute;a a nueve de estas empresas declarar p&eacute;rdidas sistem&aacute;ticas</strong>.</p>

<p><br />
&nbsp;</p>

