<div class="center"><img class="wp-image-12067" src="/app/uploads/2017/05/28768099774_792bf23804_z_0.jpg" /></div>
<p>En m&uacute;ltiples oportunidades, Igualdad Animal ha sacado a la luz p&uacute;blica la crueldad a la que miles de millones de gallinas son sometidas alrededor del mundo por la industria del huevo.</p>

<p>Sus investigaciones en Espa&ntilde;a, Alemania, M&eacute;xico, India, Italia y Brasil han logrado tener un gran impacto medi&aacute;tico que ha permitido mostrar la terrible realidad que se oculta a los consumidores.</p>

<p>En M&eacute;xico, la organizaci&oacute;n present&oacute; su investigaci&oacute;n en octubre de 2016, <strong>siendo la primera vez que en el pa&iacute;s se hac&iacute;an p&uacute;blicas im&aacute;genes de la cruel industria.</strong></p>

<p>La investigaci&oacute;n se hizo en el estado de Jalisco que es la principal regi&oacute;n en la producci&oacute;n de huevos en todo M&eacute;xico, con m&aacute;s del 50% de la poblaci&oacute;n de gallinas de todo el pa&iacute;s, que ser&iacute;a cerca de 95 millones de aves.</p>

<p>Las im&aacute;genes obtenidas muestran: &nbsp;</p>

<p>La crueldad de la muda forzada o pelecha, la cual consiste en privar a las gallinas de agua y comida por 3 d&iacute;as. Esto reduce su peso corporal y acelera el proceso de un segundo ciclo de postura de huevo.</p>

<blockquote>
<ul>
	<li>Grupos de cuatro y hasta ocho gallinas en una jaula con un espacio para cada una del tama&ntilde;o de una hoja de papel.</li>
	<li>Alta mortalidad semanal de gallinas debido a enfermedades producidas por la falta de higiene.</li>
	<li>Gallinas muertas junto a sus compa&ntilde;eras vivas en el interior de las jaulas.</li>
	<li>Gallinas a las que se les inmoviliza dentro de las jaulas para su posterior desecho.</li>
	<li>Gallinas enfermas que no reciben ning&uacute;n tipo de atenci&oacute;n veterinaria.</li>
	<li>Gallinas cuyo pico ha sido mutilado (pr&aacute;ctica est&aacute;ndar en la industria del huevo).</li>
	<li>Gallinas enfermas, abandonadas a nivel de suelo para que mueran sin poder tener acceso al agua y alimento que est&aacute; en las jaulas.</li>
	<li>Gallinas que agonizan en el interior de las jaulas sin recibir ninguna atenci&oacute;n m&eacute;dica.</li>
	<li>Gallinas que perdieron su plumaje debido al alto nivel de estr&eacute;s que provoca el hacinamiento.</li>
	<li>Gallinas con pies deformados, heridas y grietas debido al suelo de alambre de las jaulas.</li>
</ul>
</blockquote>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/no-lo-sabias-pero-las-gallinas-son-en-realidad-como-unos-gatos-o-perros-emplumados-muy/" target="_blank">No lo sab&iacute;as pero las gallinas son en realidad como unos gatos o perros emplumados muy inteligentes.</a></strong>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;La sociedad tiene derecho a saber c&oacute;mo se produce lo que consume y, en base a eso, tomar decisiones compasivas con los animales. Estas pr&aacute;cticas son legales pero no son justas&raquo;, Dulce Ram&iacute;rez, directora ejecutiva de Igualdad Animal M&eacute;xico.</p></FONT>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-12068" src="/app/uploads/2017/05/28768099224_492fc9c18b_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Poco despu&eacute;s de que se lanz&oacute; la investigaci&oacute;n <strong>Igualdad Animal inici&oacute; una campa&ntilde;a para concientizar a los consumidores e incidir en empresas del sector para que se comprometan a dejar de comprar huevos que procedan del cruel sistema de jaulas</strong>.</p>

<p><a href="https://lavidaenunajaula.igualdadanimal.mx/" target="_blank">Por favor firma aqu&iacute; para poner fin a las jaulas en bater&iacute;a</a> &nbsp;</p>

<p>Para ello tambi&eacute;n cre&oacute; el equipo de Protectores de Animales, una plataforma que permite a los consumidores y todo aquel que se preocupe por el bienestar de los animales ayudar de manera sencilla, efectiva y decisiva.</p>

<p>&nbsp;</p>

<p>El registro es sencillo. S&oacute;lo debes acceder a <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank">protectoresanimales.mx</a>, completar un par de datos y ya estar&aacute;s en contacto para que puedas realizar acciones en l&iacute;nea como enviar correos y usar las redes sociales para ayudar a los animales.</p>

<p>Igualdad Animal sigue trabajando incansablemente para que las jaulas queden en el pasado, mostrando esta terrible realidad a m&aacute;s consumidores. <strong>Ya en Italia y Brasil logr&oacute; que importantes empresas se comprometieran a dejar de proveerse de huevos que provengan de jaulas.</strong></p>

<p>La eliminaci&oacute;n de jaulas no implica la eliminaci&oacute;n del maltrato, pero si es un importante progreso porque ser&iacute;a el primer paso hacia la reducci&oacute;n del sufrimiento de estos animales.</p>

<p>Tambi&eacute;n puedes hacer mucho m&aacute;s para ayudar a las gallinas si sustituyes los huevos en tu alimentaci&oacute;n por alternativas a su consumo. &nbsp;Es muy f&aacute;cil hacerlo. <a href="http://www.igualdadanimal.org/noticias/7713/6-recetas-para-ayudarte-sustituir-los-huevos-en-tu-alimentacion" target="_blank">&iexcl;Aqu&iacute; te explicamos c&oacute;mo!</a></p>

