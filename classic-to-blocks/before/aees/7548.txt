<div class="center"><img class="wp-image-10923" src="/app/uploads/2016/08/shutterstock_140335108_-_copia.jpg" /></div>
<p>Comer a base de prote&iacute;nas animales <strong>no es saludable</strong> y las pruebas cient&iacute;ficas <a href="https://igualdadanimal.org/noticia/2015/11/10/nuevo-estudio-vincula-la-carne-con-cancer-de-rinon/" target="_blank">se amontonan</a>.</p>

<p>Investigadores del Massachusetts General Hospital acaban de publicar <a href="https://archinte.jamanetwork.com/article.aspx?articleid=2540540" target="_blank">un estudio</a> exhaustivo de larga duraci&oacute;n llevado a cabo desde los a&ntilde;os 80 en m&aacute;s de <strong>130.000 personas</strong>.</p>

<p>Los investigadores han descubierto una asociaci&oacute;n entre <strong>una alimentaci&oacute;n basada en prote&iacute;nas animales y un mayor riesgo de muerte</strong>, sobre todo como consecuencia de problemas cardiovasculares. Las personas que segu&iacute;an una alimentaci&oacute;n <strong>basada en prote&iacute;nas vegetales</strong> corr&iacute;an un menor riesgo de muerte.</p>

<p>El estudio afirma que el riego se acrecentaba en aquellos sujetos que adem&aacute;s ten&iacute;an al menos un h&aacute;bito insalubre en su estilo de vida como son la falta de ejercicio o el sobrepeso.</p>

<p>Tras comprobar los factores de riesgo relativos a estilos de vida y otros <a href="http://www.igualdadanimal.org/noticias/7511/informacion-nutricional-para-las-alimentaciones-vegetarianas" target="_blank">h&aacute;bitos alimenticios</a> los investigadores analizaron la ingesta de prote&iacute;nas animales (carne, l&aacute;cteos y huevos) y vegetales (pan, cereales, pasta, legumbres y frutos secos).</p>

<p><img alt="" class="wp-image-10924" src="/app/uploads/2016/08/shutterstock_281315621.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>El Dr. Mingyang Song, coautor del estudio, afirmaba que &laquo;los resultados sugieren que las personas deber&iacute;an considerar alimentarse con m&aacute;s prote&iacute;nas vegetales que animales&raquo;.</p>

<p>Por su parte, el Profesor Tim Key, Director del Departamento de Epidemiolog&iacute;a del C&aacute;ncer de la Universidad de Oxford, comentaba: &laquo;este estudio de larga duraci&oacute;n representa un an&aacute;lisis de alta calidad. En &eacute;l las personas que se alimentaban con m&aacute;s prote&iacute;nas de origen vegetal tend&iacute;an a ser m&aacute;s delgadas y con menos probabilidad de fumar, lo que reduce su riesgo de mortalidad&raquo;.</p>

<p>&laquo;Sobre cualquier otra cosa, el estudio viene a sumarse a la opini&oacute;n de que se deber&iacute;an promocionar los productos vegetales, incluyendo las fuentes vegetales de prote&iacute;nas, para una alimentaci&oacute;n saludable, y de que los alimentos de origen animal deber&iacute;an reducirse&raquo;, a&ntilde;ad&iacute;a.</p>

<p>&nbsp;</p>

<p>Fuente: <a href="http://www.foodnavigator.com/Ingredients/Meat-fish-and-savoury-ingredients/Heavy-intake-of-animal-protein-points-to-higher-risk-of-death-Study/?utm_source=newsletter_product&amp;utm_medium=email&amp;utm_campaign=10-Aug-2016&amp;c=mHbhxHSrmPauInTKNtvgnGCeJjcXyBv%2F&amp;p2=" target="_blank">http://www.foodnavigator.com/Ingredients/Meat-fish-and-savoury-ingredients/Heavy-intake-of-animal-protein-points-to-higher-risk-of-death-Study/?utm_source=newsletter_product&amp;utm_medium=email&amp;utm_campaign=10-Aug-2016&amp;c=mHbhxHSrmPauInTKNtvgnGCeJjcXyBv%2F&amp;p2=</a></p>

