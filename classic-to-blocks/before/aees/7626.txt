<div class="center"><img class="wp-image-11335" src="/app/uploads/2016/11/ouwiqmub.jpg" /></div>
<p>No tengas dudas: puedes llenar tu nevera de vegetales, sacar la carne y vivir saludablemente.</p>

<p><strong>Puedes introducir en tu alimentaci&oacute;n tantos platos vegetarianos como quieras</strong>. Tantos, que al final acabes por no comer carne, pescado, l&aacute;cteos o huevos. Tu salud no los echar&aacute; en falta.</p>

<p>Lo acaba de afirmar la <strong>Academia de Nutrici&oacute;n y Diet&eacute;tica de EE UU</strong> (AND, por sus siglas en ingl&eacute;s), una de las mayores autoridades del mundo en la materia, <a href="https://www.jandonline.org/article/S2212-2672%2816%2931192-3/fulltext" target="_blank">en su informe</a> &laquo;Posicionamiento de la Academia de Nutrici&oacute;n y Diet&eacute;tica sobre las dietas vegetarianas&raquo; (en ingl&eacute;s).</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Las personas que se alimentan de manera vegetariana o vegana tienen &nbsp;un menor riesgo de una serie de problemas de salud, incluida isquemia cardiaca, diabetes de tipo 2, hipertensi&oacute;n, ciertos tipos de c&aacute;ncer y obesidad&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>La AND lleva respaldando <a href="https://igualdadanimal.org/noticia/2016/03/23/las-profundas-consecuencias-de-un-modelo-alimentario-basado-en-vegetales/" target="_blank">las alimentaciones vegetarianas (vegana incluida)</a> desde el a&ntilde;o 2003, cuando emiti&oacute; su primer informe sobre las dietas sin carne. En 2009 volvi&oacute; a actualizar su apoyo y, ahora, en 2016 vuelve a hacerlo con una novedad importante.</p>

<p>Adem&aacute;s de respaldar las alimentaciones vegetarianas desde el punto de vista de la nutrici&oacute;n y la salud, <strong>incluye los beneficios medioambientales para el planeta</strong>. La producci&oacute;n de vegetales y alternativas a la carne <a href="https://igualdadanimal.org/noticia/2016/05/28/naciones-unidas-nuestro-actual-sistema-alimentario-es-insostenible/" target="_blank">es m&aacute;s sostenible y beneficiosa para combatir el calentamiento global</a>.</p>

<p><img alt="" class="wp-image-11336" src="/app/uploads/2016/11/shutterstock_335916848_-_copia.jpg" style="float:left; margin:10px; width:350px" /></p>

<p>&nbsp;</p>

<p>En su informe, la AND afirma categ&oacute;ricamente:</p>

<p>&laquo;Las dietas vegetarianas (vegana incluida) planeadas de manera apropiada son sanas, nutricionalmente adecuadas y pueden aportar beneficios para la prevenci&oacute;n y el tratamiento de ciertas enfermedades. Estas dietas son apropiadas para todas las etapas de la vida, incluyendo el embarazo, lactancia, ni&ntilde;ez, pubertad, adolescencia y la tercera edad. Tambi&eacute;n para atletas. Las dietas basadas en vegetales son m&aacute;s sostenibles medioambientalmente que las dietas ricas en productos de origen animal porque consumen menos recursos naturales y causan un impacto medioambiental mucho menor&raquo;.</p>

<p>&nbsp;</p>

<p>Y a&ntilde;ade:</p>

<p>&laquo;Las personas que se alimentan de manera vegetariana o vegana tienen &nbsp;un menor riesgo de una serie de problemas de salud, incluida isquemia cardiaca, diabetes de tipo 2, hipertensi&oacute;n, ciertos tipos de c&aacute;ncer y obesidad&raquo;.</p>

<p><strong>El tiempo de las dudas nutricionales sobre las alimentaciones vegetariana y vegana est&aacute; tocando a su fin</strong>. Los viejos mitos, ejemplificados por las preguntas &laquo;&iquest;de d&oacute;nde sacas las prote&iacute;nas?&raquo; y &laquo;&iquest;si no comes carne qu&eacute; comes?&raquo;, se derrumban ante nuestros ojos.</p>

<p>Y nosotros y los animales <a href="https://igualdadanimal.org/noticia/2016/10/25/por-que-ganaderia-industrial-equivale-maltrato-animal/" target="_blank">atrapados en la maquinaria de la ganader&iacute;a industrial</a>, a&ntilde;adimos: &iexcl;ya era hora!</p>

<p>&nbsp;</p>

<p><strong>Si quieres saber c&oacute;mo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</strong></p>

<p><strong>Adem&aacute;s, <a href="http://descubrirlacomida.es/" target="_blank">aqu&iacute; puedes descargar gratis nuestro nuevo eBook con deliciosas recetas saludables y respetuosas con los animales</a>.</strong></p>

