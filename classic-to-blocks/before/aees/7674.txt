<div class="center"><img class="wp-image-11594" src="/app/uploads/2017/02/shutterstock_95517259_0.jpg" /></div>
<p>Espa&ntilde;a tiene el m&eacute;rito de haberse puesto en contra a todos con un plan para abrir la mayor granja l&aacute;ctea industrial de Europa: una macro-explotaci&oacute;n con 20.000 vacas.</p>

<p>El lugar seleccionado ser&iacute;a la provincia de Soria y detr&aacute;s del fara&oacute;nico proyecto est&aacute; la cooperativa navarra Valle de Odieta. <strong>Incluso buena parte del propio sector l&aacute;cteo est&aacute; en contra de la macro-granja industrial</strong>, a la que tildan de &laquo;ganader&iacute;a depredadora.&raquo;</p>

<p>Organizaciones agrarias han declarado que el proyecto representa un &laquo;modelo de macro-explotaci&oacute;n industrial, intensiva e insostenible.&raquo;</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;El proyecto ocupar&iacute;a un &aacute;rea de 900 hect&aacute;reas (equivalente a 900 campos de f&uacute;tbol).&raquo;</p></FONT>

<p>&nbsp;</p>

<p>Adem&aacute;s de ser responsable del <strong>maltrato animal</strong> que conlleva inseminar artificialmente una vez tras otra a las vacas para separarlas de sus cr&iacute;as al poco de nacer, <a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/" target="_blank">la ganader&iacute;a industrial supone un problema medioambiental a escala global</a>.</p>

<p>La macro-granja industrial generar&iacute;a <strong>dos millones de litros de purines al d&iacute;a</strong> y adem&aacute;s las necesidades de agua de un proyecto de esta envergadura son enormes. Los recursos h&iacute;dricos de toda la zona est&aacute;n en riesgo y se est&aacute; teniendo que estudiar si ser&iacute;a viable medioambientalmente.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>El proyecto ocupar&iacute;a un &aacute;rea de <strong>900 hect&aacute;reas</strong> (equivalente a 900 campos de f&uacute;tbol), tanto para albergar a las vacas como para gestionar las enormes cantidades de residuos t&oacute;xicos que generar&iacute;an. El peligro de que dichos residuos <strong>se filtrasen a los acu&iacute;feros de la zona</strong> preocupa tanto a los ganaderos como a la administraci&oacute;n.</p>

<p><img alt="" class="wp-image-11595" src="/app/uploads/2017/02/shutterstock_235967143.jpg" style="float:left; margin:10px; width:550px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Este tipo de proyectos son comunes en pa&iacute;ses como China, Australia, Estados Unidos o pa&iacute;ses saud&iacute;es, <strong>en los que existen granjas industriales l&aacute;cteas de hasta 100.000 animales</strong>. Sin embargo, nunca se hab&iacute;a contemplado algo as&iacute; en Espa&ntilde;a ni en el resto de Europa.</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://www.lavozdeasturias.es/noticia/actualidad/2017/01/29/soria-prepara-terreno-mayor-granja-lactea-europa-20000-vacas/0003_201701G29P31991.htm" target="_blank">https://www.lavozdeasturias.es/noticia/actualidad/2017/01/29/soria-prepara-terreno-mayor-granja-lactea-europa-20000-vacas/0003_201701G29P31991.htm</a></p>

