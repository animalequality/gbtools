<div class="center"><img class="wp-image-12270" src="/app/uploads/2017/07/carne_limpia_2.jpg" /></div>
<p>La que parece una oficina m&aacute;s en San Francisco, Estados Unidos, alberga en su s&oacute;tano algo extraordinario: un laboratorio donde <strong>un grupo de cient&iacute;ficos desarrollan tecnolog&iacute;a que redefinir&aacute; por completo la producci&oacute;n de alimentos.</strong></p>

<p>IndieBio es una empresa financiadora de biolog&iacute;a sint&eacute;tica que respalda a startups. Y aunque apoyan diferentes tipo de esfuerzos, su mayor &aacute;rea es la comida.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/blog/carne-limpia-asi-sera-la-carne-en-un-futuro-no-tan-lejano/" target="_blank">&laquo;Carne limpia&raquo;, as&iacute; ser&aacute; la carne en un futuro no tan lejano</a></strong>

<p>&nbsp;</p>

<p>Ryan Bethencourt, su cofundador, asegura que <strong>estas empresas emergentes quieren redefinir completamente la producci&oacute;n de alimentos, reducir el impacto ambiental y acabar con la crueldad hacia los animales.</strong> &laquo;El objetivo es asegurar que las personas sigan comiendo lo que aman, pero que lo produzcan de manera que no da&ntilde;e al planeta&raquo;, a&ntilde;ade.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-12271" src="/app/uploads/2017/07/memphis_meat.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p><a href="https://upsidefoods.com/" target="_blank">Memphis Meats</a> es una de las empresas que pasaron por las manos de IndieBio y que el a&ntilde;o pasado acapar&oacute; titulares con la preparaci&oacute;n de una receta de &laquo;carne limpia&raquo; (as&iacute; se ha llamado a esta carne que no necesita criar y matar animales).</p>

<p>El proceso consiste en tomar c&eacute;lulas de un animal a trav&eacute;s de una biopsia o muestra indolora y alimentarlas con nutrientes para que las c&eacute;lulas se reproduzcan.</p>

<p>Actualmente est&aacute;n desarrollando un m&eacute;todo que permita a las c&eacute;lulas autorenovarse indefinidamente para que as&iacute;, una vez obtenidas por primera vez, no habr&iacute;a necesidad de volver a tomar muestras del animal.</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>El cofundador y director ejecutivo de Memphis Meats, Uma Valeti (centro). &copy; Memphis Meats.</p></FONT>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Nuestro objetivo es eliminar completamente al animal del proceso de producci&oacute;n de carne&raquo;, dice Uma Valeti, director ejecutivo y cofundador de la empresa.</p></FONT>

<p>&nbsp;</p>

<p>Pero <strong>la alb&oacute;ndiga de Memphis Meat no fue la primera carne limpia en el mundo</strong>. En agosto de 2013, Mosa Meat, una compa&ntilde;&iacute;a holandesa, present&oacute; la primera hamburguesa hecha completamente a partir de esta tecnolog&iacute;a.</p>

<p>Seg&uacute;n su director ejecutivo Mark Post, <strong>espera salir al mercado como producto premium en cuatro a&ntilde;os pero despu&eacute;s su precio bajar&aacute; y competir&aacute; con el precio actual de la carne de vaca.</strong></p>

<p><img alt="" class="wp-image-12272" src="/app/uploads/2017/07/carne_limpia_.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>La carne tiene un alt&iacute;simo costo para el planeta. <strong>La ganader&iacute;a industrial es uno de los mayores contribuyentes al calentamiento global </strong>ya que provoca <a href="https://www.worldwatch.org/files/pdf/Livestock%20and%20Climate%20Change.pdf" target="_blank">el 51% de las emisiones de gas de efecto invernadero</a>.</p>

<p>Adem&aacute;s, en la medida en que la escasez de agua se agrava en el planeta, ser&aacute; m&aacute;s dif&iacute;cil proporcionar los 1700 litros de agua que requiere la producci&oacute;n de una sola hamburguesa. Y, de igual manera, justificar el hecho de que los pastos para alimentar a los animales ocupen el 45% de la tierra habitable en el planeta.</p>

<p>Resulta esperanzador que muchos inversores de renombre est&eacute;n volcando sus miradas hacia la &laquo;carne limpia&raquo; ya que, sin duda, es la soluci&oacute;n a una de las amenazas m&aacute;s urgentes de nuestro tiempo.</p>

<FONT SIZE=1><FONT COLOR=#808080><p>La primera hamburguesa cultivada, cocinada y probada en vivo en 2013. &copy; culturefbeef.org</p></FONT>

<p>&nbsp;</p>

<p><br />
Fuentes:</p>

<p><a href="https://www.theguardian.com/small-business-network/2017/jul/24/lab-grown-food-indiebio-artificial-intelligence-walmart-vegetarian" target="_blank">https://www.theguardian.com/small-business-network/2017/jul/24/lab-grown-food-indiebio-artificial-intelligence-walmart-vegetarian</a></p>

<p><a href="http://nwnoticias.com/#!/noticias/el-complot-contra-la-carne" target="_blank">http://nwnoticias.com/#!/noticias/el-complot-contra-la-carne</a></p>

