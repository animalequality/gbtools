Equipos de rastreo siguen hoy la pista de once cocodrilos de unos seis metros de longitud que la semana pasada escaparon de una granja de cría de la provincia de Nakhon Ratchasima, aprovechando las inundaciones causadas por las fuertes lluvias.

Según los medios locales, otros 23 cocodrilos han sido abatidos por los grupos de cazadores encargados de esa labor, pero las autoridades provinciales reconocieron que cada vez es más difícil capturar a los que quedan en libertad.

Por ese motivo han solicitado la ayuda de expertos de un parque zoológico de Bangkok que se integrarán en breve a la batida.

'El área es enorme y los cocodrilos de gran tamaño son más expertos y pueden zambullirse permaneciendo sumergidos más tiempo que los pequeños' afirmó a la prensa local Suwira Phonkoh, funcionario del destacamento de fuerzas especiales.

Las autoridades también han advertido a los residentes de la zona que permanezcan alerta ante la presencia de los saurios.

La cría de ese animal en Tailandia ha decrecido en comparación con el pasado, pero el sector espera una recuperación tras descubrirse en la cáscara de huevo de cocodrilo una rica fuente de hidroxipatita, una substancia cálcica empleada para realizar implantes dentales y tratar fracturas óseas.


Fuente: Terra Actualidad - EFE
