Un joven fue detenido luego de que dos personas lo denunciaran por torturar a una yegua con un rebenque, aún cuando el animal había caído al suelo. Personal de Control Urbano de la Municipalidad y de la policía lograron detener al agresor.

Lejos de la indiferencia de muchos, este lunes a las 18.45 de la tarde, una mujer de profesión abogada, identificada como María A. de 55 años, decidió parar la tortura que un chico –de nombre Edgardo R, de 21 años– ejercía sobre una yegua que, dada la golpiza, estaba tirado en plena calle.

La mujer pidió colaboración a José, un hombre de 48 años que pasaba caminando y juntos se acercaron al joven y trataron de que parase con los golpes a la yegua. Además, llamaron a la policía y a un veterinario conocido.

El profesional contactó con Control Urbano municipal, cuyo personal trasladó a la yegua a un centro para su recuperación. En tanto, el responsable del animal quedó detenido en la comisaría 15ª.

Fuente: Rosario3
