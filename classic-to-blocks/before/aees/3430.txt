La traducción de su nombre es "La ladradora", pero en sus últimos días Laika seguramente no hizo más que gemir atormentada. La Unión Soviética envió el 3 de noviembre de 1957 a esta perra de dos años de edad como primer animal al espacio.

En aquel momento, los medios soviéticos afirmaron que Laika, después de varios días de vuelo en el Sputnik 2, había muerto apaciblemente, por falta de oxígeno. Sin embargo, un científico ruso reveló hace algunos años que Laika murió a las pocas horas por una combinación del recalentamiento del pequeño cubículo en el que se encontraba y el estrés sufrido a consecuencia del terror que sufrió.

El viaje a las estrellas se hizo en un cilindro de metal de 80 centímetros de largo. El oxígeno y los alimentos estaban previstos para ocho días. Asimismo no contaba con cápsula de retorno, por lo que se sabía que Laika no regresaría nunca a La Tierra.

Los ingenieros habían dispuesto en la nave lo necesario para purificar el aire, así como las técnicas necesarias de enfriamiento. Así todo, los agregados para bajar la temperatura fallaron cuando se expandió el calor de la tercera etapa del motor del cohete, que no había sido despedida.

Laika sufrió torturas en el proceso de selección (al igual que el resto de perros), tras ser secuestrada en la calle, donde vivía. Según los medios rusos, el primer requisito era que el animal pesara menos de 6 kilos y midiera menos de 35 centímetros de alto. Durante los meses previos a la partida, los perros fueron torturados encerrándoles en recintos cada vez más pequeños, buscando que se acostumbraran a dichos espacios, en algunos casos hasta tres semanas continuadas. Finalmente, la tranquilidad estoica de Laika durante el entrenamiento fue lo que facilitó la decisión.
