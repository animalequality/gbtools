<p><img alt="" class="wp-image-10266" src="/app/uploads/2015/07/scott.jpg" style="float:right; margin:5px; width:450px" /></p>

<p>El famoso corredor de carreras de ultradistancia estadounidense Scott Jurek <strong>ha vuelto a hacer historia rompiendo el r&eacute;cord del Appalachian Trail &ndash;Sendero de los Apalaches</strong>&ndash; una ruta de trekking de largo recorrido del este de los Estados Unidos con una longitud de 3.500 kil&oacute;metros.</p>

<p>Jurek culminaba la carrera en la cima del Monte Katahdin en el estado de Maine a las 2:05 de la tarde del pasado domingo 12 de julio despu&eacute;s de haber caminado durante 46 d&iacute;as, 8 horas y 10 minutos, rompiendo as&iacute; el r&eacute;cord anterior marcado por Jennifer Pharr Davis en 46 d&iacute;as, 11 horas y 20 minutos en 2011.</p>

<p><strong>Jurek volvi&oacute; a sorprender corriendo durante los &uacute;ltimos tres d&iacute;as y las tres &uacute;ltimas noches sin descanso para conseguir batir el r&eacute;cord</strong>. &ldquo;<em>Scott contin&uacute;a sorprendiendo e inspirando con su capacidad de aguantar y seguir adelante</em>&rdquo;, indic&oacute; Ricardo Balazs, director de marketing de Clif Bar, marca que ha patrocinado la carrera de Jurek. &ldquo;<em>Justo cuando las semillas de la duda comienzan a brotar, Scott las aplasta y encuentra la fuerza para terminar. Este logro es alucinante</em>&rdquo;.</p>

<p><strong>Jurek, famoso en todo el mundo por sus records hist&oacute;ricos y por romper mitos con su alimentaci&oacute;n vegana</strong>, ha superado en los m&aacute;s de 46 d&iacute;as de carrera, una lesi&oacute;n en el cu&aacute;driceps y en la rodilla y un virus estomacal que que impidi&oacute; que superara con mayor holgura el r&eacute;cord.</p>

