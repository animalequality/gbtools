MADRID.- Unos 385.000 corderos han sido asesinados con motivo de la "Fiesta del Sacrificio" musulmana, una celebración que conmemora el final de la peregrinación a la Meca y la ofrenda a Dios de este animal degollado por Abraham en lugar de su propio hijo.

El presidente de la Asociación Profesional de Salas de Despiece y Empresas Cárnicas (Aprosa), Manuel González, afirma que este ha sido el número de corderos asesinados según el rito islámico en los mataderos acreditados para ello.

Este método implica acabar con la vida del animal de una manera especial: estará tumbado por su lado izquierdo, mirando en dirección a La Meca, y la persona encargada de degollarlo, con un cuchillo, debe ser adulta.

El Aid El Adha se celebra todos los años durante el décimo día del mes lunar del Dualhuya. La fiesta rememora la historia de Abraham e Isaac, el relato bíblico en el que Dios proveyó un carnero a Abraham para que lo asesinase después de ponerle a prueba al pedirle que matara a su hijo.


Fuente: elmundo.es



NOTA: Igualdad Animal se opone al asesinato de animales en todos los ámbitos, y considera dichos asesinatos igualmente rechazables se siga un ritual o no. Para obtener más información sobre la manera en que son explotados y asesinados los corderos y otros animales "de granja", se puede visitar la web <a href=http://www.granjasdeesclavos.com>Granjas de esclavos</a>
