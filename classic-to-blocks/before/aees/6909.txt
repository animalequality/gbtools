<div class="center"><img class="wp-image-9899" src="/app/uploads/2013/02/camarles_monos.jpg" /></div>
<h4>El Equipo de Investigaciones de Igualdad Animal ha obtenido im&aacute;genes in&eacute;ditas de la <a href="http://www.experimentacionconanimales.org/camarles/" target="_blank"><strong>granja de cr&iacute;a de macacos para experimentaci&oacute;n de Camarles</strong></a>, en Tarragona, que hace p&uacute;blicas hoy domingo 10 de febrero.</h4>

<p align="justify">M&aacute;s de un centenar de <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157629665857555/" target="_blank">fotograf&iacute;as </a>muestran a <strong>animales enfermos</strong> &ndash;con sarna o afectados por herpevirus- que permanecen sin tratar evidenciando una <strong>clara desatenci&oacute;n veterinaria</strong>, as&iacute; como a animales que, debido a los trastornos psicol&oacute;gicos producidos por el de la cautividad,muerden repetidamente los barrotes de sus jaulas y los objetos de pl&aacute;stico a su alcance, lo cual supone riesgos para su salud.</p>

<p align="justify">La instalaci&oacute;n en 2002 de esta granja a las afueras de Camarles, en un lugar rec&oacute;ndito y apartado, suscit&oacute; una gran confrontaci&oacute;n con los vecinos y el ayuntamiento, que trataron de impedir su actividad. La granja, por entonces propiedad del Centre de Recherches Primatologiques, obtuvo la licencia en medio de una pol&eacute;mica unos d&iacute;as antes de la entrada en vigor de la Ley de Protecci&oacute;n Animal de Catalu&ntilde;a que prohib&iacute;a expresamente la instalaci&oacute;n de este tipo de centros.</p>

<p align="justify">&nbsp;</p>

<p align="justify"><strong>► Advertencia: el siguiente v&iacute;deo contiene im&aacute;genes de violencia contra los animales</strong></p>

<p><iframe allowfullscreen="" frameborder="0" height="512" mozallowfullscreen="" src="https://player.vimeo.com/video/59255273" webkitallowfullscreen="" width="640"></iframe></p>

<p><a href="https://vimeo.com/59255273">Granja de macacos en Camarles - Igualdad Animal</a> from <a href="https://vimeo.com/igualdadanimal">IgualdadAnimal | AnimalEquality</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

<p>&nbsp;</p>

<p><strong>Galer&iacute;a fotogr&aacute;fica de la investigaci&oacute;n:</strong></p>

<p>&nbsp;</p>

<p><a data-flickr-embed="true" href="https://www.flickr.com/photos/igualdadanimal/albums/72157629665857555" title="Camarles - Criadero de macacos para experimentación animal"><img alt="Camarles - Criadero de macacos para experimentación animal" height="555" src="https://farm7.staticflickr.com/6232/6869077648_aaef5dc4f9_z.jpg" width="640" /></a><script async src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></p>

<p>&nbsp;</p>

<hr />

<h4>El lucrativo negocio de la experimentaci&oacute;n en animales</h4>

<p align="justify"><strong>Espa&ntilde;a recibe anualmente entre 1.500 y 2.000 macacos de cola larga</strong> provenientes de Islas Mauricio [*]. Arrancados a la fuerza de sus hogares en la selva y separados de sus familias, los macacos que sobreviven a la captura y a las duras condiciones del transporte son usados en granjas de todo el mundo como m&aacute;quinas reproductoras para la industria de la vivisecci&oacute;n.</p>

<p align="justify"><a href="http://www.experimentacionconanimales.org/camarles/" target="_blank"><img alt="" src="http://www.experimentacionconanimales.org/camarles/imagenes/tatuaje.jpg" style="width: 200px; float: left;" /></a>Estos animales no son m&aacute;s que la moneda de cambio de un lucrativo negocio que se extiende desde los cazadores de Islas Mauricio hasta las empresas farmac&eacute;uticas, pasando por las agencias de transporte y las granjas de cr&iacute;a.</p>

<p align="justify">Camarles es el &uacute;nico centro de recepci&oacute;n de primates de todo el territorio espa&ntilde;ol y sirve como punto de suministro a laboratorios de experimentaci&oacute;n de toda Europa.</p>

<p align="justify">Seg&uacute;n <strong>Andrew Knight</strong>, experto europeo en veterinaria y bio&eacute;tica del Centro de Oxford para la &Eacute;tica Animal del Reino Unido, <em>&laquo;<strong>confinar a primates en entornos relativamente peque&ntilde;os y austeros por cualquier periodo de tiempo prolongado es innegablemente cruel</strong>&raquo;</em>.<br />
<br />
[*] <a href="http://www.buav.org/mauritius">Mauritius</a>. Investigaci&oacute;n de BUAV.</p>

<p>&nbsp;</p>

<hr />

<h4>Ejemplos de experimentos en macacos:</h4>

<p align="justify">Algunos de los experimentos realizados en Reino Unido sobre estos macacos de cola larga consisten en anestesiarlos y paralizarlos con un medicamento que evita que se muevan, incluso si sienten dolor. Sus cr&aacute;neos son posteriormente abiertos y les son implantados electrodos en el cerebro para comprobar la actividad neuronal. Se les mantiene bajo estas condiciones (paralizados y anestesiados de forma continuada) hasta cinco d&iacute;as, mientras les realizan todo tipo de pruebas. Una vez las pruebas finalizan, son matados y tirados a la basura, comenzando la experimentaci&oacute;n con nuevos monos.</p>

<p><a href="http://www.experimentacionconanimales.org/camarles/" target="_blank"><img alt="" src="http://www.experimentacionconanimales.org/camarles/imagenes/cabecera_peticion.jpg" style="width: 200px; text-align: justify; float: left;" /></a></p>

<p align="justify">En 2010, <strong>354 de estos macacos fueron sometidos a experimentos en los laboratorios espa&ntilde;oles</strong>, el 80% de los cuales fueron utilizados en <strong>pruebas de toxicolog&iacute;a</strong> de tipo cr&oacute;nico o subcr&oacute;nico en las que se les inyecta u obliga a ingerir a la fuerza sustancias t&oacute;xicas para ellos como <strong>pesticidas</strong>, herbicidas u otros, seg&uacute;n detalla el Informe sobre las estad&iacute;sticas de los animales utilizados para la experimentaci&oacute;n y otros fines cient&iacute;ficos publicado por el Ministerio de Agricultura, Alimentaci&oacute;n y Medio Ambiente [1].</p>

<p align="justify"><em>&laquo;La reciente prohibici&oacute;n en Espa&ntilde;a de la investigaci&oacute;n con Grandes Simios nos recuerda que tenemos una responsabilidad hacia los animales, responsabilidad que abarca tambi&eacute;n a los macacos u otros animales utilizados en experimentos. <strong>Optemos por m&eacute;todos de investigaci&oacute;n &eacute;ticos en los que no se utilicen animales</strong>.&raquo;</em> destaca Sharon N&uacute;&ntilde;ez, presidenta de Igualdad Animal.</p>

<p align="justify">[1] <a href="http://www.magrama.gob.es/es/ganaderia/temas/produccion-y-mercados-ganaderos/Espa%C3%B1a_2010_tcm7-198562.pdf" target="_blank">Informe sobre las estad&iacute;sticas de los animales utilizados para la experimentaci&oacute;n y otros fines cient&iacute;ficos, 2010. Ministerio de Agricultura, Alimentaci&oacute;n y Medio Ambiente.</a></p>

<hr />

