Recientemente, un video grabado en un restaurant de China en el cual un crustáceo se corta su propia pinza para no morir dentro de una olla hirviendo se ha hecho viral superando 1.300.000 vistas en Facebook.

El video, inicialmente publicado en la red social china Weibo, fue publicado por un usuario llamado Jiuke, quien afirmó haber decidido adoptar al crustáceo luego de presenciar su desesperada lucha por vivir. «Lo dejé vivir. Me lo llevé a casa y lo estoy criando en un acuario», afirmó cuando otros usuarios le preguntaron qué había hecho finalmente con el animal.

A principio de este año, el gobierno suizo prohibió que a las langostas, al igual que los cangrejos, langostinos y otros invertebrados se les cocine vivos debido a que son capaces de sentir dolor. Ahora los chefs deberán emplear por ley métodos de aturdimiento que no impliquen sufrimiento para estos animales.

La nueva regla entró en vigencia a partir del primero de marzo y es el resultado de una amplia revisión de las actuales leyes de protección animal del país, sustentada, además, en la evidencia científica que sostiene que langostas sí pueden sentir dolor.

&nbsp;

<iframe src="https://www.youtube.com/embed/lNOTU2jwBBA" width="815" height="460" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Un grupo de científicos dirigido por Robert Elwood, experto en comportamiento animal de la Queen's University de Belfast, consideran que langostas, cangrejos y otros crustáceos comparten con toda probabilidad esa sensibilidad al dolor. Según Elwood, el hecho de sentir dolor resulta crucial para los animales porque les permite cambiar de comportamiento tras una experiencia dañina y aumenta sus posibilidades de supervivencia.
<h4 style="text-align: center;"><span style="font-size: x-large;">¿Quieres recibir las mejores noticias de actualidad sobre los animales? </span></h4>
<h4 style="text-align: center;"><span style="font-size: x-large;">¡Suscríbete gratuitamente a <a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958"><span style="color: #0000ff;">nuestro e-boletín</span></a>!</span></h4>
«Los crustáceos vivos, incluídos las langostas, no podrán ser transportados en hielo o agua helada. Las especies marinas siempre deberán ser mantenidas en su medio natural. Y deben ser aturdidas antes de ser matadas», dicen también las nuevas medidas adoptadas por el gobierno suizo hace apenas tres meses.

Pero ya antes de esto en Italia se ha querido proteger a las langostas de la crueldad. La corte suprema italiana decretó en junio de 2017 que a las langostas no se les puede mantener en hielo en los restaurantes debido al injustificable sufrimiento que esto les provoca antes de que se les mate.

En febrero de este año fue enviada una carta conjunta a Michael Gove, secretario de medioambiente de Reino Unido, pidiendo que los cangrejos, langostas, camarones y cangrejos de río, se incluyan en la definición de «animal» en el próximo proyecto de Ley de Bienestar Animal y en la Ley de Bienestar Animal de 2006 de este país.

Desde Igualdad Animal decidimos firmar esta carta debido a que estos animales son tan merecedores de protección contra la crueldad y el abuso como cualquier otro. Además de que su bienestar actualmente se ignora por completo durante su crianza, transporte, almacenamiento en restaurantes y supermercados y durante su matanza.

Si quieres  ayudar a Crustacean Compassion a lograr su objetivo de que a los crustáceos decápodos se les proporcione la protección que merecen <a href="https://www.crustaceancompassion.org/take-action">firma la petición hoy aquí</a>.

&nbsp;
