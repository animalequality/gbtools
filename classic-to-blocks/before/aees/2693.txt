Pontevedra-Según informa La Voz de Galicia, la asociación protectora que mantiene el refugio del municipio de Ponteareas, ha denunciado que, de los 20 perros desaparecidos en las últimas semanas, seis han aparecido envenenados. 

Los causantes por el momento son desconocidos aunque desde la asociación apuntan a «intereses por parte de cazadores o el Concello porque parece que a nadie le gusta que estemos instalados aquí», según comentó la presidenta María Luisa Caride. 
