<p align="justify">&quot;<em>Ardilla gris cocida en salsa de tomate al vino tinto</em>&quot;, este es plato que sirve a sus clientes el televisivo chef Hugh Fearnley-Whittingstall, del restaurante <a href="https://twitter.com/rivercottage" target="_blank">River Cottage Canteen</a>. <strong>Siete ardillas fueron matadas</strong> en las inmediaciones del restaurante para promocionar este plato en el restaurante y aparecer en los medios como innovaci&oacute;n gastron&oacute;mica.</p>
<p align="justify"><img alt="" class="wp-image-13833" src="/app/uploads/2013/02/carne_ardilla.jpg" /></p>
<p align="justify">&nbsp;</p>
<p align="justify"><em>&laquo;Ser&aacute; interesante ver c&oacute;mo reaccionan los invitados ante la carne de ardilla, porque es bueno para nosotros ofrecer algo diferente&raquo;</em>, dijo la gerente del restaurante, Marie Mitchell.</p>
<p align="justify"><img alt="" class="wp-image-9916" src="/app/uploads/2013/02/cocinero_ardilla_p.jpg" style="float: left; width: 294px;" /></p>
<p align="justify"><img alt="" class="wp-image-13783" src="/app/uploads/2013/02/ardilla_piel.jpg" style="width: 239px; float: left;" /></p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">La inclusi&oacute;n de ardillas en el men&uacute; responde a la intenci&oacute;n de Fearnley de promover el consumo de &ldquo;alimentos silvestres&rdquo; a partir de, seg&uacute;n dice, &ldquo;fuentes sostenibles&rdquo;.</p>

