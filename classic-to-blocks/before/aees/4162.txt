Un estudio de la Universidad de Padua, dado a conocer hoy en el telediario de la televisión pública italiana RAI, ha mostrado que los peces saben contar. Su habilidad numérica se encuentra a la par de la de los monos, los delfines y los niños de entre seis y doce meses.

Según explicó a esa cadena el profesor del departamento de Psicología General, Christian Agrillo, que ha participado en el estudio coordinado por Angelo Bisazza, los peces de esa especie saben distinguir el grupo más numeroso cuando lo tiene delante.

"Estos peces, para protegerse de los predadores tienden a formar grupos numerosos. A través de los experimentos de laboratorio hemos observado que colocando un pez a un lado de una pecera frente a dos grupos de peces de número diverso, el pez solitario reconoce el mayor y tiende a agregarse a ese", explicó Agrillo.

Según el profesor, la decisión se produce siempre que el pez solitario debe escoger entre grupos de dos o tres miembros y de tres o cuatro.

Sin embargo, cuando tiene que elegir entre grupos de cinco o seis, el pez solitario no llega a ser capaz de distinguir cuál de los dos es mayor.

De ahí, los científicos han deducido que los peces mosquito, también llamado peces gambusinos o gambusias, sabe contar hasta cuatro.

Sin embargo, si la diferencia entre ambos grupos es muy grande, como cuando tiene que decidir entre grupos de ocho y dieciséis, el pez solitario es capaz de agregarse al más numeroso.

"Nuestra investigación nos ha llevado a establecer que los peces usan una especie de ŽacumuladorŽ interno para distinguir el grupo más numeroso o bien son capaces de estimar la cantidad de espacio que el grupo ocupa", explicó Agrillo, quien concluye: "Hemos aportado la primera prueba de que los peces exhiben habilidades matemáticas rudimentarias", ha señalado el psicólogo experimental Christian Agrillo, de la Universidad de de Padua (Italia).

Los científicos del estudio piensan que otras especies de peces también podrían hacer las mismas cuentas.

Fuente: Diario Hoy - 168 horas

NOTA: Igualdad Animal reivindica el respeto hacia los demás animales por el hecho de que son capaces de sentir. Esta capacidad es la única característica relevante para que un individuo sea considerado moralmente y respetado, dado que indica que se puede ver afectado por nuestros actos y tiene intereses propios. Asimismo, Igualdad Animal se opone a los experimentos realizados con animales. La publicación en esta página de datos obtenidos en experimentos con animales tiene un objetivo puramente informativo, sin que ello suponga un apoyo de Igualdad Animal a dichos experimentos.
