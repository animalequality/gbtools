El Gobierno canadiense anunció este martes que permitirá este año la caza de 275.000 focas en las aguas heladas del este del país. El número acordado este año es mayor que el del año pasado, cuando se cazaron legalmente 270.000 ejemplares debido a las malas condiciones del hielo, pero es considerablemente más bajo que el de 2006, cuando la caza aumentó hasta las 335.000 focas, informó Europa Press. Hay que tener en cuenta que estos asesinados son rechazables, sea el número que sea.

Estos individuos son disparados o golpeados hasta la muerte durante los periodos de caza, en los meses de marzo y abril. La piel es utilizada para ropa, aunque también hay un mercado creciente de grasa de foca.

Fuente: elmercuriodigital.es
