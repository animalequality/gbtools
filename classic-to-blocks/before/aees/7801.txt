<div class="center"><img class="wp-image-12061" src="/app/uploads/2017/05/shutterstock_116932882.jpg" /></div>
<p>&iquest;Te preocupan los animales y vives pregunt&aacute;ndote c&oacute;mo puedes ayudarlos? Entonces, es clave que sepas que el mayor maltrato animal en toda nuestra historia tiene lugar dentro de las granjas y mataderos de la industria de la carne.</p>

<p>Esta gigantesca maquinaria es responsable de la mayor crueldad hacia los animales que podr&iacute;as imaginar. Cada a&ntilde;o miles de millones de vacas, cerdos, pollos y otros animales sufren y son matados por su carne. Carne que nosotros los humanos ni siquiera necesitamos consumir para estar sanos y fuertes.</p>

<p>Y entre todos estos animales (apartando &uacute;nicamente a los peces), los pollos son los m&aacute;s maltratados y los que mueren en mayor n&uacute;mero.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2016/06/14/asi-fue-la-vida-del-pollo-que-compramos-en-el-supermercado/" target="_blank">As&iacute; fue la vida del pollo que compramos en el supermercado</a></strong>

<p>&nbsp;</p>

<p><strong>Hazte cuenta de que 9 de cada 10 animales consumidos en el mundo son pollos</strong>. Debido a que son m&aacute;s peque&ntilde;os que las vacas o los cerdos, se necesitan muchos m&aacute;s pollos que estos animales para poder cubrir la demanda de los consumidores. Esto significa que <strong>para producir la misma cantidad de carne que se obtiene de una sola vaca son necesarios 200 pollos. S&iacute;, &iexcl;200!</strong></p>

<p><img alt="" class="wp-image-12062" src="/app/uploads/2017/05/23199793981_36ca4c23f5_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>A los pollos se les priva desde antes de nacer de una vida en la que pueda desarrollar sus instintos b&aacute;sicos. Machos y hembras son seleccionados en su primer d&iacute;a de vida. <strong>Los m&aacute;s fr&aacute;giles son triturados vivos o desechados como basura en contenedores donde se asfixian o se les aplasta vivos con mazos</strong>.</p>

<p>Dentro de las granjas de engorde, el pollo broiler, que ha sido seleccionado artificialmente por el ser humano, crece a un ritmo antinatural. Si un humano creciera a este ritmo pesar&iacute;a 300 kilos a los dos meses de vida.</p>

<p>Los pollos sufren terribles dolores por esto ya que se les dificulta caminar al no poder soportar su peso. Muchos agonizan echados en el piso por d&iacute;as antes de morir, entre sus excrementos y sin poder alcanzar el alimento o el agua.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Puedes suscribirte gratuitamente a nuestro e-bolet&iacute;n</a> para que recibas las mejores noticias de actualidad sobre los animales y conozcas m&aacute;s opciones de alimentaci&oacute;n.</font></font></p>

<p><font size="5">&nbsp;</font></p>

<p><font size="5"><img alt="" class="wp-image-12063" src="/app/uploads/2017/05/28534189556_e26afcf57a_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></font></p>

En su d&iacute;a 40 de vida pasa lo que a cualquier animal de granja le espera en sus &uacute;ltimas horas: el cami&oacute;n que los llevar&aacute; al matadero llega a la granja. Uno a uno son metidos dentro del cami&oacute;n de forma tan brutal que les rompen las alas e, inclusive, las patas.

Y en el matadero, ya lo puedes imaginar: son colgados cabeza abajo y aturdidos con agua electrificada. Pero en el proceso muchos recuperan la consciencia y sienten c&oacute;mo su vida se va con el filo de la cuchilla clavado en sus cuellos.

Podemos hacernos una idea de lo incre&iacute;blemente gigante que resulta el maltrato al cual son sometidos estos animales a partir de las siguientes cifras: <strong>60.000 millones de pollos son matados cada a&ntilde;o y en todo momento hay 20 mil millones de pollos dentro de las granjas industriales</strong> en hacinamiento y sufriendo todo tipo de privaciones.

Todo esto se traduce tambi&eacute;n en que <strong>hay 3 pollos por cada ser humano en el planeta, todos obligados a vivir un verdadero infierno</strong> para que se produzca y venda una carne que no necesitamos consumir.

<p><font size="5"><img alt="" class="wp-image-12064" src="/app/uploads/2017/05/shutterstock_164700608_1.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 451px; " /></font></p>

<p><font size="5">&nbsp;</font></p>
Pero debes saber que t&uacute; puedes hacer mucho por ellos. <strong>Si comienzas a reducir tu consumo de pollo estar&aacute;s incrementando exponencialmente tu impacto en ayudar a los animales</strong>. Recuerda que, adem&aacute;s de los peces, son los animales m&aacute;s maltratos y matados en el planeta.

Te invitamos a visitar los websites de los chef de <a href="https://recetasveganas.net/" target="_blank">Mis Recetas Vegetarianas</a>, <a href="http://beginveganbegun.es/" target="_blank">Begin Vegan Begun</a>, <a href="https://danzadefogones.com/" target="_blank">Danza de Fogones</a>, <a href="https://whatlenalikes.es/" target="_blank">What Lena Likes</a> y <a href="https://delantaldealces.com/" target="_blank">Delantal de Alces</a>. En ellos encontrar&aacute;s incre&iacute;bles recetas para sustituir el pollo. &iexcl;Todas muy f&aacute;ciles y deliciosas!

En gran cantidad de supermercados y tiendas tambi&eacute;n puedes encontrar ya muchas alternativas, todas ellas deliciosas y mucho m&aacute;s saludables que el pollo y otro tipo de carnes. &iexcl;Te van a encantar! &nbsp;Aqu&iacute; puedes ver algunas de ellas ya disponibles en M&eacute;xico y Espa&ntilde;a.

