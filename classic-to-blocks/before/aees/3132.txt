Venezuela - El Ministerio de Alimentación y las empresas importadores de leche se han reunido para discutir sobre las subvenciones que estas recibirán.

Rafael Oropeza, ministro de Alimentación, anunció hace unos días que se subvencionará a los empresarios que importen leche en polvo entre 3.700 y 4.350 dólares por tonelada.

Roger Figueroa, presidente de la Cámara Venezolana de Industrias Lácteas, ha señalado su intención de que se aumente la explotación de animales no-humanos (recordemos que los humanos también somos animales) para obtener más leche: “Debemos producir más leche en el país, de manera que todos los esfuerzos que se hagan deberían ser para aumentar la producción nacional”.

Para obtener más información sobre las consecuencias del consumo de lácteos, se puede visitar esta página web: https://igualdadanimal.org/problematica/lacteos/
