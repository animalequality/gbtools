Canadá - El gobierno de Canadá se muestra indiferente a las presiones internacionales y a las protestas en todo el mundo realizadas por grupos de defensa de los animales contra la matanza de focas y un año más han autorizado que se lleve a cabo la masacre. El año pasado permitían cazar hasta 335.000 focas, pero los pescadores llegaron a matar 350.000

Las autoridades canadienses se vieron obligadas este año a reducir la cuota debido a las altas temperaturas experimentadas en invierno en la región meridional del Golfo de San Lorenzo, y que han provocado la desaparición de muchas de las placas de hielo en las aguas de la región.

Las focas arpa utilizan las placas de hielo flotante para dar a luz sus crías y mantenerlas a salvo durante las primeras semanas de su vida, cuando los animales no pueden nadar. Pero este año a la injustificable matanza de focas se unen la terribles condiciones del hielo debido al cambio climático, el científico del gobierno canadiense Mike Hamill dijo que al menos el noventa por ciento de las focas recién nacidas podrían haber muerto ya al Sur del Golfo debido a las terribles condiciones en las que se encuentra el hielo. A pesar de ello es probable que los cazadores atentarán contra las focas supervivientes y sus madres.

Cada año se repite una escena que tiñe de sangre el hielo de Canadá ante el rechazo de muchos países. Bélgica se ha convertido en el primer país que prohíbe la importación de productos provenientes de focas, mientras que personas de todo el mundo hacen boicot a esta industria y protestan ante sus embajadas y consulados para tratar de poner fin a esta injusticia. Ayúdanos a acabar con esta masacre.

Puedes ver imágenes del acto realizado por Igualdad Animal contra la matanza de las focas de Canadá frente a la embajada en el siguiente enlace:

<a href="https://igualdadanimal.org/noticia/2007/03/15/igualdad-animal-protesta-ante-la-embajada-de-canada-contra-la-matanza-de-focas/">Protesta de Igualdad Animal frente a la embajada</a>.

<strong>Si deseas ayudarnos a seguir luchando por los animales, <a href="_wp_link_placeholder" data-wplink-edit="true">participa en nuestras próximas actividades</a> y<a href="https://igualdadanimal.org/dona-para-ayudar-a-los-animales/"> hazte socio/a de Igualdad Animal.</a> Ayúdanos a salvar animales.</strong>
