<div class="center"><img class="wp-image-9507" src="/app/uploads/2012/04/2594173.jpg" /></div>
<div style="float: left; width: 520px; margin-bottom: 30px;">
<p style="margin-top: 30px;"><strong>ACTUALIZACIÓN (30 abril 2012): ¡Lo conseguiste! Los activistas ya han sido puestos en libertad.</strong></p>
<strong>Destinatarios de la petición: Ayuntamiento de Brescia, Defensor del Pueblo y Policía de Brescia.</strong>

Ayer 28 de abril, una manifestación contra la viviección se convirtió en un acontecimiento histórico para el movimiento de Derechos Animales cuando decenas de activistas accedieron al interior del criadero de animales para experimentación "Green Hill", en la localidad italiana de Montichiari en Brescia, para rescatar a unos 30 perros que iban a ser utilizados en experimentos.

Decenas de perros fueron rescatados a pesar de que tras unas horas la policía cerró la salida del pueblo y empezó a detener a estos activistas llegando a capturar de nuevo a 7 u 8 perros rescatados.

Hoy 12 personas, 8 mujeres y 4 hombres, están arrestadas en la cárcel de Verziano in Brescia, y aunque no se conocen los cargos que se les imputan todo apunta a que serán graves.
<p style="font-size: 90%;">Respetamos la privacidad de tus datos, puedes consultar <a href="https://igualdadanimal.org/politica-de-privacidad/">aquí nuestra política al respecto</a>.</p>

</div>
