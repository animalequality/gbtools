<p>La industria c&aacute;rnica espa&ntilde;ola es un macronegocio que genera 20.000 millones de euros al a&ntilde;o. Lo &uacute;ltimo que le importa a este multimillonario imperio es que contraigas c&aacute;ncer por consumir carne.</p>

<p><strong>Lo acaba de destapar la prensa espa&ntilde;ola a trav&eacute;s de documentos filtrados</strong>. La industria c&aacute;rnica contrat&oacute; a una consultora para &laquo;limpiar su imagen&raquo; ante <a href="http://www.iarc.fr/en/media-centre/pr/2015/pdfs/pr240_S.pdf">el informe de la Organizaci&oacute;n Mundial de la Salud (OMS) que relacionaba el consumo de carne con el c&aacute;ncer</a>.</p>

<p><a href="http://www.sprim.es/index_esp.html">La consultora Sprim</a> fue la encargada de dise&ntilde;ar y ejecutar la respuesta de la todopoderosa industria de la carne ante la crisis por <a href="https://igualdadanimal.org/noticia/2015/10/26/la-organizacion-mundial-de-la-salud-declara-cancerigena-la-carne-procesada/">la evaluaci&oacute;n de la OMS</a>. <strong>El informe fue denominado</strong> &laquo;<a href="https://s3.documentcloud.org/documents/2730412/IndustriaAlimentariaPresen.pdf">Crisis IARC-OMS</a>&raquo;. En &eacute;l la consultora Sprim detalla las medidas a tomar y resalta que no se deb&iacute;a &laquo;ofrecer a los medios de comunicaci&oacute;n testimonios de ning&uacute;n representante del sector&raquo;.</p>

<p><img alt="" class="wp-image-14202" src="/app/uploads/2016/03/practicas5.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><sub><em>Operadores procesando a animales en un matadero</em></sub></p>

<p>La consultora pretend&iacute;a as&iacute; que la respuesta fuera ofrecida &laquo;de manera indirecta&raquo; por &laquo;expertos cient&iacute;ficos&raquo; para tratar de dar m&aacute;s legitimidad a su estrategia. <strong>Los cient&iacute;ficos intervinieron en m&aacute;s de 50 ocasiones en prensa, radios y televisiones aportando la opini&oacute;n que la industria c&aacute;rnica quer&iacute;a ofrecer del informe de la OMS</strong>. Estos &laquo;expertos&raquo;, sin aclararlo en ning&uacute;n momento, <strong>hac&iacute;an el papel de portavoces indirectos</strong> de las organizaciones de la carne Interporc, Provacuno, Asici e Interovic.</p>

<p>El documento cita a cinco cient&iacute;ficos: Carmen Vidal Carou (catedr&aacute;tica de Nutrici&oacute;n y Bromatolog&iacute;a de la Universidad de Barcelona), Abel Marin&eacute; (catedr&aacute;tico Em&eacute;rito de Nutrici&oacute;n y Bromatolog&iacute;a de la Facultad de Farmacia de la Universidad de Barcelona), Carmen G&oacute;mez Candela (jefe de la Unidad de Nutrici&oacute;n Cl&iacute;nica y Diet&eacute;tica del Hospital Universitario La Paz), Antonio Villarino (Catedr&aacute;tico de Bioqu&iacute;mica de la Universidad Complutense de Madrid) y Susana Monereo (jefe del Servicio de Endocrinolog&iacute;a y Nutrici&oacute;n del Hospital Gregorio Mara&ntilde;&oacute;n). <strong>Algunos de ellos han reconocido que aceptaron dinero por parte de la industria c&aacute;rnica para ofrecer su opini&oacute;n en los medios</strong>.</p>

<p><img alt="" class="wp-image-10444" src="/app/uploads/2016/03/industriacarnicaoms.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><sub><em>Granja industrial de cerdos</em></sub></p>

<p>Las asociaciones de la carne tambi&eacute;n han dise&ntilde;ado un plan a medio plazo para seguir contrarrestando el informe de la OMS una vez que pasara la &laquo;tormenta&raquo; de los primeros d&iacute;as. <strong>Las medidas detalladas abarcan una estrategia en internet para contrarrestar cualquier informaci&oacute;n negativa sobre la carne y el c&aacute;ncer</strong>. A esta medida se la denomina &laquo;evangelizaci&oacute;n en foros y comunidades online que hablen sobre el informe de la OMS&raquo;.</p>

<p>Adem&aacute;s propone elaborar un documento &laquo;cient&iacute;fico&raquo; destinado a los profesionales de la salud que hable de las cualidades de la carne y defienda su consumo. <strong>De esta manera los propios profesionales de la salud y no la industria c&aacute;rnica estar&iacute;an haciendo el trabajo de contrarrestar a la OMS</strong>.</p>

<p><img alt="" class="wp-image-14057" src="/app/uploads/2016/03/industriacarnicaoms2.jpg" style="float:left; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><sub><em>Granja industrial de cerdos</em></sub></p>

<p>La consultora Sprim tambi&eacute;n ha planificado el uso de una website de la industria c&aacute;rnica para emitir noticias positivas sobre el consumo de carne. Esta website, CarneySalud.com, informa que <strong>est&aacute; cofinanciada por el Ministerio de Agricultura</strong>. Periodistas de <a href="https://www.eldiario.es/sociedad/estrategia-industria-carnica-informe-oms_1_1158968.html">El Diario</a>, uno de los peri&oacute;dicos que han destapado el esc&aacute;ndalo, han preguntado al Ministerio sobre esta cofinanciaci&oacute;n en una website con informaci&oacute;n fraudulenta de la industria c&aacute;rnica. El departamento de la ministra Tejerina ha contestado que <strong>&laquo;no se hace responsable de los contenidos del Portal&raquo;, a pesar de aportar dinero para su mantenimiento</strong>.</p>

<p>En total, la campa&ntilde;a de la corrupta industria de la carne ha costado <strong>m&aacute;s de 100.000 euros</strong>. Tan solo hace unos d&iacute;as esta industria se enfrentaba <a href="https://igualdadanimal.org/noticia/2016/02/19/macrooperacion-de-hacienda-contra-la-industria-carnica/">a otro esc&aacute;ndalo por una macrooperaci&oacute;n de Hacienda contra posibles fraudes que afectan a toda la cadena de producci&oacute;n y comercio mayorista</a>.</p>

