<p>
	El fundador de Facebook, Mark Zuckerberg est&aacute; llevando a cabo un nuevo reto personal. Si el a&ntilde;o pasado decidi&oacute; empezar a estudiar chino, este a&ntilde;o se ha centrado en &quot;cuidar&quot; su alimentaci&oacute;n. Zuckerberg asegura que la &uacute;nica <a href="https://igualdadanimal.org/problematica/carne/">carne </a>que come es de animales que he matado &eacute;l mismo. Y, al parecer, no come mucha.</p>
<p>
	Se trata de una nueva dieta que ha est&aacute; llevando a cabo el creador de la red social m&aacute;s grande del mundo durante este 2011, tal y como ha explicado el propio Zuckerberg a la revista Fortune en un email enviado durante su estancia en la cumbre del G-8.</p>
<p>
	Zuckerberg asegura que desde que ha decidido <a href="http://www.mataderos.info" target="_blank">matar </a><a href="http://www.investigacionesanimales.org" target="_blank">cabras, cerdos y pollos</a>, est&aacute; &quot;comiendo alimentos mucho m&aacute;s sanos&quot; y ha aprendido mucho &quot;sobre la agricultura sostenible y los animales&quot;. En este sentido, asegura que se est&aacute; volviendo pr&aacute;cticamente vegetariano.</p>
<p>
	El aprendizaje del creador de Facebook ha ido en evoluci&oacute;n. En primer lugar, empez&oacute; matando a las criaturas del mar y despu&eacute;s pas&oacute; a los de la tierra. Su primer &quot;crimen&quot; lo cometi&oacute; con una langosta que hirvi&oacute; viva. Asegura que se trat&oacute; de &quot;una muerte dif&iacute;cil, al menos emocionalmente&quot;.</p>
<p>
	Obviamente, Zuckerberg dio a conocer a sus amigos -y al resto del mundo- este nuevo reto a trav&eacute;s de Facebook. El 4 de mayo, escribi&oacute; en su muro: &quot;Acabo de matar un cerdo y una cabra&quot;. En el email, Zuckerberg explica que cada a&ntilde;o se plantea un nuevo reto personal porque, al pasar todo el d&iacute;a trabajando en Facebook, de esta manera se &quot;obliga&quot; a hacer algo diferente. En el a&ntilde;o 2009, por ejemplo, se propuso llevar corbata todos los d&iacute;as.</p>

