Lo que ocurre a los animales que se encuentran atrapados dentro de la cruel maquinaria de la industria ganadera sobrepasa a cualquiera de nuestras peores pesadillas. <strong>Las prácticas más brutales y los peores abusos se juntan en ella</strong> para hacerla responsable de provocar el mayor maltrato animal conocido.

Pero dentro de esta terrible realidad<strong> también hay historias de esperanza.</strong> Y en ellas, animales a quienes esperaba el destino que la industria selló para ellos desde el día en que nacieron, pudieron llevar la vida que siempre habían merecido, gracias a un cambio de corazón en las personas que podían hacerlo posible.

A continuación te presentamos 3 historias inspiradoras de granjeros que transformaron sus vidas y las de los animales que criaban.

<strong>1. El granjero que dejó de enviar vacas al matadero</strong>

«Siendo un granjero, tú simplemente miras para otro lado». Esto era lo que se repetía constantemente a sí mismo Mike Lanigan cada vez que enviaba una vaca al matadero.

Pero llegó el día en que<strong> no pudo seguir engañando a su conciencia.</strong> Todo cambió para él un día mientras ayudaba a un ternero recién nacido a tomar su primera leche. «Lo estaba haciendo con tanto amor, hablándole al ternero y limpiándole la cara y tratando de llevárselo a su mamá», recuerda Lanigan. Se dio cuenta que a ese mismo ternero que le inspiraba tanta ternura acabaría siendo sacrificado de una forma terrible.

Mike decidió que <strong>nunca en su vida volvería a enviar a una vaca o a un ternero al matadero </strong>y que convertiría su granja en un refugio donde todos ellos podrían vivir sus vidas en paz. Actualmente así lo hace en el refugio <a href="https://www.farmhousegardenanimalhome.com/">Farmhouse Garden Animal Home</a>, donde vive y cuida de 21 vacas, gansos, caballos y un asno de guardia llamado «Buckwheat» y se dedica a vender vegetales y jarabe de arce.


<strong>2. El granjero que comprendió lo maravillosos que son los cerdos</strong>

Tras enviar más de 2.000 cerdos al matadero durante 10 años, Bob Comis cerró su granja y decidió enviar a los cerdos que quedaban en ella a refugios de animales de granja. ¿La razón?, él mismo la explica: «mi experiencia con mis cerdos fue tan profunda e intensa que decidí no comer carne nunca más. Ser vegano se ha convertido en una parte central de mí».

Comis <strong>comenzó a sentirse cada vez más cerca de los cerdos que criaba porque le encantaba pasar tiempo con ellos. </strong>Los observaba, les puso nombres y veía como los cerdos también lo observaban y venían hasta él para buscar sus caricias.

Comis, quien pudo verdaderamente entender a los animales que enviaba al matadero, ahora está feliz con su nueva granja que actualmente produce verduras y hortalizas.

<strong>¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación? <a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente a nuestro e-boletín!</a></strong>

<strong>3. Harold Brown: de granjero a defensor de los animales</strong>

Después de dedicar la mitad de su vida a trabajar en la granja familiar, Harold Brown sufrió un ataque al corazón que lo llevó a replantear su estilo de vida y, a su vez, su conexión con los animales que por años había explotado y llamado «comida».

El cambio más profundo que experimentó <strong>fue el comenzar a reconocer a los animales como individuos:</strong> «me dí cuenta de que tienen lazos familiares, anhelan la seguridad y experimentan la alegría y la felicidad». Dejó de consumir carne y otros productos de origen animal y se convirtió en un defensor de animales y promotor de un nuevo y mejor modelo alimentario basado en vegetales y la no violencia, tal y como lo demuestra en su sitio web <a href="http://www.farmkind.org/">Farm kind</a>

Cada uno de ellos pudo reconocer que los animales son individuos que quieren vivir sus vidas igual que nosotros, y con esto cambiaron su destino. El maltrato y la crueldad son la norma en la industria ganadera, pero mucho podemos hacer para librar a los animales de tan terrible realidad.
