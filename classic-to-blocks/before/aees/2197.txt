Zúrich –Según informa  Swissinfo, una comisión presidida por un filósofo e integrada por varios científicos, han bloqueado por cuestiones éticas al menos dos proyectos que incluían experimentos con simios. La comisión recomienda desautorizar los estudios que utilizan macacos, argumentando que atentan contra la dignidad de los animales.

La legislación suiza establece que los científicos tienen que someter a la aprobación de un comité todos los experimentos con animales. El comité es el único órgano consultivo, mientras que son las Oficinas Veterinarias Cantonales las que emiten las autorizaciones siguiendo sus recomendaciones.

La Oficina Veterinaria de Zúrich ya denegó el año pasado a Daniel Kiper el permiso para realizar experimentos en monos con el fin de ayudar a los pacientes de un ictus. En esta ocasión, se han puesto del lado del científico al no seguir la recomendación de la comisión, por lo que esta ha decidido apelar ante el Departamento cantonal de Salud, bloqueando temporalmente la realización de los experimentos.

En los dos casos analizados, la comisión dictaminó que el sufrimiento de los animales era desproporcionado con relación a los beneficios que podrían aportar los experimentos a la salud humana. Los experimentos incluían la implantación de electrodos en el cerebro de los monos para realizar mediciones.

Independientemente de los beneficios obtenidos para la salud humana, la experimentación con animales no humanos es injusta. Del mismo modo que nos opondríamos a cualquier experimento sobre seres humanos en contra de su voluntad por todo lo que ello supondría (privación de libertad, aislamiento, dolor, sufrimiento), no podemos justificar que esto se haga con cualquier otro animal.

Más información sobre la experimentación en animales: http://www.igualdadanimal.org/experimentacion

