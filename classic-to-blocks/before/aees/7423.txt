<div class="center"><img class="wp-image-10429" src="/app/uploads/2016/02/torosbaleares.jpg" /></div>
<p><img alt="" class="wp-image-14309" src="/app/uploads/2016/02/torosbaleares1.jpg" style="float:left; margin-left:10px; margin-right:10px; width:400px" />En una gran d&iacute;a para los defensores de los animales, el Parlament Balear ha aprobado crear una ley para <strong>prohibir los espect&aacute;culos taurinos en todas sus modalidades</strong> y suprimir cualquier ayuda econ&oacute;mica a estas actividades.</p>

<p>La esperada votaci&oacute;n supone <strong>una estocada m&aacute;s a la tauromaquia en Espa&ntilde;a</strong>. La proposici&oacute;n no de ley surgi&oacute; de la campa&ntilde;a <a href="http://www.mallorcasinsangre.org/">Mallorca sin Sangre</a>&nbsp; de <a href="http://www.animanaturalis.org/home/es">AnimaNaturalis</a>&nbsp;y fue presentada en el Parlamento Balear por PSOE, M&Eacute;S per Mallorca, M&Eacute;S per Menorca, Podemos y Gent per Formentera.</p>

<p>Miembros de Mallorca sin Sangre estuvieron presentes durante la hist&oacute;rica votaci&oacute;n. Esta campa&ntilde;a ya hab&iacute;a conseguido que <strong>33 municipios de la isla se declarasen antitaurinos</strong>.</p>

<p>Adem&aacute;s, la iniciativa persigue <strong>prohibir cualquier espect&aacute;culo que cause sufrimiento a un animal</strong> e insta al Gobierno de Espa&ntilde;a a eliminar toda ayuda p&uacute;blica al sector taurino y a retirar la figura de Bien de Inter&eacute;s Cultural a cualquier espect&aacute;culo que maltrate animales.</p>

<p>&nbsp;</p>

<p>Distintos parlamentarios han hecho declaraciones tras la votaci&oacute;n. As&iacute;, la diputada de M&Eacute;S per Mallorca, Margalida Capell&agrave; ha calificado la &laquo;<em>fiesta</em>&raquo; taurina como macabra, a&ntilde;adiendo que &laquo;<strong><em>hay que acabar con ella por compasi&oacute;n hacia los animales</em></strong>&raquo;.</p>

<p>Por su parte, Xavier Pericay de Ciudadanos ha acusado de &laquo;<em>intervencionismo totalitario</em>&raquo; a quienes pretenden prohibir la tauromaquia ya que, seg&uacute;n &eacute;l, las corridas de toros &laquo;<em>no tienen que ver con el maltrato animal</em>&raquo;.</p>

