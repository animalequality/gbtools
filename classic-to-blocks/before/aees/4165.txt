Pájaros que pintan sus nidos de azul, insectos que se hacen regalos nupciales y monos que controlan la natalidad han formado parte del maratón científico desarrollado hoy en el Museo Nacional de Ciencia y Tecnología de Madrid.

El maratón, de periodicidad mensual, ha contado en esta ocasión con seis conferencias dedicadas a la comunicación animal y ha reunido a más de 190 profesores de secundaria y alumnos universitarios que han completado el aforo.

Fernando Peláez del Hierro, profesor de psicobiología de la Universidad Autónoma de Madrid, ha explicado el control de la natalidad en algunos monos, en los que las madres marcan a las hijas para que huelan de una forma concreta, no ovulen o retrasen su pubertad y los machos las distingan de las que sí ovulan.

Algunos gorilas pintan de forma estética, al igual que un tipo de gatos, elefantes y aves como el tilonorrinco, que construye su nido de color azul como señal de atracción sexual, según Francisco Javier de Miguel, profesor de biología de la Universidad Autónoma de Madrid.

Michel André, director del Laboratorio de Aplicaciones Bioacústicas de la Universidad Politécnica de Catalunya, ha explicado las analogías entre las señales que emiten los cachalotes para comunicarse y los tambores de algunas tribus de Senegal que no tienen tradición oral ni escrita y distinguen a sus miembros por el sonido de su tambor.

Juan Carlos Senar, jefe de Investigación del Museo de Ciencias Naturales de Barcelona, ha destacado que el color de algunos pájaros como el lugano revela su salud ya que cuanto más amarillo es, mejor se alimenta.

Angela Loeches, profesora de psicobiología de la Universidad Autónoma de Madrid y directora del evento, ha explicado que animales y humanos utilizan modos parecidos de comunicación, ya sea a través del tacto, olores, señales visuales o acústicas, como algunas parejas de monos que permanecen juntas mientras emiten unos sonidos determinados.

Fuente: Terra Actualidad - EFE

NOTA: Igualdad Animal reivindica el respeto hacia los demás animales por el hecho de que son capaces de sentir. Esta capacidad es la única característica relevante para que un individuo sea considerado moralmente y respetado, dado que indica que se puede ver afectado por nuestros actos y tiene intereses propios.
