<img class="wp-image-10304" style="float: right; margin: 10px; width: 450px;" src="/app/uploads/2015/10/prision.jpg" alt="" />Jason Brown, de 25 años, fue detenido el pasado año en un motel de Nevada en el que se encontraron varios cuerpos desmembrados de perros. Además se hallaron en la nevera de la habitación cuatro cabezas decapitadas de perros. Los fiscales informaron que Brown había matado al menos 11 perros. Desde entonces estaba en prisión.

Ahora ha sido sentenciado a 28 años de cárcel, la pena máxima contemplada por la ley para estos casos. Además no podrá pedir libertad condicional durante once años más.

El acusado conseguía los perros a través de una famosa página web de ventas e intercambio, algunos comprados y otros regalados. En el juicio testificaron algunas de las personas que dieron perros a Brown. Dijeron que lo hicieron porque parecía un hombre “<em>formal</em>”. Brown les prometía que los animales vivirían para siempre en un “<em>buen hogar</em>”.

El acusado grababa en vídeo las horribles torturas. Entre terribles gritos de dolor y agonía de los animales se le podía oír decir “<em>voy a hacer un abrigo con su piel</em>”. En algunos de los vídeos aparece con amigos, que grababan las espantosas mutilaciones. “<em>Mis favoritos son los pequeños chihuahua. Si me hago con uno de ellos, lo traeré a la casa del dolor de Jason</em>”, se mofaba en uno de los vídeos.

El juez, durante su sentencia, dijo a Brown, “<em>el sadismo y la crueldad que has mostrado es sencillamente estremecedor</em>”, y añadió “<em>lo que más me aterra de los vídeos es el hecho de que los grabaras. Eso significa que lo hacías para volver a verte torturando a esos animales. Como si fueran un trofeo</em>”.

El propio abogado de Brown, John Oakes, declaró “<em>durante 40 años de carrera he visto a personas asesinadas de todas las maneras posibles. Sin embargo este caso para mí es algo sin precedentes</em>”.
