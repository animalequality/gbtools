<p><a href="https://www.facebook.com/IgualdadAnimal"><img alt="" class="wp-image-13737" src="/app/uploads/2015/04/Seguidoresweb.jpg" style="float:right; margin:5px; width:450px" /></a>El impacto de la organizaci&oacute;n internacional Igualdad Animal en la popular red social Facebook sigue creciendo. <strong><a href="https://www.facebook.com/IgualdadAnimal">La p&aacute;gina de la organizaci&oacute;n animalista</a> </strong>acaba de superar el mill&oacute;n y medio de fans, siendo la ONG en Espa&ntilde;a con mayor impacto y seguimiento en esta red.</p>

<p>La variedad de contenidos, v&iacute;deos de las investigaciones encubiertas que realizan denunciando el maltrato animal, recetas vegetarianas, historias sorprendentes de animales y una comunicaci&oacute;n en positivo han convertido esta p&aacute;gina en todo un referente en la defensa de los animales en el mundo hispanohablante. Los seguidores son principalmente de Espa&ntilde;a, M&eacute;xico, Argentina y Colombia, aunque tambi&eacute;n hay un alto porcentaje de fans de pa&iacute;ses como Estados Unidos, Brasil, Chile y Per&uacute;.</p>

<blockquote>
<p>&quot;<em>En Igualdad Animal damos mucha importancia a las redes sociales, ya que una de las principales cosas que hacemos es ofrecer informaci&oacute;n a la ciudadan&iacute;a: sobre la situaci&oacute;n en la que se encuentran los animales en circos, zoos o granjas, y sobre las formas en las que podemos ayudarlos. Esta informaci&oacute;n es ampliamente compartida, mostrando la creciente sensibilizaci&oacute;n que existe en la sociedad sobre la defensa de los animales.</em>&quot; Indic&oacute; Javier Moreno, cofundador de Igualdad Animal.</p>

<p>&nbsp;</p>
</blockquote>

<p>&nbsp;</p>

