<p><img alt="" class="wp-image-10249" src="/app/uploads/2015/04/thumgallos.jpg" style="float:right; margin:5px; width:450px" />La documentalista de <a href="http://www.4npictures.com/">4N Picture</a> Kelly Guerin, que ya mostr&oacute; su visi&oacute;n sobre el maltrato animal con cortos como &quot;<a href="https://vimeo.com/106778779">Subasta Animal</a>&quot; y &quot;<a href="https://igualdadanimal.org/noticia/2015/04/11/jo-anne-mcarthur-presenta-la-version-en-espanol-de-su-impactante-corto-el-matadero/">El Matadero</a>&quot;, vuelve a exhibir su buen hacer con un nuevo corto documental, esta vez mostrando la crueldad de las peleas de gallos.</p>

<p>Titulado &quot;El deporte de los caballeros&quot;, que es como se le denomina en muchos pa&iacute;ses de Sudam&eacute;rica, el corto muestra en dos minutos y medio la crueldad de estos eventos, los medicamentos que son suministrados a los animales y la s&oacute;rdida atm&oacute;sfera del club gall&iacute;stico de Puerto Rico donde fue grabado.&nbsp;</p>

<p>&quot;<em>Kelly Guerin consigue hacernos reflexionar sobre el maltrato animal con un estilo &uacute;nico. Reh&uacute;ye de im&aacute;genes expl&iacute;citas pero consigue que nos pongamos en el lugar de los animales, haciendo nuestro su sufrimiento. Con su nuevo corto denunciando las peleas de gallos vuelve a hacernos reflexionar sobre la violencia de estos espect&aacute;culos, que todav&iacute;a son legales y tradici&oacute;n en algunos pa&iacute;ses como Puerto Rico, M&eacute;xico o Per&uacute;, o en Espa&ntilde;a, donde sigue siendo legal en Anadalucia y Canarias</em>&quot;.&nbsp; Manifest&oacute; Javier Moreno, cofundador de Igualdad Animal.</p>

<p>&nbsp;</p>

<p>
<div class="media_embed" height="497px" width="885px"><iframe allowfullscreen="" frameborder="0" height="497px" mozallowfullscreen="" src="https://player.vimeo.com/video/125586784?byline=0&amp;portrait=0" webkitallowfullscreen="" width="885px"></iframe></div>
</p>

