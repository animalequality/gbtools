Más de 300 corzos mueren al año ahogados en un canal de Iberdrola que se encuentra en la zona norte de la provincia de Palencia.

Según fuentes consultadas por 20minutos, el problema radica en que las vallas que rodean la instalación son muy bajas, los animales las saltan y cuando caen al agua no tienen ninguna posibilidad de salir, por lo que mueren agotados.

Además de los corzos, en el canal pierden la vida jabalíes, venados, algún lobo y perros. En sus intentos por salvarse, los animales sufren una cruel agonía, ya que tratan de subir por los lados inclinados del canal y se rasgan las patas.

La instalación, que se emplea tanto para el riego como para la producción de energía eléctrica, se encuentra situada junto al pantano de Compuerto, en Velilla del Río Carrión, y transcurre hacia el sur de la provincia, pasando por las localidades de Villalba de Guardo y Acera de la Vega, separadas entre sí por 16 kilómetros, donde tienen lugar estos accidentes.

Los cadáveres de los animales son sacados del canal y guardados en cubos de basura.

Los hechos se suceden desde hace varios años. Iberdrola conoce la situación y al parecer subió hace dos años la altura de las vallas protectoras, aunque la medida todavía resulta insuficiente porque siguen cayendo animales, y algunos cuerpos son arrastrados por la corriente. Además, hay zonas del canal que no están suficientemente protegidas.

Esta situación se ha denunciado desde varias instancias, aunque por el momento no se han adoptado más medidas de protección.

Fuente: 20minutos.es
