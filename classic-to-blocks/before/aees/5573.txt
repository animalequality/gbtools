<ul>
	<li>
		&nbsp;</li>
	<li>
		&nbsp;</li>
	<li>
		<strong>- Algunas especies, como los ping&uuml;inos, sufren mucho m&aacute;s las altas temperaturas.</strong></li>
	<li>
		<strong>- Se les dan polos de fruta y se ponen duchas y estanques para mitigar el calor.</strong></li>
	<li>
		<strong>- Los&nbsp;osos panda tienen vaporizadores y aire acondicionado.</strong></li>
	<li>
		&nbsp;</li>
</ul>
<p>
	M&aacute;s de <strong>ocho mil animales</strong> viven en el zoo de Madrid y sus cuidadores se afanen para que pasen el verano de la mejor manera posible. Polos de frutas, duchas e incluso instalaciones de aire acondicionado para mitigar los efectos del calor.</p>
<p>
	Los m&aacute;s privilegiados son <strong>los osos panda porque tienen vaporizadores de agua</strong> para asegurar una temperatura y humedad que no les provoque problemas.</p>
<p>
	Los que no tienen aire acondicionado ni vaporizadores, se tienen que conformar con <strong>bloques de fruta congelados </strong>para soportar las fuertes temperaturas. Se congelan las frutas habituales, para que tarden m&aacute;s tiempo en sacarlas, se alimenten&nbsp;y adem&aacute;s se refresquen.</p>
<p>
	<strong>Salvo los reptiles</strong>, animales acostumbrados a las temperaturas extremas, el personal del zoo suministra helados a todos los animales para que se refresquen.</p>
<p>
	&nbsp;</p>
<p>
	<a href="https://www.youtube.com/watch?v=gfP3WskNdM0"><img alt="" class="wp-image-14163" src="/app/uploads/2010/07/panda3.jpg" style="width: 544px; " /></a></p>
<p>
	<strong>M&aacute;s de ocho mil animales viven apartados de su entorno natural. El h&aacute;bitat en que se desenvuelven los ping&uuml;inos mantiene temperaturas por debajo de los 20 &ordm; C, aproximadamente. &iquest;No es este medio en libertad&nbsp; m&aacute;s adecuado para ellos?:</strong></p>
<p>
	<img alt="" class="wp-image-14032" src="/app/uploads/2010/07/gpeng1.jpg" style="width: 544px; " /></p>
<p>
	<strong>La ingesta de polos y las duchas fr&iacute;as demuestran que estos animales no deber&iacute;an estar all&iacute;.</strong></p>

