<div class="center"><img class="wp-image-10183" src="/app/uploads/2014/11/base_image.jpg"></div>
<img class="wp-image-14284" style="width: 880px;" src="/app/uploads/2014/11/team-peak-performance.jpg" alt="">
Cuando cuatro jóvenes atletas suecos de deportes extremos comenzaron hace unas semanas una de las carreras extremas más duras del mundo no sabían dos cosas. En primer lugar, que no ganarían la carrera, ya que obtendrían el puesto 12 "sólo". En segundo lugar, que ganarían algo mucho más valioso... ¡un nuevo amigo!

La carrera de aventura es un deporte extremo en el que hay que correr, montar en bicicleta, rafting y escalada. Son 700 kilómetros a través terrenos agotadores de barro, agua, montañas y selva densa - con la ayuda de mapas y GPS. Los cuatro aventureros sabían lo que hacían y estaban listos para cualquier cosa. Pero el perro Arthur les sorprendió. <img class="wp-image-14285" style="float: right; margin-left: 5px; margin-right: 5px; width: 280px;" src="/app/uploads/2014/11/team_peak2.jpg" alt="">

El capitán del equipo Mikael Lindnord y sus tres compañeros después de casi cinco días en un pequeño pueblo de montaña en los Andes, estaban a punto de comer su comida, cuando se acercaron cuidadosamente a un perro de la calle. El perro parecía muy hambriento y Lindnord le dio un bocado. A partir de ese momento, Arthur no se separó de ninguno de ellos. Los acompaño en el resto de la prueba, atravesando incluso ríos.

"Los organizadores de la carrera nos dijeron que no podíamos seguir con el animal para cruzar el río. Pero cuando nos fuimos, el perro saltó al río y nos siguió. Así que remamos hacia atrás y lo llevamos a bordo.", dijo Lindnord al diario sueco Aftonbladet.

A partir de entonces, Mikael Lindnord tuvo claro que esta amistad era duradera y decidió adptar a Arthur para llevárselo a Suecia cuando acabasen la carrera. Las leyes en Suecia son muy estrictas, pero después de un chequeo médico completo, incluyendo la vacunación contra la rabia y la implantación de chips de identificación, Lindnord recibió el esperado E-mail: Artur podía viajar con él a Suecia.

<img class="wp-image-14286" style="float: left; margin: 10px; width: 270px;" src="/app/uploads/2014/11/team_peak5.jpg" alt="">

Hace unos días, después de un viaje de 20 horas, los cinco amigos aterrizaron en el aeropuerto de Arlanda en Suecia, donde les esperaba una multitud y medios de comunicación que habían seguido la bonita historia de Arthur a través de las redes sociales.

Los próximos 120 días, Arthur tiene que estar en cuarentena, pero en ese tiempo afortunadamnete puede ser visitado por miembros del equipo. Después de ese tiempo, finalmente podrá llegar a su nuevo hogar donde una cama caliente y una familia le daran todo su cariño y amor.

Los miembros del equipo anunciaron que crearán la Fundación Arthur para concienciar y ayudar a todos los "Arthurs" que están abandonados en las calles.
