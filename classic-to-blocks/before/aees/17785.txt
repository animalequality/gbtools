<p class="p1"><span class="s1">Igualdad Animal publica un nuevo informe sobre la <a href="https://campaigns.animalequality.it/verita-industria-latte/" target="_blank" rel="noopener">industria láctea en Italia</a>, un sector que oculta muchos aspectos controvertidos a los consumidores.</span></p>
<p class="p1"><span class="s1">Las imágenes, obtenidas gracias al trabajo de nuestros investigadores en varias granjas del norte de Italia, muestran el contraste entre lo que cuenta la publicidad y lo que realmente sucede dentro de las granjas lecheras.</span></p>
<p class="p1"><strong><span class="s1">Estas son algunas de las situaciones documentadas en el video:</span></strong></p>
<p class="p1"><span class="s1">-Terneros separados de sus madres pocas horas después del nacimiento</span></p>
<p class="p1"><span class="s1">-Terneros y vacas en condiciones higiénicas inadecuadas</span></p>
<p class="p1"><span class="s1">-Terneros a los que se les aplica la pasta cáustica en ausencia del veterinario autorizado</span></p>
<p class="p1"><span class="s1">-Vacas con mastitis y heridas muy graves</span></p>
<p class="p1"><span class="s1">-Vacas con heridas de pezuñas cubiertas ilegalmente </span></p>
<p class="p1"><span class="s1">-Vacas obligadas a vivir en espacios estrechos y sucios, cubiertos de heces y barro.</span></p>
<p class="p1"><span class="s1">-Trabajadores que aplican inyecciones y controles a los animales en ausencia del veterinario designado.</span></p>
<p class="p1"><span class="s1">-Vacas sin acceso a los prados, a diferencia de lo que se muestra en los anuncios de las granjas lecheras.</span></p>
<p class="p1"><span class="s1">-La automatización de todo el proceso de producción, alienante y mecánica para los animales.</span></p>
&nbsp;

[embed]https://www.youtube.com/watch?v=C_TA9SU1x-A&feature=youtu.be[/embed]

&nbsp;
<p class="p1"><span class="s1">Todas estas prácticas demuestran una cosa: que <strong>la imagen que se transmite a diario de la industria láctea no es real</strong>, un sector en el que las vacas criadas artificialmente producen hasta 60 litros de leche al día, frente a los cuatro que producirían normalmente.</span></p>
<p class="p1"><span class="s1">Esta forma de explotación extrema - en un contexto de producción como el de la industria lechera, que denota numerosos problemas económicos y de sostenibilidad - <strong>causa un enorme sufrimiento a las vacas, en particular al consumir rápidamente su cuerpo y, por lo tanto, al destinarlas en pocos años al sacrificio</strong>. </span></p>
<p class="p1"><span class="s1">Además, la mayoría de las vacas criadas en Italia para la producción de leche y queso se ven obligadas a vivir sin pasto, es decir, sin acceso a ningún césped verde, una imagen muy explotada en los anuncios.</span></p>
<p class="p1"><span class="s1">"Este es un aspecto fundamental, porque incluso en este caso las imágenes que se divulgan al público en general son engañosas. La verdad que hay detrás de esta industria se mantiene oculta y transmite el mensaje incorrecto y parcial de que estos animales disfrutan de tratamientos que están muy lejos de la realidad cotidiana a la que se ven obligados a someterse", explica Matteo Cupi, Director Ejecutivo de Animal Equality Italy.</span></p>
<p class="p1"><span class="s1">Hay otros dos aspectos que Italia quiere poner en el centro del debate: <strong>el destino de los terneros y el impacto medioambiental de la leche</strong>.</span></p>
<p class="p1"><span class="s1">Un aspecto poco conocido por los consumidores es, de hecho, el destino de los terneros nacidos en la industria láctea, un proceso que ésta considera inevitable. De hecho, para asegurar la producción de leche, las vacas son inseminadas artificialmente y, al dar a luz a los terneros, comienzan a producir leche que luego será utilizada por la industria. Los terneros, que son separados inmediatamente de sus madres para evitar que consuman la leche, serán enviados al matadero.</span></p>
<p class="p1"><span class="s1">"Por si esto fuera poco, el impacto ambiental de este derivado es también considerable. <a href="https://ora.ox.ac.uk/objects/uuid:b0b53649-5e93-4415-bf07-6b0b1227172f" target="_blank" rel="noopener">Un estudio reciente de la Universidad de Oxford</a> ha demostrado que las plantas alternativas son mucho más sostenibles que la leche de vaca. Contrariamente a lo que se piensa entonces, cuando hablamos de impacto ambiental de la ganadería intensiva no sólo nos referimos a los bovinos criados para su carne, y por ello<strong> decidimos comprometernos a difundir en la medida de lo posible una información correcta</strong> y equilibrada respecto a lo que se transmite diariamente", concluye Cupi.</span></p>
