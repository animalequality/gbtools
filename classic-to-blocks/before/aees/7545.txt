<div class="center"><img class="wp-image-10919" src="/app/uploads/2016/08/13936636_10210595529832750_1352795071_n.jpg" /></div>
<p>El crecimiento internacional de un <a href="https://igualdadanimal.org/noticia/2016/05/28/naciones-unidas-nuestro-actual-sistema-alimentario-es-insostenible/" target="_blank">nuevo y mejor modelo alimentario</a> es imparable. La <strong>buena noticia</strong> nos llega esta vez desde Dinamarca.</p>

<p>Dos de las mayores cadenas de supermercados de Dinamarca, Coop y Dansk Supermarked han experimentado un aumento de ventas de <strong>productos alternativos a la carne</strong> de un 30% en el periodo 2014-2015.</p>

<p>Los productos cuya venta aumenta incluyen salchichas vegetales, tofu, falafel y hamburguesas vegetales elaboradas con lentejas.</p>

<p>Seg&uacute;n declaraciones del director de an&aacute;lisis de mercado de Coop, el <strong>espectacular aumento</strong> del periodo &laquo;va a continuar durante 2016&raquo;.</p>

<p>El <strong>nuevo modelo alimentario</strong> basado en opciones vegetales se extiende por toda Europa. Este mismo a&ntilde;o noticias similares llegaban desde <a href="https://igualdadanimal.org/noticia/2016/05/20/el-numero-de-britanicos-que-se-alimentan-de-forma-vegana-ha-subido-un-360/" target="_blank">Reino Unido</a> y <a href="https://igualdadanimal.org/noticia/2016/04/24/las-opciones-vegetarianas-crecen-un-633-en-alemania/" target="_blank">Alemania</a>. Tambi&eacute;n en el este del viejo continente se est&aacute; produciendo un aumento significativo de las <a href="https://igualdadanimal.org/noticia/2016/05/04/el-desarrollo-de-alternativas-vegetales-la-carne-tendencia-mundial-numero-uno-para-el/" target="_blank">opciones vegetarianas y veganas</a>.</p>

<p><img alt="" class="wp-image-10920" src="/app/uploads/2016/08/shutterstock_395490703.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>En cuanto a Dinamarca, el Instituto Nacional de Alimentaci&oacute;n ha hecho p&uacute;blico un estudio seg&uacute;n el cual el 5% de los daneses declaran <strong>no comer nada de carne</strong>. Fuentes del Instituto han comentado que &laquo;la importancia de la carne en nuestros platos est&aacute; experimentando un cambio&raquo;, y a&ntilde;aden que &laquo;en lo que se refiere a la alimentaci&oacute;n familiar cotidiana, <strong>la carne ya no juega un papel central</strong>&raquo;.</p>

<p>El estudio revela que la sociedad danesa <strong>est&aacute; reduciendo significativamente su consumo de carne</strong>. Este dato est&aacute; en sinton&iacute;a con otro estudio que la cadena de supermercados Coop ha llevado a cabo entre sus clientes. Un 3,3% de sus consumidores se declaran a s&iacute; mismos <a href="http://www.igualdadanimal.org/noticias/7433/por-que-hay-cada-vez-mas-personas-vegetarianas-y-veganas-nuestro-alrededor" target="_blank">vegetarianos o veganos</a>.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>Si quieres unirte a las personas que han decidido marcar la diferencia a trav&eacute;s de su alimentaci&oacute;n, por favor considera consumir alternativas vegetales a la carne. Tienes toda la informaci&oacute;n y recetas que necesitas en las fant&aacute;sticas websites <a href="https://www.gastronomiavegana.org/" target="_blank">Gastronom&iacute;a Vegana</a> y <a href="https://danzadefogones.com/" target="_blank">Danza de Fogones</a>.</strong></p>

<p><a href="http://cphpost.dk/news/danes-buying-more-vegetarian-substitutes-for-meat-and-dairy.html" target="_blank">Fuente: http://cphpost.dk/news/danes-buying-more-vegetarian-substitutes-for-meat-and-dairy.html</a></p>

