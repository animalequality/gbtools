<div class="center"><img class="wp-image-12781" src="/app/uploads/2018/01/shutterstock_476293381_0.jpg" /></div>
<p>Desde su creaci&oacute;n en 2003, la campa&ntilde;a global &laquo;Lunes sin carne&raquo;, a favor de la diversidad de opciones de alimentaci&oacute;n, ya se ha extendido a varias ciudades del mundo. Apenas un mes despu&eacute;s de que el ministro franc&eacute;s de transici&oacute;n ecol&oacute;gica propusiera implantarla en todo el pa&iacute;s, y que en la ciudad de Medell&iacute;n, Colombia, la misma fuera aprobada en 400 colegios p&uacute;blicos, Brasil se ha sumado tambi&eacute;n a la iniciativa.<br />
&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/12/04/proponen-un-dia-sin-carne-en-colegios-de-francia-para-luchar-contra-el-cambio/" target="_blank">Proponen un d&iacute;a sin carne en colegios de Francia para luchar contra el cambio clim&aacute;tico</a></strong>

<p><br />
El Parlamento de S&atilde;o Paulo, la capital y ciudad m&aacute;s grande de Brasil, <strong>ha dado un paso al implantar la venta de alimentos de prote&iacute;na vegetal los d&iacute;as lunes en colegios y organismos p&uacute;blicos.</strong> La medida ya se viene aplicando desde hace a&ntilde;os en m&aacute;s de cien municipios del estado que es tambi&eacute;n el m&aacute;s poblado de Brasil.<br />
&nbsp;</p>

<p><font size="5"><font color="#808080">&laquo;Detr&aacute;s de la iniciativa est&aacute; el sufrimiento que un ser vivo tiene que pasar para que ese alimento llegue hasta las personas&raquo;.</font></font></p>

<p>&nbsp;</p>

<p><font size="5"><img alt="" class="wp-image-12780" src="/app/uploads/2018/01/14664141999_a072de6f30_k.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></font></p>

<p><font size="5">&nbsp;</font></p>

<p>Tan solo un d&iacute;a menos de carne en los comedores sociales <strong>significa un reducci&oacute;n de 6 toneladas de ese alimento, equivalentes a 30 vacas adultas.</strong> De acuerdo con el diputado Feliciano Filho, impulsor de los &laquo;lunes sin carne&raquo;, &laquo;Detr&aacute;s de esta iniciativa est&aacute; <a href="https://igualdadanimal.org/blog/por-que-la-ganaderia-industrial-es-la-mayor-causante-de-maltrato-animal-de-la-historia/" target="_blank">el sufrimiento que un ser vivo</a> tiene que pasar para que ese alimento llegue hasta las personas&raquo;. Y contin&uacute;a cuestionando que &laquo;&iquest;si sumamos las escuelas, cu&aacute;ntas y cu&aacute;ntas vidas estaremos salvando con un simple gesto?&raquo;.</p>

<p><font size="5">&nbsp;</font></p>

<p><font size="5"><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</font></font></font></p>

<p><font size="5"><font size="5">&nbsp;</font></font></p>

<p><br />
<br />
Adem&aacute;s de enviar al matadero a m&aacute;s de 70.000 millones de animales al a&ntilde;o, <strong>la ganader&iacute;a industrial es tambi&eacute;n un factor principal del calentamiento global.</strong> Igualmente, el consumo de carne est&aacute; asociado a enfermedades cardiovasculares, colesterol, hipertensi&oacute;n, diabetes y c&aacute;ncer. Seg&uacute;n Filho el objetivo principal de la propuesta es llamar la atenci&oacute;n de la sociedad sobre &laquo;las consecuencias nefastas del consumo de carne&raquo;.<br />
<br />
&nbsp;</p>

<p><font size="5"><font size="5">&nbsp;</font></font></p>

<p><font size="5"><font size="5">&nbsp;</font></font></p>

<p>&nbsp;</p>

<p>Fuentes:<br />
<a href="https://www.elperiodico.com/es/internacional/20171230/lunes-carne-mayor-ciudad-brasil-6524699" target="_blank">https://www.elperiodico.com/es/internacional/20171230/lunes-carne-mayor-ciudad-brasil-6524699</a></p>

<a href="https://www.agrodigital.com/2018/01/09/lunes-sin-carne-aprobado-en-el-parlamento-de-sao-paulo/" target="_blank">https://www.agrodigital.com/2018/01/09/lunes-sin-carne-aprobado-en-el-parlamento-de-sao-paulo/</a>

