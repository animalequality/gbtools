<div class="center"></div>
Un nuevo análisis, y el más completo realizado hasta el momento sobre el impacto que la ganadería industrial tiene en el planeta, demuestra que la mejor y más sencilla forma de reducir nuestro impacto en el planeta es sustituyendo la carne y los productos lácteos en nuestra alimentación.

Este estudio realizado por científicos de la Universidad de Oxford y <span style="color: #0000ff;"><a style="color: #0000ff;" href="http://science.sciencemag.org/content/360/6392/987" target="_blank" rel="noopener">publicado en el Journal of Science</a>,</span> generó una gigantesca base de datos basada en 40 mil granjas de 119 países y abarca 40 productos que representan el 90% de lo que es consumido. A partir de estos datos, fue calculado el impacto total de estos productos, desde la granja hasta el tenedor, el uso de la tierra, la emisiones relacionada con el cambio climático, el agua potable y la contaminación del aire y el agua.

Entre varios de los efectos positivos de dicho cambio se encuentra que, sustituyendo el consumo de carne y lácteos, el uso de la tierra para generar el alimento de los animales en la ganadería industrial se reduciría en un 75%. Tal área equivaldría a  EE. UU., China, la Unión Europea y Australia, ¡todos juntos! Y aún con esta reducción se podría perfectamente seguir abasteciendo de alimentos a la población mundial.

El estudio también reveló que mientras que la carne y los lácteos solo proveen un 18% de calorías y 37% de proteínas, su producción utiliza la mayor parte de tierras (83%) y produce el 60% de los gases de efecto invernadero de la ganadería industrial.

Uno de los hallazgos más sorprendentes del estudio es el gran impacto que las piscifactorías o granjas de peces tienen en el agua potable. Según el mismo, en estos lugares los peces están constantemente depositando excrementos y se amontonan alimentos no consumidos en el fondo de los estanques donde apenas hay oxígeno, lo cual también los convierte en el medio propicio para la producción de metano, un potente gas de efecto invernadero.

&nbsp;

De acuerdo con Joseph Poore, director de la investigación, son los productos de origen animal los responsables de una multitud de problemas medioambientales. En el estudio se puede conocer las diferencias entre producir un tipo de alimento y otro: producir carne de vaca, incluso con el menor impacto posible, genera 6 veces más emisiones de gases de efecto invernadero y utilizar 36 veces más tierra que la producción de proteína vegetal. «Convertir el pasto en carne es como convertir el carbón en energía. Acarrea un costo inmenso en emisiones», afirmó Poore.

Quien considera que las etiquetas que señalan cuál es impacto medioambiental de cada producto serán un excelente comienzo para el cambio, ya que los consumidores podrán escoger aquellos que tienen un menor impacto. Pero, igualmente, el investigador considera que otorgar subsidios para aquellos productos saludables y de producción sostenible y colocar impuestos para la carne y los productos lácteos será también necesario.

La nueva investigación ha recibido los mayores elogios por parte de otros expertos alrededor del mundo. El profesor Gidon Eshel, de Bard College, EE. UU., dijo: «Me quedé anonadado. Es realmente importante, sano, ambicioso y revelador». Eshel destacó el hecho de que la investigación liderada por Poore utilizó un enfoque ascendente, analizado los datos a partir de cada granja.

El profesor Tim Benton, de la Universidad de Leeds, Reino Unido, dijo: «Este es un estudio inmensamente útil. Reúne una gran cantidad de datos. La forma en que producimos alimentos, consumimos y desperdiciamos alimentos es insostenible desde una perspectiva planetaria. La crisis mundial de la obesidad, el cambio en las dietas (comer menos productos animales y más verduras y frutas) tiene el potencial de hacernos más saludables a nosotros y al planeta».

Poore dijo: «La razón por la que comencé este proyecto fue para entender si en realidad existían productores sostenibles de productos animales. Pero dejé de consumir productos de origen animal durante los últimos cuatros años de este proyecto. Estos impactos no son necesarios para mantener nuestra forma de vida actual. La pregunta es cuánto podemos reducirlo y la respuesta es mucho».
