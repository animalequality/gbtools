<div class="center"><img class="wp-image-11774" src="/app/uploads/2017/03/shutterstock_206662132_0.jpg" /></div>
<p>Si pensaste que esa vieja creencia de que tienen una memoria de tres segundos y que no sienten dolor era cierta, mejor s&aacute;cala de tu cabeza porque los peces de est&uacute;pidos no tienen ni una escama.</p>

<p>Lamentablemente, debido a que no interactuamos con ellos por vivir en un medio tan diferente al nuestro <strong>les conocemos poco y, la verdad, nos importan muy poco</strong>.</p>

<p>Al considerar equivocadamente que son insensibles mucha gente que ya ha dejado de comer carne de vaca o de pollo, a&uacute;n no saca a los peces de sus platos.</p>

<p>Y a pesar de cualquier diferencia que existe entre ellos y el resto de los animales, <strong>los peces son tan capaces de experimentar sufrimiento y disfrute como lo p&aacute;jaros, los reptiles o los mam&iacute;feros</strong>.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n</p></FONT>

<p>&nbsp;</p>

<p><a href="https://link.springer.com/article/10.1007%2Fs10071-014-0761-0?wt_mc=Affiliate.CommissionJunction.3.EPR1089.DeepLink&amp;utm_medium=affiliate&amp;utm_source=commission_junction&amp;utm_campaign=3_nsn6445_deeplink&amp;utm_content=deeplink" target="_blank">Un reciente informe</a>, producto de a&ntilde;os de investigaci&oacute;n, ha demostrado que <strong>poseen multitud de habilidades como aprender de otros, reconocer a peces con los que han compartido durante un tiempo, conocer su lugar dentro de una jerarqu&iacute;a y recordar el complejo mapa de su entorno</strong>.</p>

<p>Todas estas capacidades hacen que sufran tanto como cualquier otro animal criado y matado para el consumo humano.</p>

<p>Pero lo cierto es que no solamente ignoramos sus habilidades sino tambi&eacute;n el terrible sufrimiento al cual los sometemos para consumir su carne.</p>

<p>Anualmente, <strong>la pesca industrial captura en todo el mundo a un n&uacute;mero de peces equivalente a la poblaci&oacute;n humana de 142 planetas Tierra</strong>. Y en las piscifactor&iacute;as (granjas de peces) m&aacute;s de 120.000 millones de peces son criados.</p>

<p><img alt="" class="wp-image-11773" src="/app/uploads/2017/03/shutterstock_226901644.jpg" style="margin: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>En el mar, <strong>cuando son atrapados en las redes y elevados a la superficie, la descompresi&oacute;n provoca que sus &oacute;rganos internos exploten</strong>. Muchos peces mueren en la superficie luego de esto, otros agonizan hasta morir.</p>

<p>Tanto la pesca industrial como las piscifactor&iacute;as los someten a un terrible sufrimiento porque, a excepci&oacute;n de otros animales de granja,<strong> no existe ninguna ley que regule el bienestar animal de los peces</strong>.</p>

<p>Dentro de las piscifactor&iacute;as viven hacinados en jaulas y los m&eacute;todos de matanza son igualmente crueles: electrocuci&oacute;n, asfixia y golpes.</p>

<p>Desamparados, sin contar con ley alguna que los protejan de los m&aacute;s terribles sufrimientos imaginables, <strong>los peces depende absolutamente de que actuemos para ayudarles</strong>.</p>

<p>Y es que aunque sus gritos no puedan ser escuchados, los peces nos est&aacute;n dejando algo muy claro: no quieren morir y quieren quedar fuera de nuestros platos.</p>

