<div class="center"><img class="wp-image-12515" src="/app/uploads/2017/10/youtube.jpg"></div>
A pesar de que las diferencias físicas entre ellos y otros animales lleve a muchos a pensar que no pueden sentir, <strong>las evidencias científicas son contundentes: los peces son capaces de sentir dolor y miedo [1] </strong>ya que anatómica, biológica y fisiológicamente son tan capaces de experimentar sufrimiento y disfrute como las aves, los reptiles o los mamíferos.

No obstante lo anterior, <strong>la pesca industrial &nbsp;somete a estos sensibles e inteligentes animales a los peores abusos.</strong> También es responsable de que se críen y maten más peces que cualquier animal en el mundo, siendo este número el equivalente a la población humana de 142 planetas Tierra.

&nbsp;

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2016/11/02/los-peces-tambien-son-victimas-del-maltrato-animal/" target="_blank" rel="noopener noreferrer">Los peces también son víctimas del maltrato animal</a></strong>

&nbsp;

Alguno humanos que se <strong>han interesado por interactuar con ellos o simplemente asistirlos en la enfermedad o necesidad, han tenido la fortuna de poder conocer el profundo mundo emocional de los peces cuando estos han sabido retribuir con afecto el cuidado y cariño que han recibido.</strong> Tal es el caso de Yoriko, un pez de la especie «Semicossyphus reticulatus» que un día se cruzó en el camino del actual veterano submarinista japonés de 79 años Hiroyuki Arakawa.

Cuando Arakawa era joven le fue encargado hacer un trabajo en un santuario sintoísta ubicado bajo del mar. De pronto, Yoriko apareció y <strong>cuando el buzo se percató de que el pez se encontraba en muy mal estado no dudó en brindarle toda la ayuda posible.</strong> Desde ese preciso instante se forjó una amistad que ha probado resistir el paso de los años y los medios físicos que separan a los dos grandes amigos.

&nbsp;

&nbsp;

<a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank" rel="noopener noreferrer">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.

&nbsp;

&nbsp;

Hiroyuki salvó la vida de Yoriko, ya que de no haber sido por sus cuidados el pez habría muerto debido a que estaba tan débil que no podía buscar comida.

La comunicación entre los dos amigos <strong>no precisa de un lenguaje conocido por alguno de ellos sino del genuino sentimiento de amor que los une:</strong> «No es que podamos hablar, sucede de forma natural. Yo diría que nos entendemos», asegura el buzo que desde el día en que se conocieron no ha dejado de visitar a su querido amigo acuático con frecuencia.

&nbsp;

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/si-te-dicen-que-nombres-un-animal-inteligente-luego-de-leer-esto-podrias-decir-pez/" target="_blank" rel="noopener noreferrer">Si te dicen que nombres a un animal inteligente luego de leer esto podrías decir «pez»</a></strong>

&nbsp;

Ya han pasado 25 años desde el día de su primer encuentro y, desde entonces, cada vez que Hiroyuki baja a las profundidades a atender el santuario, Yoriko viene a verlo y juega con él, y su amigo lo recibe siempre con un cálido beso. El video de esta increíble historia de amistad y amor fue compartido en Youtube y <strong>los usuarios no pueden creer que Yoriko se acerque a Hiroyuki con total naturalidad y que él le responda con caricias y besos.</strong>

«Me parece que hay confianza entre nosotros. Yo diría que él sabe que le salvé, que le ayudé cuando lo necesitaba. Estoy orgulloso de haberlo hecho”, asegura Hiroyuki. Nosotros, que sabemos que los peces son capaces de crear complejas relaciones y tienen una increíble memoria, no dudamos que es así.

&nbsp;
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/XyHO3-FFaVc" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
&nbsp;

Fuentes:

[1]<a class="break-all" href="https://link.springer.com/article/10.1007%2Fs10071-014-0761-0?wt_mc=Affiliate.CommissionJunction.3.EPR1089.DeepLink&amp;utm_medium=affiliate&amp;utm_source=commission_junction&amp;utm_campaign=3_nsn6445_deeplink&amp;utm_content=deeplink" target="_blank" rel="noopener noreferrer">https://link.springer.com/article/10.1007%2Fs10071-014-0761-0?wt_mc=Affiliate.CommissionJunction.3.EPR1089.DeepLink&amp;utm_medium=affiliate&amp;utm_source=commission_junction&amp;utm_campaign=3_nsn6445_deeplink&amp;utm_content=deeplink</a>

<a href="https://www.thedodo.com/diver-visits-fish-25-years-video-1695525718.html" target="_blank" rel="noopener noreferrer">https://www.thedodo.com/diver-visits-fish-25-years-video-1695525718.html</a>
