<div class="center"><img class="wp-image-11846" src="/app/uploads/2017/03/gallina.jpg" /></div>
<p><img alt="" class="wp-image-11847" src="/app/uploads/2017/03/galli.jpg" style="float:left; margin:5px 10px; width:370px" />Tras di&aacute;logos con Igualdad Animal y otras organizaciones, <strong>Esselunga</strong> <a href="https://www.esselunga.it/cms/azienda/impegno-per-il-benessere-animale/benessere-animale.html">anuncia</a> una <strong>nueva pol&iacute;tica que elimina los huevos provenientes de gallinas enjauladas en toda su cadena de producci&oacute;n</strong>, efectiva a partir de este mismo a&ntilde;o. Adem&aacute;s, la empresa ha extendido esta pol&iacute;tica para la mayor&iacute;a de productos de su propia marca que contienen huevo. Esta decisi&oacute;n reducir&aacute; el sufrimiento de miles de gallinas al a&ntilde;o.</p>

<p><a href="https://igualdadanimal.org/noticia/2017/03/09/igualdad-animal-denuncia-el-maltrato-de-las-gallinas-enjauladas-en-italia/">Igualdad Animal ha publicado recientemente una impactante investigaci&oacute;n</a> sobre las gallinas en jaulas en Italia. Las im&aacute;genes muestran el enorme sufrimiento al que son sometidas las gallinas enjauladas en la industria del huevo.</p>

<p>La investigaci&oacute;n ha tenido una particular resonancia en los medios y como resultado muchas compa&ntilde;&iacute;as italianas como Giovanni Rana, GEMOS y Dussmann se han distanciado p&uacute;blicamente de esta cruel pr&aacute;ctica.</p>

<p>A pesar de que eliminar las jaulas no significa eliminar el maltrato, pol&iacute;ticas como la de Esselunga son un importante primer paso para reducir el sufrimiento de los animales. Si quieres hacer m&aacute;s para ayudar a las gallinas, puedes optar por alternativas al consumo huevos.</p>

<p>&iexcl;<a href="http://www.igualdadanimal.org/noticias/7713/6-recetas-para-ayudarte-sustituir-los-huevos-en-tu-alimentacion">Descubre</a> lo f&aacute;cil que es!</p>

<p><iframe allowfullscreen="" frameborder="0" height="498" src="https://www.youtube.com/embed/krJYhEtDW7w" width="885"></iframe></p>

