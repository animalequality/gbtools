<div class="center"></div>
Dentro de un galpón, viviendo en insalubres condiciones de hacinamiento, tumbados sin poder moverse porque no soportan el peso de su propio cuerpo, así pasan su vida los pollos broiler que encuentras en las bandejas del supermercado. Todo esto ocurre a animales que poseen una alta complejidad social y sensibilidad emocional y cuya inteligencia ha sido hasta hora subestimada.

Lori Marino, investigadora de Neurociencia y Comportamiento Biológico en Emory, ha revelado datos sorprendentes que podrían hacer cambiar la percepción de muchas personas acerca de estas aves. En su estudio «Pollos pensantes: una reseña sobre la condición, emoción y comportamiento de los pollos domésticos»,  Marino logró separar los hechos de la ficción que existe en torno a la inteligencia de los pollos y quedó sorprendida por los resultados.

&nbsp;
<h4 style="text-align: center;"><span style="font-size: x-large;"><span style="color: #808080;">«La gente minimiza la inteligencia, sensibilidad y consciencia de cualquier animal que comen, porque causa menos disonancia cognitiva. De este modo, no se perciben más que como alimento, un producto».</span></span></h4>
&nbsp;

Según Marino, los pollos pueden hacer comparaciones entre sí mismos y otros pollos, y usar estas comparaciones en formas que podrían ser consideradas estratégicas o «maquiavélicas». Por ejemplo, los machos son capaces de hacer la llamada para avisar que han encontrado alimento aunque en realidad no es así, solo para atraer a las hembras. También cacarean más bajo si saben que hay otros machos cerca para evitar que les roben a las hembras.

Los pollos pueden aprender por observación y en términos emocionales tienen la capacidad de conmoverse ante las emociones de otros, siendo este el principio psicológico de una forma simple de empatía. «Cuando pensamos en un psicólogo, pensamos en alguien que trabaja con personas. Pero todos los animales tienen mente, por lo tanto tienen psicología y los pollos no son la excepción. Uso el término “psicología” deliberadamente, no solo porque es válido científicamente, sino que reconoce el hecho de que los pollos en verdad tienen mente», nos dice Marino.

Pero, ¿entonces por qué se les considera «más tontos» que otros animales? Marino considera que esta desventaja se debe a una combinación de factores: no son mamíferos y a las personas les gusta comerlos. La gente tiende a considerar a los mamíferos como más inteligentes que las aves cuando no necesariamente es así. «Y, más importante aún, la gente minimiza la inteligencia, sensibilidad y consciencia de cualquier animal que comen, porque causa menos disonancia cognitiva. De este modo, no se perciben más que como alimento, un producto», afirma Marino.

Para la investigadora, la forma en la cual nos referimos a otros pueden hacer cambiar nuestra percepción de ellos. Comúnmente nos referimos a los pollos como «cosas» pero es importante reconocer que tienen sensaciones, mente y que cada uno es un individuo al igual que nosotros. Y añade que «es por eso que los pollos son un quién y no un qué».

Sustentada por todas la evidencias sobre su sensibilidad e inteligencia, Lori Marino plantea a través de su estudio que deberíamos replantear seriamente la forma en que tratamos a los pollos sistemáticamente en todo el mundo, todos los días. El trato que les damos está basado en suposiciones falsas de que estos animales carecen de inteligencia o sentimientos, y al ser esto una mentira debemos transformar esta realidad si queremos reconocernos ante ellos como seres sensibles y compasivos.

&nbsp;
