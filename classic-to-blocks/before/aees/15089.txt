¡Cerditos!, ¡cerditos por todos lados! Nos alegran tanto la vida que cómo no vamos a defenderles con todas nuestras fuerzas de la cruel industria de la carne.

Estos pequeñajos son tan adorables que se han convertido en verdaderas estrellas de internet. Sí, es cierto que tienen serios competidores como los gatetes pero… ains: rosas, peludos, sonrientes… <strong>¿quién puede resistirse? ¡Nosotros no desde luego!</strong>

Sólo pensar que la mayoría de estos adorables animales tan inteligentes como delfines y primates pasan por atroces sufrimientos nos enferma. Por eso hemos pensado en dejarte aquí nuestra <a href="https://es.loveveg.com/veganizando-tradiciones/">guía gratuita para una alimentación sin crueldad,</a> que, <strong>además de ser deliciosa, encanta a los cerditos</strong> porque así no les hacemos sufrir por nuestra alimentación.

¿Qué más se puede pedir? Ah, sí, las fotos adorables, claro. ¡Pues venga! Ahí van. <strong>Esperamos que te lleguen al corazón como a nosotros</strong>. ¡Vivan los cerdos libres y felices!

<img class="attachment-temp-unmanaged-76efa3a893e9d9fe0d068ec4240d8b03 wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos1.jpg" alt="" />

<img class="attachment-temp-unmanaged-147c073186387ebb4a183d33ee3ade99 wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos2.jpg" alt="" />

<img class="attachment-temp-unmanaged-3c59bafd4254f4b729af6ca8239f3125 wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos3.jpg" alt="" />

<img class="attachment-temp-unmanaged-e7365c2d2db6fef39d695ecd285fffb6 wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos4.jpg" alt="" />

<img class="attachment-temp-unmanaged-b90d25b71e3e2df526211c5797b646ff wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos5.jpg" alt="" />

<img class="attachment-temp-unmanaged-46b02d8ff73700dfcbd53d59c962654c wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos6.jpg" alt="" />

<img class="attachment-temp-unmanaged-f8b454a23ed9696a9dd230d57096d5af wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos7.jpg" alt="" />

<img class="attachment-temp-unmanaged-4916304b749151275aff211dd41580bb wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos8.jpg" alt="" />

<img class="attachment-temp-unmanaged-93ebb17ea30274ede24e29dfeacf382e wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos9.jpg" alt="" />

<img class="attachment-temp-unmanaged-025642d5ceced6e73186d6da1ee37b2f wp-image-0000000" style="margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/cerditos10.jpg" alt="" />
