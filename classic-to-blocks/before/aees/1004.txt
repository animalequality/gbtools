[img_assist|nid=1005|title=Suma en su Jaula antes de morir|desc=|link=none|align=left|width=200|height=143]Suma, la orangutana más vieja de Europa, murió el pasado 28 de Septiembre a los 53 años en el "zoológico" de Osnabrük, en el oeste de Alemania, según ha informado el zoo.

Los "cuidadores" de Suma decidieron la semana pasada matarla porque según ellos mismos "debido a su avanzada edad, la orangutana se había debilitado mucho".

Suma nació en libertad en 1953 en Sumatra -de ahí su nombre-, fué  secuestrada y encarcelada en el "zoológico" de Dresden (al este de Alemania) hasta 1992, fecha en la que fue traslada al "zoo" de Osnabrück para que hiciera compañía al orangután Buschi de 35 años, ambos han vivido encerrados en una jaula desde entonces.

Tras la muerte de Suma, los responsables del "zoológico" de Osnabrück buscarán en los próximos dos meses otra orangutana para Buschi.
