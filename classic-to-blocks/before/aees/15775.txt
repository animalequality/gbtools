Consumir huevos forma parte de la rutina diaria de una gran cantidad de personas, porque se le considera un alimento sano y por su disponibilidad. Hace unos días se celebró el Día Mundial del Huevo y en los medios se compartieron miles recetas e información sobre dicho alimento pero, como pasa con todas las prácticas de la industria ganadera, <strong>nada se dijo sobre lo que su producción implica para los animales, el planeta y nuestra salud. </strong>
<!--more-->

Para que un huevo llegue a nuestra mesa, <strong>las gallinas padecen 26 horas de sufrimiento</strong>, encerradas en jaulas donde les niegan la posibilidad de desarrollar sus comportamientos más básicos: extender sus alas, echarse en la tierra y acicalarse. Llevan vidas miserables. ¿Podríamos siquiera imaginar la desesperación de vivir dos años encerrado en una jaula?

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2018/10/12/en-espana-las-grandes-distribuidoras-dicen-no-los-huevos-de-jaulas/" target="_blank" rel="noopener">En España, las grandes distribuidoras dicen «no» a los huevos de jaulas</a></strong>

Además del inmenso sufrimiento de millones de gallinas, la producción de huevos, al igual que la de carne y lácteos, <strong>genera efectos negativos en el medioambiente</strong>, como la emisión de gases de efecto invernadero o la contaminación del suelo y del agua. Pero no fue sino hasta la década de los 80 que comenzó a dársele a este asunto la importancia que tiene y, a pesar de esto, desde entonces pocos estudios sobre el tema han logrado salir a la luz.
<div>

<img class="wp-image-13360" style="float: left; margin-left: 10px; margin-right: 10px; width: 450px;" src="/app/uploads/2018/10/43235027751_2b7685ef8b_z_1.jpg" alt="" />

Un estudio realizado por investigadores de la Universidad de Oviedo reveló que la huella de carbono obtenida por cada docena de huevos fue de un 2,7 kg de CO2, equivalente a un valor similar al de los lácteos e inferior al de la carne de cordero, cerdo o ternera. El equipo científico analizó los efecto de la producción sobre 18 categorías ambientales, entre las que se encuentran el agotamiento del ozono, el cambio climático, la acidificación terrestre, la toxicidad humana y la ocupación del terreno, entre otras. Las más afectadas fueron la transformación de la tierra natural y las ecotoxicidades del agua y de la tierra.

En las gigantescas granjas ponedoras si la presencia de ácaros no es combatida rápidamente, como la organización internacional Igualdad Animal documentó en una de sus más recientes investigaciones, <strong>esto puede conducir a la explosión de casos reales de salmonella.</strong> La cáscara del  huevo es porosa y las malas condiciones de higiene pueden conducir a la entrada de bacterias en los huevos destinados a consumo lo cual se traduce en numerosos riesgos para la salud humana.

En todo el planeta, la producción de huevo ha alcanzado la impresionante cifra de 68 millones de toneladas anuales. Estas gigantescas cifras no dan una idea de cuán urgente resulta buscar alternativas sostenibles a los huevos que, además, sean seguras para nuestra salud y no perjudiquen a las gallinas.
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación? </a></h4>
<h4 style="text-align: center;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
A través de nuestro equipo de incidencia corporativa hemos logrado que una gran cantidad de empresas en España, Italia, México y Brasil se comprometan a comprar huevos que no provengan de gallinas enjauladas. Toda persona que quiera ayudar a cambiar la vida de millones de gallinas solo tiene que ingresar a <a href="http://https://igualdadanimal.org/defensores-animales/" target="_blank" rel="noopener">Defensores animales España</a>  desde donde podrá realizar acciones en línea o de calle de alto impacto.

</div>
