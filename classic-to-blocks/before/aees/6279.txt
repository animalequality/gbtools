
Cazadores alemanes abatieron la pasada temporada un total de 67.905 mapaches en Alemania, donde este animal originario de Norteamérica se ha convertido en una plaga que amenaza la fauna local.
Más de 500.000 mapaches viven repartidos por toda Alemania según datos de la Sociedad Cinegética germana que publica el rotativo Bild, en el que se destaca que, al carecer de enemigos naturales, se reproducen en algunas regiones de manera explosiva.

Introducido a principios del siglo pasado en el país por comerciantes de pieles que liberaron varias parejas en distintos bosques germanos, desde entonces su cifra ha aumentado de manera exponencial.
En zonas como los alrededores de la ciudad de Kassel, en el centro de Alemania, se calcula que su densidad es de 100 animales por hectárea.

La Sociedad Cinegética Alemana subraya que, al tratarse de animales omnívoros, suponen una gran amenaza para la fauna pequeña local, sobre todos para las aves, ya que roban sus huevos de los nidos para alimentarse.
