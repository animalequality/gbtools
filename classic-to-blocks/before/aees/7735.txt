<div class="center"><img class="wp-image-11821" src="/app/uploads/2017/03/foto_fb_18.jpg" /></div>
<p>Apaleados, pateados, arrastrados de las orejas, electrocutados y escaldados vivos. Este es el destino de los cerdos que acaban sus d&iacute;as en <a href="https://igualdadanimal.org/blog/bienvenido-al-matadero-un-viaje-al-lugar-que-la-industria-carnica-no-te-va-mostrar/" target="_blank">el matadero</a> de la localidad belga de Tielt.</p>

<p>Tielt es una peque&ntilde;a localidad situada a tan solo 85 kms de Bruselas. All&iacute; se encuentra <strong>uno de los mayores mataderos de B&eacute;lgica</strong>. Investigadores encubiertos de la organizaci&oacute;n de protecci&oacute;n animal belga/holandesa <a href="https://www.animalrights.be/" target="_blank">Animal Rights</a> (Derechos Animales) acaban de revelar im&aacute;genes de las pr&aacute;cticas comunes en el lugar.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>La crueldad y el desprecio hacia la vida de estos <strong>seres sensibles e inteligentes</strong> es descorazonadora. Los trabajadores, acostumbrados a presenciar un sufrimiento animal inimaginable, acaban por tener actitudes crueles e innecesarias. Las pr&aacute;cticas ilegales abundan. Animal Rights hace una llamada a la sociedad <a href="https://www.animalrights.be/sluit-het-varkensslachthuis-tielt" target="_blank">para clausurar este matadero</a>.</p>

<p><iframe allowfullscreen="" frameborder="0" height="360" mozallowfullscreen="" src="https://player.vimeo.com/video/209441374" webkitallowfullscreen="" width="640"></iframe></p>

<p>En el v&iacute;deo de la investigaci&oacute;n, <strong>grabado con c&aacute;mara oculta para que los trabajadores no supieran que estaban siendo grabados</strong>, se puede ver a los animales sometidos a pr&aacute;cticas completamente prohibidas en la Uni&oacute;n Europea.</p>

<p>Durante un mes, un investigador encubierto de la organizaci&oacute;n de protecci&oacute;n animal se infiltr&oacute; en el matadero. Nadine Lucas, presidente de la organizaci&oacute;n, ha declarado &laquo;hemos realizado una denuncia ante la fiscal&iacute;a p&uacute;blica demandando la clausura inmediata de este matadero.&raquo;</p>

