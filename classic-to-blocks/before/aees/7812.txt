A la vez que la Organización Mundial de la Salud (OMS) y la Organización de las Naciones Unidas (ONU) recomiendan <strong>reducir el consumo de carne</strong>, las ventas de productos vegetales alternativos crece en algunos países al doble de velocidad que el de los productos cárnicos.
<h4 style="text-align: center;"><strong>¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</strong></h4>
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;"><strong> ¡Suscríbete gratuitamente a nuestro e-boletín!</strong></span></a></h4>
A pesar de ello, las proyecciones de mercado hablan de un aumento global del consumo de carne. Un informe de la ONU alertaba que para el año 2050 el consumo de carne aumentará en un 75%, haciendo <strong>completamente insostenible</strong> nuestro sistema alimentario.

Los países occidentales consumen tal cantidad de carne per cápita que la situación se ha convertido en <strong>un problema de salud pública</strong>. Además, los grandes países con clases medias emergentes como China, aumentan su consumo de carne a la par que su poder adquisitivo. Pero, ¿cuánta carne per cápita se consume en el mundo?

Según datos de la Organización para la Cooperación y el Desarrollo Económicos (OCDE), Australia es el país en el que más carne se come per cápita al año, 92 kgs (205 lbs), seguido de EE. UU., 90 kgs (200 lbs) e Israel, 85 kgs (189 lbs). Por detrás se sitúan, entre otros, Brasil, 78 kgs (172 lbs) y China, 48 kgs (107 lbs).

Por su parte, en España, el Ministerio de Agricultura y Pesca, Alimentación y Medio Ambiente (MAPAMA) sitúa el consumo medio por persona al año en 50 kgs (2015).

La carne de pollo es la más consumida en nuestro país (siguiendo una tendencia bien establecida en todo el mundo), con 13 kgs por persona y año. Le sigue la carne de cerdo, con 10 kgs y la de vaca, con 5 kgs. Los gobiernos de algunos países ya han empezado a hacerse eco de la llamada a la reducción del consumo de carne. Las motivaciones son tanto de salud pública como ambientales. <strong>La ganadería industrial es un factor principal de las emisiones de gases de efecto invernadero.</strong>

<span style="font-size: x-large;"><span style="font-size: xx-small;"><span style="font-size: xx-small;"><span style="font-size: xx-small;"> </span></span></span></span>Así, China, por ejemplo ya está desarrollando un plan para reducir el consumo de carne de la población en un 50%, liderando en este sentido el cambio hacia una manera de producir alimentos sostenible.
