Nuestra innovadora película de realidad virtual <em>iAnimal: La industria de leche en 360 grados</em>, que permite a los espectadores experimentar la vida y la muerte a través de los ojos de una vaca, ha sido nominada para el Raindance VRX Award, en la categoría de Mejor Experiencia de Impacto, en el festival de cine independiente más importante de Inglaterra.

El film, narrado por la estrella de Harry Potter, Evanna Lynch, contiene imágenes en 360º registradas por investigadores de Igualdad Animal infiltrados en granjas lecheras y mataderos. Con la inclusión de escenas filmadas en Devon y Somerset, la película cuestiona la imagen idílica de la industria láctea en el Reino Unido.

<iframe src="https://www.youtube.com/embed/YnbkXbXza0Y" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Vivir la experiencia de ver todo esto a través de los lentes de realidad virtual, te hace sentir como si fueras un pequeño becerro atrapado en una corral y luego puedes ver a los ojos de una vaca exhausta mientras el matarife se acerca y ella se da cuenta que está a punto de dar su último respiro.
<h4 style="text-align: center;"><span style="font-size: x-large;"><span style="color: #808080;"><span style="color: #000000;">¿Quieres recibir las mejores noticias de actualidad sobre los animales?</span> </span></span></h4>
<h4 style="text-align: center;"><span style="font-size: x-large;"><span style="color: #808080;"><span style="color: #0000ff;">¡Suscríbete gratuitamente a nuestro<a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"> e-boletín</a>!</span></span></span></h4>
Luego de ver iAnimal: La industria de leche en de 360 grados, Evanna estaba muy consternada y nos dijo: «Solo quería hacerme lo más pequeña posible, porque así es como pienso que se sienten los animales; solo quieren esconderse, pero no existe ningún lugar en donde encuentren comodidad o paz durante todas sus vidas».

En México, la narración fue realizada por la afamada actriz Sofía Sisniega quien dijo “Hay algo muy importante por lo que lucho todos los días, acabar con la crueldad y el sufrimiento de los animales” y agrega, “Si tengo tiempo para filmar una serie y una película a la vez, tengo tiempo para apoyar a Igualdad Animal”. Su participación ha sido clave en la difusión del proyecto en America Latina.

Los ganadores serán anunciados durante el <a href="https://raindance.org/">Festival de Cine Raindance 2018</a> en Londres, desde el 26 de septiembre y hasta el 7 de octubre, donde nuestra película <em><a href="https://ianimal.es/">iAnimal</a></em> también será presentada.
