Según informa Europa Press, la Policía Local de Valencia descubrió ayer un local en la zona del Cabanyal en el cual se encontraban cuatro gallos “de pelea” y dos gallinas. La policía denunció al dueño del local, un joven de 21 años, y los animales fueron sacados de allí por el Servicio Valenciano para la Protección de Animales y Plantas.

El local estaba en lamentables condiciones de salubridad e higiene y allí se encontraron 15 jaulas artesanales de madera y hierro, productos y vitaminas para acelerar el crecimiento de los gallos, así como los elementos necesarios para su “entrenamiento” (suelo de arena, una barra para ensayar ataques).

Los gallos tenían la cresta cortada y los espolones preparados para colocarles uñones,  y mostraban falta de plumaje y heridas por el cuerpo.

