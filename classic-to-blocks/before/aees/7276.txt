<img class="wp-image-10264" style="float: right; margin: 5px; width: 450px;" src="/app/uploads/2015/07/gadhi.jpg" alt="">

"<em>Durante generaciones, los peregrinos han sacrificado los animales a la diosa Gadhimai, con la esperanza de una vida mejor. Ha llegado el momento de transformar una vieja tradición. Ha llegado el momento de sustituir la matanza y la violencia con el culto y la celebración pacífica</em>".

Con estas palabras anunciaba hoy martes Ram Chandra Shah, en representación del Templo de Gadhimai en Nepal, la decisión de prohibir el sacrificio de animales en el Festival de Gadhimai, considerado el mayor sacrificio religioso de animales del mundo.

La decisión se produce después de las negociaciones entre el Templo y las organizaciones Animal Welfare Network Nepal (AWNN) y Humane Society International / India.

La organización internacional Igualdad Animal presentó recientemente un reportaje con imágenes nunca vistas del Festival de Gadhimai en Nepal, que mostraban la brutalidad y violencia extrema que sufren los animales víctimas de este ritual religioso. El vídeo también recogía el trabajo llevado a cabo por la organización con la campaña "Stop Sacrificios" para intentar detener la matanza de animales en el festival, con la que consiguieron que el Gobierno de India bloqueara el transporte de animales al festival en la frontera con Nepal [2] y que finalmente el número de animales sacrificados durante 2014 se redujera en un 70% respecto a la edición anterior de 2009. [3]

&nbsp;
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/75W8ULrK4DY" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
&nbsp;

“<em>Es un hito histórico. Cientos de miles de animales nunca más serán víctimas de la violencia del Festival de Gadhimai. Esto demuestra que la presión internacional da resultados y esta es una victoria de todas las personas y organizaciones que hemos trabajado sin descanso en los últimos años para que esta masacre no se volviera a repetir”. &nbsp;“Estamos muy satisfechos con la decisión tomada por el Templo de Gadhimai. Ahimsa (no violencia) es un principio importante de la religión hindú y nos alegramos de que finalmente el Templo la esté llevando a la práctica</em>”. Indicó Amruta Ubale, directora de Igualdad Animal India.

[1] http://www.ibtimes.co.in/nepal-animal-sacrifice-banned-gadhimai-festival-640801

[2]http://www.europapress.es/epsocial/ong-y-asociaciones/noticia-india-impedira-transporte-animales-evitar-sacrificio-festival-religioso-nepal-20140821131750.html

[3] http://www.europapress.es/epsocial/ong-y-asociaciones/noticia-igualdad-animal-cifra-69-descenso-sacrificios-festival-gadhimai-nepal-respecto-2009-20141201181549.html
