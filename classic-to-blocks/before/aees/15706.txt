Las gallinas son más inteligentes de lo que lo que pensamos. Y también, contrariamente a lo que la industria del huevo y su engañosa publicidad suele mostrar, son uno de los animales más maltratados del planeta.

Conocemos bien que la creencia popular las ha definido como tontas y hasta perezosas y, por eso, hoy hablaremos de cómo en realidad <strong>son ellas: animales dotados de una inteligencia y sensibilidad sorprendentes similares a la de mamíferos y primates.</strong>

&nbsp;

<strong>Saben contar</strong>

Estudios realizados en gallinas recién nacidas han demostrado que son capaces de hacer operaciones matemáticas básicas como contar y tienen la capacidad de colocar cantidades en una serie. Dichas habilidades se muestran normalmente en los humanos solamente a partir de los cuatro años de edad.

<strong><img class="wp-image-13202" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/07/58b5e3aa15896-detail_2.jpg" alt="" /></strong>

<strong>Pueden llegar a superar en inteligencia a niños</strong>

Y también animales considerados entre los más inteligentes. Un estudio de la profesora Christine Nicol de la Universidad de Bristol explica que están familiarizadas con la inferencia transitiva, es decir la idea en lógica que, si A es mayor que B y B es mayor que C, entonces A es mayor que C. Este tipo de razonamiento tiene lugar en humanos sólo desde los 7 años aproximadamente.

<strong>Son empáticas</strong>

Las gallinas se muestran angustiadas cuando creen que sus polluelos están en peligro. Tratan de protegerlos de cualquier amenaza y sólo vuelven a calmarse cuando se aseguran de que ya no hay peligro.
<strong>Manejan un vocabulario complejo</strong>

Las gallinas pueden comunicarse a través de más de 30 vocalizaciones distintas para emitir una amplia gama de mensajes: desde avisar al resto del grupo de dónde hay comida hasta para alertar sobre predadores. Su comunicación empieza antes de la eclosión del huevo: estando aún dentro de ellos, los pollitos se comunican con sus madres.

&nbsp;
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</h4>
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;">¡Suscríbete gratuitamente a nuestro e-boletín!</span></a></h4>
&nbsp;

Es solo conociendo mejor a las víctimas de la cruel industria del huevo que podremos conocer también mejor lo que padecen y exponerlo al resto de la sociedad. Y es por eso que dedicamos todo nuestro esfuerzo en cambiar la vida de miles de millones de gallinas trabajando por el fin de las jaulas dentro de la industria.

Tú también puedes hacer muchísimo para ayudarlas. Puedes enviar correos y usar la redes para difundir información sobre el terrible maltrato al que son sometidas. También <strong>puedes formar parte del equipo de Igualdad Animal para hacer frente a la empresas que producen huevos a costa de su sufrimiento</strong>.

<a href="https://igualdadanimal.org/defensores-animales/" target="_blank" rel="noopener">Para más información, puedes consultar Defensores animales España </a>
