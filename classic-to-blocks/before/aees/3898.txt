El Juzgado de Instrucción número 1 de Colmenar Viejo (Madrid) ha condenado a un hombre que arrojó por la ventanilla del copiloto del coche que conducía a tres gatos bebé, uno de los cuales fue atropellado por él mismo.

Los hechos sucedieron el 4 de junio de 2006, en la carretera de Hoyo de Manzanares, en el término municipal de Colmenar Viejo (Madrid). El hombre había encontrado a los gatos en su finca, situada muy cerca de allí, los cogió, los metió en su coche y, durante el trayecto, los fue lanzando por la ventanilla del lado derecho. De los tres cachorros, sobrevivió uno, del que se hizo cargo El Refugio y que ahora está adoptado; mientras que al segundo lo atropelló el condenado con el coche dando un volantazo y el tercero se perdió en la cuneta, según los testigos.

Los testigos siguieron al coche para anotar la matrícula. Al regresar vieron a un gato atropellado, y pudieron rescatar al primero que lanzó, que estaba maullando en la parte izquierda de la carretera, en la cuneta. Este gato fue adoptado posteriormente.

Sin embargo, debido a que la legislación actual no tiene en cuenta los intereses de los animales, la condena por dicho asesinato ha sido de tan sólo 30 días de multa a razón de 12 euros diarios.


Fuente: lavanguardia.es, discapnet.es
