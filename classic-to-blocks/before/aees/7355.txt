<img class="wp-image-10361" style="float: right; margin: 10px; width: 450px;" src="/app/uploads/2015/12/tenerasinvestigacion.png" alt="">Las organizaciones de protección animal <a href="http://www.farmwatch.org.nz/">Farmwatch</a> y <a href="https://safe.org.nz/">SAFE</a>, de Nueva Zelanda, acaban de lanzar <a href="http://safe.org.nz/nz-dairy-industry-exposed">una aterradora investigación sobre la industria láctea</a>. <strong>Durante dos años</strong>, investigadores de estas organizaciones documentaron granjas y mataderos <strong>en una amplia zona del país</strong>. El pasado domingo la investigación era aireada en la televisión nacional y está causando un verdadero terremoto.

En un vídeo grabado con cámaras ocultas se muestra cómo granjeros, transportistas y personal de mataderos maltratan cruelmente a pequeñas e indefensas terneras, <strong>tras ser desgarradoramente separadas</strong> de sus desesperadas madres.

Al año, <strong>2 millone</strong>s de estos bebés son separados inhumanamente de sus madres en Nueva Zelanda y enviados al matadero. Los granjeros arrebatan los bebés a sus madres sólo horas después de haber nacido. Después son enviadas al matadero con tan solo <strong>4 días de edad</strong>. Según el Instituto Nacional de Estadística, <strong>en España son 1.083.000 las terneras enviadas al matadero al año</strong>.

&nbsp;
<div class="ae-video-container"><iframe src="https://player.vimeo.com/video/146749967?title=0&amp;byline=0&amp;portrait=0" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
&nbsp;

El investigador de Farmwatch, John Darroch, declaraba a los conmocionados medios de comunicación que, «<em>la separación de las terneras de sus madres con tan solo un día de edad forma parte del procedimiento de toda la industria láctea. <strong>No existirían productos lácteos sin esta separación</strong>. Incluso en las granjas con mejores estándares de bienestar animal, se separa a las terneras de sus madres al día de nacer</em>».

«<em>Hemos estado recibiendo llamadas de personas que viven en zonas rurales durante años, incluso de granjeros, para que investigásemos sobre el trato que se da a las terneras en la industria</em>», comentaba Darroch a la prensa.

Por su parte, representantes de las industrias lácteas, de mataderos y del propio gobierno, <strong>están teniendo que contestar a preguntas incómodas</strong> por parte de los medios de comunicación. Se ha lanzado <strong>una investigación de los hechos</strong> por parte del Gobierno neozelandés, a lo que se ha visto forzado tras el trabajo de investigación de los defensores de los animales.
