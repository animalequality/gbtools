En este país centroamericano los lugareños practican la "recolección de aletas de tiburón". Capturan a estos animales, les cercenan la aleta dorsal y les devuelven, moribundos, a la aguas del Pacífico. La membrana queda en la superficie, la secan y la preparan para viajar al otro lado del mundo. 

La razón es económica. Los compradores asiáticos están dispuestos a pagar 100 euros por cada kilo de este parte del animal. Por culpa de las mutilaciones entre 1991 y 2001 la población de tiburones en esta región del Pacífico ha bajado un 60%.

La población de estos animales se redujo un 60% entre 1991 y 2001 por culpa del "desaleteo". 


Fuentes: Periodista Digital, El País
