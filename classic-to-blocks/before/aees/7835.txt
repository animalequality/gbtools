<div class="center"><img class="wp-image-12186" src="/app/uploads/2017/06/29104127350_88785ff1cf_z_2.jpg" /></div>
<p>Hay una secuencia que se repite incansablemente d&iacute;a tras d&iacute;a y cuyo nefasto alcance queda oculto para la mayor&iacute;a de nosotros: un carrito de supermercado avanza, alguien hecha dentro de &eacute;l un cart&oacute;n de huevos, lo paga, consumen cada uno de ellos en casa y, luego, todo inicia una vez m&aacute;s.</p>

<p>Todo esto es tan normal que no solemos preguntarnos (y, sinceramente, nuestro entorno tampoco nos impulsa a hacerlo) c&oacute;mo se producen los huevos.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>Si conoci&eacute;ramos la triste vida de las gallinas que dentro de jaulas son forzadas a producir huevos a un ritmo antinatural, seguramente nos cuestionar&iacute;amos su consumo.</p>

<p><img alt="" class="wp-image-12187" src="/app/uploads/2017/06/28768099774_792bf23804_z_1.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="http://www.igualdadanimal.org/noticias/7754/gallinas-huevos-y-jaulas-20-centimetros-de-intolerable-sufrimiento" target="_blank">Gallinas, huevos y jaulas: 20 cent&iacute;metros de insoportable sufrimiento</a></strong>

<p>&nbsp;</p>

<p>Para la industria del huevo cada gallina es solo un n&uacute;mero dentro de una l&iacute;nea de producci&oacute;n en donde la productividad impera por encima de cualquier consideraci&oacute;n y respeto por estos animales.</p>

<p>La organizaci&oacute;n Igualdad Animal que se ha infiltrado en granjas industriales de gallinas en Espa&ntilde;a, Italia, M&eacute;xico e India descubri&oacute;:</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<blockquote>
<p>- Gallinas que han perdido su plumaje debido al alto nivel de estr&eacute;s al cual las someten las terribles condiciones de hacinamiento.</p>

<p>- Entre cuatro y ocho gallinas en cada jaula del tama&ntilde;o de dos folios que se pisan unas a otros en busca de espacio.</p>
</blockquote>

<blockquote>
<p>- Gallinas con pies deformados, heridas y grietas debido al suelo de alambre de las jaulas.</p>

<p>- Gallinas con abrasiones en su piel a causa de la alta concentraci&oacute;n de amon&iacute;aco de su desechos, los cuales son recogidos en montones y retirados solo una vez cada pocas semanas.</p>

<p>- Alta mortalidad semanal de gallinas debido a enfermedades producidas por la falta de higiene.</p>

<p>- Gallinas muertas junto a sus compa&ntilde;eras vivas en el interior de las jaulas.</p>

<p>- Gallinas a las que se les inmoviliza dentro de las jaulas para su posterior desecho.</p>

<p>- Gallinas enfermas que no reciben ning&uacute;n tipo de atenci&oacute;n veterinaria.</p>

<p>- Gallinas cuyo pico ha sido mutilado (pr&aacute;ctica est&aacute;ndar en la industria del huevo).</p>

<p>- Gallinas enfermas, abandonadas a nivel de suelo para que mueran sin poder tener acceso al agua y alimento que est&aacute; en las jaulas.</p>

<p>- Gallinas que agonizan en el interior de las jaulas. Ellas son obligadas a vivir dentro de jaulas donde nunca pueden extender las alas.</p>
</blockquote>

<p>&nbsp;</p>

<p>Estos inteligentes y sociables animales sufren todo este brutal maltrato para que produzcan huevos que no necesitamos consumir y que <a href="http://www.igualdadanimal.org/7583/3-increibles-formas-de-sustituir-el-huevo" target="_blank">son f&aacute;cilmente reemplazables por alternativas vegetales</a>.</p>

<p><img alt="" class="wp-image-12188" src="/app/uploads/2017/06/28768101944_9ab038eb2b_z_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Cada huevo que en cuesti&oacute;n de minutos pasa de la sart&eacute;n al plato donde es consumido, implica para cada gallina 26 horas de insoportable sufrimiento en las condiciones descritas anteriormente.</p>

<p>Recientemente, la organizaci&oacute;n Igualdad Animal ha conseguido que gigantescas empresas y cadenas de supermercados en Espa&ntilde;a, Italia y Brasil se surtan de huevos que no provienen del cruel sistema de jaulas.</p>

<p>Y aunque sabemos que &laquo;libres de jaula&raquo; no significa que ellas est&eacute;n libres del maltrato, s&iacute; representa un avance hacia la reducci&oacute;n de su sufrimiento.</p>

<p>&nbsp;</p>

<p>Si quieres ayudar m&aacute;s a las gallinas sustituye el huevo en tu alimentaci&oacute;n. Es muy f&aacute;cil y te va a encantar saber que <a href="http://www.igualdadanimal.org/noticias/7713/6-recetas-para-ayudarte-sustituir-los-huevos-en-tu-alimentacion" target="_blank">en cualquier receta, ya sea de postres o platos salados, el huevo puede ser sustituido</a>.</p>

