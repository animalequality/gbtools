<div class="center"><img class="wp-image-11104" src="/app/uploads/2016/10/gallinas.jpg"></div>
<strong>Es la primera vez que en México se consiguen imágenes de la industria del huevo</strong>, donde sensibles y frágiles animales son convertidos en máquinas de producción.

La investigación se realizó en el estado de <strong>Jalisco, principal región productora de huevo en México</strong> en la que hay cerca de <strong>95 millones de gallinas[1]</strong> que representan más del 50% de la población de gallinas de todo el país.

<strong>Las imágenes tomadas por Igualdad Animal muestran:</strong>

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- El hacinamiento en jaulas de batería.

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- La crueldad de la muda forzada o pelecha.

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Gallinas enfermas sin ningún tipo de atención veterinaria.

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Gallinas inmovilizadas contra las rejas de las jaulas para su desecho.

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Gallinas agonizando al interior de las jaulas.

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Gallinas cuyo pico ha sido mutilado.

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Gallinas muertas dentro de las naves industriales.
<h4><img alt="" class="wp-image-11105" src="/app/uploads/2016/10/fotosgallinas.jpg" style="float:left; margin:10px; width:800px"></h4>
&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

La industria avícola mundial ha transformado la vida natural de las gallinas, vulnerando sus necesidades de socialización, espacio, alimentación, y en general, evitando la expresión de sus comportamientos naturales.

Para lograr que la producción de huevos sea mayor, las prácticas generalizadas en granjas intensivas consisten en <strong>alterar los ciclos de luz y oscuridad de las gallinas y someterlas a la mutilación de su pico</strong>; las gallinas permanecen <strong>hacinadas en el menor espacio posible, durante más de 2 años, en jaulas de batería</strong>.
<p class="ae-video-container"><iframe width="860" height="484" src="https://www.youtube.com/embed/8V339GV_WDU" frameborder="0" allowfullscreen=""></iframe></p>
<strong>Una de las prácticas más crueles es la muda forzada o pelecha que consiste en privar a las gallinas de agua y comida por 3 días</strong> hasta reducir su peso corporal en un 20%, esto se hace para acelerar el proceso de un segundo ciclo de postura de huevo. Esta práctica se llega a realizar hasta <strong>dos veces en la vida de cada gallina</strong>. Muchas morirán en agonía durante y después de esta práctica. <strong>En la industria del huevo no se tiene compasión</strong>.

«Nuestro objetivo es acabar con las jaulas y todo el sufrimiento que esta inhumana práctica conlleva para millones de gallinas. La cría de estos animales en jaulas es una de las prácticas más crueles y que más sufrimiento les provoca», indicó Javier Moreno, cofundador de la organización.

Según Dulce Ramírez, directora ejecutiva de Igualdad Animal México: “En México, más del 90% de las gallinas usadas en la producción de huevo viven en jaulas denominadas de batería. Nuestros investigadores han sido testigos de las más crueles prácticas dentro de esta industria, como la práctica de la muda forzada y el corte de pico. La sociedad tiene derecho a saber cómo se produce lo que consume y en base a eso, tomar decisiones compasivas con los animales. Estas prácticas son legales pero no son justas”.

La organización ha iniciado <a href="http://www.lavidaenunajaula.com" target="_blank" rel="noopener noreferrer">una campaña online</a> pidiendo firmas para acabar con las jaulas.

&nbsp;

[1] SIAP (Servicio de Información Agroalimentaria y Pesquera). 2014. Ave para huevo: población avícola 2005-2014. http://www.siap.gob.mx/resumen-nacional-pecuario/.
