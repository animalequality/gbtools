<div class="center"><img class="wp-image-9642" src="/app/uploads/2012/08/cerdos_murcia_abandono.jpg" /></div>
<p>Agentes del Servicio de Protecci&oacute;n de la Naturaleza (Seprona) de la Guardia Civil de Murcia, en colaboraci&oacute;n con inspectores de la Direcci&oacute;n General de Ganader&iacute;a y Pesca de la Comunidad Aut&oacute;noma de la Regi&oacute;n de Murcia, <strong>inmovilizaron ayer una granja porcina ubicada en el t&eacute;rmino municipal de Lorca</strong> (Murcia) que, en el momento de la inspecci&oacute;n, <strong>manten&iacute;a encerrados a 60 cerdos en total estado de abandono</strong>.</p>
<p><strong>Los animales carec&iacute;an de agua y comida y ya hab&iacute;an comenzado a devorar los cad&aacute;veres de sus compa&ntilde;eros muertos</strong>. Muchos cerdos fueron encontrados agonizantes debido a su extrema desnutrici&oacute;n, mientras que los que se manten&iacute;an en pie presentaban un aspecto casi esquel&eacute;tico.</p>
<p>Las instalaciones, compuestas por varias naves cerradas y parques abiertos, se encontraban en estado ruinoso.</p>
<p>Las actuaciones practicadas ser&aacute;n remitidas a la Consejer&iacute;a de Agricultura y Agua de la Regi&oacute;n de Murcia, por ser constitutivas de varias infracciones en materia de sanidad y bienestar animal.</p>
<p>&nbsp;</p>
<p><strong>► Advertencia: Este v&iacute;deo contiene im&aacute;genes de extrema dureza</strong></p>
<p><iframe allowfullscreen="" frameborder="0" height="281" mozallowfullscreen="" src="https://player.vimeo.com/video/11865604?title=0&amp;byline=0&amp;portrait=0&amp;color=00c4ff" webkitallowfullscreen="" width="500"></iframe></p>
<p><a href="https://vimeo.com/11865604">Im&aacute;genes de Murcia - <strong>Granjas de cerdos</strong> | Una investigaci&oacute;n de Igualdad Animal</a></p>

