<div class="center"><img class="wp-image-12416" src="/app/uploads/2017/09/edith-barabash-former-animal-farmer-turned-vegan_1.jpg" /></div>
<p>A todos nos encantan las historias sobre animales que luego de haber conocido el maltrato pasan el resto de sus vidas en paz, protegidos de toda crueldad.</p>

<p>Pero cuando estas historias tratan sobre animales de granja resultan verdaderamente inspiradoras. &iquest;La raz&oacute;n? Los animales de granja, atrapados dentro de la industria ganadera, <a href="https://igualdadanimal.org/noticia/2016/04/14/el-peor-maltrato-animal-conocido-se-produce-en-las-granjas-y-mataderos/" target="_blank">son v&iacute;ctimas del peor maltrato animal conocido en la historia</a>.</p>

<p>Afortunadamente, para algunos de ellos, tambi&eacute;n hay finales felices.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/08/17/los-granjeros-que-dejaron-de-comer-y-criar-animales-para-salvarlos-de-la-crueldad/" target="_blank">Los granjeros que dejaron de comer y criar animales para salvarlos de la crueldad</a></strong>

<p>&nbsp;</p>

<p>Mike Lanigan, un granjero de tercera generaci&oacute;n dedicado a la cr&iacute;a de animales para la producci&oacute;n de carne, alimentaba a un ternero de su granja cuando, de pronto, tuvo una revelaci&oacute;n. &nbsp;</p>

<p>Pens&oacute; que <strong>el destino de ese ternero estaba muy lejos de ser feliz.</strong> &Eacute;l crecer&iacute;a y ser&iacute;a enviado al matadero y Lanigan, como muchas veces ya lo hab&iacute;a hecho, &laquo;apagar&iacute;a&raquo; una parte de su cerebro para s&oacute;lo pensar en que deb&iacute;a pagar sus deudas y ganarse la vida.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>Pero ese d&iacute;a las cosas no ocurrieron como &eacute;l esperaba: &laquo;De repente, encend&iacute; esa parte de mi cerebro&raquo;. &laquo;Tal vez no tengo que matarte&raquo;, le dijo al ternero.</p>

<p>Lanigan le pregunt&oacute; a Edith Barabash, su cajera en el mercado donde vend&iacute;a sus productos, si ella conoc&iacute;a otra manera en la cual pudieran hacer las cosas. Tras conversar, <strong>entre ambos surgi&oacute; la idea de crear un refugio para animales de granja.</strong></p>

<p><img alt="" class="wp-image-12417" src="/app/uploads/2017/09/sub-buzz-13924-1473440446-1.jpeg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Desde que Lanigan manifest&oacute; sus intenciones a trav&eacute;s de un video en l&iacute;nea por internet, la respuesta de la gente que lo vio fue rotundamente a favor.</p>

<p>Seg&uacute;n Barabash, las personas han cre&iacute;do en la historia de Lanigan debido a lo fuera de lo com&uacute;n que es. Y aunque todo est&aacute; marchando r&aacute;pidamente, Lanigan piensa que, de igual manera, todo esto estaba destinado a ser as&iacute;. &laquo;Las vacas que est&aacute;n aqu&iacute; y las vacas que van a nacer aqu&iacute;, van a morir aqu&iacute;, y no por mi mano&raquo;, asegura.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Las vacas que est&aacute;n aqu&iacute; y las vacas que van a nacer aqu&iacute;, van a morir aqu&iacute;, y no por mi mano&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>La transici&oacute;n a un refugio de animales de granja le cambi&oacute; la vida a Lanigan pero, sobretodo, a las vacas que ahora vivir&aacute;n en paz hasta el final de sus vidas.</p>

<p><br />
&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://www.durhamregion.com/community-story/6872962-uxbridge-cattle-farmer-mike-lanigan-has-a-change-of-heart-decides-to-start-animal-sanctuary/" target="_blank">https://www.durhamregion.com/community-story/6872962-uxbridge-cattle-farmer-mike-lanigan-has-a-change-of-heart-decides-to-start-animal-sanctuary/</a></p>

<p><a href="https://www.thedodo.com/farmhouse-garden-cattle-animal-sanctuary-1999401786.html" target="_blank">https://www.thedodo.com/farmhouse-garden-cattle-animal-sanctuary-1999401786.html</a></p>

