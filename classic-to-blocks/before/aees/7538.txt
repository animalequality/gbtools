El maltrato animal tiene muchas formas y desgraciadas consecuencias. Algunas son más visibles que otras. Nos resultan reconocibles, hasta familiares. Lo verdaderamente perturbador es que <strong>el peor maltrato animal conocido</strong> permanece oculto a nuestros ojos. Hasta ahora.

¿Quién no quiere la abolición de la tauromaquia?, ¿quién no conoce la terrible problemática de los abandonos de animales? A estas alturas, pocas, muy pocas personas.

Y, sin embargo, <strong>el peor maltrato animal</strong> está sucediendo ahora mismo muy cerca de nosotros sin que la mayoría seamos realmente conscientes de él. Hablamos de la ganadería industrial, la causante de las peores torturas imaginables a un número de animales que se escapa a nuestra comprensión.

A continuación te hacemos ver por qué estar contra el maltrato animal supone estar contra la ganadería industrial. Así marcamos la diferencia <strong>cada vez más consumidores</strong>.

<strong>1. Animales maltratados en cadena hasta la muerte</strong>

<img class="alignleft wp-image-10883 size-full" style="float: left; margin: 10px; width: 344px;" src="https://igualdadanimal.org/app/uploads/2016/07/credit_alf_ribeiro_2f_shutterstock.com_shutterstock_233353774.jpg" alt="" width="1200" height="800" />

La industria de la ganadería industrial es una engrasada maquinaria que convierte a sensibles e inteligentes animales en bandejas de carne.

Cada gallina, pollo, cerdo, vaca o ternera en manos de esta industria <strong>vive torturado sistemáticamente</strong> desde el primer día de su vida.

<strong>2. Para que la rueda siga girando hay que producir más y más animales</strong>

<img class="alignleft wp-image-10884 size-full" style="float: left; margin: 10px; width: 349px;" src="https://igualdadanimal.org/app/uploads/2016/07/shutterstock_278174186.jpg" alt="" width="1024" height="681" />

La rueda de producción en la industria ganadera nunca para. Abastecer a los supermercados requiere la crianza de más y más animales.

Las madres son inseminadas artificialmente una vez tras otra. Las crías llevadas lejos de ellas.

<strong>Las familias no existen en la ganadería industria</strong>l. Al dejar de comprar carne, leche o huevos ayudas a parar la rueda.

<strong>3. Golpes, amputaciones, confinamiento</strong>

<img class="alignleft wp-image-10885 size-full" style="float: left; margin: 10px; width: 348px;" src="https://igualdadanimal.org/app/uploads/2016/07/shutterstock_363237860.jpg" alt="" width="1024" height="683" />

Trabajar en granjas industriales y mataderos conlleva la insensibilización ante el sufrimiento de los animales.

De manera cotidiana los trabajadores tienen que manipular a tantos animales para alcanzar la cuota de producción que <strong>el maltrato se convierte en la norma</strong>.

En las granjas los animales viven en condiciones de hacinamiento. A algunas crías, como los cerditos, se les amputa el rabo y se les cortan los dientes y testículos sin anestesia.

<strong>4. Negación de instintos naturales</strong>

<img class="alignleft wp-image-10886 size-full" style="float: left; margin: 10px; width: 372px;" src="https://igualdadanimal.org/app/uploads/2016/07/shutterstock_127005695.jpg" alt="" width="1024" height="595" />

Todos los animales de granja tienen instintos naturales que necesitan satisfacer. En libertad, pueden desarrollarlos y comportarse acorde a su propia naturaleza. En las granjas industriales es bien distinto.

Su vida en ellas se limita a <strong>engordar, producir y morir</strong>. Carecen de todo aquello que necesitan para llevar una vida saludable.

Sufren maltrato animal a diario, como norma.

<strong>5. Mataderos: el infierno en la tierra</strong>

<img class="alignleft wp-image-10887 size-full" style="float: left; margin: 10px; width: 373px;" src="https://igualdadanimal.org/app/uploads/2016/07/shutterstock_306667319.jpg" alt="" width="1024" height="683" />Las bandejas de carne que vemos en los supermercados provienen de la ganadería industrial. <strong>El destino de cada animal en manos de esta industria es el matadero</strong>.

Los mataderos operan a escalas tan grandes para satisfacer la demanda que en apariencia se asemejan a cadenas de montaje.

La muerte es brutal: colgados cabeza abajo, son degollados y mueren desangrándose.

<strong>6. La ganadería industrial se basa en mentiras</strong>

¿Cómo conseguir que el peor maltrato animal conocido pasa desapercibido para sociedades cada vez más concienciadas? Sencillo: mintiendo a gran escala a los consumidores. Somos literalmente bombardeados por la <strong>publicidad falseada</strong> de la industria cárnica, láctea y del huevo. En sus productos vemos animales felices en verdes y soleados prados. Si has leído hasta aquí, ya sabes la escandalosa mentira que supone esta publicidad. Por eso se gastan millones en ella.
