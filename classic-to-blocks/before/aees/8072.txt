Un abogado que persiguió su ideal de justicia a través de su oficio se ha hecho fabricante de zapatos veganos en búsqueda de la justicia social.

Según  David Beriain, esas son las motivaciones que lo llevaron a formar la empresa «Ecoalkesan», para lo cual se unió con su socio, un fabricante de calzado que cuenta con más de 30 años de experiencia y que comparte con él los mismo principios y visión que su proyecto requiere.

«…tenemos muy en cuenta los materiales con los que fabricamos este calzado y la forma en la que lo fabricamos. De este modo, el concepto es aportar valor mediante materiales 100% veganos y estableciendo en todo el proceso relaciones éticas».

Para Beriain y su socio es fundamental el respeto hacia los animales, y por eso sus productos no tienen ningún componente de origen animal. Pero además, su consciencia de respeto se extiende al entorno: la ganadería es responsable de la emisión del 15% de los gases de efecto invernadero y del 91% de la deforestación del Amazonas. De manera que, no podrían ser una empresa sostenible si usaran la piel de animales que se acostumbra a utilizar en la fabricación del calzado convencional.

El calzado de «Ecoalkesan» está hecho con fibras vegetales de gran calidad, obtenidas de la madera gestionada de un modo sostenible, lino, algodón, suelas de TPU ecológico con el 60% de material reciclable de origen vegetal y microfibras de última generación (green fiber), 100% transpirables y antibacterianas. Durante todo el proceso utilizan técnicas tradicionales y maquinarias de última generación, consiguiendo con esto un producto de gran calidad.

En cuanto al precio de estos productos, Beriain también nos tiene una grata respuesta: «hemos logrado una relación calidad precio óptimas. De esta forma, los precios de nuestros productos oscilan entre los 130 euros que cuestan las botas Kosmos y los 12 euros que valen unas plantillas ecológicas y 100% biodegradables. Además, actualmente hay una oferta de lanzamiento de todos nuestros productos del 25%, que es excepcional porque está muy ajustada al precio de coste».
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales?</h4>
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #333333;">¡Suscríbete gratuitamente</span> <span style="color: #0000ff;">a nuestro e-boletín</span>!</a></h4>
En ese mismo orden de ideas, creen en la «ecología de las relaciones», lo cual implica que cada persona implicada en el proceso de fabricación y distribución de los productos reciba su retribución justa por su contribución.

¿Y dónde podemos comprarlos? Todos los modelos están disponibles en la <a href="https://ecoalkesan.com/" target="_blank" rel="noopener">web de la empresa</a> y se entregan a domicilio o en una de las empresas colaboradoras en las que los clientes pueden probarse el calzado.

Ecoalkesan es, sin duda, una propuesta de calzado ético y sostenible, de la más alta calidad a un precio justo y con respeto a todas las relaciones implicadas. Y es que una acción justa como ponerse del lado de los animales siempre deriva en una gran cantidad de beneficios para todos y para el entorno.
