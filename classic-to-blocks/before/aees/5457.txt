<p>
	<img align="left" alt="" height="197" hspace="9" class="wp-image-13700" src="/app/uploads/2010/05/FOTO-CARTEL-SAN-ISDRO-2010-Y-LOS-4-GALAFATES.jpg" vspace="9" width="300" />El antiguo componente de mecano, Jos&eacute; Mar&iacute;a Cano, present&oacute; una pintura que ilustrar&aacute; el cartel de la feria taurina de San Isidro. El autor explic&oacute; que comenz&oacute; a pintar cuadros de tem&aacute;tica taurina cuando hace diez a&ntilde;os le pidieron &ldquo;algo nuevo&rdquo; para una exposici&oacute;n en Nueva York.<br />
	<br />
	Desde entonces, Cano se fue vinculando cada vez m&aacute;s a la tauromaquia y el a&ntilde;o pasado colabor&oacute; con Canal Plus creando im&aacute;genes con momentos destacados de cada corrida retransmitida.<br />
	<br />
	El pintor dijo que ha quedado fascinado por &ldquo;la belleza pl&aacute;stica de la tauromaquia y por su car&aacute;cter metaf&iacute;sico&rdquo; y explic&oacute; que en la tauromaquia &ldquo;dos fuerzas contrapuestas o complementarias convergen en un mismo punto pero no llegan a chocar&rdquo;<br />
	<br />
	Jos&eacute; Mar&iacute;a Cano tambi&eacute;n tuvo tiempo para descalificar las iniciativas abolicionistas contra la tauromaquia: &ldquo;Significan la beatitud a la correcci&oacute;n pol&iacute;tica&rdquo;.<br />
	<br />
	&nbsp;</p>

