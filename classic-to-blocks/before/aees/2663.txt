Este fin de semana Igualdad Animal realizará diferentes actividades en varias de las ciudades donde tiene presencia ahora mismo. Mesas Informativas, acciones para promover el vegetarianismo, proyecciones en la calle, protestas contra el zoo... son algunas de las acciones que llevaremos a cabo en Madrid, Barcelona y Lima. 

En Igualdad Animal tenemos la firme determinación de ayudar a los animales, por eso siempre estamos en la calle, cada fin de semana, prácticamente cada día, luchando por ellos, sin excusas. ¡Únete a nosotras/os!
<a href="mailto:info@igualdadanimal.org">Hazte activista de Igualdad Animal</a>

<strong>Viernes 18 de Mayo:</strong>

Madrid:
18 horas - Mesa Informativa Madrid (frente al Oso y el madroño)
19 horas - Acción por el vegetarianismo (quedamos a las 19 horas en el Oso y Madroño)

<strong>Sábado 19 de Mayo:</strong>

Barcelona:
Participación en el FesttiAttac Barcelona - Parc de l´Espanya Industrial Metro L1 (Plaça de Sants - Hostafrancs), L3 (Sants Estació - Tarragona)

Madrid:
12 a 14 horas - Concentración contra el uso de animales como entretenimiento. (En  el Zoo de Madrid en la Casa de Campo)
19 horas - Acción por el vegetarianismo (quedamos a las 19 horas en el Oso y Madroño)

Lima:
17:30 a 19 horas Igualdad Animal realizará un acto para promover el vegetarianismo en el pórtico del Parque Keneddy de Miraflores de Lima. También se colocará una mesa informativa. 

<strong>Domingo 20 de Mayo:</strong>

Madrid:
19 horas - Acción por el vegetarianismo (quedamos a las 19 horas en el Oso y Madroño)

Contacto Madrid:
<a href="mailto:javierm@igualdadanimal.org">javierm@igualdadanimal.org</a>
Tel - 675 737 459

Contactos Barcelona:
<a href="mailto:angelam@igualdadanimal.org">angelam@igualdadanimal.org</a>
Tel - 655 051 755

<a href="mailto:guillermoc@igualdadanimal.org">guillermoc@igualdadanimal.org</a>
Tel - 629 558 296


Contacto Lima:
<a href="mailto:arturoa@igualdadanimal.org">arturoa@igualdadanimal.org</a>
