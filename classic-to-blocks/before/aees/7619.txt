<div class="center"><img class="wp-image-11306" src="/app/uploads/2016/11/go_vegan_-_copia.png" /></div>
<p>El mundo est&aacute; cambiando y cada vez son m&aacute;s las iniciativas y espacios que ofrecen productos alternativos a la carne. Uruguay, al igual que el resto de los pa&iacute;ses de Latinoam&eacute;rica, es parte de esta tendencia global que salva animales y contribuye a proteger el planeta.</p>

<p>Esto se debe, ciertamente, a que tambi&eacute;n los <a href="https://igualdadanimal.org/noticia/2016/04/29/los-consumidores-tenemos-el-poder-de-ayudar-los-animales-y-al-planeta/" target="_blank">consumidores</a> <strong>somos m&aacute;s conscientes de las pr&aacute;cticas de la ganader&iacute;a industrial</strong>.</p>

<p>A fin de satisfacer las demandas de una tendencia que aumenta constantemente, en 2015 &nbsp;abri&oacute; &ldquo;Go Vegan&rdquo;, <strong>el primer supermercado online de Uruguay especializado en productos que no contienen ingredientes de origen animal</strong>. &nbsp;</p>

<p>A trav&eacute;s de su <a href="https://shop.govegan.com.uy/index.php" target="_blank">p&aacute;gina web</a> ofrecen m&aacute;s de 1000 productos entre los que se encuentran leches vegetales, chocolates, sustitutos del huevo, milanesas, hamburguesas, panes y gran variedad de productos sin ingredientes ni materiales que provengan de animales.</p>

<p><img alt="" class="wp-image-11307" src="/app/uploads/2016/11/7c7f70d0080e53f2fe96ecc7f38a01e614478612_garba3.jpg" style="float:left; margin:10px; width:600px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;Go Vegan&rdquo; cuenta actualmente con <strong>m&aacute;s de 1000 clientes y el n&uacute;mero sigue aumentando, adem&aacute;s de 12.000 seguidores en la p&aacute;gina de <a href="https://www.facebook.com/GoVeganUruguay" target="_blank">Facebook</a></strong>. &ldquo;Recibimos entre 200 y 300 pedidos por mes y tambi&eacute;n eso aumenta consistentemente&quot;, dice Pablo Loschak, cofundador de la empresa.</p>

<p>Los pedidos se realizan a trav&eacute;s de la web. S&oacute;lo es necesario registrarse en <a href="http://www.govegan.com.uy/prestashop/index.php" target="_blank">www.govegan.com.uy</a>, hacer el pedido eligiendo los productos, y luego de confirmarse se hace el env&iacute;o al domicilio indicado.</p>

<p>Son iniciativas como estas que siguen en aumento abri&eacute;ndose camino en todas partes del mundo las que ayudan a surgir nuevas empresas respetuosas con los animales y el medioambiente.</p>

<p>Tenemos el poder de construir un nuevo sistema de producci&oacute;n de alimentos y podemos construir el futuro que queremos para los animales y el planeta.</p>

<p>An&iacute;mate a conocer c&oacute;mo puedes ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscr&iacute;bete a nuestro e-boletin</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://www.elobservador.com.uy/nota/los-veganos-tendran-nuevo-local-en-montevideo-201581212110" target="_blank">https://www.elobservador.com.uy/nota/los-veganos-tendran-nuevo-local-en-montevideo-201581212110</a></p>

<p><a href="https://www.lr21.com.uy/salud/1223857-go-vegan-primer-supermercado-vegano-uruguay" target="_blank">https://www.lr21.com.uy/salud/1223857-go-vegan-primer-supermercado-vegano-uruguay</a></p>

<p>&nbsp;</p>

