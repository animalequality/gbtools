<div class="center"><img class="wp-image-11625" src="/app/uploads/2017/02/foto_fb_10.jpg" /></div>
<p>Harold Brown se cri&oacute; en una granja familiar y pas&oacute; la mitad de su vida dedicado al negocio de la ganader&iacute;a. Un d&iacute;a, <strong>un evento lo llev&oacute; a darle la espalda al modo de vida en que su familia hab&iacute;a vivido y trabajado por cinco generaciones</strong>.</p>

<p><strong>Por a&ntilde;os Harold condujo ganado, hizo castraciones, descorn&oacute; vacas y tambi&eacute;n les mat&oacute;</strong>. Igualmente, trabaj&oacute; durante tres a&ntilde;os en la industria lechera.</p>

<p>Un ataque al coraz&oacute;n a los 18 a&ntilde;os lo llev&oacute; a replantearse su estilo de vida y, a su vez, su conexi&oacute;n con los animales que por a&ntilde;os hab&iacute;a explotado y llamado &laquo;comida&raquo;.</p>

<p><strong>El cambio m&aacute;s profundo que experiment&oacute; fue el comenzar a reconocer a los animales como individuos</strong>: &laquo;me d&iacute; cuenta de que tienen lazos familiares, anhelan la seguridad y experimentan la alegr&iacute;a y la felicidad&raquo;.</p>

<p><img alt="" class="wp-image-11624" src="/app/uploads/2017/02/20051207_farmer-brown.jpg" style="float:right; margin:10px; width:350px" />Para Harold se trat&oacute; de elegir la compasi&oacute;n antes de la violencia y replantearse todo aquello en lo que hab&iacute;a cre&iacute;do y practicado: &laquo;Eleg&iacute; la vida. As&iacute; que no, <strong>en mi experiencia, no hay tal cosa como productos animales humanos, pr&aacute;cticas de cultivo humanas, transporte humano o sacrificio humano&raquo;</strong>.</p>

<p>Desde entonces <strong>dej&oacute; de consumir carne y otros productos de origen animal</strong> y reconoce que esta decisi&oacute;n lo llev&oacute; a ser una persona m&aacute;s compasiva.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Ahora sab&iacute;a con certeza que, independientemente de las racionalizaciones que hab&iacute;a creado, cuando mat&eacute; a un animal y vi que la luz dejaba sus ojos, al extinguir esa chispa divina, hab&iacute;a roto un fideicomiso sagrado&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>Harold <strong>es actualmente un convencido defensor de los animales, de <a href="https://igualdadanimal.org/noticia/2016/03/23/las-profundas-consecuencias-de-un-modelo-alimentario-basado-en-vegetales/" target="_blank">un nuevo y mejor modelo alimentario basado en vegetales</a> &nbsp;y de la no violencia</strong>, tal y como lo demuestra en su sitio web Farm kind .</p>

<p>Desde este proyecto anima y ayuda a los ganaderos que quieran dejar atr&aacute;s el obsoleto sistema de producci&oacute;n de alimentos de origen animal y pasarse a la producci&oacute;n basada en plantas.</p>

<p>Si quieres saber c&oacute;mo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n. Adem&aacute;s, <a href="https://es.loveveg.com/" target="_blank">aqu&iacute; puedes descargar gratis nuestro nuevo eBook con deliciosas recetas saludables y respetuosas con los animales</a>.</p>

