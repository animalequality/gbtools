Madrid - La campaña pretende dar un destino responsable a gatos y perros abandonados y que ahora están en el centro municipal de recogida de animales. 

El Ayuntamiento de Rivas, a través de su página web, ha lanzado una iniciativa que pretende promover la adopción de animales que de momento se encuentran en el Centro de Recogida de Animales ‘Los Cantiles’. Por esta instalación municipal, que depende de la Concejalía de Salud y Consumo, pasan los perros, gatos y otros animales abandonados o no deseados, y están a la espera de que alguien les dé una segunda oportunidad. Se da la circunstancia de que, en verano, y coincidiendo con la época vacacional, el número de abandonos suele aumentar en todo el país. 


