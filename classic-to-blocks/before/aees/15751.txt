Nuestra sociedad se declara en su mayoría en contra del maltrato animal. Rechazamos la violencia contra los toros, las prisiones de los zoológicos y acuarios y que los humillen en los circos. Queremos vivir en un mundo donde estos terribles lugares sean parte del pasado para que los animales puedan vivir en paz y ser felices.

Estas son las expectativas que, en general, tenemos la mayoría de las personas. Pero, de igual manera, muchos no somos conscientes de que muy cerca de nosotros, a escasos kilómetros existen lugares en los que ocurre el peor maltrato animal de la historia: la ganadería industrial. Y dentro de ese sistema se encuentran atrapados los animales más maltratados y matados en mayor número en todo el mundo: los animales de granja.

Si esto ocurre día tras día y muy cerca de nuestras casas, ¿cómo es posible que siga pasando prácticamente desapercibido? La respuesta es que la industria oculta lo que ocurre detrás de sus muros y el sufrimiento de los animales es silenciado tras el hermetismo de su gigantesca maquinaria de crueldad sistemática.

En todos los países donde estamos presentes nos hemos dedicado a sacar a la luz la terrible realidad que viven los animales dentro de la industria. Nuestras investigaciones dentro de la industria del huevo han revelado que las gallinas viven vidas de entero sufrimiento, amontonadas, compartiendo una pequeña jaula con otras cinco gallinas, donde apenas pueden moverse y nunca llegar a estirar las alas. La cría de gallinas en jaulas es uno de los sistemas de producción que genera más sufrimiento a los animales en la ganadería industrial.

Investigación en la industria del huevo en España.

Ellas son animales dotados de gran sensibilidad e inteligencia y todas las privaciones a las que las someten les provocan un inmenso dolor y estrés. ¿Podríamos siquiera imaginar cómo sería vivir toda nuestra vida encerrados en una ascensor junto a otras cuatro personas solo para terminar en un matadero? Nadie merece una vida tan cruel, ¿cierto?

Es por todo lo anterior que para ayudar a las gallinas, nuestro equipo de Vinculación Corporativa se pone en contacto con las empresas y les presenta toda la información sobre el maltrato que implican las jaulas. Algunas empresas se interesan por mejorar las condiciones de vida de las gallinas y deciden casi de manera inmediata hacer una política para ya no ser parte de este sufrimiento, pero algunas otras no lo hacen.

En estos últimos casos es cuando los Defensores y Protectores de Animales informan a las empresas que la sociedad no es indiferente ante el sufrimiento de los animales y que quiere que cambien sus políticas.

Siendo un Protector y Defensor de Animales, puedes realizar desde tu portátil acciones para ayudar a las gallinas, enviando correos y usando las redes para difundir información sobre el terrible sistema de jaula en la producción de huevo, pedirles que las ayuden, así como también formar parte del equipo de Igualdad Animal para hacer frente a la empresas que producen huevos a costa de su sufrimiento.

Ser un Protector de Animales significa poder generar cambios que puedan transformar las terribles condiciones de vida de estos sensibles animales. Las decisiones de las empresas tienen un profundo impacto en la vida de las gallinas, y la opinión más importante para las empresas es la de sus consumidores. Pulsa <a href="https://igualdadanimal.org/defensores-animales/">aquí</a> para ser un defensor de los animales y comenzar a cambiar sus vidas.
