<p>
	&nbsp;<img align="left" alt="" height="180" hspace="9" class="wp-image-14102" src="/app/uploads/2010/03/los-caballos-son-procesados-en-juarez-para-vender-su-carne-ap_2007-10-03.jpg" vspace="9" width="240" /></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">La exportaci&oacute;n de carne de caballo en cortes finos a Europa&nbsp;proporciona a M&eacute;xico unos 60 millones de d&oacute;lares. Los principales pa&iacute;ses&nbsp;importadores son B&eacute;lgica, Holanda, Italia y Francia. M&eacute;xico es el&nbsp;segundo exportador mundial de&nbsp;carne de caballo&nbsp;despu&eacute;s de China.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">Son 40 mil equinos los que se&nbsp;matan en promedio cada a&ntilde;o para la comercializaci&oacute;n de carne de ese animal procesada en su mayor&iacute;a en el rastro Tipo de Inspecci&oacute;n Federal (TIF) ubicado en el municipio de Jerez, Zacatecas.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">De acuerdo con informaci&oacute;n dada a conocer durante la Primera Reuni&oacute;n del Comit&eacute; Nacional Sistema Producto Equino, el coordinador general de Ganader&iacute;a de la Secretar&iacute;a de Agricultura, Everardo Gonz&aacute;lez Padilla, y el presidente de la Confederaci&oacute;n Nacional de Organizaciones Ganaderas (CNOG), Oswaldo Ch&aacute;zaro Montalvo, explicaron que el mencionado centro de procesamiento tiene el reconocimiento de las autoridades mexicanas.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">Cuenta tambi&eacute;n con el aval de los pa&iacute;ses a donde se exporta para el manejo de este alimento c&aacute;rnico, el cual tiene una duraci&oacute;n de 80 d&iacute;as de vida de anaquel en promedio.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">Actualmente, se tiene un censo aproximado de tres millones de animales.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">Durante la reuni&oacute;n, el representante no gubernamental del Comit&eacute;, Jos&eacute; Manuel Alav&eacute;s Gonz&aacute;lez, solicit&oacute; a los participantes la elaboraci&oacute;n de proyectos productivos concretos sobre el impulso y desarrollo de este sector, para que a trav&eacute;s de la Secretar&iacute;a de Agricultura se obtengan los recursos suficientes y se tenga informaci&oacute;n fidedigna que lleve a tomar una mejor decisi&oacute;n para el crecimiento de este sector.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">Explic&oacute; que en pa&iacute;ses como Estados Unidos y, concretamente, en el estado de Kentucky, el sector de los &eacute;quidos le representa anualmente a esa econom&iacute;a cuatro millones de d&oacute;lares.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">En M&eacute;xico, dijo, se podr&iacute;a lograr que este sector tambi&eacute;n fuera un motor de desarrollo, ya que se cuenta con toda la infraestructura necesaria, adem&aacute;s de las condiciones climatol&oacute;gicas para impulsar m&aacute;s este segmento.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">Carlos Guzm&aacute;n Clarck, representante de la Comisi&oacute;n Nacional de Recursos Gen&eacute;ticos, propuso que se utilice toda la infraestructura y organizaci&oacute;n que tiene la Confederaci&oacute;n Nacional de Organizaciones Ganaderas para avanzar en la aplicaci&oacute;n de un censo relacionado con los equinos, a fin de contar con un diagn&oacute;stico lo m&aacute;s aproximado posible.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">La CNOG est&aacute; conformada por 44 Uniones Ganaderas Regionales, Especializadas y de Registro; 8 Uniones Ganaderas Regionales de Porcicultores; una Uni&oacute;n Nacional de Apicultores; 25 Asociaciones Nacionales Especializadas y de Criadores de Ganado de Registro y mil 724 Asociaciones Ganaderas Locales.</span></p>
<p>
	<span class="Apple-style-span" style="font-family: arial, helvetica, sans-serif; font-size: 14px">Estas organizaciones ganaderas se encuentran ubicadas en todo el pa&iacute;s y representan aproximadamente a quinientos mil productores pecuarios, ejidatarios, comuneros, y peque&ntilde;os propietarios.</span></p>
<p>
	<br />
	Investigaci&oacute;n realizada por Igualdad Animal en mataderos espa&ntilde;oles:&nbsp;<a href="http://www.mataderos.info/">http://www.mataderos.info/</a></p>
<p>
	Informaci&oacute;n sobre el uso de animales en el &aacute;mbito de la alimentaci&oacute;n:&nbsp;<a href="https://igualdadanimal.org/problematica/carne/">https://igualdadanimal.org/problematica/carne/</a></p>

