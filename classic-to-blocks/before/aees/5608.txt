Madrid- El pasado&nbsp;domingo 1 de agosto de 2010, tres activistas de Igualdad Animal grabaron las&nbsp;&quot;becerradas&quot; celebradas en El Escorial. A las 19 horas comenzaba en el pueblo madrile&ntilde;o la fiesta denominada &quot;Viudos, Solteros y Casados&quot; que cada a&ntilde;o se cobra la vida de becerras de entre uno y dos a&ntilde;os de vida. Los activistas&nbsp;pudieron documentar con c&aacute;maras de v&iacute;deo&nbsp;y fotogr&aacute;ficas el padecimiento de estos animales que son torturados p&uacute;blicamente en la plaza del pueblo por los vecinos del mismo.<br />
<br />
Las im&aacute;genes grabadas en v&iacute;deo muestran a los becerros gritando, desangr&aacute;ndose y siendo&nbsp;torturados. Estos animales reciben estocadas en el lomo y en los costados con banderillas (palos con arpones de varios cent&iacute;metros). Los mozos del pueblo atacan incesantemente a los animales hasta que &eacute;stos caen desplomados.<br />
<br />
Los activistas han podido grabar c&oacute;mo los becerros jadean sangrando por la boca, para posteriormente sangrar a borbotones en lo que denominan el proceso de descabello. Durante el tiempo que estos animales han estado agonizando, los activistas han documentado toda clase de torturas, incluyendo c&oacute;mo a uno de los becerros le cortaban las orejas cuando permanec&iacute;a todav&iacute;a con vida ante una ni&ntilde;a menor de edad a quien posteriormente se las han entregado, tal y como se puede ver en las im&aacute;genes.<br />
<br />
Desde Igualdad Animal hemos querido recoger estas im&aacute;genes para informar a la sociedad de la tremenda injusticia que padecen los animales y para seguir manteniendo vivo el debate social sobre la tauromaquia y en general sobre la explotaci&oacute;n y esclavitud a la que estamos sometiendo a los animales, no s&oacute;lo en plazas, sino en otros lugares como granjas, mataderos, zoos, circos o laboratorios.<br />
<br />
V&iacute;deos de la tortura: https://vimeo.com/13809298<br />
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="https://vimeo.com/13810353">https://vimeo.com/13810353</a><br />
<br />
Galer&iacute;a fotogr&aacute;fica: <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157624507843821/">http://www.flickr.com/photos/igualdadanimal/sets/72157624507843821/</a><br />
<br />
CONTACTO DE MEDIOS Sharon N&uacute;&ntilde;ez | Portavoz de Igualdad Animal<br />
Tel&eacute;fono: 609 980 196<br />
Correo electr&oacute;nico: sharonn@igualdadanimal.org&nbsp;
