Suiza - Luego de conocer un significativo descenso a finales del siglo XX, los ensayos con animales vuelven a incrementarse, según indica la Fundación Suiza de "Investigaciones 3R".

En su vigésimo aniversario, esta organización respaldada sobre todo por la Confederación y la industria, propone otras opciones para sustituir este tipo de experimentos.

Entre los años 1983 y 2000, la cifra de los animales utilizados en experimentos médicos descendió de 2 millones a 450.000; es decir, en ese periodo se registró una reducción del 80%.

No obstante, la Fundación Suiza de Investigaciones 3R lanza un nuevo llamado de alarma porque, tanto en Suiza como en Europa va en aumento el uso de animales para fines experimentales: en 2005, medio millón de ellos terminaron en un laboratorio.

En opinión del otrora legislador helvético Hugo Wick, miembro fundador y presidente de 3R, “actualmente hay muchos ratones manipulados que son utilizados en los estudios sobre fallas genéticas”.

Para estudiar determinadas enfermedades, los investigadores desactivan los genes de patrimonio hereditario del ratón remplazando o agregando elementos artificiales en el ADN.

La pérdida de esa actividad genética provoca con frecuencia cambios de apariencia y comportamiento en el animal, además de que otras características físicas y bioquímicas también resultan afectadas.

Tan solo para un proyecto de investigación se requieren con frecuencia de miles de ratones. De acuerdo a la Fundación 3R, unos 94.000 ratones genéticamente modificados han sido utilizados en 2005 en Suiza.

La Fundación fue creada en 1987 para promover la investigación y el desarrollo de métodos de experimentación que no recurriesen al uso de los animales.


Fuente: swissinfo
