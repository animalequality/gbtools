Según publica ABC/Cordoba en su versión digital, un “ganadero” de Cardeña
(Córdoba) ha denunciado ante la Guardia Civil la muerte a puñaladas de 35
cerdos y de 16 borregos, así como la de dos caballos y una vaca a tiros,
en una finca de su propiedad ubicada en la aldea de Azuel, donde también
dejaron heridos a otros 100 cerdos y a una yegua. Los hechos ocurrieron
según el propietario de la finca, en la madrugada del viernes día 8 de
septiembre.

El “ganadero” y su cuñado abandonaron la finca sobre las 20:30, «antes de
lo habitual» por motivos familiares. Cuando volvieron se encontraron con
35 cerdos muertos dentro de la nave, mientras que otros cien que lograron
escapar del recinto tenían «signos de haber recibido puñaladas», indicó el
“ganadero”, quien se sorprendió por la dimensión de las agresiones.

Como era de esperar, esta tragedia sólo se ha lamentado en términos
económicos. La Guardia Civil estimó en unos 100.000 euros las pérdidas del
explotador de animales no humanos, debidas a la masacre. Nadie se ha
parado a pensar, y mucho menos el “ganadero”, que 54 individuos han sido
asesinados a puñaladas y más de 100 han resultado heridos. Si las víctimas
hubieran sido humanas todo el mundo lo estaría lamentando. Tanto los
humanos como los demás animales tenemos interés en vivir nuestra vida y
que alguien nos la robe, es una injusta desgracia, ya seamos un cerdo, un
borrego, un caballo, una yegua, una vaca o un humano.
