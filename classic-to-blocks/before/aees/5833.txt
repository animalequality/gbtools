Con motivo del Día Internacional de los Derechos Animales, 250 activistas de<strong> Igualdad Animal</strong> realizaron el pasado  sábado 11 de Diciembre a las 12 del mediodía en la Puerta del Sol de Madrid una impactante protesta en la que sostuvieron en sus manos <strong>250 cadáveres de animales que habían sido víctimas del consumo</strong>, para reivindicar los derechos animales.

Gatos, pollos, cerdos, conejos, gallinas, corderos… son algunos de los 250 animales que formaron parte de la protesta. Recogidos de los contenedores de las granjas donde permanecían apilados o sacados del interior de dichas granjas donde habían muerto, estos animales pasaron de haber sido explotados en vida y finalmente desechados como basura a ser los protagonistas de una reivindicación solemne mediante la que exigimos que los derechos animales sean respetados. Una protesta solemne e impactante con la que queremos provocar un debate en la sociedad que nos lleve a romper la barrera del especie, y ponernos en lugar de los animales, que al igual que nosotros, desean vivir y no sufrir. Al igual que la raza o el sexo, u otras discriminaciones arbitrarias, el especismo debe ser cuestionado y superado, ya que se basa en los mismos principios injustos.

Junto a otras organizaciones en todo el mundo, <strong>Igualdad Animal</strong> celebra desde 2006 el <strong>Día Internacional por los Derechos Animales</strong> coincidiendo con el Día por los Derechos Humanos para reivindicar que el mismo principio igualdad que nos lleva a respetar a otros seres humanos debe ser extendido para proteger a los demás animales.

Los animales nohumanos -no olvidemos que los humanos también somos animales– son igualmente capaces de sentir y tienen intereses propios que merecen el mismo respeto que acordamos entre humanos. La desconsideración de sus intereses o especismo es una discriminación arbitraria, un prejuicio irracional análogo al racismo y al sexismo que debe ser superado. Igualdad Animal quiere animar a todos a respetar a los animales haciéndonos veganos y dejando de utilizar animales ya sea en nuestra alimentación, vestimenta o otros ámbitos de nuestra vida.

<strong>Igualdad Animal</strong> es una organización internacional de derechos animales actualmente presente en España, Inglaterra, Polonia, Venezuela y Colombia dedicada a la educación y concienciación social, la realización de investigaciones sobre la explotación animal y los rescates abiertos de animales.
<strong>Repercusión mediática de la protesta:</strong>

<strong>Vídeos:</strong>
<table border="0" width="700">
<tbody>
<tr>
<td width="16%"><u><a href="https://vimeo.com/17857674">Cuatro</a></u></td>
<td rowspan="13" width="84%"><iframe src="https://player.vimeo.com/video/17759292" width="640" height="512" frameborder="0" allowfullscreen="allowfullscreen"></iframe><a href="https://vimeo.com/17759292">Reuters - Acto de Igualdad Animal por el Día Internacional de los Derechos Animales 2010.</a>.</td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17769958">TV3</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17759384">Telemadrid</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17759292">Reuters</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/18008665">Italia</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17838644">Israel</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17841236">República Checa</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17888936">Euronews</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17759503">Suiza</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17769796">Colombia</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/17769796">México</a></u></td>
</tr>
<tr>
<td><u><a href="https://vimeo.com/18908354">RTVE</a></u></td>
</tr>
<tr>
<td><u>Telecinco</u></td>
</tr>
<tr>
<td><u>Tve</u></td>
</tr>
</tbody>
</table>
<strong>Prensa:</strong>
<table border="0" width="700">
<tbody>
<tr>
<td width="33%"><u><a href="http://www.elpais.com/articulo/madrid/Cadaveres/recordar/derechos/animales/elpepuespmad/20101212elpmad_5/Tes">El País</a> </u></td>
<td width="33%"><u><a href="http://www.larazon.es/noticia/1847-activistas-de-igualdad-animal-protestan-en-madrid-con-animales-muertos">La Razón</a> </u></td>
<td width="33%"><u><a href="http://www.publico.es/espana/351213/igualdad-animal-protesta-en-madrid-con-animales-muertos">Público</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.elmundo.es/elmundo/2010/12/11/madrid/1292083122.html">El Mundo</a> </u></td>
<td width="33%"><u><a href="http://www.efeverde.com/esl/contenidos/noticias/11-diciembre-2010-14-16-00-activistas-de-igualdad-animal-protestan-en-madrid-con-animales-muertos">Efe</a> </u></td>
<td width="33%"><u><a href="http://www.adn.es/tecnologia/20101211/NWS-0422-Activistas-Igualdad-Madrid-Animal-protestan.html">ADN</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.abc.es/agencias/noticia.asp?noticia=621357">ABC</a></u></td>
<td width="33%"><u><a href="http://www.google.com/hostednews/afp/article/ALeqM5gaLgobVu8S4R8WRxDUEdc5m9Nmjg?docId=CNG.04d1fb043cdb24544c73c8ccb99539df.131">AFP</a> </u></td>
<td width="33%"><u><a href="http://noticias.terra.com.ar/sociedad/espana-acto-en-madrid-con-cadaveres-de-animales-para-defender-sus-derechos,24e24a43fb3dc210VgnVCM10000098f154d0RCRD.htm">Terra</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.diariodeleon.es/noticias/noticia.asp?pkid=571008">Diario de León</a> </u></td>
<td width="33%"><u><a href="http://www.ideal.es/granada/v/20101212/sociedad/activistas-igualdad-animal-protestan-20101212.html">Ideal</a> </u></td>
<td width="33%"><u><a href="http://www.eldiariodelarepublica.com/index.php?option=com_content&amp;task=view&amp;id=37646&amp;Itemid=9">Diario de la República</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.lavanguardia.es/vida/20101211/54085658913/igualdad-animal-reivindica-los-derechos-de-los-animales-con-la-exposicion-de-250-cadaveres.html">La Vanguardia</a> </u></td>
<td width="33%"><u><a href="http://ecodiario.eleconomista.es/economia/noticias/2672270/12/10/Economia-Laboral-Trabajo-hace-balance-manana-en-el-Congreso-de-los-426-euros-tras-confirmar-que-no-habra-mas-prorrogas.html">Ecodiario</a></u></td>
<td width="33%"><u><a href="http://www.madridiario.es/2010/Diciembre/madrid/sociedad/196085/protesta-activistas-derechos-animales-muertos-cadaveres-puerta-sol-.html">Madrid Diario</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.lavozdeasturias.es/politica/activistas_animales-dia_mundial_animal_0_388161317.html">La Voz de Asturias</a> </u></td>
<td width="33%"><u><a href="http://www.diariosur.es/agencias/20101211/mas-actualidad/sociedad/activistas-igualdad-animal-protestan-madrid_201012111418.html">Diario Sur</a> </u></td>
<td width="33%"><u><a href="http://www.europapress.es/sociedad/medio-ambiente-00647/noticia-igualdad-animal-reivindica-derechos-animales-exposicion-250-cadaveres-victimas-consumo-20101211183703.html">Europa Press</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.lavozdigital.es/agencias/20101211/mas-actualidad/sociedad/activistas-igualdad-animal-protestan-madrid_201012111418.html">La Voz Digital</a> </u></td>
<td width="33%"><u><a href="http://www.lasprovincias.es/agencias/20101211/mas-actualidad/sociedad/activistas-igualdad-animal-protestan-madrid_201012111418.html">Las Provincias</a> </u></td>
<td width="33%"><u><a href="http://www.hoy.es/agencias/20101211/mas-actualidad/sociedad/activistas-igualdad-animal-protestan-madrid_201012111418.html">Hoy</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.larioja.com/agencias/20101211/mas-actualidad/sociedad/activistas-igualdad-animal-protestan-madrid_201012111418.html">La Rioja</a> </u></td>
<td width="33%"><u><a href="http://feeds.univision.com/feeds/article/2010-12-11/espana-acto-en-madrid-con?refPath=%2Fnoticias%2Fmundo%2Fnoticias%2F">Univisión</a> </u></td>
<td width="33%"><u><a href="http://www.elcorreo.com/agencias/20101211/mas-actualidad/sociedad/activistas-igualdad-animal-protestan-madrid_201012111418.html">El Correo</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.nortecastilla.es/agencias/20101211/mas-actualidad/vida-ocio/activistas-igualdad-animal-protestan-madrid_201012111418.html">Norte Castilla</a> </u></td>
<td width="33%"><u><a href="http://www.eldia.com.ar/edis/20101211/20101211132738.htm">El Día</a> </u></td>
<td width="33%"><u><a href="http://www.diariovasco.com/agencias/20101211/mas-actualidad/sociedad/activistas-igualdad-animal-protestan-madrid_201012111418.html">Diario Vasco</a> </u></td>
</tr>
<tr>
<td width="33%"><u><a href="http://www.rtve.es/mediateca/videos/20110104/on-off-manifestacion-igualdad-animal/980052.shtml">RTVE</a> </u></td>
</tr>
</tbody>
</table>
<strong><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157625450290279/with/5251170297/">Galería fotográfica de la protesta:</a></strong>
<p class="rtecenter"><iframe src="https://www.flickr.com/slideShow/index.gne?group_id=&amp;user_id=23602895@N06&amp;set_id=72157625450290279/with/5251170297&amp;text=" width="500" height="500" frameborder="0" scrolling="no" align="middle"></iframe></p>
