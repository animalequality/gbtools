Triollo, Palencia - Este año es el séptimo en el que se organiza una carrera de burros y un "concurso morfológico" en el municipio de Triollo.

Tras el "concurso morfológico", en el cual un jurado formado por tres veterinarios valoran a los burros en función de características físicas, se celebrará la carrera de burros. En esta participan contra su voluntad decenas de burros con humanos subidos encima de ellos, algunos procedentes de varias provincias. El recorrido es de 200 metros, y se realizan varias eliminatorias.

A los propietarios de los burros "ganadores" (los animales no-humanos son considerados actualmente como propiedades) se les dan varios premios, entre ellos un queso (producto obtenido a costa de la explotación y asesinato de animales).

Asimismo, se elaborará una paella para cuya elaboración serán asesinados miles de individuos (pollos, crustáceos y moluscos).
