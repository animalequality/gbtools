<div class="center"><img class="wp-image-11819" src="/app/uploads/2017/03/shutterstock_206284888.jpg" /></div>
<p>La industria c&aacute;rnica se enfrenta a un esc&aacute;ndalo internacional. Dos de las mayores empresas productoras de carne del mundo que operan en Brasil han sido acusadas de llevar al menos dos a&ntilde;os exportando carne en mal estado a diversos pa&iacute;ses.</p>

<p><a href="https://www.rtve.es/play/videos/telediario/escandalo-brasil-afecta-industria-carnica/3949241/" target="_blank">La operaci&oacute;n policial sobre la industria c&aacute;rnica brasile&ntilde;a</a> es el resultado de una investigaci&oacute;n de dos a&ntilde;os de duraci&oacute;n. A finales de la semana pasada dicha operaci&oacute;n culmin&oacute; <strong>con redadas policiales en plantas de procesado y oficinas</strong> de siete estados brasile&ntilde;os.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>La polic&iacute;a ha acusado a m&aacute;s de 100 personas, en su mayor&iacute;a inspectores de sanidad, por aceptar sobornos para pasar por alto <strong>la venta y exportaci&oacute;n de carne podrida</strong>, falsificaci&oacute;n de documentos y no realizar inspecci&oacute;n alguna en diversas plantas. Tambi&eacute;n se consent&iacute;a la exportaci&oacute;n de carne con trazas de salmonella.</p>

<p>Entre las compa&ntilde;&iacute;as clausuradas se encuentra <strong>la mayor productora de carne del mundo</strong>, JBS y la mayor exportadora de pollo, BRF, junto a docenas de compa&ntilde;&iacute;as menores. El panorama descrito por las autoridades es de corrupci&oacute;n extendida a buena parte del sector c&aacute;rnico brasile&ntilde;o.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;El panorama descrito por las autoridades es de corrupci&oacute;n extendida a buena parte del sector c&aacute;rnico brasile&ntilde;o.&raquo;</p></FONT>

<p>&nbsp;</p>

<p>Algunos de los mayores importadores de carne brasile&ntilde;a como la Uni&oacute;n Europea, Hong Kong, China o Chile <strong>han prohibido</strong> en diverso grado la importaci&oacute;n de carne procedente del pa&iacute;s sudamericano. Por su parte, el Departamento de Agricultura de EE. UU. est&aacute; inspeccionando en estos momentos toda la carne proveniente de Brasil.</p>

<p><img alt="" class="wp-image-11818" src="/app/uploads/2017/03/shutterstock_405742525.jpg" style="margin: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Las compa&ntilde;&iacute;as involucradas en el esc&aacute;ndalo han rechazado las acusaciones y diversas instancias gubernamentales, incluido el Presidente Michel Temer, se han apresurado a salir en su defensa.</p>

<p>Sin embargo, el inspector jefe de la Polic&iacute;a Federal de Brasil, Luis Boudens, <strong>ha defendido la operaci&oacute;n policial</strong>. Boudens alega que las cr&iacute;ticas de la industria y del gobierno carecen de fundamento y est&aacute;n siendo realizadas para defender un sector econ&oacute;micamente importante y a los propios pol&iacute;ticos.</p>

<p>&laquo;Agentes Federales han desmantelado una mafia de hombres de negocios que sobornaban a los inspectores y a pol&iacute;ticos para vender carne sin inspeccionar&raquo;, declara Boudens en la website de la Polic&iacute;a Federal.</p>

<p>&nbsp;</p>

<p><br />
Fuente: <a href="https://www.cnbc.com/2017/03/21/brazil-strives-to-quell-meat-scandal-as-hong-kong-bans-imports.html" target="_blank">https://www.cnbc.com/2017/03/21/brazil-strives-to-quell-meat-scandal-as-hong-kong-bans-imports.html</a></p>

