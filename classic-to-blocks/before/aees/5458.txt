<p>
	<img align="left" alt="" height="267" hspace="9" class="wp-image-13603" src="/app/uploads/2010/05/110909_atun.jpg" vspace="9" width="200" />Una comitiva de medio centenar de cocineros de todo el mundo, entre ellos los espa&ntilde;oles Juan Mari Arzak y Ferr&aacute;n Adri&aacute;, visitar&aacute;n el pr&oacute;ximo martes Barbate para conocer de primera mano la pesca del at&uacute;n de almadraba.<br />
	<br />
	La iniciativa fue ideada por el chef gaditano &Aacute;ngel Le&oacute;n junto al alcalde de Barbate, Rafael Quir&oacute;s. Para promover el consumo de estos animales, el Ayuntamiento ha ampliado este a&ntilde;o la Feria del At&uacute;n a todo el mes de mayo.<br />
	<br />
	Los atunes, como el resto de peces, son animales con un sistema nervioso muy complejo que les dota de una alt&iacute;sima sensibilidad, a pesar de lo que solemos pensar. La pesca, ya sea con redes o con ca&ntilde;as, provoca a los peces terribles sufrimientos. Al ser izados desde las profundidades, los peces sufren una descompresi&oacute;n extrema que hace que sus globos oculares se salgan de sus &oacute;rbitas o revienten, que su vejiga natatoria estalle, que los &oacute;rganos internos queden gravemente da&ntilde;ados o que incluso acaben expulsando el est&oacute;mago y el es&oacute;fago por la boca. Esta descompresi&oacute;n es terriblemente dolorosa como nos podemos imaginar.<br />
	<br />
	La mayor&iacute;a de peces capturados en el mar son matados en el propio barco, muchos de ellos destripados y descuartizados cuando todav&iacute;a est&aacute;n vivos. Incluso tras ser destripados se ha comprobado que pueden estar casi una hora todav&iacute;a vivos y conscientes, sufriendo y revolvi&eacute;ndose.</p>
<p>
	No necesitamos utilizar a los peces ni a ning&uacute;n otro animal para alimentarnos. Podemos adoptar un estilo de vida vegano y evitar una gran cantidad de sufrimiento y muerte de animales cada a&ntilde;o.</p>
<p>
	Para m&aacute;s informaci&oacute;n sobre la vida y la explotaci&oacute;n de los peces, visita:</p>
<p>
	<a href="http://www.sentirbajoelagua.com/">Sentir bajo el agua</a></p>
<p>
	<a href="http://www.granjasdeesclavos.com/peces/como-son">Granjas de esclavos<br />
	<br />
	</a></p>

