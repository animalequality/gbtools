<img class="wp-image-10372" style="float: right; margin: 10px; width: 450px;" src="/app/uploads/2015/12/peceslove.jpg" alt="">Cuando era niño mi madre me llevaba siempre a la misma peluquería. A mí no me gustaba que me cortaran el pelo. El peluquero, un señor con una cara muy seria que nunca sonreía, me daba miedo. Sin embargo, nunca protestaba por tener que ir. ¿El motivo?: el viejo peluquero tenía una gran pecera con muchos peces de colores. A mí me encantaba acercarme y observar a aquellos preciosos animales. Casi pegaba mi nariz al cristal intentando observar todo lo que hacían. Ellos por su parte me miraban con curiosidad y se quedaban allí, a centímetros de mí, observándome.

<strong>Aquellas visitas a la peluquería fueron la semilla de mi amor hacia esos sensibles e inteligentes animales: los peces.</strong>

Ahora, tanto tiempo después, me he convertido en un comprometido defensor de los animales. Tal vez quede en mí algo de aquel niño curioso que pegaba su nariz al cristal de la pecera en la peluquería. Por eso veo con tristeza&nbsp;como maravillosas personas amantes de los animales se olvidan de nuestros compañeros que pueblan ríos y mares.
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/WSs1W7mP29M" width="885px" height="664px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
¿De verdad dejaremos a los peces fuera de nuestro amor por los animales?

Los peces son seres <a href="https://www.youtube.com/watch?v=jGxOK-bS4NM">increíblemente sensibles</a> y <strong>mucho más inteligentes de lo que se pueda pensar</strong>. La ciencia ya sabe a día de hoy que los peces son perfectamente capaces de sentir dolor. ¡<a href="https://www.youtube.com/watch?v=jaUayYNFKpM">Hasta les encanta jugar con otros animales</a>!

Y sienten mucho, pero mucho dolor. Puede que nunca lo hayas pensado, pero <strong>los peces son los animales más consumidos en el mundo</strong>. Las cifras son astronómicas. Resulta difícil incluso visualizarlas: se ha estimado que entre <strong>1 y 3 billlones de peces (1.000.000.000.000 a 3.000.000.000.000) son capturados al año en todo el mundo</strong>.

Estos sensibles animales son pescados en gigantescas redes de pesca. Su muerte es horrible, <strong>su agonía se puede alargar minutos y hasta horas</strong>. La mayoría de ellos mueren aplastados en las redes, el resto morirá de asfixia, congelación o <a href="https://www.youtube.com/watch?v=ye8WUxgFva8">diseccionados vivos con cuchillos</a>.

A esta cifra se suman los peces provenientes de piscifactorías. Se estima que <strong>entre 37.000 y 120.000 millones de peces mueren anualmente en piscifactorías alrededor del mundo</strong>. Los métodos con los que les matan son lentos e inhumanos.

La ciencia no sólo afirma que los peces sienten dolor, <strong>ahora se sabe que también sienten miedo</strong>. ¿Imaginas el miedo de estos animales al ser brutalmente extraídos de su medio, el agua, por las redes de pesca?

El Profesor Donald Broom, biólogo de la Universidad de Cambridge, resume todo lo dicho afirmando que «<em>hay algunas diferencias sensoriales entre los peces y los mamíferos porque los peces viven en el agua, <strong>pero los peces sienten dolor de manera muy parecida a mamíferos y aves</strong></em>».

Por su parte, Profesor John Webster, investigador del bienestar animal de la Universidad de Bristol, ha afirmado que «<em><strong>afirmar que los peces no sienten dolor ya no puede ser defendido por ningún científico</strong></em>».

¿Y qué dirán los amantes de los animales? ¿Estaremos del lado de estos inteligentes y sensibles animales? ¿Escucharemos sus silenciosos gritos pidiéndonos ayuda? <strong>Gritos tan desesperados como los de nuestros queridos perros y gatos cuando son abandonados</strong>.

El niño que aún hay dentro de mí, aquel que una vez se hizo amigo de aquellos preciosos peces, espera optimistamente que así sea.
