<div class="center"><img class="wp-image-12508" src="/app/uploads/2017/10/carne-limpia.jpg" /></div>
<p>La llamada &laquo;carne limpia&raquo; sigue acaparando titulares en todo el mundo. Y <strong>el acuerdo entre los gobiernos de China e Israel para la exportaci&oacute;n de dicha carne al gigante asi&aacute;tico ha sido, sin duda, hist&oacute;rico</strong>. &nbsp;</p>

<p>&iquest;Pero cu&aacute;l es el verdadero impacto de los 300 millones de d&oacute;lares que China est&aacute; invirtiendo en la &laquo;carne limpia&raquo;?</p>

<p>&nbsp;</p>

<h4>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/03/08/la-carne-limpia-cada-vez-mas-cerca-de-los-supermercados/">La &laquo;carne limpia&raquo;, cada vez m&aacute;s cerca de los supermercados</a></h4>

<p>&nbsp;</p>

<p><img alt="carne limpia memphis meat" class="wp-image-12509" src="/app/uploads/2017/10/carne_limpia_memphis_meat_0.jpg" style="margin: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>La ganader&iacute;a industrial es una de las principales responsables de la emisi&oacute;n de gases de efecto invernadero y para China esto solo sigue empeorando. Este tipo de de carne producida a partir de c&eacute;lulas animales cambiar&aacute; por completo el mercado mundial de la carne ya que <strong>disminuye en un 90% el mon&oacute;xido de carbono producido por la industria ganadera y en un 99% el uso de tierras para cultivo y pastoreo</strong>.</p>

<p>&nbsp;</p>

<p><font size="1"><font color="#808080">&copy; Memphis Meats</font></font><br />
&nbsp;</p>

<p>Actualmente, muchas regiones en China est&aacute;n sufriendo una gran contaminaci&oacute;n. Inclusive, en los peores d&iacute;as, los ciudadanos deben usar m&aacute;scaras para filtrar el aire y poder respirar. Es por esto que <strong>desde hace un a&ntilde;o el gobierno chino decidi&oacute; combatir la contaminaci&oacute;n reduciendo en un 50% su consumo de carne</strong>. Este ambicioso objetivo, por dem&aacute;s imprescindible, result&oacute; en la firma del acuerdo con Israel para la importaci&oacute;n de 300 millones de d&oacute;lares de &laquo;carne limpia&raquo;.</p>

<p><img alt="Cerdo en China" class="wp-image-12512" src="/app/uploads/2017/10/cerdos_china.jpg" style="margin: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>De las ochos empresas que en el mundo se dedican a desarrollar tecnolog&iacute;a para su producci&oacute;n, tres se encuentran en Israel y son <a href="http://supermeat.com/meat.html">SuperMeat</a>, <a href="https://www.modern-agriculture.org/">Future Meat Technologies</a> y <a href="https://www.nextnature.net/themes/meat-the-future/">Meat the Future</a>. Grandes inversionistas chinos ya han dado millones a empresas estadounidenses como <a href="https://impossiblefoods.com/">Impossible Foods</a>, productora de la hamburguesa vegetal que &laquo;sangra&raquo; y <a href="https://www.eatjust.com/en-us">Hampton Creek</a> que produce mayonesa sin huevo y, m&aacute;s recientemente, se ha iniciado tambi&eacute;n con la carne limpia.</p>

<p>&nbsp;</p>

<p>Por supuesto, todo lo mencionado tiene un impacto gigantesco en los animales ya que esta es una carne cuya producci&oacute;n no requiere criarlos ni matarlos. China es el mayor productor de carne en el mundo, con un total del 20% a nivel mundial. S&oacute;lo <strong>en este pa&iacute;s se cr&iacute;an y matan por su carne 700 millones de cerdos, la mitad de la poblaci&oacute;n de estos animales en todo el planeta</strong>.</p>

<p>&nbsp;</p>

<p>Pero tambi&eacute;n existen dos principales preocupaciones alimentarias y ambientales para los gobiernos del mundo:<strong> alimentar a &nbsp;9.700 millones de personas en 2050 y la inversi&oacute;n del cambio clim&aacute;tico. Afortunadamente, ambos desaf&iacute;os pueden ser resueltos a trav&eacute;s de la &laquo;carne limpia&raquo;</strong>.</p>

<p>&nbsp;</p>

<p>Y es que ante todos estos datos resulta obligatorio plantearnos la pregunta que el diario &laquo;China Science and Technology Daily&raquo; hac&iacute;a a sus lectores con referencia a la &laquo;carne limpia&raquo;: &laquo;Imagina el futuro. Tendr&aacute;s dos productos id&eacute;nticos. Para el primero se cr&iacute;a y se mata al animal. El segundo es exactamente igual, m&aacute;s barato, sin emisiones que producen calentamiento global, sin que muera ning&uacute;n animal&hellip; &iquest;Cu&aacute;l elegir&iacute;as?&raquo;.</p>

<p>Fuente:</p>

<p><a href="https://www.timesofisrael.com/china-makes-massive-investment-in-israeli-lab-meat-technology/">https://www.timesofisrael.com/china-makes-massive-investment-in-israeli-lab-meat-technology/</a></p>

<p>&nbsp;</p>

