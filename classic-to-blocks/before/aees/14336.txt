<p>Ha fallecido con 270 años en el "zoo" de El Cairo la tortuga gigante que se cree que era la más vieja del mundo. La muerte se ha producido por su longevidad, con esta edad ya estaba prácticamente incapacitada.</p>Ha fallecido con 270 años en el "zoo" de El Cairo la tortuga gigante que se cree que era la más vieja del mundo. La muerte se ha producido por su longevidad, con esta edad ya estaba prácticamente incapacitada.

Esta tortuga llevaba encerrada al menos desde que fue regalada al "zoo" por el rey Faruk (1936-1952). Dos semanas antes había muerto Adyaita, otra tortuga gigante de al menos 250 años, en el "zoo" de Calcuta, donde llevaba recluída desde finales del siglo XIX.

En ambos casos dada la longevidad de la que pueden disfrutar estos animales, su encarcelamiento, y por lo tanto su sufrimiento, puede durar siglos.
