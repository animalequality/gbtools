A pesar de no haber conocido nada más aparte de la granja lechera donde había pasado toda su vida siendo utilizada por su leche, Brianna deseaba que su vida fuera más que eso y un impulso incontenible por perseguir su libertad le confirmaba tal esperanza.

Y así fue como, llegado el día en que dejó de ser rentable para la industria debido a que su producción de leche había descendido, <strong>Brianna fue enviada al matadero.</strong> Y en el camino, seguramente al ver y sentir por primera vez la luz del sol y la brisa, se decidió a dar ese gran salto de esperanza de casi dos metros y medio de altura que la sacó del camión y la llevó a su libertad. Brianna paralizó el tráfico de Nueva Jersey con su valiente hazaña y fue rescatada por el refugio <a href="https://skylandssanctuary.org/">Skylands Animal Sanctuary &amp; Rescue</a> y trasladada sana y salva hasta su sede. Apenas dos días después de lo ocurrido, el pasado sábado 29 de diciembre, otro inesperado acontecimiento sorprendió a todos: Brianna dio a luz a Winter.

&nbsp;
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</h4>
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;">¡Suscríbete gratuitamente a nuestro e-boletín!</span></a></h4>
&nbsp;

«Cuando la llevamos a la carretera, no sabíamos que estaba embarazada», dijo Mike Stura, fundador y presidente del refugio. «Es extraño que ella (Brianna) estuviera allí (en el camión del sacrificio) estando embarazada y saludable», continuó. Stura quedó desconcertado al pensar cómo a una vaca tan joven como Brianna fue enviada al matadero, pero la verdad es que en la industria cualquier vaca «gastada», es decir, que ya no produzca suficiente leche, es desechada.

Brianna y Winter permanecen estos días muy unidas y felices. La becerra puede tomar libremente la leche que su madre ha producido para ella, muy al contrario a cómo sucede dentro de la industria donde las habrían separado para que esa leche fuera vendida, y Winter estaría destinada al mismo destino que su madre: ser separada una y otra vez de sus hijos sufriendo hasta ser matada al dejar de ser útil. Stura afirmó que Brianna se ha comportado muy afectuosamente con él luego de que la ayudara a traer al mundo a Winter y que tanto la becerrita como su mamá lo miraban fijamente tras el alumbramiento. Y esto no debería extrañarnos ya que <strong>las vacas son animales empáticos y afectuosos que pueden desarrollar fuertes vínculos</strong> emocionales con las personas que las hacen sentir seguras y queridas.

Brianna y Winter han sido tan afortunadas que podrán vivir el resto de sus vidas en el refugio Skylands Animal Sanctuary &amp; Rescue, mostrando al mundo que los animales no son máquinas de producir.

Fuente: https://www.njherald.com/20190101/brianna-gives-birth-at-wantage-sanctuary#
