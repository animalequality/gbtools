Ian Himmelstein, <strong>un niño de 8 años</strong> que actualmente se encuentra en plena lucha contra la leucemia que padece,<strong> ha decido donar todos sus ahorros a la Sociedad Protectora de Animales </strong>del Condado de Suffolk, convirtiéndose, según la prensa internacional, en un “héroe de los animales”.

Pese a estar viviendo una situación muy dura, en plena tratamiento de quimioterapia, Ian afirma que los animales necesitan ese dinero más que él mismo: "<em>He estado ahorrando dinero para un viaje a causa de mi cáncer, pero <strong>pensé que los animales lo necesitaban mucho más que yo</strong></em>”.

El otoño pasado, Ian fue atacado y herido por el perro de un vecino; pese a ello, el joven dijo que su amor por los animales no había cambiado:

<img class="alignleft wp-image-9478 size-full" src="https://igualdadanimal.org/app/uploads/2012/04/ian_donate.jpg" alt="" width="300" height="200" />

<em>"Me encantan los perros, si</em><em>mplemente no me gusta <strong>ese </strong>perro</em>", dijo Ian.

El jefe de dicha Sociedad Protectora afirma sentirse abrumado al ver que un niño pueda sentir tal pasión por los

animales, llegando a tener un gesto tan solidario y compasivo. Según dice, “<em>Ian es un joven maravilloso”</em>.

Parte de la donación de Ian será utilizada de inmediato en ayudar en un refugio establecido tras un reciente incendio forestal.
