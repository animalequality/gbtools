La actriz Natalie Portman, conocida por sus películas y por ser ganadora de un Óscar, se ha parado detrás de las cámaras para hacernos conocer la urgencia de algo que nos afecta a todos: debemos cambiar la forma en que nos alimentamos, y debemos hacerlo lo antes posible.

«Eating Animals» o «Comer animales», es el muy esperado documental que lleva a los espectadores a un viaje por el sistema que cría y envía al matadero a más de 60 mil millones de animales al año. Para ello, el equipo se introdujo de forma encubierta en granjas industriales y mataderos a fin de exponer la realidad de lo que allí ocurre.

De acuerdo con los mismos realizadores, lo que pretenden es plantear la necesidad de revolucionar el sistema de producción de alimentos que ha tenido vigencia en los últimos 40 años. En realidad, cuenta la historia del fin de la agricultura industrial, que provoca tanto sufrimiento a los animales y que perjudica gravemente al medioambiente y nuestra salud.

El documental es una adaptación del libro homónimo de Jonathan Safran Foer, aclamado por la crítica en 2009 y está coproducido y narrado por Portman. Ha recibió grandes elogios, entre ellos, la crítica considera que aunque existen documentales similares a «Eating Animals», ninguno muestra la verdad de manera tan gráfica y cruda.

<iframe src="https://www.youtube.com/embed/rT5WgiY9508" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

En los primeros segundos de la cinta nos plantea la siguiente interrogante: «¿De dónde vienen los huevos, los lácteos y la carne?» Para responder, nos muestra el lado oculto de la industria ganadera, aquel en donde el maltrato hacia los animales es la norma, haciendo que abramos los ojos ante todo este horror que esconden las grandes compañías.

&nbsp;
<p style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales?</p>
<p style="text-align: center;"><span style="color: #333333;"><a style="color: #333333;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente <span style="color: #0000ff;">a nuestro e-boletín!</span></a></span></p>
<p style="text-align: left;">«No vamos a poder seguir por el mismo camino mucho tiempo, y si las cosas no cambian hacia un modelo más sostenible, la industria podría colapsar», advierte Portman, quien dejó de consumir carne, huevos y lácteos en 2009, luego de leer el libro de Safran Foer.</p>
Por su parte, el director de la película, Christopher Quinn, comparte lo impactante que fue la experiencia de infiltrarse en la industria y proyecta el futuro con optimismo: «Fue una verdadera revelación descubrir lo que es la explotación industrial de animales, lo que la industria cárnica no quiere que veas, no quieren que veas el sistema que existe, incluyendo estas estructuras verticalmente integradas. Ellos saben en lo más profundo de su corazón que no está bien, pero en realidad pienso que aún hay esperanza en el hecho de que saben que un tipo con una cámara no debería venir aquí porque está mal».

«Eating Animals» empezará su recorrido por las salas de cine de todo el mundo en junio, pero ya puedes comenzar a ayudar a los animales. Millones de personas en todo el mundo ya han decidido cambiar a una forma de alimentación más compasiva, sostenible y saludable.
