<div class="center"><img class="wp-image-12308" src="/app/uploads/2017/08/shutterstock_476293381.jpg" /></div>
<p>En Estados Unidos y Europa los productos alternativos a la carne se est&aacute;n haciendo cada vez m&aacute;s presentes y aparecen m&aacute;s empresas que los fabriquen.</p>

<p>&iquest;Pero pasa lo mismo en Latinoam&eacute;rica?</p>

<p>La respuesta no solo es positiva sino que esto ha ocurrido de una forma mete&oacute;rica. <strong>En un gigante como Brasil, en un tiempo r&eacute;cord de apenas tres a&ntilde;os, el mercado de alimentos producidos a base de vegetales se ha disparado enormemente.</strong></p>

<p>De acuerdo con Gustavo Guadagnini, director de la organizaci&oacute;n Good Foods Institute, las empresa que producen este tipo de alimentos en el pa&Iacute;s est&aacute;n creciendo a una tasa del 40% anual.</p>

<p>GFI es una organizaci&oacute;n sin fines de lucro que apoya a empresas de productos vegetales y a empresas productoras de &laquo;carne limpia&raquo;.</p>


<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/02/08/el-mercado-de-alternativas-la-carne-crece-un-40-cada-ano-en-brasil/" target="_blank">El mercado de alternativas a la carne crece un 40% cada a&ntilde;o en Brasil</a></strong>

<p>&nbsp;</p>

<p>En Brasil est&aacute; trabajando para ayudar a estas empresas con el desarrollo de sus productos, su financiaci&oacute;n y promoci&oacute;n porque <strong>creen firmemente que este mercado seguir&aacute; creciendo exponencialmente en los pr&oacute;ximos a&ntilde;os.</strong></p>

<p><img alt="" class="wp-image-12309" src="/app/uploads/2017/08/good-food-institute-talks-plant-based-foods-beverages-in-brazil_strict_xxl.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>Guadagnini asegura que todo esto ocurrir&aacute; porque la demanda de los consumidores es muy alta, incluyendo (y sobre todo) a aquellos no vegetarianos.</p>

<p>Desde 2014, los productos han experimentado una dr&aacute;stica transformaci&oacute;n. <strong>De existir solamente una mezcla de queso en polvo y varios productos de soya, ahora se pueden ver l&iacute;neas completas de carne vegetal </strong>y alternativas l&aacute;cteas.</p>

<p>Marcas como Goshen, Vegabom, Veggy, NoMoo y Superbom presentan opciones que incluyen leches vegetales, quesos a base de papa, alternativas a la carne hechas a base de soya y mayonesa sin huevo.</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>Leches vegetales de la marca A tal da castanha.</p></FONT>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Las empresas que operan en el sector del mercado de alternativas a la carne est&aacute;n reportando un fuerte crecimiento positivo, y el n&uacute;mero de minoristas interesados ​​en vender estos productos est&aacute; aumentando dram&aacute;ticamente&raquo;, asegur&oacute; Guadagnini.</p></FONT>

<p>&nbsp;</p>

<p>Y seg&uacute;n Guadagnini, <strong>hoy en d&iacute;a es mucho m&aacute;s f&aacute;cil encontrar productos l&aacute;cteos a base de vegetales no solo en las principales ciudades</strong>, sino tambi&eacute;n en las m&aacute;s peque&ntilde;as: &laquo;A partir de aqu&iacute;, es solo cuesti&oacute;n de tiempo antes de que estos productos sean prol&iacute;ficos en todo el pa&iacute;s&raquo;, asegura.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p><strong>La industria de la carne brasile&ntilde;a tiene un impacto dram&aacute;tico a nivel mundial. </strong>Es por eso que nos preguntamos si los consumidores de Brasil ser&aacute;n receptivos a la &laquo;carne limpia&raquo; o cultivada. Guadagnini asegura que s&iacute; y que como en cualquier mercado de consumo ser&aacute; necesario comunicar claramente sobre los beneficios y la seguridad de esta carne.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Teniendo en cuenta el impacto medioambiental positivo, la mejora del bienestar animal y la mejor calidad esperada de estos productos (esta carne estar&aacute; libre de contaminantes como la fiebre aftosa y la salmonella, que son problemas en la producci&oacute;n convencional de carne), no hay raz&oacute;n por la cual los brasile&ntilde;os no acoger&aacute;n la carne limpia&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>Los avances que est&aacute;n teniendo empresas productoras de carne limpia como Memphis Meats han logrado una gran recepci&oacute;n positiva en las redes sociales en el pa&iacute;s.</p>

<p><img alt="" class="wp-image-12310" src="/app/uploads/2017/08/carne_limpia_memphis_meat.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>Actualmente, JBS (la mayor compa&ntilde;&iacute;a de carne del mundo) y BRF, con sede en Brasil, est&aacute;n envueltas en un esc&aacute;ndalo de corrupci&oacute;n y sanidad por conspirar con funcionarios de salud en permitir que la carne podrida que estas compa&ntilde;&iacute;as adulteraron con &aacute;cido se vendiera al p&uacute;blico.</p>

<p>La compa&ntilde;&iacute;a matriz de JBS fue multada con un r&eacute;cord de 3,2 mil millones de d&oacute;lares, una cantidad que supone un retraso en la industria de la carne de Brasil durante un tiempo en que los productos a base de plantas contin&uacute;an en auge.</p>

<p>Y a la luz de estas recientes acusaciones el p&uacute;blico est&aacute; m&aacute;s abierto que nunca a considerar nuevas inteligentes ideas para la producci&oacute;n de carne.</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>&laquo;Carne limpia&raquo; de la empresa Memphis Meats &copy;Memphis Meats</p></FONT>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="http://vegnews.com/articles/page.do?pageId=9600&amp;catId=1" target="_blank">http://vegnews.com/articles/page.do?pageId=9600&amp;catId=1</a></p>

<p><a href="https://www.foodnavigator-latam.com/Article/2017/06/01/Good-Food-Institute-talks-plant-based-foods-beverages-in-Brazil" target="_blank">https://www.foodnavigator-latam.com/Article/2017/06/01/Good-Food-Institute-talks-plant-based-foods-beverages-in-Brazil</a></p>

