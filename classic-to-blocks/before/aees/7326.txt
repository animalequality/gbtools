<img class="wp-image-10329" style="float: right; margin: 10px; width: 450px;" src="/app/uploads/2015/10/festejostaurinos.jpg" alt="">El pleno del Parlamento Europeo ha acordado prohibir la financiación de la Tauromaquia con subvenciones públicas europeas. La enmienda presentada por el grupo de los&nbsp;Verdes/ALE en la Eurocámara<strong> ha conseguido 438 votos a favor y 199 en contra</strong>. Por lo tanto, los presupuestos europeos de 2016 no podrán incluir ayudas públicas a la cría de toros para su uso en espectáculos de Tauromaquia.

Para Los Verdes, el Convenio Europeo sobre protección de los animales en las ganaderías es claro: "los animales no deben sufrir dolor, lesiones, miedo o ansiedad”. “Los ganaderos que se dedican a la cría de toros para corridas no cumplen con estos requisitos y, por tal motivo, no deberían recibir subvenciones”. La formación verde reclama que la Comisión Europea, como garante de los Tratados, “informe sobre las medidas que deben tomarse para cumplir con el mencionado Convenio”.

"<em>Desde Igualdad Animal felicitamos al grupo parlamentario Los Verdes/ALE por la iniciativa y a todos los políticos que han votado a favor de la enmienda. Nos alegramos de este importante paso para acabar con la tauromaquia en España, un espectáculo que si todavía no ha desaparecido es precisamente por las subvenciones que recibe.</em>" Indicó Amanda Romero, directora de Igualdad Animal en España

Igualdad Animal lanzó en junio de 2015 la campaña "<a href="https://www.festejoscrueles.org/">Festejos Crueles</a>", con un impactante reportaje que mostraba la crueldad de los festejos taurinos y en el que reclamaba el fin de las subvenciones por parte de la Unión Europea a la tauromaquia.

&nbsp;
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/wScdq4HWtuQ" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
&nbsp;

&nbsp;

&nbsp;
