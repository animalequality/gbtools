<a href="http://diagonalperiodico.net/spip.php?article5914"><strong>Nuestros hábitos deben respetar a los demás animales</strong></a>
ENTREVISTA // JOSÉ VALLE, DE IGUALDAD ANIMAL

Pubicado en el nº 77 de Diagonal
Versión Online: <a href="http://diagonalperiodico.net/spip.php?article5914">http://diagonalperiodico.net/spip.php?article5914</a>

Alvar Chalmeta, Redacción
Por primera vez en el Estado, un activista por la liberación animal logró grabar en vídeo la cotidianidad de los mataderos. Y las imágenes están siendo difundidas.

<strong>DIAGONAL : Al presentar la investigación hablas de haber “logrado introducirte y grabar con cámara oculta”. ¿Hay opacidad ?</strong>

JOSÉ VALLE : Mataderos es una investigación abierta en la que, por el momento, hemos entrado en cinco mataderos. Seguimos trabajando en ella y, ahora mismo, hay un activista de Igualdad Animal que ha conseguido entrar en uno de aves y trataremos de ampliar esta investigación con nuevos datos e imágenes. La industria de explotación animal es consciente del efecto negativo que tiene en su imagen pública, con el consiguiente descenso de sus beneficios, el que salga a la luz lo que hacen, y por tanto no es de extrañar que traten de evitarlo y haya quienes les ayuden. Una vez que salen a la luz pública las escenas que tienen lugar en los mataderos –brutalidades, descargas eléctricas...– éstos suelen reaccionar negando los hechos o bien despidiendo a los trabajadores implicados, de modo que culpabilizan a uno o dos individuos concretos y a cambio evitan la responsabilidad directa que tienen. Algunos asesores de las industrias de explotación animal recomiendan ahora a sus clientes que sean ellos quienes se anticipen a los activistas y que instalen cámaras en el interior, en un intento de frenar determinados comportamientos y de dar imagen de transparencia y normalidad. Se han dado cuenta de que la opacidad y hermetismo despiertan sospechas y rechazo. Aparte de esto, es prácticamente imposible impedir que una cámara oculta entre en estos lugares. La tendencia es a aumentar los controles y tratar de que la sociedad acepte la existencia de los mataderos. Éstos, al fin y al cabo, hacen el trabajo sucio que los consumidores les solicitan.

<strong>D. : ¿Han intentado impedir su difusión?</strong>

J.V. : Varios matarifes nos han amenazado para que retirásemos las fotografías y vídeos de la investigación. Han evitado dar sus nombres o mencionar para qué matadero en concreto trabajan, porque saben que tienen más que perder que nosotros a este respecto. Antes de publicar la investigación contamos con asesoramiento legal sobre este tema, y la legislación al respecto es muy clara. Pero aun en el supuesto caso de que hubiese una sentencia que nos obligase a retirar las imágenes de nuestra web específica sobre la investigación (www.mataderos. info), aparecerían publicadas inmediatamente en otros sitios web de distintas partes del mundo. Su difusión es ya imposible de impedir. Tal y como estamos comprobando, poca gente permanece inmutable ante el aterrorizado rostro de un animal no humano a punto de ser matado. La industria de la explotación animal tienen grandes recursos y poder político. Creemos que tenemos la razón de nuestra parte y también una fuerte determinación por luchar por los animales. No dudamos de que esto será más fuerte.

<strong>D. : Los mataderos forman parte de una industria boyante y en expansión. ¿Cómo es esa industria?</strong>

J.V. : Suele haber cada vez menos mataderos públicos y más mataderos de carácter privado. En el Estado español hay cientos de mataderos a las afueras de nuestras ciudades. La mayoría son de carácter comarcal y matan a varios cientos de animales cada día, mientras que hay otros en los que las víctimas diarias se cuentan por miles. Hay mataderos de aves en que el ritmo es de 180 aves por minuto. Hay que tener en cuenta que son alrededor de 824 millones de animales los que son matados en estos lugares en el Estado español ; eso sin contar a los peces, cuyo número es incalculable.

<strong>D. : ¿La comprobación de que las condiciones en las que son explotados los animales afectan a los humanos –‘vacas locas’, ‘gripe aviar’– modifica los hábitos de consumo?</strong>

J.V. : Somos escépticos sobre si estos episodios de enfermedades resultan en un beneficio real para otros animales. Pescanova, por ejemplo, ha aumentado un 7% sus acciones tras conocerse los dos últimos casos de humanos afectados por la enfermedad de Creutzfeldt- Jakob, aumento que resulta de un cambio (probablemente temporal) en la tendencia del consumo : la gente compra menos carne de ternera y más peces. La preocupación por cómo afectan a la salud humana las condiciones en que viven los no humanos me resulta muy egoísta y antropocéntrica. La propia industria de explotación animal incorpora reformas en los modos de confinar, transportar y matar porque todo ello afecta al sabor de la carne, esto es, realmente está protegiendo sus beneficios, no a los animales. En mi opinión, nuestros hábitos alimenticios deben incorporar el respeto hacia los demás animales. Que dejemos de pensar tanto en nosotros mismos y empecemos a valorar también las vidas de los demás.

<strong>“LA EXPLOTACIÓN ANIMAL TIENE SU BASE EN EL CONSUMO”</strong>

<strong>DIAGONAL : ¿El movimiento antiespecista tiene puntos de confluencia o alianza con otros movimientos sociales ?</strong> JOSÉ VALLE : La defensa de los derechos animales o por la igualdad animal incluye, necesariamente, la defensa de los derechos humanos, dado que los humanos también somos animales, y las bases en que se sustenta la opresión hacia las diferentes víctimas suelen ser comunes.

<strong>D. : Propones el veganismo (abstenerse por completo del uso o consumo de productos de origen animal) para mantener una relación justa con los animales. ¿Podrías explicarlo ?</strong>

J.V. : El veganismo es la aplicación de la abolición de la esclavitud animal en nuestra propia vida. Cualquiera que afirme tomarse en serio los intereses de los animales o que afirme respetar a los animales ha de ser vegano. No es posible mantener una relación justa con los demás animales mientras les sigamos discriminando, sigamos considerando que somos sus dueños y les sigamos oprimiendo para nuestro beneficio. La explotación animal tiene su base en el consumo, en la demanda de víctimas a la que contribuimos con nuestras elecciones alimenticias, y, por tanto, el fin de la explotación animal pasa, necesariamente, por que todos y todas y cada uno y una de quienes consumimos animales dejemos de hacerlo.

<strong>D. : ¿Porqué utilizas la palabra ‘matados’ ?</strong>

J.V. : Del mismo modo que cuando un ser humano es víctima de una injusticia muriendo a manos de otros no decimos que ha sido sacrificado, sino que ha sido matado o asesinado, aplicamos lo mismo en el caso de los animales no humanos. El término sacrificar es un eufemismo que busca reducir la dimensión del daño causado y dar a entender que su muerte fue necesaria.
