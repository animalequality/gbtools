<div class="center"><img class="wp-image-11030" src="/app/uploads/2016/09/6893690035_30a5bdec93_o_-_copia.jpg" /></div>
<p>&iquest;Te indignan estas fotos? Pues he aqu&iacute; la realidad: <strong>los animales de granja </strong>son golpeados, maltratados y encerrados en sucias jaulas y <a href="https://igualdadanimal.org/noticia/2016/09/01/un-informe-revela-graves-y-permanentes-infracciones-en-mataderos-de-inglaterra/" target="_blank">la ley</a> poco o nada hace por evitarlo.</p>

<p>No, las autoridades <strong>no van a irrumpir en las granjas industriales para llevarse detenidos a los maltratadores</strong>. Mejor asumirlo de una vez por todas y <a href="https://igualdadanimal.org/noticia/2016/07/14/angela-23-anos-deje-de-comer-carne-cuando-vi-videos-de-mataderos/" target="_blank">pensar en otras maneras</a> de ayudar a estos maravillosos animales que viven en el infierno.</p>

<p>&nbsp;</p>

<FONT SIZE=4><FONT COLOR=#808080><p>&laquo;En nuestras manos est&aacute; reducir significativamente el maltrato animal.&raquo;</p></FONT>

<p>&nbsp;</p>

<p>Hay personas que dicen que no podemos hacer nada, que el maltrato es tanto, que nuestras acciones no cuentan; otras dicen que <a href="https://igualdadanimal.org/noticia/2016/07/07/perros-gatos-cerdos-vacas-pollos-todos-los-animales-quieren-vivir-y-no-ser-maltratados/" target="_blank">los animales de granja</a> no merecen <strong>el mismo respeto que nuestros perros y gatos</strong>, que ellos est&aacute;n para com&eacute;rnoslos.</p>

<p>Nosotros pensamos que, si podemos vivir vidas plenas y saludables aliment&aacute;ndonos sin da&ntilde;ar a los animales, <a href="https://igualdadanimal.org/noticia/2016/09/06/si-podemos-alimentarnos-sin-causar-dano-los-animales-por-que-no-hacerlo/" target="_blank">&iquest;por qu&eacute; no hacerlo?</a></p>

<p><img alt="" class="wp-image-11031" src="/app/uploads/2016/09/6893688499_6e4dc78ae6_o.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Porque, precisamente, esa es la mejor manera de ayudar a los animales de granja <strong>maltratados en granjas y mataderos</strong>: <a href="https://igualdadanimal.org/blog/6-razones-por-las-que-estar-contra-la-ganaderia-industrial-te-hace-marcar-la/" target="_blank">dar pasos</a> hacia una alimentaci&oacute;n a base de verduras, legumbres, cereales, frutos secos, frutas y <a href="https://igualdadanimal.org/noticia/2016/07/29/9-alternativas-la-carne-mas-saludables-y-llenas-de-proteinas/" target="_blank">alternativas a la carne</a> como tofu, tempeh, hamburguesas vegetales, etc.</p>

<p>En nuestras manos est&aacute; reducir significativamente el maltrato animal. Lo &uacute;nico que demanda de nosotros <strong>es echar en nuestra cesta de la compra unos productos en vez de otros</strong>.</p>

<p>&iexcl;&Aacute;nimo, <strong>no hay nada que no podamos conseguir</strong>! Adem&aacute;s puedes encontrar consejos para reducir tu consumo de carne <a href="http://www.igualdadanimal.org/noticias/7503/9-consejos-practicos-para-reducir-o-sustituir-la-carne-en-tu-alimentacion" target="_blank">aqu&iacute;</a>.</p>

<p><strong>Si quieres estar al tanto de historias como esta y muchas m&aacute;s, adem&aacute;s de informarte de c&oacute;mo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n</strong>.</p>

