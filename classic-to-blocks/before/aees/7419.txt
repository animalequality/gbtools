<p><img alt="" class="wp-image-10426" src="/app/uploads/2016/02/mayonesasinhuevo.jpg" style="float:left; margin-left:10px; margin-right:10px; width:350px" /></p>

<p>El gigante de la mayonesa Hellmann&rsquo;s, cuyos productos se comercializan en todo el mundo, anuncia que lanza una mayonesa sin huevo hecha completamente con productos vegetales.</p>

<p>La mayonesa sin huevo llegar&aacute; a los supermercados estadounidenses a mediados de este mes. <strong>Su lanzamiento afianza a&uacute;n m&aacute;s el imparable crecimiento de los productos hechos a base de vegetales</strong>. La carne, los huevos y la leche est&aacute;n dando paso a una nueva gama de productos alternativos que <a href="https://igualdadanimal.org/noticia/2015/10/15/los-sustitutos-de-la-carne-se-estan-generalizando-en-alemania/">cada vez son m&aacute;s demandados por los consumidores de todo el mundo</a>.</p>

<p>Cada vez surgen m&aacute;s empresas innovadoras que apuestan por <strong>un cambio de modelo alimenticio</strong> siguiendo criterios de sostenibilidad, salud y bienestar animal. Empresas como <a href="https://www.ju.st/"><em>Hampton Creek</em></a>, cuya mayonesa sin huevo Just Mayo es la pionera y predecesora de la nueva mayonesa sin huevo de Hellmann&rsquo;s.</p>

<p>Se da la curiosa circunstancia de que la empresa matriz de <em>Hellmann&rsquo;s</em>, la multinacional <em>Unilever</em>, demand&oacute; en 2014 a <em>Hampton Creek</em>. La acusaci&oacute;n de Unilever se basaba en decir que Hampton Creek enga&ntilde;aba a los consumidores al llamar &laquo;<em>mayonesa</em>&raquo; a un producto que no conten&iacute;a huevos. <strong>Tras un agria pol&eacute;mica y mucha publicidad negativa hacia la multinacional, <em>Unilever</em> acab&oacute; retirando la demanda</strong>.</p>

<p>Ahora parece que el gigante de la alimentaci&oacute;n ha pensado que si no puedes con tu enemigo lo mejor es unirte a &eacute;l.</p>

<p><img alt="" class="wp-image-14131" src="/app/uploads/2016/01/mitos3.jpg" style="float:right; margin-left:10px; margin-right:10px; width:300px" />Cuando se le pregunt&oacute; a la directora de marketing de Hellmann&rsquo;s si <strong>su nueva mayonesa sin huevo</strong> era la respuesta a la de Hampton Creek, respondi&oacute; que, &laquo;<em>Hellmann&rsquo;s se est&aacute; limitando a satisfacer los gustos de sus consumidores</em>&raquo;.</p>

<p>Lo cierto es que los gigantes de la alimentaci&oacute;n como Unilever <a href="https://igualdadanimal.org/noticia/2015/11/03/carrefour-lanza-una-linea-de-sustitutos-de-la-carne-en-francia/">est&aacute;n dando pasos hacia productos alternativos a la carne, leche y huevos</a> al ver el &eacute;xito de innovadoras empresas como Hampton Creek.</p>

<p>Los tiempos cambian, y los consumidores demandan productos cuyos est&aacute;ndares en materia de <strong>bienestar animal</strong> sigan una sencilla m&aacute;xima: &laquo;<em>si sabe delicioso y ning&uacute;n animal ha sufrido al producirlo, lo quiero en mi nevera</em>&raquo;.</p>

