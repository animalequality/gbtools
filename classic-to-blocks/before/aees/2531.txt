Venezuela - Con pancartas que exigían la libertad para los animales y reclamaban sus derechos, un grupo de Igualdad Animal protestó a las puertas del Parque Zoológico de Caricuao.

Andrés Lizardo, vocero de la ONG, aseguró que su intención es educar a la gente y hacerles entender que "los zoológicos no son divertidos".

Antes de manifestar pacíficamente los jóvenes recorrieron las instalaciones del zoológico para verificar las condiciones en las que viven los animales en ese lugar. Lizardo dijo que el espacio es grande para las personas pero pequeño para los animales, además en las jaulas no hay información para que el público aprenda sobre los animales. Criticó que muchos, que normalmente viven en grupos o manadas, están solos.

Con respecto a la alimentación dijo que desconocen si reciben los nutrientes necesarios y adecuados.

"Los zoológicos son cárceles", aseguró, lo mismo se leía en una de las pancartas.

[img_assist|nid=2537|title=|desc=|link=none|align=left|width=98|height=130]Lizardo dijo que tienen previsto solicitar reuniones con los directores de los zoológicos y planteó que luchan por "la libertad total para los animales". A su juicio no tiene sentido que los animales sean usados para alimentación, vestimenta o entretenimiento. Refirió que los animales "sienten su entorno y deben estar libres".

Consultado respecto a la necesidad de sitios para el esparcimiento y la recreación familiar, dijo que pueden existir parques pero sin jaulas para animales. Añadió que "una cosa es un refugio y otra un zoológico para distracción". Dijo que los animales que la Guardia Nacional decomise podrían ir a un refugio temporal, como transición mientras trabajan en la reinserción hacia su ambiente natural.

"Ni que esto fuera África":
La presencia del grupo no impidió que los asiduos visitantes al zoológico entraran, algunos recibieron los volantes que repartían y seguían hacia las instalaciones del zoológico. Mary Medina está de acuerdo con que los animales estén encerrados en el parque. "Esto es una ciudad, ni que estuviéramos en África" dijo y agregó que es una buena opción para los niños los fines de semana.

Jesús González también apoya la existencia del zoológico, considera que es una distracción para los niños, una manera de que conozcan los animales y aseguró que en Caricuao algunos están sueltos.

Desde hace más de ocho años Félida Contreras vende en las puertas del zoológico, para ella es una diversión para los niños y no está de acuerdo con que lo cierren. "Me consta que aquí cuidan bien a los animales, les dan su comida y mantienen el parque limpio, cuando se murió la elefanta todo el mundo lloró".

Rosalba Arenas coincidió en que el zoológico es una diversión para los niños.
