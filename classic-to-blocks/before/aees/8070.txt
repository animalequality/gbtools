Ante las declaraciones vertidas por el Sr. Tomás Fuertes, presidente del Grupo Fuertes, holding empresarial al que pertenece ElPozo Alimentación, en <a href="https://www.laverdad.es/murcia/tomas-fuertes-puede-20180325005858-ntvo.html" target="_blank" rel="noopener">una entrevista</a> concedida a un medio de comunicación y publicadas el domingo 25 de marzo, la Fundación Igualdad Animal se ve en la necesidad de emitir la siguiente comunicación:

La Fundación Igualdad Animal quiere dejar constancia de que en la actualidad se encuentra en conversaciones con ElPozo Alimentación, con el propósito de que dicha empresa implemente una serie de medidas de bienestar animal acordes a las tendencias en el sector, que mejoren las condiciones de vida de los animales a su cargo, respondiendo así a las crecientes demandas de los consumidores en este sentido.

Tras tener constancia de dicha entrevista y sorprendidos ante su desafortunado e inexacto contenido, la Fundación Igualdad Animal se puso en contacto telefónico con el Sr. José Ángel Cerón García, director de comunicación y relaciones externas de ElPozo Alimentación.

&nbsp;
<h4 style="text-align: center;">IGUALDAD ANIMAL Y 'SALVADOS'
<span style="color: #0000ff;"><a style="color: #0000ff;" href="/salvados-igualdad-animal/" target="_blank" rel="noopener">El reportaje que hizo temblar los cimientos de la industria cárnica española</a></span></h4>
&nbsp;

En dicho contacto teléfonico, lejos de mantener una actitud conciliadora ante las declaraciones del Sr. Fuertes en mitad de un proceso de conversaciones, el director de comunicación de ElPozo se limitó a indicar que «estaban sacadas de contexto», señalando como responsable al periodista autor de la entrevista.

Ante lo que consideramos una insultante falta de compromiso con las conversaciones ya iniciadas, avaladas por más de 132.000 firmantes, nos permitimos recordar al Sr. Tomás Fuertes que:

1º -  Ningún punto de nuestras peticiones incluye «<em>lavar los dientes a los cerdos todos los días después del desayuno</em>», como declara el Sr. Fuertes en dicha entrevista, no ya sólo en una falta de compromiso con el proceso de colaboración iniciado, sino en una falta de respeto hacia las 132.000 consumidores firmantes de nuestra petición y hacia el 84% de la ciudadanía española que cree que los animales deberían estar más protegidos en las granjas según el <a href="http://ec.europa.eu/commfrontoffice/publicopinion/index.cfm/ResultDoc/download/DocumentKy/71724">Eurobarómetro de 2015</a>.

2º - Lo que se mostró en el programa Salvados ni era un «lazareto» (área de una granja destinada a la recuperación de animales enfermos), ni era «un cerdo herniado». Esta afirmación resulta indignante y un insulto a la inteligencia de los consumidores. Si, como dice el Sr. Fuertes, todo lo que se mostró en Salvados era «completamente legal», ¿por qué la ElPozo Alimentación <a href="http://www.elpozo.com/noticias/elpozo-alimentacion-amplia-sus-estandares-de-calidad-para-incrementar-las-garantias-de-bienestar-animal/">cesó su relación comercial</a> con la granja?, y, lo que aún es más, ¿por qué ElPozo Alimentación tuvo a bien reunirse con la Fundación Igualdad Animal precisamente para que situaciones como las de esa granja nunca se repitan?

3º - Es completa y flagrantemente falso que, como afirma el Sr. Fuertes en la entrevista, «<em>Después de aquel reportaje, invitamos a tres personas del colectivo Igualdad Animal para que conocieran nuestras instalaciones; y que lo hicieran sin avisar del día y la hora. Cuando quisieran</em>». Muy al contrario, fue la Fundación Igualdad Animal la que se puso en contacto con ElPozo Alimentación, proponiendo una reunión con objeto de iniciar conversaciones. En ningún momento, ni por parte de ElPozo, ni por parte de Igualdad Animal, se habló de tal cosa como conocer sus instalaciones sin avisar del día y la hora. Nunca se habló de visitar granjas o su matadero. Siempre se habló de una reunión para iniciar conversaciones. ElPozo, de hecho, estableció en primer lugar esa reunión para el 10 de febrero para posteriormente retrasarla al 26 de febrero, fecha en la que tuvo lugar la reunión.

Para finalizar, queremos confirmar que desde la Fundación Igualdad Animal mantenemos el compromiso de seguir reuniéndonos con ElPozo Alimentación para la implementación de las medidas de bienestar animal ya presentadas a responsables de la empresa.

Si, como afirmaba el Sr. Fuertes en sus declaraciones, «Somos la empresa más representativa del sector», animamos a ElPozo Alimentación a demostrarlo <a href="https://www.elsecretodeelpozo.com/">implementando el paquete de medidas</a> que son, precisamente, las que los líderes del sector ya están poniendo en práctica a nivel internacional.
<p style="padding-left: 150px;"><iframe src="https://www.youtube.com/embed/frAqesSRoBk" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
