<div class="center"><img class="wp-image-11963" src="/app/uploads/2017/04/unnamed_0.jpg" /></div>
<p>En diciembre de 2016 la organizaci&oacute;n internacional Igualdad Animal grab&oacute; a un trabajador de una granja lechera en Somerset golpeando y pateando terneros, incluso lanzando a los reci&eacute;n nacidos contra el suelo.<br />
<br />
Hoy <strong>ha sido declarado culpable. Se le ha condenado a 12 semanas en prisi&oacute;n y 2 a&ntilde;os de inhabilitaci&oacute;n</strong> por sus terribles actos de crueldad, despu&eacute;s de admitir los cargos contra &eacute;l. [1]</p>

<p>&nbsp;</p>

<p><font size="5"><font color="#808080">&laquo;Este es el peor caso de maltrato animal que he visto en mis 23 a&ntilde;os de carrera y puedo decir que he visto algunos casos horribles. Por ello este Juzgado opina que esta crueldad excede el umbral de la condicional.&raquo; Indic&oacute; el juez&nbsp;Jeff Collingwood.</font></font></p>

<p><font size="5">&nbsp;</font></p>

<p><br />
<font size="5"><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/EHfDmCTaa1I" width="854"></iframe><br />
La investigaci&oacute;n, resultado del aviso de un vecino de la zona, ha revelado m&uacute;ltiples incidencias de violencia en tan solo un d&iacute;a, incluyendo:</font></p>

<blockquote>- Repetidas patadas a los terneros para ponerlos de pie.</blockquote>

<blockquote>- Retorcerle de forma agresiva el rabo a las vacas y golpearlas con las puertas de metal.</blockquote>

<blockquote>- Lanzar a los terneros contra el suelo violentamente mientras gritan obscenidades. - Patadas y golpes continuados a las vacas lecheras.</blockquote>

<p>&gt;&gt;&gt; <a href="https://www.youtube.com/watch?v=EHfDmCTaa1I&amp;feature=youtu.be" target="_blank">Material grabado en la granja West Country</a></p>

<p>&gt;&gt;&gt; <a href="https://www.flickr.com/photos/animalequalityuk/sets/72157677978488223/" target="_blank">Im&aacute;genes de la granja lechera</a></p>

<p>&nbsp;<a href="https://www.flickr.com/photos/animalequalityuk/sets/72157677978488223/" target="_blank"><img alt="" class="wp-image-11964" src="/app/uploads/2017/04/unnamed_1.jpg" style="margin: 0px 10px; float: left; width: 450px; " /></a>&laquo;Nos complace anunciar que este trabajador de granja ha sido declarado culpable por sus terribles ataques a vacas y terneros inocentes, tal como se ha revelado en nuestra investigaci&oacute;n. Agradecemos a la Sociedad Protectora de Animales en Reino Unido (RSPCA) la rapidez con la que han procesado este caso. Nuestra &uacute;nica frustraci&oacute;n ahora es que la ley a&uacute;n no admite condenas m&aacute;s severas en casos de abuso animal, y al final lo m&aacute;s probable es que no ingrese en prisi&oacute;n&raquo; declara Toni Shephard, directora de Igualdad Animal en Reino Unido.</p>

<p><br />
Y a&ntilde;ade &laquo;Este caso no hace m&aacute;s que evidenciar la peligrosa falta de supervisi&oacute;n en la industria ganadera y la completa ausencia de inspecciones sin previo aviso. Sin nuestra investigaci&oacute;n, este trabajador seguir&iacute;a hoy golpeando a esos pobres animales.&raquo;<br />
&nbsp;</p>

<p><font size="1"><font color="#808080">Granjero pateando a un ternero / Igualdad Animal</font></font></p>

<p>Esta es la segunda ocasi&oacute;n en que trabajadores de granjas en Inglaterra han sido condenados por actos de crueldad y maltrato gracias a las investigaciones de Igualdad Animal. En 2012 dos trabajadores fueron grabados golpeando lechones con una barra de metal hasta la muerte en la granja Harling Farm, en Norfolk. Ambos fueron declarados culpables del maltrato y uno de ellos fue enviado a prisi&oacute;n por 18 semanas. En septiembre de 2016 tres trabajadores en Espa&ntilde;a fueron condenados por golpear con barras de metal y apu&ntilde;alar cerdos de su granja como resultado del material en v&iacute;deo difundido por Igualdad Animal.</p>

<p><br />
[1]&nbsp;<a href="http://www.dailymail.co.uk/news/article-4447070/Thug-19-STAMPED-newborn-calf-spared-jailed.html" target="_blank">http://www.dailymail.co.uk/news/article-4447070/Thug-19-STAMPED-newborn-calf-spared-jailed.html</a></p>

