 
A dos meses de las inundaciones, los ejidatarios denuncian que no tienen pastura con qué alimentar las reses 
 

Municipio Cárdenas (México) - Por lo menos 25 poblados, rancherías, colonias y ejidos del municipio de Cárdenas se unideron al frente común de 40 comunidades de Huimanguillo que exigen apoyos para impedir que los estragos de la pasada inundación de noviembre continúe causando estragos en la ganadería, informó José Candelero Pérez, miembro de la asociación civil “Por un esfuerzo social por los pobres”

Entrevistado en la casa ejidal de La Ceiba primera sección, el activista afirmó que el "ganado" sigue muriendo por falta de pastura en algunos poblados del Plan Chontalpa y de Cárdenas, sin que hasta el momento alguna instancia gubernamental se haga responsable de bajar recursos, pese a que los dos municipios fueron declarados zona de desastre.

Junto con los representantes de las comunidades de este municipio, donde además de la pastura, se han muerto decenas de reses por el frío, se preguntaron durante una asamblea celebrada recientemente, donde se trató el destino final de los recursos del Fondo Nacional de Desastres Naturales (Fonden), que aún no se entregan a los afectados.

Candelero Pérez sostuvo que en Cárdenas han sufrido la misma situación que en este municipio, y consideró que el sector campesino y ganadero necesita con urgencia de soluciones integrales, y no de “calmantes” como el empleo temporal donde le pagan a cada persona 500 pesos, pero además no son regalados, sino que tienen que trabajar durante una semana.

Los representantes de las 75 comunidades de los dos municipio, que han decidido no quedarse de brazos cruzados, advirtieron que está en puerta una gran movilización y bloqueos si en breve no reciben lo más urgente, que es el alimento para el "ganado" que sufre desnutrición, y medicamentos, pues las pérdidas de animales podrían aumentar  en ambos municipios
