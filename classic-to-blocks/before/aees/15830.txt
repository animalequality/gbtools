La cadena de hoteles Marriott, la segunda más grande del mundo, ha anunciado <a href="http://serve360.marriott.com/wp-content/uploads/2018/12/Marriott_International_Cage-Free_Egg_Statement.pdf" target="_blank" rel="noopener">su compromiso</a> para eliminar las jaulas de su suministro de huevo a nivel global para el año 2025.

Con este compromiso las dos cadenas de hoteles más grandes del mundo, Wyndham y Marriott, se suman a la tendencia por terminar con el sistema de jaulas que tanto maltrato causa a las gallinas. Gracias a estos compromisos serán cientos de miles de gallinas las que ya no tendrán que vivir hacinadas en jaulas.

El acuerdo  se logró gracias a una campaña global por parte de la Open Wing Alliance (OWA), coalición internacional de organizaciones que trabajan para terminar con el maltrato animal, incluida Igualdad Animal, específicamente con el uso de las jaulas para las gallinas que es  uno de los sistemas más crueles.

En tan solo dos días las organizaciones que conforman la OWA realizaron protestas en ciudades como Berlín, Nueva York, Ciudad de México, entre otras, y voluntarios de todo el mundo realizaron acciones en línea pidiéndole a Marriott que hiciera su compromiso, logrando así el compromiso global.

Cada vez son más las empresas que están mostrando interés en no ser partícipes del maltrato animal que implican las jaulas y escuchan a sus clientes que les piden políticas a favor de los animales. La tendencia por eliminar este sistema crece año cada año y con cada nuevo compromiso nos queda claro que es cuestión de tiempo para que más empresas anuncien su compromiso y sean más las gallinas que ya no vivan enjauladas.
