<p>
	EFE - Rusia inaugur&oacute; hoy en su capital un monumento a los perros con los que se realizaron experimentos e investigaciones m&eacute;dicas especiales que permitieron el primer vuelo del hombre al espacio, haza&ntilde;a protagonizada por el cosmonauta sovi&eacute;tico Yuri Gagarin hace 50 a&ntilde;os.</p>
<p>
	<br />
	&quot;Para que pudiera tener lugar el vuelo del hombre al espacio se llevaron a cabo numerosos experimentos, en los que participaron un total de 42 perros&quot;, indic&oacute; a la agencia Interfax &Iacute;gor Bujtiy&aacute;rov, jefe del Instituto de Medicina Militar del Ministerio de Defensa de Rusia.<br />
	<br />
	<strong> <img alt="" class="wp-image-13576" src="/app/uploads/2011/04/000estatualaika.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 296px; " /> </strong></p>
<hr />
<p>
	<strong><iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FIgualdadAnimal&amp;width=400&amp;colorscheme=light&amp;show_faces=false&amp;stream=false&amp;header=false&amp;height=62" style="border: medium none; overflow: hidden; width: 400px; height: 62px;"></iframe></strong></p>
<hr />
<br />
<br />
<p>
	Bujtiy&aacute;rov, quien rechaz&oacute; responder a la pregunta de cu&aacute;ntos canes murieron en los experimentos, subray&oacute; que &quot;el perro, que siempre fue amigo del m&eacute;dico, fue sometido a ensayos muy complicados y muy importantes para la medicina.&quot;<br />
	<br />
	Por eso, agreg&oacute;, se ha inaugurado hoy un monumento a uno de ellos frente al edificio del Instituto de Medicina Militar.<br />
	<br />
	&quot;Los perros soportan <i>bien</i> las sobrecargas, la altura, las vibraciones, los mareos y dem&aacute;s factores&quot;, se&ntilde;al&oacute; Bujtiy&aacute;rov.<br />
	<br />
	<a href="https://www.facebook.com/album.php?aid=353478&amp;id=116189118344" target="_blank"><img alt="Fotos de los 36 beagles rescatados por simpatizantes de Igualdad Animal de un centro de cría para vivisección" class="wp-image-13815" src="/app/uploads/2011/04/beagles-rescatados.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: right; width: 200px; " /></a>Lo m&aacute;s dif&iacute;cil era ense&ntilde;arles a permanecer en la c&aacute;psula. En eso se invert&iacute;a gran parte del tiempo, a veces entre tres y cinco meses, y a&uacute;n as&iacute;, algunos perros no pod&iacute;an adaptarse a ese estado de aislamiento e inmovilidad y se les retiraba de los experimentos, precis&oacute;.<br />
	<br />
	Las perras Belka y Strelka fueron en 1960 los primeros <a href="http://www.igualdadanimal.org/antiespecismo">ejemplares de su especie</a> en realizar con &eacute;xito un vuelo orbital que les llev&oacute; de vuelta a la Tierra sanos y salvos.<br />
	<br />
	En 1983, la Uni&oacute;n Sovi&eacute;tica envi&oacute; al cosmos a los primeros monos, Abrek y Bion, y a los dos a&ntilde;os a otra pareja de simios, Verni y Gordi, despu&eacute;s a Drema y Yerosha (1987), a Zhakonia y Zabiyaka (1989), a Krosh e Ivasha (1992) y a Lapik y Multik (1996).<br />
	<br />
	El a&ntilde;o pasado se anunci&oacute; que los cient&iacute;ficos rusos renunciaban a enviar m&aacute;s perros y monos al espacio para llevar a cabo experimentos.<br />
	<br />
	&quot;Desde el punto de vista de las estad&iacute;sticas y de la realizaci&oacute;n de investigaciones, es mejor trabajar con animales peque&ntilde;os que con grandes&quot;, indic&oacute; entonces Yevgueni Ilin, investigador jefe del Instituto de Problemas Biom&eacute;dicos de la Academia de Ciencias de Rusia.<br />
	<br />
	Por lo tanto, agreg&oacute;, &quot;al menos en los pr&oacute;ximos diez a&ntilde;os, si no m&aacute;s, no volver&aacute; a haber monos, ni una Belka ni una Strelka en el espacio&quot;.<br />
	<br />
	&quot;Nuestra tarea principal es estudiar los cambios a nivel celular, molecular y gen&eacute;tico. Para ello son <i>necesarios</i> animales de l&iacute;neas consangu&iacute;neas, preferiblemente ratones, con los que experimenta todo el mundo cient&iacute;fico y para los que ya se han elaborado m&eacute;todos de investigaci&oacute;n&quot;, precis&oacute;.<br />
	<br />
	Agreg&oacute; que los animales m&aacute;s grandes, para los que adem&aacute;s se necesita un mayor espacio, sirven para hacer investigaciones fisiol&oacute;gicas, las mismas que se pueden llevar a cabo con los cosmonautas de la Estaci&oacute;n Espacial Internacional. EFE (Fuente: ABC)<br />
	&nbsp;</p>
<hr />
<br />
<p>
	<a href="https://es.wikipedia.org/wiki/Animales_en_el_espacio" target="_blank"><img alt="Animales en el Espacio" class="wp-image-13644" src="/app/uploads/2011/04/300px-Baker.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 200px; " /></a><br />
	<br />
	<a href="https://es.wikipedia.org/wiki/Animales_en_el_espacio">Todo sobre las v&iacute;ctimas: Animales en el Espacio.</a> Los experimentos con animales en el espacio sirvieron originalmente para probar la supervivencia en los vuelos espaciales antes de intentar misiones espaciales tripuladas. En numerosas ocasiones los animales tripularon veh&iacute;culos espaciales para investigar diferentes procesos biol&oacute;gicos y los posibles efectos que la microgravedad pudiera tener. A fecha de 2004, s&oacute;lo seis pa&iacute;ses han llevado animales al espacio: los Estados Unidos, la Uni&oacute;n Sovi&eacute;tica, Francia, Jap&oacute;n, China y Argentina. <a href="https://es.wikipedia.org/wiki/Animales_en_el_espacio">(Contin&uacute;a)</a><br />
	<br />
	Laika, muerte en el espacio.<br />
	<strong> <a href="https://es.wikipedia.org/wiki/Laika" target="_blank"><img alt="Laika, una muerte en el espacio" class="wp-image-13745" src="/app/uploads/2011/04/38393381_laika300.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 200px; " /></a><br />
	</strong></p>

