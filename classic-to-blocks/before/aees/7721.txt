<div class="center"><img class="wp-image-11781" src="/app/uploads/2017/03/shutterstock_472100659.jpg" /></div>
<p>Un estudio llevado a cabo por investigadores de la Universidad de Edimburgo ha investigado la eficiencia en las distintas etapas de la producci&oacute;n de alimentos. Lo que descubrieron podr&iacute;a indignar a m&aacute;s de uno.</p>

<p>Usando datos de la Organizaci&oacute;n de las Naciones Unidas para la Alimentaci&oacute;n y la Agricultura (FAO), concluyen que en <strong><a href="https://www.elmundo.es/cultura/2017/02/20/58aab4a222601d9c2f8b4642.html" target="_blank">la industria ganadera</a> se desperdicia hasta un 94% en la producci&oacute;n de comestibles.</strong></p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>En dicho porcentaje desperdiciado se analiza tanto la producci&oacute;n del alimento de los animales (grano y pastos frescos), como la de los alimentos ya procesados y listos para consumir por los humanos.</p>

<p>Para cuando <a href="https://igualdadanimal.org/noticia/2016/10/14/la-ganaderia-industrial-es-un-problema-global-que-nos-afecta-todos/" target="_blank">los productos de la ganader&iacute;a</a> (carne, huevos y l&aacute;cteos principalmente) llegan hasta los consumidores, <strong>160 millones de toneladas se desperdician sin ser consumidas</strong>. El segundo motivo de desperdicio es el sobreconsumo de dichos productos.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;El mayor porcentaje de desperdicio en la producci&oacute;n de alimentos se da en la ganader&iacute;a.&raquo;</p></FONT>

<p>&nbsp;</p>

<p>&laquo;Los resultados ponen de manifiesto el sustancial desperdicio en la producci&oacute;n de alimentos provenientes de la ganader&iacute;a. Por otro lado revela la magnitud de desperdicio por el exceso de consumo de estos alimentos, que sobrepasa los requerimientos nutricionales&raquo;, escriben los investigadores en el estudio publicado en la revista Agricultural Systems (Sistemas de agricultura).</p>

<p><img alt="" class="wp-image-11782" src="/app/uploads/2017/03/shutterstock_519758755.jpg" style="margin: 10px; float: left; width: 450px; " /></p>

<p>&laquo;El mayor porcentaje de desperdicio en la producci&oacute;n de alimentos se da en la ganader&iacute;a. Reduciendo el consumo de carne, huevos y l&aacute;cteos, los consumidores podr&iacute;an incrementar la eficiencia del sistema alimentario y reducir el impacto medioambiental&raquo;, a&ntilde;aden, haciendo notar que lamentablemente el consumo de carne y leche sigue en aumento.</p>

<p>Los autores declaran que resulta significativo que el desperdicio por sobreconsumo sea equivalente en magnitud al desperdicio de alimentos por parte de los consumidores.</p>

<p>&laquo;Cambios para influenciar en los h&aacute;bitos de los consumidores, como por ejemplo reducir el consumo de productos provenientes de los animales, reducir el desperdicio de alimentos y reducir el sobreconsumo, ayudar&aacute;n a alimentar a una poblaci&oacute;n en continuo aumento de una manera sostenible.&raquo;</p>

<p>&nbsp;</p>

<p><br />
Fuente: http://www.foodnavigator.com/Market-Trends/Consumer-waste-and-livestock-biggest-inefficiencies-in-food-production-Study</p>

