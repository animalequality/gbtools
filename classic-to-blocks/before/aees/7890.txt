<div class="center"><img class="wp-image-12392" src="/app/uploads/2017/09/shutterstock_226901644_1.jpg" /></div>
<p>A muchos de nosotros <strong>nos importan los animales y ya hemos comenzado a reducir nuestro consumo de carne </strong>para que vacas, pollos, cerdos, corderos y terneros queden fuera de nuestros platos.</p>

<p>Pero debido a que nuestros hogares pertenecen a medios tan diferentes <strong>conocemos muy poco sobre los peces</strong> y sobre el sufrimiento al cual se les somete para ser convertidos en comida.</p>

<p><img alt="" class="wp-image-13037" src="/app/uploads/2018/04/shutterstock_497414386_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>Los peces <strong>son animales tan sensibles como cualquier ave o mam&iacute;fero</strong>, inteligentes y que forman complejas sociedades. Su capacidad de sentir dolor es igual y mayor que la de ellos. Su deseo de vivir tambi&eacute;n.</p>

<p>La pesca industrial avanza en sus enormes barcos y <strong>mata cada a&ntilde;o a m&aacute;s peces que toda la industria ganadera junta</strong>. Tambi&eacute;n los somete a la mayor crueldad y maltrato imaginables porque sencillamente <strong>no existe ley alguna que regule el bienestar de los peces </strong>y lo impida.</p>

<p>A los peces se les revientan sus &oacute;rganos internos cuando son sacados del mar. Muchos <strong>mueren asfixiados y aplastados</strong> dentro de las gigantescas redes y en la piscifactor&iacute;as (granjas de peces) los m&eacute;todos de matanza son crueles: electrocuci&oacute;n, asfixia o golpes.</p>

<p>&nbsp;</p>

<p>La mayor&iacute;a de las personas que consumen peces desconocen que las piscifactor&iacute;as existen y que en ellas son criados cada a&ntilde;o <strong>120.000 millones de peces</strong>. Y tambi&eacute;n ignoran que anualmente <strong>la pesca industrial captura globalmente a un n&uacute;mero de peces equivalente a la poblaci&oacute;n humana de 142 planetas Tierra.</strong></p>

<p><img alt="" class="wp-image-12394" src="/app/uploads/2017/09/shutterstock_630563330.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>La soluci&oacute;n a toda esta crueldad y violencia depende enteramente de cada uno de nosotros. Dejar de comer peces ser&iacute;a algo poco o para nada complicado si consideramos que para los peces esa decisi&oacute;n significa, nada m&aacute;s y nada menos que, la posibilidad de vivir.</p>

<p>Y es que aunque no podemos escuchar sus gritos, si pudi&eacute;ramos seguramente entender&iacute;amos que dicen: por favor, &iexcl;s&aacute;came de t&uacute; plato!</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><font size="1"><font color="#808080">Piscifactor&iacute;a o granja de peces en el mar.</font></font></p>

<p><font size="1">&nbsp;</font></p>

