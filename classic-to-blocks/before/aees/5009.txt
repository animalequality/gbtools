
La policía trató de impedir la acción simbólica en defensa de los derechos animales de Igualdad Animal.

<iframe align="center" src="https://www.flickr.com/slideShow/index.gne?set_id=72157622362382003" width="500" height="500" frameBorder="0" scrolling="no"></iframe>

Galería fotográfica de la acción:
<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157622362382003/">http://www.flickr.com/photos/igualdadanimal/sets/72157622362382003/</a>

Esta mañana varios activistas de Igualdad Animal se han desplazado al Congreso de los Diputados para colocar una placa conmemorativa acompañada de una corona de flores en memoria de Moscatel —el toro asesinado en Tordesillas el 15 de Septiembre de este año y de cuya muerte fuimos testigos— así como del resto de víctimas de tales festejos. La placa contenía el texto "Moscatel. Febrero 2005, Septiembre 2009. En recuerdo de todas las víctimas de Tordesillas. Igualdad Animal".

La policía ha tratado de impedir esta acción de protesta ante el Congreso, viéndonos obligados a llevar nuestra reivindicación a las inmediaciones donde finalmente hemos podido poner el ramo y la placa conmemorativa.

La protesta fue convocada con motivo de la votación de la Propuesta No de Ley que tendrá lugar esta tarde en el Congreso de los Diputados. Durante la sesión de esta tarde se debatirá sobre el futuro de festejos como el Toro de La Vega de Tordesillas.

Recientemente se produjo otra votación en el Senado después de que un senador mostrase la portada de El País con una captura de el video tomado por Igualdad Animal durante la matanza de Tordesillas para mostrar el sufrimiento de estos animales en dicho espectáculo y argumentar en contra de su consideración como festejo de "Interés Turístico Nacional". El resultado de la votación fue mayoritariamente en contra de la propuesta debido a las votos del PP y el PSOE.

Desde Igualdad Animal tenemos una postura clara con respecto al Toro de la Vega y otras eventos de explotación animal: Exigimos la abolición de las mismos y el cese del uso de animales en dichos espectáculos. Los demás animales —recordemos que los humanos también somos animales— no son objetos a nuestra disposición sino individuos con intereses propios que deben ser respetados independientemente de la especie a la que pertenezcan.

Igualdad Animal grabó las imágenes del asesinato de Moscatel el 15 de Septiembre en Tordesillas, imágenes únicas e impactantes de cómo Moscatel era alanceado y acuchillado hasta la muerte, que los principales medios de comunicación recogieron y que han ayudado a crear el debate existente sobre el Toro de La Vega.

Vídeo de la matanza de Moscatel:
<a href="https://vimeo.com/6595048">https://vimeo.com/6595048</a>

Fotografías de la matanza de Moscatel:
<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157622252725939/">http://www.flickr.com/photos/igualdadanimal/sets/72157622252725939/</a>
