<div class="center"><img class="wp-image-9923" src="/app/uploads/2013/02/letonia_granjas.jpg" /></div>
<p align="justify">Una nueva investigaci&oacute;n encubierta realizada por la organizaci&oacute;n <a href="https://www.dzivniekubriviba.lv/" target="_blank">Dzīvnieku Brīvība </a>(Libertad Animal) durante el oto&ntilde;o de 2012 acaba de ser publicada y muestra el interior de seis granjas peleteras en Letonia.</p>
<p align="justify">En todas las granjas visitadas, los activistas documentaron la presencia de animales <strong>animales enfermos, heridos y muertos </strong>dentro de diminutas jaulas, donde desarrollan estr&eacute;s cr&oacute;nico debido a la la falta de est&iacute;mulos y la incapacidad para expresar su comportamiento natural, adem&aacute;s de padecer todo tipo de lesiones f&iacute;sicas.<br />
	<br />
	<strong>V&iacute;deo de la investigaci&oacute;n:</strong></p>
<p><iframe allowfullscreen="" frameborder="0" height="326" src="https://www.youtube.com/embed/ofCKeKOklBQ" width="580"></iframe></p>
<p align="justify">&nbsp;</p>
<p align="justify"><em>&laquo;Es horrible. Las im&aacute;genes me provocan un profundo disgusto y un shock general. De acuerdo con este material, es evidente que los animales se mantienen en condiciones inaceptables. Por otra parte, hay muchos <strong>animales que sufren de enfermedades cr&oacute;nicas</strong>, posiblemente plagas carn&iacute;voras. As&iacute; que parece que no hay <strong>nadie all&iacute; que brinde atenci&oacute;n m&eacute;dica</strong> regular ni atenci&oacute;n de ning&uacute;n tipo. Entonces queda claro por qu&eacute; hay tantos cad&aacute;veres en las jaulas de la granja&raquo;</em>, comenta el veterinario Normunds Fedorovs tras visualizar el material de archivo.</p>

