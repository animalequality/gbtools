<div class="center"><img class="wp-image-9459" src="/app/uploads/2012/04/ternero_exportado.jpg" /></div>
<p>El puerto de Tarragona iniciar&aacute; en las pr&oacute;ximas semanas las <strong>exportaciones de ganado vivo</strong> a pa&iacute;ses como <strong>Marruecos, Argelia, L&iacute;bano, Libia, Egipto y Turqu&iacute;a</strong> una vez reciba la aprobaci&oacute;n del Ministerio de Agricultura, Alimentaci&oacute;n y Medio Ambiente. Este proyecto se plantea como una forma de potenciar a las explotaciones ganaderas catalanas, que podr&aacute;n rentabilizar sus &ldquo;excedentes de producci&oacute;n&rdquo; enviando a los animales sobrantes a otros pa&iacute;ses.<br />
	<br />
	El Puerto de Tarragona se convierte con este&nbsp; plan en el segundo puerto espa&ntilde;ol clasificado para exportar ganado, compitiendo directamente con el Puerto de Cartagena (Murcia), &uacute;nico hasta la fecha. <strong>El puerto de Cartagena es responsable actualmente del env&iacute;o del 10% del ganado criado en territorio espa&ntilde;ol</strong>, consiguiendo con la exportaci&oacute;n mantener los precios de la carne tras la ca&iacute;da en las ventas.<br />
	<br />
	<strong>Durante 2011 se exportaron unos 33.000 animales</strong> y las expectativas de exportaci&oacute;n del puerto de Cartagena para 2012 superan los 80.000 individuos.</p>

