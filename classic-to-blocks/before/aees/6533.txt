<div class="center"><img class="wp-image-9547" src="/app/uploads/2012/05/tennessee_horses.jpg" /></div>
<p>Un v&iacute;deo encubierto obtenido por la Human Society y presentado p&uacute;blicamente esta semana, muestra c&oacute;mo los caballos utilizados en el espect&aacute;culo ecuestre &ldquo;Walking Horse&rdquo; son sometidos a <strong>brutales palizas</strong>, siendo golpeados con palos en la cabeza y extremidades y torturados con picanas.</p>
<p>Adem&aacute;s, la investigaci&oacute;n revel&oacute; que <strong>los entrenadores aplican productos qu&iacute;micos c&aacute;usticos en los pies de los caballos</strong>, una pr&aacute;ctica conocida como &quot;Soring&quot;, y que fue prohibida hace 40 a&ntilde;os por la Ley de protecci&oacute;n del caballo,&nbsp;<strong> con el fin de que levanten m&aacute;s el paso al andar</strong>. De este modo, los caballos obtienen una mayor puntuaci&oacute;n en la demostraci&oacute;n.</p>
<p><br />
	La publicaci&oacute;n de las im&aacute;genes ha dado lugar una acusaci&oacute;n federal hacia cuatro personas por&nbsp; violaci&oacute;n de la Ley de Protecci&oacute;n del Caballo.</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/gxVlxT_x-f0" width="560"></iframe></p>

