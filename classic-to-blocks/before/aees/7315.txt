<p><img alt="" class="wp-image-10316" src="/app/uploads/2015/10/mataderosfrancia.jpg" style="float:right; margin:10px; width:450px" /></p>

<p>El pasado mi&eacute;rcoles 14 de octubre, la organizaci&oacute;n francesa <a href="https://www.l214.com/enquetes/2015/abattoir-made-in-france/">L214 present&oacute; un escalofriante v&iacute;deo</a> grabado en un matadero del sur de Francia. En el v&iacute;deo se ve c&oacute;mo los animales recuperan la consciencia mientras son descuartizados o c&oacute;mo se los abandona en el suelo desangr&aacute;ndose. En el matadero se mataban 20.000 cerdos, 40.000 ovejas, 3.000 caballos y 6.000 vacas a lo largo del a&ntilde;o.</p>

<p>El v&iacute;deo ha causado una gran pol&eacute;mica en Francia que ha llevado a las autoridades a clausurar el matadero. En tan s&oacute;lo dos d&iacute;as, m&aacute;s de 800.000 personas han visto el reportaje y m&aacute;s de 100.000 personas han firmado la petici&oacute;n.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/LzvsS17ptzE" width="885px"></iframe></div>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

