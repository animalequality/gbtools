Obtener productos de origen animal provoca un gran sufrimiento. Y como vivimos en una cultura alimentaria donde estos productos son omnipresentes, la ganadería industrial nos miente constantemente para ocultar la terrible realidad que encierran los muros de las granjas y mataderos.

Publicidad engañosa que muestra a animales en libertad viviendo vidas idílicas, estrategias para contrarrestar cualquier información negativa sobre el consumo de carne aún cuando esta sea veraz; la industria ganadera nos engaña, todos los días.

Es por eso que nuestros investigadores se infiltran en lo más profundo de la industria, allí donde está todo el horror que tanto se esfuerza por ocultar. La luz que arrojan sobre el infierno que viven los animales permite que más personas reduzcan o sustituyan las proteínas animales. Con esto estamos cambiando la vida de los animales y también al mundo ya que la forma en que se producen los alimentos afecta directamente nuestra salud y el medioambiente.

A continuación te presentamos 4 imágenes que jamás habrían podido ver la luz si no fuera por nuestros valientes investigadores. Todas ellas le han dado la vuelta al mundo y que siguen salvando vidas.
<h5><strong>1. La industria láctea mexicana</strong></h5>
En la industria láctea las vacas son «descornadas». El proceso es rutinario y se realiza sin anestesia a pesar de que los cuernos de una vaca tienen una sensibilidad simi­lar a la de nuestros dientes.

<img class="wp-image-12889" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/02/33382539486_baf3a8339b_z.jpg" alt="" />

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;
<h5><strong>2. Escándalo en granja que provee al Pozo</strong></h5>
En colaboración con el programa «Salvados» de La Sexta, hicimos una investigación en una granja proveedora de la empresa cárnica El Pozo y descubrimos que los animales viven en terribles condiciones insalubres y contrarias a la ley. Actualmente estamos exigiendo a El Pozo que adopte una política de bienestar animal acorde con las necesidades de los animales. <a href="https://elsecretodeelpozo.com/" target="_blank" rel="noopener">Puedes firmar la petición aquí</a>.

<img class="wp-image-12849" style="margin-left: 10px; margin-right: 10px; float: left; width: 451px; " src="/app/uploads/2018/02/26114212948_8d82d68503_z.jpg" alt="" />

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales?</h4>
<h4 style="text-align: center;"><a href="http://eepurl.com/dhTk1v" target="_blank" rel="noopener">¡Suscríbete gratuitamente a nuestro e-boletín!</a></h4>
<h4 style="text-align: center;"></h4>
<h5><strong>3. La industria de la carne de pollo en Italia</strong></h5>
Las grandes marcas ocultan un verdadero infierno detrás de sus anuncios publicitarios. En todas las granjas y mataderos de los mayores productores de carne de pollo investigados el sufrimiento es la norma y no la excepción.

<img class="wp-image-12891" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/02/37280892106_30056cfd67_z.jpg" alt="" />

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;
<h5><strong>4. Granjas de foie gras  </strong></h5>
En 2012 revelamos impactantes imágenes que mostraron al mundo la crueldad y el maltrato brutal en granjas proveedoras de Euralis, el mayor productor de foie gras en el mundo. Para obtener este producto, los patos y ocas son alimentados a la fuerza varias veces al día, introduciéndoles un tubo metálico de hasta 30 cm por la boca que llega directamente a su estómago y que le produce heridas en el esófago y órganos internos. Esto provoca que su hígado enferme adquiriendo un tamaño 10 veces mayor mientras sufren terriblemente encerrados en diminutas jaulas.

<img class="wp-image-12892" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/02/6823070928_9c7c0bcfe0_z.jpg" alt="" />
