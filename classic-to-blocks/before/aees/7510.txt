<p><img alt="" class="wp-image-10795" src="/app/uploads/2016/06/fototordesillas_0.jpg" style="margin:10px; width:885px" /></p>

<p>La Delegaci&oacute;n Territorial de la Junta de Castilla y Le&oacute;n en Valladolid <strong>acaba de denegar el permiso para celebrar el Toro de la Vega</strong> al Ayuntamiento de Tordesillas.</p>

<p>Se cumple as&iacute; el <a href="https://igualdadanimal.org/noticia/2016/05/19/estocada-mortal-la-crueldad-del-toro-de-la-vega/">decreto ley que impide la muerte del toro</a>. El pasado 20 de junio, el Ayuntamiento del tristemente conocido pueblo de Tordesillas presentaba una solicitud en la Junta para celebrar el torneo. Dicha solicitud estaba firmada por el alcalde del pueblo, Jos&eacute; Antonio Gonz&aacute;lez Poncela (PSOE).</p>

<p>As&iacute;, la Junta <strong>deniega en resoluci&oacute;n</strong> la autorizaci&oacute;n para celebrar el brutal evento <strong>por no ajustarse al decreto ley</strong>. El escrito presentado no hab&iacute;a adaptado las bases reguladoras a la nueva normativa.</p>

