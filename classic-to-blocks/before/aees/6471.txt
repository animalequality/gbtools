<div class="center"><img class="wp-image-9480" src="/app/uploads/2012/04/valla_italia_viviseccion.jpg" /></div>
Una asociación de empresas de investigación biomédica ha lanzado en Italia una costosa <strong>campaña publicitaria internacional en televisión, vallas publicitarias, radio e internet a favor de la vivisección</strong>. La imagen utilizada no podía ser más tendenciosa y demagógica: junto a la frase «<em>Un día te podría salvar la vida</em>», aparecen una rata y una niña.

<strong>El anuncio es considerado «</strong><em><strong>un insulto que ofende la inteligencia de los italianos</strong> <strong>y son la prueba de que las multinacionales están en dificultad</strong></em>» para defender la práctica de la vivisección, según denuncia la exministra de Turismo, Michela Brambilla, fundadora de la Federación Italiana de Asociaciones por los Derechos de los Animales.

El año pasado, una campaña muy similar fue llevada a cabo en Estados Unidos en las ciudades donde se ubican los principales centros de investigación médica en primates: Los Angeles, Seattle, Portland, Chicago y Baltimore. En este caso, la campaña fue costeada por la Foundation for Biomedical Research, una organización financiada a su vez por las universidades, hospitales, compañías farmacéuticas, biotecnológicas y empresas con intereses en la investigación con animales.

<hr />

<em>¿Quieres estar al corriente de todas nuestras actividades?<a href="https://igualdadanimal.org/suscripcion/"><strong> Suscríbete a nuestro boletín</strong></a></em>

<hr />

<h4>El 86% de la población italiana rechaza la vivisección</h4>
Hace poco tiempo era impensable una gran campaña de estas características,<strong> financiada fundamentalmente por las multinacionales farmacéuticas</strong>; si ahora se hace es porque<strong> ha crecido muchísimo la conciencia social contra la vivisección</strong>. En efecto, el instituto de investigación italiano Eurispes afirma que el 86% de los italianos rechaza totalmente esa práctica.<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157624835696312/with/4939623358/" target="_blank" rel="noopener"><img style="width: 240px;" title="Primate víctima de la vivisección" src="https://farm5.staticflickr.com/4135/4939623358_756c57598f_m.jpg" alt="" /></a>

Según la exministra Brambilla, hace cinco años que la experimentación con animales ha sido eliminada en los centros de investigación estatal de Estados Unidos, porque «<em>existen seguros métodos alternativos a la vivisección, mediante el cultivo de células y tejidos</em>». «<strong><em>Los intereses económicos de estas multinacionales</em></strong> –añade Brambilla– <em><strong>no deben prevalecer sobre el derecho de los ciudadanos de poder contar con una investigación científica éticamente aceptable y verdaderamente segura</strong>. No hay necesidad de escoger entre la rata y la niña. Pueden vivir los dos</em>». Los promotores de la campaña tratan de convencer a la sociedad con esta campaña de que sin la investigación con los animales las medicinas y los tratamientos que hoy tenemos no existirían.
<h4>Próxima manifestación nacional para pedir el cierre de un criadero de perros</h4>
Seguramente, lo que ha movilizado más a los italianos contra la vivisección sea el centro de «Green Hill», donde se crían 2.500 perros de raza «beagle» destinados a los laboratorios de todo el mundo, fundamentalmente en Europa.

Se trata de un lucrativo negocio que ha irritado a muchos italianos, hasta el punto de que <strong>el próximo 28 de abril hay prevista una manifestación nacional para que sea cerrado el centro y liberados los «beagle»</strong>. Para defender sus argumentos recuerdan incluso una frase de Albert Einstein a propósito de la vivisección: «Ningún objetivo es tan alto para justificar métodos tan indignos».

<iframe src="https://player.vimeo.com/video/18732435?title=0&amp;byline=0&amp;portrait=0&amp;color=00c4ff" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

<small>Simpatizantes de Igualdad Animal rescatan a 36 perros de un criadero de animales para vivisección</small>
