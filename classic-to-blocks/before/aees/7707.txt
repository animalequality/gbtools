<div class="center"><img class="wp-image-11717" src="/app/uploads/2017/03/14425809129_17d818a30a_z.jpg" /></div>
<p>Si alguien tratase a nuestros perros y gatos como se trata a los animales de granja acabar&iacute;a en la c&aacute;rcel.</p>

<p>Tal es la magnitud del maltrato al que se ven sometidos cerdos, terneros, corderos, pollos, vacas, gallinas y dem&aacute;s animales de granja. Sin embargo, quienes maltratan a estos animales <strong>rara vez se ven perseguidos por la ley o rechazados por la sociedad</strong>. &iquest;Por qu&eacute;?</p>

<p>La respuesta no es sencilla y tiene m&uacute;ltiples capas. Para empezar, la crueldad hacia estos animales permanece oculta a los ciudadanos. Rara vez hay noticias en los medios de comunicaci&oacute;n sobre ella. Para continuar, las leyes de protecci&oacute;n son permisivas e insuficientes: <a href="https://www.publico.es/sociedad/igualdad-animal-denuncia-maltrato-animales.html" target="_blank">maltratar a un cerdo o una ternera suele salir gratis</a>.</p>

<p>&nbsp;</p>

<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</font></font></p>

<p>&nbsp;</p>

<p>Pero, tal vez, el punto m&aacute;s complejo es que el maltrato es omnipresente, institucionalizado y tiene su origen en el eslab&oacute;n m&aacute;s inc&oacute;modo de aceptar: <strong>nosotros mismos</strong> en nuestra faceta como consumidores.</p>

<p>&nbsp;</p>

<p><u><strong>&iquest;Qui&eacute;nes son los maltratadores y por qu&eacute; maltratan?</strong></u></p>

<p>El retrato de este maltratador, a grandes rasgos, es el de una persona que trabaja en el seno de las industrias c&aacute;rnica, l&aacute;ctea o del huevo. No se trata de psic&oacute;patas que disfruten haciendo sufrir (o, al menos, no suele tratarse). El problema es m&aacute;s intrincado: en estas industrias se trabaja &laquo;procesando&raquo; a animales. A muchos, much&iacute;simos animales.&nbsp;</p>

<p><img alt="" class="wp-image-11718" src="/app/uploads/2017/03/14205515176_67b7e8647b_z.jpg" style="margin: 10px; float: left; width: 500px; " /></p>

<p>Cuando el n&uacute;mero de animales a los que se tiene que manipular, castrar, hacer andar hacia el matarife, colgar boca abajo, aplicar descargas el&eacute;ctricas de aturdimiento y degollar es alto, los fallos abundan. Por otro lado, la sensibilidad de los trabajadores hacia el sufrimiento de los animales desaparece.</p>

<p>&nbsp;</p>

<p><font size="5"><font size="5"><font color="#808080">&laquo;Si los consumidores deciden demandar otro tipo de productos, las industrias los producir&aacute;n.&raquo;</font></font></font></p>

<p>&nbsp;</p>

<p>No existe una manera de criar a millones de animales en condiciones industriales sin que abunde el maltrato.</p>

<p>&nbsp;</p>

<p>Los animales manipulados bruscamente; las castraciones se realizan sin cuidado; se hace andar a los animales a base de golpes o descargas el&eacute;ctricas; se les cuelga boca abajo con violencia; el aturdimiento previo a la muerte se realiza mal o directamente no se realiza y el degollamiento en cadena deja a muchos animales sufriendo muertes ag&oacute;nicas.</p>

<p>Por si fuera poco, <strong>los trabajadores saben que no tendr&aacute;n que responder ante nadie de sus actos</strong>. As&iacute;, las leyes de bienestar animal se incumplen con asiduidad y las pr&aacute;cticas se vuelven innecesariamente crueles y violentas.</p>

<p><br />
&nbsp;</p>

<p><u><strong>&iquest;C&oacute;mo ponerle fin?</strong></u></p>

<p>Para enfrentarse a un problema como este tenemos que dirigir la mirada hacia el &uacute;ltimo eslab&oacute;n de esta cadena: los consumidores.</p>

<p>Al contrario que en el caso de perros y gatos, en el que la denuncia juega un papel esencial, en el caso del maltrato a animales de granja <strong>la herramienta m&aacute;s efectiva en nuestras manos </strong>son nuestros h&aacute;bitos de consumo.</p>

<p>Las industrias alimentarias, como el resto, responden a las leyes de la oferta y la demanda. <strong>Si los consumidores deciden demandar otro tipo de productos, las industrias los producir&aacute;n</strong>. Cuando la tendencia alcanza el suficiente grado de apoyo, los nuevos productos se normalizan, siendo f&aacute;cil adquirirlos en las cadenas de supermercados.</p>

<p><font size="5"><font size="5"><img alt="" class="wp-image-11719" src="/app/uploads/2017/03/7515714794_e9493dc58c_z.jpg" style="margin: 10px; float: left; width: 500px; " /></font></font></p>

<p>Precisamente esto ha sucedido con las leches vegetales. Estos productos han pasado de venderse en establecimientos especializados a hacerlo en las grandes cadenas. Las marcas y sabores abundan: la normalizaci&oacute;n ha sucedido en cuesti&oacute;n de pocos a&ntilde;os.</p>

<p>De la misma manera, reducir el consumo de carne, l&aacute;cteos o huevos e introducir alternativas vegetarianas a estos productos (de los que, adem&aacute;s, abusamos sin medida), generar&aacute; el mismo escenario. <strong>Y, de hecho, ya est&aacute; sucediendo</strong>: las alternativas vegetales a la carne son cada vez m&aacute;s comunes y <a href="https://igualdadanimal.org/noticia/2017/02/13/el-maltrato-animal-primera-causa-de-reduccion-del-consumo-de-carne/" target="_blank">parte de la sociedad espa&ntilde;ola ha reducido su consumo de carne hasta m&iacute;nimos hist&oacute;ricos</a>.</p>

<p>El maltratador de animales de granja es el que a m&aacute;s animales maltrata. Pero no son todo malas noticias: si el d&iacute;a de ma&ntilde;ana tiene que procesar hamburguesas vegetales y no animales, el maltrato desaparecer&aacute; de la cadena alimentaria.</p>

