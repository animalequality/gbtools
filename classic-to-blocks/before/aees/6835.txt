<p align="justify">Hoy, jueves 20 de diciembre de 2012, Miguel Salas, uno de los seis activistas juzgados por el salto al ruedo de Las Ventas en 2008, ha sido <strong>totalmente absuelto </strong>de los cargos que se le imputaban.</p>

<p><img alt="" class="wp-image-9821" src="/app/uploads/2012/12/salto_ventas_eq.jpg" style="text-align: justify; width: 200px; float: left; " /></p>

<p align="justify">Para este juicio, el delegado y presidente de la plaza de toros de Las Ventas fueron llamados a declarar y, en base a sus testimonios, <strong>el juez consider&oacute; que Miguel Salas no incumpli&oacute; ninguna orden de abandonar la plaza</strong>, ya que dicha orden no fue expresada por parte del presidente, la &uacute;nica persona con competencia en el recinto taurino para darla.</p>

<p align="justify">El pasado 9 de octubre, <strong>David Herrero, otro de los activistas, fue tambi&eacute;n absuelto</strong>. En este caso el juez consider&oacute; que la irrupci&oacute;n en el ruedo durante &ldquo;el arrastre&rdquo; no interrump&iacute;a el curso normal de la corrida de toros, pues <strong>el intermedio no forma parte de la lidia</strong>.</p>

<p align="justify">Sin embargo, y pese a haber participado en la misma acci&oacute;n, bajo las mismas circunstancias, el resto de activistas fueron v&iacute;ctimas de la parcialidad de los jueces y han sido condenados a pagar la desorbitada <strong>sanci&oacute;n de 3.000 &euro; que les ha impuesto la Comunidad de Madrid</strong>. S&oacute;lo en el caso de Alessandro Zara esta multa se redujo a 300 &euro;.</p>

<p align="justify"><strong>Un total de 9.300 &euro; en multas por una acci&oacute;n que dos de los jueces consideraron merecedoras de absoluci&oacute;n.</strong></p>

<h4 align="justify"><img alt="" src="https://pacma.es/wp-content/uploads/2015/08/concentracion-de-apoyo-a-los-activistas-multados-por-el-salto-al-ruedo-de-la-ven.jpg" style="width: 580px; " /></h4>


<h4 align="justify">Solidar&iacute;zate con los activistas</h4>

<p align="justify">Por ello, para ayudar econ&oacute;micamente a los activistas multados, el pr&oacute;ximo <strong>s&aacute;bado 29 de diciembre</strong>, celebraremos un <strong>concierto solidario </strong>de jazz y m&uacute;sica cl&aacute;sica en Vigo, donde podr&aacute;s degustar adem&aacute;s deliciosas tapas y pinchos veganos. Puedes reservar tu entrada escribiendo a Galicia@igualdadanimal.org o llamando al 650 576 631.</p>

<p>&bull; PRECIO DE LA ENTRADA: 5 &euro;<br />
&bull; D&Iacute;A Y HORA: S&aacute;bado 29 a partir de las 21h<br />
&bull; LUGAR: Local de la Asociaci&oacute;n SerHacer - Praza Santa Rita N&ordm; 3 -local 1- Vigo.</p>

<p>M&aacute;s informaci&oacute;n en el <a href="https://www.facebook.com/events/345931055514714/" target="_blank">evento de Facebook</a>.</p>

<p align="justify">En caso de no poder acudir al concierto, tambi&eacute;n puedes solidarizarte con los activistas realizando una <strong>donaci&oacute;n</strong>, por peque&ntilde;a que sea, al siguiente n&uacute;mero de cuenta:</p>

<p align="justify"><strong>BBVA 0182-4022-12-0201520820</strong>, especificando en el concepto: &quot;<strong>Multa salto al ruedo</strong>&quot;.</p>

<p><strong>Ante la injusticia, tu solidaridad es imprescindible. Gracias.</strong></p>

