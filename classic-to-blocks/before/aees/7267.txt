<img alt="" class="wp-image-10264" src="/app/uploads/2015/07/gadhi.jpg" style="float:right; margin:5px; width:450px">La organización internacional Igualdad Animal presenta un <a href="https://youtu.be/75W8ULrK4DY"><strong>nuevo reportaje con imágenes nunca vistas</strong></a> del Festival de Gadhimai en Nepal, donde cientos de miles de animales son sacrificados en nombre de la diosa hindú Gadhimai.

El reportaje que está presentando hoy internacionalmente Igualdad Animal contiene imágenes nunca vistas del Festival, que muestran la brutalidad y violencia extrema que sufren los animales víctimas de este ritual religioso. El vídeo también recoge el trabajo llevado a cabo por la organización con la campaña “Stop Sacrificios” para intentar detener la matanza de animales en el festival, con la que consiguieron que el Gobierno de India bloqueara el transporte de animales al festival en la frontera con Nepal [1] &nbsp;y que finalmente el número de animales sacrificados durante 2014 se redujera en un 70% respecto a la edición anterior de 2009. [2]

El equipo de Igualdad Animal India también documentó con un dron el sacrificio de animales durante el festival, consiguiendo imágenes nunca vistas de la matanza. [3]

&nbsp;
<div class="ae-video-container"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/75W8ULrK4DY" width="885px"></iframe></div>
&nbsp;

“<em>Hemos trabajado intensamente para detener el sacrificio de animales del Festival de Gadhimai, tanto a nivel local como a nivel internacional, realizando protestas en las embajadas de India de varios países como España, Inglaterra o Alemania. Esperamos que la publicación de este reportaje, que es un testimonio gráfico nunca visto de la brutalidad y violencia que viven los animales en este ritual religioso, sea un paso más hacia el fin del sacrificio de animales en este festival</em>”, afirmó Amruta Ubale, coordinadora de Igualdad Animal India.

Para Javier Moreno, cofundador de Igualdad Animal “<em>la presión internacional cada vez es mayor contra la crueldad de este ritual religioso. En la última edición se ha reducido en un 70 % el número de animales sacrificados y seguiremos trabajando para que no se sacrifique a ninguno, ya que la tradición o la religión no pueden estar por encima del sufrimiento de los animales</em>”. &nbsp;

[1]http://www.europapress.es/epsocial/ong-y-asociaciones/noticia-india-impedira-transporte-animales-evitar-sacrificio-festival-religioso-nepal-20140821131750.html

[2] http://www.europapress.es/epsocial/ong-y-asociaciones/noticia-igualdad-animal-cifra-69-descenso-sacrificios-festival-gadhimai-nepal-respecto-2009-20141201181549.html

[3] http://www.elmundo.es/el-mundo-tv/2014/12/01/547c5ec2268e3e2c718b4574.html
