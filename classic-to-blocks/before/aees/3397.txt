Ttal y como narra la propia Policía Local de Melilla, concretamente en la barriada de Cabrerizas una patrulla que realizaba un recorrido de vigilancia por el lugar observó a dos individuos en la Plaza de las Américas que tenían sujetos por un collar a dos perros, intentando que estos perros se enzarzaran en una pelea, con el consiguiente daño que los propios animales podía sufrir.

Al percatarse de la presencia policial, las dos personas emprendieron la huida por diferentes calles de la barriada y, aunque no pudieron ser alcanzado por los agentes, uno de ellos fue reconocido por los policías actuantes, al estar implicado en numerosas intervenciones policiales.


Fuente: infomelilla.com
