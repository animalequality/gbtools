Comunidades indígenas de una región del oriente boliviano exportarán carne de capibara a Venezuela el próximo año con fines experimentales, informó una organización ambientalista.

Será la primera vez que el país exportará carne de ese roedor gigante que habita las sabanas, ríos y pantanos del oriente boliviano, informó a la AP, Ruth Delgado de la Fundación Amigos de la Naturaleza (FAN).

Estudios en el municipio de San Javier en el departamento del Beni al noreste de Bolivia, confirmaron que la exportación de una cantidad determinada es sostenible y no pone en riesgo la especie, dijo Delgado.

Esos estudios recomendaron la exportación de entre 200 y 500 capibaras al año que representa el 15% de la población.

La exportación será en forma de charqui a un precio de 8 dólares el kilo y el mercado decidirá si la exportación es viable. “Menos de ese precio no será rentable el negocio”, subrayó.

Es la segunda iniciativa de exportación de carne "salvaje" con fines comerciales. Anteriormente se exportó a otros países carne de lagarto pero se interrumpió por que era preciso “mayor negociación por parte de la cancillería”.

<em>Fuente: </em>
www.noticias24.com


