Nueva York - Varios perros y un gato han sido obligados a participar en la Semana de la Moda, llevando ropas y, algunos de ellos, pieles.

Estos individuos han sido obligados a veestir prendas de veinte empresas de moda. A consecuencia de esta actividad, los animales han sufrido estrés, como han reconocido las propias empresas participantes. En el evento se han servido alimentos de origen animal, como caviar.

En esta actividad se está explotando a los animales no-humanos (recordemos que los humanos también somos animales) para obtener un beneficio económico, además de participar en su compraventa. Si bien esto es motivo suficiente para rechazarla, hay que decir también que los animales no-humanos obligados a participar en el evento están sufriendo estrés.



