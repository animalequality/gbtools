<p><img alt="" class="wp-image-9425" src="/app/uploads/2012/03/granja_holandesa_pavos.jpg" style="width: 220px; float: left;" />Un brote de gripe aviar supondr&aacute; la <strong>matanza inmediata de</strong> <strong>42.700 pavos</strong> en Limburgo, Holanda. La autoridad holandesa de Seguridad Alimentaria y del Consumidor decidi&oacute; prohibir adem&aacute;s el transporte de aves, huevos y esti&eacute;rcol en un radio de 3 km alrededor de la granja afectada, una zona en la que se ubican 25 explotaciones aviares m&aacute;s.</p>
<p>Estos pavos, como cualquier otro animal enfermo explotado en una granja de producci&oacute;n, <strong>ser&aacute;n desechados como basura.</strong> Ya sea por enfermedades derivadas de la propia explotaci&oacute;n o en un matadero, estos animales no vivir&iacute;an m&aacute;s que unas pocas semanas (entre 12 y 26),&nbsp; siendo su esperanza de vida natural de hasta diez a&ntilde;os.</p>
<p>Mientras otros individuos de su especie disfrutan d&aacute;ndose ba&ntilde;os de arena, corriendo o volando, los pavos sometidos por la industria engordan tanto que sus patas no pueden soportar su peso y se vuelven completos inv&aacute;lidos. Muchos de ellos mueren al ser incapaces de alcanzar alguna fuente de agua o comida. Quienes no mueren por estas causas pasan el resto de sus cortas vidas padeciendo los dolores m&aacute;s inimaginables.</p>
<p><a href="https://igualdadanimal.org/" target="_blank"><img src="http://www.granjasdeesclavos.com/themes/pavos_comoson/img/pavos.jpg" style="width: 550px; " /></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Existe un desconocimiento generalizado por parte de la sociedad de los h&aacute;bitos y personalidad de los animales explotados para consumo humano. Sin embargo, y al igual que los perros, los gatos y otros animales de especies diferentes a la nuestra con los que convivimos y establecemos fuertes v&iacute;nculos emocionales, los individuos explotados para alimentaci&oacute;n son tambi&eacute;n seres capaces de sentir, de diferenciar un golpe de una caricia, de experimentar deseos, alegr&iacute;a, dolor o miedo.</p>
<p>Desde <a href="https://igualdadanimal.org/" target="_blank">Igualdad Animal</a> te invitamos a conocerlos, a empatizar con sus emociones, a descubrir alternativas que no impliquen su esclavitud y su muerte.</p>
<p>Por favor, conoce a &eacute;stos y a muchos otros animales en <a href="http://www.GranjasDeEsclavos.org" target="_blank">www.GranjasDeEsclavos.org </a><br />
	Y descubre c&oacute;mo liberarlos en <a href="http://www.ViveVegano.org" target="_blank">www.ViveVegano.org</a></p>

