<div class="center"><img class="wp-image-9780" src="/app/uploads/2012/11/koala_incendio_p.jpg" /></div>
<p align="justify">Uno de los pocos supervivientes del devastador incendio que arras&oacute; estos d&iacute;as <strong>2.000 hect&aacute;reas de bosque y matorral </strong>en el sur de Australia es este koala, que fue encontrado y atendido por un residente de Port Lincoln.&nbsp;</p>
<p><img alt="" class="wp-image-14084" src="/app/uploads/2012/11/koala_incendio.jpg" style="width: 580px; " /></p>
<p>&nbsp;</p>
<p align="justify">El koala se encontraba solo sobre un &aacute;rbol calcinado, <strong>visiblemente afectado</strong> por la dram&aacute;tica situaci&oacute;n vivida, y ten&iacute;a<strong> las patas y gran parte de su cuerpo quemado</strong>.&nbsp;</p>
<p><iframe allowfullscreen="" frameborder="0" height="435" src="https://www.youtube.com/embed/8l9nTbJxFaA" width="580"></iframe></p>
<p align="justify">Desconocemos si el protagonista de esta noticia est&aacute; siendo adecuadamente atendido por servicios veterinarios y si ha sido llevado a un lugar seguro en el que recuperarse de tan traum&aacute;tica experiencia, aunque es de suponer que s&iacute;. En Australia, afortunadamente, existen diversos refugios y centros de acogida para estos animales, que cuentan con especial protecci&oacute;n a nivel nacional.</p>

