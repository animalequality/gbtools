Lima, Perú — En el zoo de Ica, 240 kilómetros al sureste de Lima, han muerto dos pingüinos recién nacidos. Uno de los pingüinos fue descubierto asesinado y mutilado en las cercanías de la prisión. A consecuencia de esto, dicha prisión de animales ha sido cerrada de forma temporal.

Ya con anterioridad habían muerto varios animales en esa prisión. En 2006 cuatro dromedarios murieron a causa de una intoxicación. Y el pasado 25 de mayo apareció muerto un día después de su desaparición.

Además de estas muertes, hay que tener en cuenta el sufrimiento y la privación de libertad a la que se ven sometidos todos los animales que viven encerrados en dicho zoo y en el resto de zoos del mundo.
