Reducir el consumo de carne o sustituirla por completo en nuestra alimentación es algo bueno para ti, para el planeta y para los animales. Sin duda es uno de los actos cotidianos <strong>más beneficiosos y poderosos</strong> que podemos llevar a cabo. A día de hoy el asunto está en boca de todos… literalmente.

No está de más que te demos alguna clave para conseguirlo. La cultura de la carne nos rodea y tener recursos a nuestra disposición nos ayuda. Conseguir aquello que nos proponemos <strong>nos hace sentir bien y seguros de nosotros mismos</strong>. Esto a su vez nos anima a dar más pasos e incorporar más alternativas a la carne. Sigue leyendo para saber más sobre la clave del éxito: ¡los hábitos!

Una buena idea es comenzar con pequeños cambios de hábitos. Intenta incluir un pequeño y nuevo cambio dentro de tu rutina cada semana. Por ejemplo, podrías comenzar con <strong>reducir o reemplazar la carne de pollo durante la primera semana</strong> con una alternativa sabrosa: seitán, tofu, tempeh o las clásicas y deliciosas legumbres, repletas de proteínas.

<img class="wp-image-10651 alignleft" style="float: left; margin: 10px; width: 450px;" src="/app/uploads/2016/05/notciamartes17.jpg" alt="" />

Resulta de mucha ayuda tener <strong>recordatorios positivos</strong> para alegrar tu día y que te hagan sentir bien por los cambios que estás haciendo en tu vida. Desde tener una imagen de fondo de móvil que te motive a pegar en tu nevera una foto que te recuerde lo grande que eres por ayudar en tu día a día a quienes lo necesitan.

Las posibilidades de <a href="https://igualdadanimal.org/noticia/2016/03/10/estas-son-algunas-de-nuestras-alternativas-la-carne-favoritas/">las alternativas a la carne</a> son infinitas. Tantas como las de la carne, <strong>solo tenemos que acostumbrarnos a utilizar estos nuevos productos</strong>. Dar pequeños pasos a como el descrito te ayudará a conseguir tu propósito poco a poco. Póntelo fácil, ¡no te presiones!

Otro posible hábito es unirte al movimiento de los Lunes sin carne. Este movimiento <strong>está triunfando en todo el mundo</strong> y eso es debido a su filosofía comprometida pero sencilla. Convertir en un hábito no comer carne un día a la semana ayuda a los animales y al planeta, pero también te sentará genial a ti mismo.

Además para ayudarte a comer fuera de tu casa puedes descargarte en tu móvil la app Happy Cow. Con esta increíble app <strong>siempre sabrás que bares y restaurantes te ofrecen opciones vegetarianas cerca de donde estés</strong>. ¡Es una idea genial!

¿Estás preparado? ¡Ánimo! En este viaje no estarás solo: <strong>cada vez más personas reducen su consumo de carne</strong> para sentirse increíblemente bien <a href="https://igualdadanimal.org/noticia/2016/04/12/la-revolucion-pacifica-que-soluciona-algunos-de-los-peores-problemas-de-nuestros/">ayudando al planeta y a los animales</a>.

Ve paso a paso y disfruta del viaje: este puede ser el mejor viaje de toda tu vida para ti y para <a href="https://igualdadanimal.org/noticia/2016/04/14/el-peor-maltrato-animal-conocido-se-produce-en-las-granjas-y-mataderos/">muchos seres inocentes</a> atrapados en crueles granjas industriales. ¡Gracias por lo que estás haciendo!
