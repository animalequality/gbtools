Existen obras que logran cambiar las vidas de las personas, permitiéndoles ver con nuevos ojos una realidad hasta ahora oculta para ellas. Hoy te presentamos tres películas maravillosas que <strong>han ayudado a miles de personas a reflexionar sobre el mundo emocional y el sufrimiento que padecen los animales</strong> en las granjas industriales y mataderos.

<strong>Leafie, una gallina tocada del ala</strong>

Esta película coreana lanzada en 2011 y basada en el libro de Hwang Sun-mi fue una de las más taquilleras en ese país. Leafie es una gallina que vive atrapada dentro de una jaula con el único propósito de poner un huevo tras otro y  su deseo de ser libre la lleva a intentar escapar.

Esta historia conmovedora cuestiona uno de los sistemas más utilizados y crueles de la industria ganadera: la producción de huevos. Y también nos muestra el mundo emocional de las gallinas, esos sensibles e increíbles animales que son abusados hasta la muerte.

<iframe class="giphy-embed" src="https://giphy.com/embed/dFqX69HHJkl6E" width="480" height="260" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

<a href="https://giphy.com/gifs/hens-gallinas-lifi-dFqX69HHJkl6E">via GIPHY</a>

<strong>Evasión en la granja</strong>

En 2010 esta película llegó a los cines mostrando las aventuras de un grupo de gallinas y sus intrépidas e ingeniosas ideas para escapar de su destino. Los dueños de la granja son una pareja que suele matar a las gallinas cuando les representan pérdidas económicas debido a su poca puesta de huevos, tal cual como ocurre en la industria del huevo. «Evasión en la granja» presenta una nueva perspectiva sobre la industria vista desde la mirada de las gallinas y que, también, nos lleva a reflexionar sobre nuestra relación con los animales.

https://www.youtube.com/watch?v=PDdqszX-hR8

&nbsp;
<h4 style="text-align: center;"><strong>¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja? </strong></h4>
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;"><strong>¡Suscríbete gratuitamente a nuestro e-boletín!</strong></span></a></h4>
&nbsp;

<strong>La telaraña de Charlotte</strong>

De Hanna-Barbera y basada en un libro clásico para niños de E.B. White, esta película ha movido corazones. «Yo no veo la diferencia y es el más terrible caso de injusticia que he conocido» dice en esta película la niña que quiere salvar a Wilbur, un cerdito que estuvo a punto de ser aniquilado al momento de nacer.

https://www.youtube.com/watch?v=KUBAOyTzwRQ
