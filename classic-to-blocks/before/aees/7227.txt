<div class="center"><img class="wp-image-10224" src="/app/uploads/2015/02/quintosmuro_3.jpg" /></div>
<p><img alt="" class="wp-image-13729" src="/app/uploads/2015/02/QuintosMuro.jpg" style="float:right; margin-left:5px; margin-right:5px; width:450px" />Gallinas decapitadas o gallos obligados a ingerir alcohol, han sido algunas de las v&iacute;ctimas de los quintos de este a&ntilde;o seg&uacute;n denuncian los vecinos en varios medios de comunicaci&oacute;n locales.</p>

<p>Esta cruel tradici&oacute;n, que se viene repitiendo a&ntilde;o tras a&ntilde;o, deja en evidencia la desprotecci&oacute;n que padecen los animales, pues incluso la Polic&iacute;a Local de Muro no actu&oacute; ante un evidente delito de maltrato animal.</p>

<p>La violencia contra los animales no puede formar parte de una celebraci&oacute;n juvenil si queremos construir una sociedad respetuosa y educada en valores.</p>

<p><br />
Puedes enviar tus quejas al ayuntamiento de Muro en este enlace: http://www.ajmuro.net/contacte/index.ct.html</p>

<p>El <a href="https://pacma.es/">Partido Animalista Pacma </a>ha denunciado los hechos ante la Fiscal&iacute;a de Medio Ambiente de Mallorca.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/igualdadanimal/albums/72157648544020624" title="Los quintos de Muro maltratan animales hasta la muerte"><img src="https://farm8.staticflickr.com/7401/16562118635_c968cde06f_z.jpg" width="600" height="400" alt="Los quintos de Muro maltratan animales hasta la muerte"></a><script async src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
