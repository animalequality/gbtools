En el club Rivera, en Chimbas, se celebró una exhibición de cinchada, un espectáculo en el que varios caballos son torturados.

En dicho espectáculo dos caballos, encima de los cuales se habían subido personas, fueron atados entre sí. A continuación las personas golpeaban al caballo encima del que se habían subido, buscando así que el otro retrocediera.

Una de esas dos personas, Juan Fernández, afirmó: "Cuando hay que enlazar un toro bravo o una vaca que no quiere ir con la manada hay que emplear la fuerza del caballo. Se necesitan de 7 meses a un año para que un caballo aprenda a trabajar en la cinchada".
