Washington — Según publica hoy el Washington post las autoridades estadounidenses aprobarán este año a la venta de leche y carne de animales no humanos clonados al haber llegado a la conclusión de que el consumo de dichos productos no suponen un riesgo para la salud humana

La decisión, que según el "Post" se espera para finales del 2006, llega tres años después de que la Agencia de Alimentos y Medicamentos (FDA) de Estados Unidos insinuase su intención de avanzar en esa dirección.

El periódico señala que los granjeros y compañías que crían animales clonados sostienen que con ese sistema pueden alcanzar una consistencia y calidad imposible de obtener con los sistemas de reproducción tradicionales, argumentando que pueden obtener "muchas copias de animales excepcionales".

Los animales no humanos clonados son efectivamente "replicas" de otros con la misma capacidad de sufrir y disfrutar y por lo tanto con idéntico interés en vivir y ser libres.
La clonación de animales no humanos para su consumo -o el consumo de productos derivados de la explotación de los mismos- no es más que otra muestra de especismo. Mientras la clonación de humanos se presenta como un dilema ético que pocas/os aprueban, los animales no humanos son clonados para ser esclavizados para nuestro beneficio sin que demasiadas voces se manifiesten en contra.   
