<p>Cuando miramos a los ojos a nuestros queridos perros y gatos sabemos que tras esas bellas miradas hay seres capaces de comunicarse con nosotros. De hecho les hablamos continuamente y sabemos que, a su manera, nos entienden a la perfecci&oacute;n.</p>

<p>&iquest;Pero qu&eacute; pasar&iacute;a si nos encontr&aacute;ramos <strong>cara a cara</strong> con los animales que son destinados a la producci&oacute;n de alimentos?</p>

<p>Los investigadores de Igualdad Animal <a href="https://igualdadanimal.org/blog/9-fotos-para-explicar-por-que-amamos-nuestros-investigadores/">han presenciado en granjas alrededor del mundo</a> la vida de <strong>estos maravillosos animales</strong>. Nadie mejor que ellos saben lo que nos dir&iacute;an si pudieran hablar; nadie mejor que ellos sabe <a href="https://igualdadanimal.org/blog/razones-para-amar-locamente-los-animales-de-granja/">lo mucho que se parecen a nuestros perros y gatos</a> en su capacidad de comunicarse con nosotros.</p>

<p>Aunque cada uno de ellos <strong>tiene su propia personalidad</strong>, hay algunos mensajes que los animales de granja de todo el mundo nos dir&iacute;an:</p>

<p>&nbsp;</p>

<strong>&iexcl;Ayuda! &iexcl;No quiero morir!</strong>

<h4><img alt="" class="wp-image-10446" src="/app/uploads/2016/03/farmanimal1.jpg" style="float:left; margin:10px; width:450px" /></h4>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>



<strong>&iexcl;Por favor, no nos com&aacute;is!</strong>

<p><strong><img alt="" class="wp-image-13952" src="/app/uploads/2016/03/farmanimal2.jpg" style="float:left; margin:10px; width:450px" /></strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>


<strong>&iexcl;Tenemos miedo! &iquest;D&oacute;nde est&aacute;n nuestras madres?</strong>

<h4><img alt="" class="wp-image-13953" src="/app/uploads/2016/03/farmanimal3.jpg" style="float:left; margin:10px; width:450px" /></h4>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>



<strong>&iexcl;Por favor, sacadnos de aqu&iacute;!</strong>

<h4><img alt="" class="wp-image-13954" src="/app/uploads/2016/03/farmanimal4.jpg" style="float:left; margin:10px; width:450px" /></h4>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>



<strong>&iexcl;Socorro! &iexcl;No me hagas da&ntilde;o!</strong>

<h4><img alt="" class="wp-image-13955" src="/app/uploads/2016/03/farmanimal6.jpg" style="float:left; margin:10px; width:450px" /></h4>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>


<strong>Por favor, no me quit&eacute;is a mis peque&ntilde;os, otra vez no</strong>

<p><strong><img alt="" class="wp-image-13956" src="/app/uploads/2016/03/farmanimal7.jpg" style="float:left; margin:10px; width:450px" /></strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>


<strong>&iexcl;Mam&aacute;! Mam&aacute;!</strong>

<h4><img alt="" class="wp-image-13957" src="/app/uploads/2016/03/farmanimal8.jpg" style="float:left; margin:10px; width:450px" /></h4>

