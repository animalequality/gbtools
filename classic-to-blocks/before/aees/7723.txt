<div class="center"><img class="wp-image-11787" src="/app/uploads/2017/03/foto_fb_15.jpg" /></div>
<div class="center"><img class="wp-image-11786" src="/app/uploads/2017/03/a-meet-davey-bull-011.jpg" /></div>
<p>Davey (as&iacute; lo han llamado) es tal vez el toro m&aacute;s afortunado del mundo. Luego de vivir por dos a&ntilde;os en una granja, un d&iacute;a escap&oacute; de ella en b&uacute;squeda de su libertad.</p>

<p>Tras caminar un buen trecho, Davey lleg&oacute; a un lugar donde su vida cambiar&iacute;a para siempre: el refugio Animal Place, ese espacio id&iacute;lico donde conviven de forma permanente 300 animales de granja.</p>

<p>Todos ellos hab&iacute;an sido rescatados de mataderos, granjas industriales y laboratorios y con ello de las m&aacute;s terribles formas de maltrato imaginables y de una muerte segura.</p>

<p>Cuando la directora del refugio, Hanna Beins, se dispon&iacute;a a alimentar a la manada encontr&oacute; a este joven toro pelilargo que la miraba fijamente. Su llegada era inesperada, pero la forma en la cual se integr&oacute; con el reba&ntilde;o fue a&uacute;n m&aacute;s inusual.</p>

<p>&ldquo;Cuando introducimos vacas o toros a la manada a menudo hay persecuciones&rdquo;, explica Hanna. &ldquo;Sorprendentemente, la mayor parte del reba&ntilde;o estaba tranquilo, como si Davey hubiera vivido aqu&iacute; durante a&ntilde;os&rdquo;.</p>

<p>&nbsp;</p>

<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</font></font></p>

<p><br />
<br />
<font size="5"><img alt="" class="wp-image-11785" src="/app/uploads/2017/03/a-meet-davey-bull-02.jpg" style="margin-right: 10px; margin-left: 10px; float: left; width: 450px; " /></font></p>

<p>Debido a que las posibles condenas por no intentar buscar a la persona responsable de Davey eran demasiado graves, Hanna tuvo que comenzar a correr la voz de que el imponente animal de 453 kilos hab&iacute;a irrumpido en su refugio y estaba ahora bajo su cuidado.</p>

<p>Tom&oacute; un mes para que supieran que Davey hab&iacute;a escapado de una granja cercana cuyo propietario pensaba enviarlo a &eacute;l y a 17 toros, vacas y becerros al matadero. &laquo;Mi coraz&oacute;n se hundi&oacute;&raquo;, dice Hanna.</p>

<p>Hanna se reuni&oacute; con el ranchero y le rog&oacute; que permitiera que Davey permaneciera en el refugio y, afortunadamente, este acept&oacute;.</p>

<p>&laquo;Por alguna raz&oacute;n, luego de vivir por dos a&ntilde;os en su antiguo hogar, conoci&oacute; a nuestras vacas y se enamor&oacute; de ellas. &Eacute;l tumb&oacute; la cerca para encontrar su refugio, y nosotros estamos felices de que podamos verlo crecer y envejecer&raquo;, declara Beins.</p>

<p>Davey tuvo la fortuna de llegar a este lugar en donde personas compasivas lo recibieron e hicieron todo lo posible para que pudiera vivir y ser respetado como lo merece.</p>

<p>Miles de millones de animales no corren la misma suerte y siguen sufriendo y muriendo en los mataderos y granjas.</p>

<p>T&uacute; puedes hacer mucho por ellos. Considera darles una oportunidad sac&aacute;ndoles de tu plato. &nbsp;</p>

<p><font size="5">&nbsp;</font></p>

<p>Fuentes:</p>

<p><a href="https://people.com/pets/california-cow-dodges-slaughter-by-breaking-into-animal-sanctuary/" target="_blank">https://people.com/pets/california-cow-dodges-slaughter-by-breaking-into-animal-sanctuary/</a> <a href="https://www.all-creatures.org/stories/a-meet-davey-bull.html" target="_blank">https://www.all-creatures.org/stories/a-meet-davey-bull.html</a></p>

<p><a href="https://www.all-creatures.org/stories/a-meet-davey-bull.html" target="_blank">https://www.all-creatures.org/stories/a-meet-davey-bull.html</a></p>

