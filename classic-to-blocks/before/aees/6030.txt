<div class="center"><img class="wp-image-9050" src="/app/uploads/2011/06/caballos_salvajes_bosque_letea_rumania_matanza.jpg" /></div>
<p>Los aproximadamente&nbsp;<strong>2.500 caballos salvajes</strong>&nbsp;del&nbsp;<a href="https://en.wikipedia.org/wiki/Letea_Forest" target="_blank">Bosque Letea</a>, un importante espacio natural situado en el coraz&oacute;n de la&nbsp;<a href="https://es.wikipedia.org/wiki/Delta_del_Danubio" target="_blank">Reserva de la Biosfera del Delta del Danubio</a>, en Ruman&iacute;a, est&aacute;n&nbsp;<strong>amenazados de muerte</strong>.</p>

<div style="font-family: Arial, Verdana, sans-serif; font-size: 12px; color: rgb(34, 34, 34); background-color: rgb(255, 255, 255); ">
<p>Se les quiere&nbsp;<a href="https://igualdadanimal.org/problematica/carne/" target="_blank"><strong>convertir en carne</strong></a>&nbsp;para reducir la presi&oacute;n que su alto n&uacute;mero supone a la flora protegida de un amenazado h&aacute;bitat forestal &uacute;nico en el mundo, especialmente a sus lianas y otras especies trepadoras. En realidad, la matanza oculta un&nbsp;<strong>oscuro negocio de venta de carne de equino</strong>&nbsp;hacia pa&iacute;ses como Italia.</p>

<p>Estos caballos salvajes eran animales dom&eacute;sticos utilizados para los trabajos de labranza por la poblaci&oacute;n local, pero fueron&nbsp;<strong>abandonados por sus&nbsp;<a href="http://www.igualdadanimal.org/articulos/gary-francione/animales-como-propiedad" target="_blank">propietarios</a></strong><a href="http://www.igualdadanimal.org/articulos/gary-francione/animales-como-propiedad" target="_blank">&nbsp;</a>tras la ca&iacute;da del comunismo, a partir de 1989, y ahora su excesivo n&uacute;mero pone en peligro los h&aacute;bitats y despierta la codicia de los tratantes.</p>

<p>Hace unos d&iacute;as, gracias a los esfuerzos de los voluntarios se ha podido parar&nbsp;<a href="http://www.leteawildhorses.com/?page_id=21">un cami&oacute;n que transportaba 54 caballos del Bosque Letea a un matadero</a>. Los pobres animales estaban en unas condiciones p&eacute;simas, sin haber comido y bebido agua durante m&aacute;s de 48 horas.&nbsp;<strong>Cuatro murieron</strong>&nbsp;y otros est&aacute;n muy heridos.&nbsp;<strong>L</strong><strong>a</strong><strong>s autoridades no est&aacute;n haciendo nada al respecto.</strong></p>

<p><strong>En Espa&ntilde;a, durante 2010, seg&uacute;n cifras del Ministerio de Medio Ambiente Rural y Marino, fueron matados en los mataderos a 30.706 equinos de distintas especies por su carne, piel o cuero. No hay justificaci&oacute;n razonable alguna para matar y utilizar a los dem&aacute;s animales, sean de la especie que sean. <a href="http://www.vivevegano.org">Elige justicia, elige respeto, vive vegano</a>.</strong></p>

<p><strong>V&iacute;deo emitido por la televisi&oacute;n rumana sobre la captura de estos caballos.</strong></p>
</div>

