<p><img alt="" class="wp-image-10376" src="/app/uploads/2015/12/rickymax.jpg" style="float:right; margin:10px; width:450px" />Los dos pollitos nacieron a la vez. Rompieron el cascar&oacute;n trabajosamente y empezaron a piar llamando a sus madres. <strong>Sus nombres eran Ricky y Max</strong>. Max era m&aacute;s peque&ntilde;o que Ricky, pero eso a ellos les daba igual. Cuando se dieron cuenta de que sus madres no acud&iacute;an a su llamada, buscaron protecci&oacute;n y calor arrop&aacute;ndose mutuamente.</p>

<p>Ten&iacute;an mucho miedo. No entend&iacute;an qu&eacute; pasaba y su instinto les dec&iacute;a que algo iba mal. Hab&iacute;an nacido en una incubadora industrial junto a <strong>55.000 pollitos m&aacute;s</strong>. El sonido de tantos compa&ntilde;eros llamando a sus respectivas madres les confund&iacute;a y les atemorizaba.</p>

<p>Un operario abri&oacute; la puerta y llev&oacute; la bandeja donde estaban Ricky y Max hasta la sala de procesado. <strong>Lo primero que Ricky y el peque&ntilde;o Max vieron en la vida</strong> fue aquella fr&iacute;a y metalizada nave industrial y a sus miles de compa&ntilde;eros tan atemorizados y confundidos como ellos.</p>

<p>Otro operario agarr&oacute; la bandeja y descarg&oacute; violentamente a Ricky y a Max, junto a los dem&aacute;s pollitos, en una cinta transportadora. <strong>Ricky y Max se separaron al caer</strong> y se mezclaron con decenas de otros pollitos.</p>

<p>Se buscaron desesperadamente, pues s&oacute;lo se ten&iacute;an el uno al otro. Max, m&aacute;s peque&ntilde;o y d&eacute;bil, era pisoteado por sus compa&ntilde;eros, que trataban de buscar sitio y no ser aplastados. Ricky oy&oacute; el inconfundible piar de su nuevo y &uacute;nico amigo y, tras empujar a otros, lo encontr&oacute;. Pero cuando iba a darle su protecci&oacute;n y calor una mano de una operaria lo agarr&oacute; y lo lanz&oacute; a otra cinta.</p>

<p>&nbsp;</p>

<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/VGKQKGOyPtU" width="885px"></iframe></div>

<p>Max qued&oacute; atr&aacute;s. Ricky, desesperado, lo llamaba. Pero, Max, el peque&ntilde;o y d&eacute;bil Max, <strong>fue lanzado a una bandeja de descartados</strong>. Su peso y su constituci&oacute;n no eran las adecuadas para la industria de la carne de pollo. No era rentable comercialmente.</p>

<p>Max, aterrorizado por perder a Ricky, busc&oacute; el calor de otro pollito descartado en la bandeja, pero al acercarse se dio cuenta de que estaba muerto.</p>

<p><strong>Ricky fue pasando por las cintas</strong>, era lanzado bruscamente de una a otra. En el trayecto vio c&oacute;mo varios compa&ntilde;eros ca&iacute;an al suelo, perdi&eacute;ndose para siempre. Otros mor&iacute;an debido a que hab&iacute;an nacido demasiado pronto y estaban d&eacute;biles.</p>

<p>Entonces Ricky vio a Max en la caja de descartados. Intent&oacute; llamar su atenci&oacute;n desde la cinta. Max lo escuch&oacute; y <strong>comenz&oacute; a pedirle ayuda con la esperanza de que su amigo le salvara</strong>.</p>

<p>Los dos pollitos se miraron, su coraz&oacute;n lat&iacute;a fren&eacute;ticamente. Justo en ese momento una mano agarr&oacute; al peque&ntilde;o Max. Ricky no pod&iacute;a apartar la mirada. El operario cogi&oacute; a Max por la cabeza. <strong>Ricky escuchaba a Max llam&aacute;ndole desesperadamente</strong>. El operario empez&oacute; a apretar el cuello de Max y a tirar brutalmente. Max dej&oacute; de piar. Ricky presenci&oacute; aterrorizado como el operario arrancaba la cabeza del peque&ntilde;o y d&eacute;bil Max.</p>

<p>Traumatizado, sigui&oacute; el recorrido por las cintas, camino de su propio inhumano destino. No pod&iacute;a imaginar en aquel momento que, en comparaci&oacute;n con lo que le esperaba a &eacute;l, <strong>Max hab&iacute;a sido afortunado</strong>.</p>

<p>Por Ricky, Max, y millones de animales como ellos que necesitan desesperadamente tu ayuda, visita <a href="http://suprimerdia.com/">SuPrimerD&iacute;a.com</a></p>

