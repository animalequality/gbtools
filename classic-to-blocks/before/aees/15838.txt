Una vez más el cinco veces campeón mundial de Fórmula 1, Lewis Hamilton, ha utilizado la plataforma Instagram <strong>para enviar un mensaje de compasión y respeto hacia los animales </strong>maltratados en las granjas a sus más de 8 millones de seguidores.

En esta oportunidad, Hamilton publicó una fotografía de una cerda muerta que había sido tirada junto a sus crías a un contenedor y que decía «Una madre y sus bebés tirados como basura. Sus derechos nunca fueron considerados. Sus vidas nunca fueron suyas para vivirlas. Este es el precio del bacon». Y acompañó la desoladora imagen con el siguiente texto en suspropias palabras: «Todos tenemos una voz, yo tengo esta plataforma y no usarla correctamente estaría mal para mi. Nadie es perfecto, y ciertamente yo no lo soy pero esto ocurre cada día a tantos animales en todo el mundo. Esta es la razón por la cual decidí seguir una alimentación vegana desde hace ya un año. Es duro, nada es fácil, pero nunca me he sentido mejor que como me he sentido en este último año. #chooselove #govegan».
<blockquote class="instagram-media" style="background: #FFF; border: 0; border-radius: 3px; box-shadow: 0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width: 540px; min-width: 326px; padding: 0; width: calc(100% - 2px);" data-instgrm-captioned="" data-instgrm-permalink="https://www.instagram.com/p/BrLMEO0lbN5/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12">
<div style="padding: 16px;">
<div style="display: flex; flex-direction: row; align-items: center;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div>
<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div>
<div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div>
</div>
</div>
<div style="padding: 19% 0;"></div>
<div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;"></div>
<div style="padding-top: 8px;">
<div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;"><a style="background: #FFFFFF; line-height: 0; padding: 0 0; text-align: center; text-decoration: none; width: 100%;" href="https://www.instagram.com/p/BrLMEO0lbN5/?utm_source=ig_embed&amp;utm_medium=loading" target="_blank" rel="noopener">Ver esta publicación en Instagram</a></div>
</div>
<div style="padding: 12.5% 0;"></div>
<div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
<div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div>
<div style="background-color: #f4f4f4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div>
<div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
</div>
<div style="margin-left: 8px;">
<div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div>
<div style="width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg);"></div>
</div>
<div style="margin-left: auto;">
<div style="width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div>
<div style="background-color: #f4f4f4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div>
<div style="width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
</div>
</div>
<p style="margin: 8px 0 0 0; padding: 0 4px;"><a style="color: #000; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px; text-decoration: none; word-wrap: break-word;" href="https://www.instagram.com/p/BrLMEO0lbN5/?utm_source=ig_embed&amp;utm_medium=loading" target="_blank" rel="noopener">We all have a voice, I have this platform and so to not use it correctly would be wrong of me. Nobody is perfect, I certainly am not but this is actually happening everyday to so many animals worldwide. This is why I have decided to go to a vegan plant based diet, been over a year now. Yes it’s hard, nothings ever easy but I’ve felt the best I’ve ever felt for the past year. #chooselove #govegan</a></p>
<p style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; line-height: 17px; margin-bottom: 0; margin-top: 8px; overflow: hidden; padding: 8px 0 7px; text-align: center; text-overflow: ellipsis; white-space: nowrap;">Una publicación compartida de <a style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px;" href="https://www.instagram.com/lewishamilton/?utm_source=ig_embed&amp;utm_medium=loading" target="_blank" rel="noopener"> Lewis Hamilton</a> (@lewishamilton) el <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2018-12-09T17:23:16+00:00">9 Dic, 2018 a las 9:23 PST</time></p>

</div></blockquote>
<script async src="//www.instagram.com/embed.js"></script>
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación?</h4>
<h4 style="text-align: center;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
&nbsp;

Hace pocos meses, el piloto ya había compartido una historia en Instragram con un conmovedor mensaje: <strong>«619 millones de humanos han sido asesinados en guerras a todo lo largo de nuestra historia registrada. Los humanos matan a ese mismo número de animales cada 5 días»</strong>. Y debajo añadió: <strong>«Chicos, sigo una alimentación basada en vegetales y lo he hecho ya durante un año y me siento mejor que nunca. Si más personas se dan cuenta y no hacen la vista gorda, podemos evitar que esto suceda. Por favor, hagan un espacio en su corazón para no apoyar esta horrible crueldad y prueben comenzar a alimentarse de forma vegana».</strong>
