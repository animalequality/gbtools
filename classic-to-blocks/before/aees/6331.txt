<div class="center"><img class="wp-image-9345" src="/app/uploads/2012/02/animal_equality_pig_farm_investigation.jpg" /></div>
<p><a href="https://igualdadanimal.org/" target="_blank">Igualdad Animal</a> ha llevado a cabo una <a href="http://www.britishporkindustry.co.uk/es/" target="_blank">investigaci&oacute;n &uacute;nica e intensa</a>&nbsp; de dos meses de duraci&oacute;n infiltrados en una granja de Norfolk con el sello de &quot;Calidad Asegurada&quot; perteneciente al conocido esquema Red Tractor y perteneciente a A. J. Edwards &amp; Hijos. La impactante investigaci&oacute;n ha sido presentada en la p&aacute;gina principal de <a href="http://www.thesundaytimes.co.uk/sto/" target="_blank">The Sunday Times</a> de esta ma&ntilde;ana.</p>

<p>Esta <a href="http://www.britishporkindustry.co.uk/es/" target="_blank">investigaci&oacute;n de Igualdad Animal </a>revela la realidad de la industria porcina brit&aacute;nica. Nuestro investigador document&oacute; en detalle la angustia y el sufrimiento vividos por los cerdos de la granja, adem&aacute;s de <strong>terribles escenas de violencia</strong>.</p>

<p>&nbsp;</p>

<p align="center"><em><strong>► Advertencia: Este v&iacute;deo contiene material gr&aacute;fico de extrema violencia.</strong></em></p>

<p>&nbsp;</p>

<div class="media_embed" height="315px" width="560px"><iframe allowfullscreen="" frameborder="0" height="315px" src="https://www.youtube.com/embed/pQK4261GXyg" width="560px"></iframe></div>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>M&aacute;s de <a href="https://vimeo.com/channels/britishporkindustry" target="_blank">200 horas de metraje</a> y conversaciones grabadas, as&iacute; como <a href="https://www.flickr.com/photos/animalequalityuk/sets/72157628605983581/" target="_blank">335 fotograf&iacute;as</a>, nos proporcionan una imagen realmente estremecedora de la industria de cr&iacute;a de cerdos brit&aacute;nica, y demuestra que, independientemente del sello de bienestar animal, sigue existiendo un enorme dolor, sufrimiento y explotaci&oacute;n.</p>

<p>La <a href="http://www.britishporkindustry.co.uk/es/" target="_blank">Investigaci&oacute;n de Igualdad Animal </a>ha recibido declaraciones de apoyo de los principales expertos internacionales, entre ellos un reputado veterinario brit&aacute;nico, profesores y expertos en conducta animal. Para leer sus declaraciones, haz clic <a href="http://www.britishporkindustry.co.uk/es/expertos.php" target="_blank">aqu&iacute;</a> .</p>

<p>&nbsp;</p>

<h4><a href="http://www.britishporkindustry.co.uk/es/diario-del-investigador-en-la-granja-harling.php" target="_blank">Haz click aqu&iacute; para leer el diario del investigador</a></h4>

<p>&nbsp;</p>

<strong>Hallazgos de la investigaci&oacute;n:</strong>

<p>&nbsp;</p>

<ul>
	<li><img alt="" class="wp-image-13721" src="/app/uploads/2012/02/Photos_Animal_Equality_Pig_Farm_Investigation.jpg" style="width: 250px; float: right; margin-left: 8px; margin-right: 4px;" /><strong> Lechones </strong>que hab&iacute;an sido recientemente separados de sus madres y que son agarrados por sus patas y orejas y lanzados varios metros por el aire, golpeando fuertemente el suelo. Esto causa un estr&eacute;s extremo y heridas a los animales. La separaci&oacute;n de sus madres tambi&eacute;n les causa un trauma y deja a los lechones vulnerables a las enfermedades.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Cerdos </strong>reacios a moverse siendo golpeados y pateados hasta su l&iacute;mite con el uso de tablas, barras de hierro; as&iacute; como trabajadores cortando con un cuchillo el lomo de los cerdos para obligarlos a moverse.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Lechones que luchan fren&eacute;ticamente </strong>para escapar del aplastamiento producido por sus madres. Ellas mostraban comportamientos neur&oacute;ticos como la mordedura de barrotes indicador de frustraci&oacute;n, y dolor sin ser capaces de controlar la situaci&oacute;n.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li>El<strong> corte de dientes </strong>practicado rutinariamente a lechones entre las 24 y 48 horas despu&eacute;s de haber nacido. El corte de dientes provoca dolor y hace que los lechones sean m&aacute;s vulnerables a las infecciones.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Repetidos golpes </strong>que resultan en cerdos con abrasiones faciales y en el resto del cuerpo. Los golpes eran acompa&ntilde;ados de gritos e insultos que aterrorizaban a los cerdos. Un cerdo fue golpeado con una tuber&iacute;a hasta que la sangre sal&iacute;a por su nariz.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Lechones agonizando</strong> o ya cad&aacute;veres que eran abandonados en los dep&oacute;sitos de comida.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Cerdos heridos o enfermos </strong>que eran abandonados a su suerte sin atenci&oacute;n veterinaria. Los prolapsos rectales y vaginales eran ignorados hasta que se volv&iacute;an putrefactos y se ca&iacute;an de modo que los animales pudiesen ser enviados al matadero. Tambi&eacute;n se observaron cerdos con dificultad para caminar con heridas en los hombros, articulaciones infectadas o heridas y con par&aacute;lisis parcial.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Grandes y profundas laceraciones </strong>que eran acuchilladas por los trabajadores. Las heridas permanec&iacute;an despu&eacute;s sin tratar. Los cerdos sufr&iacute;an tumores, algunos con ulceraciones. Un cerdo aparentemente malnutrido ten&iacute;a una hernia abdominal de gran tama&ntilde;o.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Cad&aacute;veres </strong>de animales que eran <strong>enterrados ilegalmente</strong> para ahorrar gastos a la granja.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>La muerte de animales, </strong>por ejemplo mediante fuertes golpes: estampando a los animales con la cabeza contra el suelo de cemento y las palizas a los animales con barras de metal. Los trabajadores fueron observados dejando atr&aacute;s a cerdos sangrantes que luchaban violentamente en el suelo. Algunos fueron arrojados al contenedor de cad&aacute;veres todav&iacute;a vivos. Los cerdos en los contenedores convulsionaban entre los cad&aacute;veres hasta que mor&iacute;an.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Cerdos lactantes</strong> que eran forzados a padecer durante semanas un intenso confinamiento en jaulas de metal ligeramente mayores que sus cuerpos, mientras que otros cerdos eran aplastados o yac&iacute;an enfermos o muertos en las jaulas. Las cerdas incapaces siquiera de levantarse eran artificalmente inseminadas.

	<p>&nbsp;</p>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/animalequalityuk/albums/72157628605983581" title="Harling Farm (A. J. Edwards and Son) - East Harling (Norfolk)"><img src="https://farm8.staticflickr.com/7018/6588360171_9831ce9da5_z.jpg" width="640" height="480" alt="Harling Farm (A. J. Edwards and Son) - East Harling (Norfolk)"></a><script async src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

	
	</li>
</ul>

<strong>&iexcl;Ponte en acci&oacute;n!</strong>

<p>&nbsp;</p>

<ul>
	<li>
	<h4>&iexcl;Hazte vegano!</h4>
	</li>
</ul>

<p>&iexcl;<a href="http://www.vivevegano.org" target="_blank">Elige un estilo de vida vegano</a> hoy mismo!</p>

<p>La mayor&iacute;a de la gente cree que los animales no deber&iacute;an ser da&ntilde;ados innecesariamente, pero el consumo de productos animales impone a los animales una vida de miseria y una muerte. Neg&aacute;ndonos a participar en esta explotaci&oacute;n estamos evitando que miles de animales sean da&ntilde;ados y matados por nuestro consumo.</p>

<p>Elige una vida basada en el respeto y la justicia.</p>

<p>Para obtener m&aacute;s informaci&oacute;n:&nbsp;<a href="http://www.vivevegano.org" target="_blank"> ViveVegano.org</a></p>

<p>&nbsp;</p>

<ul>
	<li>
	<h4>Haz una donaci&oacute;n</h4>
	</li>
</ul>

<p>Por favor, <a href="http://www.igualdadanimal.org/colabora/donativos.php" target="_blank">considera hacer una donaci&oacute;n</a> para ayudar a Igualdad Animal a continuar con este trabajo vital para desvelar y poner fin a la explotaci&oacute;n animal.</p>

<p>&nbsp;</p>

<ul>
	<li>
	<h4>&iexcl;&Uacute;nete a nosotros!</h4>
	</li>
</ul>

<p><a href="https://igualdadanimal.org/" target="_blank">Igualdad Animal</a> realiza una gran variedad de actividades en defensa de los animales y tu ayuda es siempre bienvenida. Puedes contribuir como voluntario de muchas formas: en protestas, mesas informativas, repartiendo folletos, trabajando en nuestra oficina, organizando o ayudando en eventos ben&eacute;ficos, escribiendo, sacando fotograf&iacute;as&hellip;</p>

<p><a href="http://www.igualdadanimal.org/colabora/hazte-activista.php" target="_blank">Rellena nuestro formulario </a>y haznos saber c&oacute;mo te gustar&iacute;a participar. S&eacute; la voz de los sin voz.</p>

