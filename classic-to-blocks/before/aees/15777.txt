Seguir una alimentación basada en vegetales trae múltiples beneficios a nuestra salud y con ello evitamos también una gran cantidad de sufrimiento a los animales, pero <a href="https://www.nature.com/articles/s41586-018-0594-0.epdf?referrer_access_token=ho1eoiL3LRdv3FPGiRBcD9RgN0jAjWel9jnR3ZoTv0M2ZckU8PFAjFp2beHrcOXhHVPwiboHFjCLaVoEktOYmui1E_WbtCgUiW0GQ_D1Zsf9ikhdqzS9hAlJ2OYj63T9paYkbkbjbwbiMDtAsaaLIStmp4pVAf5k2WAYS1T_ySVFXQamjSmbZZOqgcbZWrei-uyC6A_FVQNeiIMKlcXtpL1l3TMhhDAAQ6xoEU683VobvCaltof2ZDm9A_x8ewm3UaNgfGWJBRg5RRTJSRnuMB4hvOnbZwNuDKrxhCeRw3cQjyhjTS8OGrNWaP3KdZud&amp;tracking_referrer=www.theguardian.com" target="_blank" rel="noopener">un nuevo estudio</a> indica que grandes reducciones en el consumo de carne a nivel mundial son esenciales si queremos salvar al planeta de las desastrosas consecuencias del cambio climático.

La actividad de la industria ganadera produce el 15% de los gases de efecto invernadero que se producen a nivel mundial y, además, implica un enorme derroche de recursos: para producir un kilo de carne de vaca (8 hamburguesas) necesitamos 16.000 litros de agua, es decir que tendríamos que beber un litro de agua por día durante 44 años para consumir esa misma cantidad de agua. Y de acuerdo con el Instituto Internacional para la Investigación Ganadera (ILRI, por sus siglas en inglés),<strong> el 50% de toda la superficie del planeta está ocupada para la producción de carne </strong>y otros productos animales.

Pero además del derroche recursos, deforestación y contaminación, la industria ganadera <strong>también es responsable de una pésima gestión de alimentos</strong> ya que, actualmente, el 40% de los cereales producidos en el mundo son destinados a alimentar a los animales criados para consumir carne en lugar de alimentar directamente con ellos a la humanidad.

<strong>«Hacer que el sector alimentario sea sostenible o devorar nuestro planeta: esto es lo que está en el menú de hoy».</strong>

Marco Springmann del <a href="https://www.oxfordmartin.ox.ac.uk/research/programmes/future-food" target="_blank" rel="noopener">Oxford Martin Program on the Future of Food </a>de la Universidad de Oxford y autor principal del estudio, afirma que él y el resto de los 23 investigadores  alrededor del mundo descubrieron que «adoptar más dietas saludables y basadas en vegetales a nivel mundial podría reducir las emisiones de gases de efecto invernadero del sistema alimentario en más de la mitad, y también reducir otros impactos ambientales, como los derivados de la aplicación de fertilizantes y el uso de tierras de cultivo y agua dulce, de una décima a una cuarta parte».
<h4></h4>
<h4 style="text-align: center;"><strong>¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</strong></h4>
<h4 style="text-align: center;"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;"><strong>¡Suscríbete gratuitamente a nuestro e-boletín! </strong></span></a></h4>
&nbsp;

El estudio ha visto la luz poco días después de que fuera publicado <a href="https://www.ipcc.ch/sr15/" target="_blank" rel="noopener">un informe </a>del Panel Intergubernamental sobre el Cambio Climático de la ONU (IPCC) en el que los científicos más destacados del mundo hicieran lo que han descrito como <strong>«un último llamado» para advertir que solo contamos con 12 años</strong> para evitar que el aumento de la temperatura global supere los 1,5 Cº. De sobrepasarse este límite, inclusive en medio grado, las consecuencias serían devastadoras: los riesgos de sequía, inundaciones y calor extremo se elevarían significativamente.

Según Springmann, <strong>no existen soluciones mágicas pero los cambios tecnológicos y de dietas son vitales</strong> y podrían complementarse con la reducción de la pérdida y el desperdicio de alimentos. De acuerdo con los investigadores, un cambio hacia una alimentación flexitariana ha sido necesario para mantener el límite por debajo de los 2 Cº y dicha dieta implica comer 75% menos carne de res, 90% de cerdo y la mitad de los huevos. Al mismo tiempo, es necesario aumentar al triple el consumo de frijoles y legumbres y cuatro veces el de nueces y semillas.

Los impactos de la actividad ganadera serán mucho peores si no se toman acciones contundentes en la medida en que la población mundial siga aumentando hasta alcanzar los 10.000 millones estimados para 2050. «Alimentar a una población mundial de 10 mil millones es posible, pero solo si cambiamos la forma en que comemos y la forma en que producimos alimentos. Ecologizar el sector alimentario o devorar nuestro planeta: esto es lo que está en el menú de hoy», afirmó el profesor Johan Rockström del Instituto Potsdam para la Investigación del Impacto Climático en Alemania y quien formó parte de la investigación.

&nbsp;

Fuente:

https://www.theguardian.com/environment/2018/oct/10/huge-reduction-in-meat-eating-essential-to-avoid-climate-breakdown

&nbsp;

&nbsp;
