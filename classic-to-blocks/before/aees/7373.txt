<p><img alt="" class="wp-image-10379" src="/app/uploads/2015/12/firmaschina.jpg" style="float:right; margin:10px; width:450px" />La organizaci&oacute;n internacional Igualdad Animal ha entregado&nbsp;este&nbsp;jueves 17 de diciembre a las 12 horas, en la Embajada de la Rep&uacute;blica Popular China en Madrid, 580.694 firmas pidiendo una ley de protecci&oacute;n animal en China que proh&iacute;ba el comercio de carne y piel de perros y gatos.</p>

<p>Esta entrega de firmas forma parte de la campa&ntilde;a internacional &quot;<a href="https://www.sinvoz.org/">Sin Voz</a>&quot; que puso en marcha la organizaci&oacute;n el 7 de Abril de 2013 y con la que ha presentado desde entonces cuatro investigaciones que han tenido repercusi&oacute;n internacional mostrando im&aacute;genes nunca vistas sobre este cruel comercio.<br />
<br />
En China todav&iacute;a no existe una ley de protecci&oacute;n animal, y la presi&oacute;n internacional, como aseguran los activistas chinos que tambi&eacute;n est&aacute;n colaborando en la campa&ntilde;a de Igualdad Animal, es&nbsp;muy importante para presionar al gobierno para que d&eacute; pasos firmes para acabar con esta crueldad.</p>

<p>&nbsp;</p>

<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/sMz1UjBGFu0" width="885px"></iframe></div>

<p>&nbsp;</p>

<p><br />
<img alt="" class="wp-image-13965" src="/app/uploads/2015/12/firmaschina2.jpg" style="float:right; margin:10px; width:450px" />&laquo;<em>La presi&oacute;n internacional es fundamental para acabar con el cruel comercio de carne y piel de perros y gatos en China. Estas 580. 000 firmas representan la voluntad de la comunidad internacional para que definitivamente haya una ley de protecci&oacute;n animal en China que acabe con el infierno que est&aacute;n viviendo los perros y gatos v&iacute;ctimas de este cruel comercio</em>&raquo;, indic&oacute; Javier Moreno, director internacional de Igualdad Animal.&nbsp;<br />
<br />
Para Amanda Romero, directora de Igualdad Animal en Espa&ntilde;a, &laquo;<em>los activistas chinos que est&aacute;n participando en la campa&ntilde;a internacional de Igualdad Animal hacen especial hincapi&eacute; en la importancia de la presi&oacute;n internacional para acabar con el sufrimiento de de 10 millones de perros y cuatro millones de gatos al a&ntilde;o en China. Seguiremos trabajando sin descanso hasta conseguir que esta atrocidad pase a formar parte del pasado</em>&raquo;.&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><sup><em>Fotograf&iacute;as: Javier Gamonal para Igualdad Animal&nbsp;</em></sup></p>

