<p><img alt="" class="wp-image-14224" src="/app/uploads/2015/03/repre.jpg" style="float:right; margin-left:5px; margin-right:5px; width:450px" />La empresa de la perrera de A Coru&ntilde;a reclama 60.000&euro; a activistas animalistas por cuestionar el modelo actual de funcionamiento.</p>

<p>Una decena de animalistas se exponen a una amenaza judicial por parte de la compa&ntilde;&iacute;a adjudicataria del servicio de lacer&iacute;a de A Coru&ntilde;a tras proponer diferentes medidas para mejorar el funcionamiento diario de las instalaciones.</p>

<p>La empresa reclama 60.000&euro; a los animalistas, una forma clara de presionar e intentar callar las voces cr&iacute;ticas con este modelo de perrera tristemente extendido en multitud de ciudades.</p>

<p>La Asociaci&oacute;n Animalista Libera <strong><a href="https://www.change.org/p/basta-de-amenazas-por-defender-a-los-animales-soyanimalista">ha iniciado una campa&ntilde;a online </a></strong>de recogida de firmas para mostrar apoyo a los animalistas y denunciar el caso.</p>

