<p><img alt="" class="wp-image-13891" src="/app/uploads/2014/11/circo1.jpg" style="width:880px" /></p>

<p>Los circos que se presenten en el muninicipio de Tonal&aacute;n no podr&aacute;n utilizar animales. El ayuntamiento aprob&oacute; ayer una iniciativa presentada por el regidor Salvador Paredes para prohibir el uso de animales en circos.</p>

<p>Tonal&aacute; se ha unido a Guadalajara, Zapopan y Tlajomulco de Z&uacute;&ntilde;iga como los municipios de Jalisco que no permiten la instalaci&oacute;n de circos con animales.</p>

<p>El pasado 20 de noviembre, en la sesi&oacute;n de Comisi&oacute;n de Puntos Constitucionales en el Congreso del Estado de Jalisco, fue aprobada la iniciativa de ley para prohibir el uso de animales en circos en el estado presentada por el diputado Gildardo Guerrero y elaborada y sustentada con documentaciones de la asociaci&oacute;n internacional, Igualdad Animal, en m&aacute;s de 11 circos dentro del estado, con el apoyo de organizaciones locales como Adopta Guadalajara, Justicia y Dignidad Anima A.C., Red Pro Gato y Colectivo Animalia.</p>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-13892" src="/app/uploads/2014/11/circo5.jpg" style="float:right; margin:5px; width:300px" /></p>

<p>Precisamente el pasado s&aacute;bado 29 de noviembre un activista de Igualdad Animal M&eacute;xico fue agredido por un trabajador del Circo Kenia instalado en Tonal&aacute;n, en el transcuro de una protesta pac&iacute;fica de la organizaci&oacute;n.<br />
<br />
La polic&iacute;a y una ambulancia se desplazaron hasta el lugar, mientras el agresor se escondi&oacute; dentro del Circo Kenia. Cuando se requiri&oacute; su presencia para tomar declaraci&oacute;n se dio a la fuga. Tras ser atendido en la ambulancia, el activista recobr&oacute; completamente la consciencia y se recuper&oacute;. Igualdad Animal M&eacute;xico ha denunciado lo sucedido y espera que se localice al agresor para que lo sucedido no quede impune.<br />
<br />
&nbsp;</p>

<p>&nbsp;</p>

<div class="media_embed" height="495px" width="880px"><iframe allowfullscreen="" frameborder="0" height="495px" src="https://www.youtube.com/embed/rIzClwEqLZo" width="880px"></iframe></div>

<p>&nbsp;</p>

<p><br />
&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

