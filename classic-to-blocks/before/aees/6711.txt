<p>Los hechos sucedieron en la ma&ntilde;ana del pasado viernes, cuando el <strong><a href="http://www.igualdadanimal.org/entretenimiento/circos" target="_blank">circo </a>Rinaldo Orfe </strong>lleg&oacute; a la ciudad de Imola cargado de animales que exhibir y a los que <strong>obligar a realizar absurdos trucos como parte de su espect&aacute;culo</strong>. Fue entonces cuando una joven jirafa logr&oacute; escapar y emprendi&oacute; la fuga por las calles de la ciudad, entre el caos de las bocinas de los coches y los gritos de la gente, rodeada de cemento y asfalto, aturdida y sin saber hacia d&oacute;nde dirigirse.</p>
<p>La huida, que comenz&oacute; a las 9 de la ma&ntilde;ana, termin&oacute; a las tres de la tarde cuando, tras una intensa persecuci&oacute;n, un &nbsp;polic&iacute;a provincial alcanz&oacute; a la jirafa con un dardo tranquilizante.</p>
<p><img alt="" class="wp-image-14082" src="/app/uploads/2012/09/jirafa_huida_imola.jpg" style="width: 580px; float: left; " /></p>
<p>&nbsp;</p>
<p>Sin embargo, la jirafa no regres&oacute; al circo. Muerta de miedo por la estresante situaci&oacute;n vivida, <strong>cay&oacute; al suelo de un paro card&iacute;aco</strong>. Su coraz&oacute;n no puedo soportar la anestesia, ni el miedo y el sufrimiento que su fallida huida le provoc&oacute;.</p>
<p><img alt="" class="wp-image-14080" src="/app/uploads/2012/09/jirafa_circo_caida.jpg" style="width: 580px; " /></p>
<p>&nbsp;</p>
<p><strong>&laquo;<em>El circo tiene irse, no puede permanecer en la ciudad despu&eacute;s de esta demostraci&oacute;n de negligencia y &nbsp;superficialidad&raquo;, </em>afirm&oacute; el alcalde</strong> de Imola, que asegur&oacute; que se tomar&aacute;n medidas para prevenir la instalaci&oacute;n de circos con animales en el futuro.</p>
<p><img alt="" class="wp-image-14081" src="/app/uploads/2012/09/jirafa_circo_rinaldo.jpg" style="width: 580px; " /></p>
<p>&nbsp;</p>
<p>Esperamos que las palabras del alcalde de Imola se conviertan en hechos, tanto en esta como en cualquier otra ciudad en la que los animales sean utilizados para divertimento de los seres humanos.</p>

