<div class="center"><img class="wp-image-12050" src="/app/uploads/2017/05/22525327724_f1c25cc2e9_z.jpg" /></div>
<p>En todo el mundo <strong>la industria del huevo mata cada a&ntilde;o a millones de pollitos macho porque no ponen huevos y tampoco sirven para carne</strong>. A las pocas horas de nacer son triturados vivos, gaseados o aplastados con mazos dentro de contenedores.</p>

<p>Esta incre&iacute;ble crueldad podr&iacute;a acabar gracias a los avances logrados por cient&iacute;ficos alemanes que han dado con <strong>un m&eacute;todo que permitir&aacute; determinar el sexo de las aves durante la incubaci&oacute;n</strong> y as&iacute; dejar que solo nazcan las polluelas. &nbsp;</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2016/06/13/la-industria-del-huevo-de-ee-uu-dejara-de-triturar-vivos-millones-de-pollitos-macho/" target="_blank">La industria del huevo de EE. UU. dejar&aacute; de triturar vivos a millones de pollitos macho.</a></strong>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-12051" src="/app/uploads/2017/05/22757853629_1baf671519_z_1.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&laquo;No hay que lastimar al embri&oacute;n&raquo;, comenta la experta. &laquo;El huevo proviene directamente de la incubadora que le dio calor durante 72 horas. Bajo la luz se ven las delgadas venas y la delicada estructura del coraz&oacute;n&raquo;, agrega.</p>

<p>Actualmente, las pruebas se est&aacute;n realizando a mano pero se espera que pr&oacute;ximamente las m&aacute;quinas se hagan cargo de determinar el sexo.</p>

<p>Una vez que se hace un agujero en el huevo este se coloca en una m&aacute;quina donde un l&aacute;ser lo ilumina. El procedimiento permite visualizar estructuras moleculares especiales en la sangre. Al cabo de dos segundos, el sistema diagrama una curva que revela el sexo del embri&oacute;n: la curva azul si es masculino y la curva roja si es femenino.</p>

<p>&nbsp;</p>

<p>El hecho de que el embri&oacute;n sea masculino o femenino es decisivo, ya que <strong>gracias a este m&eacute;todo se evitar&aacute; la matanza de millones de pollitos que habr&iacute;an sido matados el mismo d&iacute;a de su nacimiento solamente por ser machos.</strong></p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Puedes suscribirte gratuitamente a nuestro e-bolet&iacute;n</a> para que recibas las mejores noticias de actualidad sobre los animales y conozcas m&aacute;s opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p><img alt="" class="wp-image-12052" src="/app/uploads/2017/05/kuken2_1.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Los c&aacute;lculos cient&iacute;ficos para la determinaci&oacute;n del sexo de los polluelos a trav&eacute;s de este m&eacute;todo revelan una exactitud del 95%. En el futuro se planea escanear diez mil huevos por d&iacute;a y se estima que el procedimiento estar&aacute; listo para ser aplicado a gran escala dentro de tres o cuatro a&ntilde;os.</p>

<p>Los proyectos de esta &aacute;rea &nbsp;son subvencionados por el gobierno alem&aacute;n. El titular de la cartera de Agricultura, Christian Schmidt, apuesta por la tecnolog&iacute;a de determinaci&oacute;n del sexo de embriones dentro del huevo para &ldquo;iniciar el fin&rdquo; de la trituraci&oacute;n de polluelos.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://lajornadanet.com/index.php/2017/05/10/cientificos-alemanes-determinan-el-sexo-de-polluelos-dentro-del-huevo/#.WSQlPlJDmRv" target="_blank">https://lajornadanet.com/index.php/2017/05/10/cientificos-alemanes-determinan-el-sexo-de-polluelos-dentro-del-huevo/#.WSQlPlJDmRv</a></p>

