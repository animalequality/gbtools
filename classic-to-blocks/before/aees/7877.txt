<div class="center"><img class="wp-image-12340" src="/app/uploads/2017/08/shutterstock_693458419.jpg" /></div>
<p>&iexcl;Eres incre&iacute;ble!</p>

<p>Sabemos que lees estas l&iacute;neas porque te importan los animales. &nbsp;</p>

<p>Ya sea que seas vegano o que hayas decidido comenzar a reducir tu consumo de carne, queremos que sepas que <strong>en M&eacute;xico la comida vegana es f&aacute;cil de encontrar en las calles y de preparar en casa.</strong></p>

<p>Con esta lista que te hemos preparado, no te perder&aacute;s nada de lo que nuestro pa&iacute;s ofrece para quienes han decidido comer por un mundo m&aacute;s compasivo, sostenible y saludable.</p>

<p>&nbsp;</p>

<strong>1. Puedes encontrar deliciosas alternativas a la carne y l&aacute;cteos en supermercados y tiendas online</strong>

<p>Diversos supermercados y ecotiendas en M&eacute;xico siguen incorporando m&aacute;s productos vegetales en sus estanter&iacute;as. Y si quieres comprar online, <a href="https://www.veganlabel.mx/" target="_blank">Vegan Label</a>, <a href="https://www.mrtofu.com.mx/" target="_blank">Mr. Tofu</a>, <a href="https://www.eljardinvegano.com/retail/" target="_blank">El jard&iacute;n Vegano</a> y <a href="https://tierravegana.com/" target="_blank">Tierra Vegana</a> son algunas de las tiendas que te ofrecen productos tanto importados como hechos en M&eacute;xico.</p>

<p><img alt="" class="wp-image-12341" src="/app/uploads/2017/08/deshebrada_carne_soi-yah.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>Carne deshebrada para tacos, tortas, enchiladas o chilaquiles de la marca &laquo;Soi-Yah!&raquo;.</p></FONT>

<p>&nbsp;</p>

<strong>2. M&eacute;xico es el pa&iacute;s con m&aacute;s vegetarianos en Latinoam&eacute;rica</strong>

<p>&iexcl;As&iacute; como lo escuchas!</p>

<p>Y, mejorando lo anterior, a diferencia de otros pa&iacute;ses de Latinoam&eacute;rica, es en M&eacute;xico donde m&aacute;s personas han declarado seguir esta dieta por convicciones personales.</p>

<p>Seg&uacute;n una encuesta del Gabinete de Comunicaci&oacute;n Estrat&eacute;gica, <strong>un 36% de los mexicanos que dejaron de comer carne lo hicieron por respeto a los animales.</strong></p>

<p>&iexcl;A que te hemos dejado s&uacute;per motivado!! :)</p>

<p><iframe allowfullscreen="" class="giphy-embed" frameborder="0" height="266" src="https://giphy.com/embed/lqruErDlkrWW4" width="480"></iframe></p>

<FONT SIZE=1><FONT COLOR=#808080><p><a href="https://giphy.com/gifs/chicken-lqruErDlkrWW4" target="_blank">via GIPHY</a></p></FONT>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="https://descubrirlacomida.com/" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a>, recibe los mejores tips sobre alimentaci&oacute;n vegetariana y descarga un recetario vegetariano gratis.</p></FONT>

<p>&nbsp;</p>

<strong>3. S&oacute;lo en la Ciudad de M&eacute;xico hay al menos 20 restaurantes vegetarianos y veganos</strong>

<p>&iexcl;Por supuesto que tienes que saber d&oacute;nde puedes probar deliciosos platos vegetarianos fuera de casa!</p>

<p>Por eso, entre los muchos que hay, hemos seleccionado para ti <a href="https://igualdadanimal.org/noticia/2017/04/05/10-excelentes-restaurantes-con-opciones-100-vegetales-en-ciudad-de-mexico/" target="_blank">10 excelentes restaurantes que ofrecen opciones vegetales y que se encuentran en Ciudad de M&eacute;xico</a>. As&iacute; que &iexcl;sal a probar!</p>

<p><img alt="" class="wp-image-12342" src="/app/uploads/2017/08/vegan_inc_mexico_restaurant.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>En Vegan Inc. el lema es: &nbsp;&laquo;hacemos que lo saludable tenga un sabor delicioso&raquo;.</p></FONT>

<p>&nbsp;</p>

<strong>4. En M&eacute;xico t&uacute; puedes ser un Protector de Animales desde tu computadora o celular</strong>

<p>Realizando peque&ntilde;as acciones puedes lograr importantes victorias para los animales.</p>

<p>Env&iacute;a correos, difunde informaci&oacute;n y, adem&aacute;s, forma parte del equipo de Igualdad Animal para hacer frente a la empresas que m&aacute;s maltratan a los animales. Ingresa a <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank">Protectores Animales M&eacute;xico</a> y cambia su destino. &nbsp;&nbsp;</p>

<p><iframe allowfullscreen="" class="giphy-embed" frameborder="0" height="270" src="https://giphy.com/embed/WlH9MCf0q0h1u" width="480"></iframe></p>

<p><a href="https://giphy.com/gifs/love-animal-amor-WlH9MCf0q0h1u" target="_blank">via GIPHY</a></p>


<strong>5. Hay miles de recetas vegetarianas de cocina mexicana en internet</strong>

<p>Nuestra cocina es una de las m&aacute;s populares del mundo. Gracias a ello en la web puedes encontrar una impresionante cantidad de recetas de platos mexicanos en sus versiones vegetarianas.</p>

<p>La creatividad de los chefs de <a href="https://www.youtube.com/channel/UC_XE7-_GigBlY_s4N1__Jsw" target="_blank">Comer Vegano</a>, <a href="https://www.youtube.com/channel/UCbn_IwWoaqYfy-RwYWlDF-A" target="_blank">Vida Vegana</a>, <a href="https://www.youtube.com/channel/UCrN5l3YrBp8CiYNv7_xf2AA" target="_blank">Cocina Vegana F&aacute;cil</a>, <a href="https://www.youtube.com/playlist?list=PLVBPGFauNs7MK3t_lfYWU05NOsVxaLf63" target="_blank">Vegan Booty</a> y <a href="https://danzadefogones.com/" target="_blank">Danza De Fogones</a> te impresionar&aacute;.</p>

<p><img alt="" class="wp-image-12343" src="/app/uploads/2017/08/quesadillas-veganas-6_1.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>Tacos Al Estilo Baja California de Comer Vegano.</p></FONT>

