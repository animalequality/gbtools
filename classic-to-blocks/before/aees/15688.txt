Uno de los sistemas de producción <strong>que causa mayor sufrimiento a los animales</strong> es el uso de jaulas en la industria del huevo. Las gallinas pasan toda su vida encerradas junto a otras 8 gallinas en un espacio destinado para cada una que es del tamaño de un ipad.

<strong>Te gustará: <a href="https://igualdadanimal.mx/blog/no-lo-sabias-pero-las-gallinas-son-en-realidad-como-unos-gatos-o-perros-emplumados-muy/" target="_blank" rel="noopener">No lo sabías pero las gallinas son en realidad como unos gatos o perros emplumados muy inteligentes</a></strong>

Los frágiles cuerpos de estos inteligentes y sensibles animales <strong>no soportan la violencia que ejercen sobre ellos día tras día.</strong> Cuando su producción de huevos desciende son enviadas al matadero. Dentro de la industria no existe ninguna consideración hacia ellas, son consideradas meras mercancías y tratadas como tal.
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación?</h4>
<h4 style="text-align: center;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
Igualdad Animal ya estamos logrando alcanzar un significativo objetivo: acabar con las jaulas. En Italia, España, Brasil y México hemos podido convencer a algunas de las más importantes empresas para que rechacen la crueldad del sistema de jaulas y el maltrato hacia las gallinas. Algunas de ellas son: Barilla, Dusmann, Gemos, Esselunga, Auchan, Lidl, Grupo Anderson´s, Pagasa, Carrefour, Pacific Star Foodservice y El Corte Inglés.

&nbsp;

<img class="alignnone wp-image-13165" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " src="/app/uploads/2018/06/31948921743_61619dbc1c_z_2.jpg" alt="" width="640" height="357" />

Desde Igualdad Animal <strong>vamos a seguir trabajando sin tregua</strong> hasta que las jaulas queden en el pasado. Y para ello seguiremos también mostrando esta terrible realidad a más consumidores.

Aunque la eliminación de jaulas no implica la eliminación del maltrato, sin duda, sí significa un importante progreso en cuanto a que es un primer paso hacia la reducción del sufrimiento de los animales de granja.Tú puedes hacer mucho más para ayudar a las gallinas si sustituyes los huevos en tu alimentación por alternativas a su consumo.  Es muy fácil hacerlo.

<a href="http://www.igualdadanimal.org/noticias/7713/6-recetas-para-ayudarte-sustituir-los-huevos-en-tu-alimentacion" target="_blank" rel="noopener">¡Aquí te explicamos cómo!</a>

&nbsp;

Investigación de Igualdad Animal en granjas de España.
