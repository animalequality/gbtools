<div class="center"><img class="wp-image-12538" src="/app/uploads/2017/10/26markpost0708.jpg" /></div>
<p>Mark Post, el cient&iacute;fico art&iacute;fice de la primera hamburguesa presentada en 2013 a partir de &laquo;carne limpia&raquo; o carne elaborada a partir de c&eacute;lulas, considera &nbsp;que el actual sistema de producci&oacute;n de carne cambiar&aacute; profundamente o desaparecer&aacute;. <strong>La ganader&iacute;a industrial es una pr&aacute;ctica obsoleta, responsable del mayor maltrato animal en la historia y destructora del medioambiente</strong> que, adem&aacute;s, no podr&aacute; satisfacer la demanda mundial de carne en las pr&oacute;ximas cuatro d&eacute;cadas.</p>

<p><img alt="" class="wp-image-12539" src="/app/uploads/2017/10/36372759453_a9ffd1f276_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>


<strong>Noticia relacionada: <a href="https://www.lavanguardia.com/comer/al-dia/20170828/43884301610/bill-gates-invierte-en-la-industria-de-la-carne-limpia.html" target="_blank">Bill Gates invierte en la industria de la &laquo;carne limpia&raquo;</a></strong>

<p>&nbsp;</p>

<p>&laquo;La mayor&iacute;a de las personas desconocen que la producci&oacute;n c&aacute;rnica actual est&aacute; llegando a su l&iacute;mite y no podr&aacute; atender la demanda creciente en las pr&oacute;ximas cuatro d&eacute;cadas. Necesitamos encontrar una alternativa, y esa alternativa podr&iacute;a ser esta: una producci&oacute;n &eacute;tica y respetuosa con el medioambiente&raquo;. As&iacute; lo afirm&oacute; Post en un video que se transmiti&oacute; en el congreso Crea Tech, organizado por la Asociaci&oacute;n Argentina de Consorcios Regionales de Experimentaci&oacute;n Agr&iacute;cola en C&oacute;rdoba, Argentina.</p>

<p>&nbsp;</p>

<FONT SIZE=1><FONT COLOR=#808080><p>La industria de la carne de pollo en India. Investigaci&oacute;n de Igualdad Animal.</p></FONT>


<strong>La soluci&oacute;n a la mayor amenaza de nuestro tiempo: el cambio clim&aacute;tico</strong>

<p>&nbsp;</p>

<p>La &laquo;carne limpia&raquo; podr&iacute;a ser la receta que salve al planeta al lograr revolucionar un devastador sistema de producci&oacute;n que <strong>le cuesta al planeta nada m&aacute;s y nada menos que el 14,5% de las emisiones totales de gas de efecto invernadero</strong> y un desmesurado derroche de recursos. En contraste brutal, la &laquo;carne limpia&raquo; disminuye en un 90% el mon&oacute;xido de carbono producido por la industria ganadera y en un 99% el uso de tierras para cultivo y pastoreo. &nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>Seg&uacute;n Post, <strong>en tres o cuatro a&ntilde;os este producto podr&iacute;a llegar a los restaurantes y en siete a las estanter&iacute;as de los supermercados. </strong>El cient&iacute;fico tambi&eacute;n considera que la &laquo;carne limpia&raquo; dar&aacute; la oportunidad a que lo productores migren a una nueva forma de producir o de la ganader&iacute;a a la agricultura.</p>

<p>Ya existen en el mundo 8 empresas dedicadas al desarrollo de la tecnolog&iacute;a para producir &laquo;carne limpia&raquo; e <strong>inversores de renombre siguen volcando su inter&eacute;s en ella</strong>, conscientes de que es, sin duda, la soluci&oacute;n a los mayores desaf&iacute;os actuales: alimentar a &nbsp;9.700 millones de personas en 2050 y luchar contra el cambio clim&aacute;tico.</p>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://www.lanacion.com.ar/economia/campo/un-experto-que-creo-la-carne-cultivada-desafia-a-las-vacas-gran-parte-de-la-produccion-cambiara-o-desaparecera-nid2071615/" target="_blank">https://www.lanacion.com.ar/economia/campo/un-experto-que-creo-la-carne-cultivada-desafia-a-las-vacas-gran-parte-de-la-produccion-cambiara-o-desaparecera-nid2071615/</a></p>

<p><a href="http://www.agrositio.com/vertext/vertext.php?id=189792&amp;se=1000" target="_blank">http://www.agrositio.com/vertext/vertext.php?id=189792&amp;se=1000</a></p>

