Peraleda de la Mata, Cáceres - Un vecino de este pueblo ha sido denunciado por amputar la pierna a Zizú, un perro, con una segadora de hierba.

Según los denunciantes, Zizú se encontraba atado dentro de la parcela, y aún se puede ver en la finca propiedad del agresor la pierna.
