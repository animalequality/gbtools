<div class="center"><img class="wp-image-10188" src="/app/uploads/2014/12/gadiaereo.jpg"></div>
<img class="wp-image-13989" style="float: right; margin: 5px; width: 400px;" src="/app/uploads/2014/12/gadiae.jpg" alt="">La organización internacional Igualdad Animal ha documentado en primera persona la matanza de animales en el Festival de Gadhimai, el mayor sacrificio religioso de animales del mundo, que se celebra estos días en el distrito Bara al sur de Nepal.
&nbsp;
<strong>Investigadores de Igualdad Animal han utilizado un dron para conseguir planos aéreos inéditos de la matanza, obteniendo una nueva perspectiva de la masacre de animales llevada a cabo en el ritual</strong>. Han presenciado cómo búfalos de agua, cabras, corderos y palomas han sido matados brutalmente, acuchillados hasta la muerte por una violenta multitud. El Gobierno de Nepal había emitido una orden prohibiendo que se grabase o fotografiase el festival. [1]
&nbsp;
Nuestro equipo también documentó las terribles condiciones antihigiénicas, a niños siendo testigos de la matanza y la venta y consumo de una gran cantidad de alcohol durante el festival.
<img class="wp-image-14268" style="float: left; margin: 10px 5px; width: 400px;" src="/app/uploads/2014/12/solagadi.jpg" alt="">&nbsp;
Este festival, de un mes de duración, se celebra cada cinco años como sacrificio a la diosa hindú Gadhimai, y los participantes creen que matar a estos animales les traerá salud y prosperidad.
&nbsp;
Igualdad Animal ya había conseguido que el Gobierno de India aprobase una orden para bloquear el transporte de animales en la frontera de India con Nepal [2], y desde entonces equipos de voluntarios de la organización han estado trabajando con oficiales para que se cumpliera la orden estos días, salvando a miles de animales [3]. <strong>Gracias a esto, el número de búfalos sacrificados este año ha descendido un 69% respecto a la pasada celebración de 2009</strong>.
&nbsp;

Durante el último mes voluntarios de Igualdad Animal han estado realizando una campaña educativa con los aldeanos y entrevistándose con sacerdotes del Templo de Gadhimai animándoles a llevar a cabo el ritual sin matar animales, poniendo el ejemplo de otros rituales similares donde hay ofrendas vegetales. [4] &nbsp;
&nbsp;
<div class="ae-video-container media_embed"><iframe src="https://www.youtube.com/embed/d9nBXm5Acqw" width="880px" height="495px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
“<em>Es una auténtica locura. La sangre llega hasta los tobillos. Miles de animales masacrados en nombre de la religión. Desde Igualdad Animal respetamos la libertad de credo, pero creemos que la tradición y la religión no pueden justificar causar daño a los animales. La presión internacional cada vez es más fuerte y esperamos que este año sea él último que se realiza esta masacre. Seguiremos trabajando sin descanso para que así sea.</em>” Indicó Amruta Ubale, coordinadora de Igualdad Animal en India.

La investigación ha tenido una gran repercusión mediática, apareiendo en medios nacionales e internacionales, como <a href="https://video.corriere.it/nepal-mattatoio-cielo-aperto-festival-hindu-video-drone/83cd5a10-795d-11e4-abc3-1c132dc377f5">El Corrierre de la Sera</a> en Italia, <a href="https://www.vice.com/de/article/7bq4db/das-blut-reicht-bis-zu-den-knoecheln-opferfest-gadhimai-948">Vice Alemania</a> o <a href="https://www.elmundo.es/el-mundo-tv/2014/12/01/547c5ec2268e3e2c718b4574.html">El Mundo. </a>
&nbsp;
<div class="ae-video-container media_embed"><iframe src="https://player.vimeo.com/video/113376703?title=0&amp;byline=0&amp;portrait=0" width="880px" height="495px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
Igualdad Animal es una organización internacional de defensa de los animales presente en España, Inglaterra, Estados Unidos, Italia, México, Alemania, Venezuela e India que cuenta con más de un millón de simpatizantes y que trabaja a través de la sensibilización, concienciación e investigación con el objetivo de promover cambios en la sociedad y en las leyes que sean favorables a los animales.
&nbsp;
[1]<a href="http://www.karobardaily.com/news/2014/11/brits-lobby-against-visiting-nepal-due-to-gadhimai" class="break-all">http://www.karobardaily.com/news/2014/11/brits-lobby-against-visiting-nepal-due-to-gadhimai</a>
&nbsp;
[2]<a href="https://www.publico.es/actualidad/india-prohibe-transporte-animales-territorio.html" class="break-all">https://www.publico.es/actualidad/india-prohibe-transporte-animales-territorio.html</a>
&nbsp;
[3]<a href="https://igualdadanimal.org/noticia/2014/11/27/escasas-horas-para-el-mayor-sacrificio-animal-del-mundo/" class="break-all">https://igualdadanimal.org/noticia/2014/11/27/escasas-horas-para-el-mayor-sacrificio-animal-del-mundo/</a>
&nbsp;
[4] <a href="https://www.youtube.com/watch?v=vJTeaB2yX58" class="break-all">https://www.youtube.com/watch?v=vJTeaB2yX58</a>
&nbsp;

<a href="http://www.igualdadanimal.org/colabora/donativos.php"><img class="wp-image-13963" src="/app/uploads/2014/12/fb_donaciones.jpg" alt=""></a>
