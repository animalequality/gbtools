<img class="wp-image-10172" style="width: 640px; float: left; margin: 5px 10px;" src="/app/uploads/2014/09/conejos2.jpg" alt="">Más de 100.000 personas han firmado ya la petición de la campaña <a href="http://stopmodacruel.org/">“Stop Moda Cruel</a>” de la organización internacional Igualdad Animal instando a grandes firmas de moda&nbsp; a que dejen de vender pieles.

Desde que se iniciara la campaña el pasado jueves 4 de septiembre, ésta no para de crecer, e incluso celebridades como <a href="https://igualdadanimal.org/noticia/2014/09/12/alicia-silverstone-se-suma-la-campana-stop-moda-cruel/">Alicia Silverstone se han sumado a la misma </a>pidiendo a estas firmas que dejen de vender pieles.

Después de llevar a cabo una investigación encubierta de dos años de duración en granjas de conejos de España, la organización internacional Igualdad Animal ha descubierto que numerosos diseñadores compran pieles obtenidas a base de métodos crueles y salvajes y lanza ahora una campaña mundial para instar a estas firmas a que dejen de vender prendas fabricadas en piel.

Igualdad Animal ha creado una página web <a href="https://stopmodacruel.org/">www.StopModaCruel.org</a> así como un<a href="https://www.youtube.com/watch?v=vEG-Eydunkg"> vídeo con imágenes inéditas </a>que muestra la brutalidad que se ejerce en estas granjas y declaraciones de Francisco Cuberes, responsable de la curtidora Curticub, y de Lidia Nogue, de la empresa Galaico Catalana, en la que manifiestan que venden pieles de conejo a grandes marcas de moda.

Asimismo, ha publicado una petición para enviar a las compañías implicadas, entre las que se encuentran Burberry, Dior, Armani, Yves Saint Laurent, Louis Vuitton, Marc Jacobs, y Diane Von Furstenberg.

Los investigadores han documentado un maltrato muy extendido en las granjas, como por ejemplo granjeros golpeando a conejos enfermos hasta la muerte delante de sus aterrados compañeros de jaula. A otros conejos lisiados, enfermos o con graves heridas se les dejaba sufrir sin proporcionarles tratamiento médico.

“<em>Más de 100.000 personas han firmado ya nuestra petición pidiendo a estas grandes firmas que dejen de vender pieles. Es un mensaje muy claro, en pleno siglo XXI tienen que apostar por una moda sin crueldad animal. Resulta paradójico que un mundo que hace alarde de estar a la vanguardia en este tema sigan tan anclados en el pasado</em>". Manifestó Javier Moreno, coordinador internacional de Igualdad Animal

&nbsp;
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/vEG-Eydunkg" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
