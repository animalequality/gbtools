La Comisión de Ética del Congreso peruano acordó sancionar con tan sólo 100 días de suspensión de empleo y sueldo al diputado Miró Ruiz, quien había matado a balazos a un perro de nombre Matías.

Por mayoría, el grupo de trabajo resolvió que Ruiz “es responsable”, no sólo por disparar contra el can desde su domicilio en Chaclacayo, sino por mentir, esconderse y denunciar sin pruebas una falsa cortina de humo del gobierno, faltando al Código de Ética del Congreso.

Ruiz también tendrá que afrontar responsabilidades penales ante la Subcomisión de Acusaciones Constitucionales, a sugerencia de la comisión.

Sin embargo, la sanción interpuesta no se debe al asesinato del perro, sino a los delitos de falsedad en la declaración, y uso de armas de fuego sin licencia, tal y como señala el informe final: “La Comisión de Ética considera que estos hechos configurarían los delitos de falsedad genérica, previsto en el artículo 348 del Código Penal -por negar primero su responsabilidad y luego cambiar de versión-, y uso de armas de fuego sin licencia”.

La Comisión incluso justifica el asesinato del perro, al aceptar el argumento de Miró Ruiz, quien defendió que su actuación se debió a su "cosmovisión andina", según la cual estaría justificado asesinar a los animales que entran en granjas y lugares de explotación de aves. Este razonamiento es una idea equivocada, pues todos los animales experimentan sensaciones y tienen consciencia, y ello les hace merecedores de respeto.
