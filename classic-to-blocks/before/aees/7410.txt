<div class="center"><img class="wp-image-10417" src="/app/uploads/2016/01/girlvr1.jpg" /></div>
<p><img alt="" src="https://animalequality.de/app/uploads/2016/01/sundance_girl.jpg" style="margin:10px 20px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Igualdad Animal ha unido fuerzas con la compa&ntilde;&iacute;a de tecnolog&iacute;a&nbsp;<a href="http://conditionone.com/">Condici&oacute;n One</a>&nbsp;para crear una experiencia&nbsp;revolucionaria en la protecci&oacute;n de&nbsp;los animales a trav&eacute;s del uso de la realidad virtual que ha sido presentada&nbsp;en el Festival de Cine de Sundance 2016 en Park City, Utah. &quot;<em>Factory Farm</em>&quot; es el nombre del corto documental que utilizando la tecnolog&iacute;a inmersiva de&nbsp;360 ​​grados y grabado&nbsp;en&nbsp;3D, sumerge a&nbsp;los espectadores en&nbsp;un mundo completamente nuevo que muestra la crueldad de las granjas industriales y mataderos como nunca antes se habia visto. La pel&iacute;cula&nbsp;de 12 minutos deja sin&nbsp;aliento al espectador y queda grabada para siempre como una experiencia profunda e inolvidable.</p>

<p>El cofundador y director de investigaciones de Igualdad Animal, Jos&eacute; Valle, gu&iacute;a al espectador en un viaje a trav&eacute;s de una granja de cerdos y mataderos. Esta es una&nbsp;oportunidad &uacute;nica de experimentar en primera persona la realidad que oculta la industria c&aacute;rnica a los consumidores.&nbsp;</p>

<p><img alt="" src="https://animalequality.de/app/uploads/2016/01/sundance_vierer.jpg" style="margin:10px 20px" /></p>

<p><br />
Este a&ntilde;o la realidad virtual est&aacute; teniendo un gran impacto en el Festival de Sundance, y&nbsp;los proyectos, aunque&nbsp;participan fuera de concurso, est&aacute;n siendo&nbsp;recibidos con gran expectaci&oacute;n. Distintos periodistas&nbsp;estadounidenses e internacionales de medios como&nbsp;The New&nbsp;York Times,&nbsp;LA Times,&nbsp;The Guardian y&nbsp;Democracy Now&nbsp;y&nbsp;conocidos actores como Diego Luna o la popular cantante&nbsp;Dawn Richard han mostrado gran&nbsp;entusiasmo e inter&eacute;s al experimentar&nbsp;este revolucionario proyecto de Igualdad Animal y Condition One.&nbsp;<br />
<br />
Igualdad Animal presentar&aacute; internacionalmente&nbsp;en los pr&oacute;ximos meses &eacute;ste y otros proyectos de realidad virtual que est&aacute; desarrollando.&nbsp;<br />
<br />
&laquo;<em>He investigado durante m&aacute;s de 13&nbsp;a&ntilde;os granjas y mataderos y siempre sol&iacute;a pensar &#39;ojal&aacute; pudiera conseguir que la gente experimentase esto en primera persona&#39;. Ahora, gracias a la realidad virtual, puedo</em>&raquo;, indic&oacute; Jose Valle, cofundador y director de investigaciones de Igualdad Animal.&nbsp;<br />
<br />
Javier Moreno, director internacional de Igualdad Animal, indica que &laquo;<em>hemos creado una experiencia pionera y revolucionaria, en la que trasladamos literalmente a lo espectadores dentro de las granjas y mataderos. Mostramos la crueldad de la industria c&aacute;rnica como nunca antes se hab&iacute;a hecho</em>&raquo;.</p>

<p>&nbsp;</p>

<div class="media_embed" height="315px" width="560px"><iframe allowfullscreen="" frameborder="0" height="315px" src="https://www.youtube.com/embed/mUyvbR2uW9U" width="560px"></iframe></div>

<p>&nbsp;</p>

