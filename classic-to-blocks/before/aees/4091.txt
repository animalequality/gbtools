Caracas, 09 Feb. ABN.- Convertir a Venezuela en una potencia en la explotación de vacas para obtener leche fue el planteamiento hecho por el presidente de Venezuela, Hugo Chávez, este sábado desde el estado Barinas, en la Finca La Gloria.

Tomó entre sus manos una propuesta de creación y reimpulso de la gran Cuenca Lechera del pie de monte andino, realizada por la Federación Bolivarianas de Ganaderos y Agricultores de Venezuela (Fegaven), y el jefe de Estado venezolano replanteó el plan: “podríamos hacer una macro cuenca, con al menos cuatro cuencas”.

“El año pasado la producción de leche se incrementó bastante: a 1.700 millones de litros de leche de 1.400 millones, este año debe estar por más de 2.000 millones”, expresó Chávez.

“No hay que perder un día en esto para convertir a Venezuela en una 'potencia lechera' en el continente, nosotros tenemos cómo hacerlo, Venezuela va a ser una potencia integrada, pero en este caso me refiero a la leche y carne”, aseveró Chávez.


Creación del Fondo Nacional para la Producción Lechera

Por otra parte, el presidente Hugo Chávez, anunció la creación del Fondo Nacional para la Producción Lechera, el cual permitirá la creación de redes productivas a través de créditos integrales a 6 por ciento de interés.

“Estos créditos integrales atenderán todo el problema (...) para adquisición de 'nuevos vientres', construcción o rehabilitación de pequeños y medianos sistemas de riegos, adquisición de insumos para infraestructura, potreros, cercas eléctricas, ordeño mecánico, adquisición de semillas de pasto, de tanques de enfriamiento, de transporte frío, entre otros son lo problemas a atender”, explicó el jefe de Estado.

Asimismo, indicó que el Fondo tendrá un registro de 'producción lechera nacional', para que el Ejecutivo sepa exactamente cual es la extracción de leche obtenida, además de una política de subsidios más agresiva.

En cuánto al Fondo de Desarrollo, Agropecuario, Pesquero, Forestal y Afines (Fondafa), Chávez indicó que pasará a ser el Fondo de Desarrollo Agrario Socialista (Fondas), “pues por sus grados de ineficiencia hemos decidido transformarlo en otra instancia que se a más ágil”, agregó.

El presidente manifestó que creando el Fondas se logrará mayor operatividad, mayor flexibilidad y mayor eficiencia en cuanto al tema de la leche.


Asignados 500 millones de dólares a planes para fortalecer producción de leche

Un total de 1.039 millones de bolívares fuertes (500 millones de dólares) asignó el Presidente de la República, Hugo Chávez, para planes que impulsarán la explotación de vacas para obtener leche, además de anunciar un plan masivo de inseminación artificial a costo cero para el productor, alimentación subsidiada a las vacas inseminadas durante un año, dotación de unidades de ordeño y frío, y el subsidio de la siembra y mantenimiento de pasto.

Especificó que se estima llegar a unos 9 millones de animales y explicó que se adquirirán un 1.560 mil unidades de inseminación, 650.000 toneladas alimento concentrado y 12.000 unidades del sistema de ordeño mecánico modular.


Fegaven propone sumar 800.000 litros se leche a producción anual

En este sentido, el presidente Chávez dio un visto bueno al proyecto de la Fegaven qu,e con un plan de inseminación artificial, un plan de manejo, un plan de sistema de riego y un plan de sistema e pastizales, aspira incrementar 800 mil litros de leche a la producción anual.

“Contamos con el 'rebaño' para sustituir las importaciones de leche, si nosotros tomamos las 370.000 vacas que tenemos en nuestra cuenca con un plan de fertilización, con un plan de manejo, con un plan de sistema de riego y un plan de sistema e pastizales, estamos seguros en incrementar la producción en dos litros por vaca, es decir, 800 mil litros de leche a corto plazo, en un año”, destacó.

El área descrita por el productor y presidente de Fegaven, Balsamino Belandia, está integrada por municipios del estado Táchira, municipios de Apure, y todo el estado Barinas y Portuguesa, que posee plantas que se podrían aprovechar para esta ambición, según el representante de los ganaderos, que aspira a reemplazar las importaciones de algunos productos básicos.

Belandia enumeró plantas procesadoras y pasteurizadoras de leche en estos cuatro estados para emprender la sustitución de importaciones de este producto básico, además ofreció cifras del 'ganado' que poseen y la producción que podrán tener con el reimpulso del proyecto.

“Contamos con un 'rebaño' de 5.100 mil 'cabezas de ganado', es decir, un 40% del 'rebaño' que hay en el país, un total de vacas 'disponibles para la producción de leche' de 1.350 mil, de las cuales se están ordeñando 370.000”, expresó.

Fuente: Agencia Bolivariana de Noticias

NOTA: La leche, al igual que el resto de lácteos, los huevos y demás productos de origen animal suponen el asesinato y explotación de animales. Puedes obtener más información sobre las consecuencias del consumo de huevo en la siguiente página web: <a href=https://igualdadanimal.org/problematica/lacteos/</a>, 
