ANGIE CONTRERAS C.


Actualmente, el mercado cárnico nacional se debate entre un clima de desabastecimiento que se ha prolongado durante varios meses, alza en el consumo del rubro, baja producción del rebaño e importaciones que no logran satisfacer la demanda.

De acuerdo con Jaime Pérez Branger, presidente del Consejo Venezolano de la Carne (Convecar), el panorama del rubro a largo plazo no se vislumbra nada prometedor, al contrario, estiman que de no tomarse las medidas necesarias para mejorar la producción y garantizar el suministro del producto, el país podría enfrentar una "grave crisis de abastecimiento de carne".

Explica que el aumento de la demanda, que se ha incrementado en 40% desde 2003, y la mejora del poder adquisitivo de los venezolanos ha presionado la oferta poniendo en jaque al sector cárnico, el cual según Pérez Branger, no está en capacidad de satisfacer el consumo.

Durante el primer semestre del año, el consumo final se incrementó en 19%, al tiempo que la demanda interna repuntó 22%, y la oferta agregada mostró una expansión de 17%.

Pérez Branger señaló que actualmente el consumo per cápita ha pasado de 17 a 21 kilogramos, mientras que el rebaño se encuentra estancado en 12.000 cabezas de ganado.

Señala que cada kilogramo de aumento en el consumo de carne por persona representa un alza en la demanda de 27 toneladas métricas al año. "Aumenta la demanda, crece la población y el 'rebaño' es el mismo".

El productor señala que uno de los factores que ha impulsado la demanda de carne en el país, se debe fundamentalmente a que el poder adquisitivo de la clase E se ha elevado en 169% en los últimos ocho años, por lo que estas personas ahora consumen mayor cantidad de proteína cárnica.

"El consumo que se está evidenciando actualmente se puede comprar con el que teníamos hace 30 años atrás, por lo que la demanda de proteína animal en el país está satisfecha, lo que pasa es que el consumo y la oferta no están equilibrados", dijo el presidente de Convecar.


Baja producción

De acuerdo con cifras de Convecar, este año el "rebaño de ganado" cuenta con un millón menos de individuos que hace ocho años.

Jorge Ordóñez, asesor del Consejo Venezolano de la Carne, indicó que el déficit en la producción nacional es de 650.000 individuos aproximadamente, cifra que representa 30% del consumo de 2006.

Explica que para cubrir el margen de déficit se requiere una inversión de diez mil millardo de bolívares, a través de la cual se recuperaría el sector ganadero, en un promedio de cuatro años.

Asimismo, Ordóñez aseveró que el beneficio de ganado bajó 30% durante el primer semestre de este año. Refirió que hasta abril se produjo el asesinato de 85.000 individuos, mientras que en el mismo período de 2006 se asesinó a 110.000 individuos.

Apunta que al final del año pasado, la "producción bovina" cerró con 442.654 individuos asesinados.

"Este año ha disminuido la matanza de ganado y se han reducido las inversiones en el sector, producto de las políticas gubernamentales como el control de precios y la Ley de Tierras, y por eso se ha producido la escasez", puntualizó Ordóñez.

En ese sentido, Convecar propone al Ejecutivo Nacional incentivar la inversión privada en el campo, generar mayor confianza y seguridad jurídica para que pueda recuperarse en la producción, y garantizar el abastecimiento del producto en el mercado.

El organismo exhortó al Ejecutivo Nacional a retomar las reuniones de la Junta Nacional de la Carnes, las cuales dejaron de realizarse debido a la ausencia del sector oficial, a fin de poder evaluar la realidad del sector y fijar políticas para evitar desabastecimiento del rubro en los próximos años.


"Insuficiente"

Las importaciones de "ganado en pie", que ha venido realizando el Gobierno Nacional desde Brasil en los últimos meses, han sido "insuficientes", pues no se ve estabilidad en el suministro del rubro al mercado.

A pesar de que en los últimos dos meses han ingresado al país unos 55.000 individuos, la escasez no ha disminuido. "Estamos hablando de 25% de la matanza. Hace un año se 'beneficiaban 36.000 cabezas de ganado' a la semana y ahora se matan 26.000, lo que quiere decir que todavía hay 'escasez'", apuntó Ordóñez.

Considera que para satisfacer la demanda actual, las medidas deben estar destinadas a equilibrar la demanda y la oferta, lo cual se puede lograr subiendo el precio y elevando las importaciones.

Hasta la fecha se han solicitado licencias para importar 160.000 animales, de los cuales se han otorgado 100.000.


Fuente: eluniversal.com


Como se puede observar al leer esta noticia, la propia industria ganadera propone importar animales para satisfacer la demanda creciente del consumo de carne. Esto demuestra que los veganos, al rechazar el uso y asesinato de animales en su vida, están contribuyendo a que haya menos individuos que sufran y mueran.

Venezuela es uno de los países en los que la organización internacional Igualdad Animal está presente. Si deseas contactar con ellos, puedes escribir al portavoz de la organización en Venezuela, Andrés Lizardo, a su <a href="mailto:andresl@igualdadanimal.org">correo electrónico</a>.
