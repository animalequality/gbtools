<p><img alt="" class="wp-image-10163" src="/app/uploads/2014/07/gad.jpg" style="width:900px" /></p>

<p>En noviembre, 500.000 animales morir&aacute;n en el mayor sacrificio religioso del mundo: el festival Gadhimai de Bariyarpur, Nepal. B&uacute;falos de agua, cabras, corderos y palomas son blancos de ejecuci&oacute;n, aunque cualquier animal de la zona puede ser acuchillado hasta la muerte por una violenta multitud.</p>

<p>Este festival, de un mes de duraci&oacute;n, se celebra cada cinco a&ntilde;os como sacrificio a la diosa hind&uacute; Gadhimai, y los participantes creen que matar a estos animales les traer&aacute; salud y prosperidad. Un sacerdote dijo a The Guardian: &ldquo;La diosa necesita sangre.&rdquo;</p>

<p><iframe allowfullscreen="" frameborder="0" height="500" src="https://www.youtube.com/embed/Azjtpf2Byvk" width="900"></iframe></p>

<p>En el exterior del templo de Gadhimai, que es el centro del festival, unos 5 millones de personas acuden para presenciar y participar en las matanzas en masa. Consumen grandes cantidades de alcohol y bajo este estado de embriaguez machetean y acuchillan a los animales.</p>

<p>Los participantes viajan largas distancias, muchos desde los estados de Uttar Pradesh, Terai y Bihar. Compran los animales en sus ciudades de origen oblig&aacute;ndoles a viajar durante kil&oacute;metros hacia su muerte inminente.</p>

<p>Gadhimai se celebra cada cinco a&ntilde;os y est&aacute; previsto que en noviembre de 2014 se celebre este horrible festival. <strong>Igualdad Animal da inicio a su campa&ntilde;a internacional &quot;Stop Sacrificios&quot;</strong> para pedir al gobierno de Nepal que detenga la matanza de animales. Para ello se ha presentado la p&aacute;gina web <a href="http://www.StopSacrificios.org">www.StopSacrificios.org</a> donde se puede firmar la petici&oacute;n.</p>

<p>* Desde Igualdad Animal respetamos la libertad de credo, pero creemos que la tradici&oacute;n y la religi&oacute;n no pueden justificar causar da&ntilde;o a los animales. En ese sentido instamos al gobierno de Nepal y a los participantes a celebrar el festival sin matar animales.</p>

