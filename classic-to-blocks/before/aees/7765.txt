<p>Tras conversaciones con Igualdad Animal y una campa&ntilde;a de Compassion in World Farming, <strong>Carrefour Italia <a href="http://www.carrefour.it/carrefour-italia-stop-alla-vendita-di-uova-da-galline-gabbia#.WPYNUVPyiRv" target="_blank">aclara</a> su compromiso para dejar de proveerse de huevos de gallinas enjauladas</strong> en todas las empresas del grupo presentes en Italia y <strong>efectivo en 2017</strong>.</p>

<p>Durante el mes de marzo Carrefour Italia hab&iacute;a anunciado que dejar&iacute;a de comercializar huevos procedentes de gallinas enjauladas, pero sin aclarar si implicar&iacute;a a todos los puntos de venta de la empresa. <strong>La ambig&uuml;edad de este compromiso llev&oacute; a las organizaciones involucradas con la empresa a exigir mayor claridad y transparencia</strong>.</p>

<p><img alt="" class="wp-image-11914" src="/app/uploads/2017/04/used_for_dussmann_0.jpg" style="margin: 10px; float: left; width: 300px; " /></p>

<p>&nbsp;</p>

<p>El primer pa&iacute;s del grupo multinacional en comprometerse a ser 100% cage free fue <strong>Francia, cuyo <a href="http://www.carrefour.com/providing-support-to-our-partners/all-carrefour-eggs-from-cage-free-hens" target="_blank">acuerdo</a> ser&aacute; efectivo en 2020</strong>.</p>

<p>Con este acuerdo, la compa&ntilde;&iacute;a se ha unido a <strong>Auchan Francia y Mondelez Internacional</strong> en comprometerse esta misma semana a <strong>abandonar el cruel confinamiento de las gallinas en la industria del huevo</strong>.</p>

<p>Aunque libre de jaulas no implica libre de maltrato, estos compromisos son pasos importantes para reducir el sufrimiento de los animales y <strong>ya son m&aacute;s de 400 empresas a nivel mundial que han tomado estas medidas</strong>.</p>

<p>Sin embargo, <strong>El Corte Ingl&eacute;s pretende alargar el sufrimiento de estas gallinas hasta 2030</strong>. Por favor, firma nuestra <a href="https://igualdadanimal.org/noticia/2017/12/15/el-corte-ingles-dejara-de-vender-huevos-de-gallinas-enjauladas/" target="_blank">petici&oacute;n</a> para pedirle a El Corte Ingl&eacute;s que deje de proveerse de huevos de gallinas encerradas en jaulas.</p>

