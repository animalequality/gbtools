<p><img alt="Foto Seprona" class="wp-image-14072" src="/app/uploads/2011/04/jabali-furtivos.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 300px; " /><small>Agentes del Servicio de Protecci&oacute;n de la Naturaleza (Seprona) de la Guardia Civil han detenido a tres vecinos de Cabeza del Buey (Badajoz) que fueron sorprendidos &#39;in fraganti&#39; cuando capturaban cuatro jabal&iacute;es vivos de mediana edad en acotado de <a href="http://www.igualdadanimal.org/entretenimiento/caza">caza</a> sin permiso del titular en la localidad pacense de Zarza Capilla y usando medios masivos y no selectivos.</small><br />
&nbsp;</p>

<p><small>La detenci&oacute;n de estas tres personas como supuestos autores de un delito contra la fauna se ha producido el pasado fin de semana, seg&uacute;n ha informado la Guardia Civil en nota de prensa.<br />
<br />
Durante un mes, los agentes del Seprona de Castuera y Herrera del Duque en colaboraci&oacute;n con los Guardas de <a href="http://www.igualdadanimal.org/entretenimiento/caza">caza</a> del Coto conocido como Piedra Santa, sito en el t&eacute;rmino municipal de Zarza Capilla (Badajoz), montaron un dispositivo de vigilancia en el paraje conocido como Alto de las Cabezas.&nbsp;En dicho lugar existe un corral&oacute;n de piedra, que estaba siendo &quot;cebado&quot; con ma&iacute;z y avena, supuestamente para atraer animales a su interior.<br />
<br />
En el transcurso de los d&iacute;as, se comprob&oacute; c&oacute;mo varias personas depositaban comida en el lugar.&nbsp;El pasado d&iacute;a 1 los agentes observaron c&oacute;mo los tres imputados acceden al lugar colocando un sistema definitivo de cierre con trampilla, que permitir&iacute;a la entrada de animales en el corral&oacute;n pero no su salida.&nbsp;Al d&iacute;a siguiente fueron detenidos cuando sacaban del &quot;lugar trampa&quot; cuatro jabal&iacute;es de mediana edad, introducidos vivos en otros tantos sacos de pl&aacute;stico en el interior de un veh&iacute;culo todo terreno.<br />
<br />
Los jabal&iacute;es supuestamente destinados para su venta en vivo para repoblar otros espacios de <a href="http://www.igualdadanimal.org/entretenimiento/caza">caza</a>, fueron liberados en su medio natural.<br />
<br />
La Guardia Civil ya llevaba tiempo investigando este tipo de conductas en varias fincas de la zona, de hecho ten&iacute;a constancia de que en la misma de la presente actuaci&oacute;n, el pasado a&ntilde;o se pudieron sacar m&aacute;s de 30 jabal&iacute;es.<br />
<br />
Fuente: <a href="https://www.20minutos.es/noticia/1014570/0/">20minutos.</a></small><br />
&nbsp;</p>

<hr />
<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong><big>ANEXO: EL JABAL&Iacute; Y SU CAZA</big></strong><br />
<br />
<strong>EL JABAL&Iacute; - La Voz</strong><br />
<br />
No es de extra&ntilde;ar que en una especie tan social como el jabal&iacute;, el papel desempe&ntilde;ado por la comunicaci&oacute;n sonora sea tan destacado. Se ha podido registrar que en las primeras etapas del desarrollo los rayones han llegado a desarrollar m&aacute;s de 106 tipos de sonidos diferentes que tienen la misi&oacute;n de comunicarse con la madre y con sus hermanos (Jensen y Algers, 1983). Algunos de estos sonidos son b&aacute;sicos a la hora de predisponer a la madre a iniciar una sesi&oacute;n de lactaci&oacute;n (Algers y Jensen, 1985), habi&eacute;ndose podido determinar que los cerditos son realmente honestos a la hora de manifestar, mediante diferentes frecuencias sonoras, su nivel de hambre (Weary y Fraser, 1995). (Fuente: <a href="http://www.vertebradosibericos.org/mamiferos/comportamiento/susscrco.html">vertebradosibericos.org</a>)<br />
<br />
<strong>LA CAZA DEL JABAL&Iacute; | M&Aacute;S ALL&Aacute; DE LA CRUELDAD.</strong><br />
<br />
Por desgracia para estos animales, los cazadores se ceban con ellos de varias formas, cazado con perros (muchos de los cuales mueren o sufren heridas), con arco o ballesta, con escopeta. No hay piedad y sufren la mayor de las crueldades.<br />
<br />
Youtube es un <a href="https://www.youtube.com/results?search_query=caza+de+jabal%C3%AD&amp;aq=f">escaparate de las brutalidades y matanzas ejercidas a estos individuos</a>, siempre vistiendo esta actividad como &quot;haza&ntilde;a heroica&quot;, &quot;batalla de igual a igual&quot;, necesidad, diversi&oacute;n, natural, etc. Este es solo un ejemplo, en el que un cazador ense&ntilde;a a sus perros a matar a un jabal&iacute; que trata in&uacute;tilmente de escapar del horror del sufrimiento de las mil dentelladas, finalmente, le clava un machete en el coraz&oacute;n:<br />
<br />
https://www.youtube.com/watch?v=y5dnFG-dWOg</p>

