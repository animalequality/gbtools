<div class="center"><img class="wp-image-11146" src="/app/uploads/2016/10/14697165_10211210756333028_1599843242_o.jpg" /></div>
<p>Tyson Foods es el mayor productor de carne de EE. UU. Beyond Meat es una start up productora de alternativas vegetales a la carne. &iquest;Enemigos irreconciliables?, al parecer, no.</p>

<p>El gigante de la carne estadounidense acaba de adquirir una participaci&oacute;n del 5% en la peque&ntilde;a empresa que tiene como objetivo conquistar con sus alternativas vegetales <strong>los paladares de los m&aacute;s carn&iacute;voros</strong>.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;El futuro de la alimentaci&oacute;n pasa por las opciones sostenibles y libres de maltrato animal que nos brinda la tecnolog&iacute;a.&raquo;</p></FONT>

<p>&nbsp;</p>

<p>En declaraciones a <strong>The New York Times</strong>, Ethan Brown, fundador y director ejecutivo de <a href="https://www.beyondmeat.com/en-US" target="_blank">Beyond Meat</a> manifestaba que, &laquo;s&eacute; que la inversi&oacute;n de Tyson Foods en Beyond Meat sorprender&aacute; a muchos, sobre todo a las personas vegetarianas y veganas; sin embargo, mantengo la esperanza de que la mayor&iacute;a lo vea como una estrategia por nuestra parte para hacer que nuestros productos salgan de la terrible secci&oacute;n de &laquo;diet&eacute;tica&raquo; de los supermercados, pudiendo as&iacute; generar inter&eacute;s entre los consumidores mayoritarios&raquo;.</p>

<p>El movimiento de Tyson Foods es el primero de este tipo por parte de las gigantescas empresas <a href="https://igualdadanimal.org/noticia/2016/09/06/si-podemos-alimentarnos-sin-causar-dano-los-animales-por-que-no-hacerlo/" target="_blank">productoras de carne</a>.</p>

<p><img alt="" class="wp-image-11147" src="/app/uploads/2016/10/14686582_10211210757133048_1768473509_n.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=2><FONT COLOR=#808080><p>Beyond Burger, la hamburguesa vegetal de Beyond Meat. Foto: Beyond Meat.</p></FONT>

<p>&nbsp;</p>

<p>&laquo;La calidad de la Beyond Burger [hamburguesa <a href="http://beyondmeat.com/products/view/beyond-burger" target="_blank">100% vegetal</a> de Beyond Meat y su producto estrella] es impresionante&raquo;, declaraba la vicepresidenta de inversiones estrat&eacute;gicas de Tyson Foods, Monica McGurk. &laquo;Pensamos que es un producto destinado a cambiar el mercado que nos proporciona visibilidad entre el creciente n&uacute;mero de consumidores de las alternativas a la carne&raquo;.</p>

<p>En EE. UU., el pa&iacute;s del consumo de carne por excelencia, las empresas que proponen <a href="https://igualdadanimal.org/noticia/2016/05/04/el-desarrollo-de-alternativas-vegetales-la-carne-tendencia-mundial-numero-uno-para-el/" target="_blank">alternativas vegetales</a> se est&aacute;n abriendo paso. Las prote&iacute;nas vegetales, y la <strong>&laquo;carne limpia&raquo;</strong>, carne producida mediante agricultura celular a partir de c&eacute;lulas musculares de animales, est&aacute;n captando la atenci&oacute;n de grandes inversores (Bill Gates y Google entre ellos).</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;El futuro, nuestro futuro y el de los animales, est&aacute; m&aacute;s all&aacute; de la carne&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>El <strong>futuro de la alimentaci&oacute;n</strong> pasa por las opciones sostenibles y libres de maltrato animal que nos brinda la tecnolog&iacute;a. Nuevas y <strong>m&aacute;s eficientes</strong> maneras de producir los alimentos necesarios para una poblaci&oacute;n humana en constante aumento son imprescindibles.</p>

<p>Beyond Meat es la representaci&oacute;n manifiesta de un <a href="https://igualdadanimal.org/blog/imagina-un-modelo-alimentario-que-salve-el-mundo-y-no-nos-haga-renunciar-nada/" target="_blank">cambio de modelo alimentario</a> global que est&aacute; teniendo lugar de forma lenta pero segura. La ganader&iacute;a industrial, por el contrario, <strong>representa un modelo ineficiente </strong>de producci&oacute;n de alimentos.</p>

<p>El futuro, nuestro futuro y el de los animales, est&aacute; m&aacute;s all&aacute; de la carne. &nbsp;Y <strong>no tendremos que renunciar al sabor</strong>, de eso se encargar&aacute; Beyond Meat y las empresas decididas a innovar y romper las reglas de un juego ama&ntilde;ado por los intereses de la industria c&aacute;rnica.</p>

<p><strong>Si quieres estar al tanto de c&oacute;mo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n</strong>.</p>

<p>&nbsp;</p>

<p>Fuente: <a href="http://www.nytimes.com/2016/10/11/business/tyson-foods-a-meat-leader-invests-in-protein-alternatives.html?_r=0" target="_blank">http://www.nytimes.com/2016/10/11/business/tyson-foods-a-meat-leader-invests-in-protein-alternatives.html</a></p>

