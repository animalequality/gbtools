El Secretario de Agricultura, Ganadería, Pesca y Alimentos, Javier de Urquiza, presidió un nuevo encuentro del Consejo Federal Agropecuario. Presentó para su evaluación un anteproyecto para la modificación de la ley Nº 22375.

El mismo contempla un nuevo régimen para la habilitación y funcionamiento de los establecimientos de explotación y asesinato de animales para la obtención de carne, a los efectos de promover un ordenamiento estandarizado a nivel nacional.

La intención de la propuesta presentada es poner en consideración de las provincias la posibilidad de acordar con la nación la aplicación unificada sobre todo el territorio del país de estándares necesarios para la habilitación de este tipo de plantas industriales.

“Una Ley nacional daría el respaldo requerido para lograr la modernización del sistema de comercialización de carnes, con un adecuado ordenamiento y según los objetivos, metodologías y plazos que se acuerden”, remarcó el secretario.

Ministros provinciales, cuyas presencias dieron completa representación federal al país, junto al Secretario y asesores de Economía y Agricultura, coincidieron en temas como la aplicación de políticas de incentivos para la ganadería, la potencialidad para la incorporación de valor sobre la producción primaria, la flexibilización en la tramitación de beneficios a la producción y la necesidad de apoyo a las economías regionales –especialmente a la pequeña producción-, depositando amplias expectativas en la constitución de la Subsecretaria de Desarrollo Rural, según lo anunciado meses atrás.

Entre otros temas se manifestó el reconocimiento a la labor del INTA en el desarrollo productivo, así como propiciar la participación de otras carteras del Gobierno en el ámbito del consejo, cuyas áreas influyen en la producción agropecuaria.

Por su parte las provincias de menor desarrollo de actividad agropecuaria, coincidieron en la necesidad de potenciar la aplicación de los programas de intervención para la promoción de actividades y realización de obras de infraestructura.

Las provincias ganaderas se comprometieron, junto con los funcionarios nacionales, a revitalizar la aplicación del Plan Ganadero con el objetivo inmediato de incrementar la oferta cárnica, mejorando la productividad y promocionando el consumo de carnes.

El cierre del encuentro, que se realizó esta mañana en la sede de Agricultura, contó con la presencia del Ministro de Economía y Producción, Martín Losteau, quien reconoció la fortaleza del sector agropecuario dentro de la economía argentina.

El próximo encuentro del Consejo Federal Agropecuario, se realizará en la provincia de Salta durante los últimos días de Abril o primer semana de Mayo.

Fuente:  Agrodiario
