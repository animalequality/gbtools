¿No son fantásticos? Estos maravillosos seres tan cruelmente tratados en granjas y mataderos establecen vínculos con nosotros tan rápidamente como nuestros queridos perros y gatos.

Los cerdos, por ejemplo, son tan inteligentes como los delfines… ¡y llegan al coeficiente intelectual de un niño de 3 años! Las vacas son las madres más protectoras y abnegadas que puedan existir. Y las frágiles gallinas empiezan a comunicarse con sus pollitos incluso antes de romper el cascarón.

Por su parte, los pollos, esos animales que rara vez vemos fuera de una bandeja en una nevera de un supermercado, pueden reconocer y recordar hasta a 100 miembros de su grupo. Además tienen hasta 30 distintas vocalizaciones para comunicarse entre ellos.
<div class="media_embed"><iframe src="https://www.youtube.com/embed/kAI-PFWZ95I" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
Cualquier persona que haya podido convivir con pollos y gallinas sabe que las personalidades entre ellas varían tanto como entre nuestros perros y gatos. Cada gallina y cada pollo muestran rasgos de personalidad exclusivos. Y como nuestros gatos y perros, establecen vínculos con nosotros rápidamente. Es la consecuencia de haber convivido durante miles de años.

Las vacas son seres pacíficos y sociables. Tienen sus mejores amigas en el grupo y establecen jerarquías y siguen a la que más experiencia tiene, tal y como hacen los elefantes. Se han dado casos de vacas que han conseguido escaparse del matadero y han regresado allá donde estaban sus terneros. Estos majestuosos animales irradian amor y paz. Y además… ¡Son tan curiosas como los gatos!
<div class="media_embed"><iframe src="https://www.youtube.com/embed/Dd2CWnM1zSs" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<sub><em>Cerditos y gatos: ¡la mezcla perfecta!</em></sub>

Los cerditos se han convertido en verdaderas estrellas en las rede sociales. No es de extrañar, estos animales son tan adorables que hasta los gatos más ariscos no pueden resistirse a sus encantos.

De manera que ya lo sabemos: los animales de granja son adorables y saben demostrárnoslo si les damos oportunidad.
