<div class="center"><img class="wp-image-10643" src="/app/uploads/2016/05/pollito_0.jpg" /></div>
<p>Dejando aparte a los peces, <strong>9 de cada 10 animales consumidos en el mundo son pollos</strong>. Estos animales de granja tienen la desgracia de ser el animal terrestre preferido por el paladar del ser humano.</p>

<p>La <a href="https://igualdadanimal.org/blog/7-razones-por-las-que-la-carne-de-pollo-no-es-tan-barata-como-parece/">carne de pollo</a> suele ser publicitada como &laquo;m&aacute;s sana&raquo; y muchas personas que reducen su consumo de carnes rojas aumentan el de carne de pollo. <strong>Las consecuencias para estos fr&aacute;giles y sensibles animales son devastadoras</strong>.</p>

<p>Al ser animales de menor tama&ntilde;o que vacas o cerdos se necesitan muchos m&aacute;s para satisfacer la demanda del consumidor. <strong>Para producir la misma cantidad de carne que una vaca son necesarios 200 pollos</strong>.</p>

<p>Las <a href="https://igualdadanimal.org/noticia/2016/01/07/te-presentamos-al-animal-mas-numeroso-del-mundo/">condiciones de vida</a> de los pollos que acaban en las bandejas de carne de los supermercados son terribles. La pr&aacute;ctica totalidad proviene de la ganader&iacute;a industrial. <strong>Esta macro industria es la responsable del peor maltrato animal conocido</strong>. La escala de sus operaciones a nivel internacional es gigantesca.</p>

<p>Los n&uacute;meros son astron&oacute;micos: <strong>60.000 millones de pollos son enviados al matadero cada a&ntilde;o</strong>. En todo momento hay 20 mil millones de pollos hacinados en granjas industriales en el mundo. <strong>Hay tres pollos por cada ser humano en el planeta </strong>y su vida es una verdadera pesadilla.</p>

<p>En <a href="https://igualdadanimal.org/blog/viven-los-animales-de-granja-sonrientes-en-verdes-y-soleados-prados/">las modernas granjas</a> que nos abastecen de carne, los pollos viven entre sus propios excrementos, <strong>respirando el amoniaco de sus deyecciones</strong>, por lo que desarrollan problemas pulmonares.</p>

<p><img alt="" class="wp-image-10645" src="/app/uploads/2016/05/noticiafoto2.jpg" style="float:right; margin:10px; width:450px" />Estos animales han sido hibridados y seleccionados gen&eacute;ticamente para crecer lo m&aacute;s r&aacute;pido posible. De esta manera son enviados antes al matadero y producen m&aacute;s beneficios econ&oacute;micos a la cruel industria de la carne. <strong>La vida media de un pollo destinado a la carne es de 40 d&iacute;as</strong>. La carne de pollo que comemos es en realidad carne de animales j&oacute;venes, pr&aacute;cticamente beb&eacute;s.</p>

<p>Si elevamos el consumo de carne de pollo el maltrato a estos peque&ntilde;os animales se multiplicar&aacute; exponencialmente. <strong>Es el drama de estos seres sensibles y llenos de ganas de vivir que tienen la desgracia de producir la carne preferida por todas las culturas</strong>.</p>

<p>Un drama que tiene lugar en granjas de paredes de hormig&oacute;n <strong>en las que ni siquiera entra la luz del sol</strong>; un drama evitable si realmente nos lo proponemos.</p>

<p>Por favor, considera <strong>ayudar a estos animales y al resto de animales de granja</strong> consumiendo alternativas a la carne. <strong>No consumir los productos que conllevan maltrato animal lanza un poderoso mensaje a la industrias c&aacute;rnica</strong>. &nbsp;Si te apetece intentarlo tienes toda la informaci&oacute;n que necesitas para conseguirlo, con recetas, productos y consejos, en las <strong>maravillosas websites</strong> <a href="https://www.gastronomiavegana.org/">Gastronom&iacute;a Vegana</a> y <a href="https://danzadefogones.com/">Danza de Fogones</a>.</p>

