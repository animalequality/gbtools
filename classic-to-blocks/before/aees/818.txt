Por defender a su guía, un perro de raza rottweiler, integrante de la brigada canina del serenazgo de Magdalena, fue acuchillado por un delincuente que fue sorprendido robando un vehículo en el barrio popular conocido como Medalla Milagrosa en Lima-Perú.

El agresor del perro fue llevado a la comisaría y en cuestión de horas fue liberado como si no hubiera cometido ningún delito. La razón es que en las leyes no se contempla pena alguna por dañar de gravedad a un animal no-humano (1). En la televisión mostraron este suceso como injusto, diciendo de manera irónica ¨para las leyes la vida de un perro no vale nada¨. Incluso uno de los entrevistados mencionó: ¨aunque parezca extraño matar un perro según la ley es como romper un televisor¨.

Sin embargo hay que recordar que esta desconsideración hacia los animales no-humanos sucede casi a diario en mataderos, laboratorios, peleterías, etc. y no se expone ante el público como una injusticia.

En realidad las leyes actuales son un simple reflejo de la mentalidad imperante de la sociedad que considera que los individuos que no pertenecen a la especie humana son objetos para beneficio humano y no un caso aislado de injusticia como lo mostraron en la televisión.

Tampoco hay que olvidar que el perro no hubiera sufrido este incidente si no se le utilizara como arma de defensa. De hecho esto también obedece a las razones arriba citadas.

Más información en:
http://www.rpp.com.pe/portada/nacional/49137_1.php

(1) En realidad acuchillar a un perro es según las leyes actuales una ¨falta¨. Los perros (asi como los demas animales no-humanos) no son considerados portadores de derechos. Una agresión hacia un perro es en realidad interpretada como un delito contra la propiedad de un humano. 

