<p>
	<img align="left" alt="" height="194" hspace="9" class="wp-image-13647" src="/app/uploads/2010/04/36461211.jpg" vspace="9" width="300" />Agentes de la Guardia Civil han detenido en Pedrajas de San Esteban (Valladolid) a diez j&oacute;venes implicados en la matanza a golpes y patadas de unas cien gallinas el pasado 16 de febrero.<br />
	<br />
	Los j&oacute;venes, cuatro de ellos menores de edad, entraron por la fuerza durante la noche en una granja donde se explotan gallinas para la producci&oacute;n de huevos. Seg&uacute;n se han defendido, hab&iacute;an tomado unas copas de m&aacute;s y cumpl&iacute;an una tradici&oacute;n del pueblo, consistente en coger varios animales de esa granja para torturarles y matarles.<br />
	<br />
	El due&ntilde;o de la granja, donde viven confinadas 100.000 aves, se ha lamentado diciendo que esta vez ha sido demasiado, no como en ocasiones anteriores en que no lleg&oacute; a denunciar porque el n&uacute;mero de animales masacrados fue mucho menor. El empresario ha calculado unas p&eacute;rdidas de unos 2.000 euros y se ha quejado tambi&eacute;n de que &ldquo;Adem&aacute;s de los destrozos en las instalaciones y las bajas habidas, lo peor es los efectos en la producci&oacute;n ya que se somete a un estr&eacute;s a las gallinas y eso repercute tambi&eacute;n en la puesta&rdquo;.<br />
	<br />
	Los j&oacute;venes que llevaron a cabo la masacre est&aacute;n a la espera de juicio por maltrato animal y da&ntilde;os, aunque puede que se les acuse tambi&eacute;n de robo con fuerza.<br />
	<br />
	Este tipo de noticias hacen que nos sorprendamos y horroricemos al ver hasta qu&eacute; punto puede llegar la crueldad de algunas personas. Sin embargo, no podemos olvidar que cada a&ntilde;o, s&oacute;lo en Espa&ntilde;a, unos 51 millones de gallinas son enviadas al matadero porque ya no es rentable seguir explot&aacute;ndolas para la puesta de huevos; que la mayor&iacute;a de ellas pasa una vida corta y miserable en una jaula, casi sin poder moverse, muchas de ellas con huesos rotos por la descalcificaci&oacute;n; que por cada gallina &ldquo;ponedora&rdquo; que est&aacute; ahora mismo explotada, un pollito macho fue arrojado a una trituradora; Y que todo esto ocurre porque demandamos un producto que no necesitamos: el huevo.</p>
<p>
	Conoce c&oacute;mo son las gallinas pinchando <a href="http://www.granjasdeesclavos.com/gallinas/como-son">aqu&iacute;.</a></p>
<p>
	Puedes ver el rescate de 10 gallinas que hizo Igualdad Animal pinchando <a href="https://vimeo.com/2482885">aqu&iacute;.</a></p>

