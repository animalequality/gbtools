<div class="center"><img class="wp-image-12684" src="/app/uploads/2017/12/23973379577_2465113acd_z.jpg" /></div>
<p>El s&aacute;bado 2 de diciembre Igualdad Animal celebr&oacute; en Los &Aacute;ngeles su<strong> Inspiradora Gala de Acci&oacute;n Global</strong> en compa&ntilde;&iacute;a de grandes amantes de los animales, dejando pocos ojos secos durante los emotivos e impactantes discursos de la noche. Durante toda la velada recordamos<strong> un a&ntilde;o incre&iacute;ble lleno de vanguardistas campa&ntilde;as que han provocado un cambio monumental en los animales de todo el mundo</strong>.<br />
<br />
El evento, que tuvo por anfitriona a la incre&iacute;ble Michelle Forbes, cont&oacute; con una cena gourmet a base de vegetales patrocinada por Miyoko&#39;s Kitchen, Hungry Planet y Harmless Harvest, un espect&aacute;culo de premios fenomenal, una discreta subasta y una lista de invitados llena de estrellas.</p>

<p><img alt="" class="wp-image-12681" src="/app/uploads/2017/12/collage-gala-1_0.jpg" style="margin:10px; width:550px" /><br />
<br />
Los presentadores de los premios, incluidos Moby y Kat Von D, hablaron <strong>del inmenso impacto de la organizaci&oacute;n en la prevenci&oacute;n del sufrimiento animal</strong> en los nueve pa&iacute;ses en los que trabajan: EE. UU., Reino Unido, M&eacute;xico, Brasil, Espa&ntilde;a, Italia, Alemania, India y China.</p>

<p>Marco Antonio Regil, famoso presentador de televisi&oacute;n, orador p&uacute;blico y activista de los animales, fue galardonado con el Premio &ldquo;Animal Hero&rdquo; que recibi&oacute; de la tambi&eacute;n famosa artista del tatuaje Kat Von D.</p>

<p>Al hablar sobre el trabajo de los valientes investigadores de Igualdad Animal en todo el mundo, Marco Antonio Regil dijo: &quot;Despu&eacute;s de ver estos videos, supe que era algo con lo que no pod&iacute;a alinearse. Fue en este momento que decid&iacute; cambiar. Fue una elecci&oacute;n dif&iacute;cil, pero sab&iacute;a que era algo que ten&iacute;a que hacer, ya que estaba en mi coraz&oacute;n y se siente incre&iacute;ble porque puedo trabajar con organizaciones como Igualdad Animal&quot;.</p>

<p><img alt="" class="wp-image-12682" src="/app/uploads/2017/12/collage-gala-3.jpg" style="margin-left:10px; margin-right:10px; width:550px" /><br />
<br />
Tambi&eacute;n fueron homenajeados el Pr&iacute;ncipe Khaled bin Alwaleed, quien recibi&oacute; el Premio al &ldquo;Visionary Leader&rdquo; de manos de la Cofundadora y Presidente de Igualdad Animal, Sharon Nu&ntilde;ez; &nbsp;Moby fue el encargado de otorgar a Ari Nessel el Premio &ldquo;Philanthropic Impact &ldquo;. Al recibir su premio, el Sr. Nessel se comprometi&oacute; a igualar todas las donaciones hechas durante la noche. Miyoko Schinner, fundadora de Miyoko&#39;s Kitchen se llev&oacute; a casa el Premio &ldquo;Compassionate Company&rdquo; presentado por la cantante y compositora de 16 a&ntilde;os Shannon K. Shannon se hizo vegana despu&eacute;s de asistir a la gala del d&eacute;cimo aniversario de Igualdad Animal el a&ntilde;o pasado.</p>

<p><img alt="" class="wp-image-12683" src="/app/uploads/2017/12/collage-gala-2.jpg" style="margin-left:10px; margin-right:10px; width:550px" /><br />
<br />
Entre las celebridades que prestaron su apoyo al valioso trabajo de Igualdad Animal est&aacute;n: Kat Von D, Evanna Lynch, Moby, Shannon K, Lisa Edelstein, Lesley Nicol, Seba Johnson, Jayde Nicole, Stephanie Corneliussen, Jon Mack y Jona Weinhofen.<br />
<br />
Sabemos que el &eacute;xito de nuestra celebraci&oacute;n y de <strong>nuestro trabajo se debe en gran parte a personas como t&uacute;</strong>, es por ello que es un honor absoluto poder pasar una velada evocando el trabajo que nuestros seguidores hacen posible.<br />
<br />
Para ver el resto de las fotos de est&aacute; incre&iacute;ble noche da clic <a href="https://www.flickr.com/photos/animalequalityuk/sets/72157688046500172">aqu&iacute;</a>.</p>

