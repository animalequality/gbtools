Mendata, Vizcaya - Se ha localizado a treinta animales abandonados, y con señales que mostraban agresiones, y por orden de un juez han sido trasladados a otro lugar.

A consecuencia de las agresiones, uno de los perros era incapaz de ladrar, y otro daba vueltas sobre sí mismo de manera obsesiva, al estar acostumbrado a andar encerrado en una jaula.

En el lugar se localizó a siete perros, ocho conejos, cuatro gatos, tres ocas, dos ovejas, aparte de gallinas, hamsters y erizos.
