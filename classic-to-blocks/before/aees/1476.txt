Granada.-Según informa 20 minutos, el “animalario” de la Universidad de Granada vende anualmente entre 6000 y 7000 ratones modificados genéticamente. El centro, de 600 metros cuadrados, está preparado para "albergar" a más de 3.000 ratones y ratas destinados a la investigación y la experimentación científica.

«Aquí se cuida el animal y nunca se le crea un sufrimiento extremo porque cumplimos con la normativa y creemos que la investigación es avance», dice el director.

La experimentación, es para aquellos que la sufren, cualquier cosa menos avance. Cada año millones de ratones, monos, conejos, perros y otros animales no humanos son privados de libertad, aislados, torturados y asesinados a manos de los vivisectores. Se pretenden justificar estas atrocidades alegando que el objetivo es encontrar remedio a enfermedades humanas. Pero resulta que los animales víctimas de estos experimentos no tienen la culpa de nuestras enfermedades y es injusto que paguen por ello. Además, si estamos en contra de que se experimente con individuos en contra de su voluntad debido que no pertenecen a nuestra raza, sexo o cultura, no podemos defender que se haga con aquellos que no pertenecen a nuestra especie.


