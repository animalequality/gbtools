San Diego, 18 de marzo.- Una campaña televisiva denominada "Toma leche", animada con fábulas y moralejas, y lanzada esta semana por el Consejo de la leche de California, promueve el consumo del lácteo entre 14 millones de latinos en el estado. 

El promocional incluye como personaje principal a la leche de vaca y como secundarios a una bruja y un duende en un escenario medieval, con un mensaje muy claro de que quien se abstiene de beber leche sufriría consecuencias.

El director del plan, Steve James, informó que aplicó un estudio previo de mercadotecnia concluyó que a los latinos les atraen las historias fantásticas con personajes fabulosos y desenlaces en moralejas.

El mensaje es diferente a la campaña denominada "Familia, amor y leche", realizada hace 13 años y que motivaba a las madres a promover el consumo de millones de galones de leche entre sus familias.

El Departamento de Educación de California informó que el Programa de Leche en desayunos escolares invierte 500 mil dólares de aportaciones federales cada año. 

Las campañas dirigidas específicamente a la comunidad latina y que habla castellano son adicionales a otra de California denominada "Hay leche", en la que los personajes a menudo se atragantan con galletas y otros alimentos secos. 

En los nuevos anuncios para los latinos, la moraleja compartida es que quien consume leche tiene una mejor calidad de vida, dijo James. 

La industria y el gobierno de California promueven actualmente y también con comerciales televisivos un programa que asegura que las mujeres que consumen varias veces al día derivados lácteos, en especial el yogourt, conservan la salud y una silueta atractiva. 

El Instituto de Ciencia Animal y Tecnología y Ciencia Alimenticia de la Universidad de California en Davis (UCD) divulgó en 2007 otro comercial temporal en español en el que participantes en una competencia luchaban por alcanzar último "vaso" de leche en el mundo. 

Las campañas para el consumo de la leche en California impulsan al mismo tiempo una multimillonaria industria láctea estatal en la que la mayor parte de los trabajadores son latinos. 

La empresarial Oficina de Agrícola de California informó que en el 2003 esa industria se había traducido sin precedentes en cuatro mil millones de dólares, pero a partir de 2004 ha superado los cinco mil 300 millones de dólares. (Con información de Notimex/MVC)

Fuente: El Financiero en línea

NOTA: Las vacas, al igual que el resto de animales, son individuos que merecen respeto, y por ello es inaceptable su explotación. Puedes obtener más información sobre las consecuencias del consumo de lácteos en nuestra página web <a href="https://igualdadanimal.org/problematica/lacteos/">https://igualdadanimal.org/problematica/lacteos/</a>
