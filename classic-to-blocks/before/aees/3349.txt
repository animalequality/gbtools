El Gaula de la Policía confirmó la aparición de Aldo, el perro que había sido secuestrado en un barrio al norte de Bogotá, y por el que pedían 700 millones de pesos (unos 350.000 dólares). Al parecer Aldo fue sedado con tranquilizantes durante el secuestro y llevado en una bolsa por los criminales.

El pasado martes un comando policial advertido, y de civil, se trasladó al lugar convenido con los secuestradores de Aldo que iban a recibir el rescate.

"A la hora exacta, un hombre llegó con un paquete de papel periódico en el que había varios millones de pesos. Tras unos minutos, dos personas se le acercaron y le dijeron que venían por el "encargo" y que horas después le harían llegar el "paquete": no llevaban consigo al secuestrado", señaló El Tiempo.

Una vez recibieron el dinero, los policías encubiertos trataron de detenerlos, pero los delincuentes desenfundaron armas y abrieron fuego, y tras un intercambio de disparos, fueron detenidos Ángel Egidio Cárdenas Ávila y Wílmer Son Orozco, quienes se recuperan de lesiones en un hospital de Bogotá.

Un policía también resultó herido.

Fuentes oficiales indicaron que Aldo fue dejado desde el martes pasado en un centro veterinario del Barrio Mandalay, en donde recibiría un completo tratamiento de aseo.

Aunque Aldo no consumió alimento desde ese día, los veterinarios dicen que se encuentra en perfectas condiciones de salud y ahora se reunirá con su dueña (actualmente los animales no-humanos tienen el estatus legal de propiedad).

Las autoridades mantienen la búsqueda de otros dos integrantes de la banda que secuestraron a Aldo.
