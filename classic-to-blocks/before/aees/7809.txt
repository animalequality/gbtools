<div class="center"><img class="wp-image-12089" src="/app/uploads/2017/06/783f4e6f-4758-4ef9-a4a0-1176818fc1a0.jpg" /></div>
<p>La organizaci&oacute;n internacional Igualdad Animal <a href="https://www.youtube.com/watch?v=XhLXQAQJVvU&amp;feature=youtu.be" target="_blank">ha proyectado im&aacute;genes de mataderos en la fachada del McDonald&#39;s</a> situado en Avda de Espa&ntilde;a s/n en Majadahonda, Madrid.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3D.%2540IgualdadAnimal%2520convierte%2520la%2520fachada%2520de%2520McDonald" target="_blank">Comparte esta nueva acci&oacute;n de Igualdad Animal en Twitter</a>.</p></FONT>

<p>&nbsp;</p>

<p><br />
<br />
La organizaci&oacute;n animalista proyect&oacute; <strong>im&aacute;genes de mataderos de vacas en Espa&ntilde;a obtenidas en sus investigaciones</strong>. De esta manera, el edificio de la popular cadena de comida r&aacute;pida <strong>se convirti&oacute; en un cine contra el maltrato animal.&nbsp;</strong></p>

<p><br />
<iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/XhLXQAQJVvU" width="854"></iframe></p>

<p><br />
&quot;McDonald&#39;s y la industria c&aacute;rnica&nbsp;nunca muestran los mataderos en sus anuncios. Es algo que mantienen oculto. Detr&aacute;s de cada hamburguesa que venden hay una historia de maltrato que los consumidores tienen derecho a conocer&quot;, indic&oacute; Javier Moreno, cofundador de Igualdad Animal.&nbsp;<br />
<br />
En redes sociales utilizaremos el hashtag<strong> #McMataderos</strong> para difundir la acci&oacute;n.</p>

<p>La acci&oacute;n se llev&oacute; a cabo el pasado mi&eacute;rcoles 24 de mayo a las 23 horas y se desarroll&oacute;&nbsp;sin ning&uacute;n incidente.&nbsp;</p>

