<div class="center"><img class="wp-image-9390" src="/app/uploads/2012/03/pinguino_fugado.jpg" /></div>
<p>Escalando h&aacute;bilmente por una escarpada pared de piedras, un ping&uuml;ino logr&oacute; escapar la semana pasada del <a href="http://www.igualdadanimal.org/entretenimiento/zoos" target="_blank">zool&oacute;gico </a>de Tokio en el que se encontraba recluido.</p>
<p>El ping&uuml;ino, de un a&ntilde;o de edad, fue visto por &uacute;ltima vez <strong>d&aacute;ndose un ba&ntilde;o en la desembocadura del r&iacute;o</strong> Edogawa, cerca de la bah&iacute;a de Tokio, tan tranquilamente.</p>
<p>Seg&uacute;n cuenta uno de los funcionarios del Tokyo Sea Life Park, el ave debi&oacute; escapar sorteando una de las paredes de piedra del recinto, aunque no entiende muy bien c&oacute;mo lo hizo.</p>
<p>&ldquo;Por supuesto que no puede volar, pero a veces la vida silvestre es capaz de desarrollar un &ldquo;explosivo poder&rdquo; cuando se ve amenazada por algo. Quiz&aacute;s corri&oacute; hacia la roca despu&eacute;s de asustarse por alg&uacute;n motivo&rdquo;, dijo.</p>
<p>Los funcionarios del zool&oacute;gico llevan desde la semana pasada recorriendo el &aacute;rea en la que el ping&uuml;ino fue visto por &uacute;ltima vez, con la intenci&oacute;n de capturarlo y llevarlo nuevamente al zoo.</p>
<p><br />
	&ldquo;Es bastante costoso atraparlo cuando est&aacute; nadando, debido a que lo hace a una velocidad tremenda. Estamos esperando que suba a tierra para dormir para poder capturarlo.&rdquo;</p>

