España - Así lo denuncian en una campaña lanzada ayer en la que han participado personajes tan conocidos como las actrices Adriana Ozores y Tina Sainz, los actores Fernando Tejero y Miguel Ángel Muñoz y el anterior Defensor del Menor de la Comunidad de Madrid, Pedro Núñez Morgades etc.

Al parecer la tendencia de abandonos se mantiene similar a la de años anteriores y el número de animales que se regalarán podría verse incrementado por las compras compulsivas a medida que se acercan las fiestas. 

Cada año, miles de perros, gatos y otros animales no-humanos son abandonados y en su mayoría mueren de hambre, enfermedad, atropellados, víctimas de malos tratos y/o asesinados en perreras de nuestra geografía. El motivo por el cual se les abandona es una mentalidad que considera que los intereses de los animales no-humanos no merecen la misma consideración que los intereses humanos -especismo-, mentalidad que solo se ve reforzada por la mercantilización de sus vidas cuando se compra o vende a estos individuos.

Los animales no-humanos merecen que se les trate con respeto, la misma clase de respeto con el que entendemos debemos tratar a los humanos, un respeto que pasa por no comerciar con sus vidas críandolos para nuestro consumo ni apoyando este negocio con nuestro dinero. 
