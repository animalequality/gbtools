<div class="center"><img class="wp-image-11990" src="/app/uploads/2017/05/1bdd7f6e-2757-48ed-9e3b-50c2f99a7f49.jpg"></div>
Los investigadores de Igualdad Animal grabaron cómo <strong>los pollitos son golpeados con palas y desechados vivos</strong> en una granja de pollos alemana durante marzo de 2017. Igualdad Animal ha presentado cargos contra esta granja.

El material recogido por las cámaras ocultas durante varios días registró <strong>escenas de maltrato animal</strong>. Se muestra cómo una persona pisotea un animal, intentando matarlo.
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/zyxWhuFCFmY" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
También es habitual ver cómo tiran montones de pollitos vivos en carretillas y cubos, los mismos que serán asfixiados por los demás pollos muertos que seguirán tirando sobre ellos. Otra persona golpea varias veces con el pico de la pala a un pollito, hiriéndole gravemente. Después lo tira a la basura, aún vivo. Los investigadores encuentran horas más tarde a este pollito con graves lesiones en el contenedor.
&nbsp;

<img class="wp-image-11991" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px;" src="/app/uploads/2017/05/1bdd7f6e-2757-48ed-9e3b-50c2f99a7f49_0.jpg" alt="">

«El pollito que encontramos en el contenedor tenía una pata rota, y un ala tan lastimada que los huesos sobresalían»&nbsp;indica Ria Rehberg, co-directora de Igualdad Animal en Alemania. Y añade «En estas condiciones el pobre animal fue abandonado para morir entre muchos otros pollitos muertos. Este nivel de crueldad contra una criatura tan vulnerable es sobrecogedor».

Esta granja alemana dispone de <strong>224.000 pollitos con el fin de engordarlos</strong>. Según muestra la investigación, los consumidores de carne de pollo no pueden confiar en la industria ganadera basándose en la ley de protección animal. "Además, la supervisión oficial suele fallar" dice Ria Rehberg.

Igualdad Animal ha solicitado a las autoridades <strong>llevar ante la justicia</strong> a las personas que han sido filmadas como responsables de este abuso animal.

&nbsp;
<ul>
 	<li>Galería fotográfica: &nbsp;<a href="https://www.flickr.com/photos/animalequalityde/sets/72157683138690476/" target="_blank" rel="noopener noreferrer">https://flic.kr/s/aHskYJA94N</a>&nbsp;</li>
</ul>
