<div class="center"><img class="wp-image-10217" src="/app/uploads/2015/02/15244008749_0a65c77bdd_m.jpg" /></div>
<p><img alt="" class="wp-image-13716" src="/app/uploads/2015/02/Mapaches.jpg" style="float:right; margin-left:5px; margin-right:5px; width:450px" />La investigaci&oacute;n de Igualdad Animal sobre el comercio de piel de perro mapache en China aparece hoy en el <a href="http://www.dailymail.co.uk/news/article-2867219/Inside-Chinese-fur-farms-breed-raccoon-dogs-tiny-cages-skin-alive-make-luxury-coats-sold-West.html">diario ingl&eacute;s Daily Mail.</a></p>

<p>Investigadores de la organizaci&oacute;n internacional se infiltraron en mafias en la provincia de Shandong para documentar la brutalidad a la que se somete a estos animales para comerciar su piel.</p>

<p>En las im&aacute;genes obtenidas puede verse c&oacute;mo los trabajadores utilizaban dos barras conectadas a la bater&iacute;a de un coche para electrocutar a los perros mapache. Una barra era introducida por la boca y la otra por el ano.</p>

<p>La descarga paralizaba a los animales pero no les mataba. Eran despellejados mientras a&uacute;n segu&iacute;an con vida.</p>

<p><img alt="" class="wp-image-13715" src="/app/uploads/2015/02/Mapache2.jpg" style="float:left; margin-left:5px; margin-right:5px; width:125px" />Abrigos de piel de los perros mapache, etiquetados como &ldquo;Asiatic raccoon&rdquo;, de estas y otras granjas similares, se venden en las principales tiendas de todo el mundo, sin que la gente se de cuenta de que est&aacute;n fabricadas de perro mapache &ndash; un c&aacute;nido - , al igual que los perros dom&eacute;sticos.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/BO9cFPlr6FQ" width="885px"></iframe></div>

