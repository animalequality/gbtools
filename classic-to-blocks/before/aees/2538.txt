Londres - EFE, Treinta personas fueron detenidas en unos registros practicados en varias viviendas del Reino Unido, Bélgica y Holanda en relación con actividades delictivas de defensores "radicales" de los animales, informó la Policía.

Según una portavoz de la Policía de Hampshire (sur de Inglaterra), que coordina esta operación, los sospechosos han sido llevados a distintas comisarías para ser interrogados.

Unos trescientos agentes tomaron parte en la operación, que forma parte de una investigación sobre la actividad delictiva vinculada a los defensores radicales de los animales, precisó la portavoz.

"Los agentes policiales cumplen con órdenes judiciales para entrar y registrar establecimientos en treinta domicilios en el Reino Unido y Europa", añadió la fuente.

"Las órdenes se han obtenido como parte de una investigación en curso sobre la actividad criminal asociada al extremismo de los (defensores de los) derechos de los animales", puntualizó la portavoz, y agregó que la operación se ha llevado a cabo por fuerzas del orden británicas y de otros países de la Unión Europea.

Uno de los establecimientos registrados es el centro de rescate animal de Freshfields, en Merseyside (noroeste de Inglaterra), que recibe animales abandonados en esa zona, señaló la fuente.

Fuente: Eldia.es

Igualdad Animal irá ampliando esta noticia en cuanto tengamos más información.

