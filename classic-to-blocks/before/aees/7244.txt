<p><img alt="" class="wp-image-13828" src="/app/uploads/2015/03/cabeceratheghosts.jpg" style="margin:10px; width:885px" /></p>

<p><img alt="" class="wp-image-14030" src="/app/uploads/2015/03/ghosts1.jpg" style="float:right; margin-left:10px; margin-right:10px; width:450px" /></p>

<p>El pr&oacute;ximo <strong>mi&eacute;rcoles 25 de marzo a las 19 horas en la Facultad de Filosof&iacute;a de Santiago de Compostela</strong>, Igualdad Animal proyectar&aacute; el aclamado documental &quot;<a href="https://www.theghostsinourmachine.com/"><strong>The Ghosts In Our Machine</strong></a>&quot;. El documental, <strong>considerado como uno de los mejores de tem&aacute;tica animalista realizados hasta la fecha</strong>, est&aacute; dirigido por Liz Marshall y protagonizado por la prestigiosa fot&oacute;grafa y activista Jo-Anne McArthur, y nos hace reflexionar sobre nuestra relaci&oacute;n con los animales en un mundo donde son condenados a una vida de miseria y desolaci&oacute;n.<br />
&nbsp;<br />
Jo-Anne recorre el mundo con su c&aacute;mara documentando la situaci&oacute;n de los animales en granjas peleteras, mataderos, circos o zoos. Tambi&eacute;n documenta la esperanza a trav&eacute;s de sus trabajos en refugios y santuarios donde los animales pueden vivir una vida lejos del maltrato.<br />
&nbsp;<br />
El documental <a href="http://www.theghostsinourmachine.com/awards/">ha ganado varios premios</a> y ha recibido buenas cr&iacute;ticas en Canad&aacute; y Estados Unidos.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/u5vYH4KlHBs" width="885px"></iframe></div>

<p>&nbsp;</p>

