<p>Memphis Meats acaba de presentar en exclusiva para el Wall Street Journal su &uacute;ltimo logro de &laquo;carne limpia&raquo;: nuggets de pollo y pato producidas a partir de agricultura celular.</p>

<p>La presentaci&oacute;n de estos productos marcan un antes y un despu&eacute;s en el desarrollo de la tecnolog&iacute;a de agricultura celular. La llamada <a href="https://igualdadanimal.org/noticia/2016/10/18/has-oido-hablar-de-la-carne-limpia/" target="_blank">carne limpia</a> se produce a partir de <strong>c&eacute;lulas alimentadas con los mismos nutrientes con los que se alimentan los animales y que son capaces de reproducirse por s&iacute; mismas</strong>. Estas c&eacute;lulas son extra&iacute;das de animales, sin que tengan que morir en el proceso.</p>

<p>&nbsp;</p>

<p><font size="5"><font color="#808080">&laquo;Esta es la primera vez en la historia que se ha logrado algo as&iacute;.&raquo;</font></font></p>

<p><font size="5">&nbsp;</font></p>

<p>&iquest;Cu&aacute;nta carne limpia se puede producir a partir de estas c&eacute;lulas? Seg&uacute;n los c&aacute;lculos manejados por los desarrolladores de la tecnolog&iacute;a, una &uacute;nica c&eacute;lula puede dar lugar a 75 generaciones de divisiones en solo tres meses. Esto significa que, pongamos por ejemplo, <strong>una &uacute;nica c&eacute;lula de un pollo podr&iacute;a generar suficiente carne para producir 20 billones de nuggets</strong>.</p>

<p><font size="5">&nbsp;</font></p>

<p><font size="5"><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</font></font></font></p>

<p><font size="5"><font size="5">&nbsp;</font></font></p>

<p><a href="https://upsidefoods.com/" target="_blank">La firma Memphis Meats</a> acapar&oacute; la atenci&oacute;n internacional presentando su carne sin sufrimiento animal en febrero de 2016. En aquella ocasi&oacute;n era carne de ternera, pero ahora nos presentan su carne de pollo y pato. En <a href="https://www.wsj.com/articles/startup-to-serve-up-chicken-strips-cultivated-from-cells-in-lab-1489570202?utm_source=Mailing+List&amp;utm_campaign=b8b36a5ccb-EMAIL_CAMPAIGN_2017_03_15&amp;utm_medium=email&amp;utm_term=0_84ce6c8122-b8b36a5ccb-190621565" target="_blank">el v&iacute;deo promocional en el Wall Street Journal</a> observamos c&oacute;mo se cocina para luego ser probada por unos comensales que quedan sorprendidos ante su similitud con la carne proveniente de animales.</p>

<p>El objetivo de la startup californiana es <strong>reinventar la ganader&iacute;a</strong>, reemplazando no solo a los millones de vacas, cerdos o pollos criados y sacrificados cada a&ntilde;o en el mundo, sino el grano y el agua con los que se les alimenta, as&iacute; como el despilfarro de recursos.</p>

<p><font size="5"><font size="5"><img alt="" class="wp-image-11807" src="/app/uploads/2017/03/1489630728580.jpg" style="margin: 0px 10px; float: left; width: 450px; " /></font></font></p>

<p>El presidente y fundador de la firma, Uma Valeti, declara, &laquo;En Memphis Meats producimos carne limpia usando 1/10 del agua y 1/100 de la tierra que se requiere para criar a los animales. Pero tambi&eacute;n hablamos de reducir el consumo de energ&iacute;a a menos de la mitad.&raquo;</p>

<p>&laquo;Esta es la primera vez en la historia que se ha logrado algo as&iacute;. Nos hemos centrado en las millones de personas en el mundo a las que les encanta comer pollo. La carne de pollo es la m&aacute;s consumida en el mundo y, si miramos al futuro, podr&iacute;amos cubrir la demanda cuando empecemos a producir a gran escala.&raquo;</p>

<p>&nbsp;</p>

<p><font size="1"><font size="1"><font color="#808080">El equipo de Memphis Meats observando c&oacute;mo es preparada la primera nugget de carne limpia del mundo. Imagen cortes&iacute;a de Memphis Meats.</font> </font></font></p>

<p>&nbsp;</p>

<p>&laquo;Estamos planeando llevar nuestros productos a los supermercados estadounidenses en el 2021.&raquo;</p>

<p>&nbsp;</p>

<p><font size="1"><font size="1"><font size="1">Fuente: <a href="https://www.wsj.com/articles/startup-to-serve-up-chicken-strips-cultivated-from-cells-in-lab-1489570202" target="_blank">https://www.wsj.com/articles/startup-to-serve-up-chicken-strips-cultivated-from-cells-in-lab-1489570202</a> </font></font></font></p>

