Aparecía hoy en los informativos y periódicos nacionales imágenes impactantes de los caballos que se encontraban en la finca de José Antonio Roca, presunto cerebro de la trama de corrupción del ayuntamiento de Marbella.

Según informaban los medios y parecía desprenderse de un certificado veterinario y de las imágenes mostradas, los caballos se estaban muriendo por dejadez alimentaria, habiendo muerto ya al parecer 3 de los 103 debido a la falta de alimento y atención sanitaria.

La Junta de Andalucía, y la presidenta de Asociación CYD Santa María han asegurado en cambio que los caballos "se mantienen en buenas condiciones higiénico-sanitarias" y que los equinos enfermos están recibiendo el tratamiento veterinario correspondiente. 

No debemos pasar por alto que estos caballos se encontraban en la finca de Roca después de haber sido criados por su condición de "puras razas" y para su utilización, residiendo para algunos parte o todo el valor de estos individuos en el precio que tendrían en el mercado, algo que muchos medios de información han reforzado con comentarios del tipo "de los 103 caballos de pura raza valorados en más de tres millones de euros".

Desde Igualdad Animal y como organización animalista que se manifiesta contra la utilización de otros seres sintientes, queremos dejar claro que el valor de estos individuos no reside en su raza, en el precio que alcanzarían en el mercado, ni tampoco en el uso que podamos hacer de ellos, sino únicamente en su capacidad para sentir, y por lo tanto deben ser respetados por el hecho de que poseen interés en vivir y en no sufrir, y por el valor que tiene para ellas/os -y no para otras/os- su vida.

