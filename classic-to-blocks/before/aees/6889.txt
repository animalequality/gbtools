<p align="justify">El peso de la nieve hizo ceder el tejado de una granja de San Xo&aacute;n de R&iacute;o (Ourense), sepultando bajo ella a cerca de<strong> seis mil pavos</strong>. El suceso se produjo a media ma&ntilde;ana de ayer mi&eacute;rcoles.</p>

<p align="justify"><img alt="" class="wp-image-13370" src="/app/uploads/2018/10/8699939861_a6d183c09b_z.jpg" style="width: 202px; float: left; margin: 5px;" />Uno de los titulares de la explotaci&oacute;n explic&oacute; que todo ocurri&oacute; en cuesti&oacute;n de minutos. <em>&laquo;Salt&oacute; la alarma en el ordenador de mi casa, procedente de la granja, por la falta de corriente. Fui hasta all&iacute; y me encontr&eacute; con la mitad del tejado en el suelo&raquo;. </em>Aunque varias personas intentaron apuntalar la estructura, esta se vino abajo por el peso de unos 35 cent&iacute;metros de nieve.</p>

<p align="justify">Esta granja, que se dedica a la cr&iacute;a de pavos, vende los animales a la empresa Coren desde hace&nbsp;9 a&ntilde;os y es <strong>propietaria de otra nave con similar cantidad de pavos</strong>.</p>

