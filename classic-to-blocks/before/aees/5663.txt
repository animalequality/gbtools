<div>
	<p>
		<br />
		Saced&oacute;n, Guadalajara. &nbsp;Hoy, 1 de Septiembre, sobre las 11:30 aproximadamente, activistas de Igualdad Animal y periodistas de Telecinco, han sufrido agresiones brutales por parte de los participantes en el encierro por el campo que se celebraba en <span class="Apple-tab-span"> </span>la localidad de Saced&oacute;n, en Guadalajara. &nbsp;</p>
	<p>
		&nbsp;</p>
	<p>
		Los activistas han desplegado una pancarta de 35 metros cuadrados del puente donde se estaba celebrando el encierro, que le&iacute;a: &quot;Tauromaquia abolici&oacute;n, derechos para todos los animales&quot;.&nbsp;</p>
	<p>
		<br />
		A los pocos minutos los activistas y un reportero y una redactora de Telecinco que estaban presentes, han sido agredidos por una multitud, pu&ntilde;etazos, patadas, piedras lanzadas por habitantes de la localidad... En medio de esa multitud, han intentado meterse en el coche que le esperaba para salir de all&iacute;, al cual han roto tanto la luna delantera como la trasera. Al final pudieron salir custodiados por un coche de la Guardia Civil.&nbsp;</p>
	<p>
		&nbsp;</p>
	<p>
		Este suceso s&oacute;lo pone de manifiesto de qu&eacute; lado est&aacute; la violencia. Ante una protesta pac&iacute;fica, en la que simplemente se ha desplegado una pancarta, hemos sido v&iacute;ctimas de un linchamiento p&uacute;blico, en algunos aspectos similar al que ha sufrido el toro, con la diferencia que nosotros estamos vivos y el toro no.&nbsp;</p>
	<p>
		&nbsp;</p>
	<p>
		Desde Igualdad Animal queremos focalizar la atenci&oacute;n en las verdaderas v&iacute;ctimas, que son los animales que son masacrados en este tipo de eventos,&nbsp; y en general en todas las v&iacute;ctimas que diariamente est&aacute;n sufriendo y agonizando en granjas, mataderos, laboratorios, sufriendo la explotaci&oacute;n a las que los sometemos.&nbsp; Al igual que son injustificables las agresiones que hemos sufrido por querer defender a los animales, pedimos que la solidaridad se extienda a todos los animales, pues todos tenemos inter&eacute;s en disfrutar de nuestra vida y no queremos morir ni ser agredidos.&nbsp;</p>
	<p>
		<br />
		<span class="Apple-style-span">Galer&iacute;a&nbsp;Fotogr&aacute;fica:&nbsp;<a href="void(0)/*256*/">http://www.flickr.com/photos/igualdadanimal/sets/72157624732970431/</a></span></p>
	Noticia de Telecinco: <a href="https://vimeo.com/14609916">https://vimeo.com/14609916</a>
	<p>
		V&iacute;deo de las agresiones: <a href="https://vimeo.com/14614491">https://vimeo.com/14614491</a></p>
	<p>
		<br />
		&nbsp;</p>
	<p>
		&nbsp;</p>
	<p>
		&nbsp;</p>
	<p>
		&nbsp;</p>
</div>

