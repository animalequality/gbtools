Más de 100 buzos mataron esta semana a 40.000 erizos en la zona de la Bahía de El Confital, en la playa canaria de Las Canteras.

El festival musical Aguaviva organizó estas muertes, que tuvieron lugar durante toda la mañana del domingo en la zona de 'Bajo Fernando', en Las Palmas de Gran Canaria, con colaboración de la Obra Social de La Caja de Canarias.

Los animales depredadores de los erizos, como el mero o el sargo, entre otros, han ido desapareciendo de la costa a causa de la pesca. Ello ha provocado un desequilibrio ecológico, que provoca que los erizos contribuyan a eliminar la vegetación marina. Hay que señalar que la pesca es una actividad rechazable de por sí, ya que supone la muerte de animales, quienes merecen respeto a sus intereses.

Ya el pasado 1 de mayo se realizó una acción similar en la costa de Sardina del Norte, en el municipio grancanario de Gáldar, que se saldó con la muerte de más de 20.000 erizos.

Fuente: Terra Actualidad - Europa Press 
