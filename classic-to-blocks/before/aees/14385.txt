La mujer propietaria del gato (actualmente los animales no-humanos tienen el estatus legal de propiedad) esperó en el aeropuerto de Ibiza, tras su viaje, a que llegase Pancho, el gato, pero la jaula en la que viajaba llegó vacía.

En septiembre recibió la llamada de una trabajadora del aeropuerto que le abría nuevas esperanzas. Habían visto a su gato, o a uno que encajaba con las descripciones de los carteles que había colgado. Cristina llamó a una amiga de la isla que fue esa misma noche a buscarlo. Consiguió verlo, aunque en una zona restringida del aeropuerto. Cristina cogió el primer avión que pudo. También pudo verlo, aunque estaba al otro lado de una valla de la zona restringida y se marchó.

Un juzgado comercial de Barcelona ha condenado a Iberia, la línea en la que se produjo la desaparición, a pagar una indemnización de 711,28 euros por la pérdida y daños morales. El juez reconoció los daños morales y fijó la indemnización en 500 euros. Pero obligaba también a Iberia a pagar los 180 euros del segundo viaje, la indemnización de la pérdida y el billete del gato. Según el juez, no se podía "deducir un vínculo especial, diferente del que genéricamente tiene una persona por su mascota", señalando además que "sólo" habían convivido durante dos años.

Cristina, la mujer que era propietario del gato, dijo al respecto: "A mí lo que me gustaría es estar con Pancho. La madre del gato y yo lo pasamos bastante mal con su desaparición. Es importante que las compañías tengan en cuenta a los animales, no son equipaje".

Iberia alegó que la gata que ahora vive con Cristina "puede seguir teniendo gatitos y suplir el dolor que la pérdida le ha ocasionado". Algo a lo que contestó Cristina diciendo: "No se trata de una mercancía y nada puede suplir a Pancho".
