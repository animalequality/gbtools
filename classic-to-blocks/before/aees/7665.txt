<p>Ma&ntilde;ana, 25 de enero, los miembros del Comit&eacute; de Agricultura del Parlamento Europeo <strong>votar&aacute;n&nbsp;una propuesta informe del eurodiputado Stefan Eck que podr&iacute;a allanar el camino para una legislaci&oacute;n que proh&iacute;ba las granjas de conejos en bater&iacute;a</strong>.</p>

<p>Cada a&ntilde;o, <strong>se mata a 320 millones de conejos en Europa</strong> para obtener su carne, y en la actualidad no hay ninguna disposici&oacute;n jur&iacute;dica que los proteja. Esto se traduce en que <strong>el 99 %</strong> de los conejos son encerrados en jaulas del tama&ntilde;o de un folio de papel.</p>

<p><img alt="" class="wp-image-11565" src="/app/uploads/2017/01/12947131175_7a1474cea5_z.jpg" style="float:left; margin:10px; width:600px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Igualdad Animal ha documentado en repetidas ocasiones el terrible sufrimiento que padecen los conejos en jaulas de bater&iacute;a en Espa&ntilde;a e Italia &mdash;en donde se encuentra el 60 % de las granjas de conejos de la UE&mdash;. Tan solo en Espa&ntilde;a, investigamos 70 granjas de conejos y 4 mataderos; y en todas y cada una de ellas hallamos pruebas de <strong>una crueldad extrema</strong>.</p>

<p>Nuestros investigadores vieron conejos con<strong> heridas abiertas, dolorosas infecciones oculares e incluso conejos con mordiscos en las orejas de otros conejos angustiados</strong>. Debido al hacinamiento&nbsp;y a las condiciones antinaturales, hasta el 30 % de los conejos de granja mueren o los matan antes de llegar al matadero &mdash;es el porcentaje m&aacute;s elevado de todos los animales de granja&mdash;.</p>

<p>&nbsp;</p>

<iframe width="854" height="480" src="https://www.youtube.com/embed/UN_gr4vPy7c" frameborder="0" allowfullscreen></iframe>

<p>&iexcl;T&uacute; puedes ayudar a acabar con este maltrato a los conejos!</p>

<p>En junio de 2016, presentamos pruebas de nuestras investigaciones en el Parlamento Europeo junto a la organizaci&oacute;n <a href="https://www.ciwf.org.uk/" target="_blank">Compassion in World Farming</a>; y ahora los miembros del Comit&eacute; de Agricultura est&aacute;n debatiendo una propuesta para prohibir en toda Europa las jaulas en bater&iacute;a para los conejos. Si aprueban el informe se votar&aacute; por todos los miembros del Parlamento Europeo este mismo a&ntilde;o.</p>

<p><strong>&iexcl;ACT&Uacute;A!</strong></p>

<p><strong>Necesitamos que los representantes m&aacute;s importantes de nuestro pa&iacute;s sepan que queremos que apoyen la prohibici&oacute;n</strong>. Por favor, t&oacute;mate un minuto para escribir un correo a los representantes de Espa&ntilde;a pidi&eacute;ndoles que aprueben el informe y apoyen la prohibici&oacute;n de las jaulas para los conejos.</p>

<p>Clara Eugenia Aguilera: <a href="mailto:claraeugenia.aguileragarcia@europarl.europa.eu">claraeugenia.aguileragarcia@europarl.europa.eu</a></p>

<p>Esther Herranz: <a href="mailto:claraeugenia.aguileragarcia@europarl.europa.eu">esther.herranzgarcia@europarl.europa.eu</a></p>

<p>Gabriel Mato <a href="mailto:claraeugenia.aguileragarcia@europarl.europa.eu">gabriel.mato@europarl.europa.eu</a></p>

<p>Ram&oacute;n Luis Valc&aacute;rcel: <a href="mailto:claraeugenia.aguileragarcia@europarl.europa.eu">ramonluis.valcarcel@europarl.europa.eu</a></p>

<p>&nbsp;</p>

<p>&iexcl;Por favor, no desaproveches la oportunidad de pronunciarte en contra de las granjas industriales de conejos!</p>

