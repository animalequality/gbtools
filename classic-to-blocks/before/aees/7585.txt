<div class="center"><img class="wp-image-11115" src="/app/uploads/2016/10/29104127350_88785ff1cf_z.jpg" /></div>
<p>Ac&eacute;ptalo: los huevos que compramos en el supermercado no proceden de gallinas felices porque en la ganader&iacute;a industrial no hay animales felices.</p>

<p><strong>Igualdad Animal M&eacute;xico</strong> acaba de lanzar la campa&ntilde;a &laquo;La vida en una jaula&raquo;, exponiendo ante los consumidores <strong>la realidad de la producci&oacute;n de huevos</strong> en el pa&iacute;s. En la nueva website de la campa&ntilde;a <a href="https://lavidaenunajaula.igualdadanimal.mx/" target="_blank">se puede firmar la petici&oacute;n</a> pidiendo el fin de las jaulas.</p>

<p>El 90% de los huevos puestos a la venta en M&eacute;xico proceden de gallinas que <strong>viven toda su vida encerradas en min&uacute;sculas jaulas de alambre</strong>. &iquest;<a href="https://igualdadanimal.org/noticia/2016/06/07/nuestra-alimentacion-produce-una-huella-de-sufrimiento/" target="_blank">Puede ser alguien feliz</a> viviendo en esas condiciones?</p>

<p>La campa&ntilde;a muestra a los consumidores investigaciones llevadas a cabo en granjas industriales de Jalisco, principal regi&oacute;n productora de M&eacute;xico y en la que se produce <strong>el 50% de todos los huevos comercializados en el pa&iacute;s</strong>.</p>

<p>La industria del huevo mexicana <strong>no es diferente de la de otros pa&iacute;ses</strong>. Los<a href="http://www.igualdadanimal.org/noticias/7399/hola-soy-helen-y-mi-destino-era-vivir-enjaulada-hasta-la-muerte" target="_blank"> m&eacute;todos industrializados de producci&oacute;n</a> est&aacute;n dise&ntilde;ados para producir el mayor n&uacute;mero de huevos posible al m&iacute;nimo coste. Por desgracia, quien acaba pagando el mayor coste son estas fr&aacute;giles y sensibles aves, condenadas a vivir en jaulas tan peque&ntilde;as que ni siquiera pueden extender sus alas.</p>

<p><img alt="" class="wp-image-11117" src="/app/uploads/2016/10/28770589393_6d86822bce_o.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>El <a href="https://igualdadanimal.org/noticia/2016/05/24/el-maltrato-animal-que-si-aceptamos/" target="_blank">maltrato animal</a> est&aacute; omnipresente en la ganader&iacute;a industrial. El motivo es que para abastecer la demanda de carne, l&aacute;cteos o huevos de miles de millones de consumidores en todo el mundo <strong>los m&eacute;todos de producci&oacute;n no tienen en cuenta las necesidades naturales de los animales</strong>.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Los m&eacute;todos industrializados de producci&oacute;n est&aacute;n dise&ntilde;ados para producir el mayor n&uacute;mero de huevos posible al m&iacute;nimo coste&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>De esta forma se les condena a <strong>vidas miserables en granjas industriales</strong> en las que no pueden ni siquiera satisfacer sus instintos.</p>

<p>Puedes ver el v&iacute;deo de la campa&ntilde;a &laquo;La vida en una jaula&raquo; de Igualdad Animal M&eacute;xico y firmar la petici&oacute;n <a href="https://lavidaenunajaula.igualdadanimal.mx/" target="_blank">aqu&iacute;</a>.</p>

<p><strong>Si quieres estar al tanto de c&oacute;mo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n</strong>.</p>

