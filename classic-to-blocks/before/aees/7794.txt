<div class="center"><img class="wp-image-12030" src="/app/uploads/2017/05/foto_fb_25.jpg" /></div>
<p>Campofr&iacute;o prepara en secreto (bueno, ya no TAN TAN secreto), el lanzamiento de una l&iacute;nea de productos&hellip; &iexcl;vegetarianos!</p>

<p>Lo que nos hace recordar algo: &iquest;os acord&aacute;is del anuncio de las Fin&iacute;ssimas de Campofr&iacute;o? S&iacute; s&iacute;, ese en el que <strong>se caricaturizaba a una familia de vegetarianos</strong> que ve&iacute;an con espanto c&oacute;mo su hijo adolescente quer&iacute;a comer carne.</p>

<p>En concreto, nos referimos a este anuncio:</p>

<p><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/bz3wMkbKgyo" width="854"></iframe></p>

<p>Bueno, pues parece que la empresa c&aacute;rnica ha pensado que, tal vez, despu&eacute;s de todo <strong>no era tan buena idea</strong> re&iacute;rse de los consumidores que quieren introducir alternativas a la carne en su alimentaci&oacute;n.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>Sum&aacute;ndose a otras marcas y supermercados, <strong>Campofr&iacute;o est&aacute; preparando el lanzamiento de embutido vegetal</strong> en una nueva l&iacute;nea que se llamar&aacute;&hellip; &iexcl;Vegalia!</p>

<p><img alt="" class="wp-image-12031" src="/app/uploads/2017/05/vegalia.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Seg&uacute;n informa Econom&iacute;a Digital en su website, la otrora empresa <strong>que se re&iacute;a de los consumidores que compraban alternativas a la carne</strong>, ha solicitado <a href="https://www.empresia.es/distintivo/campofrio-vegalia/oepm/m-3658524/" target="_blank">la reserva de la marca</a> en el registro de la propiedad y ya tiene listo el dise&ntilde;o y la presentaci&oacute;n de sus alternativas a la carne.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://www.economiadigital.es/tecnologia/campofrio-copia-mercadona-embutido-vegetal_406560_102.html" target="_blank">Campofr&iacute;o copia a Mercadona con su primer embutido vegetal</a></strong>

<p>&nbsp;</p>

<p>Los tiempos cambian, &iquest;eh, Campofr&iacute;o?</p>

<p>La empresa productora de las Fin&iacute;ssimas <strong>lleva con p&eacute;rdidas continuas de facturaci&oacute;n desde el a&ntilde;o 2013</strong> y est&aacute; intentando captar al creciente segmento de consumidores que prefieren opciones m&aacute;s sanas que la carne.</p>

<p>La marca se suma as&iacute; a <strong>otras empresas como Noel o Mercadona</strong>, que est&aacute;n comercializando alternativas a la carne procesada y platos preparados vegetarianos.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://www.lavanguardia.com/comer/tendencias/20170508/422388073424/por-que-alemania-encabeza-la-revolucion-de-la-comida-vegana.html" target="_blank">&iquest;Por qu&eacute; Alemania encabeza la revoluci&oacute;n de la comida vegana?</a></strong>

<p>&nbsp;</p>

<p>La tendencia a comer m&aacute;s platos y opciones vegetarianas no se limita a Espa&ntilde;a. De hecho, Espa&ntilde;a se encuentra en los puestos de cola en cuanto a comercializaci&oacute;n de alternativas a la carne. <a href="https://gastronomiaycia.republica.com/2017/05/04/alemania-es-el-pais-que-mas-nuevos-productos-veganos-lanzo-al-mercado-en-2016/" target="_blank">Un reciente informe</a> sobre tendencias de consumo ha se&ntilde;alado a Alemania como el pa&iacute;s <strong>con m&aacute;s lanzamientos de productos y negocios vegetarianos</strong>, seguido por EE. UU. y Reino Unido.</p>

