<p><img alt="" class="wp-image-13816" src="/app/uploads/2013/02/bebe_raton_p.jpg" style="text-align: justify; width: 220px; float: left;" /></p>
<p align="justify"><strong>Dos mil cr&iacute;as de rat&oacute;n muertas</strong> y <strong>rellenas de analg&eacute;sicos</strong> ser&aacute;n lanzadas esta primavera desde helic&oacute;pteros sobre la jungla de Guam, en el pac&iacute;fico, con la intenci&oacute;n de envenenar a la poblaci&oacute;n de serpientes arb&oacute;reas de la isla.<br />
	<br />
	Cada peque&ntilde;o rat&oacute;n llevar&aacute; embutida en la boca una dosis paracetamol, altamente t&oacute;xico para las serpientes, y ser&aacute; lanzado en un peque&ntilde;o paraca&iacute;das de papel para quedar as&iacute; pendido de las ramas de los &aacute;rboles sin llegar al suelo, donde el medicamento podr&iacute;a afectar a animales de otras especies.</p>
<p><img alt="" class="wp-image-9920" src="/app/uploads/2013/02/culebra_arborea1.jpg" style="margin-left: 20px; width: 220px; float: right;" /></p>
<p align="justify">En un intento de proteger a la fauna aut&oacute;ctona, las autoridades han decidido <strong>exterminar </strong>a este reptil, llegado a la isla en las bodegas de los barcos estadounidenses tras la Segunda Guerra Mundial.</p>
<p align="justify">Aunque las culebras arb&oacute;reas no presentan peligro alguno para los humanos, su impacto sobre la fauna local ha supuesto un <strong>descenso del turismo, con efectos econ&oacute;micos negativos para la isla</strong>.</p>
<p>&nbsp;</p>
<p align="justify">La<a href="http://es.wikipedia.org/wiki/Territorios_no_incorporados_de_los_Estados_Unidos#Guam" target="_blank"> isla de Guam</a> es un &ldquo;territorio no incorporado&rdquo; de EE.UU., convertido en base militar norteamericana tras su entrega por parte de<a href="https://es.wikipedia.org/wiki/Guerra_hispano-estadounidense" target="_blank"> Espa&ntilde;a en 1898</a>.&nbsp;</p>
<p>&nbsp;</p>

