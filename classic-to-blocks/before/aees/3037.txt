Los negocios en torno de las "mascotas" vienen creciendo a pasos acelerados en los últimos tiempos. Con una población con fuerte presencia en las urbes, las mascotas, fundamentalmente perros y gatos, son los nuevos clientes que hicieron emerger este negocio, que empezó con fuerza hace unos diez años y se afianzó en los últimos cinco.

Este desarrollo tiene un fuerte contenido social: cada vez hay más personas que viven solas y muchas deciden tener una "mascota" que las acompañe. En general son perros y gatos pero también tienen aves, tortugas y hasta reptiles.

El principal negocio está fuertemente instalado en los alimentos balanceados, aunque se da una creciente incidencia de los elementos para el aseo y belleza animal.

Según datos que dio a conocer la Cámara Argentina de Empresas de Producción Animal (Caena), se calcula que la producción de balanceados para mascotas alcanzó en 2006 las 404.000 toneladas y su comercialización, entre ventas al mercado interno, al externo y las importaciones (unas 1.453 toneladas), representó unos US$ 210 millones.

El año pasado, consumo nacional se ubicó en las 300.000 toneladas y representó el 74,40 % del total. Lo interesante es que este número podría triplicarse debido a que sólo se cubre el requerimiento del 30 % de los animales, de los aproximadamente 9 millones de perros y 3,5 millones de gatos que en la actualidad hay en la Argentina como mascotas.

De todas maneras es común todavía, que las "mascotas" se alimenten parcial o totalmente de frutas, verduras y restos de comidas caseras, o incluso de otros animales vivos. En nuestro país [Argentina], se estima que menos de la mitad de las "mascotas" se alimentan con alimento balanceado, con lo cual las posibilidades de incremento del negocio son muy amplias.

Según un estudio realizado por la consultora Claves Información Competitiva, el Gran Buenos Aires es la zona donde más "mascotas" por hogar hay, ya que un 80% adoptó alguna. Le sigue Capital Federal con el 66%. El animal que mas alimento balanceado consume es el perro con 74,3% del total. Le siguen los gatos con 21,1% y los pájaros con 3,8%.

El relevamiento considera que entre las principales causas que explican el crecimiento de este negocio es el dinamismo que esta tomando el comercio exterior, la consolidación de la sustitución de importaciones y el aumento del poder adquisitivo de la población.

Fuente: La Razón [Argentina]


El comercio de animales supone, además de sufrimiento y muerte, difundir la idea de que los animales son objetos cuyos intereses no han de ser tenidos en cuenta. Por ello es necesario oponerse a la compraventa de animales. En el caso de que deseemos vivir con animales como perros o gatos, la postura ética es adoptarlos y considerarlos como a un miembro más de la familia.
