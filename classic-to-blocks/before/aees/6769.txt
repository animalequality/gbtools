<div class="center"><img class="wp-image-9754" src="/app/uploads/2012/10/granjas_noruega_p.jpg" /></div>
<p align="justify">Nuevas im&aacute;genes obtenidas por la organizaci&oacute;n <a href="http://www.dyrsfrihet.no/nyheter/nye-skrekkbilder-i-skjebnear-pelsdyrnaeringen" target="_blank"><strong>Nettverk for Dyrs Frihet</strong></a> (Red por la Libertad de los Animales) mediante inspecciones sin previo aviso en granjas peleteras noruegas vuelven a demostrar el sufrimiento continuo al que son expuestos a diario los animales en estos centros de explotaci&oacute;n.</p>
<p align="justify">Durante el verano y el oto&ntilde;o de 2012, activistas de Nettverk for Dyrs Frihet hicieron <strong>inspecciones sin previo aviso en 24 granjas de pieles</strong> de Troms y el norte y sur de Agder. En todas las granjas se encontraron animales heridos y sufriendo f&iacute;sica y psicol&oacute;gicamente.</p>
<p align="justify"><img alt="" class="wp-image-14171" src="/app/uploads/2012/10/peleteria_noruega.jpg" style="width: 580px; " /></p>
<p align="justify">&nbsp;</p>
<p align="justify">Algunos de los hallazgos incluyen <strong>animales con huesos a la vista</strong>, <strong>colas y orejas mordidas</strong>, <strong>graves heridas</strong> y <strong>conducta depresiva</strong>. En una granja de Hedmark se encontr&oacute; una zorra muerta, totalmente podrida, mientras sus hijos permanec&iacute;an junto a ella en la misma jaula.&nbsp;En Telemark se encontraron seis <strong>visones con las piernas amputadas</strong> debido a las mordidas.</p>
<p><em><strong>►Advertencia: el siguiente v&iacute;deo muestra im&aacute;genes de extremo sufrimiento</strong></em></p>
<p><iframe allowfullscreen="" frameborder="0" height="330" scrolling="no" src="https://www.youtube.com/embed/zRk4WebriDY" width="580"></iframe></p>
<blockquote>
	<ul>
		<li align="justify">
			Puedes visitar la <strong>galer&iacute;a fotogr&aacute;fica </strong>de esta investigaci&oacute;n <strong><a href="https://www.flickr.com/photos/dyrsfrihet/8119502893/in/set-72157631844184758" target="_blank">aqu&iacute;</a></strong></li>
	</ul>
</blockquote>
<h4>Declaraciones de uno de los granjeros investigados</h4>
<p>&nbsp;</p>
<p align="justify">Miembros de la FSA (autoridad competente en materia de producci&oacute;n gandera) visitaron la granja de Nissedal, en Telemark. Con m&aacute;s de 11.000 animales encerrados entre sus paredes, esta granja presentaba las mismas escenas de horror y sufrimiento registradas por NDF. Tras encontrar a varios animales con las piernas amputadas, <strong>la FSA decidi&oacute; procesar al responsable de la explotaci&oacute;n</strong>.</p>
<p align="justify"><em>&laquo;<strong>Ten&iacute;an un buen apetito, por lo que simplemente se me olvid&oacute; matarlos.</strong> Parec&iacute;a como si ellos disfrutaran. Com&iacute;an, y no mostraban se&ntilde;ales de que algo estuviera mal&raquo;</em>, afirmaba&nbsp;Ottar Tveit, propietario de la granja, al tratar de justificar la falta de atenci&oacute;n a los animales con miembros amputados.</p>
<p align="justify"><strong>El granjero sostiene que los animales de peleter&iacute;a se adaptan bien a la vida sin una pierna</strong>, y niega que sientan dolor del mismo modo que los humanos. Afirmaci&oacute;n que puede ser perfectamente rebatida por cualquier persona con un m&iacute;nimo de conocimiento en biolog&iacute;a, neurolog&iacute;a o veterinaria.</p>
<p align="justify"><em>&laquo;Los animales son relativamente simples. Si reciben una buena atenci&oacute;n y comida, duermen el resto del tiempo. <strong>No tienen la necesidad de viajar a Mallorca</strong>, por ejemplo, como podr&iacute;amos tener nosotros&raquo;,</em> afirmaba c&iacute;nicamente el granjero.</p>
<p align="justify">&nbsp;</p>
<p><img alt="" class="wp-image-14332" src="/app/uploads/2012/10/zorro_oreja_noruega.jpg" style="width: 580px; " title="Un zorro que ha perdido una de sus orejas" /></p>
<p align="justify">&nbsp;</p>
<p align="justify">En 2002, una comisi&oacute;n parlamentaria conjunta sostuvo la afirmaci&oacute;n de que la industria de la piel deber&iacute;a lograr mejoras significativas con el fin de evitar el cierre. Diez a&ntilde;os m&aacute;s tarde, los animales viven en las mismas condiciones y, actualmente, <strong>un comit&eacute; gubernamental est&aacute; estudiando si esta cruel industria ha de ser prohibida en Noruega.</strong></p>

