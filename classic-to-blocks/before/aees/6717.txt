<p align="justify">Tras 12 a&ntilde;os de limitaci&oacute;n del servicio de burro-taxis en Mijas debido a las molestias que las heces y orines de estos animales provocaban en los vecinos, un arriero ha dado con la &quot;soluci&oacute;n&quot; para continuar explotando a los burros como medio de transporte para los turistas: <strong>pa&ntilde;ales</strong>.</p>
<p><br />
	<img alt="" class="wp-image-14162" src="/app/uploads/2012/09/panales_burro.jpg" title="El sistema de pañales que permitirá utilizar a los burros en el casco histórico de Mijas" /></p>
<p align="justify">&nbsp;</p>
<p align="justify">El artefacto consiste en <strong>un recipiente de tela plastificada que se coloca bajo la cola de los burros</strong> e impide que los excrementos caigan al suelo. Adem&aacute;s, la nueva ordenanza municipal obliga a los arrieros a recoger las heces si &eacute;stas cayeran directamente a la calzada.</p>
<p align="justify">En el a&ntilde;o 2000 el equipo de gobierno municipal decidi&oacute; prohibir el paso los burros por las v&iacute;as principales del n&uacute;cleo urbano debido a los malos olores que &eacute;stos provocaban, emplazando el &quot;servicio&quot; a avenidas de circunvalaci&oacute;n. Ahora, y debido al uso de estos pa&ntilde;ales, los burro-taxi volver&aacute;n a circular por las principales calles del casco hist&oacute;rico de Mijas Pueblo, <strong>cargando a turistas venidos de todo el mundo sobre los lomos de estos animales durante los 20 minutos que dura el trayecto.</strong></p>
<p><img alt="" class="wp-image-9707" src="/app/uploads/2012/09/precios_burro_taxi.jpg" style="width: 580px; " title="Precios burro-taxi Mijas" /></p>
<p align="justify">&nbsp;</p>
<p align="justify"><em>&laquo;Este recorrido siempre ha tenido much&iacute;simas quejas de los clientes, turistas en su mayor&iacute;a, ya que es muy corto y no se ve nada de inter&eacute;s a lo largo de los diez minutos que dura. Estamos muy contentos con este cambio porque pensamos que puede ser muy beneficioso para el sector, que est&aacute; muy mal&raquo;</em>, asegur&oacute; uno de los arrieros.</p>
<p>&nbsp;</p>

