La organización internacional Igualdad Animal lanza <strong>este nuevo vídeo titulado «El Escobar y ElPozo: escándalo interminable»</strong>, tras haber tenido constancia de unas imágenes aéreas grabadas con dron en el recinto de <strong>la granja El Escobar en Fuente Álamo, Murcia, proveedora de ElPozo</strong>. En el vídeo se puede ver una fosa de gran tamaño en el recinto de la granja en la que, al parecer de manera rutinaria, <strong>se arrojan de forma ilegal residuos sólidos potencialmente tóxicos</strong>, tales como material de limpieza, veterinario y todo tipo de desperdicios.

Tras consultar con expertos en materia de producción animal constatamos que la fosa de residuos sólidos podría constituir <strong>una violación de la normativa SANDACH</strong> (Subproductos Animales No Destinados al Consumo Humano).

Este lanzamiento <strong>se encuentra dentro de la campaña <a href="https://elsecretodeelpozo.com/" target="_blank" rel="noopener noreferrer">«El Secreto de ElPozo»</a></strong>, que la Fundación Igualdad Animal comenzó en el mes de febrero de 2018, cuando el programa «Salvados» emitió imágenes de una granja proveedora de El Pozo con animales deformados y enfermos. Como consecuencia, <strong>ElPozo se enfrentó a una crisis de marca que traspasó fronteras</strong>, incluyendo <a href="https://elpais.com/economia/2018/02/12/actualidad/1518460190_212018.html" target="_blank" rel="noopener noreferrer">cadenas de supermercados belgas retirando los productos</a> de la empresa cárnica.

Se da la circunstancia de que <a href="https://igualdadanimal.org/noticia/2016/09/27/caso-el-escobar-un-ano-de-carcel-y-tres-de-inhabilitacion-para-maltratadores/" target="_blank" rel="noopener noreferrer">la granja El Escobar ya fue objeto de escándalo</a> <strong>cuando en 2012 la Fundación Igualdad Animal reveló imágenes de trabajadores matando con grandes espadas &nbsp;a cerdas embarazadas para extraerles los fetos.</strong> En 2016 dos de los trabajadores recibieron <strong>la pena máxima</strong> contemplada por la ley: <strong>un año de cárcel y tres de inhabilitación</strong> para actividades relacionadas con animales. Otros dos de los acusados <strong>se dieron a la fuga </strong>y no se sabe nada de ellos.

&nbsp;
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/xMHGoTMi9uE" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
&nbsp;

La granja El Escobar <strong>continuó proveyendo a ElPozo alimentación</strong> a pesar de que el escándalo del año 2012 constituyó uno de los peores casos de maltrato en granjas industriales de los que se tenga constancia. <strong>A día de hoy y a pesar de la fosa con residuos potencialmente tóxicos y contaminantes ElPozo sigue abasteciéndose de animales de esta granja.</strong>

&nbsp;
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/T-FDREFtVlk" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
«Este nuevo capítulo en el nefasto historial de la granja El Escobar viene a corroborar la falta de voluntad de ElPozo por trabajar únicamente con granjas que cumplan estrictamente con las normativas de bienestar animal y medioambientales», declara Javier Moreno, director ejecutivo de Igualdad Animal; y añade: «no sabemos las consecuencias que pueden tener para el entorno natural la acumulación de estos residuos potencialmente tóxicos en una fosa, en claro incumplimiento de la normativa SANDACH».

«Exigimos a ElPozo que dé una respuesta pública sobre por qué continuó trabajando con esta granja a pesar del escándalo del año 2012 y, asimismo, le pedimos, como líder en el sector, que dé un ejemplo contundente ante este nuevo escándalo en su granja proveedora», solicita Javier Moreno.

&nbsp;
<h4 style="text-align: center;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://www.flickr.com/photos/igualdadanimal/sets/72157629363051319/" target="_blank" rel="noopener noreferrer">Imágenes de la investigación: Granja 'El Escobar'</a></span></h4>
<p style="text-align: center;"><strong>Aviso: Estas imágenes pueden herir su sensibilidad</strong></p>
&nbsp;

La <a href="https://elsecretodeelpozo.com/" target="_blank" rel="noopener noreferrer">campaña</a> que está llevando a cabo la organización internacional Igualdad Animal <strong>requiere a ElPozo el compromiso a una política de bienestar animal</strong> que incluya, entre otros, los siguientes puntos:
<ol>
 	<li>Prohibir las jaulas de gestación y parideras donde las cerdas pasan semanas enteras sin poder darse la vuelta.</li>
 	<li>Eliminar de las mutilaciones dolorosas, incluyendo el corte de cola, recorte de dientes o colmillos y la castración.</li>
 	<li>Eliminar el uso de descargas eléctricas y golpes, arrastres u otros tratos crueles en el manejo de los animales.</li>
 	<li>Implementar una política de tolerancia cero con el maltrato animal y cancelar los acuerdos con proveedores cuyos trabajadores hayan sido sorprendidos abusando de los animales.</li>
 	<li>Capacitar anualmente al personal en el manejo adecuado de los animales para evitar patadas, golpes, arrastre, descargas, y otros manejos crueles.</li>
</ol>
Resulta necesario señalar que, <strong>según el <a href="http://ec.europa.eu/commfrontoffice/publicopinion/index.cfm/ResultDoc/download/DocumentKy/71724" target="_blank" rel="noopener noreferrer">Eurobarómetro de 2016</a>, el 84% de la ciudadanía española demanda más protección para los animales de granja.</strong> Además, el 71% desea obtener más información sobre cómo son tratados los animales de granja en nuestro país.

&nbsp;
<h4 style="text-align: center;">IGUALDAD ANIMAL Y ‘SALVADOS’
<span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.org/salvados-igualdad-animal/" target="_blank" rel="noopener noreferrer">El reportaje que hizo temblar los cimientos de la industria cárnica española</a></span></h4>
&nbsp;

En Igualdad Animal <strong>animamos a todas las personas sensibilizadas con la degradación del medioambiente causado por la ganadería industrial así como con el maltrato animal </strong><a href="https://elsecretodeelpozo.com/" target="_blank" rel="noopener noreferrer">a firmar la petición de nuestra campaña</a>. Como líder en su sector, si ElPozo acepta implementar las medidas solicitadas, otras empresas se verán en la necesidad de seguir el ejemplo.
