A las 12:00 horas un activista de Igualdad Animal se ha descolgado haciendo rapel de la Giralda de Sevilla y ha desplegado una pancarta de 35 metros cuadrados que leía: Derechos para los Animales / Tauromaquia Abolición / Igualdad Animal.
El activista Tomás Eugenio Navas se encuentra detenido en comisaría.
La acción, mediante la cual se ha reivindicado el fin de la tauromaquia, ha sido totalmente pacífica y en ningún momento ha causado perjuicio al monumento.

Galería fotográfica de la acción:
http://flickr.con/photos/igualdadanimal
