<p><img alt="" class="wp-image-13718" src="/app/uploads/2014/10/P09-67632.jpg" style="width: 306px; float: left; margin: 3px 10px;" />Este s&aacute;bado 11 oct. 11:30 hrs. No podemos permanecer impasibles ante la infame gesti&oacute;n de los responsables pol&iacute;ticos de la Sanidad espa&ntilde;ola y madrile&ntilde;a. Su incompetencia se ha cobrado ya la vida de Excalibur y la extrema gravedad de Teresa. Los ciudadanos decentes debemos salir a la calle a reclamar la inmediata dimisi&oacute;n de Ana Mato, Francisco Javier Rodr&iacute;guez y Julio Zarco.</p>
<p>TODAS LAS CONVOCATORIAS A LAS 11:30 de la ma&ntilde;ana de este s&aacute;bado 11 de Octubre (menos Le&oacute;n que ser&aacute; a las 13:00).</p>
<p><strong>A CORU&Ntilde;A</strong>: Obelisco</p>
<p><strong>ALBACETE</strong>: Plaza del Altozano</p>
<p><strong>ALICANTE</strong>: Plaza de la Monta&ntilde;eta</p>
<p><strong>BADAJOZ</strong>: Av. de Huelva (Delegaci&oacute;n del Gobierno)</p>
<p><strong>BARCELONA</strong>: Deleg. del Gobierno, Calle Mallorca, 278</p>
<p><strong>C&Aacute;DIZ:</strong> Plaza del Ayuntamiento</p>
<p><strong>CACERES:</strong> Kiosko de Paseo C&aacute;novas</p>
<p><strong>CORDOBA:</strong> Boulevard de la Avenida Gran Capit&aacute;n (frente al Ayuntamiento viejo)</p>
<p><strong>GIRONA:</strong> Delegacion del gobierno en Av. Jaume I</p>
<p><strong>LAS PALMAS:</strong> Plaza de la Feria</p>
<p><strong>LEON</strong>: Calle Ancha &ndash; Botines (OJO, a las 13:00)</p>
<p><strong>MADRID:</strong> Plaza de Callao</p>
<p><strong>M&Aacute;LAGA:</strong> plaza constituci&oacute;n (final de calle Larios)</p>
<p><strong>OVIEDO:</strong> Plaza de la Escandalera</p>
<p><strong>OURENSE:</strong> de Plaza Mayor a Ministerios Sanidad</p>
<p><strong>PAMPLONA:</strong> Paseo Sarasate (frente Juzgados)</p>
<p><strong>SANTIAGO DE COMPOSTELA</strong>: Plaza do Obradoiro</p>
<p><strong>SEGOVIA:</strong> Fern&aacute;ndez Ladreda, junto al Acueducto</p>
<p><strong>SEVILLA</strong>: Plaza Nueva (frente Ayuntamiento)</p>
<p><strong>TOLEDO:</strong> Plaza de Zocodover</p>
<p><strong>VALENCIA</strong>: Plaza del Ayuntamiento</p>
<p><strong>ZARAGOZA:</strong> Gran V&iacute;a n&ordm;9</p>

