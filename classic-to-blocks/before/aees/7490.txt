<p><img alt="" class="wp-image-10703" src="/app/uploads/2016/05/newmartes.jpg" style="float:left; margin:10px; width:450px" />Los cultivos de soja est&aacute;n devorando las selvas de Sudam&eacute;rica. &iquest;Adivinas para qu&eacute; se destinan <strong>los millones de toneladas de soja</strong> producidas cada a&ntilde;o a costa de las selvas y la biodiversidad?</p>

<p>El aumento del <a href="https://igualdadanimal.org/noticia/2016/04/29/los-consumidores-tenemos-el-poder-de-ayudar-los-animales-y-al-planeta/">consumo de carne</a> es <strong>el principal responsable</strong> de la expansi&oacute;n de los cultivos de soja. Seg&uacute;n <a href="http://d2ouvy59p0dg6k.cloudfront.net/downloads/wwf_soy_scorecard_2016_r6.pdf">el informe</a> de WWF, alrededor del 75% de la producci&oacute;n mundial de soja se destina a alimentaci&oacute;n de los animales de granja. En Europa la proporci&oacute;n es a&uacute;n mayor: <strong>se estima que el 93% de la soja que entra en Europa se destina a alimentar a los animales que luego nos comemos</strong>.</p>

<p>&laquo;Las empresas europeas que compran y usan la soja para alimentar a los animales de las industrias c&aacute;rnica y l&aacute;ctea <strong>est&aacute;n haciendo poco</strong> para detener el impacto negativo de la producci&oacute;n de soja en las selvas y sabanas de Sudam&eacute;rica&raquo;, detalla el informe de WWF.</p>

<p>&laquo;Muchos europeos a&uacute;n no saben que de media consumen al a&ntilde;o 61 kgs de soja, principalmente <strong>como consecuencia de su consumo de carne y leche de procedencia animal</strong>, ni que este consumo tiene un impacto severo en las selvas de Sudam&eacute;rica&raquo;, a&ntilde;ade.</p>

<p>Sandra Mulder, asesora de WWF para pol&iacute;ticas de mercado, declara que &laquo;resulta evidente que muchas empresas se aprovechan de la falta de conocimiento de los consumidores sobre la producci&oacute;n de soja para no hacer nada al respecto&raquo;.</p>

<p>Los cultivos de soja hoy en d&iacute;a cubren una extensi&oacute;n de m&aacute;s de un mill&oacute;n de kil&oacute;metros cuadrados a nivel mundial, <strong>el equivalente a la extensi&oacute;n combinada de Francia, Alemania, B&eacute;lgica y Holanda</strong>.</p>

<p>La expansi&oacute;n de este cultivo <a href="https://igualdadanimal.org/noticia/2016/05/28/naciones-unidas-nuestro-actual-sistema-alimentario-es-insostenible/">se cobra un grave precio</a>. Talar selva para plantar soja <strong>est&aacute; destruyendo algunos de los ecosistemas m&aacute;s valiosos del mundo</strong>, como el Amazonas y la selva del Cerrado en Brasil. Esta pr&aacute;ctica contribuye al cambio clim&aacute;tico, extinci&oacute;n de especies y degradaci&oacute;n de recursos <a href="https://igualdadanimal.org/noticia/2016/05/05/wikileaks-el-planeta-se-encuentra-en-una-senda-catastrofica-por-su-consumo-desmedido/">como el agua dulce</a>.</p>

<p>Por favor, considera <strong>ayudar a detener la destrucci&oacute;n de selvas y ecosistemas</strong> consumiendo alternativas a la carne. <strong>Ayuda a lanzar un poderoso mensaje a las industrias c&aacute;rnica y l&aacute;ctea</strong>. &nbsp;Si te apetece intentarlo tienes toda la informaci&oacute;n que necesitas para conseguirlo, con recetas, productos y consejos, en las <strong>maravillosas websites</strong> <a href="https://www.gastronomiavegana.org/">Gastronom&iacute;a Vegana</a> y <a href="https://danzadefogones.com/">Danza de Fogones</a>.</p>

<p><br />
&nbsp;</p>

