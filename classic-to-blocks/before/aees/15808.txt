¡La Proposición 12, conocida como la Ley de Prevención de la Crueldad a los Animales de Granja, ha sido aprobada!

El domingo 5 de octubre, en el estado de California, Estados Unidos, fue aprobada <strong>la medida de bienestar animal más importante de la historia.</strong> La Prop. 12 establecerá los requisitos <strong>mínimos de espacio para los terneros</strong> de California utilizados en la industria de la ternera, las cerdas madre y las gallinas criadas para los huevos. Y no solo se proporcionará más espacio para estos animales, la medida también garantizará que los alimentos que se venden en California cumplan con estos estándares modestos.

Además, esta medida histórica <strong>garantizará a las gallinas la capacidad de participar en comportamientos naturales </strong>que se evitan mediante el confinamiento de la jaula, incluso extendiendo sus alas, moviéndose por los graneros, posándose y bañándose en polvo. También se aplica a los huevos líquidos, que constituyen un tercio de toda la producción de huevos.

&nbsp;
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación?</h4>
<h4 style="text-align: center;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
&nbsp;

El Departamento de Alimentos y Agricultura de California y el Departamento de Salud Pública de California deberán implementar conjuntamente las reglas y regulaciones para la implementación de esta Ley a más tardar el 1 de septiembre de 2019.

Queremos agradecer a todos nuestros voluntarios y simpatizantes y a la coalición de organizaciones que han hecho esto posible.

<strong>¡Esta histórica victoria para los animales no podría haber ocurrido sin ti!</strong>
