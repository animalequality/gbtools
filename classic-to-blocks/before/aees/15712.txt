Una conmovedor video que muestra a una gallina que cobija bajos sus alas a un gatito abandonado y lo protege de morir de frío, se ha viralizado rápidamente en internet y le ha tocado el corazón a cientos de miles de personas.

Todo ocurrió cuando un hombre llamado Juan caminaba por su granja y, de pronto, escuchó a las gallinas cacarear más fuerte que de costumbre. Al acercarse, se percató de que todas las gallinas estaban arrinconadas a excepción de una y, ¡sorpresa!, debajo de ella, y acurrucado entre sus alas y la paja, encontró a un precioso gatito durmiendo cómodamente.

Según Juan, el gatito habría sido abandonado y pudo haber muerto congelado de no haber sido por la generosidad y empatía de la gallina que lo cobijó y protegió. Cuando el granjero intentó mover al gatito, éste se resistió y regresó hasta donde estaba la gallina para mantenerse bajo su protección.

Gracias a esta conmovedora historia muchísimas personas han podido conocer que las gallinas son animales dotados de una sensibilidad y empatía sorprendentes, pero también poseen una gran inteligencia, similar a la de mamíferos y primates.

Las gallinas pueden realizar operaciones matemáticas básicas como contar y tienen noción de la ordinalidad, es decir, la capacidad de colocar cantidades en una serie. Ambas habilidades se muestran en los humanos solo a partir de los 4 años de edad. También manejan un vocabulario complejo compuesto por más de 30 vocalizaciones, y que lo utilizan para comunicarse con sus polluelos desde que están en el cascarón o para alertar sobre depredadores y dónde hay comida.
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/84aEKx4socE" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
La industria del huevo somete a estos animales a una existencia de sufrimiento, encerrándolas en jaulas en donde pasan su vida sin poder estirar sus alas y desarrollar cualquier comportamiento natural.

En Igualdad Animal seguimos trabajando para cambiar la vida de miles de millones de gallinas y el fin de las jaulas dentro de la industria.
