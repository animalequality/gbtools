Igualdad Animal ya tiene oficina abierta al público en Madrid, puedes pasarte por ella a conocernos de Lunes a Viernes de 10 a 14 horas y de 16:30 a 20 horas. En la oficina de Igualdad Animal además de informarte sobre nuestras actividades en España y LatinoAmérica, puedes llevarte nuestro material informativo o comprar los productos de nuestra tienda.

En breve informaremos sobre actividades informativas que tendrán lugar en dicha oficina. !Pásate a conocernos y te regalamos un recetario vegano a todo color!

La dirección es:

Calle Montera, 34
Planta Segunda, oficina 8.
Tel. 915 222 218
Metro - Gran Vía / Sol
