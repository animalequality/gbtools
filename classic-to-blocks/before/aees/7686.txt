Desde niños nos quisieron hacer creer que las granjas son lugares en donde todos los animales viven vidas felices. <strong>La triste realidad es que la crueldad que caracteriza a la <a href="https://igualdadanimal.org/noticia/2016/12/29/industria-carnica-y-maltrato-animal-2/" target="_blank" rel="noopener">industria cárnica </a>provoca los más terribles sufrimientos a los animales</strong>.

Hemos convertido la vida de los animales de granja, cuyo número puede llegar a representar 9 veces la población mundial, en una existencia de sufrimiento y dolor.

<strong>Algunos de estos animales pudieron escapar del infierno de la industria</strong> para convertirse en testimonios de libertad y la voz de todos aquellos que dejaron atrás. Estas son sus historias.

&nbsp;

<strong>1. Little Miss Sunshine</strong>

Un día el dueño de una granja de gallinas ponedoras tuvo un cambio de corazón. Él se dio cuenta que no podía seguir tratando a estos maravillosos y sensibles seres como <strong>«unidades de producción» y  lucrarse con su sufrimiento</strong>.

A continuación tomó una importante decisión: darles una vida digna y para eso pidió ayuda al refugio de animales <a href="https://edgarsmission.org.au/" target="_blank" rel="noopener">Edgard´s Mission</a>.

Las gallinas ponedoras <strong>pasan toda su vida confinadas en sucias jaulas en batería con un espacio para cada una tan pequeño como un iPad</strong> donde ni siquiera pueden extender sus alas.

Para aumentar la producción <strong>se les somete a la dolorosa mutilación de su pico y son forzadas a poner huevos a un ritmo antinatural alterando los ciclos de luz y oscuridad</strong>.

<strong>A las gallinas se les priva de agua y comida por 3 días a fin de reducir su peso corporal en un 20%</strong>. Esto acelera un segundo ciclo de puestas de huevo, pero también provoca que muchas de ella mueran durante o después de dicha práctica.

<img class="wp-image-11633" style="float: right; margin: 10px; width: 500px;" src="/app/uploads/2017/02/little-miss-sunshine5_0.jpg" alt="" />La brutal explotación que sufren día tras día hace que su salud se resienta y <strong>muchas de ellas mueran en las jaulas incluso antes de ser llevadas al matadero</strong>.

Luego de recibir la llamada del granjero, <strong>el equipo de rescate de Edgard´s Mission se preparó para transportar con delicadeza a todas y cada una de las 1081 gallinas</strong> hacia una nueva vida donde conocerían por primera vez la libertad.

Entre las rescatadas se encontraba una pequeña gallina que desde su llegada al refugio conquistó el corazón de todos.

Un día, poco después de llegar, estaba echada sobre la hierba con sus alas extendidas en una de sus primeras oportunidades de recibir la luz de sol. Uno de los trabajadores dijo «mírala, le encanta el sol pero nunca lo había visto antes. Vamos a llamarla Pequeña Miss Sunshine».

«Little Miss Sunshine era una de estas gallinas y su destino iba a ser el mismo que el resto de las 1080 gallinas que vivían con ella: sería enviada al matadero cuando su producción de huevos mermara luego de una triste vida en la que solo conoció el sufrimiento».

Alegre y resplandeciente, <strong>Little Miss Sunshine se ha convertido en la embajadora de miles de millones de gallinas</strong>.

Ella es la esperanza de un mundo mejor para todos los animales, porque no solo es posible sino que de forma imparable lo estamos construyendo.

&nbsp;
<h4 style="text-align: center;">¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</h4>
<h4 style="text-align: center;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
&nbsp;

<strong>2. Esperanza: una ternerita rescatada de la despiadada industria láctea</strong>

¿Imaginas cómo sería que te separan de tu madre apenas minutos después de nacer? Esto es lo que le ocurrió a Esperanza, una ternera que tuvo la desdicha de nacer en el seno de la industria láctea.

<strong>Esperanza estaba destinada a ser utilizada para producir leche</strong>. Pero para que esto pudiera ser posible ella tendría que sufrir una vida maltrato, explotación y sufrimiento.

<img class="wp-image-11634" style="float: left; margin: 10px; width: 500px;" src="/app/uploads/2017/02/tumblr_nejiahpxkr1qerf2so3_1280.jpg" alt="" />

«Durante al menos 7 años sería inseminada para tener una cría tras otra. En cada uno de sus numerosos partos le quitarían a su hijo para que la industria pudiera obtener durante varios meses toda la leche que ella había producido para ellos».

¿Puedes imaginar su sufrimiento? Las vacas <strong>sufren profundamente cuando son separadas de sus hijos y pueden llorarlos por días</strong>. Y vuelven a ser inseminadas cuando todavía están consternadas por el dolor que les ha producido esta separación.

Esta cadena de sufrimiento vuelve a repetirse muchísimas veces, ininterrumpidamente, a lo largo de su vida.

<strong>Cuando tienen 5 o 7 años (en lugar de los 25 que pudieran vivir en libertad) las vacas son enviadas al matadero </strong>luego de que su producción de leche disminuye.

Todo esto era lo que le esperaba a Esperanza, pero <strong>una noche miembros de la organización Elige Veganismo entraron en la granja donde acababa de nacer para llevarla lejos</strong>, muy lejos del sufrimiento y la muerte.

Hoy día, Esperanza vive en el <a href="http://www.santuarioigualdadinterespecie.org/" target="_blank" rel="noopener">Santuario Igualdad Interespecie</a> donde <strong>no solamente es respetada, cuidada y amada sino que ha comprendido que tomar su leche no es un privilegio sino un derecho que nadie debe arrebatarle.</strong>
