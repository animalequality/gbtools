<div class="center"><img class="wp-image-10410" src="/app/uploads/2016/01/consumo.jpg" /></div>
<p>Est&aacute;s haciendo la compra, llegas a la secci&oacute;n de pat&eacute;s y ves el foie gras. A unos metros tienes otra posibilidad: hummus, <strong>&iquest;qu&eacute; eliges?</strong></p>

<p>Sigues y llegas a la secci&oacute;n de leche. Entre las distintas marcas de leche de vaca, de pronto te encuentras una secci&oacute;n con distintos tipos <strong>de leche vegetal: arroz, avena, almendra, soja;</strong> &iquest;qu&eacute; eliges?</p>

<p>Al lado de la secci&oacute;n de carne, tienes la secci&oacute;n de verduras y frutas <strong>con su caracter&iacute;stica explosi&oacute;n de colores y formas</strong>; y m&aacute;s all&aacute; la secci&oacute;n de legumbres; &iquest;qu&eacute; eliges?</p>

<p>Son preguntas que a veces no nos hacemos pero&hellip; <strong>&iquest;y si esos momentos fueran los que m&aacute;s maltrato animal pudiesen evitar?</strong></p>

<p>Porque al igual que t&uacute;, <strong>a nosotros nos gusta ver a los animales as&iacute;</strong>:</p>

<p><img alt="" class="wp-image-13794" src="/app/uploads/2016/01/ayudaconsumo1.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Y no as&iacute;:</p>

<p><img alt="" class="wp-image-13795" src="/app/uploads/2016/01/ayudaconsumo2.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><sub><em>Patos de la industria del foie gras</em></sub></p>

<p>&iquest;A qui&eacute;n no le gusta ver a los animales as&iacute;?:</p>

<p><img alt="" class="wp-image-13796" src="/app/uploads/2016/01/ayudaconsumo3.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&iquest;Y a qui&eacute;n no le disgusta verlos as&iacute;?:</p>

<p><img alt="" class="wp-image-13797" src="/app/uploads/2016/01/ayudaconsumo4.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><sub><em>Vaca de la industria l&aacute;ctea.</em></sub></p>

<p>El momento de hacer la compra en el supermercado es decisivo para los animales de granja. Como consumidores, <strong>tenemos la posibilidad de mandar poderosos mensajes</strong> a las empresas de alimentaci&oacute;n.</p>

<p>Ya hay <strong>millones de personas</strong> sustituyendo la carne en su alimentaci&oacute;n alrededor del mundo. Las empresas lo saben y como consecuencia, cada vez hay m&aacute;s productos <strong>para esta &nbsp;demanda creciente</strong>.</p>

<p>Adem&aacute;s, los supermercados <strong>ya est&aacute;n llenos de posibilidades</strong> como legumbres, cereales, verduras, frutas&hellip; Un mundo de deliciosos sabores y texturas tan saludable y delicioso que est&aacute; convenciendo cada vez a m&aacute;s gente.</p>

<p>&iquest;Te sumas? Para ayudarte, te proponemos que visites la p&aacute;gina website <a href="https://www.gastronomiavegana.org/">Gastronom&iacute;a Vegana</a>, donde <strong>podr&aacute;s obtener la informaci&oacute;n para dar tus primeros pasos</strong>. &iexcl;Adem&aacute;s, en la website <a href="https://danzadefogones.com/">Danza de Fogones</a> tienes <strong>deliciosas recetas</strong> que te entrar&aacute;n por los ojos!</p>

<p>Porque&hellip; &iquest;a qui&eacute;n no le gusta ver a nuestros amigos los animales as&iacute;?:</p>

<p><img alt="" class="wp-image-13798" src="/app/uploads/2016/01/ayudaconsumo5.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&iquest;Y qui&eacute;n no siente tristeza vi&eacute;ndolos as&iacute;?:</p>

<p><img alt="" class="wp-image-13799" src="/app/uploads/2016/01/ayudaconsumo6.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><sub><em>Pollitos de la industria de la carne.</em></sub></p>

