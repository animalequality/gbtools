LONDRES (Reuters) - Los niños con un elevado coeficiente intelectual (CI) son más propensos a convertirse en vegetarianos cuando crecen, según un informe publicado el viernes.

El estudio realizado en Gran Bretaña, que incluyó a más de 8.000 hombres y mujeres de 30 años cuyo CI fue medido cuando tenían 10 años, mostró que cuanto más elevado era el coeficiente intelectual más posibilidades había de que se volvieran vegetarianos.

"Las personas que son más inteligentes de pequeñas, y que obviamente mantienen esa inteligencia cuando tienen 30 años, fueron más propensas a ser vegetarianas que aquellas menos inteligentes," dijo la doctora Catherine Gale, epidemióloga de la University of Southampton en Inglaterra.

La experta añadió que los hallazgos, publicados en la edición en internet de British Medical Journal, coincidían con otros estudios que mostraron que las personas más inteligentes tienden a comer de forma más saludable y a hacer más ejercicio.

"Hay muchas evidencias que relacionan el vegetarianismo con un riesgo menor de enfermedad cardíaca. Los vegetarianos tienden a registrar presión arterial más baja, niveles más bajos de colesterol y un riesgo menor de morir de enfermedad coronaria," añadió Gale.

Por cada incremento de 15 puntos en los niveles de coeficiente intelectual, la probabilidad de ser vegetariano creció un 38 por ciento. Incluso después de adaptarlo a factores como la clase social y la educación, la relación siguió siendo consistente.

Fuente - http://lta.today.reuters.com/news/newsArticle.aspx?type=worldNews&storyID=2006-12-15T142100Z_01_N15276449_RTRIDST_0_INTERNACIONAL-SALUD-VEGETARIANOS-SOL.XML&archived=False


