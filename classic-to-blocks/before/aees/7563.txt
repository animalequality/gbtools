<p>El destino de Goldie al nacer era incierto: formaba parte de una camada no deseada.</p>

<p>Fue llevado junto a sus hermanos y hermanas a una perrera y el reloj empez&oacute; la cuenta atr&aacute;s. Sin saberlo, <strong>Goldie ten&iacute;a los d&iacute;as contados a menos que alguien lo adoptase</strong>. Y entonces lleg&oacute; Marta.</p>

<p>&laquo;En cuanto vi a Goldie no pude apartar la mirada de &eacute;l&raquo;; &laquo;era el m&aacute;s peque&ntilde;o y triste de todos&raquo;, nos comenta Marta.</p>

<p>Marta sali&oacute; de la perrera con Goldie en sus brazos. Pod&iacute;a sentir <strong>el coraz&oacute;n del peque&ntilde;o palpitando en la palma de su mano</strong>; su propio coraz&oacute;n estaba desgarrado por los hermanos de camada a quienes no pudo ayudar.</p>

<p>&laquo;Al llegar a casa lo ba&ntilde;&eacute; y despu&eacute;s se qued&oacute; dormido en mis brazos&raquo;; &laquo;en ese momento no era a&uacute;n consciente del cambio que iba a dar mi vida a consecuencia de vivir con Goldie&raquo;.</p>

<p>Los meses pasaron y Goldie creci&oacute; en tama&ntilde;o y confianza. Los terribles d&iacute;as pasados quedaron atr&aacute;s y los d&iacute;as por venir iban a estar llenos de juegos, aventuras y amor; pero, adem&aacute;s, algo estaba sucediendo; algo de lo que Goldie no era consciente pero s&iacute; el responsable: Marta cada vez com&iacute;a <a href="https://igualdadanimal.org/blog/es-un-hecho-ademas-de-deliciosa-una-alimentacion-sin-carne-es-mas-barata/" target="_blank">menos carne</a>.</p>

<p>&nbsp;</p>

<FONT SIZE=4><FONT COLOR=#808080><p>&laquo;En ese momento no era a&uacute;n consciente del cambio que iba a dar mi vida a consecuencia de vivir con Goldie&raquo;.</p></FONT>

<p>&nbsp;</p>

<p>&laquo;A los seis meses de vivir con Goldie empec&eacute; a cambiar mis h&aacute;bitos de compra en el supermercado&raquo;; &laquo;ni siquiera me daba cuenta de ello, pero cada vez compraba m&aacute;s verduras y legumbres y menos carne&raquo;; &laquo;fue algo completamente subconsciente y se produjo de la forma m&aacute;s natural&raquo;.</p>

<p>Lo que estaba sucediendo era que, a trav&eacute;s de Goldie, <strong>Marta hab&iacute;a creado un lazo con los animales que hasta ese momento no ten&iacute;a</strong>. De pronto, se estaba dando cuenta de que los animales son seres <a href="https://igualdadanimal.org/blog/la-profunda-y-aun-desconocida-vida-emocional-de-los-animales-de-granja/" target="_blank">profundamente emocionales</a> y llenos de ganas de vivir.</p>

<p><img alt="" class="wp-image-11012" src="/app/uploads/2016/09/shutterstock_285808985.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&laquo;Un d&iacute;a vi una noticia sobre un accidente de un cami&oacute;n lleno de cerdos camino del matadero. Hab&iacute;a mucha sangre. Algunos cerdos agonizaban en la carretera sin que nadie les ayudase; otros intentaban caminar con sus patas rotas&raquo;. &laquo;Recuerdo que Goldie estaba en el sof&aacute;, a mi lado, durmiendo&raquo;. &laquo;En ese momento algo me sucedi&oacute;; de pronto sent&iacute; una enorme tristeza; un terrible peso en mis hombros&raquo;. &laquo;Abrac&eacute; a Goldie y no pude evitar ponerme a llorar&raquo;.</p>

<p>&laquo;Era como si todos los animales del mundo me pidiesen ayuda porque sab&iacute;an que hab&iacute;a salvado a Goldie y ellos necesitaban ser salvados tambi&eacute;n&raquo;.</p>

<p>&laquo;Goldie me lam&iacute;a las l&aacute;grimas; no pod&iacute;a parar de llorar&raquo;; &laquo;encend&iacute; el port&aacute;til y me puse a buscar informaci&oacute;n sobre <a href="http://www.igualdadanimal.org/noticias/7477/una-dosis-de-motivacion-para-comer-mas-platos-sin-carne" target="_blank">c&oacute;mo ayudar a los animales</a> en los camiones y en los mataderos&raquo;. &laquo;As&iacute; llegu&eacute; a vuestra website, <strong>a la p&aacute;gina web de Igualdad Animal&raquo;</strong>.</p>

<p>&laquo;Desde entonces dej&eacute; de comer carne de cerdo. Luego dej&eacute; <a href="http://www.igualdadanimal.org/noticias/7555/7-alternativas-para-dejar-la-carne-de-pollo-fuera-de-tu-plato" target="_blank">la de pollo</a> y el resto de carnes. Ahora lo intento con el pescado&raquo;; &laquo;no puedo seguir participando en la muerte de los animales&raquo;; &laquo;me lo tomo con calma, no me fuerzo&raquo;; &laquo;eso lo le&iacute; <a href="https://www.facebook.com/IgualdadAnimal/" target="_blank">en vuestro facebook</a>&raquo;, nos dice sonriendo. A su lado, Goldie observa a Marta.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>Su mirada es noble, leal, inocente y llena de amor</strong>.</p>

<p>Como la del resto de animales que, sin saberlo, <a href="http://www.igualdadanimal.org/noticias/7549/animales-de-granja-sensibles-inteligentes-adorables-y-comida" target="_blank">ha ayudado a salvar</a> del matadero por los cambios que ha producido en la vida de Marta.</p>

<p><strong>Si quieres estar al tanto de historias como esta y muchas m&aacute;s, adem&aacute;s de informarte de c&oacute;mo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales</strong>.</p>

