<div class="center"><img class="wp-image-12464" src="/app/uploads/2017/09/mercadona-k0we-u40829014357g0b-624x385ideal.jpg" /></div>
<p><img alt="" class="wp-image-12465" src="/app/uploads/2017/09/seitan_mercadona.jpg" style="float: right; width: 350px; margin-left: 10px; margin-right: 10px;" /></p>

<p>Siguiendo una tendencia global imparable, la cadena de supermercados <strong>Mercadona acaba de lanzarse al mercado de la producci&oacute;n de sustitutos de la carne.</strong></p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/07/05/paris-sonrie-las-proteinas-vegetales-con-la-apertura-de-tres-supermercados-100/" target="_blank">Par&iacute;s sonr&iacute;e a las prote&iacute;nas vegetales con la apertura de tres supermercados 100% vegetarianos</a></strong>

<p>&nbsp;</p>

<p>Desde principios de a&ntilde;o, contaban con otros &nbsp;productos 100% vegetales como el tabule con verduras frescas, edamame, la ensalada de lentejas y begur y el kale.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>Dos son los productos que bajo su marca &laquo;Hacendado&raquo; ofrecer&aacute; pr&oacute;ximamente: las hamburguesas de seit&aacute;n y los medallones de seit&aacute;n &laquo;a la piastra&raquo;. <strong>El seit&aacute;n es una alternativa a la carne muy popular </strong>entre las personas que siguen una alimentaci&oacute;n basada en vegetales.</p>

<p>Se espera que estos y otros productos puedan estar a la venta en toda Espa&ntilde;a a partir de octubre.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Fuentes:<a href="https://www.laverdad.es/sociedad/novedad-mercadona-vuelve-20170919134637-nt.html" target="_blank"> https://www.laverdad.es/sociedad/novedad-mercadona-vuelve-20170919134637-nt.html</a></p>

