<p>Si hablamos de maltrato animal, no hay animales que padezcan mayores sufrimientos que los de granja. Para abastecer de carne, leche y huevos a la humanidad, <strong>la ganader&iacute;a industrial</strong> convierte la vida de los animales en verdaderas pesadillas.</p>

<p>Solidarizarnos con los animales mostrando compasi&oacute;n hacia su sufrimiento se ha convertido en <strong>uno de los retos de nuestros tiempos</strong>. Las encuestas muestran de manera internacional como millones de personas, con especial impacto en las nuevas generaciones, <strong>est&aacute;n cambiando sus h&aacute;bitos de consumo</strong> para favorecer y ayudar a las v&iacute;ctimas de la ganader&iacute;a.</p>

<p>Aqu&iacute; te proponemos 7 formas de ayudar a nuestros compa&ntilde;eros de planeta, para los que <strong>su vida es tan importante como para nosotros la nuestra</strong>. &iexcl;Esperamos que te decidas a unirte! &iexcl;Los animales te necesitan de su lado!</p>

<strong>1. Sustituyendo la carne de pollo en tu alimentaci&oacute;n por alternativas vegetales</strong>

<p>El pollo es el animal m&aacute;s consumido a nivel mundial. La industria de la carne convierte la vida de estos fr&aacute;giles animales en una pesadilla de maltrato animal. Viven hacinados en enormes naves industriales en las que nunca ven la luz del sol. Nos necesitan desesperadamente.</p>

<p><img alt="" class="wp-image-13793" src="/app/uploads/2016/01/ayudaanimales1.jpg" style="margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>



<p>&nbsp;</p>

<p>&nbsp;</p>



<p>&nbsp;</p>

<p>&nbsp;</p>


<strong>2. Sustituyendo la carne de ternera en tu alimentaci&oacute;n por alternativas vegetales</strong>

<p>La industria l&aacute;ctea, adem&aacute;s de producir una enorme cantidad de sufrimiento a las vacas, guarda otro oscuro secreto para el p&uacute;blico general: todos los beb&eacute;s que nacen de las vacas son separados de sus madres s&oacute;lo horas despu&eacute;s de nacer. Esto sucede durante toda la vida de las vacas. La separaci&oacute;n es desgarradora. Por si fuera poco, la mayor&iacute;a de beb&eacute;s macho ser&aacute;n destinados a la inhumana industria de la carne. <strong>Estos beb&eacute;s son la carne de ternera que veremos en los supermercados</strong>.</p>

<p><img alt="" class="wp-image-13800" src="/app/uploads/2016/01/ayudaranimales2.jpg" style="margin-left:10px; margin-right:10px; width:450px" /></p>


<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>


<strong>3. Sustituyendo la carne de cerdo en tu alimentaci&oacute;n por alternativas vegetales</strong>

<p>&iquest;Sab&iacute;as que los estudios cient&iacute;ficos han demostrado que un cerdo es tan inteligente como un ni&ntilde;o o ni&ntilde;a de 3 a&ntilde;os? De hecho estos animales <strong>compiten en inteligencia con delfines, chimpanc&eacute;s y elefantes</strong>. Son seres muy sensibles que, adem&aacute;s, tras miles de a&ntilde;os de relaci&oacute;n con los humanos, desarrollan v&iacute;nculos afectivos con nosotros tan r&aacute;pido como nuestros perros y gatos.</p>

<p><img alt="" class="wp-image-13801" src="/app/uploads/2016/01/ayudaranimales3.jpg" style="margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>


<strong>4. Sustituyendo la carne en tu alimentaci&oacute;n</strong>

<p>Porque claro, una vez que te has dado cuenta de que los animales son nuestros amigos si se lo permitimos, resulta dif&iacute;cil dejar de comer s&oacute;lo a unos. Todos rebosan ganas de vivir. Poco a poco, las personas que est&aacute;n contra el maltrato animal <strong>van haciendo la conexi&oacute;n entre una forma de maltrato y otra</strong>. Alimentarse sin carne consiste esencialmente en llenar nuestra cesta de la compra con unos productos en vez de otros. En la website <a href="http://www.gastronomiavegana/">Gastronom&iacute;a Vegana</a> tienes multitud de informaci&oacute;n para dar los primeros pasos.</p>

<p>&iexcl;&Aacute;nimo!</p>

<p><img alt="" class="wp-image-13802" src="/app/uploads/2016/01/ayudaranimales4.jpg" style="margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>5. Siendo un activo colaborador de organizaciones de protecci&oacute;n animal en redes sociales</strong>

<p>Gracias a las personas que firman peticiones, comparten post con mensajes de protecci&oacute;n animal y difunden los proyectos y campa&ntilde;as, la defensa de los animales llega al resto de la sociedad. <strong>A d&iacute;a de hoy las redes sociales tienen tanta importancia en alcanzar a la sociedad como los medios de comunicaci&oacute;n</strong>. &iexcl;Cada vez que compartes cuenta!</p>

<p><img alt="" class="wp-image-13803" src="/app/uploads/2016/01/ayudaranimales5.jpg" style="margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>6. Donando a organizaciones de protecci&oacute;n animal cuyo trabajo se centre en animales de granja</strong>

<p>Recuerda: son estos animales los que en mayor n&uacute;mero viven en condiciones extremas y mueren en los horribles mataderos. La ganader&iacute;a industrial hace que m&aacute;s y m&aacute;s animales lleguen a existir con el s&oacute;lo prop&oacute;sito de convertirse en bandejas de carne. <strong>Ning&uacute;n animal merece una existencia tan llena de sufrimiento</strong>. Recuerda que Igualdad Animal es una organizaci&oacute;n centrada en defender a estos animales. Si quieres donar, <a href="http://www.igualdadanimal.org/colabora/donativos.php">puedes hacerlo aqu&iacute;</a>.</p>

<p><img alt="" class="wp-image-13804" src="/app/uploads/2016/01/ayudaranimales6.jpg" style="margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>7. Si eres vegetariano / vegano</strong>

<p>Siendo amable con quien no lo sea. No juzgues a los dem&aacute;s por su manera de alimentarse. Recuerda que muy pocos han sido vegetarianos toda su vida. Muchos estuvimos en el punto en el que ahora est&aacute;n otras personas que no son vegetarianas. <strong>Tu manera de relacionarte y comunicarte debe animar a los dem&aacute;s a interesarse por tus valores</strong>. No seas arrogante ni uses discursos agresivos. Eso es lo &uacute;ltimo que necesitan los animales. No pierdas el foco: eres un embajador de los animales.</p>

<p><img alt="" class="wp-image-10401" src="/app/uploads/2016/01/ayudaranimales7.jpg" style="margin-left:10px; margin-right:10px; width:450px" /></p>

