El Día Internacional sin carne es una fecha en la que decenas de organizaciones y cientos de activistas de todo el mundo se unen para rechazar los productos provenientes de la explotación animal, promoviendo con ello un estilo de vida sin sufrimiento y muerte.

Por ello y al igual que hicimos el año pasado en Madrid, Igualdad Animal está preparando para este año 2007 concentraciones, actos y reivindicaciones por el 20 de Marzo, día internacional sin carne, en ciudades tan importantes como Madrid, Málaga, Sevilla etc. dentro del estado español y en paises como Costa Rica, Perú y Venezuela en el resto del mundo. Además estamos preparando una web específica para este día que pronto estará disponible.
Para este año y más que nunca requerimos la colaboración de toda la gente posible tanto en tareas de organización como voluntarios este día, así conseguiremos que el Día Internacional sin carne no se olvide facilmente y que cientos de miles de personas reflexionen sobre sus hábitos de consumo.

Si quieres ayudarnos a organizar el 20 de Marzo o participar como activista en alguno de los eventos que estamos organizando.
Escríbenos a info@igualdadanimal.org

En el siguiente enlace se pueden ver imágenes del acto que realizamos en Madrid en el 2006 y la enorme repercusión mediática del mismo.
http://www.igualdadanimal.org/actividades/2006/03/20/dia-internacional-sin-carne
