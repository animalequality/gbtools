<p>
	<img align="left" alt="" height="243" hspace="9" class="wp-image-13621" src="/app/uploads/2010/04/1885328.jpg" vspace="9" width="250" />Ulman Wojtek, un joven polaco de 29 a&ntilde;os, residente en la localidad alicantina de Benitatxell, pas&oacute; 72 horas atrapado en un pozo al que cay&oacute; intentando rescatar a Julia, la gata con la que convive.<br />
	<br />
	Los hechos sucedieron el pasado viernes cuando Julia cay&oacute; al pozo y Ulman qued&oacute; tambi&eacute;n atrapado al romperse la cuerda con la que intentaba rescatar a su compa&ntilde;era. &quot;No la pod&iacute;a sacar de ninguna manera y tuve que bajar. Se rompi&oacute; la cuerda intentando subir y ca&iacute; dentro&quot;, explica ahora sano y salvo pero repleto de magulladuras.<br />
	<br />
	Un vecino de la zona escuch&oacute; sus gritos el domingo y avis&oacute; a los bomberos. Ulman fue trasladado al hospital para ser tratado de golpes y magulladuras y hoy ya se encuentra en casa, sano y salvo, junto con su compa&ntilde;era la gata Julia.<br />
	&nbsp;</p>

