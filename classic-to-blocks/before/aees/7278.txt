Aquí encontrarás las 7 verdades sobre la carne de conejo que no aparecen en los anuncios de la industria cárnica:

<strong>1. Miles de crías mueren asfixiadas o rompiéndoles el cuello cuando nacen.</strong>

<strong><img class="wp-image-13901" style="margin: 10px; width: 885px;" src="/app/uploads/2015/08/conejos1.jpg" alt="" /></strong>

<strong>2. Los conejos pasan días e incluso semanas con heridas y graves infecciones sin tratar para ahorrar en gastos veterinarios</strong>

<strong><img class="wp-image-10275" style="margin: 10px; width: 885px;" src="/app/uploads/2015/08/conejos_2.jpg" alt="" /></strong>

<strong>3. También sufren amputaciones en sus orejas y patas por canibalismo, ya que se muerden unos a otros debido al estrés del confinamiento.</strong>

<strong><img class="wp-image-13902" style="margin: 10px; width: 885px;" src="/app/uploads/2015/08/conejos3.jpg" alt="" /></strong>

<em>Fotografía: Jo-Anne McArthur para Igualdad Animal</em>

<strong>4. Es habitual que en las granjas maten a los conejos enfermos golpeandolos con barras de metal en la cabeza o en el cuello. La mayoría agoniza durante varios minutos en los contenedores de basura donde son arrojados.</strong>

<strong><img class="wp-image-11775" style="margin: 10px; width: 885px;" src="/app/uploads/2015/08/conejos4.jpg" alt="" /></strong>

<strong>5. Los conejos en las granjas nunca pisan la hierba ni ven la luz del sol. Viven en jaulas de metal llenas de suciedad donde apenas pueden ponerse de pie.</strong>

<strong><img class="wp-image-13903" style="margin: 10px; width: 885px;" src="/app/uploads/2015/08/conejos5.jpg" alt="" /></strong>

<strong>6. La primera vez que los conejos respiran aire fresco es cuando viajan hacinados en camiones hacia el matadero.</strong>

<img class="wp-image-13904" style="margin: 10px; width: 885px;" src="/app/uploads/2015/08/conejos6.jpg" alt="" />

<em>Fotografía: Jo-Anne McArthur para Igualdad Animal</em>

<strong>7. Los conejos tiemblan de miedo al ver cómo matan a sus compañeros y tratan de huir desesperadamente cuando llega su turno. Muchos de ellos están plenamente conscientes mientras se desangran en el matadero e intentan luchar por su vida hasta el último momento.</strong>

<img class="wp-image-13905" style="margin: 10px; width: 885px;" src="/app/uploads/2015/08/conejos7.jpg" alt="" />

<em>Fotografía: Jo-Anne McArthur para Igualdad Animal</em>

Conoce la investigación al completo en: <a href="http://GranjasDeConejos.org">GranjasDeConejos.org</a>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;
