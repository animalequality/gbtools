<div class="center"><img class="wp-image-12679" src="/app/uploads/2017/12/shutterstock_167586098.jpg" /></div>
<p>Con el fin de promover una alimentaci&oacute;n m&aacute;s sana y sustentable, recientemente fue aprobada por el Consejo de Medell&iacute;n la propuesta de <strong>que en 400 colegios p&uacute;blicos de esta ciudad de Colombia, se sirva un plato vegetariano una vez a la semana</strong>.</p>

<p>Seg&uacute;n el texto aprobado, la medida garantizar&aacute; que &laquo;en colegios oficiales, comedores comunitarios, jardines y restaurantes de las entidades municipales se ofrezca un men&uacute; libre de carnes rojas, carnes procesadas, embutidos, carne de cerdo, pollo o pescado&raquo;.</p>

<p>&nbsp;</p>

<h4>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/12/04/proponen-un-dia-sin-carne-en-colegios-de-francia-para-luchar-contra-el-cambio/">Proponen un d&iacute;a sin carne en colegios de Francia para luchar contra el cambio clim&aacute;tico</a></h4>

<p>&nbsp;</p>

<p>&laquo;Este proyecto va en consonancia con la recomendaci&oacute;n de la comunidad cient&iacute;fica que, hace algunos a&ntilde;os, hizo una alerta ambiental en la cual recomienda avanzar hacia el consumo de dietas vegetales e ir disminuyendo el consumo de prote&iacute;na animal, que para su producci&oacute;n, aumenta el consumo de agua potable y la deforestaci&oacute;n de la tierra&rdquo;, dijo a Telemedell&iacute;n &Aacute;lvaro M&uacute;nera, concejal de Medell&iacute;n por el Partido Conservador y promotor de la iniciativa.</p>

<p>&nbsp;</p>

<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</font></font></p>

<p>&nbsp;</p>

<p>Alrededor de 260 mil ni&ntilde;os de 400 instituciones estatales se beneficiaran con este acuerdo que forma parte del existente Programa de Alimentaci&oacute;n Escolar (PAE), un plan del Estado que garantiza el suministro de comida diaria a estudiantes de colegios oficiales de todo el pa&iacute;s.</p>

<p><img alt="" class="wp-image-12680" src="/app/uploads/2017/12/shutterstock_300003260.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Hace apenas dos a&ntilde;os, la Organizaci&oacute;n Mundial de la Salud (OMS) confirm&oacute; a trav&eacute;s de su Agencia de Investigaciones sobre el C&aacute;ncer que comer carnes procesadas (salchichas, jam&oacute;n, hamburguesas, salami, bacon) causa c&aacute;ncer y comer carnes rojas (vaca, ternera, cerdo, cordero) puede causar c&aacute;ncer. &nbsp;Y la Escuela de Harvard de Salud P&uacute;blica elimin&oacute; del Healthy Eating Plate (plato de alimentaci&oacute;n saludable) cualquier plato que tuviera l&aacute;cteos debido al alto &iacute;ndice de grasas saturadas y qu&iacute;micos que se usan para su elaboraci&oacute;n.</p>

<p>&nbsp;</p>

<p>M&uacute;nera coment&oacute; tambi&eacute;n que, adem&aacute;s de ofrecerse a los ni&ntilde;os un plato vegetariano a la semana, se ha planteado un plan estrat&eacute;gico que los orientar&aacute; &nbsp;sobre los beneficios que este tipo de alimentaci&oacute;n aporta al planeta. Recordemos que la industria ganadera es altamente contaminante y derrocha una gran cantidad de recursos naturales. &nbsp;</p>

<p>&nbsp;</p>

<p>Con esta importante iniciativa que acaba de ser aprobada, Medell&iacute;n se coloca a la vanguardia de ciudades como Los &Aacute;ngeles y Nueva York, donde ya se est&aacute;n siendo implementados este tipo de programas.</p>

