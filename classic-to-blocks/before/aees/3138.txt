Buenos Aires - Ubicado en la columna 47 del camino Vergara, en Ensenada, el hogar Canela (a cargo de la asociación Pro Amparo y Defensa del Animal) funcionaba como un espacio para la protección de animales desamparados.
 
O al menos ésa era la fachada. Porque ayer lo allanaron y encontraron 64 perros en absoluto estado de abandono, desnutridos, maltratados, heridos, con infecciones y muchos al borde de la muerte. Pero hay más: según el inspector Claudio Jaidar, jefe de la DDI La Plata, en el predio había “unos 20 animales muertos y una fosa común con esqueletos”. El encargado, identificado oficialmente como Juan Carlos Auscarría -alias “el Vasco”-, quedó detenido.

Más allá de los resultados de esta última investigación, hay que aclarar que las denuncias contra este hogar no son nuevas. Según pudo saber Trama Urbana, en 1992, bajo el número de causa nº 12.723 se tramitó en el entonces juzgado Criminal y Correccional nº 14 otra acusación contra Auscarría, que fue condenado a dos meses de prisión en suspenso al comprobarse que sometía a los perros a toda clase de maltratos: “Los golpeaba con palos o fierros hasta producirles la muerte, los bañaba en líquido con veneno para matarlos, los perforaba con un fierro y los tiraba detrás de una tapia para que se pudrieran”, detalló una denunciante.

La Cámara de Apelaciones le redujo aún más la condena y el hombre
retomó su cargo, incluso llegando a percibir cuotas para sustentar sus actividades (por eso los cargos de defraudación, informaron allegados a la causa).

En 2003 se presentó una nueva denuncia contra el hogar Canela y Auscarría, que recayó en la misma UFI que ahora requirió el allanamiento, pero recién en el pasado mes de junio, y gracias al testimonio de la abogada Silvina Cairo, el tema obtuvo un fuerte impulso. Esta vez, bajo el número de IPP 18559/07.

“Este hombre se ofrecía a limpiar las calles de animales sueltos, con el respaldo de las empresas del Polo Industrial de Ensenada y organismos oficiales, y luego los dejaba morir o los mataba. Era una perrera clandestina”, sostuvo el abogado Moreno, que hoy se acercará a la DDI para interiorizarse sobre el estado de los perritos que fueron encontrados vivos en el allanamiento de ayer.

“Quedaron en el lugar, a resguardo de otra persona”, explicó Jaidar.


Fuente: Diario Hoy
