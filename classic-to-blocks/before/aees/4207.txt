El 21 de marzo se celebra el Día Internacional de la Eliminación de la Discriminación Racial. Lamentablemente, el racismo sigue presente en todo el mundo. Los inmigrantes no han dejado de ser discriminados, y continúan produciéndose agresiones contra personas por su lugar de origen, como ha ocurrido en España varias veces durante el último año. Víctimas como Miwa Buene Monake, quien sufrió una brutal paliza en Alcalá de Henares (Madrid) a consecuencia de la cual sufre tetraplejia, y como la menor de edad ecuatoriana agredida física y sexualmente en el metro de Barcelona. Sin embargo, hay muchos más ciudadanos anónimos que sufren agresiones, que condenamos y a cuyas víctimas mostramos nuestra solidaridad.

Asimismo la esclavitud sigue vigente en la actualidad. La Organización Internacional del Trabajo (OIT) considera que en 2007 el número de víctimas de la esclavitud moderna era de 27 millones de personas, el 80% de ellos mujeres. Pero tampoco podemos olvidar a los billones de animales que viven como esclavos en todo el mundo, utilizados como recursos en granjas, laboratorios, espectáculos, tiendas, piscifactorías... y asesinados en el mismo momento en que dejan de ser rentables.

En la organización Igualdad Animal pensamos que el fin del racismo y del resto de formas de discriminación pasa por ver a los demás como individuos que merecen que sus intereses sean defendidos, y no como recursos que pueden ser usados para nuestro beneficio. Las diferencias físicas no justifican ningún tipo de discriminación. Quienes son diferentes de nosotros también tienen consciencia y capacidad de experimentar sensaciones. Suficiente motivo para que merezcan respeto.

Sharon Núñez
Presidenta y portavoz de Igualdad Animal

Tel. (+34) 915 222 218
Tel. (+34) 609 980 196

Correo electrónico: SharonN@igualdadanimal.org
Web: https://igualdadanimal.org/

Oficina: C/ Montera, 34 2º Oficina 8 - 28013 Madrid (España)
