<div class="center"><img class="wp-image-12793" src="/app/uploads/2018/01/shutterstock_110836229_0.jpg" /></div>
<p>Seg&uacute;n una nueva investigaci&oacute;n, los pollos en venta en los supermercados de Gran Breta&ntilde;a <strong>est&aacute;n mostrando niveles r&eacute;cord de superbacterias resistentes a algunos de los antibi&oacute;ticos m&aacute;s fuertes. </strong>La <a href="https://www.food.gov.uk/" target="_blank">Food Standards Agency</a>, encargada de hacer los an&aacute;lisis correspondientes, inform&oacute; tras el an&aacute;lisis de una gran muestra de pollos vendidos por minoristas sobre &laquo;proporciones significativamente m&aacute;s altas&raquo; en los &uacute;ltimos 10 a&ntilde;os en casos de bacterias campylobacter.</p>

<p>Las bacterias campylobacter <strong>pueden causar intoxicaci&oacute;n alimentaria grave y en casos m&aacute;s graves la muerte.</strong> Las cepas que son resistentes a los antibi&oacute;ticos utilizados en su contra son a&uacute;n m&aacute;s da&ntilde;inas, ya que implican la utilizaci&oacute;n de antibi&oacute;ticos de &uacute;ltimo recurso. Su uso es considerado como un &uacute;ltimo recurso en el arsenal de la medicina, pero su abuso los puede hacer tambi&eacute;n ineficaces.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/10/27/espana-usa-mas-antibioticos-en-granjas-que-cualquier-otro-pais-de-la-ue/" target="_blank">Espa&ntilde;a usa m&aacute;s antibi&oacute;ticos en granjas que cualquier otro pa&iacute;s de la UE</a></strong>

<p>&nbsp;</p>

<p>Durante d&eacute;cadas la industria ganadera ha practicado lo que se conoce como &laquo;uso no terap&eacute;utico&raquo; de antibi&oacute;ticos en animales. Es un procedimiento est&aacute;ndar <strong>utilizado en animales sanos para evitar brotes de epidemia y acelerar su crecimiento.</strong> El estudio de FSA puede haber confirmado lo que muchos cient&iacute;ficos ya hab&iacute;an conclu&iacute;do: los animales de granja son la principal causa de resistencia a los antibi&oacute;ticos entre humanos.</p>

<p><img alt="" class="wp-image-12794" src="/app/uploads/2018/01/shutterstock_519758773_2.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>La resistencia a los antibi&oacute;ticos presente en estos animales <strong>puede afectar la misma resistencia en los humanos que los consumen.</strong> Esto hace que los medicamentos com&uacute;nmente usados para tratar enfermedades dejen de ser efectivos, como es el caso, por ejemplo, de la ciprofloxacina. &nbsp;</p>

<p>Para la realizaci&oacute;n de las pruebas se utilizaron m&aacute;s de 4 mil muestras. Se identific&oacute; resistencia a la ciprofloxacina en m&aacute;s de la mitad de las muestras de una forma de Campylobacter probada, 237 de 437 pruebas con Campylobacter jejuni, y en casi la mitad (52 de 108) de otra cepa, Campylobacter coli.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Los resultados demuestran que <strong>el uso de antibi&oacute;ticos en animales de granja est&aacute; provocando la propagaci&oacute;n de bacterias resistentes altamente contagiosas para los humanos</strong>, ya que uno de los principales m&eacute;todos de transmisi&oacute;n a muchas cepas de bacterias resistentes es a trav&eacute;s de la ingesta de la carne de estos animales. Y a pesar de que las pr&aacute;cticas de higiene adecuadas y una cocci&oacute;n completa pueden contribuir a evitar el contagio, cualquier falla en el proceso puede provocar una infecci&oacute;n grave.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/09/08/el-masivo-uso-de-antibioticos-en-la-ganaderia-industrial/" target="_blank">El masivo uso de antibi&oacute;ticos en la ganader&iacute;a industrial</a></strong>

<p>&nbsp;</p>

<p><br />
<br />
&laquo;Es escandaloso que (las reglas del gobierno) a&uacute;n permitan que las aves de corral sean medicadas en masa con antibi&oacute;ticos fluoroquinol&oacute;nicos. Hace veinte a&ntilde;os, un informe de la C&aacute;mara de los Lores dec&iacute;a que esto deber&iacute;a detenerse. Incluso Estados Unidos prohibi&oacute; la pr&aacute;ctica hace m&aacute;s de 10 a&ntilde;os debido a la fuerza de la evidencia cient&iacute;fica. Entonces, &iquest;por qu&eacute; las autoridades brit&aacute;nicas y europeas todav&iacute;a se niegan a tomar medidas?&raquo;, declar&oacute; el asesor cient&iacute;fico de Alliance for Save Our Antibiotics, C&oacute;il&iacute;n Nunan.<br />
<br />
Y a pesar de que la comunidad cient&iacute;fica viene advirtiendo desde hace mucho tiempo sobre el uso no terap&eacute;utico de antibi&oacute;ticos en animales, <strong>los esfuerzos para reducir su uso han tardado en dar resultados. &nbsp;</strong></p>

<p>En medio de incumplimientos de higiene y seguridad en las plantas de procesamiento, en 2014 The Guardian revel&oacute; altos niveles de infecci&oacute;n por campylobacter en la carne de pollo del Reino Unido. Tambi&eacute;n ha revelado la presencia de MRSA, otra superbacteria propagada a trav&eacute;s de carne infectada, en productos de carne de cerdo del Reino Unido.</p>

<p>El gobierno brit&aacute;nico ya hab&iacute;a alertado sobre este problema en 2016 a trav&eacute;s de un informe. Seg&uacute;n el mismo, de no tomarse las medidas necesarias para el a&ntilde;o 2050 m&aacute;s personas morir&aacute;n a causa de las &laquo;superbacterias&raquo; que por c&aacute;ncer. &nbsp;</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://www.theguardian.com/environment/2018/jan/15/british-supermarket-chickens-show-record-levels-of-antibiotic-resistant-superbugs" target="_blank">https://www.theguardian.com/environment/2018/jan/15/british-supermarket-chickens-show-record-levels-of-antibiotic-resistant-superbugs</a></p>

