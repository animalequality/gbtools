<div class="center"><img class="wp-image-10442" src="/app/uploads/2016/02/pollis.jpg" /></div>
<p>El pollo es el animal m&aacute;s consumido en el mundo. <strong>En todo momento hay tres de estos animales por cada ser humano en el planeta</strong>. Un total de 20 mil millones de pollos a los que la cruel industria c&aacute;rnica mantiene en inhumanas granjas industriales. Terrible, &iquest;verdad?</p>

<p>Sin embargo, esta semana <strong>ha sido buena para estos fr&aacute;giles y maravillosos animales</strong>. Una investigaci&oacute;n con c&aacute;maras ocultas de <strong>Igualdad Animal</strong> se ha hecho viral en Facebook. En tan solo unos d&iacute;as el v&iacute;deo ha sido visto <strong>29 millones de veces alrededor del mundo</strong>. &iexcl;29 millones!</p>

<p>Nuestra campa&ntilde;a <a href="http://suprimerdia.com/">Su Primer D&iacute;a</a> revela el maltrato a los pollitos en las incubadoras industriales. El poderoso v&iacute;deo de la campa&ntilde;a <strong>denuncia las pr&aacute;cticas habituales con estos sensibles animales durante su primer d&iacute;a de vida</strong>.</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/VGKQKGOyPtU" frameborder="0" allowfullscreen></iframe>

<p>El v&iacute;deo que se ha hecho viral en Facebook es una versi&oacute;n narrada en ingl&eacute;s. <a href="https://www.facebook.com/gary.yourofsky/videos/947308528657729/?pnref=story">Puedes verla pinchando aqu&iacute;</a>.</p>

<p>Es una muy buena noticia que tantas personas puedan llegar a conocer <a href="https://www.facebook.com/gary.yourofsky/videos/947308528657729/?pnref=story">las pr&aacute;cticas inhumanas</a> de la industria de la carne con estos indefensos animales.</p>

<p>Lo cierto es que los consumidores tienen <a href="https://igualdadanimal.org/noticia/2015/12/03/la-carne-de-pollo-es-una-de-las-mayores-causantes-de-maltrato-animal/">un gran desconocimiento sobre todo lo relacionado con la vida de los pollos</a>. Esto es as&iacute; porque, por supuesto, la multimillonaria industria c&aacute;rnica <strong>no quiere que conozcamos c&oacute;mo produce la carne de pollo</strong>. Existe un gran hermetismo por parte de todas las siniestras industrias que explotan a los animales.</p>

<p>La gran mayor&iacute;a de la sociedad se declara abiertamente contra el maltrato animal. Esto hace que la ganader&iacute;a industrial, <a href="https://igualdadanimal.org/blog/por-que-la-ganaderia-industrial-es-la-mayor-causante-de-maltrato-animal-de-la-historia/">responsable del mayor maltrato animal de la historia</a>, necesite <strong>mantener ocultas su pr&aacute;cticas a los consumidores</strong>.</p>

<p>Los pollitos son animales <strong>extremadamente fr&aacute;giles y sensibles</strong> que necesitan el cuidado de su madre durante sus primeros d&iacute;as de vida. En condiciones normales, as&iacute; son esos d&iacute;as:</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/cuqpyyIMhUQ" frameborder="0" allowfullscreen></iframe>

<p>Si quieres ayudar a reducir el sufrimiento de los animales en manos de la industria c&aacute;rnica, puedes probar <a href="http://www.igualdadanimal.org/noticias/7432/15-increibles-recetas-vegetarianas-para-ayudarte-sustituir-la-carne">algunas de estas maravillosas recetas que cada vez m&aacute;s personas est&aacute;n preparando en sus casas</a>. &iexcl;<strong>Son incre&iacute;bles y no echar&aacute;s de menos la carne</strong>!</p>

<p>&nbsp;</p>

