<div class="center"><img class="wp-image-9748" src="/app/uploads/2012/10/sf2.png" /></div>
<p align="justify"><img alt="" class="wp-image-14260" src="/app/uploads/2012/10/second_female_anima.png" style="width: 270px; " />La organizaci&oacute;n danesa <a href="http://anima.dk/kampagner/aktuelle-kampagner/pelsdyr/pelsfri-mode/bed-second-female-droppe-pelsen" target="_blank">Anima </a>ha realizado una intensa campa&ntilde;a solicitando a la marca <a href="https://secondfemale.dk/" target="_blank">Second Female</a> que deje de utilizar piel de zorros y conejos criados en granjas para la confecci&oacute;n de sus prendas.</p>

<p align="justify">Se han realizado protestas regulares frente a sus establecimientos durante un a&ntilde;o y sin embargo, tras una reuni&oacute;n con su director general, Laust Preben, <strong>la empresa manifest&oacute; que no estaba interesada en renunciar a comercializar piel por el momento.</strong></p>

<p align="justify">Second Female cuenta con tiendas propias s&oacute;lo en Dinamarca y Holanda, pero tambi&eacute;n vende su ropa en Alemania, Noruega, Suecia, Espa&ntilde;a y Reino Unido.&nbsp;</p>

<p align="justify">&nbsp;</p>

<p align="justify">Estos son los datos de la responsable de Second Female en Espa&ntilde;a:&nbsp;<br />
<br />
Roser Cortada<br />
+34933041799<br />
roser@bolsidexu.com</p>

<p align="justify">Puedes hacerle llegar tu opini&oacute;n, de forma educada y respetuosa, mediante un correo electr&oacute;nico personalizado o firmando nuestra petici&oacute;n.</p>


<div>
<h4>*Petici&oacute;n finalizada. Gracias a todos los que hab&eacute;is colaborado.</h4>

<h1 style="color: rgb(204, 204, 204); font-size: 11px; font-family: Arial, sans-serif; margin-left: 20px;">Firmando aceptas nuestra <a href="https://igualdadanimal.org/politica-de-privacidad/" style="font-weight:normal;">Pol&iacute;tica de privacidad de datos</a>.</h1>
</div>

<p>&nbsp;</p>

