<div class="center"><img class="wp-image-12665" src="/app/uploads/2017/12/shutterstock_271498517.jpg" /></div>
<p>Como una estrategia para promover acciones contra el cambio clim&aacute;tico, <strong>el ministro franc&eacute;s de Transici&oacute;n Ecol&oacute;gica, Nicol&aacute;s Hulot, ha propuesto que muy pronto en todos los colegios del pa&iacute;s se implante un men&uacute; vegetariano una vez a la semana.</strong> Fomentar h&aacute;bitos alimenticios m&aacute;s saludables entre los m&aacute;s j&oacute;venes que, adem&aacute;s, contribuyan a reducir nuestro impacto en planeta es el objetivo de la iniciativa.</p>

<p>La propuesta va tambi&eacute;n de la mano con los debates actuales en torno al sufrimiento que padecen los animales en manos de la industria c&aacute;rnica. Seg&uacute;n Hulton, servir&iacute;an para &laquo;llevar pronto una gran reflexi&oacute;n sobre la condici&oacute;n animal con el Ministro de Agricultura, porque estoy convencido de que las mentalidades han evolucionado enormemente en este tema, y ​​es un indicio de civilizaci&oacute;n&raquo;. Y contin&uacute;a afirmando que <strong>&laquo;el animal tiene conciencia&raquo; y que es necesario &laquo;reducir al m&aacute;ximo su sufrimiento&raquo;.</strong></p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/08/29/7-escuelas-de-los-angeles-ofreceran-menu-vegetariano-sus-alumnos/" target="_blank">7 escuelas de Los &Aacute;ngeles ofrecer&aacute;n men&uacute; vegetariano a sus alumnos</a></strong>

<p>&nbsp;</p>

<p><strong>La industria ganadera env&iacute;a al matadero 70.000 millones de animales al a&ntilde;o</strong>, es decir, una cantidad de cerdos, vacas, terneras, pollos y gallinas equivalente a ocho veces la poblaci&oacute;n de seres humanos del planeta. Tambi&eacute;n es la causante del peor maltrato animal en la historia: en sus manos han muerto la mayor cantidad de animales padeciendo el mayor sufrimiento a lo largo de sus vidas. Adem&aacute;s, dentro de granjas y mataderos la crueldad es la norma ya que estos animales carecen de la protecci&oacute;n legal que tienen, por ejemplo, gatos o perros. <strong>Si alguien hiciera a nuestros perros o gatos lo que la ganader&iacute;a hace a los animales de granja, terminar&iacute;a en la c&aacute;rcel.</strong></p>

<p><img alt="" class="wp-image-12666" src="/app/uploads/2017/12/14848366384_4b2c0f9a18_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Por otra parte, cada vez son m&aacute;s los estudios que proponen seriamente la reducci&oacute;n del consumo de carne animal como una forma de combatir el cambio clim&aacute;tico. Seg&uacute;n la FAO, la Agencia de Naciones Unidas para la Alimentaci&oacute;n y la Agricultura, <strong>el 14,5% de los gases de efecto invernadero emitidos por la acci&oacute;n humana provienen del sector de la ganader&iacute;a.</strong> &nbsp;</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete gratuitamente a nuestro e-bolet&iacute;n</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>&laquo;Tambi&eacute;n es una cuesti&oacute;n de educaci&oacute;n, as&iacute; que espero que, pronto, los comedores escolares ofrezcan a los ni&ntilde;os un men&uacute; vegetariano un d&iacute;a a la semana&raquo;, afirma el ministro. &laquo;En mi familia somos cinco. Dos son vegetarianos. Los otros tres, entre los que me cuento, solo comemos carne una vez por semana. Cada uno sigue su camino&raquo;, explica Hulot en una entrevista en la revista L&rsquo;Obs.</p>

<p>A fin de reducir el consumo de carne y avanzar hacia la construcci&oacute;n de un sistema alimentario m&aacute;s sostenible, el laboratorio de ideas Terranova sugiri&oacute; tambi&eacute;n implantar un d&iacute;a vegetariano a la semana tanto en colegios como en escuelas secundarias.</p>

<p>El objetivo a perseguir ser&iacute;a <strong>&laquo;en las pr&oacute;ximas dos o tres d&eacute;cadas, reducir a la mitad nuestro consumo de carne animal y revertir la proporci&oacute;n actual de prote&iacute;nas animales y vegetales en nuestra alimentaci&oacute;n&raquo;.</strong> Esto significar&iacute;a &nbsp;apuntar a una dieta donde el 60% de las prote&iacute;nas sean de origen vegetal, contra aproximadamente el 40% actual.</p>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://elpais.com/elpais/2017/11/30/mamas_papas/1512065255_961318.html" target="_blank">https://elpais.com/elpais/2017/11/30/mamas_papas/1512065255_961318.html</a></p>

<p><a href="https://www.leparisien.fr/societe/nicolas-hulot-veut-un-menu-vegetarien-un-jour-par-semaine-a-l-ecole-30-11-2017-7425126.php" target="_blank">https://www.leparisien.fr/societe/nicolas-hulot-veut-un-menu-vegetarien-un-jour-par-semaine-a-l-ecole-30-11-2017-7425126.php</a></p>

<p><a href="https://www.huffingtonpost.fr/life/article/nicolas-hulot-voudrait-un-menu-vegetarien-dans-les-cantines-scolaires-un-jour-par-semaine_112987.html" target="_blank">https://www.huffingtonpost.fr/life/article/nicolas-hulot-voudrait-un-menu-vegetarien-dans-les-cantines-scolaires-un-jour-par-semaine_112987.html</a></p>

