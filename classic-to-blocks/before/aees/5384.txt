<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<font size="3"><font color="#000000"><font face="Times New Roman">El Rey de Espa&ntilde;a mostr&oacute; hoy su apoyo al asesinato y tortura de toros y caballos en la entrega de los <span style="mso-bidi-font-size: 7.5pt">Premios Universitarios y Trofeos Taurinos de 2009 otorgados por la Real Maestranza de Caballer&iacute;a de Sevilla.</span></font></font></font></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<font size="3"><font color="#000000"><font face="Times New Roman"><span style="mso-bidi-font-size: 7.5pt">Juan Carlos no es el &uacute;nico aficionado a la tauromaquia de la familia real. Su madre, la Condesa de Barcelona, tambi&eacute;n lo era, as&iacute; como su hija la Infanta Elena. El Rey acude siempre que puede a la plaza de tortura de toros de Las Ventas, as&iacute; como a otras actividades que implican divertirse a costa del sufrimiento y la muerte de animales, como la caza.</span></font></font></font></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<font size="3"><font color="#000000"><font face="Times New Roman"><span style="mso-bidi-font-size: 7.5pt">En su intervenci&oacute;n durante la entrega de los citados premios, el Rey felicit&oacute; a los toreros triunfantes en la pasada Feria de Abril y distinguidos con los Trofeos Taurinos 2009 por &quot;la esencia del buen lance&quot; del que, indic&oacute;, &quot;nace un mundo cultural y art&iacute;stico fecundo&quot;.</span></font></font></font></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<font size="3"><font color="#000000"><font face="Times New Roman"><span style="mso-bidi-font-size: 7.5pt">Don Juan Carlos destac&oacute; la remodelaci&oacute;n de la &quot;maravillosa&quot; Plaza de Toros de la Maestranza que, minutos antes, acababa de inaugurar tras las obras de modernizaci&oacute;n de Sol Alto y Sombra Alta.<o:p></o:p></span></font></font></font></p>
<p>
	<font size="3"><font color="#000000"><img align="middle" alt="" height="224" hspace="9" class="wp-image-13944" src="/app/uploads/2010/03/espana-toros-san-isidro-toros-san-isidro-15599x0.jpg" vspace="9" width="300" /></font></font></p>

