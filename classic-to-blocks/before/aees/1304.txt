El activista de Igualdad Animal / Animal Equality, Álex Rodríguez, ha sido entrevistado por Claudette Vaughan para la revista digital Abolitionist-Online. En la entrevista, Álex Rodríguez responde a diversas preguntas sobre el activismo llevado a cabo por Igualdad Animal / Animal Equality desde un planteamiento antiespecista así como sobre diversas formas de explotación animal que tienen lugar en el estado español.

Abolitionist-Online es la principal publicación abolicionista sobre derechos animales que cuenta en su corta, pero intensa historia, con un gran número de entrevistas y artículos de diversos activistas, autores y organizaciones del movimiento en todo el mundo.

Puedes consultar el texto íntegro (en inglés) en la siguiente dirección:
http://www.abolitionist-online.com/interview-issue05_queer.rights.animal.rights.alex-r.shtml
