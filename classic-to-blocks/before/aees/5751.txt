
<iframe frameborder="0" height="293" src="https://player.vimeo.com/video/16163156?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1" width="520"></iframe><br />Video: Karol Orzechowski (<a href="http://www.decipherimages.com">www.decipherimages.com</a>)
<br />
Galer&iacute;a fotogr&aacute;fica por Jo-Anne McArthur (<a href="htp://www.weanimals.org">www.weanimals.org</a>):&nbsp;http://www.flickr.com/photos/igualdadanimal/sets/72157624970154504<br />
<br />
Activistas de Igualdad Animal han rescatado a cinco gallinas de una granja de 160.000 animales el pasado lunes 16 de Septiembre.<br />
<br />
Tras acceder al interior de una de las naves de la granja a trav&eacute;s de una puerta sin cerradura y sin tener que hacer ning&uacute;n uso de fuerza, los activistas sacaron a cinco animales que posteriormente fueron examinadas por un veterinario. Una de ellas tuvo que ser operada para extraerle una masa de gran tama&ntilde;o que ten&iacute;a en la cabeza, resultado de una antigua infecci&oacute;n ocular. Otra de las gallinas est&aacute; ahora bajo tratamiento veterinario pero est&aacute; fuera de peligro.<br />
<br />
Estos animales estaban siendo sometidos a un proceso de muda forzada por el que, mediante una severa restricci&oacute;n de alimento -incluso pasando d&iacute;as enteros sin poder comer- y en plena oscuridad, se fuerza que inicien un nuevo ciclo de puesta de huevos.<br />
<br />
Estos animales de a&ntilde;o y medio de vida, han pasado los &uacute;ltimos once meses encerrados tras los barrotes de una jaula sin poder ver siquiera la luz del sol. Padecen diversas deficiencias nutricionales, algunos de ellos tambi&eacute;n tienen da&ntilde;os en el h&iacute;gado debido a la alimentaci&oacute;n hiperproteica que recib&iacute;an para aumentar su puesta de huevos.<br />
<br />
El Equipo de Rescates Abiertos de Igualdad Animal ha documentado todo en v&iacute;deo porque nos parece muy importante y necesario que la sociedad acceda a lo que ocurre en el interior de estos lugares, a los que no se suele tener acceso y pude ponerse en el lugar de los animales.<br />
<br />
Este rescate se ha realizado en una granja con jaulas de bater&iacute;a similares a las jaulas &#39;enriquecidas&#39; o &#39;acondicionadas&#39; que ser&aacute;n obligatorias a partir de 2012 en toda la Uni&oacute;n Europea. Pero en las granjas en que no se utilizan jaulas, ya sean &eacute;stas del modelo de bater&iacute;a, acondicionadas u otras- las gallinas siguen siendo privadas de libertad, utilizadas como recursos para beneficio humano y finalmente enviadas al matadero. En Espa&ntilde;a, m&aacute;s de 47 millones de gallinas son v&iacute;ctimas de este sistema de explotaci&oacute;n que busca satisfacer la demanda de huevos. Otros 47 millones de pollitos -sus hermanos- son tambi&eacute;n v&iacute;ctimas de este consumo, aunque frecuentemente sean olvidados y ni siquiera figuren en las estad&iacute;sticas oficiales. En todas las granjas los pollitos macho son masacrados ya sea siendo triturados vivos o gaseados, porque no ponen huevos ni resulta rentable su explotaci&oacute;n para otros fines.<br />
<br />
Rescatamos animales no s&oacute;lo para ayudar a esos individuos sino tambi&eacute;n para provocar una reflexi&oacute;n en la sociedad que nos lleve a dejar de ver al resto de animales con los que compartimos este planeta como seres a nuestra disposici&oacute;n, como recursos de los que obtener alg&uacute;n beneficio. Como en todos los rescates que realizamos, siempre nos invade una sensaci&oacute;n de tristeza e impotencia cuando cerramos la puerta y dejamos tras de nosotros a miles de animales que van a sufrir toda su vida y acabar&aacute;n degollados en un matadero. No podemos rescatar a todos los animales que est&aacute;n en granjas, mataderos o laboratorios. Poner fin al holocausto que est&aacute;n padeciendo depende de cada uno de nosotros y nosotras, dejando de financiar todo esto en cualquiera de sus formas. Viviendo de forma vegana, estamos liberando a todos los animales que iban a sufrir y ser masacrados por nuestra demanda y estamos construyendo la base para construir un mundo m&aacute;s justo para todos y todas independientemente de la especie a la que pertenezcamos.<br />
<br />
Por la abolici&oacute;n de la esclavitud animal.<br />
<br />
RescateAbierto.org<br />
OpenRescue.net<br />
<br />
V&iacute;deo: Karol Orzechowski<br />
Fotos: Jo-Anne McArthur
