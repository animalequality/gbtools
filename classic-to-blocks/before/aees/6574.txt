<p><img alt="" class="wp-image-9583" src="/app/uploads/2012/06/rosita_leche_maternizada.png" style="width: 220px; float: left; " title="Científicos posan junto a Rosita" />Cient&iacute;ficos argentinos consiguieron, hace 14 meses, <strong>clonar una ternera a la que implantaron dos genes humanos</strong> con la intenci&oacute;n de que su <a href="https://igualdadanimal.org/problematica/lacteos/" target="_blank">leche </a>fuera similar a la humana.</p>

<p><strong>Rosita</strong>, que es como se llama la vaca utilizada en este experimento, <strong>fue sometida recientemente a un proceso de inducci&oacute;n artificial de la lactancia</strong> que, mediante un tratamiento hormonal, permiti&oacute; simular la &uacute;ltima fase de gestaci&oacute;n, el desarrollo mamario y la producci&oacute;n de leche. Esta inducci&oacute;n se efectu&oacute; <strong>con el prop&oacute;sito de adelantar su producci&oacute;n de leche</strong> sin la necesidad de esperar a los 26 meses de vida y a un embarazo real.<br />
<br />
Los investigadores indican que el objetivo del experimento es elevar el valor nutricional de la leche bovina con dos genes humanos: la prote&iacute;na lactoferrina, que tiene propiedades antibacterianas y antivirales, y la lisozima, tambi&eacute;n antibacteriana. Ambas se encuentran en el contenido de la leche materna humana&nbsp; y tambi&eacute;n en la leche producida por Rosita y analizada el pasado 4 de junio.</p>

<p><em>&laquo;<strong>En su vida adulta producir&aacute; leche que se asemejar&aacute; a la humana</strong>; un desarrollo de gran importancia para la nutrici&oacute;n de los lactantes&raquo;</em>, aseguraron los especialistas.<br />
<br />
Argentina no es el &uacute;nico pa&iacute;s donde se realizan investigaciones dedicadas a alcanzar las propiedades de la leche materna humana en la bovina. <strong>China tambi&eacute;n anunci&oacute; el a&ntilde;o pasado su trabajo en este sentido</strong>, creando cerca de Pek&iacute;n una <strong>granja experimental donde hay m&aacute;s de 300 vacas &#39;modificadas&#39;</strong> y cuyo producto planean sacar al mercado dentro de unos 3 a&ntilde;os.<br />
&nbsp;</p>

