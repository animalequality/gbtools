[img_assist|nid=1308|title=Elefante en útero|desc=|link=none|align=left|width=200|height=121]Inglaterra - National Geographic ha logrado filmar la gestación entera, desde la concepción hasta el nacimiento, de un elefante, un delfín y un perro, informó el pasado día 24 el canal británico de televisión Channel 4, que emitirá ese documental en la próxima Navidad.

El novedoso programa, de dos horas de duración, ha podido grabarse gracias a la aplicación de un escáner de ultrasonido y el uso de ecógrafos de tres dimensiones, imágenes generadas por ordenador y cámaras diminutas.

Entre las asombrosas imágenes que podrán ver los telespectadores del Reino Unido estas Navidades, destacan la del feto de un delfín de ocho semanas que aprende a nadar en el vientre de su madre, así como un elefante en miniatura en pleno proceso de gestación.

El documental, titulado 'In the Womb: Animals' ('Animales en el útero), subraya 'algunos hechos fascinantes sobre nuestro patrimonio evolutivo', afirmó Jeremy Dear, jefe de desarrollo de Pionner Productions, que hizo el programa para Channel 4.

'Es inevitable no conmoverse con cada uno de los viajes de nuestros animales hacia el nacimiento', comentó Dear, al agregar que el documental constituye 'una ventana extraordinaria a este mundo no visto previamente'.

Las imágenes revelan al detalle la evolución del embarazo de una elefanta durante 22 meses que desemboca en el alumbramiento de un hijo que pesan nada menos que 120 kilogramos, aunque ya a las 18 semanas había empezado a ejercitar sus patas en el útero materno.

En el caso del delfín, se aprecia cómo durante un embarazo de un año el pequeño mamífero desarrolla todas las facultades que le permitirán nadar a una velocidad de hasta 35 kilómetros por hora y detectar a una presa a una distancia de hasta 90 metros.

Las imágenes del perro son igualmente impresionantes, pues se observa cómo el feto ya cuenta a los 63 días con todo lo necesario para sobrevivir en el mundo exterior, como un agudo sentido del olfato y la habilidad detectar sonidos a larga distancia.

Los productores del documental ya sorprendieron el año pasado con el programa 'Vida antes del nacimiento', una película que mostraba el desarrollo de un feto humano.

Noticia extraida de Terra.es
