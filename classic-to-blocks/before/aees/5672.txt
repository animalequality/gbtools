<p><a data-flickr-embed="true" href="https://www.flickr.com/photos/igualdadanimal/albums/72157624953867088" title="14/09/2010 - Tordesillas - Muerte de Platanito - Toro de la Vega"><img alt="14/09/2010 - Tordesillas - Muerte de Platanito - Toro de la Vega" height="428" src="https://farm5.staticflickr.com/4083/4989776932_920bde7e55_z.jpg" width="640" /></a><script async src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Tordesillas - Hoy Martes 14 de Septiembre a las 11 horas ha tenido lugar la matanza del Toro de la Vega en Tordesillas, donde el toro llamado Platanito ha sido perseguido, picado y matado. Por tercer a&ntilde;o consecutivo, varios activistas de Igualdad Animal se han infiltrado entre los taurinos para ser testigos de todo el evento y grabar con c&aacute;maras ocultas la matanza de Platanito con el objetivo de mostrar a la sociedad el acoso, sufrimiento y muerte padecidos por este animal para entretenimiento de los vecinos del lugar.</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Durante quince minutos el toro ha sido acosado por cientos de personas montadas a caballo y a pie y armadas con picas cuyo &uacute;nico fin era perseguirlo y matarlo. Una vez ya en la explanada a las afueras de Tordesillas, Marco, uno de los participantes montado a caballo ha clavado una pica en un flanco al toro provoc&aacute;ndole graves heridas. Tras varios minutos tratando de alancearle de nuevo, y de un intento de escapar a la muchedumbre, finalmente qued&oacute; postrado en el suelo donde otro participante ha arrancado la divisa al toro y le ha acuchillado en la nuca, seccion&aacute;ndole la m&eacute;dula espinal y provoc&aacute;ndole una par&aacute;lisis generalizada, momento en el que algunos de los presentes cubrieron al toro a&uacute;n vivo con una lona para evitar que las c&aacute;maras puedan mostrar el terrible final de este animal. Herido de muerte y oculto a las miradas de los asistentes, el toro ha muerto asfixiado en su propia sangre mientras la muchedumbre se sub&iacute;a encima suyo clamando victoria.</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Este es el tercer a&ntilde;o consecutivo en el que activistas de Igualdad Animal se infiltran entre los taurinos de Tordesillas para documentar y mostrar a la sociedad aquello que tratan ocultar a la opini&oacute;n p&uacute;blica amenazando a los periodistas y activistas por los derechos animales. Recientemente grabamos la muerte de un toro en el Encierro de Saced&oacute;n&ndash;sufriendo agresiones por parte de los taurinos&ndash; as&iacute; como la muerte de otro en Gal&aacute;pagos donde el toro fue acosado con jeeps. Desde Igualdad Animal trabajamos diariamente realizando investigaciones para sacar a la luz tanto &eacute;stas como otros ejemplos de explotaci&oacute;n animal (en granjas de visones, granjas de cerdos, mataderos, etc) de modo que la sociedad reflexione sobre esta injusticia y avancemos hacia el fin de los espect&aacute;culos con animales en particular y toda forma de explotaci&oacute;n animal en general. No olvidemos que diariamente millones de animales est&aacute;n sufriendo y muriendo en granjas, mataderos, laboratorios&hellip; Una explotaci&oacute;n sistem&aacute;tica que, aunque sea menos visible que la matanza del Toro de la Vega, es igualmente injusta e igualmente evitable.</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">======================</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">MATERIAL AUDIOVISUAL</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">======================</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Fotograf&iacute;as (primeras im&aacute;genes de la muerte de Platanito en alta resoluci&oacute;n, en breve podremos ofrecer m&aacute;s im&aacute;genes):</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">http://www.flickr.com/photos/igualdadanimal/sets/72157624953867088</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">V&iacute;deo: P&oacute;nganse en contacto con nosotros para acordar el modo de entregarles las im&aacute;genes de la matanza del Toro de la Vega obtenidas hasta con 4 c&aacute;maras diferentes.</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">NOTA: Queremos dar la mayor difusi&oacute;n posible a estas im&aacute;genes y estaremos encantados de cederlas a cualquier medio de comunicaci&oacute;n que est&eacute; interesado en ellas.</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Se permita la difusi&oacute;n de estas im&aacute;genes con la condici&oacute;n de que citen la fuente: &quot;Igualdad Animal&quot;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica; min-height: 17.0px">&nbsp;</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">CONTACTO DE MEDIOS</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Javier Moreno | Portavoz de Igualdad Animal</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Tel&eacute;fono: 691 054 172</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Tel&eacute;fono: 655 432 914</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">Correo electr&oacute;nico: javierm@igualdadanimal.org</p>

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Helvetica">https://igualdadanimal.org/</p>

