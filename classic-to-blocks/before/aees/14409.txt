La ong Defensanimal.org ha entrevistado recientemente al autor y abogado norteamericano especializado en derechos animales Gary L. Francione. A lo largo de once preguntas Francione expone su postura ante las reformas de la explotación animal, sobre el lenguaje especista y el papel del veganismo en el movimiento abolicionista entre otras cuestiones.

Estamos seguros que esta entrevista ayudará a difundir el pensamiento de este autor en los países de habla hispana y esperamos que genere interés por su obra y planteamientos.
