Para poder mantener su granja, Mike Lanigan tenía que enviar constantemente a a vacas y terneros al matadero. Esa era la parte del trabajo que no le gustaba, pero como él mismo dice «siendo un granjero, tú simplemente miras para otro lado».

Pero el punto es que Lanigan no pudo hacer la vista gorda por mucho más tiempo. Todo cambió para él un día, mientras ayudaba a un ternero recién nacido a tomar su primera leche. «Lo estaba haciendo con tanto amor, hablándole al ternero y limpiándole el sucio de la cara y tratando de llevárselo a su mamá», recuerda Lanigan.

De pronto, se dio cuenta de que a ese inocente e indefenso animal que despertaba en él tanta ternura y a quien trataba con tanto cariño, pronto le darían un golpe en la cabeza y lo cortarían en pedazos para ser comido. Esto lo hizo reflexionar profundamente: «pensé cuán hipócrita estaba siendo al amar tanto a alguien para que todo al final terminara siendo tan distinto a ese amor».

Y fue así como luego de dedicarse por mucho tiempo al mismo trabajo que realizó su padre, Mike decidió que no enviaría más nunca en su vida a una vaca o a un ternero al matadero y que convertiría su granja en un refugio donde todos ellos podrían vivir sus vidas en paz.

<iframe src="https://www.youtube.com/embed/1tLeO2pg1CI" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

&nbsp;
<p style="text-align: center;"><span style="font-size: x-large;"><span style="color: #808080;">¿Quieres recibir las mejores noticias de actualidad sobre los animales? </span></span></p>
<p style="text-align: center;"><span style="color: #0000ff;"><span style="font-size: x-large;">¡Suscríbete gratuitamente a nuestro</span><span style="font-size: x-large;"> <a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">e-</a></span><span style="font-size: x-large;">boletín</span><span style="font-size: x-large;">!</span></span></p>
Actualmente, Mike Laningan está al frente de Farmhouse Garden Animal Home con 21 vacas, gansos, caballos y un asno de guardia llamado «Buckwheat». El granjero que una vez vendió carne de vaca y de ternera para su beneficio, ahora cuida de todos estos animales vendiendo vegetales y jarabe de arce. Y todo es gracias a un cambio en su corazón que le permitió entender que todos los animales quieren vivir sus vidas en paz, y que es mucho lo que cada uno de nosotros puede hacer para que eso sea una realidad.
