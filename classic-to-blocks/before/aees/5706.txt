<p>
	<span>Hoy, Jueves 30 de Septiembre a las 14 horas, dos activistas de Igualdad Animal Laura N&uacute;&ntilde;ez e Iv&aacute;n Guijarro han sido juzgados&nbsp;</span> en el juzgado de instrucci&oacute;n n&ordm; 43 -en la Plaza de Castilla&nbsp;n&uacute;mero 1 de Madrid- bajo el cargo de desobediencia por haber saltado a la pasarela Cibeles el pasado 23 de Febrero del 2010 durante el desfile del dise&ntilde;ador Jesus Lorenzo, cuya colecci&oacute;n estaba compuesta en pr&aacute;cticamente su totalidad por prendas compuestas con pieles de animales.&nbsp;</p>
<p>
	A pesar de que los polic&iacute;as que formaban la acusaci&oacute;n han&nbsp;mantenido falsamente durante la vista que los activistas desobedecieron a la autoridad, llegando a argumentar que se resistieron dando codazos y atemorizando al p&uacute;blico neg&aacute;ndose a obedecer los polic&iacute;as tras haberse identificado estos. Las pruebas aportadas por Igualdad Animal demostraron claramente que los activistas simplemente desplegaron unos carteles de forma pac&iacute;fica tras lo cual fueron sacados&nbsp; de la pasarela. La fiscal&iacute;a argument&oacute; entonces que los cargos contra los activistas ser&iacute;an de &nbsp; alteraci&oacute;n del orden p&uacute;blico y no de desobediencia.</p>
<p>
	Tras las comparecencias, la fiscal ha pedido que los activistas sean condenados con una multa de 100 euros cada uno, el juicio ha quedado visto para sentencia.</p>
<p>
	Varios peleteros quisieron acudieron al juicio presencialmente, pero el juez no permiti&oacute; la entrada de nadie que no estuviera citado para hoy. Al juzgado acudieron adem&aacute;s una veintena de activistas para apoyar a Laura e Iv&aacute;n y varios medios de comunicaci&oacute;n nacionales.</p>
<p>
	Recordamos que dicho salto se produjo cuando el dise&ntilde;ador Jesus Lorenzo se dispon&iacute;a a recibir la ovaci&oacute;n final, consiguieron mostrar sendos carteles que le&iacute;an &quot;Piel es asesinato - Igualdad Animal&quot;, ante las c&aacute;maras de decenas de medios de comunicaci&oacute;n.&nbsp;Pese a las medidas de seguridad desplegadas, con decenas de guardas de seguridad, los activistas consiguieron llevar a cabo la acci&oacute;n.&nbsp;</p>
<p>
	Queremos aprovechar este juicio para defender a los animales v&iacute;ctimas de la moda y animar a un cambio social que lleve al fin de la utilizaci&oacute;n de animales como vestimenta.&nbsp;</p>
<p>
	Igualdad Animal present&oacute; el pasado Noviembre una investigaci&oacute;n en granjas de visones espa&ntilde;olas. Dicha investigaci&oacute;n, la primera de este tipo presentada en Espa&ntilde;a, mostraba como nacen, viven y mueren los animales explotados por su piel en m&aacute;s de la mitad de las granjas de visones de este pa&iacute;s.<br />
	El resultado de dicho trabajo puede verse en http://www.pielesasesinato.com</p>
<p>
	Si deseas apoyar a Igualdad Animal y el trabajo que estamos haciendo:</p>
<p>
	- Participa como activista. Escribe a <a href="mailto:%20info@igualdadanimal.org"><span>info@igualdadanimal.org</span></a>&nbsp;para m&aacute;s informaci&oacute;n.<br />
	- Hazte socio-colaborador y/o realiza una donaci&oacute;n para poder financiar las pr&oacute;ximas acciones y los gastos derivados de esta acci&oacute;n.</p>
<p>
	<span><a href="http://www.igualdadanimal.org/socios">http://www.igualdadanimal.org/socios</a></span><br />
	<span> <a href="http://%20www.igualdadanimal.org/donacion"><span>http:// www.igualdadanimal.org/donacion</span></a></span></p>
<p>
	<span>No te pierdas el v&iacute;deo de la acci&oacute;n:&nbsp;<a href="https://vimeo.com/15174838"><span>https://vimeo.com/15174838</span><br />
	<span> </span><span>L</span></a>a acci&oacute;n en im&aacute;genes:&nbsp;<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157623495745940/"><span>http://www.flickr.com/photos/igualdadanimal/sets/72157623495745940/<br />
	<br />
	<br />
	<img alt="" class="wp-image-14029" src="/app/uploads/2010/09/gentucilla.jpg" /></span></a></span><br />
	&nbsp;</p>

