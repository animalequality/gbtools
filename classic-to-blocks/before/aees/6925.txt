<p><img alt="" class="wp-image-9918" src="/app/uploads/2013/02/gripe_aviar_mexico2013.jpg" style="width: 220px; float: left;" /></p>
<p>Las autoridades mexicanas han matado ya a <strong>480.000 aves afectadas por un nuevo brote de gripe aviar AH7N</strong> detectado en el estado de Guanajuato.</p>
<p align="justify">Seg&uacute;n el Servicio Nacional de Sanidad, Inocuidad y Calidad Agroalimentaria (Senasica), el virus fue detectado en doce granjas de Guanajuato, de las que diez se dedican a la explotaci&oacute;n de gallinas para la producci&oacute;n de pollos de engorde y dos a la producci&oacute;n de huevos.</p>
<p align="justify"><strong>En junio de 2012</strong> se registr&oacute; un brote del mismo virus, que llev&oacute; a las autoridades y a productores a <strong>matar a m&aacute;s de 22 millones de aves afectadas.&nbsp;</strong></p>

