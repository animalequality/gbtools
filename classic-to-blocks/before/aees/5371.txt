<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<font color="#000000" face="Times New Roman" size="3"><img align="left" alt="" height="169" hspace="9" class="wp-image-13605" src="/app/uploads/2010/03/1246901549485.jpg" vspace="9" width="250" />El Parlamento de Navarra ha aprobado una declaraci&oacute;n institucional presentada por Convergencia de Dem&oacute;cratas de Navarra que reconoce los festejos taurinos de la Comunidad Foral como patrimonio cultural inmaterial de Navarra. La iniciativa ha sido apoyada adem&aacute;s por Uni&oacute;n del Pueblo Navarro y por el Partido Socialista de Navarra, mientras que Nafarroa Bai e Izquierda Unida de Navarra la han rechazado.</font></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<font size="3"><font face="Times New Roman"><font color="#000000"><o:p></o:p></font></font></font></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span lang="ES-TRAD" style="mso-ansi-language: es-trad"><font size="3"><font face="Times New Roman"><font color="#000000">Navarra se une as&iacute; a los gobiernos de Madrid, Murcia y Valencia en su lucha por proteger la matanza y tortura de animales ante el creciente rechazo por parte de la sociedad hacia estas pr&aacute;cticas<o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<font size="3"><font face="Times New Roman"><font color="#000000"><o:p></o:p></font></font></font></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000">El <span class="span2">parlamentario de CDN Jos&eacute; Andr&eacute;s Burguete</span> ha considerado &quot;conveniente y oportuno&quot; que el Parlamento reconozca &quot;el valor que los festejos taurinos tradicionales tienen en Navarra como parte del patrimonio cultural&quot;.<o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000"><o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000">El <span class="span2">grupo socialista</span>, si bien ha apoyado la resoluci&oacute;n, no considera que sea &eacute;ste un tema que est&eacute; en el &quot;orden de prioridades m&aacute;s importantes de los navarros&quot;.<o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000"><o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000">El portavoz de <span class="span2">Nafarroa Bai Patxi Zabaleta</span> ha se&ntilde;alado que no consideran que los festejos taurinos &quot;necesiten de ninguna declaraci&oacute;n institucional y eso es tergiversar un debate social que puede ser llevado a cabo dentro de la sociedad y no est&aacute; necesitado de ning&uacute;n apoyo de declaraci&oacute;n del Parlamento&quot;. A su juicio, &quot;no es necesario ni conveniente&quot; y tampoco &quot;respetuoso&quot; ya que quienes est&aacute;n en contra de las corridas de toros &quot;son merecedores de respeto&quot;.<o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000"><o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000">Desde <span class="span2">IUN, Ion Erro</span> ha manifestado que este debate no existe en la Comunidad foral y que es por tanto &quot;artificial&quot;. &quot;Hoy conviven perfectamente en nuestra comunidad los festejos taurinos con quien leg&iacute;timamente los pueden cuestionar. Parece una iniciativa importada de otras comunidades y parece que pretende meter el dedo al ojo a quien cuestiona estos festejos&quot;, ha se&ntilde;alado.<o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000"><o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000">Algunos de los festejos taurinos tradicionales de Navarra son los encierros, los recortes y las anillas. En todos ellos los toros o vaquillas son obligados a participar en contra de su voluntad, siendo marcados y transportados, sufriendo estr&eacute;s, lesiones, desorientaci&oacute;n y finalmente la muerte en una plaza o en un matadero.<o:p></o:p></font></font></font></span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt">
	<span style="mso-bidi-font-size: 8.5pt"><font size="3"><font face="Times New Roman"><font color="#000000"><o:p></o:p></font></font></font></span></p>
<p>
	<span style="font-family: 'times new roman'; font-size: 12pt; mso-fareast-font-family: 'times new roman'; mso-ansi-language: es; mso-bidi-font-size: 8.5pt; mso-fareast-language: es; mso-bidi-language: ar-sa"><font color="#000000">Desde Igualdad Animal seguiremos luchando para lograr la abolici&oacute;n de la tauromaquia, los encierros, los recortes y todo tipo de espect&aacute;culo en el que se utilice a cualquier animal. Puedes ver parte del trabajo que hemos realizado hasta ahora en los siguientes enlaces.</font></span></p>
<p>
	<a href="http://http://www.igualdadanimal.org/noticias/5361"><span style="font-family: 'times new roman'; font-size: 12pt; mso-fareast-font-family: 'times new roman'; mso-ansi-language: es; mso-bidi-font-size: 8.5pt; mso-fareast-language: es; mso-bidi-language: ar-sa"><font color="#000000">Descuelgue en el&nbsp;viaducto de Segovia en Madrid</font></span></a></p>
<p>
	<a href="http://https://vimeo.com/4579869"><u><font color="#000000">Descuelgue en la Giralda</font></u></a></p>
<p>
	<a href="http://http://www.youtube.com/watch?v=mbAsJV4HpgE"><u><font color="#000000">Descuelgue en las Ventas</font></u></a></p>
<p>
	<a href="http://https://vimeo.com/2094353"><u><font color="#000000">Descuelgue y salto a la plaza de Zaragoza</font></u></a></p>
<p>
	<a href="http://http://www.youtube.com/watch?v=BIH6dJYlvE4"><u><font color="#000000">Descuelgue y salto a la plaza de Barcelona</font></u></a></p>

