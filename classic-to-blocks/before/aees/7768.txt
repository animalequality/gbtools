<div class="center"><img class="wp-image-11921" src="/app/uploads/2017/04/impossible_foods.jpg" /></div>
<p>Carne, pescado, huevos&hellip; Nos gustan estos alimentos. Tenemos hambre de ellos. Sin embargo no nos gusta el calentamiento global, el hambre en el mundo ni el maltrato animal; y las industrias tras ellos son causantes principales de estos problemas globales. &iquest;Hay soluci&oacute;n? La respuesta es s&iacute;.</p>

<p>Si te alimentas de forma vegetariana o vegana vas a tener que reconocer que a la poblaci&oacute;n mundial le gusta la carne y no tanto las verduras. Si te alimentas de forma omn&iacute;vora vas a tener que reconocer que la industria c&aacute;rnica o pesquera son causantes del maltrato animal y de la destrucci&oacute;n de selvas.</p>

<p>&iquest;Qu&eacute; hacer?</p>

<p>Esa es la pregunta que se hicieron algunas de las mentes empresariales <strong>m&aacute;s brillantes</strong> que hay ah&iacute; fuera. La respuesta a la que llegaron colectivamente brilla con luz propia: cambiar a la industria alimentaria con productos similares a carne, pescado, huevos o l&aacute;cteos, sin animales en el proceso.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<p>&iquest;Te suena raro? M&iacute;ralo as&iacute;: tambi&eacute;n le suenan raros nuestros smartphones, port&aacute;tiles, Facebooks, Instagrams y Snaptchas a nuestros abuelos y abuelas.</p>

<p>A continuaci&oacute;n te presentamos 7 empresas dispuestas a <strong>salvar el mundo</strong> cambiando el juego en la industria alimentaria. &iquest;Qui&eacute;n sabe cu&aacute;ntas m&aacute;s est&aacute;n por llegar?</p>

<p>&nbsp;</p>

<h4>1. Memphis Meats</h4>

<p><img alt="" class="wp-image-11922" src="/app/uploads/2017/04/maxresdefault.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&laquo;Nuestro objetivo es sencillo: cambiar la forma en la que la carne llega a tu plato. Estamos desarrollando una manera de producir carne real a partir de c&eacute;lulas animales; sin la necesidad de alimentar, criar y matar a animales.&raquo;</p>

<p>Esta es la declaraci&oacute;n de principios de la empresa que ha tomado la delantera en el desarrollo de la <strong>&laquo;carne limpia&raquo;</strong>, carne producida a partir de c&eacute;lulas con la capacidad de autorreproducirse.</p>

<p>&laquo;Esperamos que nuestros productos sean mejores para el medioambiente (su producci&oacute;n requiere hasta un 90% menos de emisiones de gases de efecto invernadero, tierra y agua que que la carne convencional), para los animales y la salud p&uacute;blica. Y lo m&aacute;s importante: est&aacute;n deliciosos.</p>

<p>Parece que en Memphis Meats saben lo que nos gusta, &iquest;eh?</p>

<p>&nbsp;</p>

<h4>2. Impossible Foods</h4>

<p><img alt="" class="wp-image-11923" src="/app/uploads/2017/04/impossible_foods_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>El equipo de cient&iacute;ficos, desarrolladores y, en definitiva, amantes de la carne y la vida tras Impossible Foods lo tiene claro: &laquo;Plantas + Ciencia = Carne&raquo;.</p>

<p>Consiguen el mismo sabor a base de <strong>ingenio y talento</strong>, sin el alto precio de la carne convencional.</p>

<p>&laquo;La manera de producir carne hoy en d&iacute;a se est&aacute; cobrando un alto precio en nuestro planeta. Seg&uacute;n las investigaciones del sector, la industria ganadera utiliza hasta el 30% de todo el suelo del planeta, m&aacute;s del 25% de todo el agua potable y genera tantos gases responsables del efecto invernadero como todos los coches, camiones, trenes, barcos y aviones del mundo juntos.&raquo;</p>

<p>Bueno, si buscabas buenas razones para elegir alternativas a la carne, ah&iacute; tienes algunas.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://www.nytimes.com/es/2016/05/30/espanol/una-empresa-dice-haber-logrado-lo-imposible-una-hamburguesa-vegetariana-que-sabe-a-carne.html" target="_blank">Una empresa dice haber logrado lo imposible: una hamburguesa vegetariana que sabe a carne</a></strong>



<h4>3. New Wave Foods</h4>

<p><img alt="" class="wp-image-11924" src="/app/uploads/2017/04/newwavefoods.png" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>La pesca industrial est&aacute; vaciando los oc&eacute;anos a un ritmo tan acelerado que, seg&uacute;n los cient&iacute;ficos, &frac34; de las zonas de pesca est&aacute;n sobreexplotadas o exhaustas y a este ritmo <strong>los oc&eacute;anos se quedar&aacute;n literalmente vac&iacute;os para el a&ntilde;o 2048</strong>.</p>

<p>Y en New Wave Foods no est&aacute;n dispuestos a que eso suceda sin intentar evitarlo.</p>

<p>&laquo;Usando tecnolog&iacute;a, creamos alimentos alternativas al pescado que no requieren ser capturados en los fr&aacute;giles ecosistemas marinos. Para recrear los alimentos provenientes del mar que la gente ha comido durante siglos nos inspiramos en la madre naturaleza, haci&eacute;ndolo de una manera m&aacute;s sostenible.&raquo;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h4>4. Tomato Sushi</h4>

<p><img alt="" class="wp-image-11925" src="/app/uploads/2017/04/tomatosushi.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Nos encanta el sushi pero adem&aacute;s <strong>nos gusta ver a los majestuosos atunes en los mares</strong>. Pues Tomato Sushi tiene un mensaje claro para nosotros: &laquo;El sushi es alucinante. La extinci&oacute;n no.&raquo;</p>

<p>&laquo;Saludable y jugoso at&uacute;n, sin el at&uacute;n. Tomato Sushi es una alternativa natural y ecol&oacute;gica al at&uacute;n, creada por uno de los m&aacute;s importantes chefs de Estados Unidos.&raquo;</p>

<p><br />
&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="http://elpaissemanal.elpais.com/documentos/la-revolucion-la-carne-cultivada/" target="_blank">La revoluci&oacute;n de la &lsquo;carne cultivada&rsquo;</a></strong>

<p><br />
&nbsp;</p>

<h4>5. Beyond Meat</h4>

<p><img alt="" class="wp-image-11926" src="/app/uploads/2017/04/beyondburger.png" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>Beyond Meat est&aacute; entrando en los men&uacute;s de m&aacute;s y m&aacute;s restaurantes a ritmo de v&eacute;rtigo. &iquest;Su &uacute;ltimo &eacute;xito? Empezar a vender en la capital mundial del consumo de carne per c&aacute;pita, Hong Kong.</p>

<p>Y como nos dice Ethan Brown, fundador y director, &laquo;En Beyond Meat comenzamos haci&eacute;ndonos preguntas sencillas: &iquest;por qu&eacute; se necesita a un animal para conseguir carne?, &iquest;por qu&eacute; no se puede conseguir directamente de las plantas? Y result&oacute; que s&iacute; se puede, as&iacute; que lo hicimos. Esperamos que nuestra carne hecha a base de vegetales os permita a ti y a tu familia comer m&aacute;s (no menos) platos de los que tanto am&aacute;is mientras que os sent&iacute;s genial por los beneficios para la salud, la sostenibilidad y los animales que aportan las prote&iacute;nas vegetales.&raquo;</p>

<p>Desde luego, el p&uacute;blico objetivo de Ethan va m&aacute;s all&aacute; de <strong>vegetarianos y veganos</strong>: &eacute;l va a por los m&aacute;s carn&iacute;voros del lugar.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h4>6. Hampton Creek</h4>

<p><img alt="" class="wp-image-11927" src="/app/uploads/2017/04/hamptoncreek.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>Josh Tetrick, director de esta empresa que <strong>ha revolucionado los productos elaborados con huevo como la mayonesa</strong>, conoce muy bien a los lobbys de las industrias alimentarias. Su producto estrella, &laquo;Just Mayo&raquo;, mayonesa sin huevo, fue denunciado por el lobby de la industria del huevo en Estados Unidos. &laquo;&iquest;&iexcl;C&oacute;mo puede llamarse mayonesa a algo que no lleva huevo!?&raquo;, gritaban desde el multimillonario lobby.</p>

<p>Pues result&oacute; que se puede y adem&aacute;s, tuvieron que retirar la demanda.</p>

<p>&laquo;Deber&iacute;amos amar lo que comemos&raquo;, reza el lema de Hampton Creek. Sus productos como mayonesas, salsas y masa para galletas saben igual o mejor y cuestan lo mismo. &laquo;Eso es lo que son nuestros productos. Y esto es empezar desde abajo para construir un nuevo sistema alimenticio.&raquo;</p>

<p><br />
&nbsp;</p>

<strong>Noticia relacionada: <a href="https://www.eleconomista.es/empresas-finanzas/noticias/6397381/01/15/Carnicerias-vegetarianas-a-la-conquista-del-consumidor-global.html">Carnicer&iacute;as vegetarianas, a la conquista del consumidor global</a></strong>



<h4>7. La carnicera vegetariana</h4>

<p><img alt="" class="wp-image-11928" src="/app/uploads/2017/04/carnicerav.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Atenci&oacute;n porque esta opci&oacute;n <strong>tambi&eacute;n la puedes encontrar en Espa&ntilde;a</strong>. La carnicera vegetariana ofrece productos alternativos a la carne con un nombre de negocio que deja indiferente a pocos.</p>

<p>&laquo;Mi objetivo es liberar a los animales de la cadena alimenticia&raquo;, nos dice Jaap Korteweg, fundador de la empresa.</p>

<p>&laquo;Todos nuestros productos provienen de cultivos amigables con el medio ambiente donde no se talan selvas ni bosques. En la Carnicera Vegetariana tambi&eacute;n nos preocupamos por establecer una cadena de comercio justo.&raquo;</p>

<p><br />
&iexcl;As&iacute; s&iacute; que nos gusta comer carne!</p>

