Madrid - El Jueves 15 de Marzo es el Día Internacional contra la matanza de focas de Canada, una masacre en la que cientos de miles de focas -la mayoría de menos de tres meses de edad- mueren a golpes por su preciada piel y grasa.

En Igualdad Animal estamos organizando eventos y actos en Madrid y otras ciudades, nuestra intención es utilizar este día no solo para reivindicar el interés en vivir y no sufrir de esos animales sino para también denunciar el terrible destino que les espera a todos los animales no humanos utilizados para nuestro beneficio, víctimas todos ellos al igual que las focas de un prejuicio denominado especismo que establece que los intereses de los demás animales (no olvidemos que las/os humanas/os también somos animales) no merecen el mismo respeto que los nuestros.

Si quieres ayudarnos a organizar las actividades para este día escribenos a info@igualdadanimal.org

En breve subiremos información específica a la web sobre la matanza de focas y las actividades que se organizarán para este día.

