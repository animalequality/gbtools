<div class="center"><img class="wp-image-12034" src="/app/uploads/2017/05/8695280655_f0423a7512_z_0.jpg" /></div>
<p>Medios internacionales est&aacute;n publicando informaciones sobre una prohibici&oacute;n en la venta y consumo de carne de perro en el inminente <strong>festival de Yulin</strong> (China).</p>

<p><a href="https://www.bbc.com/news/world-asia-china-39958179" target="_blank">BBC</a>, <a href="https://time.com/4783802/china-yulin-dog-meat-festival/" target="_blank">TIME</a> e incluso el diario chino <a href="https://www.scmp.com/lifestyle/article/2094762/yulin-china-ban-sale-dog-meat-ahead-annual-dog-meat-festival-say-chinese" target="_blank">South China Morning Post</a> publican la noticia. Al parecer, la informaci&oacute;n proviene de activistas en China, quienes han declarado que el <strong>gobierno regional de Yulin habr&iacute;a prohibido la venta y consumo de carne de perro durante el Festival,</strong> que tendr&aacute; lugar el 21 de junio.</p>

<p>&nbsp;</p>

<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n. &nbsp;</font></font></p>

<p><font size="5">&nbsp;</font></p>

<p>Peter Li, abogado de la organizaci&oacute;n de protecci&oacute;n animal Humane Society International declaraba a BBC que &laquo;las autoridades ya hab&iacute;an tratado en otros a&ntilde;os disuadir la pr&aacute;ctica pero este a&ntilde;o habr&iacute;a fuertes multas para los infractores.&raquo;</p>

<p><font size="5"><img alt="" class="wp-image-12035" src="/app/uploads/2017/05/8695104375_970bbb5867_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; " /></font></p>

<p>La normativa afectar&iacute;a a vendedores, mercados y restaurantes, a&ntilde;ad&iacute;a Li. Al parecer, sin embargo, ser&iacute;a temporal y <strong>no se ha anunciado a&uacute;n oficialmente</strong>.</p>

<h4><font size="5">&nbsp;</font></h4>

<strong>Noticia relacionada: <a href="https://www.abc.es/natural/vivirenverde/abci-prohiben-venta-carne-perro-festival-yulin-201705191124_noticia.html" target="_blank">Proh&iacute;ben la venta de carne de perro en el festival de Yulin</a></strong>

<p><font size="5">&nbsp;</font></p>

<p>Los activistas atribuyen el cambio de pol&iacute;tica al nuevo secretario del Partido Comunista en Yulin, Mo Gong Ming. Supuestamente, Ming querr&iacute;a limpiar la imagen de la ciudad China, <strong>muy deteriorada internacionalmente por el consumo de carne de perro </strong>durante el festival anual.</p>

<p><font size="5">&nbsp;</font></p>

<p>A nivel de calle, sin embargo, la prohibici&oacute;n no parece que haya sido comunicada a los restaurantes y vendedores. &laquo;&iquest;Prohibir la carne de perro?, no he o&iacute;do nada&raquo;, declaraba el propietario de un conocido restaurante que sirve carne de perro en Yulin. Al respecto, los activistas declaraban que solo algunos restaurantes han sido notificados por ahora. La prohibici&oacute;n <strong>se har&iacute;a efectiva el 15 de junio</strong>, una semana antes del inicio del festival. Quienes infringiesen la ley se expondr&iacute;an al arresto y a multas de hasta 13.000&euro;.</p>

<p><font size="5">&nbsp;</font></p>

<strong>Noticia relacionada: <a href="https://www.abc.es/sociedad/abci-500000-firmas-contra-comercio-carne-perros-y-gatos-china-201512170909_noticia.html" target="_blank">500.000 firmas contra el comercio de carne de perros y gatos en China</a></strong>

<p>&nbsp;</p>

<p>En estos momentos <strong>la situaci&oacute;n es confusa</strong>, y distintos representantes del gobierno local de Yulin declaraban a BBC no saber nada al respecto de la prohibici&oacute;n. Se estima que durante el festival de 2.000 a 3.000 perros mueren para ser consumida su carne. Sin embargo, en China, <strong>la mayor&iacute;a de personas ven a los perros como compa&ntilde;eros</strong>, al igual que sucede en occidente.</p>

