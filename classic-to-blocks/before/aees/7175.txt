<div class="center"><img class="wp-image-10182" src="/app/uploads/2014/11/killer.jpg" /></div>
<p><img alt="" class="wp-image-13712" src="/app/uploads/2014/11/Killer.jpg" />Desde su estreno en televisi&oacute;n hace tres semanas, el programa Killer Karaoke de Cuatro se ha visto envuelto en una grave pol&eacute;mica: el maltrato animal en su plat&oacute;.</p>

<p>Ranas, gusanos, serpientes e incluso un caim&aacute;n, son lanzados sobre los concursantes o pisados por ellos en toda una serie de pruebas donde los animales son parte del espect&aacute;culo.</p>

<p>El estr&eacute;s causado a estos animales al sacarlos de su entorno habitual y ser encerrados, lanzados por los aires, pisados e incluso obligados a permanecer junto a sus depredadores naturales, es inimaginable.</p>

<p>El programa ha tratado de defenderse de las cr&iacute;ticas informando que hay una persona encargada de asegurar el bienestar de los animales en el programa.</p>

<p>Sin embargo, tanto desde las redes sociales como para las organizaciones defensoras de los animales,&nbsp; esta respuesta es insuficiente y se exige a los responsables del programa que retiren a los animales del concurso.</p>

<p>La Fundaci&oacute;n FAADA ha iniciado una petici&oacute;n de firmas para que Killer Karaoke deje de utilizar animales definitivamente: <a href="https://www.change.org/p/cuatro-mediaset-y-magnolia-dejad-de-utilizar-animales-para-killer-karaoke">https://www.change.org/p/cuatro-mediaset-y-magnolia-dejad-de-utilizar-animales-para-killer-karaoke</a></p>

<p>Desde Igualdad Animal mostramos nuestra oposici&oacute;n al uso de animales como reclamo televisivo, para ello desde la organizaci&oacute;n se impulsa el uso del HT #MaltratoKaraoke en las redes sociales para mostrar el rechazo de la sociedad.</p>

<p>Instamos a los responsables del programa a oponerse al maltrato animal retirando a los animales del concurso de forma inmediata y buscando otras formas de diversi&oacute;n que no supongan sufrimiento para los animales.</p>

<p>&nbsp;</p>

