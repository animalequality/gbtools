<div class="center"><img class="wp-image-11929" src="/app/uploads/2017/04/shutterstock_266544626.jpg" /></div>
<h4>1. Violeta la peque&ntilde;a cabrita</h4>

<p><iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/iwTs2PL_zuE" width="640"></iframe></p>

<p>V&iacute;deo, <a href="https://www.youtube.com/channel/UC2VpDQ3IbfTxVx3UgNz08TQ" target="_blank">Catskill Animal Sanctuary</a></p>

<h4>2. Los animales felices de Catskill Animal Sanctuary</h4>

<p><iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/9EzZ9eGswqM" width="640"></iframe></p>

<p>V&iacute;deo, <a href="https://www.youtube.com/channel/UC2VpDQ3IbfTxVx3UgNz08TQ" target="_blank">Catskill Animal Sanctuary</a></p>

<h4>3. Gallinas, madres cari&ntilde;osas y protectoras</h4>

<p><iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/cuqpyyIMhUQ" width="640"></iframe></p>

<p>V&iacute;deo, <a href="https://www.youtube.com/channel/UCAPoMJpNngbBeBJnmvLc34g" target="_blank">Santuario Igualdad Interespecie</a></p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>

<p>&nbsp;</p>

<h4>4. Ocho cerditos de lo m&aacute;s felices y tiernos</h4>

<p><iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/AMZKHr0N9i0" width="640"></iframe></p>

<p>V&iacute;deo, <a href="https://www.youtube.com/channel/UCgfnQIQVXWBwLPxYRvcsv2g" target="_blank">Edgar&#39;s Mission</a></p>

<h4>5. El v&iacute;deo m&aacute;s feliz de Edgar&rsquo;s Mission</h4>

<p><iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/kIF3BYBXZWA" width="640"></iframe></p>

<p>V&iacute;deo, <a href="https://www.youtube.com/channel/UCgfnQIQVXWBwLPxYRvcsv2g" target="_blank">Edgar&#39;s Mission</a></p>

<h4>6. Olivia, la gallina fan de National Geographic</h4>

<p><iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/2YqAui9c-ss" width="640"></iframe></p>

<p>V&iacute;deo de <a href="https://www.youtube.com/channel/UC5MzAFj1U6Djzw6nUnsrLjg" target="_blank">Animal Place</a></p>

