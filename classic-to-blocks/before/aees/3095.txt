Rosario, Argentina - El Consejo Municipal de Rosario ha señalado que las personas que paseen con perros grandes o considerados "peligrosos" por la sociedad, deberán emplear correa corta y bozal obligatoriamente. Esto incluye a los perros con un peso superior a 30 kilos o de determinadas razas.

El uso de correa corta al pasear con perros supone que estos vean sus movimientos severamente limitados, así como problemas de respiración al tirar de la correa.
