<div class="center"><img class="wp-image-12139" src="/app/uploads/2017/06/shutterstock_271169489.jpg" /></div>
<p>Tras conversaciones con el departamento de responsabilidad social corporativa de Igualdad Animal, Huevos Guill&eacute;n <a href="http://www.huevosguillen.com/Huevos%20Guillen%20se%20compromete%20con%20la%20producci%C3%B3n%20alternativa.pdf" target="_blank">ha anunciado</a> que eliminar&aacute; las jaulas en su cadena de producci&oacute;n para 2025. Esta decisi&oacute;n afectar&aacute; a 1.000.000 de gallinas que dejar&aacute;n de vivir en jaulas tan pronto como 2018, y cada a&ntilde;o hasta 2025, cuando 5.000.000 de gallinas dejar&aacute;n las jaulas cada a&ntilde;o.</p>

<p><img alt="" class="wp-image-12140" src="/app/uploads/2017/06/shutterstock_271169489_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 400px; " /></p>

<p>&nbsp;</p>

<p>Este anuncio es el m&aacute;s reciente en sumarse a una larga lista protagonizada por empresas del sector alimentario que tras conversaciones con Igualdad Animal y otras organizaciones, se han comprometido a dejar de enjaular a las gallinas.</p>

<p>Aunque libre de jaulas no significa libre de maltrato, estas empresas est&aacute;n dando un gran paso hacia la reducci&oacute;n del sufrimiento de las gallinas.</p>

