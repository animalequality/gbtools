<div class="center"><img class="wp-image-10903" src="/app/uploads/2016/08/juntos.jpg" /></div>
<p><img alt="" class="wp-image-10902" src="/app/uploads/2016/08/feliz.jpg" style="float:left; margin:10px; width:450px" />Dejar la carne (y el pescado) fuera de tu plato salva vidas. Muchas, muchas vidas.</p>

<p>El n&uacute;mero de animales que mueren al a&ntilde;o en manos de las industrias alimentarias es inimaginable. La ganader&iacute;a industrial y la pesca industrial son responsables <strong>del peor y m&aacute;s sangriento maltrato animal que se conozca</strong>. Sin embargo, hay buenas noticias.</p>

<p><strong>El n&uacute;mero de personas que est&aacute;n dejando la carne y el pescado fuera de su alimentaci&oacute;n aumenta</strong>. Otras est&aacute;n comiendo cada vez menos estos productos. Las causas por las que lo hacen van desde <a href="https://igualdadanimal.org/blog/6-razones-por-las-que-estar-contra-la-ganaderia-industrial-te-hace-marcar-la/">evitar el maltrato animal</a>, comer de forma m&aacute;s sostenible hasta por la propia salud.</p>

<p>&iquest;A cu&aacute;ntos animales est&aacute;n ayudando estas personas? Los c&aacute;lculos estiman que una persona que pasa a alimentarse <a href="https://igualdadanimal.org/blog/como-conseguir-dejar-la-carne-fuera-de-tu-alimentacion/">de forma vegetariana</a> evita de forma directa e indirecta la muerte de <strong>alrededor de 25 animales de granja al a&ntilde;o</strong>.</p>

<p>25 animales como pollos, corderos, cerdos o vacas que no ser&aacute;n criados en <strong>horribles granjas industriales</strong> y enviados al matadero.</p>

<p>Por si esto fuera poco, cuando se a&ntilde;aden al c&aacute;lculo los peces y otros animales marinos <strong>salvados por una alimentaci&oacute;n vegetariana</strong> el n&uacute;mero sube exponencialmente.</p>

<p>Esto es as&iacute; porque <strong>la pesca industrial y las piscifactor&iacute;as</strong> son las responsables del mayor n&uacute;mero de muertes de animales <a href="https://igualdadanimal.org/noticia/2016/07/22/los-peces-son-los-animales-mas-olvidados-y-nos-necesitan/">para consumo humano</a>. Y no solo para consumo, los animales descartados en la pesca industrial (animales capturados en las redes que mueren pero que no son para consumo) son innumerables.</p>

<p>As&iacute;, el n&uacute;mero de peces y otros animales marinos que dejan de morir al a&ntilde;o gracias a cada persona vegetariana es de <strong>alrededor de 550</strong>.</p>

<p>Sumando los animales de granja a los peces y animales marinos obtenemos un total de alrededor de 575 animales. <strong>575 animales al a&ntilde;o que no morir&aacute;n debido a nuestros h&aacute;bitos de consumo</strong>.</p>

<p>&iquest;No es alentador saber <a href="https://igualdadanimal.org/blog/6-cosas-increiblemente-buenas-que-pasan-al-reducir-tu-consumo-de-carne/">la enorme diferencia</a> que podemos marcar gracias a nuestra alimentaci&oacute;n? Una forma de <strong>ayudar a los animales m&aacute;s necesitados</strong> de forma directa y a diario.</p>

<p><strong>Si quieres ayudar a acabar con el maltrato animal, por favor considera consumir alternativas vegetales a la carne y el pescado. Tienes toda la informaci&oacute;n y recetas que necesitas en las fant&aacute;sticas websites <a href="https://www.gastronomiavegana.org/">Gastronom&iacute;a Vegana</a> y <a href="https://danzadefogones.com/">Danza de Fogones</a></strong>.</p>

