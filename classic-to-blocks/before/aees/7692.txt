<div class="center"><img class="wp-image-11661" src="/app/uploads/2017/02/shutterstock_306441431_2.jpg" /></div>
<p>No, la quema de combustibles f&oacute;siles no es el factor n&uacute;mero uno del cambio clim&aacute;tico, mejor qu&iacute;tate esta idea obsoleta de la cabeza porque los estudios independientes no la apoyan.</p>

<p>Seg&uacute;n la Organizaci&oacute;n Mundial de la Alimentaci&oacute;n y Agricultura (FAO por sus siglas en ingl&eacute;s), el factor principal es la producci&oacute;n de alimentos a trav&eacute;s de la ganader&iacute;a industrial. <a href="http://www.fao.org/3/i3437e.pdf" target="_blank">Esta actividad es responsable del 14,5% del total de emisiones de gases de efecto invernadero</a>.</p>

<p>Sin embargo, para la organizaci&oacute;n medioambiental internacional Worldwatch Institute, esta cifra subestima las emisiones del sector ganadero. Sumadas todas las emisiones indirectas (respiraci&oacute;n del ganado, uso de tierra y emisi&oacute;n de metano) y corregidos o actualizados los c&aacute;lculos realizados por la FAO, l<a href="https://www.worldwatch.org/files/pdf/Livestock%20and%20Climate%20Change.pdf" target="_blank">a ganader&iacute;a industrial ser&iacute;a responsable del 51% de las emisiones de gases de efecto invernadero</a>.</p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p>&laquo;Una alimentaci&oacute;n vegetariana reducir&iacute;a hasta un 63% las emisiones de gases de efecto invernadero, una vegana, hasta un 70%.&raquo;</p></FONT>


<p>&nbsp;</p>

<p>La suma total de los gases emitidos por toda la industria mundial de transportes (coches, aviones y barcos incluidos) da como resultado solo el 22%.</p>

<p>Adem&aacute;s de las emisiones de gases precursores del cambio clim&aacute;tico, <a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/" target="_blank">las industrias c&aacute;rnica y l&aacute;ctea son responsables del mayor derroche de agua potable en el mundo</a>.</p>

<p><img alt="" class="wp-image-11660" src="/app/uploads/2017/02/shutterstock_109083668.jpg" style="float:left; margin:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>El calentamiento global es una realidad que nos afecta a todos</strong>. Figuras pol&iacute;ticas, europarlamentarios e instituciones est&aacute;n empezando a tomar medidas. Algunos afirman ya que <a href="https://www.publico.es/sociedad/luchar-cambio-climatico-reducir-consumo.html" target="_blank">&laquo;para luchar contra el cambio clim&aacute;tico deber&iacute;amos reducir cinco veces nuestro consumo de carne.&raquo;</a></p>

<p>&nbsp;</p>

<FONT SIZE=5><FONT COLOR=#808080><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscr&iacute;bete ahora a nuestro e-bolet&iacute;n</a> y recibir&aacute;s de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentaci&oacute;n.</p></FONT>


<p>&nbsp;</p>

<p>La producci&oacute;n y consumo global de carne <strong>se han multiplicado por cuatro desde 1960</strong>. Mirando a&uacute;n m&aacute;s atr&aacute;s en el tiempo, la producci&oacute;n se ha multiplicado por 25 desde 1800. Las proyecciones son a&uacute;n peores, se espera que para el a&ntilde;o 2050, con una poblaci&oacute;n humana de 9.600 millones, el consumo de carne se multiplique por siete.</p>

<p>Seg&uacute;n un reciente estudio de la Universidad de Oxford, en el que se investigaban los efectos en el medioambiente de cuatro tipo diferentes de dietas, <a href="http://www.futureoffood.ox.ac.uk/news/plant-based-diets-could-save-millions-lives-and-dramatically-cut-greenhouse-gas-emissions" target="_blank">una alimentaci&oacute;n vegetariana reducir&iacute;a hasta un 63% las emisiones de gases de efecto invernadero, una vegana, hasta un 70%</a>.</p>

<p>Y todo esto en un escenario en el que los expertos en salud nos llevan tiempo diciendo que nuestro actual consumo de carne es tan elevado que, literalmente, nos est&aacute; enfermando.</p>

<p>En Espa&ntilde;a cada persona consume de media al a&ntilde;o <strong>100 kg de carne</strong>, mientras que la media mundial es de 40 kg. Este consumo deja una huella de carbono que est&aacute; en nuestras manos (o en nuestras cestas de la compra) reducir.</p>

