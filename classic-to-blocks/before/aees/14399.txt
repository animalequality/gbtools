Los animales que viven en casas, en particular los perros, son víctimas de las explosiones y luminosidades multicolores que causa el uso de pirotecnia  en las Fiestas.

“Ese susto que muestran los perros al explotar los cohetes tiene que ver con una respuesta instintiva y natural de protegerse de truenos y rayos, que es el sonido que ellos perciben al estallar algún articulo de pirotecnia. Por esa razón buscan lugares oscuros y protegidos, como por ejemplo debajo de una cama o de una mesa”, explicó el veterinario Luis De Chazal.

El profesional indicó que no todos los perros reaccionan de la misma manera, sino que hay algunos que tienen la tendencia de salir corriendo. “Esto tiene que ver con la necesidad de escapar de esa zona peligrosa y no paran hasta que encuentran un lugar alejado de las explosiones o hasta que cesan las detonaciones”, agregó.

“Entonces, ¿qué hacer? Existe un tratamiento prolongado que consiste en acostumbrar al animal mediante la reproducción de ruidos similares emitidos a través de un equipo de audio. El responsable del perro, durante la reproducción, debe  acariciar y hablarle al animal mostrándole calma y regalándole una golosina”, agregó De Chazal.

Una de las soluciones inmediatas es recurrir a tranquilizantes en gotas que se venden en las veterinarias. Si no se puede contar  con estos medicamentos cuando el perro se esconda, se aconseja acompañarlo, acariciarlo, hablarle e impartirle calma.

Según el veterinario, es importante colocarle al perro una placa de identificación sólo con el teléfono de su casa y nunca con el nombre del animal. “Son muy sencillas de conseguir en cualquier veterinaria y se colocan en el cuello de la mascota sujeta a una correa”, destacó De Chazal.

Una vez encerrado, hay que dejarle un juguete o algo para morder, además de agua y el alimento balanceado. “Si no se puede estar junto al animal es bueno encerrarlo en un baño o en una habitación siempre oscura que tenga un mueble donde guarecerse, y en última instancia recurrir a los tranquilizantes. Bien suministrados no hacen daño al animal”, explicó.

La excitación es el principal síntoma de los perros en estos casos. También pueden sufrir temblores, náuseas, insuficiencia respiratoria, jadeo y salivación, estrés, taquicardia, vómitos, convulsiones y en animales cardíacos pueden ser víctimas de un infarto, remarcó el doctor De Chazal.
