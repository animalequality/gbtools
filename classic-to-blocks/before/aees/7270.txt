Estas fotografías tomadas por el equipo de investigación de Igualdad Animal muestran el terror que padecen los cerdos durante su viaje al matadero.

A menudo privados de comida y agua durante horas, <a href="https://www.youtube.com/watch?v=3XjG1vx36GE" target="_blank" rel="noopener noreferrer">los activistas han documentado de primera mano la desesperación de los animales en estos trayectos</a>.

<img class="wp-image-13859" style="margin: 5px; width: 885px;" src="/app/uploads/2015/07/cerdo4.jpg" alt="">

Más de 85.000 cerdos mueren cada año en España de camino al matadero. Una de las causas es un factor genético, el llamado gen del halotano. Uno de los alelos de este gen es responsable de una alteración muscular que provoca lo que se conoce con el nombre de 'Hipertermia maligna del Cerdo' (también conocido como Síndrome de Estrés Porcino o de la Muerte súbita). La presencia de este gen alterado hace a algunos cerdos especialmente sensibles al estrés, de manera que ante situaciones tan estresantes como el transporte al matadero, su cuerpo reacciona con una contracción muscular prolongada que da lugar a un incremento de la temperatura corporal que desemboca en la muerte del animal por paro cardíaco.

<img class="wp-image-13858" style="margin: 5px; width: 885px;" src="/app/uploads/2015/07/cerdo2.jpg" alt="">

<img class="wp-image-10268" style="margin: 5px; width: 885px;" src="/app/uploads/2015/07/cerdo3.jpg" alt="">

Debido a las extremas temperaturas en verano y la falta de agua, muchos animales mueren exhaustos. En invierno muchos de ellos morirán congelados.

<img class="wp-image-13857" style="margin: 5px; width: 885px;" src="/app/uploads/2015/07/cerdo1.jpg" alt="">

Las condiciones son tan terribles que algunos de los cerdos con las patas rotas caen en el suelo del camión y mueren sofocados bajo el resto .

Cuando llegan al matadero y con el fin de sacar a los animales fuera de los camiones, en muchas ocasiones son golpeados y pateados por los trabajadores.

Y esto es lo que sucede cuando entran al matadero:

&nbsp;
<div class="ae-video-container"><iframe src="https://player.vimeo.com/video/1321076?color=00c4ff&amp;title=0&amp;byline=0&amp;portrait=0" width="885px" height="708px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
&nbsp;

&nbsp;
