<p align="justify">Rahm Emanuel, alcalde de la ciudad de Chicago, apareci&oacute; el pasado viernes en televisi&oacute;n solicitando a los habitantes de la ciudad que probaran a llevar una <strong>dieta vegana</strong> como forma de mejorar su salud.</p>
<p align="justify">El alcalde fue entrevistado junto al creador de la dieta &ldquo;Engine 2 Diet&rdquo;, el triatleta y ex bombero Esselystyn Rip, como parte de una campa&ntilde;a de <strong>promoci&oacute;n de la alimentaci&oacute;n saludable en la ciudad de Chicago.</strong></p>
<p><iframe frameborder="0" height="335" marginheight="0" marginwidth="0" scrolling="no" src="http://widget.newsinc.com/single.html?WID=1&amp;VID=24209910&amp;freewheel=69016&amp;sitesection=wgn&amp;w=601&amp;h=338" width="580"></iframe></p>
<p align="justify">&nbsp;</p>
<p align="justify"><strong>Emanuel avala la dieta vegana como la forma de combatir la actual epidemia de enfermedades cr&oacute;nicas </strong>y<strong>&nbsp;</strong>la obesidad<strong>*</strong>, promover la <strong>longevidad </strong>y reducir los altos costos de atenci&oacute;n m&eacute;dica.&nbsp;</p>
<p align="justify"><em>&laquo;Tenemos que mejorar la forma en que comemos y controlar nuestro peso&raquo;</em>, dijo avalando por completo las dietas 100% vegetales.</p>
<p align="justify">Rahm Emanuel es ex jefe de personal del presidente Barack Obama y est&aacute; considerado como <strong>&ldquo;el alcalde m&aacute;s apto&rdquo; </strong>de todo EE.UU.</p>
<p align="justify">&nbsp;</p>
<p align="justify"><br />
	<strong>*</strong><em><small>En <strong>Espa&ntilde;a </strong>la <strong>obesidad </strong>afecta a uno de cada cuatro adultos y a uno de cada tres ni&ntilde;os, lo cual nos sit&uacute;a a la cabeza en tasa de obesidad infantil del mundo.</small></em></p>

