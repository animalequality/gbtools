<p><img alt="" class="wp-image-10232" src="/app/uploads/2015/03/elefant.jpg" style="float:right; margin-left:5px; margin-right:5px; width:450px" />El circo Ringling Bros. y Barnum &amp; Bailey pondr&aacute;n fin a sus actuaciones con elefantes. La compa&ntilde;&iacute;a matriz del circo, Feld Entertainment, ha declarado en exclusiva a The Associated Press (AP) que las actuaciones desaparecer&aacute;n para 2018.</p>

<p>Han indicado que esta decisi&oacute;n se da por la sensibilizaci&oacute;n del p&uacute;blico sobre el maltrato que padecen los animales. &ldquo;Nuestro p&uacute;blico ha mostrado un cambio respecto al cuidado de nuestros elefantes, mucha gente no se siente c&oacute;moda con nuestra gira&rdquo; dijo Alana Feld, vicepresidenta ejecutiva de la compa&ntilde;&iacute;a.</p>

<p>Adem&aacute;s existe una fuerte campa&ntilde;a contra los circos que utilizan animales, que ha hecho que existan nuevas legislaciones m&aacute;s restrictivas en torno a los circos. En los &uacute;ltimos tiempos han salido a la luz distintas investigaciones que mostraban la brutalidad que padec&iacute;an los elefantes en este circo.</p>

<p>&nbsp;</p>

<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/J8Y9gBaPZXE" width="885px"></iframe></div>

<p>&nbsp;</p>

<p>Kenneth Feld, presidente de la compa&ntilde;&iacute;a indic&oacute; que ten&iacute;an muchas dificultades para relizar los tours por las distintas legislaciones en torno al uso de animales en los circos.</p>

<p>&nbsp;</p>

