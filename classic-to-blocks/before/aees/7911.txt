<div class="center"><img class="wp-image-12467" src="/app/uploads/2017/09/shutterstock_95001850_0.jpg" /></div>
<p>Jen y Tray Federici eran due&ntilde;os de una granja muy productiva dedicada b&aacute;sicamente a criar vacas para enviarlas al matadero. El negocio era rentable y todo parec&iacute;a ir genial, hasta que un d&iacute;a se dieron cuenta de que no pod&iacute;an seguir dedic&aacute;ndose a hacer eso.<br />
<br />
Ocurri&oacute; que un d&iacute;a decidieron que solo comer&iacute;an la carne de animales que ellos mismo hubiesen matado, pero llegado el momento no fueron capaces de matar a una vieja vaca que hab&iacute;an seleccionado de su reba&ntilde;o. El hecho los sacudi&oacute; de tal forma que <strong>se vieron cuestionando tanto su forma de alimentarse como su trabajo</strong>. Y a partir de &nbsp;entonces sus vidas y las de los animales que criaban cambiaron para siempre.</p>

<p><br />
<br />
<FONT SIZE=5><FONT COLOR=#808080>&laquo;Decidimos seguir adelante no como consumidores de animales sino como sus embajadores&raquo;.</p></FONT>

<p><br />
<br />
&laquo;Oficialmente, comimos nuestro &uacute;ltimo jam&oacute;n de navidad en 2016. Nunca nos retractamos. Juramos no volver a comer carne ni otro producto de origen animal&raquo;, dice Jen Federici. Y para el verano de este a&ntilde;o convirtieron su rancho en un refugio de animales sin fines de lucro.<br />
&nbsp;</p>

<p><img alt="" class="wp-image-12468" src="/app/uploads/2017/09/refugio_animales_cabra.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" /></p>

<p><br />
&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>FOTO &copy;The Surf and Turf Sanctuary</p>

<p>&nbsp;</p>

<p>En <a href="https://www.facebook.com/pg/thesurfandturfsanctuary/photos/?ref=page_internal" target="_blank">The Surf and Turf Sanctuary</a> conviven terneros, cerdos, cabras, pollos y cebras junto a otros animales rescatados, todos lejos del sufrimiento y la muerte de las granjas y mataderos.<br />
<br />
<img alt="" class="wp-image-12469" src="/app/uploads/2017/09/refugio_animales.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><br />
Tray y Jen aseguran que <strong>desde que llevan una alimentaci&oacute;n basada en vegetales nunca se hab&iacute;an sentido mejor </strong>en toda su vida o m&aacute;s saludables tanto f&iacute;sica como mentalmente. Y en cuanto a los animales que dejaron de enviar al matadero y que ahora protegen declaran: &laquo;Al igual que ning&uacute;n humano, ning&uacute;n animal quiere morir. Son seres incre&iacute;bles y conmovedores. Creemos que cada animal merece la vida que le pertenece&raquo;.<br />
&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>FOTO: &copy;The Surf and Turf Sanctuary</p>

<p>&nbsp;</p>

<p>Fuente: http://vegnews.com/articles/page.do?pageId=10110&amp;catId=1</p>

