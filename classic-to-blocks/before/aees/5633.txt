<div>
	<img alt="" height="170" class="wp-image-13592" src="/app/uploads/2010/08/01_galapagos_21082010.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 300px; " width="300" />Cuatro activistas de Igualdad Animal han documentado con c&aacute;maras ocultas el acoso y martirio sufrido esta ma&ntilde;ana por un toro en el denominado &quot;Encierro&nbsp;por el Campo&quot; celebrado en la localidad de Gal&aacute;pagos (Guadalajara).</div>
<div>
	&nbsp;</div>
<div>
	Alrededor de cuatrocientos aficionados &ndash;cuarenta de los cuales montados a caballo&ndash; y sesenta jeeps han perseguido durante una hora y media (desde las once y media hasta la una del mediod&iacute;a) al toro en los campos alrededor de Gal&aacute;pagos. Los activistas han sido testigos y obtenido <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157624774999364">im&aacute;genes</a> que muestran a los taurinos lanzando piedras al animal, golpe&aacute;ndolo con palos y atropell&aacute;ndole.</div>
<div>
	&nbsp;</div>
<div>
	El toro, asustado en todo momento ante tal acoso, trat&oacute; de evadirse de sus agresores buscando refugio en dos ocasiones entre unos matorrales. Los vecinos del lugar aprovecharon el momento para lanzar grandes piedras contra &eacute;l y embestirle con un jeep tratando as&iacute; de obligarle a salir y continuar la diversi&oacute;n a su costa. Finalmente y tras permanecer entre los matorrales media hora, el animal fue atropellado por un jeep e instantes despu&eacute;s se pudo escuchar un disparo que pondr&iacute;a fin a la vida del toro y al &#39;festejo&#39;.<br />
	<br />
	V&iacute;deo:&nbsp;<a href="https://vimeo.com/14323978">http://</a><a href="https://vimeo.com/14323978">vimeo.com/14323978</a></div>
<div>
	&nbsp;</div>
<div>
	Como organizaci&oacute;n de derechos animales, Igualdad Animal manifiesta su profundo rechazo hacia este evento as&iacute; como hacia cualquier otra forma de explotaci&oacute;n animal (ya sea para entretenimiento como en este caso, o para vestimenta, alimentaci&oacute;n o investigaci&oacute;n) y anima a la sociedad a ponerse en el lugar de los animales y adoptar un estilo de vida vegano.</div>
<div>
	&nbsp;</div>
<div>
	Igualdad Animal es una organizaci&oacute;n internacional de derechos animales dedicada a promover el respeto hacia los animales que trabaja en Espa&ntilde;a, Inglaterra, Polonia, Italia y Venezuela.<br />
	<br />
	<div>
		Otros eventos similares documentados por Igualdad Animal:</div>
	<div>
		&bull; <a href="http://www.igualdadanimal.org/noticias/5632">&quot;Becerrada&quot; de Deltebre</a></div>
	<div>
		&bull; <a href="http://www.igualdadanimal.org/noticias/5632">&quot;Toro embolado&quot; de Amposta</a></div>
	<div>
		&bull; <a href="http://www.igualdadanimal.org/noticias/5624">&quot;Becerrada&quot; de Segovia</a></div>
	<div>
		&bull; <a href="http://www.igualdadanimal.org/noticias/5631">&quot;Rejoneo&quot; en Legan&eacute;s</a></div>
	<div>
		&bull; <a href="http://www.igualdadanimal.org/noticias/5608">&quot;Becerrada&quot; de El Escorial</a></div>
	<div>
		&bull; <a href="https://igualdadanimal.org/noticia/2009/09/15/activistas-de-igualdad-animal-graban-imagenes-de-la-matanza-del-toro-de-la-vega-en-tordesillas/">Muerte de Moscatel en el Toro de La Vega de Tordesillas</a></div>
	<div>
		&bull; <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157622481671222/">Muerte de Valent&oacute;n en el Toro de La Vega de Tordesillas</a><br />
		&nbsp;</div>
</div>

