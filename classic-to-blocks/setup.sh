#!/bin/bash

declare -A DB_MAP=(
    [aeus]=animalequalityorg
    [aeuk]=animalequalityorguk
    [aees]=igualdadanimalorg
    [aemx]=igualdadanimalmx
    [aebr]=animalequalityorgbr
    [aede]=animalequalityde
    [aein]=animalequalityin
    [aeit]=animalequalityit
    [lvus]=lovevegcom
    [lvuk]=loveveguk
    [lves]=loveveges
    [lvmx]=lovevegmx
    [lvbr]=lovevegcombr
    [lvde]=lovevegde
    [lvin]=lovevegin
    [lvit]=lovevegit
)

prepare() {
    for y in after/${1:-*}; do
	cc=$(basename $y);
	ids=$(find $y/ -type f -printf "%f,"|sed -r 's/\.txt|,$//g')
	rsync -avui $y/ $cc:/tmp/content/;
	find $y -type f -printf "sudo -u ${DB_MAP[$cc]} wp @ae post update %fABC /tmp/content/%f\n" | sed 's/\.txtABC//' | ssh $cc tee /tmp/cmd.sh | wc -l;
	ssh $cc wp @ae db cli --skip-column-names --batch --raw<<<"SELECT CONCAT(ID, '\t', post_modified, '\t', post_modified_gmt) FROM wp_posts WHERE ID in ($ids);" \
	    | awk -F $'\t' '{printf ("UPDATE wp_posts SET post_modified=\"%s\", post_modified_gmt=\"%s\" WHERE ID = %s;\n", $2, $3, $1)}' \
	    | ssh $cc tee '~/preserved-update-time.sh' | wc -l
    done
    # sh /tmp/cmd.sh
    # sudo mysql -vv animalequalityorguk < ~/preserved-update-time.sh
    # ansible -m shell -a "sh /tmp/cmd.sh && sudo mysql -vv {{ ae_slug }} < ~/preserved-update-time.sh | grep Changed|sort |uniq -c" ae
}

check() {
    for y in after/${1:-*}; do
	cc=$(basename $y);
	echo "## $cc"
	read id file < <(find $y/ -type f -printf "%f %p\n"|head -1)
	diff -w $file <(ssh $cc wp @ae post get --field=post_content $id)
    done
}

cmd=$1 && shift
$cmd $@
