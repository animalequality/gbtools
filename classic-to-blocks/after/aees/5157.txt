<!-- wp:paragraph -->
<p>GUADALAJARA, JALISCO.- En las cenas navideñas de esta temporada aumentará el consumo de pavos importados, en su mayoría norteamericanos y chilenos, debido a que la producción mexicana de estas aves ha ido en descenso en los años recientes, por falta de un entorno económico que estimule su crianza. Así lo informó Marco Parson Parra, dirigente de la sección de productores de guajolote de la Unión Nacional de Avicultores (UNA).

Como muestra de la situación adversa de la pavicultura mexicana, citó que la producción de este año traerá consigo una caída de 24% con relación al año pasado. Detalló que la oferta nacional de estas aves será de cerca de 11 mil 500 toneladas, contra casi 15 mil que se produjeron en 2008.

La producción mexicana de pavos se concentra en Chihuahua y Yucatán. Desde hace tiempo, Jalisco no tiene avicultura especializada en guajolotes de forma comercial, sólo para autoconsumo.

Indicó que los bajos precios con los que se comercializa el pavo importado (95% proveniente de Estados Unidos y 5% de Chile), inhiben la producción mexicana, máxime que el consumo nacional no ha tenido una demanda sostenida  porque durante el año pasado se tuvo un serio incremento del precio de los granos forrajeros, lo que no hizo costeable la crianza de estos animales.  

El dirigente de los "productores" de pavo mencionó que la oferta de estos animales en la presente temporada supondrá de un poco más de dos millones 800 mil ejemplares, y sólo poco más de un millón 428 mil serán mexicanos; 500 mil pavos importados rezagados; y 900 mil ejemplares importados para la presente época.

Citó que el precio del pavo supone 54 pesos el kilo de animal natural, y 64 pesos del ahumado.

Consumo de pavo por habitante

Estados Unidos     8.3 kg
Canadá                 4.7 kg
Chile                     4 .3 kg
Unión Europea      3.3 kg
México                      2 kg
Brasil                     1.5 kg

Producción mundial: 5.8 millones de toneladas.

Fuente: informador.com.mx</p>
<!-- /wp:paragraph -->