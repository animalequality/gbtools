<!-- wp:paragraph -->
<p>Igualdad Animal asistirá con una mesa informativa a FestiATTAC, un festival que contará con la participación de diversos grupos, asociaciones y ONGs con carácter social y cultural. El objetivo de este evento es dar a conocer el trabajo de los distintos colectivos de una manera libre y desinteresada, e invitar de manera gratuita a la ciudad a disfrutar, durante una jornada, de conciertos, talleres artísticos, actuaciones, etc., y a relacionarse con grupos involucrados en buscar un cambio en nuestra sociedad.

Dispondremos de una mesa informativa en la que repartiremos folletos, documentales y diversa información sobre los derechos de todos los animales. ¡Ven a conocernos!


Lugar:
Parc de l´Espanya Industrial
Metro L1 (Plaça de Sants - Hostafrancs), L3 (Sants Estació - Tarragona)

Para participar, escríbenos:
<a href="mailto:AngelaM@igualdadanimal.org">AngelaM@igualdadanimal.org</a>
Tel - 655051755

<a href="mailto:guillermoc@igualdadanimal.org">GuillermoC@igualdadanimal.org</a>
Tel - 629558296</p>
<!-- /wp:paragraph -->