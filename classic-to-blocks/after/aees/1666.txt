<!-- wp:paragraph -->
<p>Temuco, Chile- Las autoridades del municipio de Temuco, cerraron una “feria” en la que se traficaba con animales no humanos. El motivo no ha sido que los animales allí recluidos viesen sus intereses perjudicados, sino que la “feria” era clandestina.

La fiscal jefe de Nueva Imperial, Claudia Turra Lagos, informó que el proceso tiene por objetivo impedir el comercio clandestino que no ha sido regulado por institución pública alguna y que produce efectos negativos tanto tributarios como fitosanitarios, además de permitir el ocultamiento o venta clandestina de animales que pudieren haber sido sustraído a sus “dueños”.

Como siempre sucede en estos casos, los intereses de los animales no humanos no cuentan para nada ¿Y cómo iban a contar si se les considera propiedades que se pueden vender en una mal llamada feria?</p>
<!-- /wp:paragraph -->