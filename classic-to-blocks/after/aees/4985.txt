<!-- wp:paragraph -->
<p>La organización Mercy for Animals (Misericordia para los Animales) publicó el martes pasado un terrorífico vídeo obtenido con cámara oculta sobre la el sufrimiento producido por la industria del huevo.

En él se muestra, entre otras cosas, la realidad de la que son víctimas los pollos macho de dicha industria, dado que no ponen huevos ni engordan lo suficiente para ser económicamente rentables -los pollos criados para carne son de la raza broiler que engordan rápidamente-, los pollos recién nacidos son enviados directamente a una trituradora que los machaca vivos. Alrededor de 150.000 pollitos cada día, son matados de esta manera en este centro de Hy-Line en Spencer, Iowa, Estados Unidos, donde el grupo Mercy for Animals realizó la investigación. De esta manera mueren más de 30 millones de pollos al año.

En el mismo video pueden verse imágenes espeluznantes de como se corta el pico a otros pollos recién nacidos que se convertirán en gallinas ponedoras, de esta forma se evitan ataques e incluso canibalismo producido por el stress de vivir encarcelados en las granjas, también se muestra como los animales son separados por sexadores que determinan que animales deben ser inmediatamente triturados, los machos, y cuales deben ser mutilados para servir a la industria del huevo.

Según ha justificado la propia compañía, se trata de un método conocido como "eutanasia instantánea", esta no es una práctica extraña ni poco habitual, sino más bien inherente a toda la industria de explotación de animales por sus huevos. No debemos olvidar que por cada gallina que está hoy día siendo utilizada para poner huevos, ha habido otro pollito macho (su hermano) que acabó como si fuese basura en una trituradora o un contenedor. 


Video de la investigación: 
http://www.youtube.com/watch?v=JJ--faib7to

Más información sobre la explotación de gallinas y pollos por la industria del huevo:
http://www.granjasdeesclavos.com/gallinas/explotacion</p>
<!-- /wp:paragraph -->