<!-- wp:image {"id":11596} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_114172579.jpg" alt="" class="wp-image-11596"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>¿Te has animado a sustituir la carne en tu dieta pero no estás seguro sobre cómo hacerlo? Tenemos dos estupendas noticias para ti: los alimentos de origen animal no son las únicas fuentes de proteínas de calidad <strong>y, aún mejor, no tendrás que renunciar a tus platos favoritos porque ¡prácticamente cualquier receta puede hacerse en su versión vegetariana!</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Aprovecha al máximo estas 5 alternativas vegetales que siempre han estado presentes y que tienen tantas proteínas que no extrañaras para nada la carne.<strong>1.Frijoles</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11356} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/shutterstock_281102513.jpg" alt="" class="wp-image-11356"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Repletos de fibra, proteínas, magnesio y bajos en colesterol, esta legumbre es un sustituto excepcional en tus sopas, ensaladas, hamburguesas y salsas. Su aporte proteico es de 7.5 gramos por cada media taza.<strong>2. Seitán</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11598} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_305618039_0.jpg" alt="" class="wp-image-11598"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Te aporta 20 gramos de proteína por cada media taza. Se hace a partir del gluten y <strong>tiene una textura muy parecida a la de la carne</strong> que le permite absorber fácilmente los sabores al cocinar. <strong>En internet encontrarás infinidad de recetas</strong>, ¡anímate a probarlas!<strong>3. Lentejas</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11599} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_237565954.jpg" alt="" class="wp-image-11599"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tienen un gran contenido de folato, hierro, potasio y antioxidantes y <strong>9 gramos de proteínas por cada media taza</strong>. Úsalas en ensaladas, sopas, hervidos, hamburguesas vegetales y dips.<strong>4. Tempeh</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11600} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_290101184.jpg" alt="" class="wp-image-11600"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Puedes prepararlo revuelto, al vapor, al horno o asado. Está hecho a base de soya fermentada y <strong>es muy versátil porque al cocinar absorbe perfectamente los sabores de sus acompañantes</strong>. Te aporta 11 gramos de proteína en cada media taza.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>5. Tofu</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11601} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_286815149.jpg" alt="" class="wp-image-11601"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por ser tan versátil <strong>es tal vez la más famosa de las fuentes de proteínas vegetales (7 gramos por cada media taza)</strong>. Puedes prepararlo al horno, asado en parrilla, frito, al vapor, salteado o comerlo crudo. Usa ingredientes saborizantes al cocinarlo, tales como cebollas, ajo, salsas y curry. <strong>Es una excelente fuente de proteína y calcio que no debe faltar en tu cocina.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Si quieres saber cómo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación. Además, <a href="https://es.loveveg.com/" target="_blank">aquí puedes descargar gratis nuestro nuevo eBook con deliciosas recetas saludables y respetuosas con los animales</a>.</p>
<!-- /wp:paragraph -->