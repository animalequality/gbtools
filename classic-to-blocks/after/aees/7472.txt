<!-- wp:image {"id":10628} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/05/burg.jpg" alt="" class="wp-image-10628"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10627" src="/app/uploads/2016/05/indian_curry_box.jpg" style="float:left; margin:10px; width:300px">Eric Schmidt, presidente ejecutivo de la empresa matriz de Google, Alphabet, <strong>es uno de los expertos en tendencias tecnológicas más importantes del planeta</strong>. Schmidt dedica su carrera a predecir cómo distintas nuevas tecnologías pueden cambiar el mundo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El pasado lunes en Los Ángeles, en un salón abarrotado de inversores con el potencial de financiar a empresas emprendedoras en distintas tecnologías, Schmidt realizó una predicción que es <a href="https://igualdadanimal.org/noticia/2016/02/27/un-estudio-de-mercado-revela-que-los-productos-alternativos-la-carne-experimentaran-un/">un secreto a voces</a>: <strong>la carne proveniente de animales será reemplazada por alternativas cárnicas desarrolladas con vegetales</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Eric Schmidt prevé una revolución en la industria alimentaria. Nuevas tecnologías que ya están siendo desarrolladas por empresas emprendedoras que generan <a href="https://igualdadanimal.org/noticia/2016/03/23/las-profundas-consecuencias-de-un-modelo-alimentario-basado-en-vegetales/">un nuevo modelo alimentario</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El presidente ejecutivo de Alphabet sabe de lo que habla. Empresas como <a href="https://impossiblefoods.com/">Impossible Foods</a> o <a href="https://www.beyondmeat.com/en-US">Beyond Meat</a> están creciendo exponencialmente y llegando a los supermercados de Estados Unidos. En Europa tenemos el ejemplo de <a href="http://lacarniceravegetariana.com/nuestros-productos/">La Carnicera Vegetariana</a>, en proceso de crecimiento.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La actual producción de carne proveniente de animales conlleva un colosal derroche de recursos. Los métodos de la ganadería industrial <a href="https://igualdadanimal.org/noticia/2015/11/26/un-comite-de-expertos-avisa-los-gobiernos-comer-menos-carne-es-la-unica-manera-de/">han hecho saltar las alarmas</a> de gobiernos y entidades conservacionistas y de defensa de los animales. <strong>La ganadería no puede seguir alimentando a la población mundial</strong>. Es la responsable del peor maltrato animal conocido, además del mayor factor de calentamiento global y agotamiento del agua potable del planeta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cada vez más informes nos alertan de la inviabilidad de la producción de carne tal y como la conocemos. De la misma manera que a lo largo de la historia <strong>nuevas tecnologías</strong> sustituyeron a las que quedaban obsoletas (invención del motor como sustituto de la tracción animal, por ejemplo), <strong>las nuevas tecnologías alimentarias</strong> vienen para sustituir un modelo agotado.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Eric Schmidt lo sabe bien: la carne proveniente de animales tiene los días contados</strong>.</p>
<!-- /wp:paragraph -->