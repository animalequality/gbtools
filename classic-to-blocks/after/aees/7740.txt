<!-- wp:image {"id":11842} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/shutterstock_472100659_0.jpg" alt="" class="wp-image-11842"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Las prácticas de la ganadería industrial conllevan el abuso hacia animales sensibles que viven en lugares de difícil acceso para cualquier ciudadano. Difícil denunciar cualquier maltrato en estas circunstancias. Pero, ¿están protegidos estos animales en el código penal?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La sensibilidad hacia el trato que se da a los animales de granja <strong>ha experimentando un importante aumento a nivel social</strong>. Sin embargo cerdos, terneros, gallinas o pollos se encuentran en desventaja a nivel legal ante los animales con los que convivimos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>El Código Penal experimentó su última reforma por Ley Orgánica en 2015. El artículo 337, que incluye las penas por maltrato animal, también fue modificado. Si bien la reforma amplió los animales objeto de protección legal y pasaba a incluir a cualquier animal «que temporal o permanentemente vive bajo control humano», la práctica contradice a la teoría.</p>

<figure><img alt="" class="wp-image-11843" src="/app/uploads/2017/03/hombre-encima-del-cerdo.gif" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>Según Rosario Monter, abogada especializada en Derecho Animal «no es infrecuente encontrar casos de maltrato en explotaciones ganaderas». La reforma del artículo 337, <a href="http://www.abogacia.es/2016/07/29/el-delito-de-maltrato-animal-tras-la-reforma-del-codigo-penal-por-lo-12015-art-337-del-codigo-penal/" target="_blank">para ella</a>, «es un gran avance para la protección penal de los animales, pero sigue siendo insuficiente, pues se debería haber aumentado la pena de prisión de hasta 3 años para los supuestos más graves, como en países de nuestro entorno, para que los autores de estos hechos cumplan la condena en la cárcel.»</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2014/05/21/igualdad-animal-denuncia-70-granjas-de-conejos/" target="_blank">Igualdad Animal denuncia a 70 granjas de conejos</a></strong>

<p>&nbsp;</p>

<p>&nbsp;</p>

<font size="1"><font color="#808080"><p>Caso El Escobar, uno de los más crueles casos en España.</p></font>

<p>&nbsp;</p>

<p>A día de hoy, la pena máxima por maltrato animal es de 18 meses y solo si hay muerte del animal. Lo que hace que, en la práctica, <strong>incluso los peores casos de maltrato queden sin pena de cárcel</strong>. De hecho solo ha habido dos casos con ingreso efectivo en prisión.</p>

<p>Incluso en casos tan crueles como el de los trabajadores de una granja que se grabaron <a href="https://www.lavanguardia.com/vida/natural/20160120/301545197985/lechones-muertos-whatsapp.html" target="_blank">saltando sobre crías de cerdo matando a 72 de ellas</a> no ha habido ingreso en prisión. Tampoco la ha habido (aunque el caso sigue abierto) para los trabajadores de la granja El Escobar, en Murcia, <a href="https://www.elmundo.es/sociedad/2016/09/27/57e97f01268e3e66478b4592.html" target="_blank">que mataban a cerdas embarazadas con espadas</a> para sacarles las crías.</p>

<p>Y solo hablamos de los casos que llegan a ver la luz. Las prácticas comunes de la ganadería industrial <strong>incluyen flagrantes abusos</strong>. Desde confinar a las cerdas gestantes en jaulas tan pequeñas que ni siquiera pueden darse la vuelta hasta inseminar artificialmente a las vacas tras cada nuevo parto para que sigan dando leche de forma ininterrumpida.</p>

<p>Todo este maltrato, al final, solo es regulado por tibios «manuales de buenas prácticas» que son editados o bien por el propio sector ganadero o por el Ministerio de Agricultura. Dichos manuales tienen un enfoque meramente informativo y siempre con el foco en maximizar la producción, pero poco o nada hacen por el bienestar de los animales.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/03/14/europa-mas-cerca-de-prohibir-las-jaulas-en-la-cria-de-conejos/" target="_blank">Europa, más cerca de prohibir las jaulas en la cría de conejos</a></strong>

<p>&nbsp;</p>

<p>Para Roberto Mazorriaga, miembro de ABADA (Asociación Balear de Abogados por los Derechos de los Animales), &nbsp;«los animales a día de hoy y según el Código Civil &nbsp;español (no es así en el derecho civil catalán) son formalmente cosas y no seres sintientes dignos de protección en sí mismos.»</p>

<p>La situación legal contrasta con la opinión ciudadana. Según el último Eurobarómetro, <strong>el 94% de los españoles</strong> quiere más protección para los animales de granja.</p>

<p>Es de esperar que la legislación recoja el sentir ciudadano, para pasar a un marco legal en el que maltratar a los animales de granja deje de quedar impune.</p></font></font>
<!-- /wp:html -->