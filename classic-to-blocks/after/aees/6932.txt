<!-- wp:image {"id":9923} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/02/letonia_granjas.jpg" alt="" class="wp-image-9923"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Una nueva investigación encubierta realizada por la organización <a href="https://www.dzivniekubriviba.lv/" target="_blank">Dzīvnieku Brīvība </a>(Libertad Animal) durante el otoño de 2012 acaba de ser publicada y muestra el interior de seis granjas peleteras en Letonia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En todas las granjas visitadas, los activistas documentaron la presencia de animales <strong>animales enfermos, heridos y muertos </strong>dentro de diminutas jaulas, donde desarrollan estrés crónico debido a la la falta de estímulos y la incapacidad para expresar su comportamiento natural, además de padecer todo tipo de lesiones físicas.<br>
	<br>
	<strong>Vídeo de la investigación:</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="326" src="https://www.youtube.com/embed/ofCKeKOklBQ" width="580"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«Es horrible. Las imágenes me provocan un profundo disgusto y un shock general. De acuerdo con este material, es evidente que los animales se mantienen en condiciones inaceptables. Por otra parte, hay muchos <strong>animales que sufren de enfermedades crónicas</strong>, posiblemente plagas carnívoras. Así que parece que no hay <strong>nadie allí que brinde atención médica</strong> regular ni atención de ningún tipo. Entonces queda claro por qué hay tantos cadáveres en las jaulas de la granja»</em>, comenta el veterinario Normunds Fedorovs tras visualizar el material de archivo.</p>
<!-- /wp:paragraph -->