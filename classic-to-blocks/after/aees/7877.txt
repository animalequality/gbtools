<!-- wp:image {"id":12340} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/shutterstock_693458419.jpg" alt="" class="wp-image-12340"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>¡Eres increíble!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sabemos que lees estas líneas porque te importan los animales. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ya sea que seas vegano o que hayas decidido comenzar a reducir tu consumo de carne, queremos que sepas que <strong>en México la comida vegana es fácil de encontrar en las calles y de preparar en casa.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Con esta lista que te hemos preparado, no te perderás nada de lo que nuestro país ofrece para quienes han decidido comer por un mundo más compasivo, sostenible y saludable.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>1. Puedes encontrar deliciosas alternativas a la carne y lácteos en supermercados y tiendas online</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diversos supermercados y ecotiendas en México siguen incorporando más productos vegetales en sus estanterías. Y si quieres comprar online, <a href="https://www.veganlabel.mx/" target="_blank">Vegan Label</a>, <a href="https://www.mrtofu.com.mx/" target="_blank">Mr. Tofu</a>, <a href="https://www.eljardinvegano.com/retail/" target="_blank">El jardín Vegano</a> y <a href="https://tierravegana.com/" target="_blank">Tierra Vegana</a> son algunas de las tiendas que te ofrecen productos tanto importados como hechos en México.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12341} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/deshebrada_carne_soi-yah.jpg" alt="" class="wp-image-12341"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><font color="#808080"><p>Carne deshebrada para tacos, tortas, enchiladas o chilaquiles de la marca «Soi-Yah!».</p></font>

<p>&nbsp;</p>

<strong>2. México es el país con más vegetarianos en Latinoamérica</strong>

<p>¡Así como lo escuchas!</p>

<p>Y, mejorando lo anterior, a diferencia de otros países de Latinoamérica, es en México donde más personas han declarado seguir esta dieta por convicciones personales.</p>

<p>Según una encuesta del Gabinete de Comunicación Estratégica, <strong>un 36% de los mexicanos que dejaron de comer carne lo hicieron por respeto a los animales.</strong></p>

<p>¡A que te hemos dejado súper motivado!! :)</p>

<p><iframe allowfullscreen="" class="giphy-embed" frameborder="0" height="266" src="https://giphy.com/embed/lqruErDlkrWW4" width="480"></iframe></p>

<font size="1"><font color="#808080"><p><a href="https://giphy.com/gifs/chicken-lqruErDlkrWW4" target="_blank">via GIPHY</a></p></font>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p><a href="https://descubrirlacomida.com/" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a>, recibe los mejores tips sobre alimentación vegetariana y descarga un recetario vegetariano gratis.</p></font>

<p>&nbsp;</p>

<strong>3. Sólo en la Ciudad de México hay al menos 20 restaurantes vegetarianos y veganos</strong>

<p>¡Por supuesto que tienes que saber dónde puedes probar deliciosos platos vegetarianos fuera de casa!</p>

<p>Por eso, entre los muchos que hay, hemos seleccionado para ti <a href="https://igualdadanimal.org/noticia/2017/04/05/10-excelentes-restaurantes-con-opciones-100-vegetales-en-ciudad-de-mexico/" target="_blank">10 excelentes restaurantes que ofrecen opciones vegetales y que se encuentran en Ciudad de México</a>. Así que ¡sal a probar!</p>

<figure><img alt="" class="wp-image-12342" src="/app/uploads/2017/08/vegan_inc_mexico_restaurant.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<font size="1"><font color="#808080"><p>En Vegan Inc. el lema es: &nbsp;«hacemos que lo saludable tenga un sabor delicioso».</p></font>

<p>&nbsp;</p>

<strong>4. En México tú puedes ser un Protector de Animales desde tu computadora o celular</strong>

<p>Realizando pequeñas acciones puedes lograr importantes victorias para los animales.</p>

<p>Envía correos, difunde información y, además, forma parte del equipo de Igualdad Animal para hacer frente a la empresas que más maltratan a los animales. Ingresa a <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank">Protectores Animales México</a> y cambia su destino. &nbsp;&nbsp;</p>

<p><iframe allowfullscreen="" class="giphy-embed" frameborder="0" height="270" src="https://giphy.com/embed/WlH9MCf0q0h1u" width="480"></iframe></p>

<p><a href="https://giphy.com/gifs/love-animal-amor-WlH9MCf0q0h1u" target="_blank">via GIPHY</a></p>


<strong>5. Hay miles de recetas vegetarianas de cocina mexicana en internet</strong>

<p>Nuestra cocina es una de las más populares del mundo. Gracias a ello en la web puedes encontrar una impresionante cantidad de recetas de platos mexicanos en sus versiones vegetarianas.</p>

<p>La creatividad de los chefs de <a href="https://www.youtube.com/channel/UC_XE7-_GigBlY_s4N1__Jsw" target="_blank">Comer Vegano</a>, <a href="https://www.youtube.com/channel/UCbn_IwWoaqYfy-RwYWlDF-A" target="_blank">Vida Vegana</a>, <a href="https://www.youtube.com/channel/UCrN5l3YrBp8CiYNv7_xf2AA" target="_blank">Cocina Vegana Fácil</a>, <a href="https://www.youtube.com/playlist?list=PLVBPGFauNs7MK3t_lfYWU05NOsVxaLf63" target="_blank">Vegan Booty</a> y <a href="https://danzadefogones.com/" target="_blank">Danza De Fogones</a> te impresionará.</p>

<figure><img alt="" class="wp-image-12343" src="/app/uploads/2017/08/quesadillas-veganas-6_1.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<font size="1"><font color="#808080"><p>Tacos Al Estilo Baja California de Comer Vegano.</p></font></font></font></font></font></font>
<!-- /wp:html -->