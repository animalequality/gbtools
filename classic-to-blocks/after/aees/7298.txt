<!-- wp:paragraph -->
<p>¿Qué descubriríamos en una película hacia atrás desde el momento en el que echamos a nuestra cesta de la compra el brik de leche? Si esto fuera posible nos encontraríamos con prácticas que pocos conocen. Prácticas comunes de la industria láctea. La producción de leche en las granjas industriales conlleva un terrible maltrato animal. Pasamos a enumerar algunas de dichas prácticas crueles:

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14087} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/lechenoticia1.jpg" alt="" class="wp-image-14087"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>Foto: Jo-Anne McArthur / Weanimals</em>

1.&nbsp;A partir del cuarto día de nacer y hasta el destete, a las terneras se les alimenta con lactoreemplazantes. Debido a que la leche artificial es más barata que la leche natural la utilización de lactorremplazantes está muy extendida en las explotaciones de vacas de leche. También se usa para alimentar a estos bebés toda la leche que por motivos de algunas enfermedades de las vacas adultas, como la mastitis, no es legalmente posible comercializar.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10297} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/lechenoticia2.jpg" alt="" class="wp-image-10297"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

2.&nbsp;Las terneras hembra recién nacidas son separadas de las madres y viven en pequeños boxes hasta los dos meses de edad donde son alimentadas artificialmente. Su destino será reemplazar a las vacas que ya no produzcan suficiente leche. Los terneros macho son vendidos a las 15-20 días de edad. Su principal destino será ser convertidos en carne de ternera siendo aún bebés.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14088} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/lechenoticia3.jpg" alt="" class="wp-image-14088"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>Foto: Jo-Anne McArthur / Weanimals</em>

3.&nbsp;Las vacas que producen leche son inseminadas artificialmente una y otra vez para que sigan produciendo leche. Todo el ciclo de embarazos está estudiado desde antes de que nazcan para que produzcan la mayor cantidad posible de leche. Su primer embarazo será a los dos años de edad.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14089} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/lechenoticia4.jpg" alt="" class="wp-image-14089"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>Foto: Karol Orzechowski</em>

4.&nbsp;Tras 60-90 días después de cada parto las vacas vuelven a ser inseminadas artificialmente. El objetivo es conseguir un parto al año. Este ritmo de partos y ordeños deja exhaustas a las vacas rápidamente.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14090} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/lechenoticia5.jpg" alt="" class="wp-image-14090"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>Foto: Karol Orzechowski</em>

5.&nbsp;El embarazo se suele realizar por inseminación artificial ya que se han eliminado los machos de las explotaciones. La inseminación artificial consiste en depositar en el aparato genital de las hembras el semen obtenido del macho por medios artificiales. a los 60 días se confirma la gestación a través de palpación rectal. Si no quedan embarazadas vuelven a ser inseminadas artificialmente.

6.&nbsp;Para comprobar que la vaca ha quedado embarazada se usa el método de la palpación rectal. Se introduce la totalidad del brazo en el recto de las vacas para hallar síntomas de embarazo.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14091} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/lechenoticia6.jpg" alt="" class="wp-image-14091"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>Foto: Jo-Anne McArthur / Weanimals</em>

7. Todas las vacas son “descornadas” para evitar que se produzcan heridas al vivir en condiciones estresantes para ellas. Los métodos con los que son descornadas dependen de a la edad a la que se les haga pasar por este traumático proceso. A los bebés se les aplica sosa caústica en la zona donde aparecerían los cuernos. Esto hace que nunca lleguen a crecer. A partir del mes de vida el método es utilizar un descornador eléctrico o un hilo de acero para arrancar los cuernos.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14092} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/lechenoticia7.jpg" alt="" class="wp-image-14092"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

8. Eliminación de las vacas que no rindan lo suficiente para los estándares de la industria láctea. El 25% de las vacas de una granja industrial son enviadas al matadero anualmente. Las causas para matarlas son:

— Bajas producciones.

— Edad.

— Infertilidad.

— Accidentes.

— Enfermedades cuyo coste de veterinaria exceda al de producción.

— Mamitis crónica.

Afortunadamente a día de hoy cada vez son más las leches de origen vegetal que podemos encontrar en los supermercados. Leches de alto valor nutricional que podemos echar a la cesta de nuestra compra evitando gran cantidad de maltrato animal. ¡Sustituir la leche de vaca por leche de almendras, de arroz, de soja o de avena es cada vez más fácil! ¿Te animas a probarlas?

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/82afx4sLeMk" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->