<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Si pudieras elegir nacer de nuevo, seguramente lo último que quisieras ser sería un pez. ¿Por qué? Porque los peces son los animales más maltratados y matados en todo el planeta. Aprovechando que no existe ninguna legislación que proteja a los peces y utilizando los métodos más crueles, <strong>la pesca industrial arrasa día tras día con poblaciones de peces</strong>.

Pertenecemos a medios tan diferentes que realmente sabemos poco de los peces y eso, en gran parte, también ha hecho que ignoremos su sufrimiento. Equivocadamente se les considera como animales sin inteligencia (famosa es la frase «memoria de pez») e incapaces de sentir dolor y todo esto contribuye a que muchas personas aún no consideren sacarlos de su plato.

A continuación, te presentamos 5 motivos por los cuales podrías dejar de apoyar a esta cruel industria que maltrata a los animales más olvidados del planeta.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13461,"align":"left"} -->
<figure class="wp-block-image alignleft"><img src="/app/uploads/2018/11/peces_victimas_mas_olvidadas_del_maltrato_animal_igualdadanimal.org_2.jpg" alt="" class="wp-image-13461"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>1.La agonía de los peces en la pesca</strong>

Al ser &nbsp;capturados en el mar, los peces sufren muertes terribles. Son perseguidos hasta el cansancio y, atrapados por las redes y sin ninguna posibilidad de escapar, son sacados violentamente del fondo del mar mientras sufren descompresión y sus órganos internos se revientan. ¿Imaginas cuán doloroso puede ser esto?

<strong>2. El número de peces que la industria mata cada año es alarmante</strong>

Se estima que cada año la pesca industrial mata a un número de peces equivalente a la población humana de 142 planetas tierras. Y tan solo en las piscifactorías (granjas de peces) 120 mil millones de peces son criados cada año.

<strong>3. Las piscifactorías son «el máximo exponente del desprecio hacia la vida animal»</strong>

La cría de peces para consumo conlleva algunas de las más crueles prácticas y formas de maltrato animal conocidas. En las piscifactorías los peces permanecen recluidos dentro de jaulas en el mar o ríos, y en tanques si se encuentran en la superficie. Las condiciones de vida son tan extremas que los peces sufren depresión severa y llegan a generar actitudes suicidas por la falta de estímulo y las condiciones de hacinamiento.

En las piscifactorías los peces son matados brutalmente. Alguno de los métodos utilizados son la asfixia, los golpes y la electrocución.

<strong>4. No existe ninguna ley que proteja a los peces</strong>

Al contrario de otros animales de granja, los peces se encuentran totalmente desprotegidos ante la ley. No existe ninguna normativa que regule el bienestar animal de los peces y que los proteja de la crueldad de la pesca industrial y de las piscifactorías. Prácticamente, pueden (y, de hecho, así ocurre) hacer lo que les da la gana con ellos.

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente a nuestro e-boletín!&nbsp;</a></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><strong>5. Las poblaciones marinas han disminuido casi un 50% desde 1970.</strong>

Y las de algunas especies hasta un 75%. El informe «Living Blue Planet» (Planeta Azul Vivo) de la organización internacional WWF muestra cómo «en una sola generación la actividad humana ha dañado gravemente los océanos capturando peces más rápido de lo que pueden reproducirse, mientras se destruyen sus zonas de alimentación. Se necesitan cambios profundos para garantizar una vida marina abundante».

Como consumidores podemos hacer una gran diferencia para los peces y por eso te animamos a que comiences a sustituir el pescado en tu alimentación. Son tantas las alternativas vegetales que existen, todas exquisitas y muy nutritivas y las puedes conocer en nuestras web <a href="https://es.loveveg.com/">Love Veg España </a>y <a href="https://loveveg.mx/">Love Veg México</a> donde encontrarás todo lo que necesitas para probar una alimentación vegetal.</p>
<!-- /wp:paragraph -->