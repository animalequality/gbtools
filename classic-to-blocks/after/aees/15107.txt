<!-- wp:image {"id":10410} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/consumo.jpg" alt="" class="wp-image-10410"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Estás haciendo la compra, llegas a la sección de patés y ves el foie gras. A unos metros tienes otra posibilidad: hummus, <strong>¿qué eliges?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sigues y llegas a la sección de leche. Entre las distintas marcas de leche de vaca, de pronto te encuentras una sección con distintos tipos <strong>de leche vegetal: arroz, avena, almendra, soja;</strong> ¿qué eliges?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Al lado de la sección de carne, tienes la sección de verduras y frutas <strong>con su característica explosión de colores y formas</strong>; y más allá la sección de legumbres; ¿qué eliges?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Son preguntas que a veces no nos hacemos pero… <strong>¿y si esos momentos fueran los que más maltrato animal pudiesen evitar?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porque al igual que tú, <strong>a nosotros nos gusta ver a los animales así</strong>:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13794} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaconsumo1.jpg" alt="" class="wp-image-13794"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Y no así:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13795} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaconsumo2.jpg" alt="" class="wp-image-13795"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><sub><em>Patos de la industria del foie gras</em></sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿A quién no le gusta ver a los animales así?:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13796} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaconsumo3.jpg" alt="" class="wp-image-13796"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Y a quién no le disgusta verlos así?:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13797} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaconsumo4.jpg" alt="" class="wp-image-13797"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><sub><em>Vaca de la industria láctea.</em></sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El momento de hacer la compra en el supermercado es decisivo para los animales de granja. Como consumidores, <strong>tenemos la posibilidad de mandar poderosos mensajes</strong> a las empresas de alimentación.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ya hay <strong>millones de personas</strong> sustituyendo la carne en su alimentación alrededor del mundo. Las empresas lo saben y como consecuencia, cada vez hay más productos <strong>para esta &nbsp;demanda creciente</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Además, los supermercados <strong>ya están llenos de posibilidades</strong> como legumbres, cereales, verduras, frutas… Un mundo de deliciosos sabores y texturas tan saludable y delicioso que está convenciendo cada vez a más gente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Te sumas? Para ayudarte, te proponemos que visites la página website <a href="https://www.gastronomiavegana.org/">Gastronomía Vegana</a>, donde <strong>podrás obtener la información para dar tus primeros pasos</strong>. ¡Además, en la website <a href="https://danzadefogones.com/">Danza de Fogones</a> tienes <strong>deliciosas recetas</strong> que te entrarán por los ojos!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porque… ¿a quién no le gusta ver a nuestros amigos los animales así?:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13798} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaconsumo5.jpg" alt="" class="wp-image-13798"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Y quién no siente tristeza viéndolos así?:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13799} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaconsumo6.jpg" alt="" class="wp-image-13799"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><sub><em>Pollitos de la industria de la carne.</em></sub></p>
<!-- /wp:paragraph -->