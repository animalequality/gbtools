<!-- wp:image {"id":9493} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/rata_caricia.jpg" alt="" class="wp-image-9493"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La empatía y la solidaridad han sido, en cuanto a la ciencia se refiere, patrimonio exclusivamente humano durante siglos. Después, muchos estudios mostraron que los primates también se ayudan unos a otros y saben entender sus <strong>emociones</strong>, especialmente cuando son de dolor. Este reciente experimento ha encontrado comportamientos solidarios entre roedores que implican que la aparición de la <strong>empatía</strong> en el árbol de la evolución es mucho más antigua de lo que se pensaba.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El trabajo, publicado en Science, se basa en experimentos* con ratas. Una de ellas estaba <strong>cautiva en un tubo</strong> y la otra estaba "libre" cerca de ella. Un mecanismo en el exterior permitía liberar a la rata encerrada.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los expertos comprobaron que las ratas que estaban fuera del tubo se mostraban mucho más agitadas cuando había otro animal cautivo dentro del tubo que cuando no lo había, algo que interpretan como signo de "<strong>contagio emocional</strong>", una forma básica de empatía que se ha observado en primates y humanos.<img alt="" class="wp-image-9491" src="/app/uploads/2012/04/ratas_empatia.jpg" style="width: 220px; float: right;"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las ratas "libres" tardaron días en percatarse del funcionamiento del resorte liberador, pero, cuando lo hicieron, abrieron la jaula una vez tras otra para <strong>desenjaular a sus congéneres</strong>. Los autores del trabajo señalan que esto implica un comportamiento de ayuda motivado por un nivel de <strong>empatía más complejo</strong> y avanzado que nunca se ha observado en roedores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los expertos <strong>confirmaron </strong>sus hipótesis ya que, cuando ponían una rata de peluche en la jaula, el animal libre lo ignoraba y no accionaba el resorte. Para averiguar qué empujaba a las ratas a este comportamiento, los expertos pusieron otro resorte que dispensaba chocolate. Para su sorpresa, las ratas también <strong>liberaron </strong>a los roedores cautivos antes de accionar la palanca de la comida.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>"No había otra razón para actuar salvo acabar con la inquietud de las ratas cautivas"</strong>, explica Inbal Ben-Ami Bartal, psicólogo de la Universidad de Chicago y coautor del trabajo. "En roedores, ver a una rata hacer algo una y otra vez significa que esa acción le supone una recompensa", concluye el investigador.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><small>(*) En Igualdad Animal nos oponemos a la experimentación en animales independientemente del resultado de los estudios realizados.</small></p>
<!-- /wp:paragraph -->