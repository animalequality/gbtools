<!-- wp:paragraph -->
<p>Como desde hace tres años Igualdad Animal realizará varias acciones con motivo del Día Internacional del Veganismo, que se celebra el próximo Domingo 1 de Noviembre, con el objetivo de dar el estilo de vida basado en el respeto a los animales.
<b>TALLER DE FORMACIÓN DE ACTIVISTAS - BARCELONA</b>

Sábado 31 Octubre • 11 h.
Gran de Gracia 190-192 - Barcelona (Metro-Fontana)

Investigaciones, rescates abiertos de animales, protestas llamativas, saltos al ruedo, descuelgues… Igualdad Animal es una organización de derechos animales que destaca por su activismo comprometido, mediático y serio, pero detrás de cada acción hay muchas horas de preparación y sobre todo una base ideológica fuerte que nos lleva a trabajar como lo hacemos. Si quieres conocer nuestra linea de trabajo, como lo llevamos a cabo y sobre todo como puedes tú trabajar con nosotros, el próximo Sábado 31 de Octubre a partir de las 11 h. realizaremos un taller de formación sobre activismo en el Casal d'Associacions Juvenils de Barcelona. ¡Esperamos conocerte ahí!

El taller es de carácter abierto y participativo por lo que te animamos a plantear cualquier duda que puedas tener ya sea sobre el trabajo y planteamiento de Igualdad Animal como sobre la defensa de los animales en general.

Si compartes el deseo de ayudar a los animales y crees que juntos podemos aprender a defenderles mejor, ven y participa en el taller aportando tu experiencia.

Acceso libre y gratuito.

Teléfono: 655 432 914
Teléfono: 639 587 936
Correo electrónico: info@igualdadanimal.org
                                    
Lugar: Sala de Juntas
Casal d'Associacions Juvenils de Barcelona
c. Gran de Gràcia, 190-192 
Acceso en Metro: Fontana


<b>PROTESTA POR EL DIA INTERNACIONAL DEL VEGANISMO - BARCELONA</b>

Domingo 1 de Noviembre • 12 h.
Plaza Nova - La Catedral - Barcelona

Con motivo del Día Internacional del Veganismo, que se celebra el 1 de Noviembre, Igualdad Animal convoca un llamativo acto-protesta en el centro de Barcelona. Varios activistas se empaquetarán en bandejas de plástico para dar a conocer el sufrimiento de los animales víctimas de la industria alimenticia. ¡Participa!

Teléfono: 609 980 196
Teléfono: 639 587 936
Correo electrónico: <a href="mailto:info@igualdadanimal.org">info@igualdadanimal.org</a>
Acceso en Metro: Plaza Catalunya / Urquinaona / Jaume I


<b>SALUDA A UN VEGANO - SEVILLA</b>

Domingo 1 de Noviembre • 11 h.
Calle Tetuan - Sevilla

A partir de las 11:00 horas en la Calle Tetuán varios activistas de  Igualdad Animal repartirán folletos informativos en los que se explica  qué significa ser vegano. Acercar un mensaje de respeto a los animales a la sociedad supone necesariamente dar visibilidad al termino veganismo y esta acción es una forma de mostrar que el respeto a los animales es una forma de vida adoptada cada vez por más personas que cambian su relación con los demás animales oponiéndose a su explotación de manera práctica cada día.

¿Cómo puedes ayudarnos?

Si eres vegano y vives en Sevilla ayúdanos en esta acción simplemente repartiendo el folleto informativo y explicándole a los interesados qué es el veganismo y lo importante que es para la vida de millones de animales su difusión.

¿Aún necesitas más información sobre veganismo?

No te pierdas esta oportunidad y pásate el día 1 de Noviembre por la calle Tetuán, pregúntanos todas tus dudas y recoge toda la información que necesites en nuestro stand informativo.

Teléfono: 608975085
Correo electrónico: conchip@igualdadanimal.org</p>
<!-- /wp:paragraph -->