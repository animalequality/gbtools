<!-- wp:image {"id":12090} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/foto_fb_28.jpg" alt="" class="wp-image-12090"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Para los animales que viven dentro de las granjas industriales <strong>no hay posibilidad alguna de escapa</strong>r al destino que como propiedades de la industria ésta ha fijado para ellos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Están allí para satisfacer una demanda. Son considerados mercancías y por eso no existe ningún respeto y consideración hacia ellos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pero sabemos que sufren. Sabemos que aún cuando no han conocido la libertad, instintivamente la anhelan con todo su ser.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pocos animales tienen la fortuna de escapar de esta maquinaria de horror y crueldad. Ellos se convierten en los embajadores de los millones de voces que día tras día son silenciados para siempre por la industria.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lucía y Celeste son dos de ellos. Estaban destinadas a vivir, por al menos dos años, dentro de una jaula compartida con otras 6 u 8 gallinas y <strong>con un espacio destinado para cada una tan pequeño como una hoja de papel. &nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/en-mexico-tambien-podemos-acabar-con-la-crueldad-del-sistema-de-jaulas-en-la/" target="_blank">En México también podemos acabar con la crueldad del sistema de jaulas en la producción de huevos</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se les forzaría a producir alrededor de 300 huevos al año en lugar de los 30 que en libertad pondrían [1]. Cuando su producción decayera, es decir, cuando fueran inservibles para la industria, serían llevadas al matadero.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No existe nunca dentro de la cruel industria del huevo consideración alguna por <a href="https://igualdadanimal.mx/blog/no-lo-sabias-pero-las-gallinas-son-en-realidad-como-unos-gatos-o-perros-emplumados-muy/" target="_blank">estos sensibles e inteligentes animales</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A Lucía y Celeste también les esperaba tener que soportar todo este horror, pero la hija menor de los dueños de la granja las rescató para cambiar su vida para siempre.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12091} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/habitantes_grande_lucia.jpg" alt="" class="wp-image-12091"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><font color="#808080"><p>Lucía</p></font>

<p>Ambas viven ahora en el Santuario Igualdad Interespecie. Algo tan natural como pisar la tierra o estirar las alas era imposible para ellas antes de llegar a este, su nuevo hogar.</p>

<p>&nbsp;</p>

<p><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/9zvyVJ0lVcI" width="854"></iframe></p>

<font size="1"><font color="#808080"><p>Celeste tomando su baño de tierra.</p></font>

<p>&nbsp;</p>

<p>Lucía y Celeste no volverán a ser maltratadas y se les respetará hasta el final de sus días pero <strong>al menos 190 millones de gallinas en México pasan todas sus vidas encerradas en minúsculas jaulas de alambre</strong> sin poder siquiera estirar sus alas.</p>

<p><a href="https://lavidaenunajaula.igualdadanimal.mx/" target="_blank">Por favor firma aquí para poner fin a las jaulas en batería</a>.</p>

<p>También puedes hacer mucho más para ayudar a las gallinas si sustituyes los huevos en tu alimentación por deliciosas alternativas vegetales. &nbsp;Es muy fácil hacerlo. <a href="http://www.igualdadanimal.org/noticias/7713/6-recetas-para-ayudarte-sustituir-los-huevos-en-tu-alimentacion" target="_blank">¡Aquí te explicamos cómo!</a></p>

<p>&nbsp;</p>

<p>[1] Romanov MN, Weigend S. <a href="https://core.ac.uk/download/pdf/18531515.pdf" target="_blank">Analysis of genetic relationships between various populations of domestic and jungle fowl using microsatellite markers</a>.</p></font></font>
<!-- /wp:html -->