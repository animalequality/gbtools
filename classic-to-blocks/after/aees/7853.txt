<!-- wp:image {"id":12262} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/07/shutterstock_209461318.jpg" alt="" class="wp-image-12262"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Por primera vez en una década <strong>la Guía Alimentaria de Canadá hace énfasis en los alimentos vegetales</strong> y minimiza la proporción de carne y productos lácteos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Esto podría provocar cambios en lo que los niños comen en las escuelas y guarderías</strong>, lo que aprenden acerca de los alimentos y, en última instancia, cómo almacenan sus estanterías.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12263} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/07/shutterstock_211422370.jpg" alt="" class="wp-image-12263"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Hasta ahora los principios del nuevo proyecto me parecen excelentes», dijo Cara Rosenbloom, una experta dietista. «Hay mucho más énfasis en los alimentos elaborados con vegetales y menos en comer alimentos procesados».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/blog/imagina-un-modelo-alimentario-que-salve-el-mundo-y-no-nos-haga-renunciar-nada/" target="_blank">Imagina un modelo alimentario que salve el mundo y no nos haga renunciar a nada</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Una de las cosas que destaca particularmente para Rosenbloom es que <strong>en la guía han sido agrupados todos los alimentos que son ricos en proteínas en un mismo grupo</strong>. En este sentido, afirma: «Así que en lugar de un grupo de carne y alternativas y un grupo de leche y alternativas, los vemos todos agrupados como proteínas».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5">&nbsp;</font>

Actualmente, tiendas y supermercados en Canadá ofrecen alternativas basadas en leches enriquecidas con calcio y vitamina D, muchas de las cuales son de soja, almendras y coco. &nbsp;

El proceso de consulta pública para la revisión de la guía inició el pasado otoño, se cerrará el 25 de julio y sus resultados se publicarán el próximo año. Rosenbloom predice que las nuevas directrices tendrán un efecto directo en las comidas servidas en entornos institucionales, incluyendo hospitales, centros de atención y escuelas.

</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
Fuente: <a href="https://toronto.citynews.ca/2017/07/17/first-canada-food-guide-overhaul-decade-sees-shift-away-meat-dairy/" target="_blank">https://toronto.citynews.ca/2017/07/17/first-canada-food-guide-overhaul-decade-sees-shift-away-meat-dairy/</a></p>
<!-- /wp:paragraph -->