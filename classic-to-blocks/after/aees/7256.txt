<!-- wp:image {"id":10252} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/17122797405_d9a5ed5f41_m.jpg" alt="" class="wp-image-10252"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-14001" src="/app/uploads/2015/05/gallinasalemania.jpg" style="float:right; margin:5px; width:450px">La organización internacional Igualdad Animal <a href="https://www.youtube.com/watch?v=yE-zHYKT_3E">presenta una nueva investigación </a>en dos granjas de huevos ecológicas de Alemania que revelan imágenes impactantes del sufrimiento que padecen los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los investigadores de Igualdad Animal visitaron dos granjas al norte de Alemania en marzo de 2015. Ambas granjas albergaban hasta 30.000 gallinas ponedoras.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Una de las granjas suministra los huevos a ‘Deutsche Frühstücksei GmbH’, el mayor productor de Alemania y uno de los mayores de Europa. Suministra a los principales supermercados y cadenas al por menor.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>OVOBEST Eiprodukte GmbH &amp; Co. KG, una empresa filial propiedad de ‘Deutsche Frühstücksei GmbH’, es una de las productoras y distribuidoras de huevos más importantes tanto en Alemania como en el mercado internacional.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las imágenes tomadas por Igualdad Animal muestran:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
&nbsp;&nbsp; &nbsp;•&nbsp;&nbsp; &nbsp;Más de diez mil animales viviendo en el mismo recinto entre suciedad, polvo y heces.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;&nbsp; &nbsp;•&nbsp;&nbsp; &nbsp;Animales muertos abandonados entre otros que aún siguen con vida.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;&nbsp; &nbsp;•&nbsp;&nbsp; &nbsp;A la mayoría de las gallinas solo les quedan algunas plumas debido a que se picotean entre ellas y se las arrancan por el estrés que soportan.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;&nbsp; &nbsp;•&nbsp;&nbsp; &nbsp;La falta de higiene les ocasiona inflamaciones en las trompas de falopio y otras lesiones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/yE-zHYKT_3E" width="885px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
"<em>Las imágenes obtenidas por los investigadores de Igualdad Animal muestran las terribles condiciones en las que se encuentran estos animales. Una vez más, comprobamos cómo para estas industrias los animales son simple mercancía de la que obtener el máximo beneficio económico. Millones de personas en todo el mundo están optando ya por opciones vegetales en su alimentación en solidaridad con los animales y como rechazo a la crueldad de estas industrias. Consideramos por otro lado que la ciudadanía tiene derecho a conocer esta información para poder tomar decisiones conscientes sobre su consumo</em>.", afirmó Javier Moreno, coordinador internacional de Igualdad Animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/albums/72157649593341023"><img src="https://farm8.staticflickr.com/7636/17096895016_2c030caf3f_z.jpg" alt="Maltrato en granja del mayor productor de huevos de Alemania"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></p>
<!-- /wp:paragraph -->