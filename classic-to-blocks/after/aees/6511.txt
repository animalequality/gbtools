<!-- wp:image {"id":9527} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/05/santino_zoo.jpg" alt="" class="wp-image-9527"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Santino, un chimpancé macho recluido en el zoológico de Furuvik, en Suecia, diseña ataques cada vez más complejos contra los visitantes del centro.

Al principio, <strong>Santino era famoso por lanzar piedras y otros proyectiles a los visitantes que le molestaban</strong>. Pero ahora ha mejorado su técnica, innovando en una forma que requiere de un pensamiento planificado, basado en el engaño y la previsión de situaciones futuras.

«<em>Después de que un grupo de visitantes abandonara la zona del complejo, Santino fue al interior del recinto y<strong> trajo un buen montón de heno,</strong> que colocó cerca de la sección del visitante, e inmediatamente después <strong>guardó unas piedras debajo</strong>»,</em> explicó el investigador Mathias Osvath, director científico de la Estación de Investigación de Primates Furuvik, en la Universidad de Lund.

«<em>También colocó algunos proyectiles atrás. Después de esto, <strong>se sentó al lado de la paja y esperó</strong>. Cuando volvieron a llegar visitantes, él esperó a que estuvieran cerca y, sin ningún tipo de manifestación previa, se puso a lanzar piedras contra la multitud.»</em>

Según Osvath, <strong>calcular estos ataques por sorpresa a los visitantes demuestra un pensamiento muy avanzado</strong>, generalmente asociado en exclusiva a los seres humanos.

<em>«Lo más interesante es que Santino hizo estos preparativos cuando los visitantes se encontraban fuera, incorporando innovaciones en su comportamiento y reproduciendo mentalmente un resultado futuro.»</em>

Los investigadores creen que la recombinación de las experiencias anteriores, junto con la innovación <em>«es una buena señal de las capacidades de previsión y sofisticación del pensamiento en los chimpancés.»</em><em><img class="wp-image-14256" style="width: 534px;" src="/app/uploads/2012/05/santino_proyectiles.jpg" alt="Escondite de los proyectiles de Santino"></em><small>Situación de los proyectiles preparados por Santino</small></p>
<!-- /wp:paragraph -->