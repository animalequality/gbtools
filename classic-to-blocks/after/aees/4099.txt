<!-- wp:paragraph -->
<p>Igualdad Animal está presente también en Andalucía realizando numerosas actividades de las cuales tú puedes formar parte. Organizamos acciones, concentraciones, conferencias, talleres, mesas informativas, jornadas y todo tipo de actividades en defensa de los animales, no obstante nunca es suficiente y ellos necesitan desesperadamente tu ayuda. Desde Igualdad Animal queremos que los animales dejen de ser considerados recursos, queremos promover el respeto a los animales y en el menor tiempo posible, para lograrlo te necesitamos. 

Estamos organizando acciones y mesas informativas en Andalucía en las cuales puedes participar, también puedes ayudarnos difundiendo convocatorias, repartiendo folletos, pegando carteles, buscando lugares donde podamos ofrecer una conferencia o realizar una proyección, sugiriendo ideas etc. Únete al equipo de Igualdad Animal, ayúdanos a salvar animales. 

Ponte en contacto con las coordinadoras de Igualdad Animal en Andalucía, ¡Te esperamos!

<a href="mailto:conchip@igualdadanimal.org">conchip@igualdadanimal.org</a><a href="mailto:helenac@igualdadanimal.org">helenac@igualdadanimal.org</a>

Tel- 678 243 530

Por la abolición de la esclavitud animal.</p>
<!-- /wp:paragraph -->