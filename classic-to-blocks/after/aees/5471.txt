<!-- wp:paragraph -->
<p>
	Científicos de la Universidad de Montreal (Canadá) han creado una escala para clasificar la expresión facial de los ratones que permitirá discernir cuándo sienten dolor, según publicó hoy la revista "Nature Methods".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Este sistema permite saber el dolor que sufren los ratones utilizados en experimentos de laboratorio. <img align="left" alt="" height="154" hspace="9" class="wp-image-13602" src="/app/uploads/2010/05/11036_221439638344_116189118344_4170722_3142159_n.jpg" vspace="9" width="200">Para realizar el estudio, los investigadores analizaron cientos de imágenes de ratones antes y mientras los sometían a estímulos de dolor de intensidad moderada.<br>
	<br>
	De esta manera establecieron cinco rasgos de rigidez facial, hinchazón de nariz y mejillas, movimientos de las orejas y bigotes que cambian según la gravedad de los estímulos y que permiten crear una escala de gestos relacionándolos con la intensidad del dolor.<br>
	<br>
	Uno de los objetivos que se pretende alcanzar con este estudio es detectar en los postoperatorios con una simple inspección visual si la dosis de analgésicos administrada a los ratones es insuficiente.<br>
	Resulta paradójico preocuparse por el dolor de los ratones cuando se les va a seguir utilizando como objetos de laboratorio, se les va a seguir causando dolor y se les va a seguir matando por millones cada año. No era necesario realizar este estudio para saber que los ratones, así como todos los animales utilizados en los laboratorios, experimentan diferentes grados de dolor. Su sistema nervioso les hace percibir sensaciones desagradables como el dolor, el miedo o el estrés, pero también sensaciones agradables como el placer. Los animales, al igual que nosotros, tienen interés en seguir vivos y no quieren ser privados de libertad y sometidos a experimentos.<br>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Para más información sobre experimentación con animales pincha <a href="http://www.igualdadanimal.org/experimentacion">aquí.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Puedes ver imágenes de ratones en un laboratorio obtenidas por Igualdad Animal pinchando <a href="https://www.facebook.com/album.php?aid=170006&amp;id=116189118344#!/album.php?aid=170006&amp;id=116189118344">aquí.</a></p>
<!-- /wp:paragraph -->