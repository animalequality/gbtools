<!-- wp:image {"id":10166} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/08/4132817113_9aa0822130_z.jpg" alt="" class="wp-image-10166"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":13708} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/08/ImagenWeb.jpg" alt="" class="wp-image-13708"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Dentro de la campaña internacional que está llevando a cabo Igualdad Animal para detener el mayor sacrificio animal del mundo, que tendrá lugar el próximo Noviembre en Nepal, <strong>hoy se anuncia la decisión del gobierno de la India de bloquear el transporte de animales para el sacrifico en la frontera con Nepal</strong><strong>.</strong> Esta medida supone un gran avance ya que la mayoría de los asistentes al festival provienen de la India y traen consigo a búfalos, corderos, cabras y aves hasta el templo de Gadhimai en Nepal. Normalmente, unos 500.000 animales son matados en el festival – sin embargo, <strong>ahora casi la mitad de los animales se salavarán de ser sacrificados</strong> gracias al apoyo de la India.

Para Javier Moreno, coordinador internacional de Igualdad Animal, "<em>este es un gran avance. Iban a masacrar 500.000 animales y ahora con el apoyo del gobierno indio a nuestra petición, esa cifra se verá reducida a la mitad. Seguiremos con nuestra campaña internacional para detener esta masacre y hacer que el gobierno nepalí recapacite y evite la barbarie que tendrá lugar en el Festival de Gadhimai"</em>.

"<em>Este es un paso en la dirección correcta. Ya hemos podido confirmar con el gobierno indio la decisión y trabajaremos para que se lleve a cabo. Sin duda es un gran avance, pornun lado por los 200.000 animales que se salvarán de ser sacrificados, y por otro porque esperamos que esto haga recapacitar al gobierno nepalí para detener la masacre de animales</em>." Indicó Amruta Ubale, coordinadora de Igualdad Animal en India.

En el exterior del templo de Gadhimai, que es el centro del festival, unos 5 millones de personas acuden para presenciar y participar en las matanzas en masa. Consumen grandes cantidades de alcohol y bajo este estado de embriaguez machetean y acuchillan a los animales. Los participantes viajan largas distancias, muchos desde los estados de Uttar Pradesh, Terai y Bihar.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/Azjtpf2Byvk" width="900" height="500" frameborder="0" allowfullscreen="allowfullscreen"></iframe><strong>En tan sólo una semana la campaña internacional de Igualdad&nbsp;Animal ha conseguido más de 100.000 firmas</strong> en total en los distintos países donde ha sido lanzada. "<em>Desde Igualdad Animal respetamos la libertad de credo, pero creemos que la tradición y la religión no pueden justificar causar daño a los animales. Vamos a seguir trabjando sin descanso hasta conseguir que el gobierno Nepalí detenga este masacre que tendría que formar parte del pasado.</em>" Aseguró Amanda Romero, coordinadora de Igualdad Animal en España.

&nbsp;

&nbsp;</p>
<!-- /wp:paragraph -->