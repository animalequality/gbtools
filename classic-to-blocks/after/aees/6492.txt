<!-- wp:image {"id":9508} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/cerdo_cat.jpg" alt="" class="wp-image-9508"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Investigadores del MIT (Instituto Tecnológico de Massachusetts, en Cambridge) afirman que la <strong>mucosidad que recubre el estómago de los cerdos</strong> podría ser una fuente abundante de 'mucina', considerada como un agente antiviral que <strong>podría ser empleado para higiene personal y otros fármacos</strong> profilácticos para prevenir una amplia gama de enfermedades.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según un informe publicado en la revista “Biomacromolecules”, las mucinas de cerdo se podrían añadir a<strong> pastas de dientes, enjuagues bucales, pomadas para heridas y lubricantes genitales</strong> como remedio para prevenir infecciones virales. <em>"Imaginamos que las mucinas gástricas porcinas son prometedores componentes antivirales para futuras aplicaciones biomédicas</em>", reza el informe de los científicos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Katharina Ribbeck y sus colegas, afirman las mucosidades que recubren el interior de la nariz, la boca y la vagina son la primera línea de defensa del sistema inmunológico.</p>
<!-- /wp:paragraph -->