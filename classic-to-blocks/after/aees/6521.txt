<!-- wp:image {"id":9537} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/05/pozo_galgo_caza.jpg" alt="" class="wp-image-9537"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Agentes Forestales y Bomberos de la Comunidad de Madrid <strong>rescataron ayer con vida a un galgo que había sido arrojado a un pozo de más de ocho metros de profundidad</strong>, en el Camino de Los Villares, en el término municipal de Villarejo de Salvanés.<br>
	<br>
	El rescate se produjo tras una llamada al teléfono de emergencias 112, que hizo que los bomberos se desplazaran al lugar para tratar de sacar a la superficie al animal. <strong>El perro se encontraba en el interior del pozo, aplastado por un tronco y un neumático</strong>, y uno de los bomberos tuvo que descolgarse para liberarlo y poder subirlo. <strong>En el fondo del pozo se localizaron además dos sacos con restos de huesos que pertenecían a otros perros</strong> muertos con anterioridad.<br>
	<br>
	Tanto el galgo vivo como los restos de los sacos portaban un chip localizador que permitió a los agentes identificar al responsable de los perros. El Colegio de Veterinarios certificó que el animal rescatado y los restos de otro que aparecieron en el saco estaban registrados a nombre de la misma persona.<br>
	<br>
	El galgo rescatado con vida ha sido adoptado por el vecino que avisó al Centro de Emergencias de la Comunidad de Madrid, 112.<br>
	<br>
	Según han informado los Agentes Forestales de la Comunidad de Madrid, en la región <strong>son frecuentes los casos de galgos abandonados o matados cuando se considera que ya no son útiles para la <a href="http://www.igualdadanimal.org/entretenimiento/caza" target="_blank">caza</a>, o simplemente menos eficaces que los ejemplares más jóvenes</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los Agentes Forestales han presentado denuncia ante el Juzgado número uno de Arganda del Rey, que amplía otra anterior de la Policía Municipal de Villarejo de Salvanés, por si los hechos pudieran ser constitutivos de un Delito de Maltrato Animal.<br>
	&nbsp;</p>
<!-- /wp:paragraph -->