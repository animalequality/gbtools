<!-- wp:image {"id":10787} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/06/europeanparlament1.jpg" alt="" class="wp-image-10787"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://animalequality.de/app/uploads/2016/06/eu_parl1.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Este martes&nbsp;21&nbsp;de junio,&nbsp;<strong>representantes de Igualdad&nbsp;Animal de España, Italia y Alemania estuvieron en&nbsp;el Parlamento Europeo</strong> en Bruselas para hablar sobre el maltrato animal en granjas de conejos en Europa que han documentado en sendas investigaciones en España e Italia.&nbsp;<strong>Cada año&nbsp;326&nbsp;millones de conejos son sacrificados en la Unión Europea</strong> - el 99 % de los conejos están encerrados&nbsp;en pequeñas jaulas de alambre del tamaño de una hoja de papel. Los malos tratos por parte de los trabajadores, heridas abiertas sin atención veterinaria,&nbsp;canibalismo e&nbsp;infecciones no son excepciones, sino la norma.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" src="https://animalequality.de/app/uploads/2016/06/ria_eu.jpg" style="float:left; margin:10px">Las investigaciones de Igualdad Animal en granjas de conejos de Italia y <a href="https://igualdadanimal.org/noticia/2014/05/21/igualdad-animal-denuncia-70-granjas-de-conejos/">España</a> han puesto de manifiesto las pésimas condiciones en las explotaciones&nbsp;europeas en los últimos años.&nbsp;Ahora, por primera vez,&nbsp;tuvimos la oportunidad de presentar nuestros resultados en el Parlamento Europeo. La iniciativa de&nbsp;Compassion in World Farming y la campaña del eurodiputado&nbsp;Stefan Eck demandan la&nbsp;prohibición a nivel europeo las jaulas en batería para conejos. Ria Rehberg, codirectora&nbsp;de Igualdad Animal en Alemania, hizo un llamamiento en su discurso a los diputados presentes a trabajar por la prohibición de las jaulas en batería y denunciar la dramática situación que viven estos animales en las granjas:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«<em>Buenas tardes, en primer lugar quisiera expresar mi más sincero agradecimiento por la oportunidad de hablar hoy aquí en el Parlamento Europeo a favor de los animales. Mi nombre es Ria Rehberg y soy codirectora de Igualdad Animal en Alemania. Igualdad Animal es una organización internacional en defensa de los&nbsp;animales de granja, dedicada a protegerlos promoviendo cambios&nbsp;en la&nbsp;sociedad, en los políticos&nbsp;y empresas para&nbsp;tomar decisiones compasivas hacia los animales.&nbsp;</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>En los últimos años, los investigadores de Igualdad Animal han puesto de manifiesto el abuso sistemático y el maltrato&nbsp;en las granjas de conejos europeas. Más del 60% del total de la producción de conejos en la UE se lleva a cabo en Italia y España. En ambos países Igualdad Animal ha documentado las terribles&nbsp;condiciones en las que malviven los&nbsp;conejos en jaulas en batería. Sólo en España hemos&nbsp;documentado 70 granjas de conejos y 4 mataderos y en todos estos lugares&nbsp;hemos encontrado evidencias de malos tratos.&nbsp;</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Nuestros investigadores han visto conejos con heridas abiertas, infecciones oculares dolorosas e incluso conejos cuyas orejas&nbsp;habían&nbsp;sido mordidas&nbsp;por parte de sus compañeros de jaula&nbsp;debido al estrés del confinamiento. Un&nbsp;conejo en una granja europea&nbsp;es muy probable que viva&nbsp;en una jaula tan pequeña en la que ni siquiera&nbsp;pueda&nbsp;moverse. Un conejo en una granja industrial europea sufre&nbsp;dolor crónico y tiene úlceras en las patas&nbsp;debido al suelo&nbsp;de alambre. Un conejo en una granja industrial europea viven en una jaula del tamaño&nbsp;de una hoja de papel.</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Debido a estas condiciones deplorables hasta un&nbsp;30% de estos sensibles&nbsp;animales&nbsp;mueren o son sacrificados&nbsp;incluso antes de llegar al matadero - ese porcentaje es mayor que con cualquier otro animal de granja. &nbsp;Nuestras investigaciones en granjas de conejos muestran que los animales heridos o enfermos a menudo son golpeados en la cabeza con una barra de metal, o aplastados hasta la muerte contra el suelo. A veces son arrojados a los contenedores estando aún&nbsp;vivos.&nbsp;Cuando mostramos&nbsp;estas imágenes a los cargos públicos para&nbsp;ejercer presión contra las granjas, los ciudadanos de toda Europa están indignados. Como sociedad, los europeos se preocupan por el bienestar de los animales y quieren protegerlos de cualquier daño. </em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Como una encuesta en toda la UE muestra: el 94% de los europeos piensa que la protección del bienestar de los animales de granja es importante. Por eso hoy pido a&nbsp;los representantes de millones de ciudadanos europeos que&nbsp;pongan&nbsp;fin a una de las peores maltratos que padecen los&nbsp;animales de granja. Les pido que apoyen la iniciativa de prohibir el uso de jaulas de batería.&nbsp;No hay diferencia entre dañar&nbsp;a un perro, un conejo o un gato. Todos ellos sufren de la misma forma.&nbsp;Ahora es el momento de poner fin a algunos de los maltratos más horribles que padecen estos animales&nbsp;en Europa.</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Los animales no tienen voz en nuestra sociedad. Es nuestra responsabilidad dársela.&nbsp;Gracias</em>».</p>
<!-- /wp:paragraph -->