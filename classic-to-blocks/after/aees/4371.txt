<!-- wp:paragraph -->
<p>La Consejería de Gobernación, a través de la Delegación del Gobierno de Sevilla, ha sancionado por tan sólo la cantidad de 1.200 euros a una mujer afincada en Sevilla por abandonar a su perro de edad avanzada cuando éste se encontraba en un estado «casi agónico y deplorable», a consecuencia del cual fue eutanasiado para evitarle mayores sufrimientos. La responsable de los hechos nunca presentó ningún recurso negando los mismos.

Según el parte de la clínica veterinaria en la que fue atendido el perro, éste presentaba un tumor testicular avanzado y testículo ectópico; soplo cardíaco, gran infección bucal, con periodontitis severa, retracción de encias, hundimiento del palatino y cornetes nasales debido a la progresión de la infección hacia el maxilar. Además, presentaba problemas de locomoción y cierto grado de pérdida de visión.

Fuente: abcdesevilla.es</p>
<!-- /wp:paragraph -->