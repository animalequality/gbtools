<!-- wp:image {"id":9197} -->
<figure class="wp-block-image"><img src="/app/uploads/2011/11/6360098679_585955a9f4.jpg" alt="" class="wp-image-9197"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Hoy viernes 18 de noviembre, dos activistas de Igualdad Animal han mostrado carteles pidiendo la abolición de los correbous y derechos para los animales mientras se celebraba el mitin de final de campaña electoral de CiU en el pabellón Italia de la Fira de Montjuïc de Barcelona. Los hechos han ocurrido alrededor de las 21 horas, mientras Duran i Lleida realizaba su discurso.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Duran i Lleida y su partido han impulsado la regulación de los correbous alegando a la identidad cultural y a la libertad, ignorando el sufrimiento de los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Con este acto pretendemos generar un debate en la sociedad sobre la explotación a la que son sometidos&nbsp; los animales, y forma parte de nuestra campaña constante en contra de la tauromaquia, que entre otras acciones nos ha llevado a descolgarnos de la fachada de la Plaza de Las Ventas y a saltar a las plazas de Barcelona y Zaragoza reclamando la abolición.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Igualdad Animal es una organización internacional de derechos animales actualmente presente en España, Inglaterra, Polonia y Venezuela.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Enlace a la galeria fotográfica: <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157628042829879">http://www.flickr.com/photos/igualdadanimal/sets/72157628042829879</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Contacto de medios: Maria Xosé Gómez&nbsp; – Portavoz de Igualdad Animal</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>catalunya@igualdadanimal.org Tel. 637 531 306</p>
<!-- /wp:paragraph -->