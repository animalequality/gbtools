<!-- wp:image {"id":12084} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/dehorned_and_branded_animal_for_sale_for_slaughter_0.jpg" alt="" class="wp-image-12084"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-12083" src="/app/uploads/2017/05/dehorned_and_branded_animal_for_sale_for_slaughter.jpg" style="margin-left: 10px; margin-right: 10px; float: right; width: 450px; ">El Ministerio indio de Medio Ambiente y Bosques &nbsp;ha incluido la ley de Prevención de maltrato animal &nbsp;(Regulación de los mercados ganaderos 2017), la cual limita la venta de carne de ganado únicamente en los mercados ganaderos. Esta ley responde a la resolución del Tribunal Supremo y a las recomendaciones del Consejo de Bienestar Animal de India.<br>
<br>
El Tribunal Supremo ha solicitado al gobierno redactar estas nuevas leyes después de que el activista Gauri cumplimentase la petición. <strong>A raíz de esto, Igualdad&nbsp;Animal ha presentado una investigación desde dentro de estos mercados ganaderos</strong>. Lo que descubrieron fue impactante.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hemos publicado imágenes que documentan el maltrato animal a lo largo de siete estados. A los animales s<strong>e les niega el agua, la comida y el cobijo en los mercados ganaderos</strong>. Algunos son atizados con palos o con la mano, o incluso se les restriega <strong>chile picante en los ojos para que continúen moviéndose.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/9U4SwxLexf8" width="854"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La nueva ley que ha anunciado el gobierno, <strong>que incluye las peticiones de Igualdad&nbsp;Animal</strong>, aportará una mejora en las condiciones de los animales en estos mercados:</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li>El ganado no será vendido a mataderos en estos mercados.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Queda prohibido cualquier tipo de maltrato, incluido el marcado a fuego.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Protección a los animales gestantes.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Queda prohibido usar la fuerza contra ellos y el abuso físico.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p>Amruta Ubale, Directora de Igualdad Animal en India, dice: “Estas crueles prácticas son ilegales según el acto de Prevención de Maltrato Animal de 1960, pero no existía un control de estos mercados para hacerlos responsables ante la ley. <strong>Esta nueva ley llena ese vacío legal</strong>.”</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Le agradecemos al Tribunal Supremo de India y al Ministerio de Medio Ambiente y Bosques por haber introducido estas medidas de protección que mejorarán la vida de estos animales.<br>
&nbsp;</p>
<!-- /wp:paragraph -->