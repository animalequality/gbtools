<!-- wp:paragraph -->
<p>REUTERS

BRASILIA.- Las autoridades brasileñas han descubierto la matanza de alrededor de 740 cocodrilos en una reserva natural de la selva del Amazonas, cuyos cadáveres fueron despellejados y conservados en sal en la reserva natural de Piagacu-Purus, a unos 300 kilómetros al oeste de la capital estatal brasileña de Manaus.

"Nos quedamos sorprendidos y alarmados. Esto indica una operación comercial a gran escala", señala Aldenira Queiroz, responsable de la citada agencia.

La carne estaba destinada a ser vendida para consumo humano en el estado brasileño adyacente de Para, y ahora será probablemente incinerada tras su confiscación.

Las pieles de los cocodrilos han sido arrojadas a ríos, según ha indicado un portavoz de IPAAM.

Ya se habían producido advertencias de que la reserva era el lugar predilecto para la caza de los políticos locales y empresarios adinerados de la zona.</p>
<!-- /wp:paragraph -->