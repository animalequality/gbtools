<!-- wp:paragraph -->
<p>La Policía Local de Santa Cruz de Tenerife rescató el pasado fin de semana a un cachorro de perro al que un hombre de 24 años, intentó ahogar en la playa de Las Teresitas.

Minutos antes, los agentes habían sido alertados de los hechos a través de radiocontrol, gracias a las llamadas al 092 de varios bañistas indignados ante la violenta escena.

Según los testigos, el hombre, después de intentar ahogar al perro, trató de enterrarlo en la arena y, al no conseguirlo, se dedicó a propinarle patadas. Cuando los policías llegaron al lugar, el perro se encontraba encogido en el suelo, con síntomas de hipotermia y una pata lesionada.

Ante la intervención de los agentes, el agresor se limitó a justificarse alegando que se encontraba en tratamiento psiquiátrico e incluso mostró documentos para acreditarlo.

Finalmente, los policías trasladaron al perro al albergue comarcal de animales de Valle Colino y pusieron a la autoridad judicial competente en conocimiento de los hechos.

Fuente: 20minutos.es</p>
<!-- /wp:paragraph -->