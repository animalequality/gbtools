<!-- wp:image {"id":11185} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/10/28768101944_9ab038eb2b_z.jpg" alt="" class="wp-image-11185"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Encerrados de por vida, en minúsculos reductos, sin espacio para estirarse, en ocasiones ni para darse la vuelta y aislados del resto de animales. <strong>Así viven miles y miles de animales en el mundo en las granjas ganaderas y un movimiento imparable ya ha logrado alcanzar un objetivo: acabar con las jaulas</strong>.

<strong>Recientemente Igualdad&nbsp;Animal ha mostrado al mundo <a href="https://lavidaenunajaula.igualdadanimal.mx/" target="_blank" rel="noopener">cómo viven las gallinas en las granjas de producción de&nbsp;huevos</a> de México</strong>. Pasillos y pasillos con barrotes en varios niveles, olor a excrementos, ruido y desesperación. Sus patas asoman entre los hierros, no pisarán la arena nunca, sus uñas acaban dejándolas atrapadas entre los barrotes. Jamás extenderán sus alas, se pisarán unas a otras por el poco espacio, pasarán hambre y sed al forzarlas a desplumarse. Muchas morirán por estas causas o por otras, nunca tendrán ayuda veterinaria individualizada y agonizarán hasta terminar su vida.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11186} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/10/28770589393_f13fb4606d_z.jpg" alt="" class="wp-image-11186"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Granja de la industria del huevo. Investigación de Igualdad Animal México, 2016.

Las granjas industriales rara vez verán sus jaulas como la consecuencia que provoca esta cruel realidad. En las guías que manejan los ganaderos rara vez se tomará en cuestión el bienestar de los animales. En un estudio de la Universidad de Ciudad de Juárez, los científicos estudian la relación entre las jaulas y las gallinas solo para ver cúal es el punto de productividad y coste más óptimo para la empresa. No tienen en cuenta ni bienestar ni trastornos que sufren los animales que estarán dentro de las jaulas de por vida a la hora de hablar de densidad dentro de las jaulas.

<strong>La ley, del lado del bienestar animal</strong>

Algo que sucede de forma similar en muchos países pero que en algunos como en Estados Unidos debatieron si debería continuar siendo así. <strong>En <a href="http://www.citizensforfarmanimals.com/" target="_blank" rel="noopener">Massachusetts</a>, se llevará a cabo la votación de una propuesta que aspira a acabar con las jaulas en la ganadería industrial para siempre</strong>. La aprobación de esta iniciativa significaría dar los pasos para que en en ningun lugar de ese estado hubiera una gallina que no pudiera darse la vuelta en la jaula, tumbarse o extender sus alas. Algo tan básico como eso, debe reconocerse en la ley.

Lo mismo con los cerdos, otro de los animales que sufren en masa la producción industrial. Los granjeros aseguran que emplean jaulas porque, además del ahorro de espacio, les facilita la alimentación, la limpieza y la medicación de los animales. Pero entrar en una nave donde hay confinados cientos de cerdos, esos aspectos para facilitar la vida del granjero no parecen un argumento de peso. <a href="https://www.youtube.com/watch?v=PK9d3i_22M8&amp;feature=youtu.be" target="_blank" rel="noopener">Los investigadores de Igualdad Animal Italia</a> demostraron cómo es la vida de los cerdos desde que nacen hasta que son enviados al matadero.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11187} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/10/17746796519_01bdce1ed4_z.jpg" alt="" class="wp-image-11187"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Cerdas en jaulas de gestación. Investigación de Igualdad Animal Italia en la industria porcina, 2015.

Además de prácticas tan crueles como arrancar los dientes o cortar la cola o genitales a los bebés sin anestesia, las cerdas se llevan la peor parte. <strong>Ellas son confinadas en jaulas de gestación donde permanecerán semanas y semanas tumbadas sobre su costado. Nunca podrán ponerse en pie, ni darse la vuelta</strong>. Debido a esto tienen heridas profundas en su piel y padecen cuadros de ansiedad extrema.

«En Massachusetts, se llevará a cabo la votación de una propuesta que aspira a acabar con las jaulas en la ganadería industrial para siempre».

Otras campañas ponen el foco en acabar con las jaulas también en otro tipo de industria: <a href="https://www.youtube.com/watch?v=VGKQKGOyPtU&amp;feature=youtu.be" target="_blank" rel="noopener">la de carne de pollo</a>. <strong>Se trata de uno de los animales qué más se crían para consumo humano y que peores condiciones de vida tiene antes de su sacrificio</strong>. Son apilados nada más salir del huevo, desechados si tienen algún tipo de anomalía, amputados sus picos con un dolor extremo, empujados a agredirse los unos contra los otros por la falta de espacio. Esto para conseguir su carne porque <a href="https://igualdadanimal.org/noticia/2016/07/19/nueva-investigacion-en-australia-saca-la-luz-lo-que-nos-oculta-la-industria-del-huevo/" target="_blank" rel="noopener">la industria del huevo directamente ‘elimina’ a los machos</a> nada más nacer porque no ponen huevos y son ‘inservibles’ para carne (link video).

Contra esa otra industria, la de granjas de gallinas ponedoras, lucha la alianza <a href="https://openwingalliance.org/" target="_blank" rel="noopener">Open Wing Alliance</a>. Una veintena de asociaciones animalistas se unen para crear una coalición global para lograr que las jaulas en las granjas industriales sean cosa del pasado.

<strong>Si quieres estar al tanto de cómo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank" rel="noopener">suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación</strong>.</p>
<!-- /wp:paragraph -->