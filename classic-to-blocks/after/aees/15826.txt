<!-- wp:paragraph -->
<p>Con apenas 18 meses de vida, Louise-May ya era considerada «desperdicio» para la industria que la hizo nacer y explotó su cuerpo hasta el extremo. Y todo esto a pesar del hecho de que una gallina, en condiciones naturales, puede vivir una vida feliz y saludable por más de 10 años.

Durante año y medio, Louise-May tuvo que soportar lo que ninguna persona durante toda su vida podría siquiera llegar a imaginar que podría ocurrirle. Encerrada en una jaula en la que jamás pudo extender sus alas o posarse y donde le fue negado realizar cualquier comportamiento natural de su especie, jamás pudo sentir y ver la luz del sol. Sí, la industria del huevo es infinitamente cruel con las gallinas que, al contrario de lo que dice la creencia popular,<strong> diversos estudios científicos afirman que son animales altamente sensibles e inteligentes.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><strong>¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</strong></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><strong><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></strong></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
&nbsp;

Un día, alguien sacó a Louise-May de su prisión y la colocó en una pequeña caja en la parte trasera de una camioneta. Ella y sus hermanas Meg, Jo, Amy y Beth dejaban atrás ese infierno para siempre. Pero iban directamente a ese lugar donde las gallinas jamás regresan e, irónicamente, el primer y único día en que sintieron la luz del sol iba también a ser el último en el que lo hicieran.

Pero las cinco gallinas fueron sumamente afortunadas de que la compasión interviniera. En lugar de ser llevadas al matadero, un giro del destino hizo que Louise-May y sus hermanas fueran llevadas al refugio de animales Edgar´s Mission.

Hoy, Louise-May vive cada día felizmente en un entorno seguro y, sobre todo, lleno de un sol infinito. El santuario está lleno de espacios abiertos <strong>que le permitieron estirar y batir sus alas de nuevo</strong> y poner sus huevos en privado y en nidos construidos por ella.

Louise-May lleva una alimentación nutritiva que ha hecho que sus plumas crezcan gruesas y saludables y cubran lo que antes de llegar al refugio era un cuerpo desnudo, maltratado y adolorido. Tanto ella como sus hermanas saben que ya no son unas prisioneras y aman tumbarse en la tierra para tomar baños de sol y tierra por horas.

La gallina, considerada por la industria como una simple mercancía, logró encontrar el lugar que le corresponde como ser sensible y es un ejemplo de cómo la compasión puede transformar el dolor en esperanza.

Las elecciones que hacemos cada día pueden también cambiar el destino de millones de gallinas. Por favor, únete a <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank" rel="noopener">Protectores de Animales en México </a>y <a href="https://igualdadanimal.org/defensores-animales/" target="_blank" rel="noopener">Defensores de Animales en España</a> para hacer la diferencia para ellas.</p>
<!-- /wp:paragraph -->