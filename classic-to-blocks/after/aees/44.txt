<!-- wp:paragraph -->
<p>El pasado Lunes 20 de Febrero, seis galgos fueron hallados muertos dentro de bolsas de plástico en el interior de un pozo de la localidad de Alcázar de San Juan (Ciudad Real). No es la primera vez que en este pozo aparecen animales no humanos muertos, ya que el pasado Noviembre, se halló vivo y encerrado en un plástico a otro galgo.

Los perros de esta raza son algunas de las habituales víctimas de la caza en el estado español. Suelen ser asesinados u abandonados cuando dejan de cumplir adecuadamente la función para la que fueron criados o comprados, la caza.

Desgraciadamente esta es una "actividad" que se lleva a cientos de vidas por delante cada día. Por ello, debemos cuestionarla y mostrar las víctimas que produce independientemente de que estas sean galgos, codornices, jabalíes o pichones.

Esta forma de explotación animal no debe ser rechazada por ser "innecesaria", o debido a que los "beneficios" que se obtienen no justifiquen el sufrimiento que se inflige a los demás animales, sino porque al igual que cualquier forma de explotación animal es inaceptable tratar a otras/os animales como no trataríamos a otros humanos.</p>
<!-- /wp:paragraph -->