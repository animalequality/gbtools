<!-- wp:image {"id":11848} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/04/32572539594_c335980035_z.jpg" alt="" class="wp-image-11848"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>«Este es el peor caso que he visto y puedo decir que he visto algunos casos horribles. Por ello este Juzgado opina que esta crueldad excede el umbral de la condicional.»</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Así de expeditivo se mostraba el juez Jeff Collingwood hablando a Owen Nichol, acusado de dos penas de maltrato y crueldad animal. Sucedió en una granja de Somerset, Inglaterra. <strong>Igualdad&nbsp;Animal grabó con cámara oculta cómo el granjero abusaba terriblemente de vacas y terneras.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5">&nbsp;</font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nichol aparece en las imágenes pateando a <strong>una ternera recién nacida</strong> hasta siete veces, después la alzó y la tiró por los aires lejos de su madre, que también sufrió la cólera del acusado. Nichol golpeó repetidamente <strong>a la vaca que acababa de dar a luz</strong> y estampó una puerta en su costado una vez tras otra. Las durísimas imágenes conseguidas por Igualdad Animal Reino Unido <strong>han dado pie a la detención &nbsp;del granjero.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5">&nbsp;</font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font color="#808080">«Se trata del peor caso de abuso hacia animales que he presenciado en mis 35 años de carrera.»</font> </font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font size="5">&nbsp;</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El detenido ha admitido dos cargos de abuso hacia animales y ahora espera condena, <strong>que podría ser de cárcel. </strong>El Juzgado tiene previsto dictar sentencia el 26 de abril.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11849} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/04/95445599_nichol2.jpg" alt="" class="wp-image-11849"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><font size="5"><font size="5"></font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El veterinario encargado de realizar un informe para el juzgado en base a las imágenes de Igualdad Animal, declara que «se trata del pero caso de abuso hacia animales que he presenciado en mis 35 años de carrera.»</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Toni Shephard, Directora Ejecutiva de Igualdad Animal Reino Unido, describe el comportamiento de Nichol como «crueldad inimaginable».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Estamos satisfechos de que haya sido detenido por esta cruel violencia hacia animales indefensos y esperamos que sea condenado a la pena máxima», añade.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font size="5">&nbsp;</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="1"><font color="#808080">El detenido, Owen Nichol</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font size="5"><font size="1">&nbsp;</font></font></font>La noticia ha saltado a la prensa británica. Medios nacionales como la <a href="https://www.bbc.com/news/uk-england-somerset-39483165" target="_blank">BBC</a> y <a href="http://www.dailymail.co.uk/news/article-4376772/Farm-worker-facing-jail-STAMPING-calf.html" target="_blank">Daily Mail</a> se han hecho eco del caso, mostrando las imágenes y el vídeo conseguido por los investigadores de Igualdad Animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font size="5"><font size="1"><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/EHfDmCTaa1I" width="854"></iframe></font></font></font></p>
<!-- /wp:paragraph -->