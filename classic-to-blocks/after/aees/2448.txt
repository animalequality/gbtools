<!-- wp:paragraph -->
<p>Copenhague-Según informa Agrodigital.com, Dinamarca prohibió el transporte de animales a países externos a la UE de más de 24 horas de duración. Esta medida se tomó debido a la difusión televisiva de unas imágenes en las que varios cerdos eran maltratados y matados durante un transporte en camión hacia Rusia que fue bloqueado en la frontera con Letonia.

En dicho transporte murieron 33 cerdos y otros muchos fueron heridos con huesos rotos y otras lesiones. La misma televisión confirmó que estos casos se repiten en la frontera con Bielorusia.

Resulta paradójica esta muestra de preocupación porque los cerdos mueran o sean torturados en el transporte y no haya ningún problema en que esto ocurra en los mataderos. El principal problema de los cerdos y otros animales no es su transporte, ni el modo en que se les mata, sino que se les cría, explota y asesina, infravalorando todos sus intereses, solo porque no pertenecen a la especie humana</p>
<!-- /wp:paragraph -->