<!-- wp:paragraph -->
<p>DAKAR, Senegal - Los últimos hipopótamos en el oriente del Congo podrían desaparecer para fin de año si continúan las intensas cacerías efectuadas por las milicias.

Apenas en las primeras dos semanas de este mes más de 400 hipopótamos fueron atacados y perdieron la vida en el Parque Nacional Virunga, hogar de una de las mayores concentraciones de estos animales en África Central, afirmó el sábado la Sociedad Zoológica de Londres en una declaración en su página de internet.

Un estudio reciente patrocinado por ese grupo conservacionista mostró que actualmente existen menos de 900 hipopótamos en el parque, "una reducción dramática de los 22.000 registrados allí en 1988", señaló el comunicado.

"Si la matanza continúa al ritmo actual, los trabajadores de campo de la Sociedad temen que para Navidad no quedarán hipopótamos en muchas partes del parque", indicó el grupo.

La milicia llamada Mai-Mai, un grupo de combatientes congoleses de diversas tendencias y lealtades que opera en vastas zonas del este del Congo, estableció una base en el parque a principios del mes, dijo la Sociedad.

En las últimas semanas, "se han matado a más de 400 hipopótamos, además de numerosos búfalos, elefantes y otros animales… La principal causa es el uso del área como base por un grupo de rebeldes conocidos como Mai-Mai, quienes comen y venden carne de hipopótamo, además de vender el marfil encontrado en los dientes caninos de esos animales. El grupo ha atacado además a guardabosques y sus familiares".

Soldados del gobierno, así como rebeldes hutus que escaparon el genocidio étnico en Ruanda en 1994 y se refugiaron en la selva del oriente del Congo, han sido responsabilizados también por la matanza de hipopótamos, así como de otros animales en Virunga y otros parques congoleses, incluyendo Kahuzi-Biega en el sur, hogar de una especie de gorilas.

Independientemente de la cantidad de hipopótamos que se maten de una vez o el ritmo en que descienda su población debido a la caza, no debemos olvidar que la vida de cada hipopótamo es valiosa para ese individuo. Resulta igual de injusto matar a un individuo perteneciente a un grupo numeroso que a uno cuyo grupo sea más reducido.</p>
<!-- /wp:paragraph -->