<!-- wp:paragraph -->
<p>Un particular sin identificar asesinó a una pava, lanzándola desde el campanario de la parroquia Santa María de la Magdalena de Cazalilla (Jaén).

El concejal del PP en el Ayuntamiento de Cazalilla Rafael Martínez confirmó el asesinato del animal, que se produjo a las 17.45 horas, aunque admitió no saber la identidad de la persona que lo hizo, extremo que ya ha sucedido en años anteriores, puesto que no es inusual que lo hagan con "capuchas o con caretas, para que no se les reconozca". Martínez recordó que el Consistorio de la localidad "está desligado" de la iniciativa.

El hombre que asesinó el año pasado a otra pava desde el campanario de Cazalilla fue sancionado por la Junta de Andalucía con 2.000 euros, multas que en los años 2004 y 2006 recayeron sobre el Ayuntamiento de la localidad y en 2005 sobre la persona que entonces se identificó a sí misma ante la Guardia Civil como la que asesinó a este animal.</p>
<!-- /wp:paragraph -->