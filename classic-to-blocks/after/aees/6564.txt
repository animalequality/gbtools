<!-- wp:image {"id":9573} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/06/arruis_caza.jpg" alt="" class="wp-image-9573" title="Cadáver de arrui mostrado por un cazador"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>La Consejería de Presidencia de Murcia, a través de la Dirección General de Medio Ambiente, da<strong> vía libre a la caza arruís en los montes de los alrededores del Parque Regional Sierra España</strong>, para <em>«minimizar su impacto en las explotaciones agrícolas, la ganadería o los montes».</em>

Esta medida se introduce en consonancia con la aplicación del <a href="https://www.boe.es/boe/dias/2011/12/12/pdfs/BOE-A-2011-19398.pdf" target="_blank" rel="noopener">Real Decreto 1628/2011</a>, de 14 de noviembre, que regula el listado y <strong>catálogo español de especies exóticas invasoras</strong> y que propone medidas de erradicación, basadas primordialmente en la matanza, para animales de determinadas especies. Dicho catálogo concede a las comunidades la posibilidad de gestionar a los arruís (también llamados “muflones del Atlas”) dentro de las reservas de caza o cotos en los que fueran introducidos legalmente antes del año 2007.

Pese a que las organizaciones ecologistas –muy críticas con el mantenimiento de la especie en la Región– tenían la esperanza de que finalmente se decidiese su erradicación, <strong>la Comunidad Autónoma ha querido convertir a los arruís en un atractivo turístico y económico</strong>, manteniéndolos como especie cinegética.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13784} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/06/arruis_murcia.jpg" alt="" class="wp-image-13784"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><small>Los arruís, originarios del norte de África, fueron introducidos en Sierra Espuña con fines cinegéticos en el año 1970.</small>

El período ordinario de caza mayor del arruí se fija del 1 de julio de este año al 31 de enero de 2013. Asimismo, se podrá dar muerte a cabras montesas desde el 12 de octubre hasta el 3 de febrero, y a ciervos, muflones y gamos entre el 9 de septiembre y el 17 de febrero del próximo año.</p>
<!-- /wp:paragraph -->