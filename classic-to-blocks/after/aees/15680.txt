<!-- wp:paragraph -->
<p>Durante su ponencia «Doctor, quiero ser vegano» en el III Congreso de la Sociedad Española de Médicos de Atención Primaria en Cataluña, las doctoras Montserrat Rayo y Patricia Comas han reclamado que las médicas y médicos deben recibir durante su formación universitaria mayor formación sobre alimentación vegetariana y vegana. Según las doctoras, el motivo de esta necesidad se debe a que la presencia de pacientes que siguen este tipo de alimentaciones «es una realidad creciente en las consultas de atención primaria».

La ponencia presentada por Rayo y Comas tuvo como objetivo dotar al personal sanitario de mayor conocimiento sobre las personas vegetarianas y veganas. «El paciente vegetariano y vegano está cada vez más presente en las consultas de atención primaria, pero la formación que los médicos reciben durante la carrera sobre nutrición en general y alimentación vegetariana, específicamente, es muy escasa», opinan ambas doctoras.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente a <span style="color: #0000ff;">nuestro e-boletín</span>!</a></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Siendo profesionales de la salud, resulta «necesario y exigible» que médicas y médicos tengan conocimientos en nutrición y sean capaces de «acompañar a las personas en el proceso de adopción de hábitos de vida saludables, ya sea tanto en forma de alimentación vegetariana como omnívora o incluso prescribiendo ejercicio físico», agregaron.

De acuerdo con Rayo y Comas una alimentación vegetariana variada, con gran porcentaje de verduras, frutas, legumbres y derivados, semillas molidas, frutos secos, cereales integrales y grasas de calidad, que alterne vegetales cocidos con crudos, con abundantes hojas verdes (espinacas, kale, col) y crucíferas (coliflor, brócoli), que procure una porción de alimento proteico de calidad en todas las ingestas principales y que asegure hábitos de vida saludables (beber agua, ejercicio físico y tomar el sol 10-15 minutos al día), «garantiza una nutrición adecuada».

«Afortunadamente, en la actualidad, se sabe que la alimentación saludable es un pilar fundamental para garantizar la salud en las personas y disminuir la incidencia de enfermedades crónicas como la diabetes, la dislipemia o la hipertensión arterial, y cada vez hay más estudios que lo confirman», concluyeron.</p>
<!-- /wp:paragraph -->