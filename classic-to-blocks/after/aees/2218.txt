<!-- wp:paragraph -->
<p>¡Por favor, ayuda a que esta convocatoria tenga la máxima difusión!

Cuatro activistas de Igualdad Animal van a ser juzgados el próximo Lunes día 19 de Marzo por encadenarse a un matadero el pasado 4 de Octubre (Día Internacional de los Animales) para denunciar el uso de animales como alimentación y promover el vegetarianismo. Este es el primer juicio -según nos consta- a activistas que defienden a los animales en el estado español. El juicio será a las 11 de la mañana en el Juzgado de Instrucción número 1 de Leganés -Madrid- (Plaza de la Comunidad de Madrid, nª 5). 

Por ello, Igualdad Animal ha convocado una concentración frente al juzgado a esa misma hora para que quienes quiera apoyarnos y quieran defender el derecho a mostrar nuestra repulsa a la explotación animal puedan hacerlo. <b>Está permitido además que quienes lo deseen entren de público hasta que se llene la sala. Según nos ha comentado la abogada de Igualdad Animal, es importante que haya gente apoyando a los activistas imputados ya que puede revertir en una reducción de condena.</b>
¡Gracias a quienes habéis apoyado a los cuatro activistas de Igualdad Animal!

Más información:
https://igualdadanimal.org/
Tel. 675 737 459</p>
<!-- /wp:paragraph -->