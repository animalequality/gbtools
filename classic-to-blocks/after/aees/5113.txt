<!-- wp:paragraph -->
<p>El 10 de Diciembre, se celebra en todo el mundo el día por los derechos humanos. Desde hace muchos años, se viene celebrando también el día por los derechos animales, precisamente para remarcar que los animales que no pertenecen a la especie humana también merecen respeto. 

Igualdad Animal desde sus comienzos ha realizado protestas espectaculares este día para llamar la atención de los medios de comunicación, consiguiendo llegar a millones de personas.

Mención especial cabe para la acción realizada el año pasado, en la que 50 activistas sostuvimos cadáveres reales de animales. Una acción sobria e impactante para una fecha tan especial, que tuvo una excelente repercusión mediática.

Este año volveremos a repetir la protesta pero trataremos de doblar el número de activistas -siendo 100 o más- que mostraremos a la sociedad sus propias víctimas.

La protesta será el Domingo 6 de Diciembre a las 12 horas en la Puerta Del Sol. Madrid

Apúntate y participa en un impactante acto al que acudirán activistas de distintas ciudades de España y también de otros países, como Italia, República Checa y Reino Unido.

Si quieres participar, por favor escríbenos a info@igualdadanimal.org o llámanos al 915 222 218 o al 691 054 172.

Web del Día de los Derechos Animales: www.DiadelosDerechosAnimales.com
El vídeo de la protesta del 2008 en La2 Noticias: http://www.youtube.com/igualdadanimaltv#p/a/u/1/PT1SdY_Mph0</p>
<!-- /wp:paragraph -->