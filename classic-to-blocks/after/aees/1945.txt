<!-- wp:paragraph -->
<p>Washington – Según informa 20minutos.es, la Marina Estadounidense está estudiando la posibilidad de desplegar grupos de delfines y leones marinos “entrenados” para proteger bases militares de posibles ataques submarinos.

El grupo estaría formado por un total de 30 individuos entre delfines y leones marinos, los cuales están “completamente capacitados para realizar las funciones que necesita esta misión", según Tom LaPuzza, portavoz del grupo de “entrenamiento” de los mamíferos. 

Los delfines y los leones marinos son individuos que experimentan sus propias vidas y desean disfrutarlas en libertad. Nadie tiene ningún derecho a secuestrarles y esclavizarles, sea con el fin que sea.</p>
<!-- /wp:paragraph -->