<!-- wp:paragraph -->
<p>Hemos hecho públicas nuevas imágenes de una estremecedora investigación que revela las terribles condiciones de los peces durante la pesca de arrastre en el mar Mediterráneo, particularmente en la costa de Cerdeña en Italia.

En los mares, son capturados a diario millones de peces en total incumplimiento de las normas que regulan la pesca y sin ninguna consideración del sufrimiento que les causa esta práctica.

Las<span style="color: #0000ff;"> <a style="color: #0000ff;" href="https://www.flickr.com/photos/animalequalityitalia/sets/72157669709010897">imágenes obtenidas</a> </span>por nuestro investigador encubierto revelaron:

- Las redes capturan cientos de miles de peces que, arrancados de su entorno, se retuercen en agonía mientras luchan por respirar.

- Peces cuyos órganos internos salen por sus bocas debido a la descompresión.

- Los peces y otros animales considerados desecho son tirados al mar, heridos o ya muertos.

- Desechos y muebles que son recogidos junto con los peces son devueltos al mar.

- Tiburones pequeños, anguilas y peces blancos de todo tipo que agonizan debido a la falta de oxígeno.

- Un operador que atraviesa a una anguila con un gancho para que se desangre, sufriendo un inmenso dolor hasta morir.

- Operadores que destripan y decapitan a los peces estando aún conscientes.

- Peces que son congelados vivos en cámaras frigoríficas.

- Peces, langostas y otros animales que buscan desesperadamente una vía de escape.

&nbsp;

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/AlNleAQ0pSw" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Debido a que viven en un medio tan distinto al nuestro, tendemos a pensar poco en los peces o a no sentir empatía hacia ellos. Pero mientras que admiramos su belleza en documentales o cuando los observamos al nadar, olvidamos lo que implica para ellos ser arrancados de su entorno y arrastrados dentro de las redes, para luego agonizar por minutos sobre la cubierta de los barcos antes de ser matados o congelados.

Diversos estudios comprueban que los peces sienten dolor y sufren inmensamente bajo condiciones de estrés prolongado. Y, particularmente, el estrés que le provoca la imposibilidad de respirar luego de ser sacados del mar es muy fuerte. Puede durar minutos y termina solo con la muerte violenta o la congelación.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large; color: #000000;">¿Quieres recibir las mejores noticias de actualidad sobre los animales? </span></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large; color: #0000ff;">¡Suscríbete gratuitamente a nuestro<a style="color: #0000ff;" href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank" rel="noopener"> e-boletín</a>!</span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Según los últimos informes de la FAO y Oceana, corremos el riesgo de que tan pronto como 2048 no haya peces en los mares. En particular, el mar Mediterráneo está en grave peligro. Además, una de las amenazas destacadas por ambos organismos es la práctica constante de la pesca ilegal, que en Italia ha alcanzado las 10.000 horas solo frente a Sicilia.

Todo esto sucede en un mundo donde la demanda de carne de pescado está alcanzando picos nunca antes vistos, incluso excediendo el de la carne de otros animales.

Desde hace algún tiempo, en Igualdad Animal venimos trabajando para reducir el sufrimiento de estos animales. No solo con respecto al consumo de peces capturados en el mar y con todas las consecuencias que esto conlleva, sino, especialmente, con respecto a la acuicultura y sus facetas aún por verificar y potencialmente muy peligrosas.

«En todas estas prácticas destacan dos elementos muy importantes en los que deberíamos centrarnos en el futuro: la sostenibilidad ambiental y el sufrimiento animal"», dice Matteo Cupi, nuestro director ejecutivo en Italia.

«No podemos escapar de todo esto, porque nuestros mares están sufriendo mucho por estas prácticas a menudo incontroladas, sin mencionar que la literatura científica ha demostrado que los peces poseen características que normalmente atribuimos a otros animales, como la capacidad de sentir dolor y la sensibilidad ante los estímulos negativos y el sufrimiento bajo estrés. Esperamos que cada vez más personas se den cuenta de lo que realmente significa consumir tantos peces, y trabajaremos en el futuro cada vez más para resaltar todas las contradicciones de esta industria», concluye Cupi.</p>
<!-- /wp:paragraph -->