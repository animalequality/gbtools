<!-- wp:image {"id":10919} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/13936636_10210595529832750_1352795071_n.jpg" alt="" class="wp-image-10919"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>El crecimiento internacional de un <a href="https://igualdadanimal.org/noticia/2016/05/28/naciones-unidas-nuestro-actual-sistema-alimentario-es-insostenible/" target="_blank">nuevo y mejor modelo alimentario</a> es imparable. La <strong>buena noticia</strong> nos llega esta vez desde Dinamarca.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dos de las mayores cadenas de supermercados de Dinamarca, Coop y Dansk Supermarked han experimentado un aumento de ventas de <strong>productos alternativos a la carne</strong> de un 30% en el periodo 2014-2015.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los productos cuya venta aumenta incluyen salchichas vegetales, tofu, falafel y hamburguesas vegetales elaboradas con lentejas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según declaraciones del director de análisis de mercado de Coop, el <strong>espectacular aumento</strong> del periodo «va a continuar durante 2016».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El <strong>nuevo modelo alimentario</strong> basado en opciones vegetales se extiende por toda Europa. Este mismo año noticias similares llegaban desde <a href="https://igualdadanimal.org/noticia/2016/05/20/el-numero-de-britanicos-que-se-alimentan-de-forma-vegana-ha-subido-un-360/" target="_blank">Reino Unido</a> y <a href="https://igualdadanimal.org/noticia/2016/04/24/las-opciones-vegetarianas-crecen-un-633-en-alemania/" target="_blank">Alemania</a>. También en el este del viejo continente se está produciendo un aumento significativo de las <a href="https://igualdadanimal.org/noticia/2016/05/04/el-desarrollo-de-alternativas-vegetales-la-carne-tendencia-mundial-numero-uno-para-el/" target="_blank">opciones vegetarianas y veganas</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10920} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/shutterstock_395490703.jpg" alt="" class="wp-image-10920"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En cuanto a Dinamarca, el Instituto Nacional de Alimentación ha hecho público un estudio según el cual el 5% de los daneses declaran <strong>no comer nada de carne</strong>. Fuentes del Instituto han comentado que «la importancia de la carne en nuestros platos está experimentando un cambio», y añaden que «en lo que se refiere a la alimentación familiar cotidiana, <strong>la carne ya no juega un papel central</strong>».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El estudio revela que la sociedad danesa <strong>está reduciendo significativamente su consumo de carne</strong>. Este dato está en sintonía con otro estudio que la cadena de supermercados Coop ha llevado a cabo entre sus clientes. Un 3,3% de sus consumidores se declaran a sí mismos <a href="http://www.igualdadanimal.org/noticias/7433/por-que-hay-cada-vez-mas-personas-vegetarianas-y-veganas-nuestro-alrededor" target="_blank">vegetarianos o veganos</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Si quieres unirte a las personas que han decidido marcar la diferencia a través de su alimentación, por favor considera consumir alternativas vegetales a la carne. Tienes toda la información y recetas que necesitas en las fantásticas websites <a href="https://www.gastronomiavegana.org/" target="_blank">Gastronomía Vegana</a> y <a href="https://danzadefogones.com/" target="_blank">Danza de Fogones</a>.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="http://cphpost.dk/news/danes-buying-more-vegetarian-substitutes-for-meat-and-dairy.html" target="_blank">Fuente: http://cphpost.dk/news/danes-buying-more-vegetarian-substitutes-for-meat-and-dairy.html</a></p>
<!-- /wp:paragraph -->