<!-- wp:paragraph -->
<p>Madrid- Según informa el diario ADN, la Feria de Arte Contemporáneo, ARCO, expone una “obra” en la que dos pájaros vivos están encerrados en una urna de cristal. 

Los pájaros son seres sintientes que no desean verse privados de su libertad y recluidos en un espacio infinitamente menor del que necesitan, pues esta situación les es desagradable, igual que nos lo sería a cualquiera de nosotros. No tener en cuenta sus intereses y utilizarles, en este caso, para nuestro entretenimiento, es injustificable.</p>
<!-- /wp:paragraph -->