<!-- wp:paragraph -->
<p>En Igualdad Animal presentamos hoy, domingo 7 de abril, una i<a href="https://www.sinvoz.org/" target="_blank">ntensa investigación realizada en mataderos y comercios de carne de perro</a> en la península de Leizhou y la provincia de Pengjiang en China.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://www.flickr.com/photos/animalequalityuk/sets/72157630067365847/" target="_blank"><img alt="" src="https://farm6.staticflickr.com/5194/7187911613_e3f37b78a2_b.jpg" style="width: 200px; float: left;"></a>Los perros criados para el comercio de carne son muchas veces cogidos de la calle o separados de sus familias. Otros&nbsp;<strong>pasan la práctica totalidad de su vida confinados en jaulas de alambre.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La tortura física y psicológica a la que son sometidos estos animales alcanza límites difíciles de asimilar. En el momento de ser matados, y debido a que el aumento de adrenalina intensifica el sabor de su carne, es habitual que los perros sean <strong>arrojados vivos al agua hirviendo</strong> o sean <strong>degollados deliberadamente delante de otros perros </strong>para aumentar así el grado de terror que experimentan.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>► Advertencia: contenido gráfico de extrema violencia</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/S1KYkeXuKRI" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong style="text-align: justify;">Más de 10 millones de perros y 4 millones de gatos son matados cada año por su carne y piel en China.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph -->
<p><em>«Vi que algunos perros corrían libres por los callejones. Otros estaban encadenados. Me dijeron que estos perros no eran para consumo humano y eran “mascotas” de la gente. ¿Pero cuál era la diferencia entre estos individuos y los perros en las jaulas que iban a ser asesinados pronto? ¿Con qué criterio deciden los humanos quién debe ser enjaulado y en algún momento comido, y quién debe ser perdonado y tenido como mascota?»&nbsp;</em><a href="http://www.sinvoz.org/investigators-diary-2/" target="_blank"><em>Diario del investigador</em></a></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/animalequalityuk/albums/72157630067365847"><img src="https://farm8.staticflickr.com/7215/7187802001_8b447ef0c4_z.jpg" alt="Dog Meat Trade - China"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Visita <a href="https://www.sinvoz.org/" target="_blank">www.SinVoz.org</a> y descubre todos los detalles de la investigación en mataderos de perros en China.</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">*Petición finalizada. Gracias a todos los que habéis colaborado</h4>
<!-- /wp:heading -->