<!-- wp:image {"id":12538} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/10/26markpost0708.jpg" alt="" class="wp-image-12538"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Mark Post, el científico artífice de la primera hamburguesa presentada en 2013 a partir de «carne limpia» o carne elaborada a partir de células, considera &nbsp;que el actual sistema de producción de carne cambiará profundamente o desaparecerá. <strong>La ganadería industrial es una práctica obsoleta, responsable del mayor maltrato animal en la historia y destructora del medioambiente</strong> que, además, no podrá satisfacer la demanda mundial de carne en las próximas cuatro décadas.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12539} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/10/36372759453_a9ffd1f276_z.jpg" alt="" class="wp-image-12539"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>Noticia relacionada: <a href="https://www.lavanguardia.com/comer/al-dia/20170828/43884301610/bill-gates-invierte-en-la-industria-de-la-carne-limpia.html" target="_blank">Bill Gates invierte en la industria de la «carne limpia»</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«La mayoría de las personas desconocen que la producción cárnica actual está llegando a su límite y no podrá atender la demanda creciente en las próximas cuatro décadas. Necesitamos encontrar una alternativa, y esa alternativa podría ser esta: una producción ética y respetuosa con el medioambiente». Así lo afirmó Post en un video que se transmitió en el congreso Crea Tech, organizado por la Asociación Argentina de Consorcios Regionales de Experimentación Agrícola en Córdoba, Argentina.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><font color="#808080"><p>La industria de la carne de pollo en India. Investigación de Igualdad Animal.</p></font>


<strong>La solución a la mayor amenaza de nuestro tiempo: el cambio climático</strong>

<p>&nbsp;</p>

<p>La «carne limpia» podría ser la receta que salve al planeta al lograr revolucionar un devastador sistema de producción que <strong>le cuesta al planeta nada más y nada menos que el 14,5% de las emisiones totales de gas de efecto invernadero</strong> y un desmesurado derroche de recursos. En contraste brutal, la «carne limpia» disminuye en un 90% el monóxido de carbono producido por la industria ganadera y en un 99% el uso de tierras para cultivo y pastoreo. &nbsp;</p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>Según Post, <strong>en tres o cuatro años este producto podría llegar a los restaurantes y en siete a las estanterías de los supermercados. </strong>El científico también considera que la «carne limpia» dará la oportunidad a que lo productores migren a una nueva forma de producir o de la ganadería a la agricultura.</p>

<p>Ya existen en el mundo 8 empresas dedicadas al desarrollo de la tecnología para producir «carne limpia» e <strong>inversores de renombre siguen volcando su interés en ella</strong>, conscientes de que es, sin duda, la solución a los mayores desafíos actuales: alimentar a &nbsp;9.700 millones de personas en 2050 y luchar contra el cambio climático.</p>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://www.lanacion.com.ar/economia/campo/un-experto-que-creo-la-carne-cultivada-desafia-a-las-vacas-gran-parte-de-la-produccion-cambiara-o-desaparecera-nid2071615/" target="_blank">https://www.lanacion.com.ar/economia/campo/un-experto-que-creo-la-carne-cultivada-desafia-a-las-vacas-gran-parte-de-la-produccion-cambiara-o-desaparecera-nid2071615/</a></p>

<p><a href="http://www.agrositio.com/vertext/vertext.php?id=189792&amp;se=1000" target="_blank">http://www.agrositio.com/vertext/vertext.php?id=189792&amp;se=1000</a></p></font></font>
<!-- /wp:html -->