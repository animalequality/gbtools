<!-- wp:paragraph -->
<p>La comisión internacional EAT-Lancet ha reunido a más de 30 científicos de todo el mundo para idear y publicar la ‘dieta de salud planetaria’. Una nueva forma de alimentación para el ser humano que ha estudiado tanto la comida normalmente consumida por miles de millones de personas y sus efectos positivos para reducir el cambio climático.

Esta primera dieta científica consistiría en recortar el consumo de carne roja en occidente pero, sobre todo, abordaría cambios radicales en todo el mundo. Ya que además de proporcionar alimentos nutritivos a la población, al mismo tiempo intenta abordar la ganadería como uno de los factores que más afectan al medio ambiente, como la destrucción de la vida silvestre y el aumento de la contaminación del agua de nuestro planeta.

<strong>Te puede interesar:&nbsp;<a href="https://igualdadanimal.org/blog/comiendo-menos-carne-y-lacteos-salvamos-al-planeta/" target="_blank" rel="noopener">Comiendo menos carne y lácteos salvamos al planeta</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13546} -->
<figure class="wp-block-image"><img src="/app/uploads/2019/01/portada-noticia.jpg" alt="" class="wp-image-13546"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>¿En que consiste?</strong>

La dieta que proponen los expertos <strong>está principalmente basada en plantas, y requiere reducir a la mitad las cifras de consumo de carne roja </strong>y azúcar en el mundo, duplicando así el consumo de verduras, frutas, legumbres y frutos secos. Esta medida evitaría hasta 11 millones de muertes al año causadas por una alimentación poco saludable, al tiempo que reduciría el impacto medioambiental que su producción supone.

Y es que los datos demuestran que, con 10 mil millones de personas que se espera que habiten la Tierra para 2050, continuar con una alimentación insostenible no solo agravaría los problemas de salud, sino que se llegaría a provocar un calentamiento global severo.

Según publica ‘The Guardian’, las academias científicas del mundo publicaron recientemente que el sistema alimentario está roto. Y es que <strong>la ganadería industrial no solo afecta al bienestar de los animales de granja</strong>, también supone un efecto devastador para el medioambiente, no solo por la emisión de grandes cantidades de metano que afectan al calentamiento del planeta, sino también por la deforestación de bosques que ello supone.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><strong>¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación?</strong></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><strong>&nbsp;<a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;">¡Suscríbete gratuitamente a nuestro e-boletín!</span></a></strong></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong>¿Cómo puede implementarse?</strong>

‘The Lancet’ reconoce en su publicación la dificultad que tendrá esta dieta para imponerse entre la sociedad mundial. “Alcanzar este objetivo requerirá la rápida adopción de numerosos cambios y una colaboración y compromiso global sin precedentes: una gran transformación de los alimentos”.

Pero también apuntan que en unas zonas del planeta se requerirán más esfuerzos que en otros. Por ejemplo, <strong>en EEUU debería reducirse el consumo de carne roja en un 84%</strong> y debería aumentar seis veces más en legumbres. En el caso de Europa, debería comerse un 77% menos de este tipo de carne y aumentar hasta 15 veces el consumo de frutos secos y semillas para entrar en los parámetros de esta nueva dieta ‘transformadora’.

“Las dietas del mundo deben cambiar drásticamente” apunta Walter Willett, profesor de la Universidad de Harvard y uno de los autores del informe. Según los editores de ‘The Lance’, “la civilización está en crisis. Ya no podemos alimentar a nuestra población con una dieta saludable mientras equilibramos los recursos planetarios. Si podemos comer de una manera que funcione para nuestro planeta y nuestros cuerpos, el equilibrio natural se restaurará.</p>
<!-- /wp:paragraph -->