<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10204" src="/app/uploads/2015/01/lili1.jpg" style="float:right; margin:10px; width:450px">Lili es una pequeña pata recién nacida que llega a la granja en la que vivirá.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Qué le sucederá?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pero esta historia que te llegará al corazón no es sólo la historia de Lili. Es también la historia de pequeñas terneras, de cerditos, de millones de pollitos y otras aves… La historia de los animales destinados a nuestra alimentación.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Millones y millones de ellos. La historia de su vida en las granjas modernas. ¿Qué sentirán?, ¿soñarán con una vida mejor?, ¿qué nos dirían si pudieran hablar? Al fin y al cabo, muchos de ellos son sólo bebés cuando son enviados al matadero.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Conoce a Lili, la pequeña bebé de pata que se convierte en este vídeo en la embajadora de los animales de granja. No la olvidarás.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/71tym6DgitY" width="885px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->