<!-- wp:image {"id":10407} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/1453305407_672076_1453305727_noticia_normal_recorte1.jpg" alt="" class="wp-image-10407"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-12526" src="/app/uploads/2017/10/lechones.jpg" style="float:left; margin-left:10px; margin-right:10px; width:350px">Mientras uno se lanzaba salvajemente sobre ellos con todo su peso, el otro lo grababa con el móvil. <strong>Así mataron a 72 pequeños cerdos aún en edad lactante</strong>. Después difundieron las escenas a través de Whatsapp.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>La Guardia Civil está investigando los hechos</strong>, que constituyen un delito de maltrato animal. Ambos trabajadores ya han sido identificados. La granja se encuentra en la provincia de Almería.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según la información con la que cuenta el Seprona, los empleados de la granja aprovecharon el momento en el que trasladaban a los animales hacia otro módulo para cerrarles el paso; <strong>de tal forma que los aterrorizados bebés no tenían&nbsp;ninguna escapatoria posible.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En ese momento uno de ellos empezó a grabar las imágenes. El segundo saltó hasta en tres ocasiones, matando hasta 19 cerdos en el acto <strong>por aplastamiento</strong>. Los 53 restantes murieron poco después, como consecuencia de las <strong>lesiones traumáticas</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ambos trabajadores han sido puestos a disposición del Juzgado de Instrucción de Huércal-Overa para prestar declaración.</p>
<!-- /wp:paragraph -->