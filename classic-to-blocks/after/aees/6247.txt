<!-- wp:paragraph -->
<p>El Gobierno canadiense dijo hoy que defenderá la caza comercial de focas después de que Rusia y otros países de la antigua Unión Soviética anunciasen una propuesta para reducir la comercialización de productos derivados de este mamífero.

"Nuestro Gobierno sigue comprometido a defender el sector de productos de foca de Canadá", afirmó el ministro de Comercio Internacional de Canadá, Ed Fast, a través de un comunicado.

"He ordenado a los funcionarios canadienses para que de forma activa expliquen a sus homólogos internacionales las preocupaciones de Canadá sobre estas restricciones propuestas y examinar las opciones para asegurar el continuado acceso a los mercados del sector de productos de foca de Canadá", añadió Fast.

Canadá dijo hoy que Rusia, Kazajistán y Bielorrusia han propuesto restricciones al comercio de productos derivados de focas que proceden de la caza comercial que se produce cada año en la primavera frente a las costa atlántica de Canadá.

La propuesta de los tres países de la antigua Unión Soviética se suma a la prohibición de importación de productos canadienses derivados de la caza comercial de focas impuesta por la Unión Europea (UE).

Canadá ha presentado una queja contra la UE ante la Organización Mundial del Comercio (OMC).

El Parlamento Europeo justificó su decisión de prohibir la importación de productos derivados de la caza comercial canadiense de focas al entender que los cazadores utilizan prácticas consideradas inhumanas para matar a los mamíferos marinos.

Cada año, los cazadores canadienses matan unas 350.000 focas, una actividad que desde hace décadas ha sido duramente criticada por organizaciones de defensa de los derechos animales. EFE</p>
<!-- /wp:paragraph -->