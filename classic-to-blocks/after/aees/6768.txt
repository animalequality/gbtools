<!-- wp:paragraph -->
<p><strong>Puñaladas, machetazos, quemaduras y golpes</strong> son algunas de las agresiones que sufren de forma continuada muchos animales en Cartagena, Colombia.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13823} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/10/burro_herido.jpg" alt="" class="wp-image-13823" title="Uno de los burros heridos"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según denuncia el director de la Guardia Ambiental, Roberto Ruiz,&nbsp;<em>«<strong>los más afectados son los burros</strong>, dado que en menos de cuatro días hemos recibido al menos cinco de estos animales con graves heridas, sobre todo con cortes y chuzadas profundas».&nbsp;</em></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13825} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/10/burros_maltratados.jpg" alt="" class="wp-image-13825"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«Uno de los casos más lamentables es el de una burrita que estaba recién parida y fue recuperada en momentos en que niños de un sector de Nariño la estaban atacando con punzones e incluso se llevaron a su cría pero no sabemos para dónde”, </em>afirma Ruiz.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las investigaciones realizadas hasta el momento indican que <strong>muchas de estas lesiones son causadas por adolescentes y jóvenes</strong> que utilizan a los animales como blanco para practicar técnicas de pelea con armas blancas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los animales torturados permanecen en la sede de la Guardia Ambiental de Cartagena, donde reciben atención por parte de médicos de la Unidad Municipal de Asistencia Técnica Agropecuaria (Umata).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->