<!-- wp:image {"id":12575} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/10/not-co-equipo-770x385_1.jpg" alt="" class="wp-image-12575"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>¿Es posible que podamos seguir comiendo todo lo que nos gusta pero sin que ningún animal haya tenido que estar implicado en el proceso? La respuesta es un sí rotundo. <strong>Una gran cantidad de empresas en todo el mundo lo está haciendo posible</strong> y lo está llevando a niveles que no habríamos podido imaginar.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/07/27/varias-empresas-ya-producen-carne-real-libre-de-crueldad/" target="_blank">Varias empresas ya producen carne real libre de crueldad</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En Chile está <a href="https://www.notco.com:443/" target="_blank">The Not Co</a>, un emprendimiento creado por científicos que busca <strong>replicar tanto el sabor como la textura y el perfil nutritivo de los productos de origen animal más consumidos</strong>. Y todo esto, por supuesto, sacando a los animales de la línea de producción.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Te preguntarás cómo lo logran y la verdad lo han hecho de una forma verdaderamente innovadora. Van al nivel químico y <strong>buscan moléculas que están en las plantas que tengan las mismas características de los ingredientes de origen animal</strong>. Para esto se valen de Guiseppe, un programa de inteligencia artificial que crear nuevas combinaciones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«No cambiamos lo que comemos. Cambiamos la forma en que se hacen los alimentos».</p></font>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/09/13/impossible-foods-comienza-producir-454000-kilos-de-hamburguesas-vegetales-al-mes/" target="_blank">Impossible Foods comienza a producir 454.000 kilos de hamburguesas vegetales al mes</a></strong>

<p>&nbsp;</p>

<p>Hasta el momento, The Not Co ha logrado un rotundo éxito con su mayonesa sin huevo «Not Mayo», disponible en 120 supermercados de Chile y considerada la mejor mayonesa vegetal del mercado. Y, según su Gerente de desarrollo, Camila Sepúlveda, <strong>cuentan con una gama de productos ya desarrollados que solo esperan escalar la producción a nivel industrial e internacional</strong>. Entre ellos se encuentran leche, leche cultivada, yoghurt (natural y griego), queso, un mousse de chocolate y, más adelante, irán más allá de las alternativas a los lácteos y huevos creando salchichas, pollo y bistec.</p>

<figure><img alt="" class="wp-image-12576" src="/app/uploads/2017/10/not_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p><strong>Todos los productos de The Not Co son veganos, sin ingredientes transgénicos y sin preservantes</strong>. Su ambición es tan grande como su lema «Nosotros democratizamos la comida saludable» porque su competencia es directamente con las grandes empresas de alimentos a nivel mundial. Y grandes o, mejor dicho ¡inmensos! son los beneficios que este nuevo sistema de producción de alimentos: menos sufrimiento animal, destrucción medioambiental y mejor salud.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Fuentes: <a href="https://pousta.com/the-not-co-veganismo-emprendimiento/" target="_blank">https://pousta.com/the-not-co-veganismo-emprendimiento/</a></p></font>
<!-- /wp:html -->