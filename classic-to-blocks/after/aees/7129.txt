<!-- wp:image {"id":10129} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/03/974224-42fa0c50-b468-11e3-993f-a57163efdfb6.jpg" alt="" class="wp-image-10129"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":13578} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/03/0010826114.jpg" alt="" class="wp-image-13578"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La administración del zoológico de Copenhague que causó indignación internacional el pasado frebero cuando mató a una jirafa por “razones genéticas”, es de nuevo el blanco de críticas por el sacrificio de cuatro leones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El zoológico danés indicó&nbsp; que no podía seguir manteniendo a dos leones viejos y sus dos crías porque introduciría un nuevo león macho. “Debido a la naturaleza y el comportamiento de los leones, el zoo realizó una eutanasia a los dos leones viejos y a los dos leones jóvenes que no tenían edad suficiente para defenderse”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/EeswfdWXiK8" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según el semanario danés The Copenhagen Post, los cuatro mamíferos carnívoros sacrificados la víspera, mediante una inyección letal, fueron los mismos que se alimentaron de la jirafa Marius. Hace un mes el zoológico mató a la jirafa Marius, de dos años, y la descuartizó frente al público para luego arrojar su cadáver a dichos leones. Este hecho causó indignación mundial, y puso de relieve una practica cotidiana en los zoológicos pero que no era conocida por el público.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Precisamente para la organización Animal Rights Sweden, el caso de Marius puso de relieve lo que hacen los zoológicos regularmente con los animales. “No es ningún secreto que los animales son asesinados cuando ya no hay espacio, o si los animales no tienen genes que son lo suficientemente interesantes”, dijo la organización en un comunicado. “La única manera de parar esto es no visitar los zoológicos”, añadió. Señaló que algunos zoológicos trabajan para preservar las especies de animales, pero nunca a individuos. “Cuando las lindas crías de animales que atraen a los visitantes crecen, ya no son tan interesantes”, dijo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->