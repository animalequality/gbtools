<!-- wp:paragraph -->
<p>El alto tránsito de botes en Bahía Drake e Isla del Caño (Costa Rica) afecta negativamente los comportamientos de la población de delfines de la zona.

La alimentación y el reposo son las actividades más alteradas, según un estudio realizado por la bióloga Andrea Montero de la Universidad de Costa Rica (UCR).

La investigación incluyó muestreos durante la época seca del 2004 al 2006 y abarcó un área aproximada de 260 kilómetros cuadrados, cuyos vértices fueron Punta Sierpe, Llorona e Isla del Caño.

“Durante el reposo, los delfines disminuyen los latidos del corazón y el cuerpo entra en un metabolismo reducido que les permite acumular energía, la cual será esencial para reproducirse y alimentarse eficientemente”, explicó Montero.

“A lo largo del muestreo no se vio reposo en presencia de botes turísticos y había más probabilidad que se alimentaran en presencia del bote de investigación que cuando había dos o tres botes”, destacó la investigadora.

Sociables. Los delfines son sociables y su forma de comunicación es por sonidos. “La acústica para ellos es como la vista para nosotros. La usan para conseguir comida, socializar, coordinar actividades, ubicarse”, explicó Montero.

Para ello, utilizan dos tipos de llamados: clics y silbidos. Los primeros, son sonidos de alta frecuencia que usan para explorar el ambiente. “Cuando se habla de ecolocalización es cuando el delfín envía un clic y, con base al eco que recibe, puede interpretar qué es lo que hay. Igual sucede con una presa, puede identificar hasta el tamaño y forma que tiene”, manifestó la bióloga.

Los silbidos son más usados para comunicarse. En este sentido, ayudan a coordinar el grupo para cazar, para reaccionar frente a un depredador y para la relación madre–hijo.

Respecto a la alimentación, podría existir un enmascaramiento debido a que “el sonido de los motores coincide en algún ámbito de las frecuencias de las señales acústicas de los delfines; lo que provoca que busquen otros lugares en donde alimentarse sin tener que invertir más energía durante la ecolocalización de sus presas”, destacó Montero.

Recomendaciones. El avistamiento de ballenas y delfines genera cerca de un millón de dólares anuales en Península de Osa. Para regular la actividad, desde el 2006 entró en vigencia el Reglamento para regular observación de ballenas y delfines (Decreto Ejecutivo 32495).

El estudio sugiere incluir disposiciones para controlar lo que sucede bajo el agua. En este sentido, la investigadora recomienda añadir regulaciones en cuanto al tipo de motor y caballaje.

“Un motor de cuatro tiempos es menos ruidoso. En cuanto al caballaje, en investigaciones realizadas en Manzanillo y Bocas del Toro se vieron menos reacciones negativas ante aproximaciones de botes que tenían menos de 50 caballos de fuerza. Sin embargo, sólo un manejo responsable del bote permitirá reducir reacciones negativas por parte de los delfines”, explicó la investigadora.

Asimismo, recomienda dar un incentivo a aquellas personas o empresas que sigan las disposiciones del decreto e inviertan en sus botes.

Eso podría hacerse a manera de certificación, de forma que les ayude para mercadearse y facilite el control.

El 66% de los operadores de viajes turísticos ya han apostado voluntariamente por el cambio y han demostrado tener la voluntad para autoregularse.

Además de la UCR, este estudio contó con el patrocinio de MICIT-CONICIT, Rufford Foundation, Society of Marine Mammalogy y Fundación Keto.

Fuente: nacion.com</p>
<!-- /wp:paragraph -->