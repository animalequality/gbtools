<!-- wp:image {"id":11368} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/shutterstock_262406051.jpg" alt="" class="wp-image-11368"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>La manera en que concebimos la alimentación está cambiando</strong> y los Millenials (jóvenes nacidos entre 1981 y 1995) tienen mucho que ver con eso.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En general, <strong>a los consumidores de todas las edades nos preocupa el bienestar de los animales y queremos llevar vidas más saludables</strong> pero en cuanto a elegir alternativas más sanas y sin <a href="https://igualdadanimal.org/noticia/2016/04/14/el-peor-maltrato-animal-conocido-se-produce-en-las-granjas-y-mataderos/" target="_blank">maltrato animal</a> los más jóvenes son los líderes en esta decisión.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Los millennials son actualmente la generación de consumidores más grande e influyente en todo el mundo y sus necesidades son cada vez más tomadas en cuenta</strong> por la industria para crear nuevas formas de impulsar la producción, incluída, por supuesto, la de alimentos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según el estudio de sustentabilidad de 2015 de Nielsen <a href="http://www.nielsen.com/content/dam/nielsenglobal/dk/docs/global-sustainability-report-oct-2015.pdf" target="_blank">[1]</a> el 45% de los encuestados dijeron tener un compromiso con el <a href="https://igualdadanimal.org/blog/7-razones-por-las-que-la-ganaderia-industrial-es-un-desastre-ecologico/" target="_blank">medioambiente</a> y por eso prefieren consumir alimentos de medios y procesos sostenibles.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De igual manera, <strong>esta generación también está consumiendo menos carne y otros productos derivados de animales porque también les preocupa su salud y el bienestar de los animales</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11369} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/shutterstock_188491196.jpg" alt="" class="wp-image-11369"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>A lo largo de los años el vegetarianismo ha sido una tendencia creciente en el mundo pero los millennials avanzaron más en este sentido que cualquiera de las generaciones anteriores</strong>. Un estudio del Grupo Hartman <a href="http://store.hartman-group.com/content/millennials-2011-overview.pdf" target="_blank">[2]</a> reveló que el 12% de los milennials son «vegetarianos fieles», en comparación con el 4% de los Generación X (35-49 años) y el 1 % de los Boomers (50-64 años).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De acuerdo con otro estudio <a href="https://www.packagedfacts.com/Collegiate-Gen-Eating-6576735/" target="_blank">[3]</a> sobre demandas universitarias, más estudiantes manejan sus dietas dentro del espectro de menos carne a no comer nada de carne y <strong>el 42 % de los que no comen carne están entre las edades de 18 a 34 años, es decir, la generación milenaria</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Resulta alentador que las nuevas generaciones también se preocupen por el bienestar de los animales y el medioambiente pero lo cierto es que <strong>todos los consumidores de diferentes edades tenemos el poder de cambiar el mundo</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Muchos de nosotros aún no somos conscientes del <a href="https://igualdadanimal.org/noticia/2016/03/23/las-profundas-consecuencias-de-un-modelo-alimentario-basado-en-vegetales/" target="_blank">impacto</a> que cada una de nuestras decisiones de consumo tiene en el planeta <strong>y es que debemos saber que solo nosotros podemos revolucionar la industria de alimentos demandando productos que sean respetuosos con los animales y el medioambiente y rechazando aquellos que no lo son</strong>. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«La ganadería industrial no solo es responsable del 15% del total de las emisiones de gas de efecto invernadero a nivel mundial sino que condena a miles de millones de animales a una vida de sufrimiento».</p></font>

<p>&nbsp;</p>

<p>Cuando elegimos un producto libre de maltrato animal y que es además de origen sostenible estamos contribuyendo a que menos animales sufran y a salvar el planeta.</p>

<p>La diferencia entre construir y destruir la marcamos cuando elegimos un producto en lugar de otro. En nuestras manos está construir el mundo que queremos o dejar que todo siga igual. Y tú, ¿qué eliges?</p>

<p><strong>Anímate a conocer cómo puedes ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete a nuestro e-boletin</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación</strong>.</p>

<p>&nbsp;</p>

<p>[1]&nbsp;<a href="http://www.nielsen.com/content/dam/nielsenglobal/dk/docs/global-sustainability-report-oct-2015.pdf" target="_blank">http://www.nielsen.com/content/dam/nielsenglobal/dk/docs/global-sustainability-report-oct-2015.pdf</a></p>

<p>[2] <a href="http://store.hartman-group.com/content/millennials-2011-overview.pdf" target="_blank">http://store.hartman-group.com/content/millennials-2011-overview.pdf</a></p>

<p>[3] <a href="https://www.packagedfacts.com/Collegiate-Gen-Eating-6576735/" target="_blank">https://www.packagedfacts.com/Collegiate-Gen-Eating-6576735/</a></p></font>
<!-- /wp:html -->