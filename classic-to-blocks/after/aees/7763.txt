<!-- wp:image {"id":11907} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/04/foto_fb_21.jpg" alt="" class="wp-image-11907"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":11908} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/04/ar_investigation.jpg" alt="" class="wp-image-11908"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El secretario de estado para asuntos económicos en Holanda, Martijn Van Dam <strong>quiere instalar cámaras en los mataderos del país para que la Autoridad Holandesa de Seguridad Alimentaria (NVWA) pueda evitar el maltrato animal.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La iniciativa surge apenas tres meses luego de que el parlamento francés aprobara la instalación de cámaras de seguridad en 250 mataderos del país debido a las imágenes de crueldad extrema captadas por la organización L214 Étique &amp; Animaux. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En este caso ha sido en reacción al escándalo provocado por <a href="https://dearkitty1.wordpress.com/2017/03/23/massive-animal-abuse-in-belgian-corporate-slaughterhouse/" target="_blank">imágenes de una investigación</a> que la organización belga <a href="https://www.animalrights.nl/" target="_blank">Animal Rights</a> ha sacado a la luz y que muestran abusos terribles en un matadero de Bélgica.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><font color="#808080"><p>© Animal Rights</p></font>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>En ellas se ve cómo <strong>cerdos son pateados, arrastrados por su orejas y abiertos con cuchillos o siendo ahogados mientras están conscientes</strong>. Luego de esto, el matadero, que también surtía a carniceros holandeses, fue cerrado indefinidamente.</p>

<p>La organización señaló a sesenta carniceros holandeses que compraban carne a este matadero. Algunos de ellos <strong>ostentaban etiquetas de calidad específica en sus productos, las cuales, desde el escándalo, han sido revocadas.</strong></p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p>«Resulta incomprensible que las personas que trabajan allí sean culpables de hacer o permitir que esto ocurra», declaró Van Dam.</p></font>

<p>&nbsp;</p>

<figure><img alt="" class="wp-image-12265" src="/app/uploads/2017/07/shutterstock_386878810_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>VanDam definió lo registrado como «repugnantes imágenes» y quiere instalar cámaras en los mataderos de Holanda para así poder evitar incidentes similares y asegurar que las normas sobre bienestar animal sean respetadas.</p>

<p>La industria misma se ha mostrado muy receptiva ante la propuesta y la Asociación de Mataderos y de Compañías Procesadoras de Carne también piensa que las cámaras son una buena idea.</p>

<p>Frans Wouters, el presidente de esta última, considera que con este monitoreo a distancia con cámaras que sustituirá las inspecciones físicas se reducirán costos ya que estas cuestan a los mataderos más que toda su fuerza de trabajo.</p>

<p>Según Van Dam, legalmente se puede obligar a los mataderos a instalar las cámaras pero es preferible que la medida se implementada de forma voluntaria ya que esto simplificaría las cosas para todos. Sin embargo, no descartó que pudieran convertirse en un requisito legal.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://nltimes.nl/2017/03/29/put-cameras-slaughterhouses-monitor-abuse-says-dutch-cabinet" target="_blank">https://nltimes.nl/2017/03/29/put-cameras-slaughterhouses-monitor-abuse-says-dutch-cabinet</a></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p></font></font></font>
<!-- /wp:html -->