<!-- wp:image {"id":11329} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/marco_antonio_regil.jpg" alt="" class="wp-image-11329"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Las verdaderas estrellas brillan por ser la voz de quienes más lo necesitan.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ellas y ellos son admirados y queridos por sus fans, están constantemente en el ojo público y afortunadamente ¡también les preocupan los animales!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Conoce a las celebridades latinas que son su voz y están promoviendo importantes cambios para ellos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¡Son unos verdaderos héroes para los animales!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>1. Marco Antonio Regil</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El presentador de televisión mexicano <strong>es un defensor de los animales</strong> desde que en 2008 decidiera sacar la carne de su dieta. Desde entonces Marco dice que tiene más energía que nunca y se ve y siente años más joven.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Recientemente presentó junto a Igualdad Animal en México el proyecto pionero de realidad virtual <a href="https://ianimal.es/mx/" target="_blank">iAnimal</a> el cual coloca al espectador en el lugar de animales de granja y expone cómo son explotados por la industria.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11330} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/marco_antonio_regil_0.jpg" alt="" class="wp-image-11330"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>2. Patricia de León</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La actriz, modelo y presentadora de televisión panameña que ha conquistado tanto el mercado latino como el estadounidense <strong>es vegetariana por convicción</strong> ya que se declara defensora de los animales. Ha participado en varias campañas globales de concienciación. «Hacerme vegetariana me convirtió en un mejor ser humano, al respetar la vida de otros seres vivientes, ¡y me convirtió en una persona mucho más saludable!», nos dice Patricia. ¡Y estamos de acuerdo!</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11331} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/patricia_de_leon_2.jpg" alt="" class="wp-image-11331"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><p>© SBukley</p></font>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>3. Rubén Alarbarán</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Rubén, a quien se le conoce también por ser el líder de la reconocida banda mexicana Café Tacuba, <strong>es un activo promotor de la alimentación vegetariana</strong> que nos anima a todos a cambiar la vida de millones de animales y proteger al planeta desde nuestros platos. ¡Bien, Ruben!</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11332} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/ruben_albarran.jpg" alt="" class="wp-image-11332"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><p>© El Universal, Archivo.</p></font>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>4. Natalia Villaveces</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La polifacética estrella colombiana (nada mas y nada menos que presentadora de TV, escritora, productora y blogger) afirma, «una vez que hice la conexión entre amar a un animal y comer a otro animal fue cuando todo hizo click en mi mente». Desde entonces Natalia ha adoptado una alimentación basada en vegetales y ha participado en diferentes campañas para promoverla.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11333} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/natalia_3.jpg" alt="" class="wp-image-11333"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><p>© nataliavillaveces.com</p></font>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>5. Rodrigo Sánchez y Gabriela Quintero</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El integrante del dúo musical mexicano «Rodrigo y Gabriela» es, al igual que su compañera, un trotamundos que no para de alzar su voz para llamar la atención <strong>sobre el impacto que la industria ganadera tienen en los animales, el medioambiente y la salud</strong>. Para Rodrigo el asunto es muy simple: «al disfrutar de una alimentación basada en vegetales, puedes prevenir la crueldad contra los animales, mejorar tu salud y proteger nuestro planeta». Rodrigo y Gabriela han participado en eventos a favor de lo animales que incluyen desde campañas hasta conciertos en la vía pública.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11334} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/rodrigo_y_gabriela.jpg" alt="" class="wp-image-11334"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><p>© qthemusic.com</p></font>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Anímate a conocer cómo puedes ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete a nuestro e-boletin</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación</strong>.</p>
<!-- /wp:paragraph -->