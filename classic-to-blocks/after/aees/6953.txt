<!-- wp:image {"id":9943} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/03/cerdo_china_rio.jpg" alt="" class="wp-image-9943" title="El cadáver de un cerdo flota entre desperdicios en el río"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Más de <strong>3.300 cadáveres de cerdos</strong> han aparecido flotando en los últimos días en el principal río de Shanghái en China. Según la prensa local, los criadores decidieron deshacerse así de los animales muertos durante el invierno en las explotaciones al no poder vender su carne a traficantes ilegales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según un campesino de la zona, <em>«los cerdos muertos aparecieron intactos el primer día en que fueron abandonados, pero <strong>sus patas desaparecieron al día siguiente</strong>»</em>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ahora, la preocupación de la población está centrada en si partes de los cadáveres hallados habrán sido puestos a la venta en los mercados. <em>«No sabemos quién cogió las patas de los cerdos ni adónde han ido a parar, pero <strong>sería horrible si al final esa carne llegara a nuestras mesas</strong>»</em>, afirma un vecino.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>También se está midiendo la <strong>calidad calidad del agua potable </strong>de la ciudad, ya que proviene del mismo río.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Advertencia. Contenido gráfico.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe allowfullscreen="" frameborder="0" width="698" height="573" scrolling="no" id="molvideoplayer" title="MailOnline Embed Player" src="https://www.dailymail.co.uk/embed/video/1001902.html"></iframe>
<!-- /wp:html -->