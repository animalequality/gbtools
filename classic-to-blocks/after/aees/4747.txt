<!-- wp:paragraph -->
<p>La pasada nochevieja, una babuina que vivía encerrada permanentemente en un circo fue rescatada de su jaula por simpatizantes de la organización Igualdad Animal. Ahora, algo más de dos meses después de aquella noche -tiempo dejado por la propia seguridad del animal rescatado- recibimos en nuestra oficina, en exclusiva y de forma anónima, un comunicado explicando la vida que había tenido esta babuina y el motivo de este rescate. El comunicado iba acompañado de varias imágenes de ella una vez rescatada con las que hemos editado el siguiente vídeo:
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://player.vimeo.com/video/7002625?color=00c4ff&amp;title=0&amp;byline=0&amp;portrait=0" width="640" height="512" frameborder="0" allowfullscreen="allowfullscreen"></iframe><a href="https://vimeo.com/7002625" target="_blank" rel="noopener noreferrer">Rescate de Moses, una babuína explotada en un circo</a> on Vimeo.

<a href="https://vimeo.com/5637791" target="_blank" rel="noopener noreferrer"><strong>Click here to watch the English version of this video</strong></a>

Igualdad Animal no se hace responsable de las acciones que simpatizantes de la organización puedan realizar a título individual. Nosotros nos hacemos eco de esta acción porque consideramos que es un acto de justicia hacia un individuo que era explotado y nos alegramos de que ahora sea libre.

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div>

COMUNICADO SOBRE EL RESCATE DE LA BABUÍNA MOSES DE UN CIRCO

La pasada nochevieja, cinco minutos antes de las campanadas, y coincidiendo con el momento en que todos los trabajadores del Circo Roma Dola -entonces instalado en San Sebastián de los Reyes- estaban en una carpa del circo celebrando la entrada del año 2009, tres individuos entramos en el remolque donde se hallaba la jaula de una babuina que dicho circo tenía esclavizada. Esa noche que ponía fin al año 2008, también pondría fin a muchos años de privación de libertad, sufrimiento y desolación. Dos mil nueve sería un nuevo año de esperanza y libertad para ella. Tras abrir los cerrojos de la jaula y ponerle una correa que facilitase poder salir con ella de aquel lugar sin perder tiempo, la babuina salió de su celda corriendo delante nuestro hacia su libertad, dejando atrás a cada paso la que había sido su cárcel durante los últimos años y subiendo ella misma al vehículo que la alejaría de aquel lugar para siempre.

Los esclavos humanos del siglo XIX se encontraron con el racismo de una época que les llevó al sometimiento en base a su diferente color de piel. En nuestra época actual, esta babuina fue víctima del especismo que permitió que se la utilizase en contra de su voluntad como parte de un espectáculo y, posteriormente, cuando ya no cumplía esa función, como "mascota" y reclamo publicitario.

La babuina, a quien hemos decidido llamar Moses en referencia a una antigua esclava humana liberada, ha sido llevada muy lejos de sus explotadores, tan lejos que nunca la encontrarán y ni podrán volver a esclavizarla. No tendrá que temer por los castigos ni tendrá que volver a repetir los comportamientos absurdos que le habían obligado a aprender para diversión de algunos humanos. Actualmente vive en un lugar adecuado a sus necesidades, siendo respetada y protegida por personas humanas responsables que procuran que tenga una vida lo más plena y feliz posible.

Esta babuina, al igual que los demás animales que permanecen dominados por los seres humanos en diversos ámbitos, merecía ser libre y disfrutar de su vida sin ser utilizada para que otros se entretengan a su costa. Su rescate, lejos de lo que se ha afirmado, ha consistido en un acto de justicia y solidaridad hacia ella.

Los medios de comunicación han difundido las declaraciones realizadas por el dueño del circo en su versión de los hechos. Ahora somos quienes realizamos dicho rescate quienes exponemos la realidad de los mismos para evidenciar la diferencia entre lo que es cierto y lo que han afirmado.

• El dueño del circo, imaginamos que en un intento de evitarse problemas y responsabilidades legales, ha mentido públicamente al decir que en el circo había un vigilante encargado de la seguridad las veinticuatro horas del día. Realmente de noche, al igual que en algunos momentos del día, no había nadie, tal y como sabe todo vecino o vecina de San Sebastián de los Reyes que se haya acercado al mismo lugar. Podríamos haber rescatado a la babuina cualquier otra noche si bien, en nochevieja, era más seguro para nosotros y para ella.

• Pudimos entrar con facilidad al remolque en el que se encontraba la babuina sin tener que romper el candado de la puerta como han afirmado algunos medios, pues, al igual que sucedía las noches anteriores en que habíamos estado vigilando, dejaban el candado colocado en la puerta pero sin cerrar. Esto se puede comprobar con el propio vídeo de la noticia en que se ve el candado abierto en la mano del dueño del circo todavía intacto. Cabe añadir que si el candado hubiese estado cerrado no hubiéramos dudado en romperlo, porque no podemos permitir que un objeto impida ser libre a alguien.

• A pesar de las afirmaciones del explotador, no fue necesario administrar ningún sedante a Moses, bastó con abrir su jaula -cerrada con simples cerrojos-, y ponerle una correa que facilitase poder salir con ella de aquel lugar sin perder tiempo. Cuando salimos juntos de su cárcel, ella misma iba corriendo delante nuestro hacia su libertad, dejando atrás su celda a cada paso y subiendo al vehículo que la alejaría de aquel lugar para siempre.

• Este rescate no responde a un interés explotador por comerciar con ella, ni mucho menos se trata de una gamberrada o un robo como algunos seguramente han pensado. Moses fue premeditadamente rescatada de la situación de explotación en que se encontraba por su propio bien, en base a sus intereses. Desgraciadamente hay quienes sacan a unos animales de unas jaulas para llevarlas a otras y seguir beneficiándose a costa de ellos. No es el caso. Nosotros nos oponemos a los circos con animales y al comercio y venta de éstos porque la esclavitud es injusta independientemente de la especie a la que pertenezca quien la padece. No comerciamos con animales ni consideramos que éstos sean propiedad de nadie que pueda ser comprada, vendida ni robada.

• Otra afirmación errónea que se ha difundido es la de que ella "sólo respeta a su cuidador", refiriéndose de este modo al individuo encargado de mantenerla presa en el circo. Si ella no atacaba a su esclavista -que no "cuidador"- no era porque le respetase sino porque tenía miedo al castigo que recibiría si intentaba rebelarse. Su cuello todavía tiene las cicatrices de los eslabones de la cadena que se le clavaban en el cuello con cada tirón que le daban para dominarla.

• Los medios han presentado a la babuina como agresiva y peligrosa, incluso mostrando imágenes de otros babuinos atacando a crías pequeñas o con ellas ya muertas -a pesar de que Moses no tenía hijos ni relación alguna con otros babuinos-. Nos gustaría aprovechar esta errónea presentación para plantear una reflexión: ¿cuáles son los motivos por los que, en caso de ser cierto, un animal que estuviese en un circo se comportaría de forma agresiva? ¿No sería quizás por la desesperación y trastornos psicológicos que sufriría por haber tenido que pasar toda su vida encerrada en una jaula del tamaño de tu sofá, obligado a realizar trucos estúpidos para evitar castigos y mayores privaciones?

Cuando su contacto con los humanos ha sido el que ha tenido, es lógico esperar que la víctima reaccione ante ellos de forma adversa. Sin embargo, tal y como se puede apreciar en las imágenes que enviamos, su actitud con nosotros no ha sido agresiva. Moses ha confiado en nosotros tanto como para dejarnos cuidarla, jugar con ella, acariciarla, examinar su cuerpo en busca de heridas, caminar con ella de la mano y quitarle la cadena que había tenido atada a su cuello durante prácticamente toda su vida. Nada que ver, desde luego, con la imagen que se ha pretendido transmitir de ella.

Por último, queremos dedicar esta acción tanto a todos los no humanos que continúan presos en los circos como a los humanos que están presos por defender a los demás animales. Pongámonos en el lugar de estos y otros animales que son explotados, miremos más allá de la especie a la que pertenecen y de las diferencias que tenemos con ellos. No es justo que sigamos quitándoles su libertad y su vida.

Toma partido ante la explotación de los demás animales, hazte vegano / vegana y defiéndelos.

Contra la esclavitud animal, contra el especismo. Por el fin de los circos con animales, por el fin de la explotación animal.

</div>
<!-- /wp:html -->