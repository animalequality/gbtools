<!-- wp:image {"id":10144} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/05/3137736249_6de412cb9b_b.jpg" alt="" class="wp-image-10144"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":13645} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/05/3138687420_c745dcec11_b.jpg" alt="" class="wp-image-13645"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Parlament ha aprobado esta mañana, con los votos a favor de PSC, CiU, Esquerra, Iniciativa y la CUP, la tramitación de la prohibición de los espectáculos de circo con animales. Han votado en contra PP y Ciutadans. La norma que desde 2010 prohíbe las corridas de toros en Cataluña, se ha ampliado para incluir la prohibición de los circos con animales. En Cataluña ya existían 99 municipios que impedían la instalación de circos con animales en su territorio.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La proposición de ley fue impulsada por la campaña CLAC (Catalunya Lliure d’Animals en Circs) de la <a href="http://liberaong.org/">Asociación Animalista LIBERA!</a> y la Fundación Franz Weber.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta proposición de ley fue registrada el pasado 29 de octubre como respuesta al compromiso preelectoral adquirido por ERC, ICV y CiU a través de la firma del manifiesto de la campaña CLAC (21 de noviembre de 2012), en el que los partidos se comprometieron a incorporar un nuevo punto en la Ley de protección de los animales de Catalunya que prohibiera los circos con animales en todo el territorio catalán a lo largo de esta legislatura.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Previamente al primer debate celebrado hoy en el Parlament, los únicos partidos que presentaron enmiendas a la totalidad de la proposición fueron PP y C’s. Sin embargo, ERC, CiU, ICV, PSC y CUP han apoyado la proposición, sumando cerca del 80% de los votos de la cámara (101 contra 27). Por tanto, el resultado de este debate de totalidad permite continuar la tramitación de la proposición de ley, que pasará a debatirse en comisión.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->