<!-- wp:paragraph -->
<p>Si se suspende el ingreso en prisión de los responsables <a href="https://www.youtube.com/watch?v=xMHGoTMi9uE">de las torturas</a>, <strong>Igualdad&nbsp;Animal recurrirá la decisión</strong>.

<strong>Este martes, a las 10 horas, en el juzgado de lo penal Nº3 de Cartagena</strong> ha tenido lugar el juicio oral por delito de maltrato a animales domésticos a dos ex-trabajadores (los otros dos acusados se encuentran en fuga) de la granja de cerdos El Escobar, situada en Fuente Álamo, Murcia.

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://www.flickr.com/photos/igualdadanimal/sets/72157629363051319/" target="_blank" rel="noopener noreferrer">Imágenes de la investigación: Granja 'El Escobar' (Murcia)</a></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><strong>Aviso: Estas imágenes pueden herir su sensibilidad</strong>
&nbsp;

Javier Moreno, director internacional de Igualdad Animal, ha indicado a los medios de comunicación a la salida del Juzgado que «a pesar de ser insuficiente, es la mayor pena posible contemplada por la ley actual, una de las mayores condenas a maltrato a animales de granja en España», y que «recurrirán si la jueza suspende el ingreso en prisión» ya que «es uno de los casos más crueles de maltrato animal de la historia reciente de España. Estos maltratadores tienen que ir a prisión».

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">IGUALDAD ANIMAL Y ‘SALVADOS’
<span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.org/salvados-igualdad-animal/" target="_blank" rel="noopener noreferrer">El reportaje que hizo temblar los cimientos de la industria cárnica española</a></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
&nbsp;

Daniel Dorado, abogado de Igualdad Animal en el caso, ha indicado que «<strong>es necesaria una reforma del Código Penal</strong> para que casos como este supongan la entrada en prisión de los maltratadores».

El caso de las torturas continuadas a cerdos en la granja El Escobar está siendo ampliamente cubierto por los medios españoles. Asimismo, periódicos internacionales como <a href="http://www.dailymail.co.uk/news/article-3809362/The-pigs-experience-pain-human-s-Barbaric-video-shows-Spanish-farm-workers-beating-pregnant-pigs-metal-bars-stabbing-rusty-swords-RIPPING-piglets-wombs.html">Daily Mail</a> y <a href="https://www.mirror.co.uk/news/world-news/evil-farm-workers-use-swords-8922556">Mirror</a> también se han hecho eco del caso.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/xMHGoTMi9uE" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe><strong><a href="https://igualdadanimal.org/campana/el-escobar/">Más de 85.000 personas han firmado ya la petición</a></strong> de Igualdad Animal solicitando el ingreso en prisión de los maltratadores. En las redes sociales de Igualdad Animal los vídeos del caso han llegado a más de<strong> 3 millones de usuarios</strong>.</p>
<!-- /wp:paragraph -->