<!-- wp:image {"id":11976} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/toro_escape_matadero.image_.jpg" alt="" class="wp-image-11976"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Seis toros escaparon de un matadero ubicado al norte de Saint Louis, Estados Unidos, al irrumpir por una de las puertas no seguras de la empresa Star Packing Co.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Entre ellos se encontraba <strong>Chico, un imponente toro de apenas dos años de edad que se negó rotundamente a ser atrapado</strong>. Por más de una hora Chico y sus cinco compañeros interrumpieron el tráfico y trotaron por las calles de los vecindarios dando bastante trabajo a las fuerzas policiales.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11977} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/toro_escape_matadero_004.jpg" alt="" class="wp-image-11977"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Durante la persecución, tanto los oficiales de control de animales como la Humane Society of Missouri estuvieron presentes para colaborar con la policía.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Luego de que cinco de lo toros fueran atrapados, Chico logró fugarse por segunda vez tras haber sido acorralado en la Residencia de las Hermanas de los Pobres. Rompió una valla y reinició la persecución por 2 horas más a lo largo de más de un kilómetro y medio hasta que finalmente fue capturado.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><font color="#808080"><p>© David Carson</p></font>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Puedes suscribirte gratuitamente a nuestro e-boletín</a> para que recibas las mejores noticias de actualidad sobre los animales y conozcas más opciones de alimentación.</p></font>

<p>&nbsp;</p>

<figure><img alt="" class="wp-image-11978" src="/app/uploads/2017/05/toro_escape_matadero_005.jpg" style="margin: 0px 10px; float: left; width: 450px; "></figure><p></p>

<p>La determinación de Chico en luchar por su libertad despertó la compasión de los espectadores y motivó esfuerzos para que todos los toros fueran salvados del matadero.</p>

<p>Una de las iniciativas consistió en impulsar el rescate de los animales a través de una campaña para recolectar fondos para su traslado a un refugio de animales de granja y su manutención. Para <strong>el mismo lunes ya habían sido recaudados 16.500 dólares</strong>. &nbsp;</p>

<p>Susie Coston, la directora de <a href="https://www.farmsanctuary.org/" target="_blank">Farm Sanctuary</a>, un refugio para animales de granja con sede en Nueva York, <strong>se ofreció a recibir a los seis valientes fugitivos</strong> y dijo que tanto ella como el líder de la organización de rescate Skyland viajarían hasta Saint Louis para buscarlos.</p>

<p>&nbsp;</p>

<font size="1"><font color="#808080"><p>© David Carson</p></font>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p>«Es tan increíblemente triste, solo están corriendo para salvar su vida», dijo Coston.</p></font>

<p>&nbsp;</p>

<p>Mientras tanto, a Chico y a sus cinco aguerridos camaradas les toca aún esperar por dos semanas en el refugio Gentle Barn en Tenessee antes de hacer su viaje hasta Farm Sanctuary.</p>

<figure><img alt="" class="wp-image-11979" src="/app/uploads/2017/05/toro_escape_matadero_003.image_.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 451px; "></figure><p></p>

<p>&nbsp;</p>

<p>Luego de que fueran rescatados, Jay Weiner, el director de Gentle Barm afirmó: «Están asustados… Han pasado por muchas cosas. Pero están destinados a las cosas buenas».</p>

<p>Y estamos seguros de eso. A partir de ahora, Chico y sus compañeros <strong>no volverán a conocer lo que es el maltrato</strong> y vivirán vidas plenas en libertad y siendo respetados.</p>

<p>Muchos animales aún permanecen dentro de las granjas y mataderos siendo víctimas del mayor maltrato hacia los animales que existe en el mundo. Pero tú puedes hacer mucho por ellos. Prueba comenzar a dejarles fuera de tu plato incorporando alternativas a la carne que son nutritivas y deliciosas.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Fuente: <a href="http://www.stltoday.com/news/local/crime-and-courts/cattle-that-escaped-from-st-louis-slaughterhouse-are-headed-to/article_fde14374-866c-54da-a96a-c9363ba1cc31.html" target="_blank">http://www.stltoday.com/news/local/crime-and-courts/cattle-that-escaped-from-st-louis-slaughterhouse-are-headed-to/article_fde14374-866c-54da-a96a-c9363ba1cc31.html</a></p></font></font></font></font>
<!-- /wp:html -->