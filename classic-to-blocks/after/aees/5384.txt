<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font size="3"><font color="#000000"><font face="Times New Roman">El Rey de España mostró hoy su apoyo al asesinato y tortura de toros y caballos en la entrega de los <span style="mso-bidi-font-size: 7.5pt">Premios Universitarios y Trofeos Taurinos de 2009 otorgados por la Real Maestranza de Caballería de Sevilla.</span></font></font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font size="3"><font color="#000000"><font face="Times New Roman"><span style="mso-bidi-font-size: 7.5pt">Juan Carlos no es el único aficionado a la tauromaquia de la familia real. Su madre, la Condesa de Barcelona, también lo era, así como su hija la Infanta Elena. El Rey acude siempre que puede a la plaza de tortura de toros de Las Ventas, así como a otras actividades que implican divertirse a costa del sufrimiento y la muerte de animales, como la caza.</span></font></font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font size="3"><font color="#000000"><font face="Times New Roman"><span style="mso-bidi-font-size: 7.5pt">En su intervención durante la entrega de los citados premios, el Rey felicitó a los toreros triunfantes en la pasada Feria de Abril y distinguidos con los Trofeos Taurinos 2009 por "la esencia del buen lance" del que, indicó, "nace un mundo cultural y artístico fecundo".</span></font></font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font size="3"><font color="#000000"><font face="Times New Roman"><span style="mso-bidi-font-size: 7.5pt">Don Juan Carlos destacó la remodelación de la "maravillosa" Plaza de Toros de la Maestranza que, minutos antes, acababa de inaugurar tras las obras de modernización de Sol Alto y Sombra Alta.<o:p></o:p></span></font></font></font></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13944} -->
<figure class="wp-block-image"><img src="/app/uploads/2010/03/espana-toros-san-isidro-toros-san-isidro-15599x0.jpg" alt="" class="wp-image-13944"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>
	<font size="3"><font color="#000000"></font></font></p>
<!-- /wp:paragraph -->