<!-- wp:paragraph -->
<p><img alt="" class="wp-image-13718" src="/app/uploads/2014/10/P09-67632.jpg" style="width: 306px; float: left; margin: 3px 10px;">Este sábado 11 oct. 11:30 hrs. No podemos permanecer impasibles ante la infame gestión de los responsables políticos de la Sanidad española y madrileña. Su incompetencia se ha cobrado ya la vida de Excalibur y la extrema gravedad de Teresa. Los ciudadanos decentes debemos salir a la calle a reclamar la inmediata dimisión de Ana Mato, Francisco Javier Rodríguez y Julio Zarco.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>TODAS LAS CONVOCATORIAS A LAS 11:30 de la mañana de este sábado 11 de Octubre (menos León que será a las 13:00).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>A CORUÑA</strong>: Obelisco</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>ALBACETE</strong>: Plaza del Altozano</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>ALICANTE</strong>: Plaza de la Montañeta</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>BADAJOZ</strong>: Av. de Huelva (Delegación del Gobierno)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>BARCELONA</strong>: Deleg. del Gobierno, Calle Mallorca, 278</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>CÁDIZ:</strong> Plaza del Ayuntamiento</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>CACERES:</strong> Kiosko de Paseo Cánovas</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>CORDOBA:</strong> Boulevard de la Avenida Gran Capitán (frente al Ayuntamiento viejo)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>GIRONA:</strong> Delegacion del gobierno en Av. Jaume I</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>LAS PALMAS:</strong> Plaza de la Feria</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>LEON</strong>: Calle Ancha – Botines (OJO, a las 13:00)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>MADRID:</strong> Plaza de Callao</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>MÁLAGA:</strong> plaza constitución (final de calle Larios)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>OVIEDO:</strong> Plaza de la Escandalera</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>OURENSE:</strong> de Plaza Mayor a Ministerios Sanidad</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>PAMPLONA:</strong> Paseo Sarasate (frente Juzgados)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>SANTIAGO DE COMPOSTELA</strong>: Plaza do Obradoiro</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>SEGOVIA:</strong> Fernández Ladreda, junto al Acueducto</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>SEVILLA</strong>: Plaza Nueva (frente Ayuntamiento)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>TOLEDO:</strong> Plaza de Zocodover</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>VALENCIA</strong>: Plaza del Ayuntamiento</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>ZARAGOZA:</strong> Gran Vía nº9</p>
<!-- /wp:paragraph -->