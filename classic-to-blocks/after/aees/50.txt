<!-- wp:paragraph -->
<p>El 14 de febrero del presente año apareció en el diario <em>La Industria</em> de Trujillo (Perú) la noticia de que en el establo <em>Montecristo</em> las vacas explotadas para la producción de leche habían contraído brucelosis, una enfermedad infecciosa que además de afectar gravemente a animales no humanos tales como las cabras, vacas, puede afectar a su producción de leche, y  transmitirse a los animales humanos a través de los productos lácteos no pasteurizados, o simplemente al estar en contacto con los mencionados animales.El 14 de febrero del presente año apareció en el diario <em>La Industria</em> de Trujillo (Perú) la noticia de que en el establo <em>Montecristo</em> las vacas explotadas para la producción de leche habían contraído brucelosis, una enfermedad infecciosa que además de afectar gravemente a animales no humanos tales como las cabras, vacas, puede afectar a su producción de leche, y  transmitirse a los animales humanos a través de los productos lácteos no pasteurizados, o simplemente al estar en contacto con los mencionados animales.

El propietario y explotador de las vacas lamentaba mucho que sus vacas estuviesen enfermas y estaba tomando las "medidas necesarias" para erradicar la enfermedad. Sin embargo su preocupación no se centraba en el sufrimiento padecido por sus esclavas debido a la enfermedad (no olvidemos que estas padecían ya de antemano debido a la privación de libertad, explotación, inseminación artificial, etc.) sino en las pérdidas económicas que podían derivarse si esta enfermedad afectaba la capacidad de producción de leche de las mismas.
 
Por ello, el dueño del establo concluía que la "solución" era sacrificar (asesinar) tanto las vacas enfermas como aquellas que presentasen problemas al parir aún no siendo víctimas de la enfermedad (la enfermedad es fácilmente contagiable a través del parto). Como el mismo ganadero declaraba en la entrevista: </p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<cite>Vaca con problemas, aunque los resultados den negativo, irá al "gancho"</cite>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>, es decir, al matadero. Claro está que toda vaca acabará en el matadero sea por contagio de brucelosis o debido al descenso de su producción de leche, ya que este es el verdadero rostro de dicha industria: el sufrimiento y la muerte.</p>
<!-- /wp:paragraph -->