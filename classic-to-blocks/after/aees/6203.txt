<!-- wp:paragraph -->
<p>El próximo sábado 26 de noviembre, a las 12 horas, más de 200 activistas protestaremos frente a la granja de visones situada en la localidad madrileña de Santa María de la Alameda, la única que existe en la comunidad de Madrid. Durante estas fechas se produce el gaseamiento masivo de visones, y se calcula que en el estado español son matados aproximadamente 300.000 visones por la industria peletera. Los activistas, que iremos vestidos de negro en señal de luto por las víctimas animales, permaneceremos&nbsp;en&nbsp;silencio durante aproximadamente dos horas, mostrando&nbsp;imágenes&nbsp;de&nbsp;animales despellejados&nbsp;obtenidas&nbsp;durante&nbsp;los&nbsp;tres&nbsp;años&nbsp;que&nbsp;ha estado&nbsp;investigando Igualdad Animal las&nbsp;granjas&nbsp;de&nbsp;visones&nbsp;del estado español.<br>
<br>
Asimismo, leeremos un manifiesto recordando a los animales que estarán siendo gaseados estos días y en el que realizaremos un llamamiento a la sociedad, al avance de la ética hacia un mundo donde dejen de existir lugares como las granjas donde son explotados y matados los animales.<br>
<br>
La&nbsp;protesta, que cuenta con la colaboración de la Fundación Equanimal, &nbsp;forma&nbsp;parte&nbsp;de&nbsp;la&nbsp;campaña que en&nbsp;Igualdad&nbsp;Animal&nbsp;venimos&nbsp;realizando&nbsp;en&nbsp;los&nbsp;últimos&nbsp;años&nbsp;contra&nbsp;el&nbsp;uso&nbsp;de animales&nbsp;como&nbsp;vestimenta&nbsp;en&nbsp;la&nbsp;que,&nbsp;entre&nbsp;otras&nbsp;acciones,&nbsp;hemos saltado&nbsp;por tres años&nbsp;consecutivos&nbsp;a&nbsp;los&nbsp;desfiles&nbsp;de&nbsp;la&nbsp;Pasarela&nbsp;Cibeles&nbsp;y&nbsp;hemos sacado a luz imágenes inéditas tanto de la explotación en las granjas como del gaseamiento de los visones. Los resultados de las investigaciones que hemos realizado a la industria peletera española pueden verse en : <a href="http://www.GranjasDeVisiones.es">http://www.GranjasDeVisiones.es</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Vídeo de la protesta del 2010:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://player.vimeo.com/video/17562977?color=00c4ff&amp;title=0&amp;byline=0&amp;portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><a href="https://vimeo.com/17562977">Concentración frente a una granja de visones.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dentro de esta campaña, nuestro fin&nbsp;es&nbsp;promover&nbsp;el respeto&nbsp;hacia&nbsp;todos&nbsp;los&nbsp;animales&nbsp;en&nbsp;general&nbsp;y&nbsp;conseguir&nbsp;la&nbsp;prohibición&nbsp;de las&nbsp;granjas&nbsp;de&nbsp;visones&nbsp;en&nbsp;particular,&nbsp;prohibición&nbsp;que&nbsp;países&nbsp;como Inglaterra&nbsp;o&nbsp;Austria&nbsp;ya&nbsp;han&nbsp;llevado&nbsp;a&nbsp;cabo.<br>
<br>
Igualdad&nbsp;Animal&nbsp;es&nbsp;una&nbsp;organización&nbsp;internacional&nbsp;de&nbsp;derechos&nbsp;animales presente&nbsp;en&nbsp;España,&nbsp;Inglaterra, Polonia,&nbsp;y&nbsp;Venezuela<br>
<br>
Contacto&nbsp;de&nbsp;medios<br>
Javier Moreno |&nbsp;Portavoz&nbsp;de&nbsp;Igualdad&nbsp;Animal<br>
Teléfono:&nbsp;691 054 172<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 674 216 012</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Correo&nbsp;electrónico:&nbsp;info@igualdadanimal.org<br>
<br>
&nbsp;</p>
<!-- /wp:paragraph -->