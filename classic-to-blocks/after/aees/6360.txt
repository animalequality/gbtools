<!-- wp:paragraph -->
<p>La universidad del Norte de Texas abrió recientemente un <strong>servicio completo de cafetería vegano</strong> en sus campus, provocando el aplauso de activistas por los derechos animales, medioambientalistas y, por supuesto, estudiantes veganos. Aunque los campus universitarios en la nación han estado ofreciendo opciones veganas durante algunos años, la cafetería de la Universidad del Norte de Texas parece ser l<strong>a primera exclusivamente vegana</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-9381" src="/app/uploads/2012/02/vegan_texas.jpg" style="width: 220px; float: left;">El menú de esta cafetería no contiene productos de origen animal, como carne, leche y huevos, y en lugar de ellos hay <strong>sopas vegetarianas</strong>, <strong>paninis </strong>y <strong>sushi vegetariano</strong>. Los servicios del comedor de la universidad dicen que hasta ahora algunos de los estudiantes que comen allí no son necesariamente veganos, sino que simplemente quieren comer de forma más saludable.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¡Y estos estudiantes no están solos! Una encuesta realizada en 2004 sobre el servicio de comida del proveedor Aramark mostró que <strong>uno de cada cuatro estudiantes encuestados quería la opción del menú vegano </strong>en los campus.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Keith Ayoob, director de la clínica de nutrición en el Centro de Evaluación y Rehabilitación de Niños en la Universidad de Medicina Albert Einstein dice “Mucha gente joven experimenta: bebidas, drogas… ¿Por qué no una nueva forma de comer?”</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A primera vista, la dieta vegana parece bastante más sana que la típica dieta universitaria. Pero dietistas advierten que los estudiantes que no eligen sabiamente sus frutas y verduras&nbsp; pueden estar perdiendo vitaminas y nutrientes, como proteínas, hierro y vitamina B12. “Simplemente seguir una dieta vegana no garantiza que estés comiendo mejor” Dice Ayoob. “Puede haber comida basura vegana también”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según&nbsp; Diekman, director de nutrición de la Universidad de Washington en St.Louis. “La comida vegetariana, y la vegana, pueden ser muy nutritivas si las personas están educadas para hacer las elecciones correctas y reunir sus necesidades nutritivas”</p>
<!-- /wp:paragraph -->