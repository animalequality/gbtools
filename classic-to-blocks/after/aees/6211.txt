<!-- wp:image {"id":9212} -->
<figure class="wp-block-image"><img src="/app/uploads/2011/11/dsc_1134.jpg" alt="" class="wp-image-9212"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
	Equanimal e Igualdad Animal convierten la entrada del Palacio de Congresos de Valencia en un Cementerio Animal en protesta contra la experimentación con animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Galería fotográfica: http://www.flickr.com/photos/igualdadanimal/sets/72157628146566473</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Hoy viernes 25 de noviembre, a las 11 horas, las organizaciones en defensa de los animales Equanimal e Igualdad Animal convirtieron la entrada del Palacio de los Congresos de Valencia en un "cementerio animal" instalando cien cruces en protesta contra la experimentación animal. Este acto coincidió con la clausura del XI Congreso Nacional de la Sociedad Española para las Ciencias del Animal de Laboratorio (SECAL) que se ha venido desarrollando estos días en el Palacio de Congresos de Valencia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Cada cruz llevaba la foto de un animal víctima de experimentos y representó a las millones de víctimas de la experimentación animal. Con este acto se quiere provocar un debate en la sociedad sobre la explotación de los animales en general y sobre la experimentación con animales en particular. Según datos del Ministerio de Medio Ambiente y Medio Rural y Marino, un total de 1,40 millones de animales murieron en los laboratorios españoles en 2009, lo que representa un aumento del 56,3 por ciento respecto al año anterior. Esta cifra no refleja el número real de víctimas de la experimentación con animales en España, dado que en dicho informe no se contabiliza a los animales invertebrados que son también capaces de sentir y con intereses propios, por lo que la cifra exacta ha de ser necesariamente mucho mayor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Pensamos que el avance de la ciencia no puede estar justificado a toda costa, y apostamos por un avance de la ética. Por una ciencia sin víctimas. Se debe invertir en las alternativas a la experimentación con animales, y cuestionar un avance científico que discrimina a los animales por el simple hecho de no pertenecer a nuestra especie. Un prejuicio denominado especismo, análogo a otras discriminaciones arbitrarias como el sexismo o el racismo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Igualdad Animal y Equanimal somos dos organizaciones por los derechos animales que trabajamos para denunciar las injusticias que padecen los animales y luchar por sus derechos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Contacto de Medios</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Alba García Bernal&nbsp; -&nbsp; Tel: 646 250 649 &nbsp;</p>
<!-- /wp:paragraph -->