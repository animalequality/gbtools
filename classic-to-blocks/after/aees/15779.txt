<!-- wp:paragraph -->
<p>Recientemente <a href="https://www.nature.com/articles/s41586-018-0594-0.epdf?referrer_access_token=ho1eoiL3LRdv3FPGiRBcD9RgN0jAjWel9jnR3ZoTv0M2ZckU8PFAjFp2beHrcOXhHVPwiboHFjCLaVoEktOYmui1E_WbtCgUiW0GQ_D1Zsf9ikhdqzS9hAlJ2OYj63T9paYkbkbjbwbiMDtAsaaLIStmp4pVAf5k2WAYS1T_ySVFXQamjSmbZZOqgcbZWrei-uyC6A_FVQNeiIMKlcXtpL1l3TMhhDAAQ6xoEU683VobvCaltof2ZDm9A_x8ewm3UaNgfGWJBRg5RRTJSRnuMB4hvOnbZwNuDKrxhCeRw3cQjyhjTS8OGrNWaP3KdZud&amp;tracking_referrer=www.theguardian.com" target="_blank" rel="noopener">un estudio</a> reveló que grandes reducciones de carne son necesarias si queremos evitar la catástrofe a las que nos llevarían los efectos del cambio climático.

Siendo la ganadería industrial responsable del 15% de la emisión de gases de efecto invernadero, derroche de agua, tierras y desigualdad alimentaria, queda muy claro que un cambio en la forma en que producimos nuestros alimentos nos es una opción sino algo absolutamente necesario si queremos asegurar nuestra subsistencia en el planeta.

<strong>Te interesará:&nbsp;<a href="https://igualdadanimal.org/noticia/2018/07/19/empresas-carnicas-y-lacteas-sobrepasan-la-industria-petrolera-como-las-mayores/"> Empresas cárnicas y lácteas sobrepasan a la industria petrolera como las mayores contaminantes</a></strong>

El estudio fue publicado poco días después del&nbsp;<a href="https://www.ipcc.ch/sr15/" target="_blank" rel="noopener">informe</a> del Panel Intergubernamental sobre el Cambio Climático de la ONU (IPCC), en el cual los científicos más destacados del mundo hicieron «un último llamado» <strong>para alertar sobre que solo nos quedan 12 años para mantener el límite de temperatura global en 1,5 Cº </strong>y evitar que los riesgos de sequía, inundaciones y calor extremo (todos ellos consecuencias del cambio climático), se eleven significativamente.

A pesar de la emergencia, <a href="http://changingmarkets.org/wp-content/uploads/2018/10/Growing-the-Good-report-v3.pdf" target="_blank" rel="noopener">un reporte</a> publicado por las organizaciones Changing Markets Foundation, Mighty Earth, and Compassion in World Farming puntualizó que en todo el mundo <strong>las políticas gubernamentales están casi todas a favor del crecimiento de la ganadería industrial </strong>y, en algunos casos, están obstaculizando el desarrollo de&nbsp;la producción de alternativas a la carne y otros productos animales que además de sostenibles son más sanos y libres de crueldad hacia los animales.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente a nuestro e-boletín</a></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Berenice Dupeux, responsable de políticas agrícolas de la Oficina Europea del Medio Ambiente declaró con respecto al informe del IPCC que «este informe es una evidencia más de que para limitar el calentamiento global a 1,5 grados, el sector agrícola debe reducir significativamente la producción de carne y productos lácteos para reducir las emisiones globales, al igual que muchas otras industrias están haciendo».

Dupeux instó a los líderes mundiales a «no hacer la vista gorda» ante el cambio climático y a abordarlo dentro de la reforma actual de la Política Agrícola Común de la UE. «Los ministros de fincas y los eurodiputados tienen la obligación moral de poner los derechos de las generaciones futuras en primer lugar», concluyó.

<a href="https://www.onegreenplanet.org/news/world-governments-failure-address-meat-overconsumption-destroying-environment/" target="_blank" rel="noopener">Fuente</a></p>
<!-- /wp:paragraph -->