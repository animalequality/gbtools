<!-- wp:image {"id":9664} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/08/a1.jpeg" alt="" class="wp-image-9664"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La Corte de Magistrados de Norwich ha condenado Geoffrey Towell, de 54 años, de East Harling, Norfolk, a <strong>18 semanas de cárcel e inhabilitación para trabajar con animales durante 10 años</strong>. James Dove, de 27 años y vecino de Wymondham, Norfolk, ha recibido una pena de<strong> prisión de ocho semanas, además de la suspensión para cualquier actividad ganadera durante cinco años.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El juez de distrito Peter Veits dijo que fue <strong>uno de los peores casos de crueldad deliberada que había visto.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Jonathan Eales, fiscal, dijo que los cargos más graves fueron para Towell debido a que golpeó en la cabeza a tres cerdos, hasta la muerte, con una barra de metal. A una de las cerdas, incluso, la golpéo 59 veces seguidas. También fue acusado de utilizar una fuerza excesiva para mover a los lechones y tirar de ellos, retorciéndoles las orejas y arrojándolos al suelo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El tribunal escuchó a Dove, el tractorista encargado de a ayudar a Towell con los 300 cerdos de la granja en los momentos de "dificultades sistemáticas". Dove fue grabado pateando a los animales y lanzándolos por encima de una barrera.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Towell admitió cinco y Dove dos de los cargos que se les imputaban por causar un "sufrimiento innecesario" a los cerdos de la granja, incumpliendo sistemáticamente la Ley de Bienestar Animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allow="autoplay; encrypted-media" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/pQK4261GXyg" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ian Fisher, el abogado de Dove, dijo: <em>«Algunas de las cosas que hizo las hizo de forma impulsiva, instintiva o copiando a otros.»</em> Y agregó: &nbsp;<em>«<strong>La vergüenza pública que está viviendo es ya una forma de castigo</strong>».</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Días después de salir a la luz la investigación de Igualdad Animal, Stephen Brown, de 52 años y <strong>propietario de la explotación</strong>, fue encontrado muerto con un disparo en la cabeza. <strong>Un médico forense dictaminó que se había suicidado con una escopeta.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En ese sentido, Jamieson Plummer, abogado de Towell, dijo: <em>«Ha tenido que cargar con el peso de la muerte del propietario, que le ha afectado y deprimido</em>.» &nbsp;Plummer solicitó al juez que reconsiderara la prohibición de trabajar en explotaciones ganaderas, ya que Towell perdería su trabajo y su hogar, adosado a la granja.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En su sentencia, el juez Veits afirmó: <em>«<strong>Se trata de crímenes horribles. </strong>Es fácil culpar a los que ya no están aquí para responder a los cargos. El Sr. Towell se comportó sin mostrar empatía alguna ni atención a los cerdos bajo su responsabilidad. <strong>Ustedes golpearon a los cerdos repetidas veces y los mataron uno tras otro.»&nbsp;</strong></em>Dijo que Dove había jugado un &nbsp;«<em>papel menor»</em>, pero que no mostró empatía alguna hacia los cerdos. Dove deberá completar 180 horas de trabajo social no remunerado.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/animalequalityuk/albums/72157628605983581"><img src="https://farm8.staticflickr.com/7018/6588360171_9831ce9da5_z.jpg" alt="Harling Farm (A. J. Edwards and Son) - East Harling (Norfolk)"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p> <script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->