<!-- wp:paragraph -->
<p>Berlín - La unidad de cuidados intensivos pediátricos del hospital de Munster atendió el pasado fin de semana a un bebé, con la diferencia de que este bebe se llamaba Mary Zwo y era un gorila que recibió tratamiento por deshidratación e hipotermia.

[img_assist|nid=2863|title=|desc=|link=none|align=left|width=144|height=165] La pequeña primate, hallada el sábado semiinconsciente en brazos de su madre en el "zoológico" de Munster (Berlin) fue ingresada en cuidados intensivos en el hospital ese mismo día.
 
A pesar de que el bebé gorila recibió un trato privilegiado en comparación con los padecimientos que sufren las crías de otras especies, su destino, "el zoológico" de Berlin, nos hace recordar las terribles diferencias que existen hoy día entre el trato que acordamos entre humanos (y el que acordamos para nuestros bebés) y el que reciben los demás animales por nuestra parte, viendo éstos últimos su vida supeditada a nuestros gustos y deseos. Mary Zwo fue atendida en un hospital como un miembro más de nuestra comunidad moral, en cambio fue devuelta a una cárcel donde diarmente encerramos a miles de animales.</p>
<!-- /wp:paragraph -->