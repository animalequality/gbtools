<!-- wp:paragraph -->
<p>Londres - La ex mujer de Paul McCartney se ha hecho vegana. Al contrario de otras famosas, que eliminan la carne de sus dietas para guardar la línea, la nueva alimentación de Heather están motivados por causas ideológicas, según ella misma afirma al periódico Británico Daily Mail. 

[img_assist|nid=2778|title=|desc=|link=none|align=left|width=130|height=93] Heather quiere aprovechar su fama como ex mujer del antiguo miembro de los Beatles para apoyar el veganismo, “algo muy importante para mí, ya que detesto la crueldad”, dice.

“Seguir una dieta basada en plantas no sólo beneficia la salud considerablemente, sino que ayuda al planeta, los animales y la alimentación del mundo”, subrayó Heather McCartney.

Según explicó en unas declaraciones que publica Daily Mail, hay muchos problemas de salud que están vinculados con la leche de vaca, como las alergias. Heather asegura que su decisión es la correcta. “No podría volver a comer carne, pescado o productos lácteos”, dijo.</p>
<!-- /wp:paragraph -->