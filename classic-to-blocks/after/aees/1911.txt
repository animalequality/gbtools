<!-- wp:paragraph -->
<p>Girona - Un incendio que se declaró esta mañana en una “granja” de Cabanes (Girona) acabó con la vida de unos 500 cerdos, según informaron los Bomberos de la Generalitat.

El fuego se inició sobre las 7 horas por causas que aún se desconocen y afectó a cables y plásticos de la granja. A consecuencia del humo, unos 500 cerdos que había en el interior de la nave murieron.

Fuente: Terra.es</p>
<!-- /wp:paragraph -->