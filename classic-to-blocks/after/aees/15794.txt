<!-- wp:paragraph -->
<p>Dhul, empresa lider en postres en España, ha confirmado a Igualdad Animal que <a href="https://www.lavanguardia.com/vida/20181029/452622651534/dhul-dejara-de-usar-huevos-de-gallinas-enjauladas-para-elaborar-sus-flanes.html" target="_blank" rel="noopener">todos sus productos que contengan huevo serán libres de jaula</a> a finales de este año 2018.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13002} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/04/623898432267586.ijdyvjccensm5ulsjkr2_height640_0.png" alt="" class="wp-image-13002"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Dhul se suma así a las más de 500 empresas en todo el mundo que ya han asumido el compromiso de dejar de usar huevos de gallinas enjauladas. Entre ellas se encuentran las conocidas Lidl, ALdi, Kraft Heinz, Sodexo, Aldi, Mondelez Internacional o las españolas Mercadona, Carrefour, el Corte Inglés o Dulcesol entre otras.

«La producción de huevos procedentes de gallinas enjauladas es uno de los sistemas más crueles con los animales en la ganadería industrial. Aunque libre de jaula no implica libre de maltrato, es indispensable que las empresas adopten un papel relevante para reducir el sufrimiento extremo que padecen millones de animales. &nbsp;En esta línea, el compromiso de Dhul&nbsp;es un paso muy importante para poner fin al sufrimiento de las gallinas enjauladas en la producción de huevos en España. La ciudadanía demanda políticas de bienestar animal a las empresas y es muy buena noticia que las empresas se alineen con esta creciente sensibilización», indicó Javier Moreno, director de Igualdad Animal.

Igualdad Animal está trabajando sin descanso para conseguir que la crueldad de las&nbsp;jaulas pase a formar parte del pasado.

<a href="https://igualdadanimal.org/defensores-animales/" target="_blank" rel="noopener">Únete al equipo de defensores animales</a> para ayudarnos a hacerlo posible.</p>
<!-- /wp:paragraph -->