<!-- wp:image {"id":10207} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/01/5809680657_92d2d91294_z.jpg" alt="" class="wp-image-10207"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":13925} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/01/delfin.jpg" alt="" class="wp-image-13925"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La Secretaría de Medio Ambiente y Recursos Naturales (Semarnat) indicó que a partir de hoy martes 27 de enero, queda prohibida la utilización de ejemplares mamíferos marinos en espectáculos itinerantes en México.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Este lunes lo han anunciado en el Diario Oficial de la Federación, donde se detalla el decreto por el que se añade un párrafo al Artículo 60 Bis de la Ley General de Vida Silvestre.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se agrega el siguiente párrafo "queda prohibida la utilización de ejemplares de mamíferos marinos en espectáculos itinerantes” y entrará en vigor a partir de mañana.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Otro párrafo incluye una observación similar sobre los primates, en el que también prohíbe su captura para fines científicos y educativos y sólo pueden estar sujetos a actividades de restauración, repoblamiento y de reintroducción de estas especies en su hábitat natural.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El artículo 55 bis refuerza la protección tanto de mamíferos marinos como de primates al prohibir cualquier acto de exportación e importación de estas especies.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La nueva adición a la Ley General de Vida Silvestre, sólo hace específico que los especímenes de mamíferos marinos no podrán ser utilizados para actividades de entretenimiento itinerante. Las asociaciones en defensa de los animales quieren seguir trabajando sobre el punto para que incluya una mención específica a los parques acuáticos.</p>
<!-- /wp:paragraph -->