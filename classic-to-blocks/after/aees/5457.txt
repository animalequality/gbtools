<!-- wp:paragraph -->
<p>
	<img align="left" alt="" height="197" hspace="9" class="wp-image-13700" src="/app/uploads/2010/05/FOTO-CARTEL-SAN-ISDRO-2010-Y-LOS-4-GALAFATES.jpg" vspace="9" width="300">El antiguo componente de mecano, José María Cano, presentó una pintura que ilustrará el cartel de la feria taurina de San Isidro. El autor explicó que comenzó a pintar cuadros de temática taurina cuando hace diez años le pidieron “algo nuevo” para una exposición en Nueva York.<br>
	<br>
	Desde entonces, Cano se fue vinculando cada vez más a la tauromaquia y el año pasado colaboró con Canal Plus creando imágenes con momentos destacados de cada corrida retransmitida.<br>
	<br>
	El pintor dijo que ha quedado fascinado por “la belleza plástica de la tauromaquia y por su carácter metafísico” y explicó que en la tauromaquia “dos fuerzas contrapuestas o complementarias convergen en un mismo punto pero no llegan a chocar”<br>
	<br>
	José María Cano también tuvo tiempo para descalificar las iniciativas abolicionistas contra la tauromaquia: “Significan la beatitud a la corrección política”.<br>
	<br>
	&nbsp;</p>
<!-- /wp:paragraph -->