<!-- wp:image {"id":12067} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/28768099774_792bf23804_z_0.jpg" alt="" class="wp-image-12067"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>En múltiples oportunidades, Igualdad Animal ha sacado a la luz pública la crueldad a la que miles de millones de gallinas son sometidas alrededor del mundo por la industria del huevo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sus investigaciones en España, Alemania, México, India, Italia y Brasil han logrado tener un gran impacto mediático que ha permitido mostrar la terrible realidad que se oculta a los consumidores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En México, la organización presentó su investigación en octubre de 2016, <strong>siendo la primera vez que en el país se hacían públicas imágenes de la cruel industria.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La investigación se hizo en el estado de Jalisco que es la principal región en la producción de huevos en todo México, con más del 50% de la población de gallinas de todo el país, que sería cerca de 95 millones de aves.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las imágenes obtenidas muestran: &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La crueldad de la muda forzada o pelecha, la cual consiste en privar a las gallinas de agua y comida por 3 días. Esto reduce su peso corporal y acelera el proceso de un segundo ciclo de postura de huevo.</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Grupos de cuatro y hasta ocho gallinas en una jaula con un espacio para cada una del tamaño de una hoja de papel.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Alta mortalidad semanal de gallinas debido a enfermedades producidas por la falta de higiene.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Gallinas muertas junto a sus compañeras vivas en el interior de las jaulas.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Gallinas a las que se les inmoviliza dentro de las jaulas para su posterior desecho.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Gallinas enfermas que no reciben ningún tipo de atención veterinaria.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Gallinas cuyo pico ha sido mutilado (práctica estándar en la industria del huevo).</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Gallinas enfermas, abandonadas a nivel de suelo para que mueran sin poder tener acceso al agua y alimento que está en las jaulas.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Gallinas que agonizan en el interior de las jaulas sin recibir ninguna atención médica.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Gallinas que perdieron su plumaje debido al alto nivel de estrés que provoca el hacinamiento.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Gallinas con pies deformados, heridas y grietas debido al suelo de alambre de las jaulas.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/no-lo-sabias-pero-las-gallinas-son-en-realidad-como-unos-gatos-o-perros-emplumados-muy/" target="_blank">No lo sabías pero las gallinas son en realidad como unos gatos o perros emplumados muy inteligentes.</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«La sociedad tiene derecho a saber cómo se produce lo que consume y, en base a eso, tomar decisiones compasivas con los animales. Estas prácticas son legales pero no son justas», Dulce Ramírez, directora ejecutiva de Igualdad Animal México.</p></font>

<p>&nbsp;</p>

<figure><img alt="" class="wp-image-12068" src="/app/uploads/2017/05/28768099224_492fc9c18b_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>Poco después de que se lanzó la investigación <strong>Igualdad&nbsp;Animal inició una campaña para concientizar a los consumidores e incidir en empresas del sector para que se comprometan a dejar de comprar huevos que procedan del cruel sistema de jaulas</strong>.</p>

<p><a href="https://lavidaenunajaula.igualdadanimal.mx/" target="_blank">Por favor firma aquí para poner fin a las jaulas en batería</a> &nbsp;</p>

<p>Para ello también creó el equipo de Protectores de Animales, una plataforma que permite a los consumidores y todo aquel que se preocupe por el bienestar de los animales ayudar de manera sencilla, efectiva y decisiva.</p>

<p>&nbsp;</p>

<p>El registro es sencillo. Sólo debes acceder a <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank">protectoresanimales.mx</a>, completar un par de datos y ya estarás en contacto para que puedas realizar acciones en línea como enviar correos y usar las redes sociales para ayudar a los animales.</p>

<p>Igualdad Animal sigue trabajando incansablemente para que las jaulas queden en el pasado, mostrando esta terrible realidad a más consumidores. <strong>Ya en Italia y Brasil logró que importantes empresas se comprometieran a dejar de proveerse de huevos que provengan de jaulas.</strong></p>

<p>La eliminación de jaulas no implica la eliminación del maltrato, pero si es un importante progreso porque sería el primer paso hacia la reducción del sufrimiento de estos animales.</p>

<p>También puedes hacer mucho más para ayudar a las gallinas si sustituyes los huevos en tu alimentación por alternativas a su consumo. &nbsp;Es muy fácil hacerlo. <a href="http://www.igualdadanimal.org/noticias/7713/6-recetas-para-ayudarte-sustituir-los-huevos-en-tu-alimentacion" target="_blank">¡Aquí te explicamos cómo!</a></p></font>
<!-- /wp:html -->