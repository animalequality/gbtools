<!-- wp:paragraph -->
<p><b>Por favor, ayuda a que esta convocatoria tenga la máxima difusión </b>

Igualdad Animal convoca una concentración de protesta contra la matanza de focas de Canadá el próximo Jueves 15 de Marzo a las 12 h. junto a la embajada de Canadá en Madrid. Durante la concentración se realizarä además un acto protesta en el que varios activistas representando a los cazadores de focas y utilizando garfios, palos y rifles asesinarán simbólicamente ante la embajada a varios humanos desnudos sobre una bandera gigante de Canadá mientras otros muestran con pantallas de vídeo portátiles las imágenes reales de la matanza de focas, todo ello como denuncia de los terribles padecimientos y finalmente muerte que sufren cientos de miles de focas (entre ellos muchas apenas tienen más de dos semanas de vida) en las costas de dicho país.

Bajo la fuerza de los garrotes, los garfios, y los golpes 350.000 focas serán asesinadas esta primavera en el golfo de Saint Lawrence, en Canada. Las madres verán como sus crías se desangran vivas, mientras ellas permanecen impotentes esperando su muerte y viendo a sus familias morir.

La masacre de Canadá atenta contra los intereses básicos de cualquier animal en vivir y disfrutar de su vida en libertad. Por ello, e independientemente de la mayor o menor utilidad de la caza de focas o de la edad de estas víctimas, dicha caza resulta injustificable -al igual que la explotación para nuestros fines de cualquier otro animal (sea humano o no)-.

Desde Igualdad Animal pedimos y exigimos al gobierno de Canadá que cese la matanza de focas, pero también pedimos a todas las personas sensibles al sufrimiento animal que muestren su rechazo ante esta y cualquier otra matanza de animales. No olvidemos que cientos de animales no humanos mueren cada día en laboratorios, mataderos, etc. Es necesario que todos y todas reflexionemos sobre nuestra relación ellos, no contribuyendo con nuestros hábitos de vida (alimentación, entretenimiento, vestimenta, etc.)
con la muerte de nadie y tomando partido para detener injusticias como la que en unas horas ocurrirá en Canadá.

Igualdad Animal es una organización sin ánimo de lucro dedicada a cuestionar la discriminación que padecen los demás animales en función de su especie (prejuicio conocido como especismo). A lo largo de toda la historia las discriminaciones se han basado en considerar que ciertas diferencias irrelevantes (sexo, raza, etc.) justifican infravalorar los intereses de otros. A pesar de las diferencias que puede haber todos
los animales, humanos o no, somos iguales, ya que todos queremos vivir y disfrutar de nuestra vida.

Para cualquier información relacionada con nuestra organización y actividades escribir a: info@igualdadanimal.org o llamar al (+34) 675 737 459

<strong>Participa en nuestra protesta el próximo Jueves 15 de Marzo ante la Embajada de Canadá en la calle Núñez de Balboa, 35 (Madrid)
Metro: Línea 4, estación Velázquez.
Líneas de autobús: 51, 9, 19, parada Velázquez/Goya, 21 y 53, parada Goya/Castelló; o 29, 52 parada Goya/Príncipe de Vergara.</strong></p>
<!-- /wp:paragraph -->