<!-- wp:image {"id":12136} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/shutterstock_110836229.jpg" alt="" class="wp-image-12136"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Resulta lógico pensar que cada persona que reduce o elimina su consumo de carne reduce, de igual manera, el número de animales que tendrían que morir para satisfacer ese consumo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según el blog Counting Animals <strong>quien decide alimentarse de forma vegetariana salva 25 animales de granja cada año</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pero esta significativa cifra contiene una triste verdad: de estos 25 animales, 24 son pollos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Sorprendido? Piensa solamente que se necesitan hasta 200 pollos para proporcionar el mismo número de comidas que una vaca, y alrededor de 40 pollos en comparación con un cerdo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2016/05/10/por-que-comer-mas-pollo-aumenta-exponencialmente-el-maltrato-animal/" target="_blank">Por qué comer más pollo aumenta exponencialmente el maltrato animal</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12137} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/broiler1_1.jpg" alt="" class="wp-image-12137"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los pollos, dejando a parte únicamente los peces, son los animales más maltratados del planeta. Cuando al cálculo anterior se añaden los peces y otros animales marinos la cifra se dispara increíblemente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El cálculo total: una persona que decide no comer ni peces ni ningún otro animal salva 575 animales de granja y peces al año. Es impresionante, ¿verdad?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="http://www.igualdadanimal.org/noticias/7729/cuantos-animales-salvarias-al-ano-si-dejaras-de-comer-carne" target="_blank">¿Cuántos animales salvarías al año si dejaras de comer carne?</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por favor, considera ayudar a estos y todos los animales. Cada día en cada decisión que tomes, ya sea reduciendo la carne de pollo o pescado que comes o eliminándola por completo. Con esto estarías haciendo tu mayor contribución en contra de maltrato animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porque es la industria cárnica la responsable del mayor mopcionesaltrato animal que la historia ha conocido. Pero lo importante es que en nuestras manos está cambiar esa realidad. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las opciones y posibilidades para sustituir la carne en tus comidas cada día están más disponibles en supermercados y tiendas de <a href="https://igualdadanimal.org/noticia/2017/01/10/10-deliciosos-productos-vegetales-que-puedes-encontrar-en-tiendas-de-mexico/" target="_blank">México</a> y <a href="https://igualdadanimal.org/noticia/2016/08/04/8-deliciosos-productos-vegetales-que-ya-puedes-encontrar-en-los-supermercados-de/" target="_blank">España</a>. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Y si te gusta cocinar, no podrás creer la cantidad de recetas que tenemos para ti. <a href="http://www.igualdadanimal.org/noticias/7432/15-increibles-recetas-vegetarianas-para-ayudarte-sustituir-la-carne" target="_blank">Para comenzar acá tienes una muestra</a>.</p>
<!-- /wp:paragraph -->