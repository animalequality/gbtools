<!-- wp:image {"id":11987} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/foto_fb_23.jpg" alt="" class="wp-image-11987"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La mayor amenaza que enfrenta la humanidad actualmente se resume en dos palabras: cambio climático.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La temperatura global de nuestra planeta sigue en aumento y conseguir mantener el calentamiento global dentro del “nivel de riesgo límite” de 2ºC representa todo un reto.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La variación de la temperatura dependerá en gran medida de la emisión de gases de efecto invernadero en las próximas décadas. Pero, lamentablemente, a pesar del inminente riesgo los gobiernos no están llevando a cabo acciones efectivas.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11988} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/shutterstock_153806906.jpg" alt="" class="wp-image-11988"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La razón principal de esta inercia es que la escasa conciencia pública sobre el tema hace que no exista sobre ellos una verdadera presión. Y es que en parte <strong>la opinión pública desconoce que la principal fuente de emisiones de gases de efecto invernadero en todo el planeta es la ganadería industrial</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según la Organización Mundial de la Alimentación y Agricultura (FAO, en inglés), esta actividad es responsable del 14,5% del total de emisiones de gases de efecto invernadero.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pero si se suman todas las emisiones indirectas (de metano, uso de tierras y respiración del ganado) <strong>en realidad la ganadería industrial sería responsable de la emisión de 51% de los gases</strong>. Esto resulta alarmante si consideramos que la suma total de los gases emitidos por toda la industria mundial de transportes (coches, aviones y barcos incluidos) da como resultado solo el 22%.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por si lo anterior no fuera suficiente, la ganadería industrial es también responsable del <a href="https://igualdadanimal.org/blog/7-razones-por-las-que-la-ganaderia-industrial-es-un-desastre-ecologico/" target="_blank">mayor derroche de agua potable y de deforestación de selvas</a>. Literalmente, nuestro sistema de producción de alimentos está acabando con el planeta a pasos agigantados.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11989} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/shutterstock_183496451.jpg" alt="" class="wp-image-11989"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Desde 1960 el consumo de carne se ha multiplicado por cuatro y se espera que para el año 2050 este se multiplique por siete para satisfacer a una población humana de 9.600 millones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Un <a href="http://www.futureoffood.ox.ac.uk/news/plant-based-diets-could-save-millions-lives-and-dramatically-cut-greenhouse-gas-emissions" target="_blank">estudio</a> de la Universidad de Oxford que evaluó los efectos que cuatro tipos de dietas tienen sobre el medioambiente, reveló que <strong>una alimentación vegetariana puede reducir las emisiones de gases de efecto invernadero en un 63%, y una vegana hasta un 70%</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De manera que el mensaje es claro: si queremos asegurar que el planeta sea un lugar habitable en las próximas décadas hay que comer menos carne y sustituirla por opciones saludables.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font color="#808080"><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Puedes suscribirte gratuitamente a nuestro e-boletín</a> para que recibas las mejores noticias de actualidad sobre los animales y conozcas más opciones de alimentación.</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5">&nbsp;</font>

Somos la generación a la cual le tocó vivir el episodio más crítico de la devastación del planeta y <strong>como consumidores tenemos la responsabilidad de tomar decisiones que contribuyan a resolver esta crisis global</strong>.

Somos solo nosotros quienes, a través de los productos que decidamos echar dentro del carrito del supermercado, transformaremos el sistema de producción de alimentos y no al contrario.

Y ahora, al saber que eres protagonista de un momento histórico en el cual se definirá el destino del mundo tal cual como hoy lo conocemos, ¿que decidirás hacer?</p>
<!-- /wp:paragraph -->