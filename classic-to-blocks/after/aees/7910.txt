<!-- wp:image {"id":12464} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/mercadona-k0we-u40829014357g0b-624x385ideal.jpg" alt="" class="wp-image-12464"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":12465} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/seitan_mercadona.jpg" alt="" class="wp-image-12465"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Siguiendo una tendencia global imparable, la cadena de supermercados <strong>Mercadona acaba de lanzarse al mercado de la producción de sustitutos de la carne.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/07/05/paris-sonrie-las-proteinas-vegetales-con-la-apertura-de-tres-supermercados-100/" target="_blank">París sonríe a las proteínas vegetales con la apertura de tres supermercados 100% vegetarianos</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Desde principios de año, contaban con otros &nbsp;productos 100% vegetales como el tabule con verduras frescas, edamame, la ensalada de lentejas y begur y el kale.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>Dos son los productos que bajo su marca «Hacendado» ofrecerá próximamente: las hamburguesas de seitán y los medallones de seitán «a la piastra». <strong>El seitán es una alternativa a la carne muy popular </strong>entre las personas que siguen una alimentación basada en vegetales.</p>

<p>Se espera que estos y otros productos puedan estar a la venta en toda España a partir de octubre.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Fuentes:<a href="https://www.laverdad.es/sociedad/novedad-mercadona-vuelve-20170919134637-nt.html" target="_blank"> https://www.laverdad.es/sociedad/novedad-mercadona-vuelve-20170919134637-nt.html</a></p></font>
<!-- /wp:html -->