<!-- wp:paragraph -->
<p>
	Venezuela - Ayer se publicó en Gaceta Oficial número 39.338 la Ley para la Protección de la Fauna Doméstica Libre y en Cautividad, la cual ha generado polémica entre los grupos defensores de los derechos de los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	El punto más controversial es la prohibición absoluta de poseer un perro de la raza pittbull después del 31 de diciembre de 2014, como lo señala la tercera disposición transitoria. Esto obedece a que los asambleístas consideran de alta peligrosidad a esa especie canina. Mientras llega ese momento, los que posean ejemplares de la especie no los podrán sacar de sus casas, pues deben tenerlos "permanentemente en condiciones de cautividad", tal como reza el parágrafo 1° del artículo 33.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Días antes de la aprobación, la Asociación Pro-Defensa de los Animales envió un comunicado al presidente Hugo Chávez, en el que le pedía no firmar el instrumento legal enviado por la Asamblea Nacional, ya que el articulado era violatorio de la Declaración Universal de los Animales aprobada por la ONU.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	"La intención del legislador se transforma en aberrante y tautológica, pues pretende en este artículo la extinción de esta raza de perros al prohibir al ciudadano poseerlos, y establece condiciones crueles para los caninos, al obligar a los propietarios a mantenerlos en condiciones de absoluto cautiverio, por ser considerados animales `peligrosos’, no siendo lo más adecuado con una ley cuya finalidad es proteger a los animales", indica el comunicado. Manuel Díaz, representante del Movimiento Ecológico de Venezuela, se manifestó en contra de la prohibición; incluso, está en desacuerdo con que se les mantenga en cautividad.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	"La normativa de prohibir la tenencia contradice una ley que busca proteger a los animales. No se puede exterminar una raza por el capricho de un ser humano. Tienen los mismos derechos que los seres humanos. En este caso en particular se pueden corregir las irregularidades por ordenanzas, como en el municipio Libertador, donde se establece que deben tener bozal para no causar daños a terceros", expresó. Abogó por la implantación de bozales, arnés y collares a la hora de pasear a esos animales, así como cumplir con todas las vacunas; incluso, mencionó la posibilidad de contar con un seguro, como se estila en otros países. Emily Quevedo, representante de la ONG Activistas de los Derechos Animales y del Entorno, de Carabobo, también repudió la medida: "No estoy de acuerdo con que se quiera eliminar una raza. Lo que debe haber es campañas educativas sobre el cuido. Pero, como suele pasar con los mandatarios, prefieren hacer prohibiciones que elaborar una campaña educativa".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Responsabilidad municipal Quevedo considera negativo que la ley dejara decisiones finales en manos de las alcaldías, como la prohibición de corridas de toros o la elaboración de la lista de animales prohibidos, por ejemplo."Sabemos que muchas alcaldías tienen deudas en materia de ordenanzas. En Carabobo, por ejemplo, no se ha aprobado una ordenanza de protección animal porque el poder económico de la gente de las corridas lo detuvo", manifestó. Díaz también cuestionó que en el articulado no se haya prohibido el sacrificio de animales con fines religiosos, una solicitud de los grupos activistas desde el comienzo de la discusión de esta ley. "Ante las tradiciones y visiones religiosas debe privar la razón. Si hablamos de respeto a los animales, es un contrasentido legal que se permita la tortura de un animal por caprichos religiosos", expresó. El activista indicó que en los próximos días realizarán un taller con otras organizaciones para analizar el alcance del texto legal. Allí se decidirá si se interpone un recurso en contra, en el Tribunal Supremo de Justicia. Lo que sí es seguro es que el 26 de enero pedirán un amparo para exigir a los ministerios del Ambiente y de Educación que garanticen una enseñanza ambientalista, como lo establece el artículo 107 de la Constitución Nacional.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Fuente: analítica.com</p>
<!-- /wp:paragraph -->