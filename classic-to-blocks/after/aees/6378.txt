<!-- wp:paragraph -->
<p><img alt="" class="wp-image-9392" src="/app/uploads/2012/03/vela_llanes.jpg" style="width: 220px; float: left;">Comenzó coleccionando grillos a la edad de 3 años y más de 70 años después ha convertido su finca en un <a href="http://www.igualdadanimal.org/entretenimiento/zoos" target="_blank">zoológico </a>privado en el que <strong>mantiene cautivos cerca de 200 animales</strong>, mayoritariamente aves, entre los cuales se encuentran un cuatí, un lémur, un mono y erizos tropicales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pese a las <strong>denuncias</strong> recibidas en los últimos años, y la actualmente interpuesta por la asociación Anadel, Vela no se muestra preocupado, pues <strong>afirma que ninguna de ellas llegó nunca a desembocar en sanción</strong>. «Tengo las licencias del Ministerio y de la Consejería para poder coleccionar estos animales. Creé una Sociedad de Criadores de Aves y Pequeños Mamíferos, que es como figura este sitio en los permisos. Esto está clasificado como núcleo zoológico».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Seguro de sí mismo y respaldado por la legalidad vigente, que le concede el derecho a tener animales como objetos de exposición, este “coleccionista” asegura que mientras pueda y los años se lo permitan seguirá manteniendo su “colección particular”.</p>
<!-- /wp:paragraph -->