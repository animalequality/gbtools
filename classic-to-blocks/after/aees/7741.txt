<!-- wp:paragraph -->
<p>La investigación realizada por Igualdad Animal&nbsp;muestran una granja de productos lácteos en Reino Unido que desobedece la ley de bienestar animal, la cual prohíbe mantener a los terneros mayores de 8 semanas de vida en jaulas aisladas. Esta granja suministra leche a Marks &amp; Spencer. La investigación ha tenido una gran repercusión mediática en el país, apareciendo en medios como <span style="color: #0000ff;"><a style="color: #0000ff;" href="https://www.theguardian.com/commentisfree/2017/mar/30/dairy-scary-public-farming-calves-pens-alternatives?CMP=share_btn_fb" target="_blank" rel="noopener noreferrer">The Guardian</a>, <a style="color: #0000ff;" href="http://www.thetimes.co.uk/_TP_/article/marks-spencer-farm-kept-cows-in-cramped-huts-8rkxpmcks?ni-statuscode=acsaz-307" target="_blank" rel="noopener noreferrer">The Times</a> o <a style="color: #0000ff;" href="http://www.dailymail.co.uk/news/article-4354586/Shocking-pictures-calves-battery-farm-Dorset.html" target="_blank" rel="noopener noreferrer">Daily Mail</a></span> entre otros.

Las imágenes muestran a terneros de hasta 6 meses de vida encerrados en estas jaulas, algunos tan crecidos que apenas pueden entrar y salir de ellas. También se ve como algunos, tratando de entrar en estos cobertizos de plástico diseñados para protegerlos del mal tiempo, se provocan grandes heridas en el lomo.

Toni Shephard, directora de Igualdad Animal en Reino Unido, explica: «Es desolador ver filas y filas de terneros encerrados y aislados en esas pequeñas jaulas -cuando aún deberían permanecer junto a sus madres-, pero lo que más impacta es darse cuenta de que muchas son hembras que, en contra de la ley británica de bienestar animal, llevan muchos meses confinadas».

Y añade: «La ley de bienestar animal de Reino Unido reconoce la importancia que el ejercicio y la interacción social tiene para los terneros, y por ello restringe el uso de jaulas aisladas hasta las 8 semanas de vida, a pesar de esto, Igualdad Animal ha encontrado en esta granja terneros de hasta 6 meses sufriendo en estrechas jaulas individuales.

Le pedimos a M&amp;S que acabe sus negocios con este proveedor inmediatamente. Alentamos a todos los supermercados a implementar una política de tolerancia cero ante las granjas que no cumplan esta ley de bienestar animal».

Separar a las crías de sus madres un día después de nacer para confinarlas en este tipo de jaulas, es una práctica habitual en la industria láctea de todo el mundo. Sin embargo, la ley británica establece que los terneros deben ser trasladados a rediles comunes una vez cumplan las 8 semanas de vida para satisfacer su fuerte necesidad de interacción social.

Las imágenes obtenidas por Igualdad Animal muestran cómo algunos de los más mayores tratan desesperadamente de lamerse y darse afecto los unos a los otros a través de las rejas de metal que los separan.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/9BMXQv25pms" width="885" height="498" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
Igualdad Animal puso estos datos en conocimiento de la Dirección General de Comercio y ellos comprobaron pudieron confirmar que muchos de ellos eran mayores de 8 semanas. M&amp;S además ordenó hacer una auditoría.
Cerca de 1000 terneros viven confinados en la granja Grange Dairy en East Chaldon, Dorset, propiedad de J.F. Coob e hijos. Allí se crían terneras nacidas en otras localidades que pertenecen a la empresa, lo que supone un constante suministro de terneros a consecuencia de la inevitable producción de leche.

Según la página web de J.F Coob e hijos, esta granja provee leche a Marks &amp; Spncer con un contrato de calidad que premia un alto nivel de bienestar animal.</p>
<!-- /wp:paragraph -->