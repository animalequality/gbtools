<!-- wp:image {"id":10190} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/12/13dportadaweb.jpg" alt="" class="wp-image-10190"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":13719} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/12/Panorama13D.jpg" alt="" class="wp-image-13719"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

El pasado sábado 13 de diciembre a las 12 horas en La Puerta del Sol en Madrid, 500 activistas de Igualdad Animal realizaron una impactante acción para conmemorar el Día Internacional por los Derechos Animales. En las primeras filas algunos activistas mostraron cadáveres reales (que habían salido de contenedores y centros de explotación) de algunos de los animales más maltratados por la industria ganadera, como el cerdo, el cordero, el conejo o la gallina.

El resto de activistas mostraron imágenes de primeros planos de animales explotados fotografiados por Igualdad Animal en las distintas investigaciones que han llevado a cabo en los últimos años en granjas de visones, circos, zoos, mataderos, granjas de cerdos, criaderos... De esta forma cada activista sujetó la foto de un animal diferente, visibilizando su vida de maltrato y explotación.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13615} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/12/13D.jpg" alt="" class="wp-image-13615"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Según Javier Moreno, cofundador de Igualdad Animal “<em>hoy más de 500 personas hemos visibilizado la violencia que se comete diariamente contra los animales. Hemos sacado del anonimato a todas estas víctimas que sólo conocieron el sufrimiento y la crueldad</em>”. &nbsp;“<em>Para estas industrias los animales son simple mercancía de la que obtener el máximo beneficio económico y es necesario que la ciudadanía conozca el maltrato que viven los animales en estos centros de explotación</em>”.

Igualdad Animal viene realizando este acto desde 2008, convirtiéndose desde entonces en un acto referente en el movimiento animalista a nivel internacional, habiéndose llevado a cabo en multitud de países como Estados Unidos, Alemania, Israel, Chile, Perú, Austria, Francia, Australia y Canadá.

El 10 de Diciembre desde hace más de una década se viene celebrando el Día Internacional de los Derechos Animales coincidiendo con el Día de los Derechos Humanos. De esta forma se remarca que el deseo de vivir y de no sufrir no entienden de especie.

El acto tuvo una gran repercusión mediática, <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157649719509676/">apareciendo en medios nacionales e internacionales</a>, llegando literalmente a millones de personas.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://player.vimeo.com/video/114475414" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe><a href="https://vimeo.com/114475414">TVE - Protesta de Igualdad Animal por el Día Internacional de los Derechos Animales</a> from <a href="https://vimeo.com/animaltv">Animal Tv</a> on <a href="https://vimeo.com">Vimeo</a>.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14197} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/12/postal.jpg" alt="" class="wp-image-14197"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Al terminar el acto se entregó un sobre a cada participante con la imagen de cada uno de los animales fotografiados donde se podía leer:

"Hoy le has mostrado al mundo mi realidad, gracias a ti ya no soy invisible. Investigadores de Igualdad Animal vinieron hasta aquí para tomar esta fotografía y hoy tú la has mostrado a la sociedad. Tal vez ya no puedas hacer nada por mí, pero sí por todos los que vendrán.

Te pido que no te rindas, que sigas defendiéndonos y alzando tu voz. Te animo a que nunca te falten las fuerzas, porque te necesitamos. Tú eres mi esperanza y la de un futuro mejor. Gracias por participar en el Día Internacional de los Derechos Animales".

&nbsp;

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/LfvEVsNuSV4" width="875px" height="492px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/albums/72157649675517286"><img src="https://farm8.staticflickr.com/7497/15388793344_807fcbb11c_z.jpg" alt="Acto por el Día de los Derechos Animales"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/albums/72157649719509676"><img src="https://farm9.staticflickr.com/8603/16006922906_e7d904a958_z.jpg" alt="Repercusión Mediática Acto por el Día de los Derechos Animales"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></p>
<!-- /wp:paragraph -->