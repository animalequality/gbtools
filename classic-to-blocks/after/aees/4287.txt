<!-- wp:html -->
<div id="osborne" align="center">
<table border="0" width="580" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td><img class="wp-image-13826" src="/app/uploads/2008/04/cabecera_osborne_01.jpg" alt="Acción sobre el Toro de Osborne" width="580" height="482" border="0"></td>
</tr>
<tr>
<td valign="top" bgcolor="#234d9f">
<div align="center">
<table border="0" width="580" cellspacing="16" cellpadding="0">
<tbody>
<tr>
<td valign="top">
<p class="style2"><span style="color: #ffffff;">Cuatro activistas de Igualdad Animal se subieron ayer Domingo 27 de Abril a lo más alto de la figura del Toro de Osborne de Valdemoro (Madrid) para desplegar una pancarta de 6 metros de largo por 3 de alto reivindicando la abolición de las matanzas de toros y defendiendo los derechos de todos los animales.</span></p>
<p class="style1"><span style="color: #ffffff;">A pesar de que tanto la Guardia Civil como la Policía Municipal vigilaban la figura y trataron de impedir la acción, nuestros activistas pudieron al final subir rápidamente a la estructura, desde la que Tomás y Jose se descolgaron, mientras David gritaba lemas como "<strong>Tauromaquia Abolición</strong>" o "<strong>Respeto para todos los animales</strong>". Una vez la pancarta fue izada con la ayuda de otros activistas, las fuerzas policiales presentes ya no pudieron evitar la acción y se limitaron a esperar a que descendiésemos para identificarnos y denunciar los hechos.</span></p>
<p class="style1"><span style="color: #ffffff;">Esta ha sido la primera vez que se utiliza esta figura taurina, conocida internacionalmente como un símbolo asociado a España y a las matanzas de los animales a los que representa, para defenderles y exigir el fin de su explotación. Esta acción forma parte de la campaña que Igualdad Animal está llevando a cabo para promover el fin de los espectáculos taurinos en concreto y cualquier forma de explotación en general.</span></p>

<table border="0" width="296" cellspacing="0" cellpadding="0" align="center" bgcolor="#000000">
<tbody>
<tr>
<td align="center"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157604758665497/show/"><img class="wp-image-14275" src="/app/uploads/2008/04/superior_fotos.jpg" width="548" height="74" border="0"></a></td>
</tr>
<tr>
<td align="center">
<table border="0" width="380" cellspacing="6" cellpadding="0">
<tbody>
<tr>
<td><a href="https://www.flickr.com/photos/igualdadanimal/2444974589/"><img class="wp-image-13577" src="/app/uploads/2008/04/001.jpg" width="169" height="113" border="0"></a></td>
<td height="113"><a href="https://www.flickr.com/photos/igualdadanimal/2444990791/"><img class="wp-image-13579" src="/app/uploads/2008/04/002.jpg" width="169" height="113" border="0"></a></td>
<td><a href="https://www.flickr.com/photos/igualdadanimal/2451357149/"><img class="wp-image-13580" src="/app/uploads/2008/04/003.jpg" width="169" height="113" border="0"></a></td>
</tr>
<tr>
<td><a href="https://www.flickr.com/photos/igualdadanimal/2445761090/"><img class="wp-image-13581" src="/app/uploads/2008/04/004.jpg" width="169" height="113" border="0"></a></td>
<td height="113"><a href="https://www.flickr.com/photos/igualdadanimal/2444918105/"><img class="wp-image-13582" src="/app/uploads/2008/04/005.jpg" width="169" height="113" border="0"></a></td>
<td><a href="https://www.flickr.com/photos/igualdadanimal/2445896942/"><img class="wp-image-13583" src="/app/uploads/2008/04/006.jpg" width="169" height="113" border="0"></a></td>
</tr>
<tr>
<td><a href="https://www.flickr.com/photos/igualdadanimal/2445026255/"><img class="wp-image-13584" src="/app/uploads/2008/04/007.jpg" width="169" height="113" border="0"></a></td>
<td height="113"><a href="https://www.flickr.com/photos/igualdadanimal/2445063813/"><img class="wp-image-13585" src="/app/uploads/2008/04/008.jpg" width="169" height="113" border="0"></a></td>
<td><a href="https://www.flickr.com/photos/igualdadanimal/2445842660/"><img class="wp-image-13586" src="/app/uploads/2008/04/009.jpg" width="169" height="113" border="0"></a></td>
</tr>
<tr>
<td><a href="https://www.flickr.com/photos/igualdadanimal/2444943307/"><img class="wp-image-13588" src="/app/uploads/2008/04/010.jpg" width="169" height="113" border="0"></a></td>
<td height="113"><a href="https://www.flickr.com/photos/igualdadanimal/2445021335/"><img class="wp-image-13589" src="/app/uploads/2008/04/011.jpg" width="169" height="113" border="0"></a></td>
<td><a href="https://www.flickr.com/photos/igualdadanimal/2449852627/"><img class="wp-image-13591" src="/app/uploads/2008/04/012.jpg" width="169" height="113" border="0"></a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center"><img class="wp-image-14059" src="/app/uploads/2008/04/inferior_fotos.jpg" width="548" height="46"></td>
</tr>
</tbody>
</table>
<table border="0" width="100%" cellspacing="6" cellpadding="0" align="center">
<tbody>
<tr>
<td colspan="3" align="center" valign="top"><img class="wp-image-14297" src="/app/uploads/2008/04/titulo_accion.gif" width="548" height="30"></td>
</tr>
<tr>
<td colspan="3" align="center" valign="top"></td>
</tr>
<tr>
<td align="center" valign="top" width="167"><img class="wp-image-13920" src="/app/uploads/2008/04/david.jpg" width="167" height="225"></td>
<td align="center"></td>
<td align="center" valign="top">
<div align="left"><span style="color: #ffffff;">DAVID</span>
<p class="style1"><span style="color: #ffffff;">Mi nombre es David Granada soy un activista de Igualdad Animal. Desde hace algún tiempo la organización ya tenía planeada esta manifestación, pero solo hace unos pocos días me han informado que haría parte de los 4 activistas que se subirían al monumento del toro y pues me emocioné bastante porque sabia la importancia del acto y la gran ocasión de llevar nuestro mensaje a muchas personas.
De todas maneras cuando llegó el día de hacerlo estaba muy tranquilo porque tenía muchas esperanzas que todo habría salido bien, así que llegamos muy pronto al lugar y al inicio todo parecía muy tranquilo, solo que pocos minutos después de nuestra llegada empezamos a ver algunos agentes de la guardia civil y en ese momento si que empezaron a subir mis nervios y más que todo mi desilusión porque pensé que ya no habríamos podido hacer nada debido a ellos...</span></p>
Así que algunos de los activistas empezaron a hacer todo tipo de propuestas para poder talvez engañar un poco a la policía e irnos a otro lugar y planeábamos irnos solamente los 4 que íbamos a subir y que se quedaran todos los otros activistas para que la policía pensara que lo íbamos a hacer de todas maneras allí.

Al final ya estábamos en el coche para irnos y no sé como se decidió de un momento a otro de aprovechar un instante en el que la policía no estaba y no hice más que correr hacia la furgoneta para cambiarme, coger las cuerdas de la pancarta y empezar a correr con los demás hacia el toro, incluso hasta pasamos al lado de Sharon que estaba dando una entrevista a unos periodistas y creo que ni ella se lo esperaba en ese mismo instante y no se como me subí hasta arriba en cuestión de poquísimos segundos, creo que debido a la tensión ni me di cuenta como subí. Ya desde arriba lanzamos todas las cuerdas y vi que ya estaba lleno de patrullas de la policía que salieron de la nada así que hicimos todo en modo muy veloz, los otros activistas que estaban abajo hicieron su trabajo muy rápidamente y logramos subir la pancarta con un poco de dificultad para atar las cuerdas pero al final pudimos posicionarla bien y ya con más calma me di cuenta que lo habíamos logrado, sentí una gran felicidad por esto. Luego, todos los activistas presentes empezamos a gritar mensajes contra la injusticia que reciben todos los animales en los diferentes ámbitos para hacer entender que protestábamos no solo por la abolición de la tauromaquia si no también por todos los millones de animales que mueren día a día.

Luego vi que la policía se dedicaba a identificar todos los presentes así que pudimos estar bastante tiempo mostrando la pancarta, al final me han dicho que estuvimos más de una hora pero a mi me parecieron pocos minutos... en esos momentos me sentía honorado por estar allí porque estaba luchando por una justa causa.

Dejamos la pancarta allí y bajamos y nos abrazamos todos porque estábamos muy contentos por haberlo logrado y que todo había salido bien, fue un momento muy emotivo de verdad.

Ya al final estaba de verdad feliz porque habíamos logrado dar el mensaje y así llamar la atención para reivindicar también la abolición de cada tipo de explotación animal, esa injusticia que viven miles de millones de animales diariamente y que podemos evitar cada uno de nosotros y nosotras simplemente dándonos cuenta que tenemos el poder de liberales cambiando nuestros hábitos de consumo, es algo así de simple y que lleva a salvar la vida de muchos animales. Al día siguiente cuando me dirigía al trabajo, abrí uno de los diarios que distribuyen en el metro y vi la noticia prácticamente en primera pagina, en ese momento sentí simplemente satisfacción por haber llevado nuestro mensaje a millones de personas….

</div></td>
</tr>
<tr>
<td colspan="3" align="center"></td>
</tr>
<tr>
<td align="center" valign="top" height="225"><span style="color: #ffffff;"><img class="wp-image-13946" src="/app/uploads/2008/04/eu.jpg" width="167" height="225"></span></td>
<td align="center"></td>
<td align="center" valign="top">
<div align="left"><span style="color: #ffffff;">TOMÁS</span>
<p class="style1"><span style="color: #ffffff;">Siempre pensé que la tauromaquia se sustenta en los pilares de la ignorancia y que tras el escudo de "símbolo de identidad" nacional o una hipócrita excusa de "salvar una especie", lo que realmente hay detrás es el beneficio económico de unos pocos, y la tortura y muerte despiadada de este precioso animal... sin olvidar que la utilización se hace extensiva al resto de animales, sea o no en forma de espectáculo.</span></p>

</div></td>
</tr>
<tr>
<td colspan="3" align="center"></td>
</tr>
<tr>
<td align="center" valign="top"><img class="wp-image-14078" src="/app/uploads/2008/04/javier.jpg" width="167" height="225"></td>
<td align="center" width="10"></td>
<td align="center" valign="top">
<div align="left"><span style="color: #ffffff;">JAVIER</span>
<p class="style1"><span style="color: #ffffff;">Fue un día de gran tensión, sobre todo al comprobar que la convocatoria se había filtrado a la policía y que había varias patrullas de la Guardia Civil vigilando al Toro. Cuando vi a los compañeros desplegar la pancarta y ver que todo había salido bien fue una gran satisfacción.</span></p>

</div></td>
</tr>
</tbody>
</table>
<table border="0" width="547" cellspacing="0" cellpadding="0" bgcolor="#11264F">
<tbody>
<tr>
<td><img class="wp-image-13921" src="/app/uploads/2008/04/defiende.jpg" width="547" height="274"></td>
</tr>
<tr>
<td>
<table border="0" width="547" cellspacing="12" cellpadding="0">
<tbody>
<tr>
<td valign="top">
<p class="style1"><span style="color: #ffffff;"><strong>Tú también puedes ser activista por los derechos animales</strong></span></p>
<p class="style1"><span style="color: #ffffff;">En Igualdad Animal pensamos que todos podemos ser parte de la solución y que no podemos seguir ignorando todo el sufrimiento y las muertes que suceden diariamente. Tú también puedes ayudar a los animales explicando a otros lo que padecen y los motivos por los que deben ser respetados, dejando de apoyar su explotación (haciéndonos veganos o veganas), difundiendo nuestra información y actividades o colaborando con Igualdad Animal en alguna de nuestras próximas acciones.</span></p>
Los animales no-humanos no pueden defenderse ni reclamar justicia. Si estás cansado de ver cómo son masacrados, si crees que es injusto que nos creamos sus dueños y quieres ayudar a que todo esto cambie, participa en <span style="color: #ffffff;">próximos actos</span><span style="color: #ffffff;">, realizamos más de cinco protestas cada semana sobre diversos ámbitos, colabora con nosotros, te esperamos. A través de nuestra </span><span style="color: #ffffff;">sección de actividades</span><span style="color: #ffffff;"> podrás consultar las próximas acciones en varios países como </span><span style="color: #ffffff;">España</span><span style="color: #ffffff;">, </span><span style="color: #ffffff;">Perú</span><span style="color: #ffffff;">, </span><span style="color: #ffffff;">Venezuela</span><span style="color: #ffffff;"> y </span><span style="color: #ffffff;">Colombia</span><span style="color: #ffffff;">.</span>

Ahora mismo, en tan sólo un momento, puedes ayudar a difundir esta acción y dar a conocer lo que padecen los no-humanos, <span class="style1"><span style="color: #ffffff;">pulsa el </span><a href="%PERS_FRIEND%"><span style="color: #ffffff;">siguiente enlace</span></a><span style="color: #ffffff;"> para reenviar este correo a tus amistades.</span></span>

<strong>• Hazte socio-colaborador de Igualdad&nbsp;Animal</strong>
Por tan sólo 4 euros al mes, puedes ayudarnos a seguir trabajando por los animales <span style="color: #ffffff;">haciéndote socio-colaborador</span><span style="color: #ffffff;"> de Igualdad Animal.</span>

<hr>
<p class="style1"><span style="color: #ffffff;"><strong>Ayúdanos a defender a los animales en tu zona</strong></span></p>
<p class="style1"><span style="color: #ffffff;"><strong>• Dona para financiar nuestras actividades</strong>
Desgraciadamente nuestros recursos son limitados y necesitamos toda la ayuda que sea posible para poder seguir luchando por los animales. Con una pequeña aportación por tu parte podremos imprimir más pancartas y realizar esta acción también en tu zona.&nbsp;</span></p>
Igualdad Animal es una organización sin ánimo de lucro y los únicos recursos provienen de las <a href="https://igualdadanimal.org/dona-para-ayudar-a-los-animales/"><span style="color: #ffffff;">donaciones</span></a><span style="color: #ffffff;">, cuotas de </span><a href="https://igualdadanimal.org/dona-para-ayudar-a-los-animales/"><span style="color: #ffffff;">socios-colaboradores</span></a><span style="color: #ffffff;"> y de la </span><span style="color: #ffffff;">venta de nuestro material</span><span style="color: #ffffff;">.</span>
<p class="style2"><span style="color: #ffffff;">• ¿Puedes alojar durante un par de días a nuestros activistas?</span></p>
<p class="style1"><span style="color: #ffffff;">Igualdad Animal es una organización internacional que trabaja en España, Perú, Venezuela y Colombia. En España está actualmente presente en Madrid, Barcelona, Sevilla y Huelva donde realizamos acciones en defensa de los animales de forma constante, pero queremos llegar a más sitios y a más gente. Una forma en que puedes ayudarnos es ofrecer un espacio para que nuestros activistas puedan viajar a otras ciudades y defender a los animales en tu zona.</span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td height="12"><img class="wp-image-13922" src="/app/uploads/2008/04/defiende_inferior.gif" width="547" height="12"></td>
</tr>
</tbody>
</table>
<a href="mailto:tienda@igualdadanimal.org"><img class="wp-image-13831" src="/app/uploads/2008/04/camiseta.jpg" width="548" height="222" border="0" hspace="5"></a></td>
</tr>
</tbody>
</table>
</div></td>
</tr>
<tr>
<td valign="top" bgcolor="#000000" height="80"><a href="https://igualdadanimal.org/"><img class="wp-image-14058" src="/app/uploads/2008/04/inferior_contacto.gif" alt="" width="580" height="73" border="0"></a></td>
</tr>
</tbody>
</table>
</div>
<!-- /wp:html -->