<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><big><b>Activista contra la caza de patos recibe un disparo en la cara.</b></big></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->
<h5 class="wp-block-heading">
	(21/03/2011)</h5>
<!-- /wp:heading -->

<!-- wp:image {"id":13664} -->
<figure class="wp-block-image"><img src="/app/uploads/2011/03/495500-julia-symons.jpg" alt="Julia Symons recibió un disparo en la cara por uno de los cazadores de patos en el día de la inauguración de la temporada. Foto: Yuri Kouzmin / Fuente Weekly Times." class="wp-image-13664"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>
La activista contra la caza, Julia Symons, de Melburne recibió el pasado sábado 19 de marzo de 2011 un disparo en la cara por uno de los cazadores de patos en el día de la inauguración de la nueva temporada al oeste de Victoria.<br><br>
Julia&nbsp;sufre lesiones de perdigones en la cara, los dientes y en la mano por un disparo accidental en el lago Buloke, a 12 km al este de Donald en el oeste de Victoria.<br><br>
Los testigos dijeron&nbsp;que la activista, de unos 20 años de edad, se introdujo en el agua hasta la cintura momento en el que recibió el impacto de un disparo de uno de loscazadores, que se cree iba destinado a una de las aves en vuelo bajo. Julia permaneció consciente cuando la trasladaron desde el agua hasta el punto de rescate por otros manifestantes y seguidores del grupo de rescates de patos. Se dijo que aún había perdigones presentes en su rostro, y uno de sus dientes había sido disparado. Fue llevada al Hospital Horsham donde se encuentra en estado estable.<br><br>
Los manifestantes que se encontraban en el lago expresaron su enojo por la temporada de caza de 2011 que permite a cada cazador matar a 10 aves por día, hasta el próximo 13 de junio. Tiradores de tan solo 12 años de edad están autorizados a cazar patos con licencias válidas de caza.<br><br>
Sigue informándote: <a href="http://www.igualdadanimal.org/entretenimiento/caza">La caza</a><a href="http://www.granjasdeesclavos.com/patos/como-son">Los patos</a><br><br><small>Fuentes: <a href="http://www.heraldsun.com.au/ipad/duck-protester-shot/story-fn6bfkm6-1226024498276" target="_blank">Herald Sun</a></small><br><br></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:html -->
<iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FIgualdadAnimal&amp;width=400&amp;colorscheme=light&amp;show_faces=false&amp;stream=false&amp;header=false&amp;height=62" style="border:none; overflow:hidden; width:400px; height:62px;"></iframe>
<!-- /wp:html -->