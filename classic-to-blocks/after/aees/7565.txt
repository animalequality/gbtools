<!-- wp:image {"id":11025} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/09/shutterstock_132872258_-_copia.jpg" alt="" class="wp-image-11025"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Es un hecho: podemos vivir de forma plena y saludable alimentándonos con alternativas a la carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lo prueban personas que llevan <strong>décadas alimentándose sin carne</strong>; lo prueban<a href="https://igualdadanimal.org/noticia/2016/08/30/8-deportistas-que-prueban-que-comer-carne-no-es-necesario/" target="_blank"> deportistas de élite</a> que han decidido alimentarse de forma vegetariana; lo prueban expertos en nutrición que han <a href="https://www.abc.es/salud/noticias/20150305/abci-dieta-vegetariana-ictus-201503051053.html" target="_blank">estudiado a fondo</a> las alimentaciones vegetarianas y han descubierto que son <strong>perfectamente saludables</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La pregunta está en el aire: <a href="http://www.igualdadanimal.org/noticias/7503/9-consejos-practicos-para-reducir-o-sustituir-la-carne-en-tu-alimentacion" target="_blank">si podemos hacerlo</a> y de esta forma no dañar a los animales en granjas y mataderos, <strong>¿por qué no hacerlo?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La verdad es que conocemos la respuesta: estamos tan habituados a comer carne que alimentarnos <a href="http://www.igualdadanimal.org/noticias/7554/15-recetas-tradicionales-en-su-deliciosa-version-vegetariana" target="_blank">con alternativas</a> nos parece raro. Pero, ¿es realmente raro?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La publicidad de las industrias de alimentación <strong>nos ha hecho depender de la carne</strong>; sin embargo, el supermercado siempre ha estado lleno de alternativas tan <a href="http://www.igualdadanimal.org/noticias/7511/informacion-nutricional-para-las-alimentaciones-vegetarianas" target="_blank">nutritivas y saludables</a>. Para su desgracia, las industrias de las verduras, legumbres, frutos secos, frutas y hortalizas, no son tan poderosas como la de la carne.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11179} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/10/shutterstock_305470052_0.jpg" alt="" class="wp-image-11179"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La industria cárnica es responsable <strong>del peor maltrato animal en la historia del ser humano</strong>. Nunca antes tantos animales habían nacido con el mero propósito de ser enviados al matadero; ¿cómo ha hecho esta industria para que personas que se declaran <strong>en contra del maltrato animal</strong> defiendan, sin embargo, que millones y millones los animales de granja están para que nos los comamos?, ¿cómo han hecho sus departamentos de publicidad para que en occidente se coma carne de forma tan excesiva que haya <a href="https://igualdadanimal.org/noticia/2016/08/11/consumir-mas-proteinas-animales-apunta-un-mayor-riesgo-de-muerte-segun-estudio/" target="_blank">un problema de salud pública</a>?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sean cuales sean las respuestas, hay algo que está claro para cualquiera que está dispuesto a ver: si fuésemos un pollo, <a href="https://igualdadanimal.org/noticia/2016/07/15/video-laura-la-cerdita-rescatada-de-la-industria-carnica-que-ha-conquistado-internet/" target="_blank">un cerdo</a> o una vaca, <strong>no querríamos estar en manos de la industria cárnica</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="4"><font color="#808080"><p>«La publicidad de las industrias de alimentación nos ha hecho depender de la carne; sin embargo, el supermercado siempre ha estado lleno de alternativas tan nutritivas y saludables.»</p></font>

<p>&nbsp;</p>

<p>Hay otra cosa igualmente clara: podemos vivir <strong>vidas saludables y plenas</strong> alimentándonos con alternativas a la carne, dejando así de dañar a los animales.</p>

<p>La pregunta permanece en el aire: ¿por qué no hacerlo?</p>

<p><strong>Si quieres estar al tanto de historias como esta y muchas más, además de informarte de cómo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales</strong>.</p></font>
<!-- /wp:html -->