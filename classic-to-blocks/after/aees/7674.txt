<!-- wp:image {"id":11594} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_95517259_0.jpg" alt="" class="wp-image-11594"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>España tiene el mérito de haberse puesto en contra a todos con un plan para abrir la mayor granja láctea industrial de Europa: una macro-explotación con 20.000 vacas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El lugar seleccionado sería la provincia de Soria y detrás del faraónico proyecto está la cooperativa navarra Valle de Odieta. <strong>Incluso buena parte del propio sector lácteo está en contra de la macro-granja industrial</strong>, a la que tildan de «ganadería depredadora.»</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Organizaciones agrarias han declarado que el proyecto representa un «modelo de macro-explotación industrial, intensiva e insostenible.»</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«El proyecto ocuparía un área de 900 hectáreas (equivalente a 900 campos de fútbol).»</p></font>

<p>&nbsp;</p>

<p>Además de ser responsable del <strong>maltrato animal</strong> que conlleva inseminar artificialmente una vez tras otra a las vacas para separarlas de sus crías al poco de nacer, <a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/" target="_blank">la ganadería industrial supone un problema medioambiental a escala global</a>.</p>

<p>La macro-granja industrial generaría <strong>dos millones de litros de purines al día</strong> y además las necesidades de agua de un proyecto de esta envergadura son enormes. Los recursos hídricos de toda la zona están en riesgo y se está teniendo que estudiar si sería viable medioambientalmente.</p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>El proyecto ocuparía un área de <strong>900 hectáreas</strong> (equivalente a 900 campos de fútbol), tanto para albergar a las vacas como para gestionar las enormes cantidades de residuos tóxicos que generarían. El peligro de que dichos residuos <strong>se filtrasen a los acuíferos de la zona</strong> preocupa tanto a los ganaderos como a la administración.</p>

<figure><img alt="" class="wp-image-11595" src="/app/uploads/2017/02/shutterstock_235967143.jpg" style="float:left; margin:10px; width:550px"></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Este tipo de proyectos son comunes en países como China, Australia, Estados Unidos o países saudíes, <strong>en los que existen granjas industriales lácteas de hasta 100.000 animales</strong>. Sin embargo, nunca se había contemplado algo así en España ni en el resto de Europa.</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://www.lavozdeasturias.es/noticia/actualidad/2017/01/29/soria-prepara-terreno-mayor-granja-lactea-europa-20000-vacas/0003_201701G29P31991.htm" target="_blank">https://www.lavozdeasturias.es/noticia/actualidad/2017/01/29/soria-prepara-terreno-mayor-granja-lactea-europa-20000-vacas/0003_201701G29P31991.htm</a></p></font></font>
<!-- /wp:html -->