<!-- wp:paragraph -->
<p>No existe, en toda la historia, un mayor responsable de causar sufrimiento a los animales que la industria ganadera. La violencia y crueldad con la que se trata a pollos, vacas, gallinas y cerdos, <strong>sería ilegal si perros o gatos fueran las víctimas</strong>, y, seguramente, los responsables de cometer estos actos irían presos.

Cada año, la ganadería industrial mata a un número de animales equivalente a la población humana de 8 planetas Tierra. Cada uno de esos animales vive sus vidas en constante dolor, hacinamiento y privados de poder disfrutar de algo tan sencillo como sentir el sol, el aire o pisar la hierba.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13322} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/09/shutterstock_1056678593.jpg" alt="" class="wp-image-13322"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Todos anhelan su libertad y que su sufrimiento acabe, luchando hasta el final por sus vidas, a pesar de que <strong>no existe escapatoria </strong>alguna para ellos dentro de un sistema que fija las fechas de sus muertes desde el día en que nacen.

Sin duda, la ganadería industrial <strong>es el enemigo número uno de los animales</strong>.

Pero, ¿sabías que existe alguien capaz de cambiar esta terrible situación? ¿Y de transformar este sistema de producción de alimentos obsoleto y cruel en algo mejor? ¿Alguien que podría cambiar el destino de los animales?

Y ese alguien eres tú. Los consumidores tenemos el poder de transformar a esa industria de alimentos que actualmente provoca tanto sufrimiento a los animales. En la medida en que demandamos más productos alternativos a la carne, y que además son más saludables y protegen al planeta, enviamos un mensaje muy claro a la industria: <strong>nos importan los animales</strong> y por eso preferimos consumir productos que no provengan de su sufrimiento.

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;">¡Suscríbete gratuitamente a nuestro e-boletín!</span></a></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
&nbsp;

Considera comenzar a ayudar a los animales sustituyendo la carne en tu alimentación por las muchas alternativas vegetales que existen. Para animarte a hacerlo aquí<a href="https://igualdadanimal.org/blog/5-consejos-infalibles-para-reducir-tu-consumo-de-carne/" target="_blank" rel="noopener">&nbsp;te explicamos cómo.&nbsp;</a></p>
<!-- /wp:paragraph -->