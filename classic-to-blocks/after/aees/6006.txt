<!-- wp:image {"id":9031} -->
<figure class="wp-block-image"><img src="/app/uploads/2011/05/FE3A9484-ABD7-5377-E274C514E7665108.jpg" alt="" class="wp-image-9031"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
	No les quedaba mucha vida por delante porque su destino estaba solo a 60 kilómetros, <strong>en el <a href="http://www.mataderos.info">matadero</a></strong> de la capital burgalesa. El destino les tenía reservada una muerte diferente al <a href="https://igualdadanimal.org/problematica/carne/">"sacrificio"</a> que normalmente encuentran los cerdos a manos de los <a href="http://www.mataderos.info">matarifes</a>. Sus cadáveres no terminarán colgados de argollas, listos para su despiece. Su San Martín lo encontraron en la carretera, en la cuneta de la N-234, cerca de Hacinas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<strong>Más de cien <a href="http://www.granjasdeesclavos.com/cerdos/como-son">cerdos</a>&nbsp;de los 210</strong> que transportaba el camión que los traía desde una <a href="http://www.granjasdecerdos.org/videos/comunidades/aragon">granja de Zaragoza</a> murieron en la cuneta del kilómetro 321 de la N-234 después de que el vehículo volcara, al parecer por un despiste del conductor, ya que el siniestros se produjo en una recta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	El accidente ocurrió sobre la una y media de la madrugada de ayer martes entre la localidad de Hacinas y el cruce de Pinilla de los Barruecos. En el <strong>siniestro </strong>sólo resultó dañado el remolque y <a href="http://www.igualdadanimal.org/antiespecismo">su carga</a>, ya que el conductor salió ileso, según informó un compañero del mismo que ayer por la mañana trataba de recuperar a los animales que no estaban dañados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Gran parte de los cerdos<strong> murieron en el accidente y quedaron tendidos en el suelo</strong>. Algunos de los que quedaron vivos permanecieron junto al camión. Otros huyeron.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Durante la mañana de ayer, veterinarios de la Junta de Castilla y León y empleados de la empresa que <a href="http://www.granjasdecerdos.org/informe/explotacion/transporte">transportaba los animales</a> se encontraban en el lugar donde ocurrió el incidente contabilizando los <a href="http://www.granjasdeesclavos.com/cerdos/como-son"><strong>cerdos muertos</strong></a> e intentando que los vivos no llegaran a la carretera. También subieron a un camión a los animales que previamente habían introducido en una especie de vallado al lado de donde el remolque, de gran tamaño y varios pisos, volcó. Al parecer, también se "sacrificaron" a varios animales que resultaron <strong>malheridos en el accidente</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<strong>Nota de Igualdad&nbsp;Animal:&nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3 class="wp-block-heading">
	<strong>Para prevenir estos accidentes hoy y en el futuro, no proponemos mejores condiciones de transporte, sino que cada ciudadano adopte un estilo de&nbsp;<a href="http://www.vivevegano.org/" style="color: rgb(2, 122, 198); text-decoration: none; ">vida vegano</a>&nbsp;</strong>que supone reducir a 0 el uso de animales, la no necesidad de tener que transportar animales a los mataderos y no contribuir así con la explotación y muerte de animales en granjas, carreteras o mataderos.</h3>
<!-- /wp:heading -->