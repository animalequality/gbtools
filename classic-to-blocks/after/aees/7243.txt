<!-- wp:image {"id":10239} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/03/salvaunagnello_0.jpg" alt="" class="wp-image-10239"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><a href="https://www.youtube.com/watch?v=CVj9evKX0X4"><img alt="" class="wp-image-13666" src="/app/uploads/2015/03/60sec_video2.jpg" style="float:right; margin-left:10px; margin-right:10px; width:400px"></a>La organización internacional Igualdad Animal lanza su conocida campaña “Salva un Cordero” <a href="https://www.youtube.com/watch?v=CVj9evKX0X4"><strong>con un impactante vídeo</strong></a> que muestra la vida de este animal en 60 segundos. En el vídeo se muestra desde el momento en el que nace el animal y es separado de su madre, hasta el momento en el que es brutalmente matado en el matadero. Todas las imágenes que aparecen el vídeo han sido obtenidas por investigadores de Igualdad Animal en la industria.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta es una de las campañas de mayor de impacto de Igualdad Animal, <a href="https://igualdadanimal.org/noticia/2013/04/11/la-investigacion-de-igualdad-animal-en-italia-reduce-las-ventas-de-cordero-en-el-pais/">que en ediciones anteriores ha reducido en un 40% el consumo de carne de cordero en Italia durante La Pascua</a>, por el impacto mediático de las imágenes presentadas por la organización animalista.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"<em>Creemos que un vídeo de estas características, que condensa la triste vida de un cordero en 60 segundos, va a tener un gran impacto en las redes sociales, y muestra la terrible realidad que viven estos animales diariamente. Para la industria cárnica los animales son simple mercancía de la que obtener el máximo beneficio económico, nosotros creemos que la ciudadanía tiene derecho a saber cómo son tratados los animales</em>” afirmó Javier Moreno, cofundador de Igualdad Animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En España mueren cada año en los mataderos 19,3 millones de corderos. En la Unión Europea la cifra asciende hasta los 64,5 millones de ovejas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/CVj9evKX0X4" width="885px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->