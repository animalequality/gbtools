<!-- wp:image {"id":14291} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/04/thecoweb.jpg" alt="" class="wp-image-14291"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13916} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/04/cowspi.jpg" alt="" class="wp-image-13916"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El <strong>sábado 6 de junio a las 17:30 horas en el auditorio de La Casa Encendida en Madrid,</strong>&nbsp; Igualdad Animal proyectará de forma gratuita el aclamado documental "<a href="https://www.cowspiracy.com/"><strong>Cowspiracy: el secreto de la sostenibilidad</strong></a>".&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El documental, dirigido por Kip Andersen y Keegan Kuhn y que financiaron a través de una <a href="https://www.indiegogo.com/projects/cowspiracy-the-sustainability-secret">exitosa campaña de crowfunding</a> en IndieGoGo, explora el impacto de la ganadería en en el medio ambiente e investiga las políticas de importantes organizaciones ecologistas sobre el tema. Algunas de las organizaciones que son investigadas en la película son Greenpeace, Sierra Club, Surfrider Foundation y Rainforest Action Network.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Louie Psihoyos, director ganador de un Óscar por "The Cove", ha indicado que "Cowspiracy puede ser el documental más importante realizado hasta el momento para ayudar a salvar el planeta". También el conocido realizador Darren Aronofsky ha manifestado que "el documental sacudirá e inspirará al movimiento ecologista".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"Estamos destruyendo el planeta y a quienes habitamos en él y el tiempo corre en nuestra contra. De hecho quizá ya sea demasiado tarde para dar marcha atrás y precisamente por eso, por la urgencia de poner el foco en las causas principales del abismo al que nos dirigimos, Cowspiracy es tan necesario y revolucionario". Indicó Javier Moreno, cofundador de Igualdad Animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/6D112Y3ua8E" width="885px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->