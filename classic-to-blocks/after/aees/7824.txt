<!-- wp:image {"id":12143} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/tmg-article_tall.jpg" alt="" class="wp-image-12143"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Luego de ser abandonada por segunda vez por otro granjero que estaba a su cargo, la cerdita Nelly quedó sola y muy triste.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pero, rápidamente, Nelly pudo superar la desolación del abandono. <strong>Encontró consuelo y la mejor compañía no en un humano sino en un simpático chiquillo que se convertiría muy pronto en su mejor amigo: Pedro, un gato de granero.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/blog/la-inteligencia-de-los-cerdos-comparable-la-de-elefantes-o-delfines/" target="_blank">La inteligencia de los cerdos, comparable a la de elefantes o delfines</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Una vez que la granja fue vendida, el par de inseparables amigos fueron recibidos en el refugio para animales de granja Happy Tails Farm Sanctuary, muy famoso por sus amistades entre especies.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12144} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/47cac6_45d95d0fcbfb44f3836a80e3c3020f1bmv2.jpg" alt="" class="wp-image-12144"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Mientras se instalaban en su nuevo hogar y dormían juntos en el heno dejaron muy claro que están más unidos que nunca.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A Pedro le sigue encantando acurrucarse con Nelly y podría estar todo el día montado en su lomo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A ambos les queda por delante una vida feliz y tranquila. Nelly, la cerdita, ha sido particularmente afortunada ya que <strong>es de los pocos cerdos que entre miles de millones ha podido escapar de <a href="https://igualdadanimal.org/noticia/2016/04/14/el-peor-maltrato-animal-conocido-se-produce-en-las-granjas-y-mataderos/" target="_blank">la crueldad de la industria cárnica</a>. &nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A pesar de que la percepción que solemos tener de un cerdo y un gato sea tan distinta, <strong>Nelly y Pedro son animales que sienten dolor y conocen el miedo y quieren disfrutar de sus vidas en libertad y sin ser maltratados</strong>. Ninguno de los dos quiere terminar en el plato de alguien.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>Apreciamos claramente a través de la sólida amistad que han forjado que ellos mismos se perciben y respetan como iguales y que cada uno quiere lo mejor para el otro.</p>

<p>¿Podemos nosotros también comenzar a dejar de lado esas diferencias que solo existen en nuestra percepción?</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://www.petcha.com/pig-and-cat-stick-together-through-several-owners-thanks-to-rescue-group-trending/" target="_blank">https://www.petcha.com/pig-and-cat-stick-together-through-several-owners-thanks-to-rescue-group-trending/</a></p></font>
<!-- /wp:html -->