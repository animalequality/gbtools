<!-- wp:image {"id":10181} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/11/foie_1.jpg" alt="" class="wp-image-10181"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-13706" src="/app/uploads/2014/11/IMG_0387.jpg" style="float:left; margin-left:5px; margin-right:5px; width:350px">Igualdad Animal ha protestado hoy miércoles 26 de noviembre frente a la Embajada de Francia en Madrid con motivo de la celebración del Día Mundial Contra el Foie Gras. Voluntarios de la organización mostraron imágenes de animales sometidos a alimentación forzada, una práctica extremadamente cruel e inherente a esta industria. Además se hizo entrega de una carta solicitando el fin de esta crueldad. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Francia es responsable del 75% de la producción mundial de foie gras, por eso la presión internacional es un factor determinante a la hora de conseguir cambios importantes para los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Día Mundial Contra el Foie Gras es una iniciativa de la organización francesa <a href="https://www.l214.com/">L214</a> que cuenta con el apoyo internacional de Igualdad Animal. Estas protestas a nivel mundial han tenido lugar en ciudades como París, Berlín, Stuttgart, Londres, Madrid o Roma.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para Javier Moreno, cofundador de Igualdad Animal, "<em>la producción del foie gras está prohibida ya en 18 países, y en Europa sólo se produce en 5, entre ellos España y Francia. La alimentación forzada de los patos es una auténtica tortura y es necesario avanzar en protección animal para que esta crueldad desaparezca</em>".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Gracias a la campaña de Igualdad Animal por la prohibición del foie gras, recientemente el Gobierno de India prohibió la importación del producto, convirtiéndose en el primer país en adoptar esta medida. [1]</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Igualdad Animal es una organización internacional de defensa de los animales presente en España, Inglaterra, Italia, México, Alemania, Venezuela e India que cuenta con más de un millón de simpatizantes y que trabaja a través de la sensibilización, concienciación e investigación con el objetivo de promover cambios en la sociedad y en las leyes que sean favorables a los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>[1] <a href="https://elpais.com/sociedad/2014/07/04/actualidad/1404476441_203265.html">https://elpais.com/sociedad/2014/07/04/actualidad/1404476441_203265.html</a></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13701,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157649482760725/show"><img src="/app/uploads/2014/11/Foie-2.jpg" alt="" class="wp-image-13701"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->