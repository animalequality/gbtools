<!-- wp:paragraph -->
<p>Buenos Aires, Argentina - Hoy ha dado comienzo la 121ª Exposición Rural de Palermo, en la que serán expuestos 3.836 animales.

El primer animal obligado a participar en dicho acto fue Mendieta, un toro nacido tras un proceso de selección genética.

Los casi 4.000 animales estarán encerrados en ese lugar hasta el próximo 7 de agosto.

El presidente de la Sociedad Rural, Luciano Miguens, afirmó: "será una fabulosa demostración de la tecnología que utiliza el sector para crecer y reinventarse, tanto en genética animal como en maquinaria. Hoy hay tanta tecnología desarrollada en la molécula de una vaca como en el motor de una sembradora".</p>
<!-- /wp:paragraph -->