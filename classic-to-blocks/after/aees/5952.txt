<!-- wp:paragraph -->
<p>
	<strong><strong><img alt="Un elefante de 6 años ha fallecido en el zoo de Berlín. (Foto: Público)" class="wp-image-13611" src="/app/uploads/2011/04/1302110917148elefantedn.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 300px; ">Los veterinarios confirman que ha muerto por una infección que, en principio, no está relacionada con la que acabó con la celebridad polar</strong></strong><br>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>
	<strong><iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FIgualdadAnimal&amp;width=400&amp;colorscheme=light&amp;show_faces=false&amp;stream=false&amp;header=false&amp;height=62" style="border: medium none; overflow: hidden; width: 400px; height: 62px;"></iframe></strong></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>
	El zoo de Berlín ha confirmado que otro joven animal ha muerto inesperadamente en sus instalaciones, sólo dos semanas después de haber hecho público el fallecimiento del oso polar Knut, que se había convertido en una celebridad en el país. <a href="https://www.facebook.com/video/video.php?v=142484682484680"><b>[Vídeo: muerte en directo de Knut, oso polar preso en el zoo de Berlín]</b></a><br>
	<br>
	Claudia Bienek, portavoz del zoológico, informa de que en esta ocasión el fallecimiento es el de un elefante asiático de seis años de edad, que murió en la madrugada del martes.<br>
	<br>
	Los veterinarios del zoológico aseguran que el elefante ha muerto debido a una infección que no pudieron detectar a tiempo, aunque aún están pendientes de análisis los resultados de la necropsia que se ha realizado al cuerpo del animal para determinar la causa exacta de la muerte.<br>
	<br>
	Debido a que los elefantes pueden vivir hasta 80 años en cautiverio y a que el oso polar Knut murió con solo cuatro años de edad, Bienek se ha apresurado a aseverar que las dos muertes no están relacionadas. "Es simplemente muy, muy triste", concluye.<br>
	<br>
	El pasado 19 de marzo, tras desmayarse debido a unas convulsiones y caer al agua en el recinto de foso, <a href="https://www.facebook.com/video/video.php?v=142484682484680">el oso polar Knut falleció frente a la mirada de los visitantes del zoológico berlinés</a>. Los veterinarios señalaron que su muerte fue causado por una inflamación del cerebro debido a una infección.<br>
	<br>
	<br>
	Fuente: <a href="https://www.publico.es/internacional/joven-elefante-fallece-zoo-murio.html" target="_blank">Público.es</a><br>
	<br>
	&nbsp;</p>
<!-- /wp:paragraph -->