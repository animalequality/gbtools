<!-- wp:image {"id":13899} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/05/concentracion_tauromaquia.jpg" alt="" class="wp-image-13899"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Activistas de Igualdad Animal celebraremos este <strong>domingo 13 de mayo, en Sevilla,</strong> un&nbsp;<strong>acto pacífico a las puertas de la Plaza de Toros de la Maestranza</strong> para reivindicar la abolición de la tauromaquia. Esta iniciativa tendrá lugar de forma paralela a la celebración de una corrida de toros en el coso sevillano.<br>
	<br>
	Con este acto se pretende <strong>recordar y dar voz de forma solemne a las víctimas de la tauromaquia</strong>, víctimas que son igualmente capaces de sentir y tienen intereses propios que merecen el mismo respeto que acordamos entre humanos, y que sin embargo son torturadas hasta la muerte por el simple hecho de no pertenecer a nuestra especie.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
	<br>
	Pretendemos con este acto hacer ver a la sociedad nuestro <strong>rechazo </strong>ante la iniciativa que se está poniendo en marcha desde distintos sectores, como partidos políticos o los propios profesionales de los toros, para que la<strong> tauromaquia sea reconocida como Patrimonio Cultura Inmaterial de la Unesco.</strong> En este sentido, el Pleno del Ayuntamiento de Sevilla aprobó recientemente un moción para que los espectáculos taurinos de la ciudad sean Patrimonio Cultural Inmaterial de Sevilla.</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>No lo dudes,<a href="https://igualdadanimal.org/dona/" target="_blank"> únete a nuestro trabajo. </a>Los animales necesitan que seamos su voz.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fecha:<strong> Domingo 13 de mayo</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hora: <strong>17:30</strong> h</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lugar: <strong>Plaza de la Maestranza de Sevilla</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Es<strong> imprescindible confirmar asistencia</strong> escribiendo a: conchip@igualdadanimal.org&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->