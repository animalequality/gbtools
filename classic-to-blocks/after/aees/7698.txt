<!-- wp:image {"id":11680} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_304647254.jpg" alt="" class="wp-image-11680"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Comer menos carne es bueno para los animales, el planeta y para tu salud.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los consumidores somos los únicos que tenemos el <a href="https://igualdadanimal.org/noticia/2016/04/29/los-consumidores-tenemos-el-poder-de-ayudar-los-animales-y-al-planeta/" target="_blank">poder</a> de cambiar <a href="https://igualdadanimal.org/noticia/2016/04/01/8-imagenes-de-animales-atrapados-en-un-modelo-alimentario-obsoleto-y-cruel-que-no/" target="_blank">la forma en que se produce lo que comemos</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Al demandar más productos que provienen de medios de producción sostenibles y respetuosos con los animales <strong>vamos dejando atrás el obsoleto modelo alimentario que se basa en la ganadería industrial</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porque nos importan los animales y <strong>no queremos seguir apoyando a una industria que los maltrata</strong>, cada vez somos más los que queremos ayudarles.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Si ya estás decidido a sumarte a <a href="http://www.igualdadanimal.org/noticias/7433/por-que-hay-cada-vez-mas-personas-vegetarianas-y-veganas-nuestro-alrededor" target="_blank">esta tendencia global</a>, estos 5 tips que traemos hoy son para ti.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Te invitamos a dar el primer paso a hacia <strong>una nueva forma de alimentación que trae beneficios y satisfacción a todos</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>1.Súmate al Lunes Sin Carne.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Engánchate a esta campaña internacional que promueven estrellas de la talla de Paul McCartney y Gwyneth Paltrow. Al menos un día a la semana, prueba no comer carne. Puede que esto no sea un gran esfuerzo para ti pero <strong>reducirá tu consumo en un 15% lo cual beneficiará masivamente a los animales, el medioambiente y a ti</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11682} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_103263902.jpg" alt="" class="wp-image-11682"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>2. Organiza tus platos con más vegetales y menos carne.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Con tantas alternativas tan nutritivas y deliciosas la carne no tiene porqué seguir siendo el alma de la fiesta. <strong>Prueba incorporar más vegetales a tu plato y haz combinaciones de texturas, colores y sabores</strong>. ¡Te sorprenderá y deleitará la cantidad de posibilidades!</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11683} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_181314668.jpg" alt="" class="wp-image-11683"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>3. Inspírate en los sabores de otras nacionalidades al comer dentro o fuera de casa.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Muchos de los platos más populares del mundo son vegetarianos y ya los conoces</strong>. Tal es el caso de la cocina de México, India, Italia y China, por solo mencionar algunos. Además, las recetas, franquicias y restaurantes con este tipo de cocina abundan, así que no podemos añadir otra cosa más que ¡buen provecho! &nbsp;&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11684} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_286136471_1.jpg" alt="" class="wp-image-11684"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<strong>4. Reduce a la mitad las porciones de carne.</strong>

<p>Antes de comenzar a comer piensa en cuanta carne comes y reemplaza una parte con vegetales y ve agregando cada vez más porciones de tus alternativas a la carne favoritas. &nbsp;Esto hará que puedas comenzar a sentirte satisfecho comiendo menos carne y más vegetales. &nbsp;</p>

<figure><img alt="" class="wp-image-11685" src="/app/uploads/2017/02/shutterstock_372447313_0.jpg" style="float:left; margin:10px; width:450px"></figure><p></p>

<p>&nbsp;</p>










<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>



<strong>5. Conecta esta meta con otra y disfrútalo.</strong>

<p>Reducir tu ingesta de carne te traerá grandes beneficios a tu salud como más energía y rendimiento físico, así que aprovechando estas ventajas ¿por qué no comenzar alguna actividad física? De esta forma «comer menos carne» se convertirá en parte de tu entrenamiento y también una motivación más.</p>

<figure><img alt="" class="wp-image-11686" src="/app/uploads/2017/02/shutterstock_150676640.jpg" style="float:left; margin:10px; width:450px"></figure><p></p>

<p>&nbsp;</p>









<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>6. Haz que otros conozcan tus intenciones.</strong>

<p>Nada facilita más un cambio que dar a conocer a otros nuestros propósitos.</p>

<p>Míralo así: si te invitan a cenar seguramente tus anfitriones prepararán con anticipación algo especialmente para ti y si sales a comer con tus amigos será más fácil para todos escoger el restaurante.</p>

<p>Al saber que estás reduciendo tu consumo de carne, muchas personas podrán hacerte recomendaciones de restaurantes, recetas e información en general.</p>

<p>Además, dando a conocer tus motivos (los animales, la sostenibilidad del planeta y tu salud) puedes animar a otros a dar el primer paso como seguramente alguien te motivó a ti.</p>

<figure><img alt="" class="wp-image-11687" src="/app/uploads/2017/02/shutterstock_245735908_0.jpg" style="float:left; margin:10px; width:450px"></figure><p></p></font>
<!-- /wp:html -->