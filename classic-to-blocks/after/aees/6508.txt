<!-- wp:image {"id":9524} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/05/perro_orion_condenado_a_muerte_las_vegas.jpg" alt="" class="wp-image-9524"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Orion</strong>, un perro de seis años, mezcla de mastín y crestado rodesiano, mató al bebé durante el festejo de su primer cumpleaños. Tras la muerte de <strong>Jeremiah Eskew-Shahan</strong>, ocurrida el 27 de abril, la familia entregó de manera voluntaria al animal para que fuese <strong>matado</strong>, pero el grupo <strong>Proyecto Lexus</strong>, con sede en Nueva York, argumenta que debería ser enviado a un refugio en las afueras de Denver.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La muerte de Orion estaba programada para este martes, pero una orden judicial emitida el lunes la postergó al menos hasta la audiencia del viernes. El padre del menor, Christopher Shahan, dijo al diario <em><a href="http://www.lvrj.com/news/dog-that-killed-baby-gets-reprieve-from-euthanization-150537165.html" target="_blank">Las Vegas Review-Journal</a></em> que el perro merece morir. Su hijo se encontraba en casa de su abuela en Henderson, con motivo de su primer cumpleaños, cuando gateó hacia el perro dormido y empezó a acariciarlo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los oficiales dijeron que el perro, de 54 kilos, prendió su mandíbula en la cabeza del bebé y empezó a sacudirlo. La abuela del menor trató de quitárselo y otros miembros de la familia que se encontraban en la casa corrieron a ayudarla, pero fue demasiado tarde. El niño fue trasladado a un hospital, donde fue declarado muerto la mañana siguiente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Rich Rosenthal, un abogado de Nueva York que dirige el Proyecto Lexus, argumentó que <strong>los padres del bebé son los verdaderos culpables</strong> y no el perro, porque el <strong>instinto del animal</strong> es atacar cuando es provocado mientras duerme.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«Estoy seguro que los padres viven su propio infierno al pensar cómo pudieron permitir que algo como esto sucediera, pero el matar al perro no resucita al niño", </em>sostuvo Rosenthal. <em>"Alguien tomó al perro por el pelo mientras dormía y el animal reaccionó. En su mente, no hizo nada más que lo que es normal que un perro haga».</em></p>
<!-- /wp:paragraph -->