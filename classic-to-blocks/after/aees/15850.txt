<!-- wp:paragraph -->
<p>Representantes de ProAnimal y Animals Australia, dos organizaciones por la defensa de los animales, anunciaron que en las próximas semanas presentarán un proyecto de ley que prohibiría en Chile el transporte en barco de un continente a otro de animales vivos destinados a ser matados por su carne.

Los motivos que han impulsado dicha iniciativa son <strong>las evidencias del terrible maltrato que esta práctica provoca a los animales,</strong> tales como someterlos a temperaturas extremas y el nulo retiro de los excrementos y de la asistencia veterinaria. Patricia Cocas, representante de ProAnimal Chile, declaró que “desde el 2014 al 2017 se exportaron desde Chile más de 60 mil animales con destino a Medio Oriente. Los llevan hacinados y sobre toneladas de excrementos, se aplastan entre ellos y los que se enferman los arrojan vivos al mar. Es un negocio de tortura y en Turquía, uno de los destinos, los sacrifican sin insensibilización previa y de una manera brutal”.

<strong><a href="https://es.loveveg.com/recetas/" target="_blank" rel="noopener">Consigue un fantástico recetario con deliciosos platillos veggie</a>, además de una guía con consejos prácticos para una alimentación sostenible.</strong>

En julio de este año, el gobierno australiano decidió suspender la licencia de Emanuel Exports, el mayor transportista de ovejas vivas de Australia, luego de que a principios de año un video que mostraba a ovejas hacinadas en corrales sucios y jadeando por el estrés y el calor provocara la indignación pública. Aproximadamente <strong>2.400 de estos animales murieron debido al agotamiento y deshidratación</strong> provocados por las altas temperaturas.

<a href="https://www.radioagricultura.cl/nacional/2018/12/15/anuncian-ley-que-prohibe-exportar-animales-vivos.html" target="_blank" rel="noopener">Fuente</a></p>
<!-- /wp:paragraph -->