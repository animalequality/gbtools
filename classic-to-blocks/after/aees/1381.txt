<!-- wp:paragraph -->
<p>El diario 20minutos ha publicado un artículo bajo el título "<strong>¿El beneficio del hombre justifica la experimentación con animales?</strong>" en referencia a un estudio publicado por un profesor de Oxford al respecto. Igualdad Animal es mencionada en dicho artículo como una de las ONG que rechazan la vivisección o experimentación con animales con la siguiente cita:

"<strong>Pero, ¿está justificada la experimentación con primates en beneficio del ser humano?</strong>

No, para ONG como la española Igualdad Animal, que denunció la explotación animal el pasado domingo con motivo del Día Internacional de los Derechos Animales.

Precisamente protestas como la de Igualdad Animal y otras activistas de los derechos de los animales está cuestionando el uso de estos animales en la experimentación."

El artículo va acompañado de una encuesta sobre la legitimidad de experimentar con primates para beneficio humano, desde Igualdad Animal os animamos a participar en dicha encuesta y dar un rotundo NO a la utilización de otros animales como objetos de laboratorio. Porque todo animal capaz de sentir merece respeto, el mismo respeto que tenemos entre humanos, porque la especie, al igual que la raza o el sexo, no convierte a nadie en un recurso a nuestro beneficio.</p>
<!-- /wp:paragraph -->