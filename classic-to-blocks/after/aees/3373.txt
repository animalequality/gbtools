<!-- wp:paragraph -->
<p>Un caballo que se encontraba suelto ha muerto atropellado por un autobús en la carretera MU-603, a la altura de Cuevas de Reyllo (Murcia), sin que ninguno de los ocupantes del vehículo haya resultado herido.

Fuentes de la Guardia Civil de Tráfico informaron de que el suceso ocurrió sobre las seis de esta mañana en la citada carretera en dirección a Alhama de Murcia.

El conductor del autobús se encontró el equino en medio de la calzada y no pudo esquivar al animal.


Fuente: laverdad.es</p>
<!-- /wp:paragraph -->