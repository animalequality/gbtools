<!-- wp:paragraph -->
<p>La camella que el periodista iraquí Atta Soosa regaló a la tropa salvadoreña del octavo contingente del Batallón Cuscatlán murió el pasado lunes, según informaron fuentes de El Diario de Hoy desde Iraq.

El animal fue encontrado muerto en un espacio donde, junto al camello también donado por Soosa, había sido aislada desde hacía varias semanas.

Según las fuentes, no se ha establecido qué causó la muerte del rumiante, pero entre los militares se presume que pudo haber sido a causa de inanición, pues no se les proporcionaba la suficiente comida. Temen que lo mismo suceda con el macho.

El obsequio de los camellos ocurrió después de que el comandante del octavo contingente, coronel Víctor Bolaños, comentara a Soosa que el dromedario del zoológico salvadoreño había muerto. Pero, por falta de recursos, no trajeron a los rumiantes. 


Fuente: elsalvador.com</p>
<!-- /wp:paragraph -->