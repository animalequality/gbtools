<!-- wp:paragraph -->
<p>Aldi España ha anunciado <a href="https://www.aldi.es/somos-responsables/somos-responsables-materias-primas/" target="_blank">su compromiso</a>&nbsp;(sección <strong>Política Internacional de Compra Sobre Bienestar Animal</strong>) para dejar de vender huevos de gallinas enjauladas.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12573} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/10/3_0.jpg" alt="" class="wp-image-12573"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La empresa ha confirmado a Igualdad Animal su compromiso para dejar de vender huevos de código 3 en 2020, una medida que cambiará las condiciones de vida de 170.000 aves.<br>
&nbsp;<br>
El acuerdo de Aldi España es el más reciente en unirse a más de 400 empresas a nivel mundial en comprometerse a dejar de vender huevos de gallinas enjauladas.<br>
<br>
El pasado 16 de junio Huevos Guillén, <strong>el mayor productor de huevos de España</strong>&nbsp;<a href="http://www.huevosguillen.com/noticia3.html" target="_blank">anunciaba su compromiso</a> para deja de producir huevos provenientes de gallinas enjauladas en 2025.<br>
<br>
Estos acuerdos son una clara respuesta a la creciente demanda de los consumidores para una mayor transparencia y mejor trato para los animales en el sistema alimentario.<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Aldi España está dando un paso importante hacia la mejora de las condiciones de los animales que sufren en las granjas industriales», declara Javier Moreno, cofundador y director internacional de Igualdad Animal. Y añade: «El sistema de jaulas en la ganadería industrial es uno de los más crueles con los animales. Aunque libre de jaula no es libre de maltrato, es indispensable que las empresas respondan a las demandas crecientes de la ciudadanía y se comprometan a dejar de apoyar esta crueldad».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://www.youtube.com/watch?v=QcRVWIpGNJA&amp;feature=youtu.be" target="_blank">Igualdad Animal lanzó recientemente una campaña sobre el sistema de jaulas</a>&nbsp;en granjas de gallinas en la industria del huevo en España, revelando imágenes nunca vistas de la industria.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" gesture="media" height="480" src="https://www.youtube.com/embed/QcRVWIpGNJA" width="854"></iframe></p>
<!-- /wp:paragraph -->