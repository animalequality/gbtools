<!-- wp:paragraph -->
<p>Blanket, Texas- Mike Ratliff, de 83 años, cerró su negocio de entrenamiento de gallos tras 39 años de funcionamiento. En este tiempo ha enseñado a más de 8000 personas a entrenar gallos para utilizarles como medio de entretenimiento y apuestas. Desde que Oklahoma prohibió las peleas de gallos en el 2002, Nuevo México y Luisiana son los únicos estados que las permiten. Pero Texas autoriza a la gente a criar los gallos, y a enseñar cómo alimentarlos y entrenarlos.

Los expertos sostienen que el negocio prospera a pesar de que está prohibido en la mayoría de los estados. Los aficionados a esta “práctica” publican revistas e intercambian consejos por Internet. Las peleas de gallos aún se practican en diferentes lugares del mundo. A los gallos generalmente les adosan en las patas una especie de navajas lo que provoca que se generen heridas mortales.

Mike Ratliff dijo "Dios los puso aquí con un propósito. Y es para pelear". Quizás en los tiempos del Imperio Romano, la gente que veía el circo desde la grada, también pensaba que los gladiadores fueron puestos por los dioses para pelear y asesinarse. Pero tanto esas personas como el explotador de gallos se equivocaron. Ningún ser sintiente ha sido puesto por nadie para servir de entretenimiento a otros individuos, pues el interés de alguien en vivir libre y no sufrir (ya sea gallo o humano) siempre estará por encima del interés de otro individuo en disfrutar a su costa.

Fuente: www.univision.com</p>
<!-- /wp:paragraph -->