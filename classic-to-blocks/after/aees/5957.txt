<!-- wp:paragraph -->
<p>
	<strong>Su mujer le telefoneó y respondió diciendo: “Me he caído al río y me estoy muriendo”.<br>
	El hombre se precipitó por el barranco de su finca, por lo que sufrió múltiples fracturas.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<img alt="Foto: Bomberos / EFE" class="wp-image-14225" src="/app/uploads/2011/04/rescate-hombre-y-gato-barranco.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: right; width: 400px; ">Quería salvar a su gato que se había caído por un barranco y casi le cuesta la vida a él. Un vecino de Vila-real, Santiago Gutiérrez, de 68 años, sufrió a mediodía de ayer múltiples fracturas después de precipitarse por un terraplén de su propia finca con una vertiginosa pendiente que tenía alrededor de 40 metros. Afortunadamente, el cuerpo del varón quedó colgado de un árbol sin llegar a caer al cauce del río Millars, tal y como explicaron a Mediterráneo desde el Consorcio Provincial de Bomberos. Todo sucedió en la zona del Termet de Vila-real, en una vivienda de la urbanización San Fermín, sobre las 14.22 horas. El afectado se percató de que su gato estaba en apuros, así que decidió bajar por el barranco para rescatarlo. Por causas que no han trascendido, y dada la peligrosidad del terreno, el hombre sufrió una caída golpeándose el cuerpo con múltiples rocas hasta que quedó tendido, todavía consciente, en el margen del río Millars, sin llegar a alcanzar el agua.<br>
	<br>
	Fue su mujer quien le llamó por teléfono al ver que no acudía a comer. Santiago, pese a las heridas y fracturas tan graves, pudo responderle y le dijo: “Me he caído al río y me estoy muriendo”.<br>
	<br>
	Para rescatarlo, el Consorcio de Bomberos desplazó a la zona una dotación del parque de Nules y la Unidad de Rescate de Montaña. Los bomberos utilizaron técnicas de rescate de montaña, descendiendo con tirolina y cuerdas.<br>
	<br>
	Una vez estabilizado el accidentado, le ascendieron en camilla hasta la superficie, donde fue atendido por los servicios sanitarios y trasladado al General.<br>
	<br>
	<strong>Además, el gato&nbsp;también pudo ser rescatado por los bomberos en el terraplén, sano y salvo.</strong><br>
	<br>
	&nbsp;</p>
<!-- /wp:paragraph -->