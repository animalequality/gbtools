<!-- wp:paragraph -->
<p>El próximo Viernes 14 de Noviembre, iniciamos la campaña en contra de la utilización de animales como vestimenta. Al igual que hicimos la temporada pasada, nos pondremos en frente de peleterías con pantallas portátiles y carteles mostrando la esclavitud a la que son sometidos los animales y repartiremos información para promover una relación justa con el resto de animales, que sin duda pasa por que dejemos de verles como recursos para nuestro beneficio y dejemos de financiar su utilización/explotación.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El punto de encuentro será en la salida del Metro de Callao a las 18 horas. Esperaremos aproximadamente unos 15 minutos e iremos en frente de una peletería situada en la Calle Preciados. Si quieres participar, no dudes en ponerte en contacto con nosotros a través del e-mail activismo@igualdadanimal.org o llamándonos al 915 222 218 ó al 691 054 172.</p>
<!-- /wp:paragraph -->