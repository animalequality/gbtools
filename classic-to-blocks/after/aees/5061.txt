<!-- wp:paragraph -->
<p>VALENCIA, 14 Oct. (EUROPA PRESS) - 

Efectivos del Consorcio Provincial de Bomberos de Valencia rescataron hoy a dos perros tras incendiarse una vivienda de la Arte de la Seda del municipio valenciano de Xirivella, según informaron en un comunicado fuentes de este Cuerpo.

El incendio se originó sobre las 15.45 horas en una vivienda de Xirivella por causas que no han sido precisadas, y el fuego afectó a uno de los dormitorios.

Hasta el lugar de los hechos se desplazaron varios efectivos de los parques de Paterna y Torrent, que sofocaron el fuego y rescataron a dos perros.

Fuente: www.europapress.es</p>
<!-- /wp:paragraph -->