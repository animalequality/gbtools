<!-- wp:paragraph -->
<p>Madrid - A las 12.30 horas de hoy día 19 de Marzo, quedaba visto para sentencia el juicio contra los cuatro activistas de Igualdad Animal: Javier Moreno, Azucena López, Iván Guijarro y Sharon Núñez.

Tras más de 20 minutos de interrogatorios a los activistas y con una reclamación inicial del matadero por las supuestas pérdidas económicas producidas por la protesta estimadas por el dueño del matadero en más de 12.000 euros y posteriormente rebajadas por la fiscal a 600 euros, la juez determinará en los próximos días el veredicto del juicio.

Durante el mismo, los cuatro activistas que llevaban con camisetas reivindicativas contra el consumo de animales para alimentación, expresaron su alegato en favor de los animales, los testigos avalaron la explicación de los activistas frente a la declaración del director del matadero que incurrió en diversas contradicciones. Los cuatro activistas contaron además con diversos apoyos entre los asistentes.

Desde Igualdad Animal Animal de nuevo, queremos dar las gracias a quienes nos habéis apoyado en este juicio que desgraciadamente no será el último de cuantos tendremos que sufrir quienes sin miedo a las consecuencias defendemos a los demás animales.

<strong>Si deseas ayudarnos a seguir luchando por los animales, <a href="https://igualdadanimal.org/defensores-animales/">participa en nuestras próximas actividades</a> y <a href="https://igualdadanimal.org/dona/">hazte socio/a de Igualdad Animal</a>. Ayúdanos a salvar animales.</strong></p>
<!-- /wp:paragraph -->