<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
En los últimos años, las organizaciones de protección animal han lanzado repetidamente investigaciones encubiertas que revelan el terrible sufrimiento de los animales de granja. Por lo general, para los operadores de las instalaciones investigadas hay muy pocas o ninguna consecuencia, incluso en casos de extrema crueldad hacia los animales y graves violaciones del bienestar animal. En cambio, aquellos que trabajan para arrojar luz sobre estas terribles condiciones están siendo acusados.

Hoy, sin embargo, por primera vez, un Tribunal regional superior en Alemania confirmó la legalidad de una investigación encubierta.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->
<h5 class="wp-block-heading"><strong>¿Qué ha pasado?</strong></h5>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
En 2013, la organización alemana de protección animal Animal Rights Watch (ARIWA) publicó una impactante investigación encubierta en una de las granjas de cría de cerdos más grandes de Alemania, la «van Gennip Tierzuchtanlagen GmbH» en el estado federal de Sajonia-Anhalt. <a href="https://www.flickr.com/photos/animalrightswatch/sets/72157671354550422/with/28812668661/" target="_blank" rel="noopener">Las imágenes revelaron crueldad extrema y violaciones graves al bienestar animal</a>. Mientras que el enjuiciamiento penal de los operadores de la granja había sido abandonado antes de finales de 2015, tres investigadores encubiertos fueron acusados ​​de entrada ilegal en septiembre de 2016.

Habían sido declarados inocentes en primera y segunda instancia. Y hoy, en la audiencia de revisión, el veredicto acaba de ser pronunciado: el Tribunal regional superior del estado federal de Sajonia-Anhalt absolvió a los tres investigadores, confirmando así los dos veredictos anteriores del tribunal de distrito y el tribunal regional.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->
<h5 class="wp-block-heading"><strong>¿Por qué las investigaciones son tan importantes?</strong></h5>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Los animales sufren una agonía insoportable en granjas de engorde y mataderos. Sin la exposición de las violaciones del bienestar animal por parte de investigadores encubiertos, su sufrimiento nunca sería conocido por el público. En Igualdad Animal apreciamos este veredicto y esperamos que envíe un mensaje claro: la compasión no es un crimen. La crueldad hacia los animales debe continuar siendo revelada.</p>
<!-- /wp:paragraph -->