<!-- wp:image {"id":10235} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/03/panfleto-aguilas-negras-bloque-dc_ediima20150312_0157_5.jpg" alt="" class="wp-image-10235"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-13758" src="/app/uploads/2015/03/aguilas.jpg" style="float:right; margin-left:5px; margin-right:5px; width:450px">La Plataforma Alto de Colombia ha informado <a href="http://www.plataformaalto.com/noticias-detalle/paramilitares-amenazan-a-animalistas-en-bogota/183">en su página web</a> de las amenazas que han sufrido representantes del movimiento animalista colombiano por parte del grupo paramilitar 'Águilas Negras Bloque D.C.' Este es el comunicado que han hecho público desde la plataforma:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El 10 de marzo de 2015, representantes del movimiento animalista colombiano estamos siendo objeto de hostigamientos por medio de un panfleto firmado por 'Águilas Negras Bloque D.C.', en el cual nos amenazan de muerte y nos dan plazo de 24 horas para salir del país, señalándonos de defensores del terrorismo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De todas y todos es sabido que el movimiento animalista ha estado proponiendo iniciativas de participación ciudadana para poner fin a la tauromaquia y otras prácticas crueles con animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Asimismo, ha apoyado el proyecto 'Bogotá Humana con la Fauna', manteniendo, sin embargo, su autonomía e independencia con respecto al gobierno distrital.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Finalmente, ha apoyado las iniciativas de paz, en aras de la inclusión de los animales, que también han sido víctimas del conflicto.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por lo tanto, rechazamos de manera categórica la criminalización y amenazas al movimiento animalista que, más allá de cualquier posicionamiento político, trabaja por el respeto a los animales y por una sociedad que viva en armonía con la naturaleza en todas sus expresiones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La lucha del movimiento animalista busca paz y justicia, y por ello prevalecerá sobre el miedo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Natalia Parra, Carlos Crespo, Batman Camargo, Sebastián Párraga, Jesús Merchán, Andrea Padilla</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Bogotá, 11 de marzo de 2015</p>
<!-- /wp:paragraph -->