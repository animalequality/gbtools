<!-- wp:paragraph -->
<p>La Policía Local de Algeciras (Cádiz) detuvo el pasado sábado a J.M.A., de 24 años, al ser propietario de una casa que servía como posible reñidero de peleas de gallos. En la casa donde fue arrestado el individuo se encontraban otras 30 personas en el momento de la detención.
	 
La concejal de Protección Ciudadana y teniente de alcalde del Ayuntamiento gaditano, Cristina Garrido, explicó en un comunicado que los hechos se produjeron a las 16.05 horas, cuando se solicitó presencia policial en el número 8 de la calle Noria.

En el lugar había unas 30 personas, además de un gallo de pelea que presentaba graves heridas, por lo que fue solicitada la comparecencia de un veterinario, a fin de determinar que las lesiones eran producidas por otro animal.

Los agentes recogieron en el lugar un par de protectores de espolones, que habitualmente se utilizan en las riñas de gallos, por lo que procedieron a la detención de J.M.A., natural de Alicante y vecino de Algeciras, como propietario de la casa en la que se desarrollaron los hechos.

Fuente: Panorama-Actual.es</p>
<!-- /wp:paragraph -->