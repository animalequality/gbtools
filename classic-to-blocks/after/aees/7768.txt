<!-- wp:image {"id":11921} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/04/impossible_foods.jpg" alt="" class="wp-image-11921"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Carne, pescado, huevos… Nos gustan estos alimentos. Tenemos hambre de ellos. Sin embargo no nos gusta el calentamiento global, el hambre en el mundo ni el maltrato animal; y las industrias tras ellos son causantes principales de estos problemas globales. ¿Hay solución? La respuesta es sí.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Si te alimentas de forma vegetariana o vegana vas a tener que reconocer que a la población mundial le gusta la carne y no tanto las verduras. Si te alimentas de forma omnívora vas a tener que reconocer que la industria cárnica o pesquera son causantes del maltrato animal y de la destrucción de selvas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Qué hacer?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esa es la pregunta que se hicieron algunas de las mentes empresariales <strong>más brillantes</strong> que hay ahí fuera. La respuesta a la que llegaron colectivamente brilla con luz propia: cambiar a la industria alimentaria con productos similares a carne, pescado, huevos o lácteos, sin animales en el proceso.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>Suscríbete ahora a nuestro e-boletín y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>¿Te suena raro? Míralo así: también le suenan raros nuestros smartphones, portátiles, Facebooks, Instagrams y Snaptchas a nuestros abuelos y abuelas.</p>

<p>A continuación te presentamos 7 empresas dispuestas a <strong>salvar el mundo</strong> cambiando el juego en la industria alimentaria. ¿Quién sabe cuántas más están por llegar?</p>

<p>&nbsp;</p>

<h4>1. Memphis Meats</h4>

<figure><img alt="" class="wp-image-11922" src="/app/uploads/2017/04/maxresdefault.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>«Nuestro objetivo es sencillo: cambiar la forma en la que la carne llega a tu plato. Estamos desarrollando una manera de producir carne real a partir de células animales; sin la necesidad de alimentar, criar y matar a animales.»</p>

<p>Esta es la declaración de principios de la empresa que ha tomado la delantera en el desarrollo de la <strong>«carne limpia»</strong>, carne producida a partir de células con la capacidad de autorreproducirse.</p>

<p>«Esperamos que nuestros productos sean mejores para el medioambiente (su producción requiere hasta un 90% menos de emisiones de gases de efecto invernadero, tierra y agua que que la carne convencional), para los animales y la salud pública. Y lo más importante: están deliciosos.</p>

<p>Parece que en Memphis Meats saben lo que nos gusta, ¿eh?</p>

<p>&nbsp;</p>

<h4>2. Impossible Foods</h4>

<figure><img alt="" class="wp-image-11923" src="/app/uploads/2017/04/impossible_foods_0.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>El equipo de científicos, desarrolladores y, en definitiva, amantes de la carne y la vida tras Impossible Foods lo tiene claro: «Plantas + Ciencia = Carne».</p>

<p>Consiguen el mismo sabor a base de <strong>ingenio y talento</strong>, sin el alto precio de la carne convencional.</p>

<p>«La manera de producir carne hoy en día se está cobrando un alto precio en nuestro planeta. Según las investigaciones del sector, la industria ganadera utiliza hasta el 30% de todo el suelo del planeta, más del 25% de todo el agua potable y genera tantos gases responsables del efecto invernadero como todos los coches, camiones, trenes, barcos y aviones del mundo juntos.»</p>

<p>Bueno, si buscabas buenas razones para elegir alternativas a la carne, ahí tienes algunas.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://www.nytimes.com/es/2016/05/30/espanol/una-empresa-dice-haber-logrado-lo-imposible-una-hamburguesa-vegetariana-que-sabe-a-carne.html" target="_blank">Una empresa dice haber logrado lo imposible: una hamburguesa vegetariana que sabe a carne</a></strong>



<h4>3. New Wave Foods</h4>

<figure><img alt="" class="wp-image-11924" src="/app/uploads/2017/04/newwavefoods.png" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>La pesca industrial está vaciando los océanos a un ritmo tan acelerado que, según los científicos, ¾ de las zonas de pesca están sobreexplotadas o exhaustas y a este ritmo <strong>los océanos se quedarán literalmente vacíos para el año 2048</strong>.</p>

<p>Y en New Wave Foods no están dispuestos a que eso suceda sin intentar evitarlo.</p>

<p>«Usando tecnología, creamos alimentos alternativas al pescado que no requieren ser capturados en los frágiles ecosistemas marinos. Para recrear los alimentos provenientes del mar que la gente ha comido durante siglos nos inspiramos en la madre naturaleza, haciéndolo de una manera más sostenible.»</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h4>4. Tomato Sushi</h4>

<figure><img alt="" class="wp-image-11925" src="/app/uploads/2017/04/tomatosushi.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Nos encanta el sushi pero además <strong>nos gusta ver a los majestuosos atunes en los mares</strong>. Pues Tomato Sushi tiene un mensaje claro para nosotros: «El sushi es alucinante. La extinción no.»</p>

<p>«Saludable y jugoso atún, sin el atún. Tomato Sushi es una alternativa natural y ecológica al atún, creada por uno de los más importantes chefs de Estados Unidos.»</p>

<p><br>
&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="http://elpaissemanal.elpais.com/documentos/la-revolucion-la-carne-cultivada/" target="_blank">La revolución de la ‘carne cultivada’</a></strong>

<p><br>
&nbsp;</p>

<h4>5. Beyond Meat</h4>

<figure><img alt="" class="wp-image-11926" src="/app/uploads/2017/04/beyondburger.png" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>Beyond Meat está entrando en los menús de más y más restaurantes a ritmo de vértigo. ¿Su último éxito? Empezar a vender en la capital mundial del consumo de carne per cápita, Hong Kong.</p>

<p>Y como nos dice Ethan Brown, fundador y director, «En Beyond Meat comenzamos haciéndonos preguntas sencillas: ¿por qué se necesita a un animal para conseguir carne?, ¿por qué no se puede conseguir directamente de las plantas? Y resultó que sí se puede, así que lo hicimos. Esperamos que nuestra carne hecha a base de vegetales os permita a ti y a tu familia comer más (no menos) platos de los que tanto amáis mientras que os sentís genial por los beneficios para la salud, la sostenibilidad y los animales que aportan las proteínas vegetales.»</p>

<p>Desde luego, el público objetivo de Ethan va más allá de <strong>vegetarianos y veganos</strong>: él va a por los más carnívoros del lugar.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h4>6. Hampton Creek</h4>

<figure><img alt="" class="wp-image-11927" src="/app/uploads/2017/04/hamptoncreek.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>Josh Tetrick, director de esta empresa que <strong>ha revolucionado los productos elaborados con huevo como la mayonesa</strong>, conoce muy bien a los lobbys de las industrias alimentarias. Su producto estrella, «Just Mayo», mayonesa sin huevo, fue denunciado por el lobby de la industria del huevo en Estados Unidos. «¿¡Cómo puede llamarse mayonesa a algo que no lleva huevo!?», gritaban desde el multimillonario lobby.</p>

<p>Pues resultó que se puede y además, tuvieron que retirar la demanda.</p>

<p>«Deberíamos amar lo que comemos», reza el lema de Hampton Creek. Sus productos como mayonesas, salsas y masa para galletas saben igual o mejor y cuestan lo mismo. «Eso es lo que son nuestros productos. Y esto es empezar desde abajo para construir un nuevo sistema alimenticio.»</p>

<p><br>
&nbsp;</p>

<strong>Noticia relacionada: <a href="https://www.eleconomista.es/empresas-finanzas/noticias/6397381/01/15/Carnicerias-vegetarianas-a-la-conquista-del-consumidor-global.html">Carnicerías vegetarianas, a la conquista del consumidor global</a></strong>



<h4>7. La carnicera vegetariana</h4>

<figure><img alt="" class="wp-image-11928" src="/app/uploads/2017/04/carnicerav.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Atención porque esta opción <strong>también la puedes encontrar en España</strong>. La carnicera vegetariana ofrece productos alternativos a la carne con un nombre de negocio que deja indiferente a pocos.</p>

<p>«Mi objetivo es liberar a los animales de la cadena alimenticia», nos dice Jaap Korteweg, fundador de la empresa.</p>

<p>«Todos nuestros productos provienen de cultivos amigables con el medio ambiente donde no se talan selvas ni bosques. En la Carnicera Vegetariana también nos preocupamos por establecer una cadena de comercio justo.»</p>

<p><br>
¡Así sí que nos gusta comer carne!</p></font>
<!-- /wp:html -->