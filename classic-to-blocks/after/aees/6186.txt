<!-- wp:paragraph -->
<p><img alt="" class="wp-image-9189" src="/app/uploads/2011/11/perrosasilvestrados.jpg" style="width: 138px; float: left; ">El pasado viernes <strong>11 de noviembre,</strong> el Consejo de Ministros anunció la aprobación de un <strong>Real Decreto</strong> por el que se regula el listado y catálogo español de <strong>especies consideradas “invasoras”.</strong> Este es uno de los pasos dados para promover las <strong>medidas para la erradicación, </strong>básicamente mediante la matanza, de los animales que se considera pertenecen a esta “categoría”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>La aprobación de este catálogo ha sido aplaudida desde el movimiento ecologista.</strong> Organizaciones como Greenpeace, WWF, SEO/Birdlife o Ecologistas en Acción, entre otras, defienden medidas como esta, lo cual es una muestra más de que el movimiento ecologista, que niega un valor intrínseco a los animales y solamente los ve como piezas de un ecosistema, tiene planteamientos y fines diametralmente diferentes al movimiento para la defensa de los animales.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Una parte de la sociedad confunde a dos movimientos muy distintos, el movimiento de defensa de los animales y el ecologismo. Estos hechos ponen de manifiesto las claras diferencias entre ambos. Quienes defendemos a los animales tenemos en cuenta a todos y cada uno de los animales, con independencia de su ecosistema, y de si pertenecen a especies autóctonas o alóctonas. Por ese motivo, <strong>desde el movimiento que defiende a los animales no podemos más que mostrar nuestro total rechazo ante la aprobación de este catálogo, que supondrá&nbsp;la muerte de incontables animales.&nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las organizaciones de defensa animal conocemos perfectamente los argumentos ecologistas a favor de la erradicación de animales “invasores”, pero los rechazamos por considerarlos inmorales. Debemos tener en cuenta que el impacto de los seres humanos en el medio es incomparablemente mayor que el de los animales de otras especies. Sin embargo, la matanza de seres humanos con este fin jamás sería aceptada, debido a que, por encima de cualquier supuesto valor medioambiental, ha de prevalecer el respeto por nuestros prójimos. A día de hoy se daña y se masacra sistemáticamente a los animales en todos aquellos casos en los que hay un interés humano en hacerlo. &nbsp;El motivo es que no se tiene ningún respeto por los intereses de los animales. Pero ellos, al igual que nosotros, también poseen la capacidad de sufrir y disfrutar, y, por lo tanto, un interés en no ser dañados ni matados. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El hecho de que no pertenezcan a nuestra especie o no tengan ciertas capacidades intelectuales no es una razón para no respetarlos, así como no lo es para discriminar a aquellos seres humanos que no poseen tales capacidades.&nbsp; Por estos motivos, las organizaciones dedicadas a la defensa de los animales no podemos aceptar de ningún modo esta medida. <strong>Manifestamos nuestra oposición al lanzamiento de cualquier campaña de matanza masiva de animales por motivos ecológicos</strong>, y anunciamos el lanzamiento de movilizaciones de protesta contra esta medida, y de promoción del respeto por todos los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Organizaciones firmantes:&nbsp; </strong></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br>			<em><em>Alicante Animalista</em></em></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<em>Askekintza-Liberacción Animalista</em></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<em>Bloque Antiespecista de Euskad</em>i</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<em><em><em><em><em><em>Centro Legal para la Defensa de los Animales</em></em></em></em></em></em></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<em><em><em><em>Equanimal</em></em></em></em></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<em><em><em>Fauna Libre</em></em></em></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<em>Igualdad Animal</em></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<em>Partido Animalista–PACMA</em></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<em>Santuario Wings of Heart</em></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->