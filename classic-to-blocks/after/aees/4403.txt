<!-- wp:paragraph -->
<p>Santa Cruz de Tenerife (Canarias) dirá adiós en unos pocos años a su emblemática plaza de toros. El Ayuntamiento capitalino y los propietarios del recinto taurino han alcanzado un acuerdo y en menos de un mes se conocerán las propuestas de varios arquitectos para urbanizar la manzana que ahora ocupa en pleno centro de la ciudad.

En 20 días un concurso de ideas convocado por el Ayuntamiento de Santa Cruz de Tenerife decidirá el futuro urbanístico de la manzana que ocupa en pleno centro de la ciudad. La manzana que ahora ocupa la plaza de toros, a menos de 500 metros del parque García Sanabria, tendrá distintos usos (residencial, comercial, aparcamientos), pero en el concurso primará el público, teniéndose especialmente en cuenta a la hora de puntuar las ideas de los arquitectos que se presenten la manera en la que se haga viable el uso de equipamiento recreativo y cultural que históricamente ha tenido el recinto actual. 

El jurado del concurso hará público previsiblemente el próximo 7 de julio, día de San Fermín, la idea ganadora del concurso. El resultado será la ordenación urbanística a incorporar en el documento de revisión del Plan General de Ordenación Urbana (PGOU) de la ciudad, cuyo plazo legal concluye el próximo mes.

El jurado que elija la mejor idea arquitectónica para urbanizar la manzana que ocupa la plaza de toros será eminentemente técnico, pero también abierto a la participación ciudadana. Además de arquitectos, técnicos y representantes sociales, los vecinos podrán votar durante tres días las propuestas. La votación popular valdrá un voto.

Fuente: Canarias7.es</p>
<!-- /wp:paragraph -->