<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
A través de un fallo sin precedentes, el Tribunal Superior del estado de Uttarakhand, ubicado a 320 kilómetros de Nueva Delhi, India, otorgó recientemente el estatus de «persona o entidad legal» a los animales.

La decisión tiene como finalidad promover un mayor bienestar para los animales, incluídas las aves y los animales marinos y, para ello, dicho estatus ha sido otorgado junto con la presentación de una serie de instrucciones para prevenir la crueldad contra los animales.

&nbsp;

Los nativos de Uttarakhand han sido también declarados guardianes de los animales con el deber de garantizar su bienestar, según los jueces Rajiv Sharma y Lokpal Singh. La jurisprudencia india incluye dos tipos de personas: las físicas o humanas y las personas jurídicas. A través de este dictamen, a los animales se les ha otorgado la condición de persona jurídica.

Entre las instrucciones para prevenir la crueldad se encuentran las regulaciones sobre el transporte y encierro de animales. De acuerdo a las mismas, no se permitirá que bajo una temperatura superior a los 37 grados centígrados o por debajo de los 5 sean transportados animales en vehículos o que sean mantenidos en un lugar de encierro.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large;">¿Quieres recibir las mejores noticias de actualidad sobre los animales? </span></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large;">¡Suscríbete gratuitamente a <a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958"><span style="color: #0000ff;">nuestro e-boletín</span></a>!</span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Luego de la sentencia, será también obligatoria la instalación de reflectores fluorescentes en vagones y encierros de animales, los certificados de peso en vacío de los vehículos y los refugios en todas las ciudades adecuados para caballos, bueyes y otros animales que transitan por las calles.

Además, también se giró una dirección a los médicos veterinarios de Uttarakhand para tratar a los animales que viven en las calles. Los carros tirados por caballos&nbsp; serán eliminados progresivamente. Estos animales deberán ser entregados a organizaciones animalistas para su rehabilitación.</p>
<!-- /wp:paragraph -->