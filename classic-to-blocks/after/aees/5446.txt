<!-- wp:paragraph -->
<p>
	<img align="left" alt="" height="200" hspace="9" class="wp-image-14248" src="/app/uploads/2010/04/sabater.jpg" vspace="9" width="200">El filósofo Fernando Savater ha vuelto a manifestar su opinión sobre la tauromaquia al ser preguntado en la apertura del ciclo "Los retos de la ciencia para el siglo XXI", en el Museo de León.<br>
	<br>
	Ante la pregunta “¿Y qué opina usted sobre la polémica petición de que se supriman los toros?”, Savater respondió:<br>
	<br>
	Mi opinión es que desde luego no hay que prohibirlos. Los animales no son humanos, así que esta idea de que son como nosotros, pues no. Son seres vivos pero no tenemos obligaciones morales con ellos, sino quizá otro tipo de obligaciones. Entiendo que a uno no le gusten los toros como a otro no le gusta el fútbol, pero lo que creo que no se puede hacer es prohibirlos.<br>
	<br>
	Comentario de Igualdad Animal: Los animales no son como nosotros en muchos aspectos. Algunos son más fuertes que nosotros, otros son más veloces, otros se adaptan mejor a situaciones extremas... incluso algunos animales son más inteligentes que algunos humanos, como por ejemplo los cerdos, cuya inteligencia es superior a la de los bebés humanos recién nacidos. Pero del mismo modo que existen esas diferencias, también existen similitudes: los animales, al igual que nosotros, poseen sistema nervioso y les afectan los actos de los demás; experimentan dolor, placer, alegría estrés, aburrimiento, y todo tipo de sentimientos y emociones, así como el deseo de continuar viviendo. Y precisamente, al hablar de consideración u obligaciones morales, las similitudes que tenemos con los animales son infinitamente más relevantes que las diferencias.<br>
	&nbsp;</p>
<!-- /wp:paragraph -->