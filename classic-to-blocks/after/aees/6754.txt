<!-- wp:image {"id":9740} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/10/21_dias_toreando_p_0.jpg" alt="" class="wp-image-9740"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>El próximo <strong>jueves 18 de octubre</strong> se estrenará en Cuatro, dentro del programa “21 días”, el reportaje <strong>“21 días toreando”</strong>. En él, la periodista Adela Úcar nos mostrará cómo es la vida de un torero, poniéndose durante 3 semanas en su lugar y <strong>aprendiendo a dominar, torturar y matar toros y vaquillas </strong>bajo la supervisión de “profesionales” como El Juli o Juan José Padilla.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No es la primera vez que este programa nos muestra cómo es la vida de quienes acosan, torturan y matan a los animales. Así, en marzo de 2011, <a href="http://www.mitele.es/programas-tv/21-dias/temporada-3/programa-21/" target="_blank">Adela convivió 21 días con cazadores profesionales</a>, aprendió a manejar un arma y participó en diferentes monterías e incluso fue de safari a África, disparando a jabalíes, perdices y ciervos y <strong>acabando con la vida de varios animales durante el rodaje.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13630} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/10/21_dias_caza.jpg" alt="" class="wp-image-13630" title="Adela Úcar durante &quot;21 días de caza&quot; tras matar a un ciervo"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Creemos que en televisión <strong>no todo vale</strong> <strong>con tal de obtener audiencia</strong>. Así, fomentar prácticas que causan sufrimiento a otros individuos, como sucede con la caza y la tauromaquia, además de no aportar ningún valor positivo a la sociedad, promueve explícitamente la violencia hacia los animales como algo “normal” y perfectamente justificable.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según declaraciones de la propia Adela Úcar al respecto de la función del programa 21 días, <em>«Hay problemas que sólo se entienden cuando se viven en la propia piel. Como no es lo mismo contarlo que vivirlo».</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Inspirándonos en las propias palabras de Adela, <strong>solicitamos a la cadena Cuatro y a su periodista que se pongan por un momento en la piel de los animales heridos y asesinados durante la realización de sus reportajes</strong> y reflexionen sobre qué necesidad había de causarles tantísimo daño. Pedimos, tanto a la cadena como a la propia Adela Úcar, que se pongan también en la piel de quienes dedicamos nuestra vida a defender a las víctimas de la caza, a las víctimas de la tauromaquia y a las víctimas de cualquier forma de explotación animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Desde <strong>Igualdad&nbsp;Animal</strong> invitamos al programa 21 días a conocer <strong>cómo es la vida de un activista por los derechos animales</strong>, ofreciéndonos a acompañar a Adela durante su reportaje para que descubra, en primera persona, qué motiva a alguien a invertir su tiempo y sus energías de forma desinteresada defendiendo a individuos que no son de su propia especie.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13631} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/10/21_dias_toreando.jpg" alt="" class="wp-image-13631"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">*Petición finalizada. Gracias a todos los que habéis colaborado.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->