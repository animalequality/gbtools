<!-- wp:paragraph -->
<p>Tras haber hecho públicas <a href="https://www.youtube.com/watch?v=zs8E9CwVRWg" target="_blank" rel="noopener">impactantes imágenes de maltrato anima</a>l en una granja de cerdos de Fir Tree en Lincolnshire, propiedad de Elsham Linc, uno de los productores de carne de cerdo más grandes de Gran Bretaña, tres trabajadores de dicha granja <strong>han sido acusados de delitos contra el bienestar de los animales.</strong>

Nuestras cámaras encubiertas capturaron cómo los empleados pateaban a cerdos en la cara y en la cabeza, a menudo violenta y repetidamente, con más de 100 incidentes de patadas registrados en 10 días. También, los pinchaban repetidas veces con rastrillos de limpieza o <strong>golpeaban sus cabezas contra puertas o tablas de plástico.</strong> A los cerdos heridos o gravemente enfermos los golpeaban para forzarlos caminar, o los abandonaban a una dolorosa agonía por días sin proporcionarles asistencia veterinaria.

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
&nbsp;

Todo esto ocurría mientras los trabajadores se burlaban del maltrato en medio de risas y ofensas contra los cerdos, siendo esta la tercera vez que desde Igualdad Animal filmamos a trabajadores abusando de animales en granjas británicas.

Los tres empleados <strong>han sido acusados de infligir daño por fuerza brusca, violencia física</strong>, uso inadecuado de rastrillos de limpieza y de causar un sufrimiento innecesario a un animal protegido, todo tras una acusación presentada por la RSPCA luego de que emprendieran una investigación a partir de las evidencias que les entregamos.

Fuentes:

<a href="https://www.bbc.com/news/uk-england-lincolnshire-46017860?intlink_from_url=&amp;link_location=live-reporting-story" target="_blank" rel="noopener">1</a><a href="https://metro.co.uk/2018/10/29/three-men-charged-after-pigs-beaten-with-forks-and-kicked-in-the-head-8085703/" target="_blank" rel="noopener">2</a></p>
<!-- /wp:paragraph -->