<!-- wp:paragraph -->
<p>Madrid- Según informa ADN en su versión digital, el SEPRONA ha desmantelado una red de caza furtiva, detenido a siete personas.

En los registros se intervinieron 2 silenciadores, una carabina de aire comprimido, miras telescópicas, cartuchos para armas de gran calibre, numerosos cuchillos de montería, machetes, ropa de camuflaje y navajas multiusos entre otros objetos.

Además, los cazadores tenían en sus domicilios diferentes restos de los cuerpos de los animales a los que asesinaban, como colmillos de jabalí, cráneos de antílopes, búfalos, cabras y zorros así como un frigorífico, ubicado en un garaje, con 5 cabezas de rebeco, 18 de corzo, 3 de jabalí, 1 de lobo, dos jinetas, 1 zorro, 1 gato montés, 2 ánades reales, 1 perdiz y 1 turón.

Los cabecillas de la red eran dos hermanos y la mujer de uno de ellos, así como tres clientes de Málaga, que son empresarios. A través del boca a boca o mediante revistas especializadas, se ponían en contacto con gente interesada en matar animales protegidos, por lo que llegaban a pagar hasta 18.000 euros.

A pesar de la buena noticia que supone el hecho de que muchos animales no serán asesinados por estas personas, no podemos olvidar que muchos otros lo son cada día debido a la práctica de la caza. Y es que matar a un animal que no se encuentre en peligro de extinción, está perfectamente permitido, a pesar de que todos los animales tienen interés en vivir sus vidas y merecen que ese interés sea tenido en cuenta.

Más información sobre la caza:
http://www.igualdadanimal.org/entretenimiento/caza</p>
<!-- /wp:paragraph -->