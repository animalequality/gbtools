<!-- wp:image {"id":9793} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/11/caracierzos_pico_roto.jpg" alt="" class="wp-image-9793"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Igualdad&nbsp;Animal</strong> presenta hoy miércoles 21 de noviembre su última investigación sobre la industria del foie gras, esta vez realizada en la granja de <strong>Caracierzos</strong>, la segunda granja de foie gras con mayor producción de España. Las imágenes obtenidas muestran las irregularidades y escenas de sufrimiento animal que conlleva un producto que llega a las estanterías de supermercados como&nbsp;<strong>Carrefour</strong>, <strong>Eroski </strong>o <strong>Alcampo </strong>y a las mesas de <strong>restaurantes </strong>de chefs tan conocidos<strong>&nbsp;</strong>como <strong>Arzak y Berasategui.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta es la cuarta investigación presentada por Igualdad Animal durante 2012 sobre la industria del foie gras. En estos últimos meses Igualdad Animal ha presentado investigaciones llevadas a cabo en las&nbsp;granjas de foie gras de <a href="https://igualdadanimal.org/noticia/2012/07/05/igualdad-animal-destapa-la-crueldad-en-las-granjas-de-foie-gras-de-espana/" target="_blank">Catalunya</a>, <a href="https://igualdadanimal.org/noticia/2012/07/15/el-secreto-del-foie-gras-de-mugaritz-al-descubierto/" target="_blank">Euskadi </a>y del sur de <a href="https://igualdadanimal.org/noticia/2012/07/24/igualdad-animal-presenta-una-nueva-investigacion-en-granjas-de-foie-gras-francesas/" target="_blank">Francia</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En esta ocasión, la granja investigada pertenece a la empresa <strong>“Caracierzos SL”</strong>, situada en la localidad de Santa Eulalia del Campo en la provincia de Teruel, y proveedora de las empresas <a href="http://www.collverd.com/cast/home.htm" target="_blank">Collverd </a>y <a href="https://martiko.com/" target="_blank">Martiko</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Collverd</h4>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-13895" src="/app/uploads/2012/11/collverd.jpg"><strong>Collverd </strong>comercializa sus productos derivados del foie gras a nivel nacional en tiendas especializadas y restaurantes y exporta a Francia, Holanda, Luxemburgo, Bélgica, México, Perú, Colombia y China.&nbsp;Es importante a este respecto destacar que el actual director de Collverd es Jordi Terol, presidente de la Interpalm (Asociación Interprofesional Agroalimentaria de Palmípedas Grasas) y vicepresidente de EuroFoieGras (Asociación europea de productores de foie gras).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Con esta nueva investigación son ya tres las veces en lo que va de año que Igualdad Animal pone de manifiesto la relación de <strong>Jordi Terol </strong>con las irregularidades y la crueldad inherente a la industria del foie gras.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El pasado 5 de julio de 2012 Igualdad Animal dió a conocer las <a href="https://vimeo.com/45218406" target="_blank">imágenes grabadas en el matadero de Collverd</a> que mostraban a patos siendo acuchillados mientras estaban todavía plenamente conscientes, algo que incumple las normativas en materia de bienestar animal. Ese mismo día la organización también presentó evidencias gráficas de las irregularidades y el sufrimiento de los animales de la granja Can Ruet que, al igual que Caracierzos, suministra hígados de patos a Collverd.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A la vista de estos hechos recientes, desde Igualdad Animal consideramos que estamos ante un escándalo para la industria de foie gras europea ya que Jordi Terol es el máximo representante de la industria de foie gras en España y vicepresidente de Eurofoiegras y su responsabilidad ante estos hechos, por tanto, es indudable.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Martiko</h4>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-14100" src="/app/uploads/2012/11/logoMartiko.png">La otra empresa cliente de Caracierzos es el grupo navarro <strong>Martiko</strong>,<strong>&nbsp;</strong>que acapara más del 70% de la cuota del mercado español en el sector. El 80% de la producción de foie gras de Martiko es distribuido y comercializado en grandes superficies como Eroski, Alcampo, Mercadona, Carrefour, El Corte Inglés o Makro, establecimientos “gourmet” y restaurantes tan conocidos como Arzak y Berasategui, el restaurante Akelarre del chef Pedro Subijana y el restaurante Zuberoa.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Martiko exporta el 20% restante de su foie a Francia, aunque también llega a Alemania e Italia, India, China o Japón y América Latina.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Hallazgos de la investigación en Caracierzos</h4>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>Durante esta reciente investigación Igualdad Animal documentó todo el proceso de explotación de los patos para la producción de foie gras en la granja de Caracierzos desde el momento en que los patos llegan a la granja con unos días de vida hasta las condiciones en la que se encuentran los animales durante las dos semanas en que son alimentados a la fuerza.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Equipo de Investigaciones de Igualdad Animal ha registrado tanto en vídeo como mediante fotografías el sufrimiento que padecen los animales en esta granja. Entre otros hechos hemos documentado:</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph -->
<p>• <strong>Un pato cubierto de sangre</strong> y con el <strong>pico roto </strong>que permaneció varios días –grabado hasta en tres días diferentes– sin ningún tipo de tratamiento veterinario.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• Patos con <strong>infecciones oculares</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• Patos tratando de salir de las jaulas individuales en las que se encontraban y que debido a ellas presentaban serias <strong>dificultades para moverse</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• Patos con <strong>comportamientos repetitivos</strong> en las jaulas lo que es un posible indicador de <strong>estrés</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• Patos con <strong>dificultades para respirar</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• <strong>Patos muertos dentro y fuera de las jaulas </strong>e incluso un pato muerto cuya cabeza yacía dentro del bebedero.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p><strong>► Advertencia: Este vídeo contiene imágenes de sufrimiento animal.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="325" mozallowfullscreen="" scrolling="no" src="https://player.vimeo.com/video/53847809?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=c0e7f8" webkitallowfullscreen="" width="580"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Denuncias interpuestas</h4>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>Igualdad Animal hace pública hoy esta investigación llevada a cabo en la granja Caracierzos a la vez que anunciamos que hemos interpuesto las denuncias correspondientes por vía administrativa contra ella por las siguientes irregularidades documentadas durante el transcurso de nuestra investigación.</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph -->
<p>• Indicios de desatención veterinaria. Se documentaron en repetidas ocasiones diversos problemas de salud en los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• Instalaciones de cebado de la granja que carecen de un sistema de filtrado de aire y/o ventanas. Los animales son retenidos en construcciones de plástico en la que no entra luz natural.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• Instalaciones de cebado que carecen de un sistema de comederos y bebederos que impida el contagio de enfermedades.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• Instalaciones de cebado que carecen de equipos y controles adecuados para la limpieza y desinfección de los vehículos que entran y salen de las instalaciones.</p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p>Es importante destacar además que Caracierzos mantiene a los patos encerrados en<strong> jaulas individuales</strong> durante el proceso de cebado, <strong>práctica que prohíbe expresamente la normativa europea</strong>. Esta ilegalidad fue denunciada por Igualdad Animal el pasado mes de agosto de 2012.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13992,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157631698965555/show/" target="_blank"><img src="/app/uploads/2012/11/galeria_caracierzos.jpg" alt="" class="wp-image-13992" title="Galería Flickr de la investigación"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Repercusión de nuestras investigaciones en granjas de foie gras</h4>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>Queremos recordar además que el pasado 17 de octubre <a href="https://igualdadanimal.org/noticia/2012/10/17/eurodiputados-exigen-hoy-en-bruselas-la-prohibicion-del-foie-gras-en-europa/" target="_blank">ocho eurodiputados exigieron la prohibición de foie gras en Europa</a> a través de una iniciativa en la que informaron de las diferentes investigaciones realizadas por Igualdad Animal en granjas de foie gras españolas y francesas y animaron a la sociedad a apoyar nuestra petición de prohibición de la venta de foie gras en todo el territorio europeo. También queremos destacar que a raíz de nuestras investigaciones, <strong>la mayor cadena de supermercados italiana, COOP</strong>, que cuenta con 1.471 puntos de venta en toda Italia, nos informó el pasado 31 de octubre que <a href="https://igualdadanimal.org/noticia/2012/11/01/una-gran-cadena-italiana-deja-de-vender-foie-gras/" target="_blank">habían decidido dejar de vender foie gras.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Desde Igualdad Animal aplaudimos estas iniciativas e instamos a las administraciones a que prohíban la producción de foie gras en España como ya está prohibido por ser considerado maltrato y crueldad hacia los animales en Argentina, Austria, Dinamarca, República Checa, Finlandia, Israel, Turquía, Alemania, Irlanda, Italia, Luxemburgo, Noruega, Polonia, Suecia, Suiza, los Países Bajos, Reino Unido y más recientemente en California (Estados Unidos) donde, además de su producción, se ha prohibido también su consumo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Igualdad Animal es una organización internacional de defensa de los animales presente en España, Alemania, India, Inglaterra, Italia, México y Venezuela cuyas investigaciones han obtenido repercusión internacional en reputados medios como la BBC, The Times o Il Corriere della Sera entre otros.</p>
<!-- /wp:paragraph -->