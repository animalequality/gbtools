<!-- wp:paragraph -->
<p>Costa Rica prohibirá definitivamente los parques zoológicos en todo su territorio. La nueva legislación entrará en vigor en 2014 y supondrá la reconversión de los dos únicos centros públicos de este tipo que existen en el país.<br>
	<br>
	<em>«<strong>Estamos mandando un mensaje al mundo</strong>. Queremos ser congruentes con nuestra visión de país que protege a la naturaleza»</em>, afirma Ana Lorena Guevara, viceministra de Medio Ambiente de Costa Rica.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las instalaciones de los zoológicos serán cedidas para la creación de un <strong>jardín botánico de orquídeas</strong>. El objetivo principal de los jardines será la entrada de aves y otras especies de forma natural de forma que los ciudadanos puedan contar con un parque natural urbano en el que los animales se muevan libremente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14093} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/08/leon_jinterwas.jpg" alt="" class="wp-image-14093"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«<em>El Ministerio tiene la responsabilidad de responder al <strong>aumento de conciencia ambiental del costarricense, que ya no quiere ver animales enjaulados</strong>. Esa es una idea antigua que ya no va más con los costarricenses, lo pudimos haber hecho antes, pero nunca es tarde»</em>, manifestó el ministro de Medio Ambiente, René Castro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Costa Rica también fue el país pionero en prohibir la cautividad de cetáceos como delfines y ballenas. Esta ley se aprobó en julio de 2005, desde entonces se ha logrado que países como Chile, Hungría y recientemente India se unan a la prohibición.</p>
<!-- /wp:paragraph -->