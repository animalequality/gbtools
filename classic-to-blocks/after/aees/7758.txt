<!-- wp:image {"id":11896} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/04/8701249509_a7673bc32e_z.jpg" alt="" class="wp-image-11896"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Altas multas, elevadas penas de cárcel y difusión pública de fotografías para avergonzamiento de los infractores: Taiwán está a un paso de convertirse en el primer país asiático que prohíba el consumo de carne de perro y gato.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La pionera medida en el continente asiático puede sentar un importante precedente y es consecuencia de <strong>un cambio de actitud en toda Asia</strong> hacia perros y gatos. Estos animales han pasado a ser vistos como compañeros y amigos, sobre todo por las generaciones jóvenes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>La nueva Ley de Bienestar Animal contempla multas de 250.000 dólares taiwaneses (7.700€) a quienes coman carne de perro o gato. Además, las penas por crueldad o sacrificio ilegal se ha elevado hasta dos millones de dólares taiwaneses (60.000€) y dos años de prisión.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://elpais.com/sociedad/2013/10/12/actualidad/1381604379_482010.html" target="_blank">Cae una red en China que traficaba con perros y gatos para consumir su carne</a></strong>

<p>&nbsp;</p>

<p>Pero eso no es todo, aquellas personas que infrinjan la nueva ley de forma repetida podrían ser <strong>encarceladas hasta cinco años</strong> y multadas con cantidades aún mayores. También se contempla que el Gobierno distribuya públicamente fotografías de los infractores para avergonzarlos.</p>

<figure><img alt="" class="wp-image-11897" src="/app/uploads/2017/04/8692654362_0692fb006e_z.jpg" style="width: 450px; margin-left: 10px; margin-right: 10px; float: left;"></figure><p></p>

<p>&nbsp;</p>

<p>La carne de perro <strong>no es consumida ampliamente en el continente asiático</strong> y la de gato aún menos. Son solo ciertas cocinas regionales, enraizadas en la tradición, las que continúan utilizándola. En el caso de la isla de Taiwán, perros y gatos son vistos de forma pública como compañeros y amigos.</p>

<p>De hecho, la presidenta taiwanesa, Tsai Ing-wen es públicamente conocida como una <a href="https://verne.elpais.com/verne/2016/01/22/articulo/1453481684_587442.html" target="_blank">amante de los gatos</a> y el año pasado <strong>adoptó a tres perros guías ya retirados</strong>. Durante la pasada campaña electoral Tsai posó repetidamente con sus queridos gatos.</p>



<p>&nbsp;</p>

<font size="1"><font color="#808080"><p>Vita, la perrita rescatada por Igualdad Animal de un matadero de carne de perro en China. <a href="https://www.youtube.com/watch?v=4zWfse-aP0k&amp;feature=youtu.be" target="_blank">Aquí</a> puedes ver el emocionante vídeo de su rescate.</p></font>


<strong>Noticia relacionada: <a href="https://www.elmundo.es/ciencia/2013/12/13/52ab4efd61fd3dd2348b457a.html" target="_blank">Por qué queremos a los perros pero nos comemos a los cerdos</a></strong>

<p>&nbsp;</p>

<p>Taiwán <strong>ya había prohibido</strong> la venta de carne de perro y gato en 2001 y, a nivel regional, algunos gobiernos ya habían prohibido su consumo. Otro estado asiático, Hong Kong, ya había prohibido la muerte y la venta de carne de perro, pero no así el consumo.</p>

<p>Igualdad Animal ha llevado a cabo <a href="https://www.sinvoz.org/" target="_blank">distintas investigaciones</a> en el oscuro comercio de carne y pieles de perro y gato en China. En 2013 y 2015 el equipo de investigaciones lanzó impactantes vídeos y una campaña pública que <strong>recogió más de medio millón de firmas</strong> que fueron entregadas en la Embajada China en España.</p>

<p><br>
<br>
Fuente: <a href="https://www.theguardian.com/world/2017/apr/12/taiwan-bans-dog-and-cat-meat-from-table-as-attitudes-change" target="_blank">https://www.theguardian.com/world/2017/apr/12/taiwan-bans-dog-and-cat-meat-from-table-as-attitudes-change</a></p></font></font>
<!-- /wp:html -->