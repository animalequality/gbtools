<!-- wp:paragraph -->
<p>Un hombre de 26 años que responde a las iniciales I. G. ha sido acusado por la Policía Municipal de Tarragona de someter a una agonía de varias semanas a su perro y dejarlo morir de inanición. La policía local ha explicado que el animal estuvo encerrado en el balcón del piso de I. G., que no le dio comida ni agua durante tiempo, por lo que fue debilitándose hasta que no pudo ladrar y, ya exhausto, falleció.

"Lo hizo en represalia porque habíamos roto nuestra relación", dijo la exnovia de I. G. al testificar ante los agentes. La chica añadió que hacía meses que su excompañero desatendía al perro, de seis años. Sin embargo, todo apunta a que el suplicio del perro habría podido evitarse si las quejas de los vecinos hubieran sido atendidas. Uno de ellos desveló que avisaron varias veces al ayuntamiento "pero tardaron en venir".

La policía local reconoció que la exnovia y un residente del edificio habían denunciado la situación. Por ello, veterinarios del laboratorio municipal se trasladaron al domicilio del propietario del perro el pasado 8 de octubre, aunque éste hizo caso omiso a las llamadas y no abrió la puerta. Los técnicos no oyeron nada sospechoso y tampoco divisaron al animal en el balcón. El día 12, un vecino telefoneó y advirtió a los servicios municipales de que el perro estaba muerto. 


INTENTO DE JUSTIFICACIÓN

Como justificación de lo sucedido I. G. argumentó que el animal padecía leismaniasis, una enfermedad casi siempre incurable y provocada por una picadura de mosquito. Y aseguró que "no se podía hacer nada por él". Los veterinarios analizaron el cadáver e informaron de que no estaba aquejado de ninguna dolencia conocida y que el óbito se produjo por malos tratos continuados. Tras disponer del informe, los policías locales iniciaron el correspondiente atestado, que derivaron al juzgado. 


Fuente: elperiodico.com</p>
<!-- /wp:paragraph -->