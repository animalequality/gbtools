<!-- wp:paragraph -->
<p>¿Puede producirse carne real que no conlleve la muerte de ningún animal? ¿Y cuya producción sea sostenible y satisfaga la creciente demanda mundial de proteínas? Sí, se llama «carne limpia» y ya son varias las empresas alrededor del mundo que desde hace años vienen trabajando en desarrollar tecnología para elaborarla. Algunas de ellas como <a href="https://upsidefoods.com/" target="_blank" rel="noopener">Memphis Meats</a> cuentan con el apoyo de inversionistas de la talla de Bill Gates y Richard Branson.

<strong>Te gustará:<a href="https://igualdadanimal.org/noticia/2018/08/15/la-carne-limpia-llegara-pronto-los-supermercados-y-muchos-consumidores-les-encantaria/"> La «carne limpia» llegará pronto a los supermercados y a muchos consumidores les encantaría probarla</a></strong>

En España, una empresa vasca llamada <a href="https://www.biotech-foods.com/" target="_blank" rel="noopener">Biotech Foods </a>también <strong>ha desarrollado la tecnología que redefinirá por completo la producción de alimentos </strong>y que consiste en alimentar a células en condiciones óptimas para que crezcan y se reproduzcan generando la carne equivalente a la que consumimos actualmente, pero sin el impacto medioambiental ni el sufrimiento animal que ésta conlleva.

<strong>«Está buena, está sabrosa y es sostenible».</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><img class="wp-image-13379" style="float: right; margin-left: 10px; margin-right: 10px; width: 450px;" src="/app/uploads/2018/10/carne_limpia_espana.jpg" alt=" Así es como lucirán los productos de Biotech Foods.">Mercedes Vila, investigadora y una de las fundadoras de la empresa, confiesa que se sentía responsable de hacer algo para contribuir con sus conocimientos creando&nbsp;alternativas que transformen nuestro obsoleto sistema de producción en uno eficiente y sostenible. La producción de carne genera el 15% de los gases de efecto invernadero y ocupa el &nbsp;50% de la superficie del planeta, mientras que la de «carne limpia» <strong>requiere un 99% menos de tierra, un 90% menos de agua y emite un 90% menos de gases de efecto invernadero</strong>, y todo esto sin implicar el sufrimiento y muerte de ningún animal.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong>Biotech Foods es la primera empresa en España en desarrollar esta tecnología</strong> y lo hace con un proceso que es totalmente natural, sin el uso de químicos o la manipulación genética y que permite que las células crezcan a su ritmo. «Está buena, está sabrosa y es sostenible», asegura Vila. La empresa espera salir al mercado en 2021 y para eso se encuentra actualmente en la búsqueda de inversores estratégicos.

<a href="https://innovadores.larazon.es/es/not/esta-es-la-primera-empresa-espanola-que-fabrica-carne-cultivada" target="_blank" rel="noopener">Fuente</a></p>
<!-- /wp:paragraph -->