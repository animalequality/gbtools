<!-- wp:paragraph -->
<p>
	Un simple frenazo de un camión provocó ayer por la tarde un espectáculo dantesco en la carretera vieja de Santiago, al volcar parte de la carga que llevaba: cientos de kilos de restos de cadáveres de animales, que quedaron sembrados a un lado y otro de la calzada provocando, primero un corte de tráfico de varias horas en la zona. La carga -restos de animales descuartizados entre la que había un cadáver entero de una vaca- se esparció a 200 metros del empalme de Portomarín, en dirección al cementerio, y bajó hasta 100 metros más adelante.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	El camión se dirigía hacia Lugo y venía de un matadero de Rábade. Los que osaron acercarse al lugar del accidente pudieron presenciar restos de cuerpos de vacas y ovejas. Precisamente, una vaca que estaba entera fue trasladada por una empresa especializada en el tratamiento de este tipo de cadáveres con el fin de incinerar, posteriormente, el cuerpo en la planta de Cerceda. Personal de Urbaser se encargó de recoger, con un tractor, los restos que quedaron esparcidos en la calzada y de limpiar y desinfectar la zona. Posteriormente, la empresa que transportaba la mercancía recogió los restos de nuevo con el objetivo de ser tratados, según la Policía Local.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	El accidente provocó el corte total de tráfico de la carretera vieja de Santiago durante parte de la tarde. Posteriormente, se habilitó un carril. La Policía reguló el tráfico en la zona hasta las nueve menos cuarto de la noche. Los trabajos de limpieza duraron varias horas, también hasta la noche.</p>
<!-- /wp:paragraph -->