<!-- wp:image {"id":10312} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/10/cerdos.jpg" alt="" class="wp-image-10312"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":11365} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/granjaindustrialcerdos.jpg" alt="" class="wp-image-11365"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los animales son las mayores víctimas de la historia, y el trato que se da en las <a href="https://igualdadanimal.org/noticia/2015/09/25/8-crueles-practicas-estandar-de-la-industria-lactea-con-las-vacas/">granjas industriales</a> a los animales domesticados es tal vez el peor crimen de la historia. La marcha del progreso del ser humano está salpicada de animales muertos. Incluso hace decenas de miles de años, nuestros ancestros de la edad de piedra fueron ya responsables de toda una serie de desastres ecológicos. Cuando los primeros humanos llegaron a Australia, hace alrededor de 45.000 años, rápidamente llevaron a la extinción al 90% de los grandes animales. Este fue el primer impacto significativo del Homo sapiens en el ecosistema del planeta. No fue el último.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hace unos 15.000 años los humanos colonizaron América, aniquilando en el proceso a alrededor del 75% de los grandes mamíferos. Otras numerosas especies desaparecieron de África, Eurasia y de la miríada de islas a lo largo de sus costas. Los registros arqueológicos de un país tras otro cuentan la misma triste historia. La tragedia se inicia mostrando una escena repleta de una variada población de grandes animales, sin rastro del Homo sapiens. En la segunda escena, aparecen los humanos, evidenciados por algún hueso fósil, alguna punta de lanza, o quizás una fogata. Rápidamente sigue la tercera escena, en la que hombres y mujeres ocupan el centro del escenario y desaparecen la mayoría de grandes animales junto a muchos otros más pequeños. En total, sapiens llevó a la extinción a alrededor del 50% de todos los grandes mamíferos terrestres del planeta antes incluso de haber plantado el primer campo de trigo, forjado la primera herramienta de metal, escrito el primer texto o acuñado la primera moneda.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El siguiente hito en las relaciones humano-animal fue la revolución agrícola: el proceso por el cual pasamos de ser <a href="http://www.ugr.es/~pwlac/G21_22Oscar_Arce_Ruiz.html">cazadores-recolectores nómadas</a> a granjeros viviendo en asentamientos permanentes. Esto conllevó la aparición de una forma de vida completamente nueva en el planeta: los animales domesticados. Inicialmente este acontecimiento podría parecer de poca importancia, ya que los humanos sólo consiguieron domesticar menos de 20 especies de mamíferos y aves, en comparación con las miles y miles de especies que permanecieron siendo salvajes. Sin embargo, con el paso de los siglos, estas nuevas formas de vida se convirtieron en la norma. Hoy en día, más del 90% de los grandes animales están domesticados (“grandes” se refiere a animales que pesen al menos unos kilogramos). Pongamos como ejemplo a el pollo. Hace diez mil años, era un ave poco común existente sólo en pequeñas zonas de Asia del sur. Hoy, billones de pollos viven repartidos por casi cualquier continente e isla, excepto la Antártida. El pollo doméstico es probablemente el animal más extendido en los anales del planeta Tierra. Si medimos el éxito en base a su número, los pollos, vacas y cerdos son los animales de más éxito que nunca hayan existido.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sin embargo, las especies domesticadas pagan por su incomparable éxito colectivo con sufrimiento individual sin precedentes. El reino animal ha conocido muchos tipos de dolor y miseria durante millones de años. Pero aun así, la revolución agrícola ha generado nuevos tipos de sufrimiento, que además han ido empeorando con el paso de las generaciones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A primera vista, puede parecer que los animales domesticados están mucho mejor que sus primos salvajes o sus ancestros. Los búfalos salvajes pasan los días buscando comida, agua y cobijo, y están constantemente amenazados por leones, parásitos, inundaciones y sequías. El ganado doméstico, en contraste, disfruta del cuidado y la protección de los humanos. Los humanos proveen a vacas y terneras de comida, agua y cobijo, tratan sus enfermedades y las protegen de predadores y desastres. Sí, antes o después la mayoría de vacas y terneras son enviadas al matadero. Pero, ¿hace eso que su destino sea peor que el de los búfalos salvajes?, ¿es mejor ser devorado por un león que matado por un humano?, ¿son los dientes de los cocodrilos mejor que los cuchillos de acero?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lo que hace particularmente cruel la existencia de los animales de granja no es sólo la manera en la que mueren sino sobre todo cómo viven. Dos factores han modelado las <a href="https://igualdadanimal.org/noticia/2015/09/18/encierran-vacas-en-habitaciones-30-grados-durante-dias-para-estudiar-como-obtener-mas/">condiciones de vida de los animales de granja</a>: por un lado, los humanos quieren carne, leche, huevos, cuero y la fuerza de trabajo de los animales. Por otro lado, los humanos tienen que asegurar a largo plazo la supervivencia y la reproducción de dichos animales. Teóricamente esto debería proteger a estos animales de la crueldad extrema. Si un granjero ordeña a su vaca sin proveerla de alimento y agua, la producción de leche decrecerá y la vaca morirá pronto. Pero desafortunadamente <a href="https://igualdadanimal.org/blog/4-crueles-practicas-de-la-industria-porcina-con-los-cerditos-recien-nacidos/">los humanos causan un tremendo sufrimiento a los animales de granja</a> de otras maneras, incluso aunque aseguren su supervivencia y reproducción. La raíz del problema es que los animales domesticados han heredado de sus ancestros salvajes muchas necesidades físicas, emocionales y sociales que son despreciadas en las granjas. Los granjeros ignoran rutinariamente estas necesidades porque no aportan beneficios económicos. Encierran a los animales en pequeñas jaulas, mutilan sus cuernos y colas, separan a madres y crías y cruzan razas generando monstruosidades. Los animales sufren horriblemente pero, aun así, viven y se multiplican.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14189} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/10/pollobroiler.jpg" alt="" class="wp-image-14189"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><sub><em>Los pollos sufren fracturas debido a la modificación genética para que aumenten de peso en poco tiempo.</em></sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿No contradice eso los principios más básicos de la <a href="http://www.unav.es/cryf/teoriaevolucion.html">evolución darwinista</a>? La teoría de la evolución sostiene que todos los instintos y deseos han evolucionado en interés de la supervivencia y la reproducción. Si es así, ¿no prueba la continua reproducción de los animales de granja que todas sus necesidades están aseguradas? ¿Cómo una vaca puede tener una “necesidad” que no sea esencial para su supervivencia y reproducción?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Es completamente cierto que los instintos y deseos evolucionaron para satisfacer las presiones evolutivas de supervivencia y reproducción. Sin embargo, cuando estas presiones desaparecen, los instintos y deseos que han moldeado no se evaporan instantaneamente. Incluso aun no siendo primordiales para la supervivencia y la reproducción, continúan moldeando las experiencias subjetivas del animal. Las necesidades físicas, emocionales y sociales de las vacas, perros y humanos de hoy en día no reflejan sus condiciones actuales sino las presiones evolutivas con las que se encontraron sus ancestros hace decenas de miles de años. ¿Por qué nos gustan los dulces tanto a los humanos actuales? No es porque tengamos que atiborrarnos de helado y chocolate para sobrevivir en pleno siglo XXI. Más bien es porque si nuestros ancestros de la edad de piedra se topaban con frutas dulces y maduras, lo mejor que podían hacer era comer rápidamente tantas como pudieran. ¿Por qué la juventud conduce imprudentemente, se mete en discusiones violentas y hackea websites confidenciales? Porque obedecen decretos genéticos ancestrales. Hace setenta mil años, un joven cazador que arriesgase su vida persiguiendo a un mamut sobresalía sobre todos sus competidores y se ganaba la mano de alguna bella mujer (y ahora nosotros nos vemos atrapados en sus genes de machote).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Exactamente la misma lógica evolutiva moldea la vida de vacas y terneras en nuestras granjas industriales. Las vacas salvajes prehistóricas eran animales sociales. Para poder sobrevivir y reproducirse necesitaban comunicarse, cooperar y competir de manera eficiente. Como todos los mamíferos sociales, las vacas salvajes aprendían las necesarias habilidades sociales a través del juego. A todos los perritos, gatitos, terneras y bebés les encanta jugar porque la evolución implantó en ellos el deseo de hacerlo. En un entorno salvaje, necesitaban jugar. Si no lo hacían no aprendían las habilidades sociales vitales para la supervivencia y la reproducción. Si un gatito o una ternera nacía con una extraña mutación que le hiciese no desear jugar, difícilmente sobreviviría o se reproduciría, y si sus ancestros no hubiesen adquirido esa habilidad ni siquiera habría llegado a existir. De la misma manera, la evolución implantó en perritos, gatitos, terneras y bebés un abrumador deseo de generar vínculos con su madre. Una mutación casual que debilitase el vínculo madre-hijo era una sentencia de muerte.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Qué pasa a día de hoy cuando un granjero separa a una joven ternera de su madre, la mete en una pequeña jaula, la vacuna contra distintas enfermedades, la provee de alimento y agua y luego, cuando es lo suficientemente adulta, la insemina artificialmente con esperma de toro? Desde un punto de vista objetivo, una ternera así ya no necesita ni el vínculo maternal, ni compañeros de juego para sobrevivir y reproducirse. Sus amos humanos se hacen cargo de todas sus necesidades. Pero desde un punto de vista subjetivo, la ternera sigue sintiendo una profunda necesidad del vínculo materno y de jugar con otras terneras. Si no puede satisfacer estos deseos la ternera sufre terriblemente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta es la lección básica de la psicología evolutiva: una necesidad moldeada hace miles de generaciones se sigue sintiendo subjetivamente incluso si ya no es necesaria para la supervivencia y la reproducción en el presente. La revolución agrícola, trágicamente, otorgó a los humanos el poder de asegurar la supervivencia y reproducción de los animales domesticados mientras se ignoran sus necesidades subjetivas. Como consecuencia los animales de granja son, de manera colectiva, los de más éxito en el planeta, y a la vez, son individualmente los animales con las vidas más miserables que jamás hayan existido.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La situación ha ido siempre a peor en los últimos siglos, en los que la agricultura tradicional ha dado paso a la ganadería industrial. En sociedades tradicionales como la del antiguo Egipto, el Imperio romano o la China medieval, los humanos tenían unos conocimientos muy escasos de bioquímica, genética, zoología y epidemiología. Consecuentemente, su poder de manipulación estaba limitado. En las aldeas medievales, las gallinas correteaban libres entre las casas, picoteaban semillas y gusanos de las montañas de desperdicios y construían sus nidos en los establos. Si un campesino ambicioso hubiese tratado de encerrar a 1.000 gallinas en un atestado gallinero, probablemente se habría desatado una mortífera epidemia de gripe aviar, aniquilando a todas las gallinas además de a muchos aldeanos. Ningún clérigo, chamán o curandero podría haberla frenado. Pero una vez que la ciencia moderna descifró los secretos de las aves, los virus y los antibióticos, los humanos pudieron empezar a someter a los animales a condiciones de vida extremas. Con la ayuda de las vacunaciones, medicaciones, hormonas, pesticidas, sistemas centrales de aire acondicionado y comederos automáticos, ahora ya es posible apiñar decenas de miles de gallinas y pollos en gallineros y producir carne y huevos con una eficiencia sin precedentes.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14314} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/10/vacamadre.jpg" alt="" class="wp-image-14314"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><sub><em>La ciencia ha demostrado que los animales son seres que sienten y que pueden experimentar miedo y soledad. </em></sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por el número de animales involucrados, la ganadería industrial se ha convertido en uno de los problemas éticos más apremiantes de nuestra era. En nuestros días la mayoría de grandes animales viven en granjas industriales. Nos imaginamos que nuestro planeta está poblado por leones, elefantes, ballenas y pingüinos. Puede que eso sea cierto en el canal de National Geographic, las películas de Disney y en los cuentos de hadas, pero no es verdad en el mundo real. En el mundo hay 40.000 leones pero, en contraste, hay aproximadamente 1 billón de cerdos domesticados. Hay 500.000 elefantes y 1,5 billones de vacas domesticadas. 50 millones de pingüinos y 20 billones de pollos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En 2009 había 1,6 billones de aves salvajes en Europa, contando a todas la especies. Ése mismo año, la industria de la carne y los huevos europea crió a 1,9 billones de pollos. Todos juntos, los animales domesticados del mundo entero pesan alrededor de 700 millones de toneladas, comparadas con las 300 millones de toneladas que pesan los humanos y menos de 100 millones de toneladas que pesan los grandes animales salvajes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Es por esto que el destino de los animales de granja no es un asunto ético menor. Hablamos de la mayoría de grandes animales del planeta Tierra: decenas de billones de animales que sienten, cada uno con un complejo mundo de sensaciones y emociones, pero que viven y mueren en una línea industrial de producción. Hace cuarenta años el filósofo moral <a href="https://es.wikipedia.org/wiki/Peter_Singer">Peter Singer</a> publicó su canónico libro Animal Liberation (Liberación animal), que ha hecho mucho por cambiar la mentalidad de las personas al respecto de este problema. Singer afirmaba que la ganadería industrial es responsable de más sufrimiento y miseria que todas las guerras de la historia juntas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El estudio científico de los animales ha jugado un funesto papel en esta tragedia. La comunidad científica ha utilizado su creciente conocimiento de los animales principalmente para manipular sus vidas más eficientemente al servicio de la industria humana. Y al mismo tiempo este mismo conocimiento ha demostrado más allá de cualquier duda que l<a href="http://news.bbc.co.uk/hi/spanish/science/newsid_4370000/4370627.stm">os animales de granja son animales que sienten</a>, con intrincadas relaciones sociales y sofisticados rasgos psicológicos. Pueden no ser tan inteligentes como nosotros, pero desde luego conocen el dolor, el miedo y la soledad. Ellos también pueden sentir el sufrimiento y la felicidad.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Es el momento oportuno de llevar a nuestro corazón estos hallazgos científicos, porque al mismo tiempo que la población humana sigue creciendo, también lo hace nuestra habilidad de beneficiar o dañar a los animales. Durante 4 billones de años la vida en la Tierra fue gobernada por la selección natural. A día de hoy está cada vez más gobernada por el diseño inteligente humano. La <a href="https://es.wikipedia.org/wiki/Biotecnolog%C3%ADa">biotecnología</a>, <a href="https://es.wikipedia.org/wiki/Nanotecnolog%C3%ADa">nanotecnología</a> y la i<a href="https://es.wikipedia.org/wiki/Inteligencia_artificial">nteligencia artificial</a> capacitarán pronto a los humanos para remodelar a seres vivos de nuevas y radicales maneras, que redefinirán el significado mismo de la vida. Cuando se trate de diseñar este nuevo mundo feliz, deberíamos tener en cuenta el bienestar de todos los animales y no sólo el del Homo Sapiens.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Artículo escrito por Yuval Noah Harari en The Guardian.</em></p>
<!-- /wp:paragraph -->