<!-- wp:paragraph -->
<p>Solo después de los peces, los pollos son los animales más maltratados del mundo. No solo son víctimas de prácticas realmente crueles sino que son matados en cantidades impresionantes. Siendo más pequeños que los cerdos y las vacas, lógicamente, deben ser matados muchos más pollos que cualquier otro animal. Para producir la misma cantidad de carne que se obtiene de una sola vaca<strong> son necesarios 200 pollos. &nbsp;</strong>

Al ser seleccionado genéticamente, el pollo broiler crece en las granjas de engorde a un ritmo antinatural, tanto que <strong>si fuera un bebé humano pesaría 300 kilos a los dos meses de vida.</strong> Su sufrimiento es inimaginable: al no poder soportar su propio peso sufren terribles dolores y dificultad para caminar. Muchos agonizan echados en el piso por días antes de morir, entre sus excrementos y sin poder alcanzar el alimento o el agua.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener">¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong>Hay 3 pollos por cada ser humano en el planeta</strong>, todos obligados a vivir un verdadero infierno para que se produzca y venda una carne que no necesitamos consumir ya que podemos sustituirla por muchas opciones vegetales que son deliciosas, nutritivas y sanas.

<a href="https://es.loveveg.com/recetas/" target="_blank" rel="noopener">¡Puedes probar estas maravillosas recetas!</a></p>
<!-- /wp:paragraph -->