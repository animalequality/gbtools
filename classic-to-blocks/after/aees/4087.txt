<!-- wp:paragraph -->
<p>SANTIAGO DE CHILE.- En la comuna de Peñalolen han detectado a un sujeto que arroja veneno en las calles, arriesgando la salud de humanos, perros y gatos.

También en la comuna de Puente Alto, más de una decena de perros amanecieron muertos con signos de envenenamiento por sustancias peligrosas sustancias químicas.

Los vecinos de calle Río Bueno, en la comuna de Peñalolen, no saben cómo detener a un sujeto ya identificado que -en un  acto irracional- esparce comida envenenada.

Vecinos tienen las pruebas del veneno encontrado, a la espera  de contar con el apoyo necesario que permita analizar el tipo de sustancia química que este sujeto está arrojando en las  calles de la comuna.

Con este fin, realizaron la denuncia ante Carabineros, pero los uniformados no han podido concretar hasta ahora ninguna acción. 

Los vecinos denunciantes están a la espera de que la  Municipalidad tome cartas en el asunto, retire los cuerpos y estudie el caso, puesto que los particulares no pueden arrogarse el uso de sustancias tóxicas.

Fuente: emol.com</p>
<!-- /wp:paragraph -->