<!-- wp:image {"id":12032} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/shutterstock_76940707.jpg" alt="" class="wp-image-12032"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La gastronomía alemana tiene a la carne como ingrediente protagonista en muchos de sus platos y aún así <strong>este país lidera la tendencia de adoptar una alimentación basada en vegetales</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En Alemania, una buena parte de la población está muy consciente de los beneficios que aporta adoptar este tipo de dietas. <strong>La cifra de alemanes que no comen carne tres o más días por semana, los «vegetarianos a tiempo parcial», asciende a los 42 millones, algo más de la mitad de la población del país</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por detrás de Alemania se encuentra Estados Unidos (17%), Reino Unido (11%), Francia (6%), Taiwán y Canadá (5%), Suráfrica (4%) y España, Italia y Austria (3%).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«En Alemania adoptar una alimentación vegetariana o vegana es algo que está a la orden del día, sobre todo entre los jóvenes».</p></font>

<p>&nbsp;</p>

<p>Y todo indica que esta cifra va en aumento. La Unión Vegetariana Alemana asegura que <strong>en los últimos 30 años el número de vegetarianos se ha multiplicado por diez</strong>. Alemania contaba para 2016 con 900.000 veganos y 8 millones de vegetarianos. Y es que en Alemania adoptar una alimentación vegetariana o vegana es algo que está a la orden del día, sobre todo entre los jóvenes.</p>

<figure><img alt="" class="wp-image-12033" src="/app/uploads/2017/05/shutterstock_300214889.jpg" style="margin-right: 10px; margin-left: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>Un estudio realizado por un grupo de profesores alemanes, a partir de datos de encuestas de 2008 y 2011, confirma que los alemanes entre 18 y 29 años son el grupo demográfico más vegetariano.</p>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2016/12/01/por-que-las-generaciones-mas-jovenes-estan-cambiando-la-forma-en-que-nos-alimentamos/" target="_blank">¿Por qué las generaciones más jóvenes están cambiando la forma en que nos alimentamos?</a></strong>

<p>&nbsp;</p>

<p>Según la agencia Mintel Alemania fue el país que en 2016 lideró el lanzamiento de productos alternativos a la carne. En total, ¡produjo el 18% de todos los nuevos lanzamientos que tuvieron lugar ese año!</p>

<p>En Alemania, al igual que otros países de Europa, muchos supermercados tienen pasillos enteros dedicados a estos productos. Y en 2011 abrió en Berlín «Veganz», la primera cadena de supermercados de Europa y que cuenta con distintos establecimientos fuera de Alemania también y más de 4.500 productos distribuidos en Europa Central.</p>

<p>Todos estos productos contribuirían a facilitar la reducción del consumo de carne ya que son nutricionalmente completos, forman parte de una alimentación equilibrada y son mucho más saludables que la carne.</p>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://www.lavanguardia.com/comer/tendencias/20170508/422388073424/por-que-alemania-encabeza-la-revolucion-de-la-comida-vegana.html" target="_blank">https://www.lavanguardia.com/comer/tendencias/20170508/422388073424/por-que-alemania-encabeza-la-revolucion-de-la-comida-vegana.html</a></p>

<p><a href="http://www.playgroundmag.net/food/pais-liderando-revolucion-vegetariana_0_1974402557.html" target="_blank">http://www.playgroundmag.net/food/pais-liderando-revolucion-vegetariana_0_1974402557.html</a></p></font>
<!-- /wp:html -->