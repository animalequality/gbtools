<!-- wp:paragraph -->
<p>Jinan - Según informa la agencia Xinhua, científicos del Centro de Investigación Tecnológica de  Ingeniería Robot de la Universidad de Ciencia y Tecnología de  Shandong, en el este de China, afirmaron que habían injertado microelectrodos en el cerebro de una paloma, con los que lograron controlar la dirección de su vuelo.  

Los injertos estimularon diferentes áreas del cerebro de la  paloma de acuerdo con los signos enviados desde una computadora  manejada por los científicos, y obligaron al pájaro a cumplir sus órdenes.  

“Se trata del primer experimento exitoso realizado en una paloma en el mundo”, señaló Su Xuecheng, jefe de los científicos, que ya realizó un experimento similar con un ratón en 2005. 

La noticia muestra una vez más las terribles consecuencias para los demás animales derivadas del hecho de ser considerados objetos. A pesar de que las palomas sienten y desean vivir sus vidas en libertad, es aceptado por la sociedad que se las utilice en contra de su voluntad para realizar experimentos sobre ellas, los cuáles nunca desearíamos para nosotros mismos.

Más información sobre la experimentación con animales: http://www.igualdadanimal.org/experimentacion</p>
<!-- /wp:paragraph -->