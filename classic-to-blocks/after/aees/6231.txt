<!-- wp:image {"id":9237} -->
<figure class="wp-block-image"><img src="/app/uploads/2011/12/42114-944-550.jpg" alt="" class="wp-image-9237"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Según ha informado el zoo de Barcelona, hoy viernes 16 de diciembre, Barus, un tigre macho de Sumatra, nacido en el Zoo de Barcelona en 2003, ha muerto ahogado al caer en el foso de agua que envuelve la instalación donde vivía tras sufrir un ataque epiléptico, enfermedad que padecía desde hace años y por la que se encontraba en tratamiento desde 2007. Al mediodía de hoy, y a consecuencia de uno de estos ataques crónicos, el felino ha caído al foso y los operarios no han podido recuperarlo con suficiente rapidez ya que ha quedado sumergido dentro del agua en una posición inaccesible para los operarios tanto desde dentro de la instalación (donde se encontraba Barus) como desde fuera, ha informado el Zoo de Barcelona.<br>
	<br>
	La crisis epiléptica se ha mantenido mientras Barus estaba en el agua, lo que le ha impedido que pudiera salir del foso por sí mismo. Esta no ha sido la primera vez en la que el animal caía al foso debido a un ataque epiléptico, aunque en otras ocasiones se pudo sacar al animal antes de que se ahogara. Desde Igualdad Animal consideramos este caso una evidencia más del sufrimiento y las privaciones que padecen los animales encerrados en los zoológicos. Como pudimos constatar en la investigación que realizamos en los zoológicos más importantes del estado español, entre los que se encuentra el zoológico de Barcelona, los animales padecen toda clase de problemas físicos y psicológicos debido a la vida en cautividad. Esperamos que este caso traiga de nuevo el debate social que desde Igualdad Animal venimos creando para cuestionar la existencia de estos centros, avanzado hacia una sociedad donde estos lugares pasen a formar parte del pasado.<br>
	<br>
	Es revelador que el zoológico de Barcelona vaya a comunicar la muerte de este animal y solicitar el encierro de un nuevo animal al programa de cría europea (EEP), quedando claramente reflejado la visión de los animales como objetos de exposición. Lamentablemente esta trágica muerte será sustituida por otro animal que pasará su vida encerrado. Los zoos, al contrario de lo que mucha gente se imagina, son centros donde los animales sufren y padecen todo tipo de privaciones. A los animales en los zoos se les enjaula de por vida privándoles de la posibilidad de desarrollarse según sus intereses y necesidades. Apenas pueden relacionarse con otros animales de su misma especie. Muchos de ellos conviven al lado de sus propios depredadores o entre sus propios excrementos en un continuo estado de angustia. Nuestra investigación así lo ha constatado.<br>
	<br>
	Vídeo de la investigación realizada en el zoológico de Barcelona: https://vimeo.com/28434118<br>
	Fotografías de la investigación en el zoológico de Barcelona: http://www.flickr.com/photos/igualdadanimal/sets/72157627344979775/<br>
	Sitio web de la investigación en zoológicos: http://www.VidasEnjauladas.org</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Igualdad&nbsp;Animal&nbsp;es&nbsp;una&nbsp;organización&nbsp;internacional&nbsp;de&nbsp;derechos&nbsp;animales fundada&nbsp;en&nbsp;2006,&nbsp;que&nbsp;trabaja&nbsp;para&nbsp;concienciar&nbsp;a&nbsp;la&nbsp;sociedad&nbsp;sobre&nbsp;las injusticias&nbsp;que&nbsp;padecen&nbsp;los&nbsp;animales&nbsp;y&nbsp;avanzar&nbsp;hacia&nbsp;un&nbsp;mundo&nbsp;donde&nbsp;dejen&nbsp;de ser&nbsp;explotados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>CONTACTO DE MEDIOS</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>María José Gómez - Portavoz de Igualdad Animal en Catalunya<br>
	Tel 637 531 306<br>
	E-mail: xoxeg@igualdadanimal.org</p>
<!-- /wp:paragraph -->