<!-- wp:image {"id":9446} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/03/caballo_arrastrado.jpg" alt="" class="wp-image-9446"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Los hechos sucedieron el pasado domingo 25 de marzo, cuando un hombre de Cangas de Onís <strong>ató a un caballo a la bola del remolque de su furgón y lo arrastró durante un kilómetro por la carretera</strong>, abandonándolo ya muerto a la puerta del parque de bomberos de esta localidad.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El cadáver fue inspeccionado por veterinarios que certificaron que las <strong>lesiones abrasivas</strong> que presentaba eran consecuencia de haber sido arrastrado, siendo las responsables de su muerte.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El supuesto autor de los hechos, de 40 años, cuenta ya con numerosos antecedentes policiales. Fue detenido este martes y puesto en libertad con cargos unas horas más tarde por el juez instructor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Acusado de un delito de maltrato animal con resultado de muerte, se enfrenta a una pena de tres meses a un año de prisión, además de a una inhabilitación para ejercer cualquier profesión relacionada con animales no humanos.</p>
<!-- /wp:paragraph -->