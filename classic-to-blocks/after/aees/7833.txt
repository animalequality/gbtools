<!-- wp:image {"id":12181} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/hamptoncreek_0.jpg" alt="" class="wp-image-12181"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Hampton Creek, la empresa que comercializa la famosa mayonesa sin huevo <a href="http://www.eatjust.com/en-us" target="_blank">Mayo</a>, anuncia que pretende llevar la <strong>«carne limpia»</strong> a todos los supermercados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Boom.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La noticia, que está dando la vuelta al mundo, fue lanzada por el Wall Street Journal (WSJ). Josh Tetrick, fundador y Director Ejecutivo de Hampton Creek, quiere producir carne más saludable, sostenible y barata, <strong>pero sin que ningún animal </strong>tenga que acabar en el matadero para ello.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porque eso es la ya mundialmente famosa «carne limpia»: <strong>carne proveniente de células que se produce en tanques de fermentación similares a lo que son usados para la cerveza.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2016/10/18/has-oido-hablar-de-la-carne-limpia/" target="_blank">¿Has oído hablar de la «carne limpia»?</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Al contrario que otras empresas de carne limpia como <strong>Memphis Meats</strong> o <strong>SuperMeat</strong>, que llevan tiempo ganando titulares, Hampton Creek ha estado desarrollando su trabajo en el más estricto de los secretos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El objetivo de la empresa californiana es llevar productos de esta carne de nueva generación a lo supermercados <strong>a finales de 2018</strong>. Memphis Meats, su principal competidora, prevé alcanzar los supermercados en 2021.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Durante un periodo de tiempo desconocido Hampton Creek ha estado recopilando plantas de más de 51 países para que sus técnicos consigan identificar y desarrollar <strong>un medio de cultivo vegetal</strong> en el las células puedan reproducirse.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta aspecto juega un papel clave en la viabilidad de la producción a gran escala. El medio de cultivo utilizado hasta ahora era suero sanguíneo proveniente de animales, que encarecía la producción hasta el punto de hacerla comercialmente inviable.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://www.eldiario.es/cultura/comer-carne-animales-veganismo_1_3811044.html" target="_blank">Tecnología culinaria: comer carne, pero sin animales</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Sin resolver el problema del medio de cultivo no es posible producir carne y pescado sostenibles y asequibles, incluso más económicos que la carne y pescado convencionales», declaraba Tetrick al WSJ. De los retos a los que se enfrenta la carne limpia, que no son pocos, el del medio de cultivo es el más significativo.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12182} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/joshhampton.jpg" alt="" class="wp-image-12182"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La ya mencionada Memphis Meats todavía utiliza el suero animal para producir carne limpia, pero tiene como objetivo reemplazarlo por un medio de cultivo vegetal. Uma Valetti, Director Ejecutivo, ya ha declarado que pretenden hacerlo «tan pronto como sea posible». Otro de los actores en el emergente sector de la carne libre de mataderos, la empresa holandesa Mosa Meats, dice estar «al 80% de conseguir medios de cultivos vegetales». «Hemos probado más de 400 medios no provenientes de animales», declaraba su Director Ejecutivo, Peter Verstrate.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><font color="#808080"><p>Josh Tetrick, fundador y Director Ejecutivo de Hampton Creek</p></font>

<p>&nbsp;</p>

<p>La carrera por producir carne sostenible, saludable y sin animales de por medio está servida. Una carne que venga a solucionar <strong>los graves problemas que produce la ganadería industrial</strong>. Hasta ahora pensábamos que la carne provenía de una fuente renovable: los animales.</p>

<p>El calentamiento global producido por la ganadería industrial y nuestra preocupación como consumidores preocupados por el bienestar de los animales nos han enseñado que <strong>estábamos equivocados</strong> en pensar de tal modo.</p></font>
<!-- /wp:html -->