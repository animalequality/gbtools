<!-- wp:paragraph -->
<p>Un nuevo plan para controlar los choques aéreos contra aves en los aeropuertos de Nueva York supondrá la <strong>matanza de miles de animales</strong>, de hasta seis especies diferentes, <strong>en un radio de 8 kilómetros alrededor del JFK</strong>. Esta medida, pendiente de aprobación, va en contra de las actuaciones llevadas hasta el momento para tratar de restaurar el hábitat de las aves migratorias de los humedales cercanos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La propuesta, presentada por el Departamento de Agricultura de los EE.UU., ha provocado la inmediata respuestas de grupos ecologistas y de defensa de los derechos de los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«¿Podría decir alguien que hay que matar a todos los osos de Yellowstone?</em>», pregunta Ida Sanoff, presidente de un grupo conservacionista.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Quienes defienden la propuesta de matar a todos estos animales utilizan como argumento las estadísticas, que indican que el número de choques con aves en el aeropuerto JFK se doblaron en 2011 en comparación con 2005.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De ser aprobado, este plan supondría la matanza de los gansos, cisnes, cormoranes, mirlos, cuervos, palomas y estorninos que viven en las zonas cercanas al aeropuerto JFK.</p>
<!-- /wp:paragraph -->