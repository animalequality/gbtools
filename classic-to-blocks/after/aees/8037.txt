<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Dulcesol, el primer fabricante en volumen de pastelería y bollería en España,&nbsp;<a href="http://www.dulcesol.com/ver/6889/el-grupo-dulcesol-eliminara-las-jaulas-de-la-cria-de-sus-gallinas.html" target="_blank" rel="noopener">acaba de anunciar su compromiso</a> de dejar de usar huevos de gallinas enjauladas en 2023.

La empresa, que ha mantenido conversaciones con Igualdad Animal en esta línea, ha confirmado que el número de gallinas afectadas por esta política será de 500.000 las cuales pasarán a crecer en libertad de forma gradual durante los próximos 5 años.

Hace apenas tres meses Nestlé asumió el compromiso de implementar esta misma política antes de 2020. Dulcesol se suma así a las más de 400 empresas en todo el mundo que han asumido ya el compromiso de dejar de vender huevos de jaulas antes de 2025. Entre ellas se encuentran Kraft Heinz, Sodexho, Aldi, Mondelez Internacional o las españolas Huevos Guillen y Carrefour España.

Javier Moreno subraya que «Dulcesol muestra su compromiso para reducir el gran sufrimiento que padecen las gallinas de jaula, respondiendo así a la creciente demanda de los consumidores por un mejor trato para los animales en el sistema alimentario. Aunque libre de jaula no implica libre de maltrato, es muy importante que una empresa como Dulcesol, líder en su sector, adopte este compromiso, ya que nos acerca al fin de las jaulas en la producción de huevos en España».</p>
<!-- /wp:paragraph -->