<!-- wp:paragraph -->
<p>SAN SALVADOR- Según informa Diario Co Latino, el huracán Ida afectó al único zoológico público de la capital salvadoreña, causando la muerte a un jabalí y un tucán y afectando a muchos otros animales encerrados en aquel lugar.

Raúl Miranda, director del Parque Zoológico Nacional, declaró: "Lamentablemente, un jabalí murió ahogado cuando tuvimos una inundación, mientras que un tucán murió por hipotermia, las acciones de rescate que pusimos en marcha evitaron que más animales murieran".

Los hechos ocurrieron porque se desbordó un río paralelo a las instalaciones, afectando a diversas zonas de este centro de reclusión de animales.

Según la veterinaria del parque, Virna Ortiz, resultaron dañados el “serpentario”, los “aviarios” y las jaulas de distintas especies como la de un tapir, que resultó con heridas."Tenemos a muchos animales en cuarentena, pues algunos de ellos tenían síntomas de hipotermia, infecciones en la piel por la humedad, otros con padecimientos pulmonares y por eso se mantienen medicados con antibióticos y se les aplica reforzamiento vitamínico", agregó Ortiz.</p>
<!-- /wp:paragraph -->