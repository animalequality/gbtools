<!-- wp:image {"id":9374} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/02/tiro_palomas_valencia1.jpg" alt="" class="wp-image-9374"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Hoy&nbsp; domingo 26 de febrero, activistas por los derechos animales, de la <strong>Fundación Equanimal, <a href="https://igualdadanimal.org/" target="_blank">Igualdad Animal</a> y Fauna Libre</strong>, hemos llevado a cabo <strong>dos novedosas protestas contra el campeonato de "tiro a palomas a brazo" que se ha celebrado hoy en Valencia</strong>, en el municipio de Ribarroja, coincidiendo con la primera tirada puntuable del campeonato de España de esta modalidad todavía permitida en España.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Primera protesta: </strong>A las <strong>10:00h en pleno campo de tiro del campeonato de Ribarroja,</strong> activistas defensores de los animales hemos ofrecido a los cazadores platos de tiro, con el fin de incentivarles a que opten por su uso frente a provocar el sufrimiento y matanza de miles de animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Segunda protesta:</strong> A las <strong>12:30h en la Plaza de los Pinazo de Valencia</strong> alrededor de 50 activistas llevamos a cabo una protesta en la que mostramos a 3 palomas muertas obtenidas del campeonato con el fin recordar a las miles de palomas que en esos mismos instantes estaban perdiendo la vida y exigir el fin de esta modalidad de caza y de cualquier otra práctica que no respete a los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li><strong>Galería fotográfica de nuestro acto de hoy</strong> <strong><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157629457942253" target="_blank">aquí</a></strong></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La modalidad de caza tiro de palomas a brazo consiste en disparar y abatir al mayor número de palomas posible. Estos animales sufren terriblemente durante todo el proceso, desde el momento en el que son transportados allí hacinadas en jaulas hasta el momento en el que son agarradas y arrojadas de forma violenta al aire y sufren el impacto de los disparos. Caen al suelo a una velocidad de entre 50 y 70 km/hora, y muchas sufren una prolongada agonía en el suelo, que queda sembrado de incontables víctimas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En otros países de Europa las palomas ya no son matadas con este fin, y en lugar de ellas se utilizan platos. Activistas de la Fundación Equanimal documentaron el Campeonato de Europa de esta modalidad el pasado septiembre donde puede verse la brutalidad del mismo. Vídeos y fotos de la matanza aquí.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La <strong>Fundación Equanimal</strong> e <strong>Igualdad&nbsp;Animal</strong> organizamos esta protesta, en la que contamos con el apoyo de la organización <strong>Fauna libre</strong>, para crear debate social sobre la explotación y discriminación a la que sometemos a los animales. Este campeonato, al igual que cualquier modalidad que suponga utilización de animales, debería ser abolido, ya que causa sufrimiento y muerte. Seguiremos trabajando para avanzar hacia una sociedad donde matanzas como las que se van a llevar cabo en este campeonato pasen a formar parte del pasado.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Equanimal e Igualdad Animal somos dos organizaciones por los derechos animales que trabajamos para concienciar a la sociedad sobre las injusticias que se cometen diariamente contra los animales y avanzar hacia una sociedad donde dejen de ser discriminados y explotados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p><strong>CONTACTO DE MEDIOS</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Óscar Horta </strong><br>
Portavoz de Fundación Equanimal <br>
Teléfono: 600 427 239</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>David Granada </strong><br>
Portavoz de <a href="https://igualdadanimal.org/" target="_blank">Igualdad Animal</a><br>
 Teléfono: 618 843 146 <br>
Correo electrónico: davidg@igualdadanimal.org</p>
<!-- /wp:paragraph -->