<!-- wp:paragraph -->
<p>MÉRIDA, 23 Dic. (EUROPA PRESS) -  

   El Servicio de Animalario de la Universidad de Extremadura (UEx) recordó hoy que ofrece apoyo a todas las entidades que investigan en la región en sus unidades de Cáceres y Badajoz.

   En nota de prena la UEx explica que tales unidades permiten llevar a cabo en sus instalaciones los experimentos de los investigadores. Disponen de laboratorios y de un quirófano con el más moderno sistema de anestesia inhalatoria para roedores y animales de talla media.

    Este servicio permite a cualquier investigador, sea o no de la comunidad universitaria, tener un lugar donde ubicar los "animales de experimentación" con las mejores condiciones en cuanto a personal de atención, alimentación, lecho, luz, etcétera. 

   El único requisito para poder disfrutar de las instalaciones es firmar un convenio de colaboración con la UEx. De este modo, pueden utilizarla hospitales, clínicas, otras instituciones, ya sean públicas o privadas, nacionales o internacionales.

   "Nos dedicamos a la cría y mantenimiento de ´animales de experimentación´ bajo las mejores condiciones, tanto legales como las requeridas por los propios experimentadores", explica María Reyes Panadero, directora del Servicio del Animalario de la UEx. 

   "Localizamos el tipo de animal que necesitan y los mantenemos con una temperatura, luminosidad, ciclo de luz y oscuridad, alimentación, enriquecimiento ambiental, etcétera, adecuados tanto para la especie y estado fisiológico en cada caso, como para las necesidades específicas del proceso experimental que en concreto se vaya a llevar a cabo con cada uno de los lotes" añade Reyes.

   El Animalario asesora además en todo lo referente a la búsqueda de cepas más adecuadas y su adquisición, fisiología, manejo, procesos anestésicos, patología, tratamientos, interacciones en procesos experimentales o procesos legales.

NUEVO EDIFICIO

   El Servicio de Animalario cuenta en Cáceres con un nuevo edificio, en uso desde el pasado mes de mayo, que dispone de laboratorios y de un quirófano con el más moderno sistema de anestesia inhalatoria para roedores y animales de talla media, el cual incluye un ventilador específico de última generación para estas especies.

   El equipamiento se completa con racks ventilados para el mantenimiento de roedores SPF (libre de patógenos específicos) y racks para peces. La sala para los SPF, animales con deficiencia inmunitaria, presenta unas medidas especiales con aisladores donde mantienen a los roedores. Cada jaula es independiente del resto y vive en un ambiente totalmente estéril.

   La Unidad de Cáceres, tiene como "sistema de barrera", además de la propia distribución del edificio, una red de aire acondicionado con filtros absolutos (Hepa) que aseguran tanto la entrada a la instalación, como la salida de la misma, de aire absolutamente limpio. 

   Dispone también de un sistema de acceso del personal (tanto propio del Servicio como de Investigadores Autorizados) a la "zona bajo barrera"  mediante tarjetas codificadas y un sistema informatizado de registro de todas las entradas y salidas a las distintas salas, así como una recepción previa de los animales en habitáculos para cuarentena antes de su paso definitivo a la colonia de reproductores o a los lotes de investigación.

   El sistema de lavado del utillaje se realiza con máquinas específicamente diseñadas para este cometido, con ciclos de termodesinfección. Posteriormente (y para materiales que soporten el proceso) todas las jaulas, rejillas, biberones, etc., son autoclavados antes de su siguiente uso. En un futuro próximo, el Servicio de Animalario quiere dotar con el mismo nivel de instalaciones a la Unidad de Badajoz.


Nota de Igualdad Animal - El avance científico es una de las bases de nuestra cultura y reporta grandes beneficios para los seres humanos, pero este avance tiene ciertos límites. Por ejemplo, la mayor parte de la sociedad no justificaría que se experimentara con humanos en contra de su voluntad aunque ésto supusiera grandes avances en la búsqueda de vacunas y curas. El mismo criterio debe seguirse con los demás animales, pues ellos, al igual que nosotros/as, no quieren morir y desean disfrutar de su vida en libertad.

Más información sobre la experimentación animal: http://www.igualdadanimal.org/experimentacion

Imágenes de Igualdad Animal de dentro de un laboratorio y rescate de 4 ratones: http://www.flickr.com/photos/igualdadanimal/sets/72157622764411378/

Fuente: europa prees</p>
<!-- /wp:paragraph -->