<!-- wp:paragraph -->
<p>Entre enero y junio se asesinaron en España 394 millones de aves, cerdos, cabras, vacas, ovejas y caballos. Esta cifra no incluye a los millones de peces asesinados a diario.

Los ganaderos españoles mataron (con el objetivo de satisfacer la demanda existente de consumo de carne) en el primer semestre del año un 7,3 por ciento más de animales, según los datos de la Encuesta de Sacrificio de Ganado 2007 del Ministerio de Agricultura.


Fuente: agroprofesional.com</p>
<!-- /wp:paragraph -->