<!-- wp:paragraph -->
<p>Vizcaya, País Vasco - La Diputación ha decidido repetir la campaña de caza del año pasado, en la que se asesinaron a mil jabalíes, un 50% más que en 2005. El portavoz del área de Agricultura defendió el asesinato para solucionar los conflictos entre jabalíes e individuos de otras especies, con las siguientes palabras: "Cuando se rompe el equilibrio de la fauna y el número de animales es más grande de lo adecuado, hay que aminorar su población, y la forma de hacerlo es mediante la caza".

Los asesinatos se realizarán de una forma controlada con la ayuda de cuadrillas de cazadores durante los fines de semana y días festivos en las zonas donde se estime que pueden vivir un número elevado de jabalíes. Según  Juan Antonio Sarasketa, presidente de la Asociación vasca de Cazadores: "Ésta es una de las razones que dan sentido a la caza: retirar la renta del patrimonio que sobra [sic]. El área quedará acotada y señalada para evitar que puedan entrar seteros o montañeros mientras se realizan los disparos. Sarasketa también afirmó, de forma especista, "La vida de una persona [humana] vale más que la de todos los jabalíes del mundo".

Para evitar accidentes de tráfico entre humanos y otros animales (recordemos que los humanos también somos animales), el biólogo Mario Sáenz de Buruaga aconsejó a los automovilistas que tengan "cuidado por la noche" y que reduzcan la velocidad al pasar por "zonas forestales donde haya poco arcén". Asimismo recomendó no sobrepasar la velocidad indicada: "Si vas a 90 con las largas no puedes pretender ver a un corzo, hay que respetar los 50". 

En algunos lugares se colocan placas reflectantes para deslumbrar a los animales y disuadirles de cruzar, o pasos acolchados con vegetación natural, como en Benavente, lo que prueba que existen alternativas para solucionar estos conflictos sin perjudicar a ningún individuo.


Fuente: Terra Actualidad - VMT</p>
<!-- /wp:paragraph -->