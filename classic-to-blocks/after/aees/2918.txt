<!-- wp:paragraph -->
<p>Madrid - La organización Igualdad Animal ha convocado a las 18.30 horas de hoy una concentración de protesta contra las corridas de toros en la Puerta del Sol de Madrid. 

Los organizadores lamentan en un comunicado no haber obtenido autorización de la Delegación del Gobierno en Madrid para celebrar esta protesta en los alrededores de la plaza de toros de Las Ventas, por motivos de seguridad. 

El objetivo de Igualdad Animal es expresar su rechazo "ante el asesinato de toros y la explotación animal en general". La organización denuncia que "más de 17.000 animales son víctimas de la afición a ver cómo son torturados y matados para regocijo popular". 

SERVIMEDIA</p>
<!-- /wp:paragraph -->