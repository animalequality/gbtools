<!-- wp:paragraph -->
<p>La publicidad de las industrias cárnica, láctea y del huevo es una perfecta maquinaria para mantenerte en la ignorancia del <strong>peor maltrato animal conocido</strong>.

Mientras que en los anuncios se nos muestra a animales pastando o disfrutando del sol, cerca, muy cerca de nosotros <strong>la realidad se nos oculta</strong> tras los muros de cemento y hormigón de <a href="https://igualdadanimal.org/noticia/2016/04/14/el-peor-maltrato-animal-conocido-se-produce-en-las-granjas-y-mataderos/">las granjas industriales</a>.

<strong>La práctica totalidad&nbsp;de la carne, huevos y lácteos que son consumidos provienen de la ganadería industrial</strong>. La producción proveniente de ganadería ecológica es estadísticamente insignificante.

Es hora de descubrir la realidad.

Porque los consumidores <a href="https://igualdadanimal.org/noticia/2016/02/24/nuestras-leyes-actuales-no-solo-fallan-los-animales-de-granja-tambien-nosotros/">tenemos derecho a saber</a> qué prácticas llevan a cabo las industrias <strong>que tienen en sus manos las vidas de millones de animales</strong> tan sensibles y especiales como nuestros perros y gatos.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10620,"align":"left"} -->
<figure class="wp-block-image alignleft"><img src="/app/uploads/2016/05/noticiamartes2.jpg" alt="" class="wp-image-10620"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Así se ven desde el exterior las granjas industriales donde los animales nacen y son engordados. En este caso se trata de una <strong>granja industrial de cer</strong><strong>dos</strong>. Las reminiscencias de estos gigantescos barracones son tenebrosas.

El interior es aún peor. En las <strong>naves destinadas al engorde</strong>, las cerdas son aisladas en jaulas tan pequeñas que apenas tienen espacio para moverse. Todo está pensado para convertir a los animales en máquinas de producir carne.

Las vacas no corren mejor suerte. <strong>Además de ser inseminadas artificialmente una y otra vez y separadas de sus pequeños en cada parto</strong>, la industria láctea las convierte en máquinas de producir leche. Cuando el ritmo de producción de leche decrece son enviadas al matadero, aunque todavía les quedarían muchos años de vida por delante.

La peor parte se la llevan los pollos. <strong>Estos frágiles y sensibles animales son los más maltratados por las industrias ganaderas</strong>. Al ser animales pequeños se necesitan muchos más para abastecer a los supermercados. <strong>9 de cada 10 animales de granja consumidos en el mundo son pollos</strong>. La estadística es escalofriante.

Las granjas industriales donde viven estos animales son terroríficas. Ni siquiera tienen ventanas. <strong>Jamás ven la luz del sol o pisan hierba fresca</strong>. Tras cuarenta días en la granja de engorde son enviados al matadero y sacrificados brutalmente siendo degollados cabeza abajo.

Las gallinas&nbsp;ponedoras sufren uno de los ciclos de explotación más crueles de todos los animales de granja. En <a href="https://igualdadanimal.org/blog/por-que-la-ganaderia-industrial-es-la-mayor-causante-de-maltrato-animal-de-la-historia/">las granjas industriales</a>, decenas de miles (a veces cientos de miles) de estos animales viven en pequeñas jaulas junto a varias compañeras (hasta siete). <strong>Cada una cuenta con un espacio equivalente a un folio para vivir</strong>. A lo largo de su vida nunca verán la luz del sol y no podrán ni extender sus alas por falta de espacio.

Por favor, considera <strong>ayudar a los animales de granja </strong>consumiendo alternativas a la carne. <strong>No consumir los productos que conllevan maltrato animal lanza un poderoso mensaje a las industrias cárnica, láctea y del huevo</strong>.</p>
<!-- /wp:paragraph -->