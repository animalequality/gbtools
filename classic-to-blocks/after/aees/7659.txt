<!-- wp:paragraph -->
<p>A todos nos encantan y conmueven las historias de animales que reciben una segunda oportunidad tras haber conocido solamente el maltrato y los abusos. Nos llena de esperanza saber que esos testimonios confirman que un mundo más compasivo para los animales es posible.

Por eso hoy queremos presentarte una historia con final feliz, la de una superviviente entre millones que sufren día tras día las formas más terribles de maltrato imaginables.

Lisa fue rescatada por Igualdad Animal de una granja de producción de huevos. La granja de animales que desde pequeños nos muestran los libros infantiles es maravillosa, los animales viven felices en ella, son cuidados y amados. Pero la realidad es bien distinta.

Dentro de las <a href="https://igualdadanimal.org/blog/9-imagenes-que-retratan-la-crueldad-de-la-industria-del-huevo/" target="_blank" rel="noopener">granjas industriales</a> para producción de huevos las gallinas pasan sus vidas hacinadas dentro de jaulas. <strong>El espacio destinado para cada una es tan pequeño como un ipad y por eso no pueden siquiera extender sus alas</strong>.

https://youtu.be/2qfzN-OTqhs</p>
<!-- /wp:paragraph -->