<!-- wp:paragraph -->
<p>Varios activistas, introducidos en sendas bandejas a modo de las que se emplean en los supermercados para vender carne -como se aprecia en la imagen-, protagonizaron en la mañana de ayer en la Plaza Nueva de Sevilla una acción organizada por la organización Igualdad Animal Andalucía con motivo del Día Internacional del Veganismo. Con este gesto quisieron protestar por el comercio de carnes [y el resto de productos de origen animal].

Esta es la primera reividicación que se realiza en Sevilla por el Día Internacional del Veganismo, y en el transcurso de la misma fueron muchas las personas que comprendieron el significado de la palabra veganismo y de su importancia para poder hablar de verdadero respeto hacia todos los animales.</p>
<!-- /wp:paragraph -->