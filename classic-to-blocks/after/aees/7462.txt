<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10569" src="/app/uploads/2016/04/noticia12041.jpg" style="float:left; margin:10px; width:450px">Alimentar a la población mundial con productos de la ganadería industrial es inviable. El modelo alimentario que genera es desastrosamente insostenible y produce problemas globales que nos afectan a todos (calentamiento global, deforestación de selvas, enfermedades, pérdida de nuestros valores humanos respecto al maltrato animal, etc).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Vivimos en una época decisiva, para el año 2050 seremos 9.600 millones de seres humanos en el planeta y <strong>se requieren cambios en la manera de producir alimentos</strong>. Gobiernos e instituciones <a href="https://igualdadanimal.org/noticia/2016/04/05/el-gobierno-holandes-recomienda-reducir-el-consumo-de-carne-dos-raciones-semanales/">ya están promocionando</a> un cambio de modelo a escala global; uno que sea más sostenible, más eficiente, más limpio y <strong>acorde con los valores humanos de los consumidores</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Recientemente en Estados Unidos han surgido dos innovadores proyectos para ayudar a solucionar <a href="https://igualdadanimal.org/noticia/2015/11/26/un-comite-de-expertos-avisa-los-gobiernos-comer-menos-carne-es-la-unica-manera-de/">los graves problemas</a> causados por la ganadería y el obsoleto modelo alimentario que esta genera. <a href="http://www.thegoodfoodinstitute.org/">The Good Food Institute</a> y <a href="http://www.newcropcapital.com/">New Crop Capital</a> pretenden enfrentarse al problema apoyando económica, legal y publicitariamente a las empresas innovadoras que están desarrollando <a href="https://igualdadanimal.org/noticia/2016/02/27/un-estudio-de-mercado-revela-que-los-productos-alternativos-la-carne-experimentaran-un/">productos alternativos a carne, huevos y leche</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The Good Food Institute es una organización sin ánimo de lucro. <strong>El proyecto trata de animar a científicos y emprendedores a crear empresas que investiguen y desarrollen productos alternativos a la carne</strong>. La idea es obtener el mismo sabor y textura pero sin las contraindicaciones ya mencionadas. 


<font size="4"><font color="#808080">The Good Food Institute facilita la creación de alternativas basadas en vegetales y en carne generada mediante agricultura celular, sin maltrato animal asociado (ningún animal muere en el proceso). Además este proyecto ayuda a que los productos alimentarios de estas innovadoras empresas estén presentes en los supermercados y restaurantes.</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="4">


<p><img alt="" class="wp-image-10570" src="/app/uploads/2016/04/noticia12042.jpg" style="float:right; margin:10px; width:450px">New Crop Capital es un proyecto que pretende financiar a las empresas emprendedoras con mayor potencial para cambiar el obsoleto sistema alimentario basado en la ganadería industrial. Ayudando al desarrollo de estas revolucionarias empresas New Crop Capital <strong>pretende crear un fuerte mercado de productos alternativos a la carne que ayude a llevar estos deliciosos productos a los hogares de los consumidores</strong>.</p>

<p>Como explica Bruce Friedrich, director ejecutivo de The Good Food Institute, «Estamos comprometidos a impulsar el futuro de la alimentación. Los consumidores eligen qué comer en base al sabor, las ventajas y el precio; y ahí es donde The Good Food Institute y New Crop Capital entran en juego. Los dos proyectos están enfocados en crear alternativas a los productos de origen animal que sean sostenibles y limpias; que estén presentes en todos los sitios y que tengan un precio igual de asequible que los productos de origen animal».</p></font>
<!-- /wp:html -->