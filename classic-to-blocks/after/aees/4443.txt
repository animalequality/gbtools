<!-- wp:paragraph -->
<p>A mes y medio de que se celebren las cucañas marítimas en las fiestas patronales del Port de Sagunt, el presidente de la federación de peñas, José Bosque, sigue apostando por usar patos en las fiestas.

Pese a las multas, que ascienden a tan sólo 90.000 euros, y a su intención de no perjudicar al ayuntamiento, el propio Bosque justifica así la explotación de patos: «queremos ser nosotros quienes decidamos cuándo y cómo se acaba con este acto».

El máximo dirigente de las peñas porteñas reconoce que «nos hemos comprometido por escrito ante la Conselleria de Gobernación a acabar con la suelta de patos en este tiempo en el que iremos reduciendo el número de animales empleados».

José Luis Martí (PP), concejal de Fiestas, asegura que el ayuntamiento sigue haciendo «todos los esfuerzos» para mantener la suelta y confía en el recurso contra la última multa.

<strong>Fuente: El Mercantil Valenciano</strong></p>
<!-- /wp:paragraph -->