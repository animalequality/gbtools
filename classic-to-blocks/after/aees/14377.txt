<!-- wp:paragraph -->
<p>Perú - Con el fin de concientizar a la población sobre lo injusto que es utilizar animales como espectáculo, miembros de la organización Igualdad Animal, el último sábado realizaron un pequeño plantón frente a un conocido circo ubicado a la atura de la Av. Tomas Valle en el distrito de los Olivos.  

Escribe Martín Chilo Espinoza 

En esta época del año, aparecen una cantidad considerable de circos, los cuales mantienen animales en cautiverio y poco les importa ponerlos en peligro y maltratarlos con tal de ver sus arcas llenas. 

"Los responsables son tanto el que explota al animal y los que van a ver este clase de espectáculos, asimismo nos preocupamos no solo en concientizar con respecto a los animales que se usan para el entretenimiento sino que nos preocupamos de los animales que se explotan para la producción de abrigo ó cuero", aseveró Arturo Arnáez Vaella, Coordinador en el Perú de la organización Igualdad Animal.

Según fuentes provenientes de diversas instituciones que luchan incansablemente contra la violencia a los animales, es que estos seres (animales), en los circos son encarcelados en pequeñas jaulas que apenas les proveen de espacio para pararse o echarse. Los animales más grandes como los elefantes son encadenados mientras no están siendo obligados a actuar.

Se supo por fuentes allegadas a la organización que durante el plantón hubo incidentes por parte de los trabajadores del circo quienes impedían a toda costa que se siga distribuyendo los volantes que contenían todos los abusos que se comenten en contra de los animales en esta clase de circos denominados "Circos de la Muerte". "Los animales tienen intereses en vivir, ser libres y deben de valorarse y respetárselos de la misma forma que lo es la especie humana", sentenció Arnáez Vaella.

Para todos aquellos interesados en esta noble causa y unirse a este grupo de valientes -porque en estos tiempos solo una persona valiente y consecuente con uno mismo es capaz de hacer estos actos tan desinteresados- pueden unirse visitando la pagina Web www.igualdadanimal.org 

Fuente: Rumbos


Para contactar con Arturo Arnáez, coordinador de Igualdad Animal en Perú, se puede escribir a <a href="mailto:arturoa@igualdadanimal.org">arturoa@igualdadanimal.org</a></p>
<!-- /wp:paragraph -->