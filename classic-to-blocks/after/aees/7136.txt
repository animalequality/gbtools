<!-- wp:image {"id":10137} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/04/paolobernini.jpg" alt="" class="wp-image-10137"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>El político Paolo Bernini, del Movimiento 5 Estrellas (M5S) hizo referencia ayer viernes 18 de abril, en su intervención en el Parlamento Italiano,&nbsp; a la investigación de Igualdad Animal en granjas y mataderos de corderos italianos. Manifestó el impacto que le habían provocado las imágenes, y habló del sufrimiento que padecen los corderos en este época por la celebración de la Pascua, y finalizó su intervención sugiriendo optar por una alimentación vegetariana para evitar el sufrimiento de estos animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/DVgz-KnmELQ" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Igualdad Animal hizo públicos el pasado lunes 14 de Abril los hallazgos de su nueva investigación en granjas y mataderos de corderos en Italia. Los investigadores de Igualdad Animal documentaron dos granjas de corderos en Lazio y dos mataderos de Lombardia, Brescia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las impactantes imágenes muestran corderos siendo degollados sin aturdimiento previo y las condiciones en las que se encuentran en las granjas. El vídeo también incluye imágenes del momento en que los corderos son pesados para llevarlos al matadero, en el cual sufren terriblemente debido al estrés y a la brusquedad con la que se realiza este proceso, llegando incluso a fracturarse algunas extremidades.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/59lUpGn5Zt4" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En palabras de Sharon Núñez, directora internacional de Igualdad Animal, "<em>nuestra investigación está causando de nuevo un gran impacto en la sociedad italiana, y una muestra de ello es la intervención de Paolo Bernini en el Parlamento italiano. Nos alegra ver que el debate llega a la esfera política."</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta investigación forma parte de la campaña “Salva un Cordero” que Igualdad Animal inició el pasado año y que tuvo un alto impacto en la sociedad italiana, consiguiendo que durante la Pascua se redujera el consumo de cordero un 40%, lo que supuso un descenso en la venta de aproximadamente 320.000 corderos. [1] Durante la Pascua se calcula que son matados 800.000 corderos y 3.000.000 durante todo el año. [2]</p>
<!-- /wp:paragraph -->