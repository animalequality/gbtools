<!-- wp:paragraph -->
<p>Desde Igualdad Animal felicitamos a Noble Foods por unirse a todos los supermercados de Reino Unido al comprometerse a poner fin a los huevos procedentes de gallinas enjauladas.

Así, el productor de huevos más grande del Reino Unido, ha anunciado su compromiso de producir únicamente huevos libres de jaulas para el año 2025. La decisión, que reducirá el sufrimiento de millones de aves cada año, se produce días después de que Igualdad Animal publicase varias imágenes impactantes de sufrimiento obtenidas en una granja de gallinas enjauladas de Noble Foods en Dorset.

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe src="https://www.youtube.com/embed/IDfHeZmGOAs?rel=0" width="720" height="405" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"><span style="color: #0000ff;">¡Suscríbete gratuitamente a nuestro e-boletín!</span></a></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
La ausencia de jaulas no significa una vida libre de crueldad, pero Noble Foods está dando un paso importante para mejorar las condiciones de los animales que sufren enormemente en las granjas industrializadas. Encerrar aves en jaulas es una práctica horrible. Los estudios han demostrado que las aves son criaturas sociables y sensibles con inteligencia a la par con un niño pequeño. Sin embargo, están obligadas a vivir en jaulas de alambre sucias sin que puedan llevar una vida natural ni desarrollar sus comportamientos naturales.&nbsp;Estamos muy orgullosos de haber participado en esta decisión histórica y felicitamos a nuestros amigos de The Humane League en Reino Unido por sus incansables esfuerzos durante los últimos seis meses para que esto sucediera.

Tú también puedes hacer mucho para liberar a las gallinas del sufrimiento. Puedes enviar correos y usar la redes para difundir información sobre el terrible maltrato al que son sometidas. También puedes formar parte del equipo de Igualdad Animal para hacer frente a la empresas que producen huevos a costa de su sufrimiento.</p>
<!-- /wp:paragraph -->