<!-- wp:image {"id":9387} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/03/cctv_australia_mataderos.jpg" alt="" class="wp-image-9387"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Los principales productores de carne de Australia han accedido a introducir <strong>cámaras de circuito cerrado de TV para vigilar los mataderos</strong> de su país con la intención de que los métodos de matanza sean más transparentes para los consumidores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La situación que viven los animales en los mataderos de Australia ha estado bajo el punto de mira varias veces en los últimos meses. En febrero de este año,<strong> un matadero de Sidney fue cerrado tras publicarse un vídeo</strong> de Animal Liberation NSW mostrando el brutal trato recibido en este lugar por ovejas, vacas, cabras y cerdos. El matadero está ahora bajo investigación.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En noviembre de 2011<strong> otro matadero fue cerrado en Gippsland</strong> después de ser hacerse públicas otras tantas imágenes de manos de la organización Animals Australia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Una de las empresas que seguirá este procedimiento es la Teys Australia, responsable de aproximadamente el 18% de las matanzas realizadas en territorio australiano. Sin embargo, una de las mayores procesadoras de carne, la JBS, se resiste a seguir el ejemplo de Teys.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En 2011, una serie de grandes cadenas de supermercados del Reino Unido exigió la instalación de sistemas de circuito cerrado de TV en todos sus mataderos proveedores, en un intento de tranquilizar a los consumidores y mostrar que los animales no estaban siendo “cruelmente tratados”. Se estima que<strong> uno de cada cinco mataderos en el Reino Unido han sido ya equipados con estos sistemas de grabación</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Además, la Food Standards Agency de Reino Unido está estudiando la introducción obligatoria de cámaras de circuito cerrado en todos los mataderos del país, según propuesta de varias organizaciones de bienestar animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En EE.UU. estos sistemas son también comunes, aunque no obligatorios.</p>
<!-- /wp:paragraph -->