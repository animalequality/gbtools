<!-- wp:paragraph -->
<p>Nuestro innovador proyecto de realidad virtual <a href="https://ianimal.es/">iAnimal</a> fue presentado en el festival de cine independiente de Sundance, en Estados Unidos. <strong>Cientos de personas probaron la experiencia inmersiva con las gafas tras hacer cola</strong>. Al acabar, se les preguntaba qué imagen les había impactado más. La mayoría (algunas personas visiblemente afectadas), elegían la misma escena.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Un cerdo es forzado a entrar en el matadero a través de una pequeña puerta. Es su turno. Aterrorizado, observa lo que les ha sucedido a sus compañeros solo segundos antes. <strong>El inteligente y sensible animal se da cuenta de que va a morir como ellos</strong>. Intenta darse la vuelta desesperadamente y salir por la pequeña puerta, pero es demasiado tarde: los operarios ya la han cerrado. En un par de agónicos minutos, todo ha acabado para él.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10450} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/ianimal2.jpg" alt="" class="wp-image-10450"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Es la escena definitiva. Es la prueba que las personas necesitan. Es un símbolo de la tragedia que sufren los animales día tras día en las industrias que los envían al matadero</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No hay manera de negar lo que ves: <strong>ese animal no quiere morir</strong>. Está aterrorizado por lo que presencia. Sabe lo que les ha pasado a sus compañeros y sabe lo que le va a pasar a él. El miedo de ese sensible cerdo desmonta cualquier justificación ante lo que vemos. <strong>Su tragedia, como la de sus compañeros, de pronto se convierte en algo que nos afecta profundamente; su mirada, en una súplica a nuestra más profunda y benevolente humanidad</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10453} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/ianimal3.jpg" alt="" class="wp-image-10453"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La escena resume perfectamente dos cosas: por un lado, que <strong>nuestra actual industria alimenticia está obsoleta y traiciona nuestros valores humanos hacia seres que sufren terriblemente</strong>; por otro, que <a href="http://ianimal.es/cerdos/tour/index.html">el proyecto de realidad virtual iAnimal</a> es una máquina de empatía que nos hace conectar con la tragedia de los animales. Nos abre los ojos a una realidad que las industrias que envían a los animales al matadero <strong>nos mantiene&nbsp;oculta intencionadamente</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Quién podría seguir pensando lo mismo sobre el sufrimiento de los animales en los mataderos después de presenciar a uno de los billones de animales que mueren anualmente intentando escapar a su fatídico destino?, ¿quién daría un paso adelante para decir «<strong>lo que veo me deja indiferente, no me importa el sufrimiento de ese animal</strong>»?</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10454} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/ianimal4.jpg" alt="" class="wp-image-10454"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Si piensas que tú podrías ser quien lo dijera, tal vez es el momento adecuado para que pases por la experiencia <a href="https://igualdadanimal.org/noticia/2016/03/18/los-medios-de-comunicacion-quedan-impactados-en-el-lanzamiento-de-ianimal/">iAnimal</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Puede que no lo sepas, pero lo que está en juego es nuestra humanidad</strong>.</p>
<!-- /wp:paragraph -->