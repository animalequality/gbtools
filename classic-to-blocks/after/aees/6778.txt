<!-- wp:image {"id":9762} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/11/mataderos_granada.jpg" alt="" class="wp-image-9762"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La Guardia Civil ha sancionado y clausurado dos mataderos ilegales en los municipios granadinos de Láchar y Chimeneas, alertados por diferentes irregularidades detectadas en los días previos a la celebración musulmana del <a href="https://igualdadanimal.org/noticia/2011/11/08/fiesta-del-cordero/" target="_blank">“sacrificio” del cordero.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ambos mataderos obtenían buena parte de sus ingresos durante el período de fiestas musulmanas, ya que los animales, principalmente<strong> corderos, eran matados por el rito halal.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://farm7.staticflickr.com/6052/6324741774_664eacd3b3_z.jpg" alt="" title="Foto tomada durante la investigación de Igualdad Animal en Melilla"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Careciendo de los permisos legales pertinentes para desarrollar tal actividad, e incluso sin tener uno de ellos la simple licencia de explotación ganadera, en estos lugares <strong>los clientes elegían a los animales con vida y ordenaban allí mismo su matanza.</strong></p>
<!-- /wp:paragraph -->