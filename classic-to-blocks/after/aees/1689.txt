<!-- wp:paragraph -->
<p>Corrinetes, Argentina- Tres caballos han muerto y otros 32 han resultado heridos al volcar el camión donde eran transportados. El accidente tuvo lugar en la provincia argentina de Corrientes, según informa el diario digital Momarandu.

Al parecer el camión volcó quedando atravedado en la carretera. Se desconocen las causas del accidente.</p>
<!-- /wp:paragraph -->