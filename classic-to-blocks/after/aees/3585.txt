<!-- wp:paragraph -->
<p>Una flota de seis barcos "balleneros! de Japón zarpó ayer hacia el Océano Antártico en una expedición que tiene por objetivo asesinar a ballenas. La flota ballenera partió alrededor del medio día del puerto Shimonoseki, oeste del país, como pare de su vigésimo primera expedición a cargo del Instituto de Investigación de Cetáceos, que comenzó sus campañas en 1987.

Durante su campaña, que durará hasta mediados de abril de 2008, el equipo ballenero planea cazar 950 ballenas. La carne de las ballenas cazadas para el estudio se venderá en el mercado japonés y los ingresos serán para futuras expediciones científicas, según la agencia japonesa de noticias Kyodo.

La Comisión Ballenera Internacional autoriza cada año el asesinato de cetáceos con fines científicos, aunque muy generalmente dicha caza tiene por objeto vender carne de ballena en la industria de alimentación. Pero hay que señalar que el asesinato de ballenas y de animales de otras especies es igualmente rechazable tanto si el objetivo es realizar investigaciones científicas como si el objetivo es vender su carne.</p>
<!-- /wp:paragraph -->