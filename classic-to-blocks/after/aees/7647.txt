<!-- wp:image {"id":11469} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/30873467830_2a8118baaf_z_0.jpg" alt="" class="wp-image-11469"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La industria cárnica es a los animales de granja lo que las peleas de perros a nuestros compañeros de vida o la tauromaquia a los toros: violencia y maltrato.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por desgracia para los inteligentes, sensibles y desprotegidos animales de granja, la diferencia es que <strong>ellos están en manos de una industria que no es vista aún como lo son la tauromaquia o las peleas de perros</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Por qué?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porque esta industria está tan institucionalizada y subvencionada que nos ha hecho creer que necesitamos comer carne a diario para sobrevivir; de hecho, nos ha hecho creer que <a href="https://www.abc.es/sociedad/abci-igualdad-animal-pide-reducir-5242805119001-20161210015305_video.html" target="_blank">no hay ningún problema ético con enviar al matadero a 68.000 millones de animales cada año a nivel mundial</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>68.000 millones de animales al año: <strong>la población humana de 9 planetas Tierra</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«A la industria cárnica solo hay una cosa que le importe menos que las vidas de estos desprotegidos animales: tu salud».</p></font>

<p>&nbsp;</p>

<p>Y lo que resulta incluso más perturbador es que la abrumadora mayoría de la carne proveniente de estos animales <strong>es consumida en las sociedades occidentales</strong>. Sociedades en las que las autoridades sanitarias <a href="https://igualdadanimal.org/noticia/2016/04/05/el-gobierno-holandes-recomienda-reducir-el-consumo-de-carne-dos-raciones-semanales/" target="_blank">llevan años recomendando que se reduzca el consumo de carne</a> y se aumente el de verduras, legumbres, cereales y fruta.</p>

<p>¿Quieres saber la verdad? <strong>A la industria cárnica solo hay una cosa que le importe menos que las vidas de estos desprotegidos animales: tu salud</strong>. Una sociedad que tiende a ver cualquier plato sin carne como un plato nutricionalmente incompleto es una sociedad engañada por la publicidad de una industria sin escrúpulos.</p>

<figure><img alt="" class="wp-image-11452" src="/app/uploads/2016/12/30873479890_ac41541748_z.jpg" style="float:right; margin:10px; width:500px"></figure><p></p>

<p>De hecho, como acaba de avalar un año más <a href="https://igualdadanimal.org/noticia/2016/11/25/la-alimentacion-vegetariana-respaldada-por-la-academia-de-nutricion-mas-importante-del/" target="_blank">la academia de nutricionistas más influyente del mundo, las alimentaciones vegetarianas (vegana incluida), son nutricionalmente saludables y ecológicamente preferibles</a>. Esta academia, además, señala que lo son en todas las etapas de la vida del ser humano, infancia y embarazo incluido.</p>

<p>¿Cuánto tiempo más continuaremos negando la evidencia?</p>

<p>Los animales de granja, los millones de animales de granja, <strong>viven en un continuo ciclo de violencia, desamparo y sufrimiento</strong>. Las hembras son seleccionadas para seguir concibiendo más y más animales que nos abastezcan de carne que no necesitamos comer. Son inseminadas artificialmente una y otra vez para traer al mundo a crías que no deberían nacer. Sus crías son separadas de ellas al poco tiempo de nacer para ser enviadas a <a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/" target="_blank">granjas industriales de engorde que no deberían existir</a>.</p>

<p>El nombre de estos lugares es ya de por sí perturbadoramente descriptivo: engordamos a los animales con el solo propósito de comérnoslos.</p>

<p>En nuestra manera de ver la alimentación, intencionadamente manipulada por la industria cárnica, si no comemos carne, enfermaremos porque nos faltará proteína. <strong>Al igual que hace años se nos decía que si no bebíamos leche de vaca nos quedaríamos sin calcio</strong>. Y, sin embargo, el consumo de leche de vaca no para de descender a la vez que no para de aumentar el de leches vegetales. ¿Dónde están esas hordas humanas con carencia de calcio?</p>

<p>Tal vez, estén en el mismo lugar que todos esas personas que han decidido decir no a la industria cárnica: <strong>viviendo vidas saludables y satisfactorias</strong> alimentándose con alternativas a la carne.</p>

<p><strong>Si quieres saber cómo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación</strong>.</p></font>
<!-- /wp:html -->