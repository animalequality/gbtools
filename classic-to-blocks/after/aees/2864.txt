<!-- wp:paragraph -->
<p>Pekín - China obsequiará al rey Juan Carlos, de visita en el país asiático, con una pareja de osos panda para promocionar la "amistad mutua", dijo el Ministerio de Asuntos Exteriores chino el jueves.

No es la primera vez que Pekín ofrece pandas como regalos de "buena voluntad", o más frecuentemente alquilado o prestado, para impulsar las relaciones diplomáticas, comerciales y de relaciones públicas con otros países.

"Será un regalo precioso para el pueblo español, y esperamos que esta pareja de pandas, como amistosos embajadores del pueblo chino, puedan ayudar a promocionar las relaciones mutuas", dijo el portavoz del ministerio, Qin Gang, en rueda de prensa. "También esperamos que los zoológicos españoles puedan dar buenos cuidados a nuestro tesoro nacional", añadió.

El ministerio aclaró después en un mensaje telefónico que los pandas iban a ser prestados a España, no regalados. No dijo cuándo se esperaba que los devolviera Madrid. China regaló en 1978 otra pareja de pandas a los monarcas españoles, que "donaron" al zoo de Madrid.

Fuente: Reuters

Los pandas son convertidos en objetos de intercambio, "tesoros" que se regalan como parte del protocolo diplomático entre países. Animales convertidos en mercancías que el zoo de Madrid buscará rentabilizar económicamente.</p>
<!-- /wp:paragraph -->