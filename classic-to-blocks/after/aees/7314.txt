<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10315" src="/app/uploads/2015/10/meatsubstitute.jpg" style="float:right; margin:10px; width:450px">Apreciando el cambio de hábitos de consumo y actitudes de los consumidores, distintas empresas cárnicas alemanas se han propuesto innovar. Es normal: <a href="http://www.igualdadanimal.org/noticias/7280/8-hamburguesas-100-vegetarianas-que-haran-que-te-chupes-los-dedos">cada vez más personas sustituyen la carne en sus comidas</a>. “Somos la primera y la última generación que comerá carne todos los días”, declara una de estas empresas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El director de una de las mayores empresas de carne alemanas llamó a las salchichas “el tabaco del futuro”. En una entrevista anunció que quería que para el 2019 el 30% de los beneficios de su empresa proviniesen de sus productos sustitutos de la carne. La industria cárnica alemana lo ha llamado “traidor” por ello. Sin embargo, ante la avalancha de consumidores <a href="https://igualdadanimal.org/blog/5-hechos-que-no-sabias-sobre-el-procesado-de-la-carne-antes-de-llegar-al-plato/">diciendo NO a la carne</a>, es probable que acaben uniéndose a él. El futuro de la carne no parece muy prometedor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="http://www.vegetalia.com/productes/categoria/4/salsitxes-hamburgueses-i-barretes">Los sustitutos de la carne</a> llevan ya tiempo en el mercado alemán (y el europeo). Sin embargo esta nueva generación de deliciosas alternativas están enfocadas al público general. A los amantes de la carne. Las empresas saben que la reducción del consumo de carne y el vegetarianismo están creciendo. La salud, <a href="https://igualdadanimal.org/blog/7-datos-de-la-produccion-y-consumo-de-carne-que-estan-acabando-con-los-recursos-del/">el medioambiente</a> y la protección de los animales son las principales causas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los estudios de tendencias del mercado muestran el crecimiento. Y lo hacen a costa de la carne. Las previsiones para el 2020 hablan de un crecimiento significativo de estas saludables alternativas. Las mismas previsiones hablan de un decrecimiento de la carne. Estamos entrando en una nueva era como consumidores. Somos más responsables. <a href="https://igualdadanimal.org/noticia/2015/10/11/la-ganaderia-industrial-es-uno-de-los-peores-crimenes-de-la-historia/">Nos importan los animales</a>. Nos importa el medioambiente y nuestra salud. Y nos desagrada la carne y el sufrimiento que conlleva. Por no hablar de l<a href="https://igualdadanimal.org/blog/7-razones-por-las-que-la-carne-de-pollo-no-es-tan-barata-como-parece/">os muchos antibióticos que contiene</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><sub><em>Imagen: www.ZsusVeganPantry.com</em></sub></p>
<!-- /wp:paragraph -->