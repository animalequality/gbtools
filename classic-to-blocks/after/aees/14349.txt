<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dieron comienzo en Pamplona el pasado Miércoles las fiestas de San Fermín 2006 con el lanzamiento del chupinazo desde la Casa Consistorial como viene siendo costumbre cada año. El viernes 7, día de San Fermín, comenzaron los encierros taurinos probablemente el acontecimiento más popular de las fiestas. Durante un encierro varios toros aterrorizados y confusos son obligados a correr por la ciudad de Pamplona entre la gente que corre delante de ellos con el objetivo de no ser lastimados por los animales no humanos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dieron comienzo en Pamplona el pasado Miércoles las fiestas de San Fermín 2006 con el lanzamiento del chupinazo desde la Casa Consistorial como viene siendo costumbre cada año.

El viernes 7, día de San Fermín, comenzaron los encierros taurinos probablemente el acontecimiento más popular de las fiestas. Durante un encierro varios toros aterrorizados y confusos son obligados a correr por la ciudad de Pamplona entre la gente que corre delante de ellos con el objetivo de no ser lastimados por los animales no humanos. Más adelante todos los toros mueren asesinados en la plaza de toros de la ciudad. Y entre tanto sufrimiento y muerte experimentado por unos, la gente se divierte durante una semana, convirtiendo a los San Fermines en unas fiestas conocidas en todo el mundo por lo alegres y divertidas que resultan. 

Pero desgraciadamente durante los San Fermines siempre hay heridos, algunas/os de las/os heridos han elegido voluntariamente participar en los "arriesgados" encierros. Otros, la mayoría de las víctimas, son heridas física y psicológicamente además de asesinadas, y siempre participan en las "fiestas" porque son obligados a ello.</p>
<!-- /wp:paragraph -->