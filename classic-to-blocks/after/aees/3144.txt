<!-- wp:paragraph -->
<p>Los hogares españoles consumen una media de 12 huevos-cáscara de gallina por persona y mes y un total de 141 al año (incluyendo huevos iofilizados empleados en la industria la cifra asciende a 210). Así se se desprende de los últimos datos disponibles en el Panel de Consumo del Ministerio de Agricultura, Pesca y Alimentación (MAPA).

El MAPA indicó hoy en un comunicado que en el 69% de los hogares se compran cada mes huevos de gallina y que este consumo es constante casi todo el año, aunque precisó que durante el verano baja ligeramente el porcentaje de hogares compradores.

En cambio, el consumo de huevos de otras aves sí varía en los meses invernales, sobre todo en el periodo navideño, en el que se se duplica el número de hogares compradores y la cantidad huevos de otras aves adquiridos.

Por hogares, los más maduros, es decir, los de parejas adultas y sin hijos o adultos independientes retirados), son los que consumen más huevos de gallina, sobre todo, las parejas adultas sin hijos que alcanzan los 226 huevos por persona y año, muy por encima de los 141 de media, mientras que las parejas con hijos pequeños consumen 81 al año.

En cuanto al consumo por tipo de hábitat, los valores más altos se encuentran en las poblaciones de menos de 2.000 habitantes, frente a las ciudades de más de 500.00 habitantes, en las que se come una media de 137 huevos al año.

Por clases sociales, los de clase media-baja son los mayores consumidores esta clase de huevos de gallinas, mientras que las clases más acomodadas prefieren los huevos de otras aves.

LOS NAVARROS, LOS QUE CONSUMEN MÁS HUEVOS DE GALLINA

En cuanto a las CCAA, los navarros se sitúan a la cabeza al consumir 189 huevos de cáscara por persona y año (48 huevos más que la media), seguidos de los riojanos (185) y los asturianos (177), frente a los canarios y baleares que consumen entre 20 y 30 huevos menos que la media.

Según los datos del Panel, la cantidad total de huevos consumidos por los hogares españoles en lo que va de año (6.262 millones de unidades) disminuye un -1,2% respecto al año anterior, por la reducción del consumo de huevos de gallina (-1,3%).

El gasto por individuo y año en huevos crece en 2007 un 0,6%, y se sitúa en unos 16 euros, de los que sólo 0,15 euros se gastan en huevos de otras aves, un 8% más por persona respecto al año anterior.

La evolución en euros del mercado (699 millones de euros) es creciente en casi un 3%, liderada por el crecimiento de los huevos de gallina en un 2,9% y apoyada por la de los huevos de otras aves con un 10,2%.


Fuente: Terra


Los huevos son productos obtenidos de la explotación animal, e implican el sufrimiento y la muerte de las gallinas. Para obtener más información sobre las consecuencias del consumo de huevos, se puede visitar esta página: <a href="https://igualdadanimal.org/problematica/huevos/">https://igualdadanimal.org/problematica/huevos/</a></p>
<!-- /wp:paragraph -->