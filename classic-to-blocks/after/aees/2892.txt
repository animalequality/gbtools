<!-- wp:paragraph -->
<p>Washington.- La policía del estado de California ha arrestado a un hombre por la acusación de torturar a una tortuga.

Según fuentes policiales de la localidad de Ventura, el hombre acuchilló a la tortuga, llamada Bob y con una edad de 25 años, para quitarle el caparazón. Bob fue encontrado en un depósito de basura, tras el aviso de una mujer. Actualmente se está recuperando, y tiene que recibir alimentación a través de un tubo.

Por su parte, el presunto agresor se encuentra en prisión, y está siendo investigado.

Hay que decir que esta noticia seguramente no hubiera aparecido en los medios de comunicación si Bob no estuviera considerado como un individuo de una "especie en peligro de extinción", y si Bob no hubiera sido "robado" a su "dueño" legal (en la actualidad sólo los humanos somos considerados sujetos de Derecho, y el resto de animales legalmente son considerados propiedades). Debemos oponernos al comercio de animales (tanto de tortugas como de cualquier otro), y no participar en el mismo.</p>
<!-- /wp:paragraph -->