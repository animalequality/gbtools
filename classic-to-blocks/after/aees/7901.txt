<!-- wp:image {"id":12439} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/hamburguesa_imposible.jpg" alt="" class="wp-image-12439"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Desde su planta en Oakland (California), la empresa que está revolucionando la forma en que vemos la carne podrá introducir sus «hamburguesas imposibles» en al menos <strong>1.000 restaurantes en EEUU y lanzarlas al mercado minorista e internacional para fines de año.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/06/28/hampton-creek-se-une-la-carrera-por-llevar-la-carne-limpia-los-consumidores/" target="_blank">Hampton Creek se une a la carrera por llevar la «carne limpia» a los consumidores</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La hamburguesa imposible está hecha a partir de una combinación de vegetales pero su ingrediente estrella es el heme, una molécula presente en animales y plantas. Es ella la que permite que esta hamburguesa sangre y tenga así una apariencia increíblemente parecida a la carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/R_1VRJAuTy4" width="854"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«Nuestra misión de transformar el sistema alimentario mundial es urgente, y la oportunidad es enorme».</p></font>

<p>&nbsp;</p>

<p>Impossible Foods afirma que su hamburguesa <strong>«luce, se siente, huele, se cocina y sabe como la carne picada de vaca» pero su huella medioambiental es mucho menor. </strong>Utiliza 75% menos agua, genera 87% menos gases de efecto invernadero y requiere 95% menos tierra para su fabricación que las hamburguesas tradicionales.</p>

<p>«Nuestra misión de transformar el sistema alimentario global es urgente, y la oportunidad es enorme; por eso nos estamos embarcando en uno de los aceleradores más ambiciosos de cualquier inicio en la industria alimentaria», afirma el director ejecutivo Patrick O. Brown.</p>

<p>Con la nueva planta de Oakland<strong> la empresa ha incrementado su producción en un 250%</strong>, y aunque muchos restaurantes han tenido ya la oportunidad de ofrecer la hamburguesa imposible, la demanda ha aumentado tanto que la distribución será ahora a nivel nacional.</p>

<p>Desde sus inicios, Impossible Foods se trazó un objetivo muy claro: llegar a las personas que aman comer carne. Además de las grandes cadenas de restaurantes, han logrado convencer a inversionistas de la talla de Bill Gates, la empresa Temasek y Open Philanthropy Project, de quienes han ya recibido financiación por 75 millones de dólares.</p>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://www.foodbev.com/news/impossible-foods-opens-new-oakland-facility-boost-output/" target="_blank">https://www.foodbev.com/news/impossible-foods-opens-new-oakland-facility-boost-output/</a></p>

<p><a href="https://thecounter.org/the-impossible-burger-factory-is-open-for-business/" target="_blank">https://thecounter.org/the-impossible-burger-factory-is-open-for-business/</a></p></font>
<!-- /wp:html -->