<!-- wp:image {"id":12706} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/12/shutterstock_143972716.jpg" alt="" class="wp-image-12706"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Poner impuestos a productos que puedan tener un impacto negativo en la salud es una estrategia que vienen siguiendo varios gobiernos desde hace años.</strong> En 180 países se pagan impuestos por el tabaco, en más de 60 por la emisiones de efecto invernadero y en 25 al azúcar. Y según un informe del grupo inversor Farm Investment Risk and Return (FAIRR), el producto que sigue en esta lista es la carne.<br>
<br>
Según FAIRR, aplicar un impuesto a la carne será inevitable y probablemente esto se hará a nivel internacional en un plazo de cinco a diez años. La idea detrás de esto, por supuesto, tiene que ver con el hecho de que l<strong>a industria ganadera es responsable del 14,5 % de los gases de efecto invernadero que se emiten en el mundo</strong>, y con los problemas de salud relacionados con el consumo de carne. &nbsp;Además, el derroche de recursos como el agua y tierras y el abuso de antibióticos por parte de la industria también juegan un rol esencial en la producción de carnes.<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/10/17/la-produccion-carnica-actual-esta-llegando-su-limite/" target="_blank">«La producción cárnica actual está llegando a su límite»</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Si los diseñadores de políticas deben cubrir el verdadero costo de epidemias humanas como la obesidad, la diabetes y el cáncer, a la vez que abordar desafíos paralelos como el cambio climático y la resistencia a los antibióticos, los impuestos a la industria de carne parecen inevitables», advirtió Jeremy Coller, fundador de FAIRR, al periódico The Guardian. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12707} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/12/shutterstock_217956703.jpg" alt="" class="wp-image-12707"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ya el año pasado <a href="https://igualdadanimal.org/noticia/2017/10/17/la-produccion-carnica-actual-esta-llegando-su-limite/" target="_blank">un estudio de la Universidad de Oxford</a> recomendaba gravar con un impuesto la carne de bovino un 40% y los lácteos un 20%. Esto <strong>podría evitar casi medio millón de muertes al año y recortaría drásticamente las emisiones de gases de efecto invernadero.</strong><br>
<br>
Según el informe, el impuesto a las carnes ya está siendo discutido en los parlamentos de Alemania, Dinamarca, Suecia y China. La directora de FAIRR, Maria Lettini, comenta que a medida que avance la implementación del acuerdo climático de París comenzaremos a ver acciones gubernamentales que busquen la reducción del impacto ambiental de la industria ganadera, y que <strong>es probable que dentro de un plazo de cinco a diez años se aplique algún impuesto a la carne. &nbsp;&nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</p></font>

<p>&nbsp;</p>

<h4>Hasta el momento los gobiernos han ignorado la necesidad de implementar cambios en la forma en que se producen los alimentos que consumimos. Estos prefieren la idea de aplicar impuestos antes que prohibir su consumo porque saben que los productos se seguirán consumiendo y con esto obtienen ingresos importantes.</h4>

<h4>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/11/03/el-futuro-de-la-carne-sera-libre-de-la-crueldad-de-las-granjas-y-mataderos/" target="_blank">El futuro de la carne será libre de la crueldad de las granjas y mataderos</a></h4>

<p>&nbsp;</p>

<figure><img alt="" class="wp-image-12708" src="/app/uploads/2017/12/shutterstock_142619857.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>Pero las emisiones de gases de efecto invernadero, causantes del cambio climático, siguen en aumento, ya que ciertos segmentos de la población que alcanzan un mayor poder adquisitivo elevan exponencialmente el consumo de carne global. &nbsp;Los autores del estudio de Oxford consideran que el dinero recaudado a partir de los impuestos podría utilizarse para asegurar que otras poblaciones tuviesen acceso a alimentos más saludables.<br>
<br>
«Es algo ya sabido que si no hacemos algo para parar las emisiones que generan las industrias alimentarias, no tendremos ninguna opción de limitar el calentamiento global por debajo de los 2ºC», declaró el director del estudio, Marco Springmann. &nbsp;Y añade «pero si tienes que pagar un 40% más por tu chuleta, puede que elijas comerla una vez a la semana en vez de dos».</p>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://www.elespectador.com/ambiente/empieza-el-camino-del-impuesto-a-la-carne-article-727834/" target="_blank">https://www.elespectador.com/ambiente/empieza-el-camino-del-impuesto-a-la-carne-article-727834/</a></p>

<p><a href="https://gastronomiaycia.republica.com/2017/12/12/un-impuesto-en-los-productos-carnicos-sera-inevitable/" target="_blank">https://gastronomiaycia.republica.com/2017/12/12/un-impuesto-en-los-productos-carnicos-sera-inevitable/</a></p></font>
<!-- /wp:html -->