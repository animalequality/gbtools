<!-- wp:paragraph -->
<p>El tetracampeón mundial de Fórmula 1, Lewis Hamilton, lanzó recientemente un impactante mensaje a sus 7 millones de seguidores a favor de los animales atrapados en la industria cárnica

Tras asegurar su victoria en el Gran Premio de Alemania, Hamilton compartió una historia en su cuenta de Instagram con un contundente y conmovedor mensaje: «619 millones de humanos han sido asesinados en guerras a todo lo largo de nuestra historia registrada. Los humanos matan a ese mismo número de animales cada 5 días».

Y debajo de la imagen escribió: «Chicos, sigo una alimentación basada en vegetales y lo he hecho ya durante un año y me siento mejor que nunca. Si más personas se dan cuenta y no hacen la vista gorda, podemos evitar que esto suceda. Por favor, hagan un espacio en su corazón para no apoyar esta horrible crueldad y prueben comenzar a alimentarse de forma vegana».
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large; color: #000000;">¿Quieres recibir las mejores noticias de actualidad sobre los animales? </span></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large; color: #000000;">¡Suscríbete gratuitamente a <span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">nuestro e-boletín!</a></span></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Hace ya casi un año, luego de ver el documental What the Health, Hamilton decidió dejar la carne de vaca, luego la de pollo y, finalmente, el pescado. Desde entonces, el piloto ha atribuido sus victorias en las carreras a su nueva alimentación, las cuales le han permitido defender su título de Campeón Mundial de Fórmula 1 a lo largo de 2018.</p>
<!-- /wp:paragraph -->