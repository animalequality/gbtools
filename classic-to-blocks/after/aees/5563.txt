<!-- wp:paragraph -->
<p>
	Los matarifes afectados&nbsp;por el cierre del matadero de Zorroza, aglutinados en la Plataforma&nbsp;<em>Bizkaian Hiltegia Orain. Zorroza zabaldu</em>, reparten este lunes animales vivos como señal de protesta, según ha informado CC.OO.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	El sindicato ha precisado que, después de que el reparto de animales vivos que tenían previsto para la semana pasada no se pudiera llevar a cabo por problemas higiénicos y de prevención ante las altas temperaturas, el lunes finalmente procederán a repartir cochinillos y perdices, entre otros animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Los afectados reclaman una salida para la reanudación inmediata del sacrificio en el Matadero de Bilbao hasta que se cree una infraestructura suficiente que garantice el sacrificio en Bizkaia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	CC.OO. ha indicado que las movilizaciones continuarán, al tiempo que se siguen produciendo contactos con el Departamento de Agricultura del Gobierno vasco y el Ayuntamiento de Bilbao, entre otros. El sindicato ha denunciado que las instituciones, hasta el momento, no han dado "respuesta o una solución" a un problema que afecta a muchos colectivos.</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>
	Todos estos animales son los afectados por el consumo que la sociedad demanda. Podemos aprovechar este acto para ser partícipes de su liberación. Animaos y acercaos a recoger a cuantos podáis, cuantos más seamos más animales podrán disfrutrar de sus vidas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Es importante no hacer ver que somos activistas, y hacer la liberación lejos del lugar , asi podremos salvar a muchos más. El lugar donde realizarán la entrega es en la calle Gran Vía, 85 de Bilbao (cercal del Sagrado Corazón).</p>
<!-- /wp:paragraph -->