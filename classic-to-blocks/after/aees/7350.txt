<!-- wp:paragraph -->
<p>Sustituir la carne en nuestra alimentación y pasar a llevar dietas más vegetarianas <strong>será un factor crítico</strong> para conseguir mantener el calentamiento global dentro del “nivel de riesgo límite” de 2ºC. Así avisa a los gobiernos el Real Instituto de Asuntos Internacionales Chatham House, afincado en Londres.

<a href="https://www.chathamhouse.org/2015/11/changing-climate-changing-diets-pathways-lower-meat-consumption">El informe</a> ha visto la luz a una semana del inicio de la <strong>Cumbre del Clima en París</strong>. Esta cumbre tiene como objetivo llegar a acuerdos internacionales para conseguir limitar el calentamiento global a 2ºC, por encima de los cuales, de producirse, <strong>los desastres globales serían irreparables</strong>.

<a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/">La ganadería</a> es la responsable del 15% del total de las emisiones de gases de efecto invernadero a nivel mundial. Este porcentaje <strong>supera al de toda la industria de transportes mundial junta</strong>, incluidos coches, aviones y barcos.

El informe pone de manifiesto que el consumo de carne ha alcanzado ya niveles de <a href="https://igualdadanimal.org/noticia/2015/11/20/superbacterias-resistentes-antibioticos-halladas-en-carne-de-cerdo-y-pollo/">problema de salud pública</a> y va en aumento. Se calcula que el consumo global de carne aumentará un 75% para el 2050.

Los expertos internacionales de Chatham House avisan de que los gobiernos <strong>están perdiendo la oportunidad </strong>crítica para aliviar el calentamiento global, <a href="https://igualdadanimal.org/noticia/2015/11/04/la-ganaderia-industrial-chantajea-la-union-europea/">atrapados en bucles de inercia</a>. A pesar de la imperiosa necesidad de abordar el problema del excesivo consumo de carne y promocionar un cambio de modelo alimenticio, los gobiernos temen las consecuencias de intervenir. Además, <strong>la escasa conciencia pública</strong> hace que no sientan la necesaria presión para hacerlo.

Los hallazgos fundamentales del demoledor informe son:

La conciencia pública sobre la relación entre el cambio climático y la ganadería es escasa.

La investigación que han llevado a cabo muestra que las sociedades de distintos países, culturas y continentes opinan que son los gobiernos los que han de liderar a las sociedades hacia una reducción drástica del consumo de carne.

El mensaje que se lance para concienciar a las sociedades debe ser simple. Se deben realizar esfuerzos comunes para lanzar mensajes impactantes que calen en las sociedades. El mensaje central ha de ser claro: <strong>globalmente, hay que comer menos carne o sustituirla por opciones más saludables</strong>.

Para poder concienciar a las sociedades es necesario que el mensaje sea lanzado por fuentes públicas en las que las personas confíen. La confianza en los gobiernos varía de país a país, pero a nivel general los informes de expertos se muestran como las fuentes en las que más confía la sociedad.

Si quieres ayudar a <strong>detener el cambio climático</strong>, además de salvar la vida de incontables animales, <a href="https://es.loveveg.com/">aquí puedes descargarte gratuitamente</a> nuestra guía para una alimentación saludable y compasiva. <strong>Millones de personas</strong> ya lo están cambiando sus hábitos alimenticios en todo el mundo, ayudando así a millones de animales y al planeta.</p>
<!-- /wp:paragraph -->