<!-- wp:paragraph -->
<p>Los delfines y lobos marinos son continuamente torturados mientras son cautivos. 

Una investigación ha mostrado las torturas que sufren y las enfermedades que padecen. Aunque dichos acuarios son mostrados como lugares de entretenimiento, para esos individuos no supone ninguna diversión. 

El modo en que los delfines y lobos marinos sufren su cautiverio ha sido mostrado en el libro "Delfinarios", de Laura Rojas y Yolanda Alaniz. Es el resultado de una investigación que ha abarcado siete años y 24 prisiones. Laura Rojas señala que los delfines sufren enfermedades que no padecerían estando en libertad. Al respecto dice lo siguiente:

“Si no hacen los ejercicios no les dan de comer, por lo que hay hambre y sufrimiento. Las albercas tienen exceso de cloro y poca sal, por lo que es común la ceguera, así como quemaduras en la piel y ámpulas.

“En nuestro informe de morbilidad descubrimos que los delfines se la pasan enfermos de neumonía, gastritis y úlceras; además algunos han muerto por comer objetos extraños, como balones o bolsas de dulces.

“Incluso reportamos una delfina que había muerto por comer un kilo 800 gramos de hojas de árbol y una bola de mecate. Eso los empresarios no lo reportan, o mienten a las autoridades, argumentando que murieron de vejez.

“En mar abierto, los delfines nadan 150 kilómetros diarios a profundidades hasta de 100 kilómetros. En la alberca, solamente unos 25 metros, lo cual también les genera mucho estrés.”

Según Laura Rojas, la Ley General de Vida Silvestre "es una normativa hecha a medida de las empresas de los delfinarios y no de las necesidades fisiológicas y biológicas de los delfines". Esto se explica por el hecho de que los animales no-humanos (recordemos que los humanos también somos animales) son considerados actualmente propiedades, y legalmente las propiedades no van a ver ver protegidos sus intereses por las leyes. Es una de las causas de que las campañas bienestaristas sean infructuosas a la hora de conseguir sus objetivos.

Nadar con los delfines y emplear a estos como instrumentos terapéuticos son formas de esclavitud más rentables para los propietarios que los espectáculos tradicionales en acuarios.

“Un boleto en el delfinario, como el del zoológico de San Juan de Aragón, puede costar 30 pesos, pero uno para el nado con delfines cuesta entre mil 800 y 2 mil pesos. Aunque la norma dice que con cada delfín pueden nadar cuatro personas, en la práctica lo hacen hasta 15".

“A cuatro sesiones por día de 40 minutos, la ganancia es enorme. En el caso de la definoterapia, 12 sesiones cuestan unos 15 mil pesos".

“Para realizar esas terapias, los delfines tienen que estar mucho tiempo quietos; si no, tampoco les dan de comer”.

Los delfines son individuos muy inteligentes. Son capaces de reconocer la muerte, desarrollan complejas relaciones sexuales, pueden reconocerse en un espejo (lo cual muestra que son conscientes de su propia identidad) e incluso se ponen nombres entre sí.

En la organización Igualdad Animal consideramos que lo más efectivo que podemos hacer para que los animales no-humanos sean respetados es rechazar su uso en los diversos ámbitos (alimentación, vestimenta, entretenimiento, investigación). Asimismo reivindicamos el respeto hacia los demás animales por el hecho de que son capaces de sentir. Esta capacidad es la única característica relevante para que un individuo sea considerado moralmente y respetado dado que indica que se puede ver afectado por nuestros actos y tiene intereses propios.</p>
<!-- /wp:paragraph -->