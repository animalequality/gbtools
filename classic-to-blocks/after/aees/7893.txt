<!-- wp:image {"id":12404} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/shutterstock_106125503.jpg" alt="" class="wp-image-12404"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Reducir o eliminar tu consumo de carne <strong>libera a los animales de granja de una vida de inmenso sufrimiento</strong> y de la muerte. Pero ¿sabías que, además, beneficia tu salud?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Un grupo de investigadores publicó recientemente resultados sorprendentes al respecto. En una revisión exhaustiva de casi 50 estudios examinaron los <strong>efectos que una alimentación a base de vegetales tiene a largo plazo en nuestros niveles en sangre de lípidos, colesterol y triglicéridos.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El <a href="https://academic.oup.com/nutritionreviews/article/doi/10.1093/nutrit/nux030/4062197/Association-between-plantbased-diets-and-plasma%20d" target="_blank">metaanálisis</a> de 30 estudios observacionales y 19 ensayos clínicos arrojó que las alimentaciones basadas en vegetales se asocian <strong>con niveles significativamente más bajos de colesterol total.</strong> Todos los hallazgos se registraron en comparación con una alimentación omnívora.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12405} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/shutterstock_335916848_0.jpg" alt="" class="wp-image-12405"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los niveles de colesterol asociados con esta alimentación fueron de un total de 29,2 mg/dL menos de la media de aquellos para una alimentación omnívora. El consumo de verduras, legumbres y cereales fue asociado también con la disminución del peso corporal y de la ingesta de grasas saturadas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para los autores de la revisión de los 50 estudios (los doctores Yoko Yokoyama, Susan Levin y Neal Barnard), <strong>los niveles de colesterol pueden mantenerse en niveles bajos a través de una alimentación adecuada y ejercicio físico.</strong> También mantienen la hipótesis de que las alimentaciones basadas en vegetales tienen un gran impacto no solo en el peso corporal, sino en la salud en general.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fuente: <a href="https://www.webconsultas.com/noticias/dieta-y-nutricion/las-dietas-vegetarianas-se-asocian-a-niveles-bajos-de-colesterol-malo" target="_blank">https://www.webconsultas.com/noticias/dieta-y-nutricion/las-dietas-vegetarianas-se-asocian-a-niveles-bajos-de-colesterol-malo</a></p>
<!-- /wp:paragraph -->