<!-- wp:image {"id":12665} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/12/shutterstock_271498517.jpg" alt="" class="wp-image-12665"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Como una estrategia para promover acciones contra el cambio climático, <strong>el ministro francés de Transición Ecológica, Nicolás Hulot, ha propuesto que muy pronto en todos los colegios del país se implante un menú vegetariano una vez a la semana.</strong> Fomentar hábitos alimenticios más saludables entre los más jóvenes que, además, contribuyan a reducir nuestro impacto en planeta es el objetivo de la iniciativa.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La propuesta va también de la mano con los debates actuales en torno al sufrimiento que padecen los animales en manos de la industria cárnica. Según Hulton, servirían para «llevar pronto una gran reflexión sobre la condición animal con el Ministro de Agricultura, porque estoy convencido de que las mentalidades han evolucionado enormemente en este tema, y ​​es un indicio de civilización». Y continúa afirmando que <strong>«el animal tiene conciencia» y que es necesario «reducir al máximo su sufrimiento».</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/08/29/7-escuelas-de-los-angeles-ofreceran-menu-vegetariano-sus-alumnos/" target="_blank">7 escuelas de Los Ángeles ofrecerán menú vegetariano a sus alumnos</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>La industria ganadera envía al matadero 70.000 millones de animales al año</strong>, es decir, una cantidad de cerdos, vacas, terneras, pollos y gallinas equivalente a ocho veces la población de seres humanos del planeta. También es la causante del peor maltrato animal en la historia: en sus manos han muerto la mayor cantidad de animales padeciendo el mayor sufrimiento a lo largo de sus vidas. Además, dentro de granjas y mataderos la crueldad es la norma ya que estos animales carecen de la protección legal que tienen, por ejemplo, gatos o perros. <strong>Si alguien hiciera a nuestros perros o gatos lo que la ganadería hace a los animales de granja, terminaría en la cárcel.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12666} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/12/14848366384_4b2c0f9a18_z.jpg" alt="" class="wp-image-12666"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por otra parte, cada vez son más los estudios que proponen seriamente la reducción del consumo de carne animal como una forma de combatir el cambio climático. Según la FAO, la Agencia de Naciones Unidas para la Alimentación y la Agricultura, <strong>el 14,5% de los gases de efecto invernadero emitidos por la acción humana provienen del sector de la ganadería.</strong> &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>«También es una cuestión de educación, así que espero que, pronto, los comedores escolares ofrezcan a los niños un menú vegetariano un día a la semana», afirma el ministro. «En mi familia somos cinco. Dos son vegetarianos. Los otros tres, entre los que me cuento, solo comemos carne una vez por semana. Cada uno sigue su camino», explica Hulot en una entrevista en la revista L’Obs.</p>

<p>A fin de reducir el consumo de carne y avanzar hacia la construcción de un sistema alimentario más sostenible, el laboratorio de ideas Terranova sugirió también implantar un día vegetariano a la semana tanto en colegios como en escuelas secundarias.</p>

<p>El objetivo a perseguir sería <strong>«en las próximas dos o tres décadas, reducir a la mitad nuestro consumo de carne animal y revertir la proporción actual de proteínas animales y vegetales en nuestra alimentación».</strong> Esto significaría &nbsp;apuntar a una dieta donde el 60% de las proteínas sean de origen vegetal, contra aproximadamente el 40% actual.</p>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="https://elpais.com/elpais/2017/11/30/mamas_papas/1512065255_961318.html" target="_blank">https://elpais.com/elpais/2017/11/30/mamas_papas/1512065255_961318.html</a></p>

<p><a href="https://www.leparisien.fr/societe/nicolas-hulot-veut-un-menu-vegetarien-un-jour-par-semaine-a-l-ecole-30-11-2017-7425126.php" target="_blank">https://www.leparisien.fr/societe/nicolas-hulot-veut-un-menu-vegetarien-un-jour-par-semaine-a-l-ecole-30-11-2017-7425126.php</a></p>

<p><a href="https://www.huffingtonpost.fr/life/article/nicolas-hulot-voudrait-un-menu-vegetarien-dans-les-cantines-scolaires-un-jour-par-semaine_112987.html" target="_blank">https://www.huffingtonpost.fr/life/article/nicolas-hulot-voudrait-un-menu-vegetarien-dans-les-cantines-scolaires-un-jour-par-semaine_112987.html</a></p></font>
<!-- /wp:html -->