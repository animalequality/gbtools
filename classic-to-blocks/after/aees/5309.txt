<!-- wp:paragraph -->
<p>Prohibido el sexo entre personas y animales en los Países Bajos. La Cámara Alta del Parlamento holandés ha prohibido la distribución, producción y venta de contenido zoofílico. Se estima que casi el 80% de la producción mundial de pornografía animal se realizaba en la tierra de los tulipanes.

La nueva ley prohíbe el sexo de humanos con animales, tanto en público como en privado, independientemente de que el animal resulte herido o no. Además, prohíbe la distribución de pornografía animal.
El proyecto de ley se presentó en abril de 2007 y fue aprobado por la cámara baja en julio de 2008, aunque tardó en llegar a la cámara alta para su aprobación final. Aún no está claro cuándo entrará la ley en vigor.

Menos zoofilia en la Red 

Expertos consultados por la agencia Reuters considera que la cantidad de material de zoofilia en Internet disminuirá considerablemente porque los Países Bajos eran uno de los mayores productores de este tipo de contenido sexual. Esta práctica era legal siempre que se demostrase que no se hacía ningún daño a los animales.

Los distribuidores en los Países Bajos eran responsables de alrededor de un 80% de los vídeos de zoofilia mundiales según un estudio que realizó el diario holandés Algemeen Dagblad en el año 2007.
 

fuente. telecinco.es</p>
<!-- /wp:paragraph -->