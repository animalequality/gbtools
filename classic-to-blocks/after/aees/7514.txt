<!-- wp:paragraph -->
<p>La ganadería industrial tiene los días contados si queremos asegurar un futuro a nuestro planeta. Producir carne es un desastre ecológico. Las evidencias se amontonan en forma de estudios científicos, artículos de expertos y campañas de organizaciones y ongs.

Existen muchas razones para que la carne pase a ser un producto del pasado. Las razones medioambientales son una de ellas. <strong>El futuro está en juego</strong>. ¿Estaremos <a href="https://igualdadanimal.org/noticia/2016/03/23/las-profundas-consecuencias-de-un-modelo-alimentario-basado-en-vegetales/">del lado del planeta</a> o la industria cárnica seguirá haciéndonos creer que no existe problema con consumir carne?

He aquí algunas razones que <strong>cualquier persona con sensibilidad ecológica</strong> debería tener en cuenta:

<strong>1. Emisión de gases de efecto invernadero</strong>

Una reducción del 50% del consumo de carne y leche en los países desarrollados podría conllevar <strong>un ahorro del 40%</strong> en la pérdida global de nutrientes y la emisión de <a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/">gases de efecto invernadero</a>.

<strong>2. Mala gestión de los alimentos</strong>

Alrededor del 35% de las cosechas mundiales <strong>se destinan a alimentar a los animales que nos comemos, en vez de alimentar directamente con ellas a la humanidad</strong>.

<strong>3. Dióxido de carbono</strong>

En relación con el dato anterior, los científicos predicen que para el año 2050 ya <strong>solo el dióxido de carbono de la agricultura mundial</strong> sobrepasará el máximo permitido para evitar la catástrofe del calentamiento global. A día de hoy ya supone un tercio del total. La ganadería industrial es la máxima responsable.

<strong>4. Derroche de agua</strong>

Producir 1 kg de carne de pollo requiere la misma cantidad de agua que la que gastamos en 82 duchas. En el caso de la carne de cerdo, 1 kg es equivalente al agua de 115 duchas. Por último 1 kg de carne de vaca equivale al agua de 288 duchas.

<strong>5. Deforestación</strong>

¿Adivinas para qué se destinan los millones de toneladas de soja producidas cada año a costa de las selvas y la biodiversidad? Según <a href="http://d2ouvy59p0dg6k.cloudfront.net/downloads/wwf_soy_scorecard_2016_r6.pdf">un informe de WWF</a>, <strong>alrededor del 75% de la producción mundial de soja se destina a alimentación de los animales de granja</strong>. En Europa la proporción es aún mayor: se estima que el 93% de la soja que entra en Europa se destina a alimentar a los animales que luego nos comemos.

Muchos europeos aún no saben que de media consumen al año 61 kgs de soja, principalmente <strong>como consecuencia de su consumo de carne y leche de procedencia animal</strong>, ni que este consumo tiene un impacto severo en las selvas de Sudamérica.

<strong>6. Desaparición de acuíferos</strong><a href="https://revealnews.org/blog/the-world-wouldve-already-run-out-of-water-if-everyone-ate-like-americans/">Un informe</a> llevado a cabo por la multinacional Nestlé predice que <strong>un tercio de la población mundial será afectada por escasez severa de agua para el 2025</strong>. La situación empeorará convirtiéndose en potencialmente catastrófica en 2050.

<strong>7. Un modelo alimentario insostenible</strong><a href="http://www.futureoffood.ox.ac.uk/news/plant-based-diets-could-save-millions-lives-and-dramatically-cut-greenhouse-gas-emissions">Un estudio</a> de la Universidad de Oxford proyecta que para el año 2050 la mitad de los gases de efecto invernadero que el mundo podría permitirse <a href="https://igualdadanimal.org/noticia/2015/11/26/un-comite-de-expertos-avisa-los-gobiernos-comer-menos-carne-es-la-unica-manera-de/">provendrían de nuestro actual modelo alimentario</a>. Ciñéndonos a una alimentación acorde a las directrices actuales las emisiones se reducirían un 29%; en <strong>una alimentación vegetariana se reducirían hasta un 63%; en una vegana, hasta un 70%</strong>.

Por favor, considera los <strong>beneficios globales de alimentarnos con alternativas a la carne</strong>. Con nuestra manera de alimentarnos podemos formar parte de la solución a algunos de los peores problemas de nuestros tiempos. <strong>Ayuda a lanzar un poderoso mensaje a las industrias que están destruyendo el planeta</strong>.</p>
<!-- /wp:paragraph -->