<!-- wp:image {"id":9778} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/11/fabre_gatos_belgica.jpg" alt="" class="wp-image-9778"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Jan Fabre, que <strong>provocó heridas de diversa índole a varios gatos</strong> durante el rodaje de un vídeo sobre su trayectoria, ha recibido hasta el momento más de 20.000 mensajes de repulsa, dos demandas por maltrato animal, varias agresiones físicas y la prohibición de continuar el rodaje de su vídeo en el zoo y otros lugares públicos de Amberes.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14077} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/11/jan_fabre_insectos.jpg" alt="" class="wp-image-14077" title="Escultura de Fabre realizada con los cadáveres de cientos de escarabajos"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El artista, conocido por utilizar habitualmente en sus<a href="http://www.bartschi.ch/ggb.php?opt=work&amp;id=7207&amp;op=showone&amp;size=large&amp;imgind=2" target="_blank"> obras</a> a <strong>insectos</strong>, <strong>pájaros, perros&nbsp;</strong>y <strong>felinos</strong>, quiso homenajear la célebre fotografía de Philippe Halsman, "<a href="https://es.wikipedia.org/wiki/Archivo:Salvador_Dali_A_(Dali_Atomicus)_09633u.jpg" target="_blank">Dali atomicus</a>", que muestra al pintor rodeado de 'gatos volantes'. Fabre intentó reproducir dicha performance danzando en las escaleras del Ayuntamiento de Amberes mientras sus colaboradores<strong> lanzaban a varios gatos por los aires, a un par de metros de altura.</strong> Pese a que las escaleras estaban equipadas en parte por material acolchado, los gatos, aterrorizados, sufrieron aterrizajes más o menos duros contra las escaleras, lastimándose y golpeándose en algunas ocasiones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fabre, abrumado por las críticas, dijo a un canal de televisión flamenca: <em>«Lamento profundamente que los gatos aterrizaran mal. Quiero pedir disculpas a los amantes de los gatos. No fue mi intención herir o lastimar a ninguno de ellos. Los gatos están bien.»</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>También afirmó más tarde a una cadena francesa que la cobertura que se había hecho de su "lanzamiento de gatos" había sido <em>«sensacionalista y exagerada»</em>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="573" id="molvideoplayer" scrolling="no" src="https://www.dailymail.co.uk/embed/video/3823.html" title="MailOnline Embed Player" width="698"></iframe></p>
<!-- /wp:paragraph -->