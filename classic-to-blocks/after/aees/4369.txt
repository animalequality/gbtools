<!-- wp:paragraph -->
<p>Una tortuga hembra marina de entre 20 y 25 años de vida fue apedreada hasta su muerte por varios desconocidos cuando desovaba en una playa del oriente venezolano, se informó hoy.

«Queremos saber quién o quiénes pudieron estar detrás de este hecho enmarcado en un ecocidio que es tipificado como un delito penal», dijo el teniente Keideer Ramírez, jefe de una de las zonas orientales de la Guardia Nacional (GN, policía militarizada).

El diario El Tiempo de la ciudad de Puerto La Cruz, del estado oriental de Anzoátegui, reveló hoy que el hecho se produjo el martes en la Playa Pupú y que «las huellas que dejó en la arena la tortuga eran evidentes este miércoles» después de que intentara desovar en el lugar.

«Inexplicablemente, fue masacrada a pedradas por sujetos desconocidos. Ayer, el cadáver del enorme quelonio se confundía entre la gran cantidad de personas que curioseaban alrededor de sus restos», añadió el periódico al dar cuenta de que «la noticia corrió como pólvora en todo el pueblo».

La directora del Centro de Investigación y Conservación de Tortugas Marinas (Cictmar), Hedelvy Guada, detalló al diario que la tortuga, que tenía entre 20 y 25 años de edad y que medía 1,46 metros de largo por 1,04 de ancho, murió por fractura craneal cuando estaba desovando.

Fuente: lavozdegalicia.es</p>
<!-- /wp:paragraph -->