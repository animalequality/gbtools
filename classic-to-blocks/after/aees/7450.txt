<!-- wp:image {"id":10492} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/noticia.jpg" alt="" class="wp-image-10492"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10493" src="/app/uploads/2016/03/shutterstock_69189196.jpg" style="float:left; margin:10px; width:450px">Estás en el sofá. Tu perro o tu gato se acerca a ti, se para, te mira y tú le devuelves la mirada. Observas sus ojos, su mirada te cautiva: sabes que tras ella hay alguien que te quiere y a quien quieres.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No podrías soportar ver sufrir a tu perro o a tu gato. Pero, ¿sabes qué? <strong>Hay quienes solo verían en ellos comida</strong>. Los matarían y se los comerían.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Seguro que la sola idea te repugna y te indigna. <strong>¿Cómo podría alguien comerse a un perro o a un gato?</strong> ¿Es que no ven sus miradas?, ¿no saben que tras ellas hay alguien a quien querer y que nos quiere?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hasta aquí todo normal. Lo tenemos claro: nuestros perros y gatos son nuestra familia y les queremos. <strong>Ellos no son comida: son nuestros amigos</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pero la cosa se complica: <strong>¿sabías que los cerdos son animales con mayor inteligencia que perros y gatos?</strong>, ¿que, de hecho, <a href="https://igualdadanimal.org/blog/la-inteligencia-de-los-cerdos-comparable-la-de-elefantes-o-delfines/" target="_blank">su inteligencia es comparable a la de delfines y chimpancés</a>?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>¿Sabías que los pollitos empiezan a comunicarse con su madre incluso antes de salir del huevo</strong> y que pasan los primeros días de vida emitiendo un característico piar para que su madre sepa dónde están en todo momento?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong><img alt="" class="wp-image-10495" src="/app/uploads/2016/03/shutterstock_175074488_0.jpg" style="float:right; margin:10px; width:450px">¿Sabías que las vacas pasan días enteros mugiendo, desgarradas por el dolor de ser separadas de sus hijos, los terneros y terneras que nos abastecen de carne?</strong> ¿Y que eso les sucede una vez tras otra, en cada parto, tras ser inseminadas artificialmente una y otra vez?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>¿No son todos estos rasgos de los animales de granja muestra de que también hay alguien tras sus miradas?</strong> Si vivieran con nosotros y fueran ellos los que se aproximaran y nos miraran, ¿nos los comeríamos igualmente?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Son comida los perros y gatos? ¿Lo son las vacas, cerdos y pollos? <strong>¿Que sentiría alguien que conviviese con un cerdito como mascota si nos lo comiéramos?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Y la pregunta más importante de todas: en un mundo en el que <a href="https://igualdadanimal.org/noticia/2016/03/23/las-profundas-consecuencias-de-un-modelo-alimentario-basado-en-vegetales/" target="_blank">podemos alimentarnos perfectamente</a> sin dañar a los animales, ¿por qué elegir comérnoslos?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Algo no encaja en nuestra manera de ver a unos y otros animales. <strong>Muy dentro de nosotros, en un lugar entre nuestro cerebro y nuestro corazón</strong>, <strong><a href="https://igualdadanimal.org/noticia/2016/03/24/9-cosas-increiblemente-buenas-para-todos-que-suceden-cuando-alineas-tus-valores-sobre/" target="_blank">el interruptor de la empatía</a> hacia los animales de granja permanece oculto</strong>. Las luces de la publicidad de la cruel industria cárnica mantienen distraída nuestra capacidad de ponernos en su lugar.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10496} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/shutterstock_253873549.jpg" alt="" class="wp-image-10496"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Mientras, en un infernal e inhumano matadero, cerca, muy cerca de nosotros, <strong>un animal espera su turno en la fila, camino del matarife</strong>. Sus preciosos ojos observan aterrados alrededor, pero no tiene escapatoria.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A menos que nosotros decidamos hacer algo por él <a href="https://igualdadanimal.org/blog/7-fotos-de-animales-de-granja-que-comparan-como-son-sus-vidas-y-como-podrian-ser/" target="_blank">y por todos los que vendrán después</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Lo haremos?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Si quieres dar los primeros pasos hacia una alimentación que tenga en cuenta a los animales de granja visita las fantásticas websites <a href="https://www.gastronomiavegana.org/" target="_blank">Gastronomía Vegana</a> y <a href="https://danzadefogones.com/" target="_blank">Danza de Fogones</a> donde encontrarás información y recetas para ayudarte a dar el paso</strong>.</p>
<!-- /wp:paragraph -->