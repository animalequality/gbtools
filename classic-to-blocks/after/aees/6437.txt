<!-- wp:image {"id":9449} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/circo_americano_leones.jpg" alt="" class="wp-image-9449"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La junta de distrito de Casablanca, en Zaragoza, aprobó el pasado viernes una declaración para que se considere <strong>distrito libre de circos con animales</strong>. Leticia Crespo, presidenta de la Junta de Distrito, afirmo que “es irracional que se permita el uso de animales en los <a href="http://www.igualdadanimal.org/entretenimiento/circos" target="_blank">circos</a>, máxime cuando somos conscientes del sufrimiento al que se les somete”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La Junta de Distrito es un órgano municipal que facilita la participación ciudadana directa en la toma de decisiones sobre los barrios. La propuesta de CHA (Chunta Aragonesista) contó con los votos a favor de PSOE, CHA, IU, el voto de la presidenta y la abstención del PP.<strong> No hubo ningún voto en contra</strong> ni ninguna organización vecinal que manifestara su oposición a esta medida.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para Leticia Crespo “es una <strong>cuestión de valores y de responsabilidad </strong>y eso se ha entendido perfectamente en el distrito de Casablanca. Esperamos que el Ayuntamiento de Zaragoza tome nota de esta decisión y sea respetuoso con la voluntad ciudadana”. La presidenta de la Junta apostó “por el nuevo circo que mantiene la calidad y la sorpresa del espectáculo circense sin necesidad de maltratar a ningún animal”.</p>
<!-- /wp:paragraph -->