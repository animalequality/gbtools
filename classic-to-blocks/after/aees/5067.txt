<!-- wp:paragraph -->
<p>Estocolmo- Según informa BBC, miles de conejos son quemados como combustible en una planta que genera calefacción para la ciudad sueca.

Cada año se matan en Estocolmo miles de conejos, a los que consideran una plaga. Son descendientes de conejos que en el pasado fueron liberados por personas que les tenían como mascotas. Ahora no tienen depredadores naturales y el estado contrata a cazadores para aniquilarlos.

Tommy Tuvunger, uno de estos cazadores, le dijo al diario alemán Spiegel que el año pasado mataron a unos 6.000 conejos y este año cerca de 3.000. "Son un gran problema", aseguró Tuvunger. "Una vez que los matamos, los guardamos en un congelador y cuando juntamos una cantidad suficiente, viene un contratista y se los lleva", agregó.

Los conejos congelados son trasladados a una planta calefactora en Karlskoga, donde se los incinera para generar calor, que luego se distribuye por las casas de la ciudad.

Comentario de Igualdad Animal: Los conejos de Estocolmo no tienen culpa de nada y como cualquiera de nosotros haría, intentan sobrevivir buscando comida, por ejemplo en los parques de la ciudad. Es completamente injusto que se les asesine por eso. Como suele ocurrir casi siempre en estos casos, las autoridades suecas no han hecho el más mínimo esfuerzo por buscar una solución que no implique la matanza de miles de conejos cada año.</p>
<!-- /wp:paragraph -->