<!-- wp:paragraph -->
<p>Se dice que la capacidad de utilizar herramientas es una coincidencia importante compartida entre humanos y primates no humanos - del mismo modo que lo es, por lo que parece, es <strong>el deseo de ser libres.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En las primeras horas de la mañana del pasado 10 de enero, un grupo de ocho monos capuchinos protagonizó una muy inteligente huida de un zoológico en Brasil. Para sorpresa de los empleados en la pequeña instalación, los atrevidos monos utilizaron, aparentemente, <strong>una herramienta de piedra para romper las cerraduras</strong> de su recinto antes de huir a la selva circundante.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De los ocho monos escapados de las instalaciones en el estado brasileño de Paraná, los funcionarios sólo han logrado capturar hasta el momento a cuatro. La coordinadora del Zoo, Gladis Dalamina, dijo a la cadena televisiva <a href="https://g1.globo.com/pr/parana/noticia/2012/01/macaco-que-fugiu-de-zoologico-do-pr-e-recapturado-dentro-de-restaurante.html" target="_blank">Globo</a> que mediante rastros de fruta y trampas jaula fue posible capturar a tres de los monos en las horas posteriores a la&nbsp; fuga. El cuarto fue hallado un día después tras irrumpir éste en un restaurante cercano. "Fue una sorpresa porque no estamos en la selva, como para que entre un mono en mi establecimiento. Fue muy divertido”,&nbsp; dijo el dueño del restaurante.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dalamina afirma que ella y su equipo continuarán tratando de localizar al resto de los fugados, pero que piensa que ya han sido atrapados los “cabecillas” de la huida: un mono adulto llamado Ceará, junto con su novia Amarela. Según las autoridades,<strong> ésta no es la primera vez que los monos han tratado de escapar del zoológico </strong>de esta pequeña comunidad, aunque el uso de una herramienta de piedra ha sido el método más sorprendente y eficaz hasta la fecha. "Su trabajo es correr. El nuestro es atraparlos", comenta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se cree que los monos capuchinos son los más inteligentes de los monos del Nuevo Mundo, exhibiendo una notable capacidad para utilizar herramientas de piedra. Los investigadores han observado a capuchinos salvajes utilizando piedras en procesos de recolección, que realizan hasta grandes distancias, para ayudarse a abrir nueces. Esta habilidad se transmite generacionalmente, ya que los monos más jóvenes aprenden observando a sus mayores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La aplicación de este uso de una misma herramienta para la nueva tarea de romper cerraduras indica un extraordinario uso de la lógica para resolver el dilema artificial de su cautiverio. Pero lo que, quizás, más fuertemente guió sus acciones fue un deseo fundamental: <strong>el anhelo de ser libres.</strong></p>
<!-- /wp:paragraph -->