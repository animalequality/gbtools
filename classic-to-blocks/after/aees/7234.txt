<!-- wp:image {"id":10231} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/03/koali.jpg" alt="" class="wp-image-10231"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-14083" src="/app/uploads/2015/03/koala.jpg" style="float:right; margin-left:5px; margin-right:5px; width:450px">Las autoridades del estado de Victoria, sacrificaron a unos 700 koalas en Cape Otway, debido a “problemas de superpoblación”, informaron medios locales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las autoridades australianas han indicado que sacrificaron cerca de 700 koalas en el Cape Otway, en el estado de Victoria, debido a “problemas de superpoblación”. Argumentaron que los animales estaban condenados a “morir de hambre” debido a la sobrepoblación que ha registrado la especie en la región. Las declaraciones han causado la indiganción de organizaciones por los derechos animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los 686 koalas fueron capturados y sedados antes de ser sacrificados entre 2013 y 2014 en la región de Cape Otway, y según indicó la ministra del Medio Ambiente del estado, Lisa Neville, se hizo de forma discreta "para evitar cualquier reacción violenta de los grupos ecologistas y la comunidad".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>“Estaba claro que había un problema de superpoblación y que los koalas estaban sufriendo en Cabo Otway porque no gozaban de buena salud y padecían de hambre porque no tenían con qué alimentarse”, explicó Neville.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A pesar de la medida, el gobierno ha reconocido que el problema de la superpoblación continúa, por lo que se encuentran buscando otras alternativas que no supongan matar a los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La ministra ha asegurado que no habrá más campañas de sacrificio y ha anunciado la implementación de una campaña para protegerlos con el asesoramiento de expertos.</p>
<!-- /wp:paragraph -->