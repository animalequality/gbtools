<!-- wp:paragraph -->
<p>Asturias- Según informa lne.es, ayer 10 de noviembre dió comienzo la “Tercera Exhibición de avicultura artística” en el recinto As Granxas, situado en el pueblo asturiano de Castropol. Más de tres mil animales no humanos llegaron al recinto encerrados en jaulas para ser evaluados por un jurado internacional que valorará sus características estéticas.

Escolares de los centros educativos de la zona visitaron la “exposición” acompañados de sus profesores. El elevado número de animales no humanos concentrados, ha situado a la “exhibición” de Castropol como la más importante de España.

Puede que haya humanos a los que les resulte entretenida la visita de este lugar. Sin embargo, no será así para las 3000 aves que van a permanecer mas de 70 horas encerradas en jaulas metálicas, privadas de su libertad, reducidas a simples objetos que “admirar”… sin olvidar que nunca pudieron ser dueñas de sus vidas y que nunca se les permitirá serlo.</p>
<!-- /wp:paragraph -->