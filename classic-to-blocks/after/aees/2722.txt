<!-- wp:paragraph -->
<p>Pekín - Un total de 5 000 lagartos gigantes, tortugas y pangolines fueron descubiertos en el interior de un barco abandonado a la deriva en el sur de China a punto de morir debido a las altas temperaturas, informó hoy el "Diario de Cantón".

Las autoridades del puerto de Yangjiang (Cantón, sur) desconocen el origen de la embarcación, ya que al subir abordo no encontraron a ningún tripulante o documentos, a excepción de un diario que se vende en Malasia y Singapur y que envolvía uno de los 21 brazos de oso que también fueron hallados en el interior de la nave.

Del total de animales hallados a bordo, 2 720 eran lagartos gigantes, 30 pangolines (mamífero del orden de los folidotos que vive en Asia y África) y el resto tortugas, todos ellos encerrados en unas 200 jaulas de madera.

Fueron los vecinos quienes denunciaron esta semana la presencia de un barco de madera antiguo de 25 metros de eslora a la deriva del que emanaba un fuerte olor en una zona cercana a la isla de Qingzhou.

Al subir a bordo la policía descubrió que el motor estaba roto, por lo que arrastraron el barco hasta la costa y trasladaron a los animales a un centro de protección en Cantón, capital provincial.

Según la investigación, los animales no llegaron hasta Cantón en esa embarcación, sino que fueron trasladados a ese barco en una zona cercana al puerto de Yangjiang.

Los platos cocinados a base de estos animales son característicos de la cocina cantonesa, ingredientes que también se utilizan para la medicina tradicional china.

Fuente - ElComercio</p>
<!-- /wp:paragraph -->