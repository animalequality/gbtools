<!-- wp:paragraph -->
<p><img alt="" class="wp-image-9641" src="/app/uploads/2012/07/cruelty_free_china.jpg" style="width: 220px; float: left; ">Algunas de las más grandes empresas de cosmética han sido obligadas a retirar de sus productos el logotipo “Cruelty free”, que indica que éstos no han sido testados sobre animales, debido a que han decidido retomar la <a href="http://www.igualdadanimal.org/experimentacion/" target="_blank">experimentación sobre animales </a>para poder vender sus productos en China.&nbsp;<br>
	<br>
	Las ventas de cosméticos en China aumentaron un 18% el año pasado, lo cual ha debido suponer un atractivo aliciente para algunas empresas de este sector. L'Occitane, Yves Rocher, Mary Kay, Avon, Estée Lauder&nbsp; y Caudalie no pueden utilizar el ya el logo oficial, conocido como “<em>salto de conejo</em>”, que indica que sus productos están libres pruebas sobre animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El director ejecutivo de Cruelty Free Internacional, Michelle Thew, afirma: «<em><strong>El estándar de calidad simbolizado por el logo del “salto de conejo” es el más riguroso sistema internacional de certificación de no testeo sobre animales del mundo</strong>. Cada empresa es auditada regularmente para garantizar que no se realiza experimentación con animales en ningún eslabón de la cadena de producción.»</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«<strong>Me decepciona que algunas empresas hayan caído en la tentación del mercado chino y permitan que los animales paguen el precio.</strong>»</em>, lamenta Thew. <em>«<strong>La presión que ejerzan los consumidores puede marcar la diferencia.</strong>»</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sin embargo, muchas otras empresas se niegan a ceder ante la “tentación” del mercado chino, manteniendo su compromiso con los animales y con sus consumidores.&nbsp;Puedes consultar la lista de empresas garantizadas con el sello Cruelty Free en la página <a href="http://www.GoCrueltyFree.org" target="_blank">www.GoCrueltyFree.org.</a><br>
	&nbsp;</p>
<!-- /wp:paragraph -->