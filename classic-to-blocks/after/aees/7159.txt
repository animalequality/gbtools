<!-- wp:image {"id":10167} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/09/agresion1.jpg" alt="" class="wp-image-10167"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":13692} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/09/Concentración-Antitaurina2.jpg" alt="" class="wp-image-13692"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El sábado 30 de agosto a las seis de la tarde se celebró por tercer año consecutivo una concentración legalizada por la abolición de la tauromaquia frente a la plaza de toros de San Sebastián de los Reyes, convocada por el colectivo Sanse Antitaurino.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Asistieron entorno a 100 personas de forma pacífica. Transcurrida una hora, coincidiendo con el paso de la Peña de los Olivares hacia el interior de la plaza, tres individuos comenzaron a insultar y amenazar a los presentes para provocarlos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En ese momento en la esquina de la calle Estafeta se produjo una agresión. <strong>Un individuo agredió a una activista antitaurina, cayendo ésta al suelo y dándose con una valla móvil. Una mujer la agarró de los pelos intentándola arrastrar por el asfalto, mientras un tercero la amenazaba de muerte.</strong> La intervención policial y de varios asistentes a la concentración evitó males mayores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La activista de 20 años de edad y 1.50 cm de estatura fue atendida por los servicios sanitarios presentes. Sufre un golpe y una herida en la rodilla, además de una señal superficial&nbsp; en la cara. Se ha interpuesto denuncia en la comisaría de Policía Nacional.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Colectivo SanSe Antitaurino condena la agresión y anima a las instituciones públicas, los partidos, movimientos y colectivos sociales y políticos de la zona a condenar este tipo de agresiones intolerantes que atentan contra derechos básicos y elementales en cualquier sociedad democrática, como es el derecho de manifestación y expresión.<br>
	&nbsp;</p>
<!-- /wp:paragraph -->