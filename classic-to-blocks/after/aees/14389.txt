<!-- wp:paragraph -->
<p>La fundación holandesa AAP ha previsto poder trasladar en el mes de mayo del próximo año los primeros 11 chimpancés que ocuparán las instalaciones que actualmente están en construcción a siete kilómetros de Villena, cerca de la sierra de Salinas.

Primadomus es el nombre del proyecto de AAP en España, que tiene la intención de ofrecer un hogar permanente a un grupo de primates sanos, ya socializados e imposibles de reintroducir en sus habitats naturales. Responsables de la fundación indican que «después de años de sufrimiento los primates encontrarán por fin la paz, la tranquilidad y la protección que se merecen. Después de hacer llevado una vida miserable, al fin tendrán la oportunidad de disfrutar de su retiro».

Las nuevas instalaciones donde se sitúa Primadomus ocupan 180 hectáreas de superficie, aunque una parte importante de esta área es una zona protegida, que así permanecerá pero dentro de los terrenos del centro.

La Fundación AAP ha informado de que cuenta con todas las licencias necesarias para hacer realidad este proyecto y, la que ellos consideran más importante, la Declaración de Interés Comunitario, les ha sido concedida por un plazo de 30 años, contando con un amplio apoyo del gobierno autonómico para desarrollar el proyecto.

Al depender casi exclusivamente de donaciones de particulares y empresas, la Fundación AAP ha puesto en marcha una original forma de captación de fondos para afrontar los gastos de traslado de los 11 primeros chimpacés.
Once personajes famosos en Holanda, cantantes, actores, periodistas, etcétera «apadrinan» cada uno de los chimpancés, formando un equipo que tiene que recorrer virtualmente la distancia entre Holanda y España, acumulando cada equipo puntos o kilómetros según los fondos recaudados o la publicidad realizada del proyecto.

La Fundación AAP ha reiterado que los animales que serán trasladados a Primadomus en Villena están completamente sanos, y que éste será el hogar definitivo para ellos, que consideran mucho más idóneo que el actual de Almere, en Holanda. Las ventajas de la instalación de Villena con respecto a la holandesa son, según las mismas fuentes, por el clima, la orografía del terreno y que serán nuevas instalaciones, que quieren tener terminadas en el mes de marzo.

Mientras en Holanda cada chimpancé dispone de unos cinco metros cuadrados, en Villena tendrán unos 640 metros cuadrados, ya que el alojamiento completo constará, cuando esté finalizado, de unos 7.000 metros cuadrados.
Primadomus no funcionará como un zoológico. La fundación holandesa quiso dejar claro que sólo se podrá visitar previa cita y bajo la supervisión de una persona cualificada. Por último, en AAP recuerdan que obsequiaron a Villena cuando decidieron instalarse en esta tierra con un terreno para crear un centro de educación de la naturaleza, y que están implicados también en la protección del pino piñonero, que crece en esta zona de la sierra de Salinas.

Fuente: INFORMACION.ES</p>
<!-- /wp:paragraph -->