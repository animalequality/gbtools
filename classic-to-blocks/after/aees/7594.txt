<!-- wp:image {"id":11169} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/10/14741866_10211282579208555_380498733_n.jpg" alt="" class="wp-image-11169"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>El movimiento de protección animal tiene tras de sí una larga historia. Sin embargo, si tuviésemos que poner un punto de partida a nuestro movimiento tal y como se le conoce hoy en día una buena elección sería la publicación del libro <strong>«Liberación Animal»</strong>, del autor y filósofo australiano Peter Singer.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Corría el año 1969 y en la universidad de Oxford un grupo de jóvenes filósofos se reunían habitualmente para hablar y debatir sobre un tema al que pocos prestaban atención, <strong>los derechos de los animales</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Todos tenían algo en común: su pasión por hacer ver a la sociedad el terrible problema existente <a href="https://igualdadanimal.org/noticia/2016/10/14/la-ganaderia-industrial-es-un-problema-global-que-nos-afecta-todos/" target="_blank">en nuestro trato a los animales</a>. Fueron publicados toda una variedad de artículos sobre el tema y vieron la luz varios libros, entre ellos, «Animals, Men and Morals» (Animales, humanos y ética), un libro colectivo escrito por varios autores.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11167} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/10/peter-singer-002.jpg" alt="" class="wp-image-11167"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="2"><font color="#808080"><p>Peter Singer</p></font>

<p>&nbsp;</p>

<p>Un joven filósofo australiano del entorno del grupo escribió un artículo sobre el libro para ayudar a que fuera publicado en Estados Unidos, lo tituló «Liberación Animal» (1973).</p>

<p>El joven y entusiasta autor acababa de adoptar una alimentación vegetariana junto a su mujer y algunos de los filósofos de la Universidad de Oxford. Su nombre era Peter Singer <strong>y estaba a punto de revolucionar el modo en el que veíamos a los animales destinados a alimentación</strong>.</p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p>«Todos tenían algo en común: su pasión por hacer ver a la sociedad el terrible problema existente en nuestro trato a los animales».</p></font>

<p>&nbsp;</p>

<p>Pronto empezó a recibir cartas de personas anónimas agradeciéndole que hubiese puesto palabras (y base filosófica) a lo que llevaban sintiendo desde hacía mucho. Entre esas cartas se encontraba la de un famoso editor de Nueva York, animándolo a desarrollar el artículo hasta convertirlo en un libro.</p>

<figure><img alt="" class="wp-image-11168" src="/app/uploads/2016/10/13501595_899584396830098_1310206559946022169_n.jpg" style="float:left; margin:10px; width:450px"></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
<font size="2"><font color="#808080"><p>Peter Singer con Igualdad Animal, probando la experiencia inmersiva de realidad virtual iAnimal</p></font>

<p>&nbsp;</p>

<p>Singer aceptó el reto, y dos años más tarde, en 1975 veía la luz <strong>«Liberación Animal: una nueva ética para nuestro trato a los animales»</strong>.</p>

<p>Liberación Animal conmocionó no solo a las personas del entorno de la protección animal, sino a la totalidad del mundo académico. Generó un intenso debate entorno al sufrimiento de los animales destinados a la <a href="http://descubrirlacomida.es/" target="_blank">alimentación</a>. Como ningún otro libro anterior, denunció la inmoralidad de las prácticas comunes en granjas y mataderos e inició un nuevo camino para nuevas generaciones de defensores de los animales.</p>

<p>A lo largo del tiempo, Singer se ha convertido en <strong>uno de los filósofos y autores vivos más influyentes</strong>. Su obra es estudiada en universidades de todo el mundo. Su visión de los problemas globales está centrada en aportar soluciones prácticas.</p>

<p>En la actualidad, el Profesor Peter Singer sigue activo y forma parte del movimiento conocido como <a href="https://www.project-syndicate.org/commentary/altruism-world-giving-index-by-peter-singer-2015-04/spanish" target="_blank">Altruismo Efectivo</a>. Singer está dedicado a incentivar las donaciones a aquellas problemáticas <strong>con más necesidad de recursos económicos</strong>.</p>

<p>Para él, el sufrimiento de los animales sigue siendo tan importante como siempre.</p>

<p><strong>Si quieres estar al tanto de cómo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación</strong>.</p></font></font></font>
<!-- /wp:html -->