<!-- wp:paragraph -->
<p>Según estimaciones de la ONG Renctas, unos <strong>38 millones de animales salvajes -el 80% de los cuales son pájaros- son capturados cada año de forma ilegal en la selva de Brasil, y casi el 90% muere durante su transporte.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sin embargo, para quienes trafican con la vida de estos animales, las muertes se ven ampliamente compensadas en términos económicos por la venta de un solo individuo. <strong>Ganando cerca de 2.000 millones de dólares por año</strong> en Brasil, este tráfico se convierte en el tercero más lucrativo tras el de la droga y las armas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>La Policía Federal incauta, de media, unos 250.000 animales por año</strong>. También el Instituto Brasileño de Medio Ambiente (Ibama) descubre otros 45.000 en controles, que fueron multiplicados en los últimos años en todo el país.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En el Centro de Selección de Animales Salvajes (Cetas) de Río de Janeiro, un organismo que depende del Ibama, el veterinario Daniel Neves se ocupa cada año de <strong>entre 7.000 y 8.000 animales recuperados, en general enfermos o hambrientos</strong>, víctimas de contrabandistas brasileños.<img alt="" class="wp-image-14041" src="/app/uploads/2012/05/guacamaya.jpg" style="width: 550px; float: right;"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los animales<em> "permanecen en cuarentena hasta que su salud mejora. El objetivo es liberarlos en la naturaleza, pero sólo lo logramos en un 20% a 30% de los casos. Es seguro que quien esté dispuesto a adoptar un papagayo ciego no le hará ningún daño</em>", argumenta el veterinario. <em>"Es un verdadero problema, porque ya no están aptos para desenvolverse solos en su hábitat"</em>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Comprar en el mercado negro un papagayo verde o un tucán capturado en su hábitat natural cuesta menos de 100 dólares, mientras que su precio es más de 10 veces superior en una "tienda legal". Esta diferencia de precios es lo que alienta el tráfico, según la Policía.</p>
<!-- /wp:paragraph -->