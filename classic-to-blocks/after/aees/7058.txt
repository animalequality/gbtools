<!-- wp:paragraph -->
<p><a href="https://igualdadanimal.org/" target="_blank"><strong>Igualdad&nbsp;Animal</strong></a>&nbsp;presenta hoy imágenes inéditas de la investigación que realizó en los puestos de venta de perros y gatos de los <strong>mercados de Tres Pájaros de Dali</strong>, situados en Nanhai, Foshan (región de Guangdong), así como en dos granjas de cría y engorde y un matadero de la zona.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta nueva investigación se enmarca dentro de la campaña <a href="https://www.sinvoz.org/" target="_blank">SinVoz</a>, iniciada en abril de este mismo año, con la que <strong>pretendemos poner fin al comercio de carne de perro y gato en China.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="435" scrolling="no" src="https://www.youtube.com/embed/V1GldnYCtyE" width="580"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A pesar de que los vigilantes de seguridad nos siguieron y trataron de impedir que presenciásemos la descarga de los animales vivos en los mercados, fuimos testigos de cómo <strong>miles de ellos llegaban en enormes camiones, agolpados dentro de pequeñas jaulas</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13926} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/09/descarga_perros_china.jpg" alt="" class="wp-image-13926" title="Descarga de perros en mercado de Dali (China)"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los perros, gatos y conejos son descargados de noche y las jaulas en las que están encerrados son lanzadas desde lo alto de los camiones. A muchos de estos animales <strong>se les rompen los huesos al caer en las jaulas</strong> contra el suelo. Algunas gatas embarazadas dan a luz en el trayecto hasta los mercados, que dura varios días y durante el cual no comen ni beben, &nbsp;y sus <strong>crías mueren al ser aplastadas</strong> en la brutal descarga de las jaulas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Visita a la granja de cachorros de Shandong</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><img alt="Cachorros en granja de cría Shandong (Jining)" class="wp-image-10054" src="/app/uploads/2013/09/jining_perros.jpg" style="width: 300px; float: left;" title="Cachorros en granja de cría Shandong (Jining)"></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Para conocer de cerca cómo funciona la industria de la carne de perro en China, visitamos la <a href="http://www.sdrgyzc.com/index.html" target="_blank">granja de cachorros de Shandong</a>&nbsp;(en <a href="http://maps.google.es/maps?q=jining+china&amp;um=1&amp;ie=UTF-8&amp;hq=&amp;hnear=0x35c46533b87353f9:0x2bd94b0f3d17511d,Jining,+Shandong,+China&amp;gl=es&amp;sa=X&amp;ei=F3AfUrGVDOiQ7Abtt4CoAQ&amp;ved=0CMYBELYD" target="_blank">Jining</a>) donde se crían perros para el consumo de su carne y su piel.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pudimos visitar dos naves de la granja de Shandong: una destinada a <strong>perros adultos</strong> y otra repleta de <strong>cachorros</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li>La primera nave contaba con varios cheniles, dentro de los cuales había unos 10 perros adultos de tamaño grande (alaska malamute, galgos y otras razas) destinados a <strong>reproducción</strong>.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>En la nave de cría pudimos contar unos <strong>150 cachorros</strong> de apenas semanas de vida, hacinados en pequeñas jaulas de alambre tiradas en el suelo. Estas jaulas tenían aproximadamente medio metro de anchura por 40 cm de altura, y llegamos a contar <strong>hasta 13 crías por cada jaula</strong>. Los perritos apenas podían moverse en su interior.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p>Según nos informaron en la granja, l<strong>as crías son vendidas a las 3 semanas de edad&nbsp;</strong>a otra empresa donde son sometidas a engorde hasta que alcanzan el peso adecuado. Una vez engordados, <strong>los cachorros son degollados en la propia granja o llevados al matadero</strong>. Cada cachorro es vendido por 200 yuanes (unos 25 euros).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>► Puedes ver la galería completa de fotografías del criadero&nbsp;<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157633386363979/" target="_blank">aquí</a>:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14034,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157633386363979/" target="_blank"><img src="/app/uploads/2013/09/granja_china_cachorros.jpg" alt="" class="wp-image-14034" title="Granja de cachorros de Shandong"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Visita a la granja de perros de Jiaxiang (嘉祥县)</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Al llegar a esta granja de engorde pudimos observar una única jaula con una decena de <strong>cachorros,&nbsp;</strong>y los&nbsp;cheniles donde mantenían a unos <strong>60 perros grandes. </strong>Los perros son engordados en Jiaxiang&nbsp;hasta que alcanzan una media de <strong>90 kg de peso </strong>y posteriormente son vendidos para el consumo de su carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>► Puedes ver la galería completa de fotografías de la granja&nbsp;<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157635260371290/" target="_blank">aquí</a>:&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14079,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157635260371290/" target="_blank"><img src="/app/uploads/2013/09/jiaxiang_perros.jpg" alt="" class="wp-image-14079" title="Granja de perros de Jiaxiang"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Matadero de perros de Zhanjiang</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Durante nuestra investigación visitamos un matadero localizado en un barrio de Zhanjiang, en una zona de viviendas. <strong>Aquí son matados alrededor de 15 perros al día</strong>. Los perros son encerrados en una habitación sin ventilación y totalmente a oscuras, sin agua ni comida, hasta el momento de su muerte. Son golpeados en la cabeza y acuchillados y posteriormente se les quita el pelo para enviar la carne limpia a los&nbsp;restaurantes locales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Puedes ver la galería de imágenes tomadas en el matadero de Zhanjiang <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157633369838751/" target="_blank">aquí</a>:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Advertencia: imágenes de extrema violencia</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14329,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157633369838751/" target="_blank"><img src="/app/uploads/2013/09/zanjiang_matadero_perros.jpg" alt="" class="wp-image-14329" title="Matadero de perros de Zhanjiang"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Mercado de Wuhan (Husband Seafood Market)</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>En Wuhan no es habitual el consumo de carne de perro. Sin embargo, descubrimos un mercado con un establecimiento lleno de jaulas, dentro de las cuales encontramos una gran cantidad de animales vivos, todos para carne: 15 <strong>perros </strong>distribuidos en varias jaulas, además de <strong>ciervos</strong>, <strong>caimanes</strong>, <strong>conejos, mapaches, erizos, un puercoespín, faisanes, burros y ocas</strong>. Observamos que esa misma mañana habían matado al menos a un perro, pues su piel yacía sobre una de las jaulas. En otra llegamos a ver incluso el cráneo de un perro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>► Puedes ver la galería completa de fotografías del mercado <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157633403302124/" target="_blank">aquí</a>:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14127,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157633403302124/" target="_blank"><img src="/app/uploads/2013/09/mercado_Wuhan.jpg" alt="" class="wp-image-14127" title="Mercado de Wuhan (Husband Seafood Market)"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Comercio de carne de gato en el mercado de FuXing (Taiping)</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>En el mercado de FuXing pudimos comprobar cómo los gatos también son víctimas del consumo de carne. En este mercado <strong>los gatos son directamente capturados de las calles</strong> y matados en los restaurantes mediante un golpe en la cabeza. Posteriormente son metidos en enormes ollas de agua hirviendo y servidos como parte del famoso plato "Dragón, tigre y fénix", que según la tradición "ayuda a fortalecer el cuerpo".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Además, en este mercado también eran vendidos para alimentación <strong>perros</strong>, <strong>mapaches </strong>y otros animales no domesticados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se estima que <strong>alrededor de 4 millones de gatos son matados al año en China por su carne.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>► Puedes ver la galería completa de fotografías del mercado de FuXing <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157635288961614/with/9622137578/" target="_blank">aquí</a>:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13704,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157635288961614/with/9622137578/" target="_blank"><img src="/app/uploads/2013/09/FuXing_taipin_gatos.jpg" alt="" class="wp-image-13704" title="Comercio de carne de gato en FuXing (Taiping)"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p><span style="font-size: 12px;">Visita<strong>&nbsp;</strong></span><strong><a href="https://www.sinvoz.org/" style="font-size: 12px;" target="_blank">www.SinVoz.org</a></strong><span style="font-size: 12px;">&nbsp;y descubre todos los detalles de la investigación en mataderos de perros en China.&nbsp;</span><strong><span style="font-size: 12px;">¡Ayúdanos a parar esta masacre!</span></strong></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><a href="www.sinvoz.org" target="_blank"><img alt="" class="wp-image-13809" src="/app/uploads/2013/09/banner_sinvoz.jpg" style="width: 580px; "></a></h4>
<!-- /wp:heading -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph -->
<p><em>«Vi que algunos perros corrían libres por los callejones. Otros estaban encadenados. Me dijeron que estos perros no eran para consumo humano y eran “mascotas” de la gente. ¿Pero cuál era la diferencia entre estos individuos y los perros en las jaulas que iban a ser asesinados pronto? ¿Con qué criterio deciden los humanos quién debe ser enjaulado y en algún momento comido, y quién debe ser perdonado y tenido como mascota?»&nbsp;</em><a href="http://www.sinvoz.org/investigators-diary-2/" target="_blank"><em>Diario del investigador</em></a></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">*Petición finalizada. Gracias a todos los que habéis colaborado</h4>
<!-- /wp:heading -->