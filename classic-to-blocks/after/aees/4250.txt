<!-- wp:paragraph -->
<p>CENTRAL ISLIP, Nueva York, EE.UU. (AP) - Un juez del condado de Suffolk emitió una orden de protección para Circles, un pato que había sido atacado a tiros por un hombre, prohibiendo a este acercarse a Circles y a sus responsables.

Según la acusación, Mathews se infiltró en el jardín de una casa junto con otras personas el 17 de marzo y disparó al pato varias veces, a consecuencia de lo cual el pato sufrió heridas. Posteriormente Matthews fue detenido.

Fuente: Univision.com

NOTA: Aunque esta noticia puede dar a entender a primera vista que el juez ha actuado así pensando en los intereses de Circles, debemos tener en cuenta que los animales tienen actualmente el estatus legal de propiedades. En este caso, Circles es considerado una propiedad de sus responsables, y tanto las leyes como las decisiones judiciales van a adoptar medidas para defender a los propietarios frente a los ataques a sus propiedades. El objetivo de proteger el derecho de propiedad explica esta decisión judicial. Para que la situación cambie y los intereses de los animales sean respetados por sí mismos (y no por el deseo de proteger a los propietarios), es necesario que la sociedad tome consciencia sobre la injusticia de que los animales sean considerados recursos.</p>
<!-- /wp:paragraph -->