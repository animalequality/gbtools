<!-- wp:paragraph -->
<p>Si hablamos de maltrato animal, no hay animales que padezcan mayores sufrimientos que los de granja. Para abastecer de carne, leche y huevos a la humanidad, <strong>la ganadería industrial</strong> convierte la vida de los animales en verdaderas pesadillas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Solidarizarnos con los animales mostrando compasión hacia su sufrimiento se ha convertido en <strong>uno de los retos de nuestros tiempos</strong>. Las encuestas muestran de manera internacional como millones de personas, con especial impacto en las nuevas generaciones, <strong>están cambiando sus hábitos de consumo</strong> para favorecer y ayudar a las víctimas de la ganadería.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Aquí te proponemos 7 formas de ayudar a nuestros compañeros de planeta, para los que <strong>su vida es tan importante como para nosotros la nuestra</strong>. ¡Esperamos que te decidas a unirte! ¡Los animales te necesitan de su lado!<strong>1. Sustituyendo la carne de pollo en tu alimentación por alternativas vegetales</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El pollo es el animal más consumido a nivel mundial. La industria de la carne convierte la vida de estos frágiles animales en una pesadilla de maltrato animal. Viven hacinados en enormes naves industriales en las que nunca ven la luz del sol. Nos necesitan desesperadamente.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13793} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaanimales1.jpg" alt="" class="wp-image-13793"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>2. Sustituyendo la carne de ternera en tu alimentación por alternativas vegetales</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La industria láctea, además de producir una enorme cantidad de sufrimiento a las vacas, guarda otro oscuro secreto para el público general: todos los bebés que nacen de las vacas son separados de sus madres sólo horas después de nacer. Esto sucede durante toda la vida de las vacas. La separación es desgarradora. Por si fuera poco, la mayoría de bebés macho serán destinados a la inhumana industria de la carne. <strong>Estos bebés son la carne de ternera que veremos en los supermercados</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13800} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaranimales2.jpg" alt="" class="wp-image-13800"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>3. Sustituyendo la carne de cerdo en tu alimentación por alternativas vegetales</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Sabías que los estudios científicos han demostrado que un cerdo es tan inteligente como un niño o niña de 3 años? De hecho estos animales <strong>compiten en inteligencia con delfines, chimpancés y elefantes</strong>. Son seres muy sensibles que, además, tras miles de años de relación con los humanos, desarrollan vínculos afectivos con nosotros tan rápido como nuestros perros y gatos.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13801} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaranimales3.jpg" alt="" class="wp-image-13801"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>4. Sustituyendo la carne en tu alimentación</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porque claro, una vez que te has dado cuenta de que los animales son nuestros amigos si se lo permitimos, resulta difícil dejar de comer sólo a unos. Todos rebosan ganas de vivir. Poco a poco, las personas que están contra el maltrato animal <strong>van haciendo la conexión entre una forma de maltrato y otra</strong>. Alimentarse sin carne consiste esencialmente en llenar nuestra cesta de la compra con unos productos en vez de otros. En la website <a href="http://www.gastronomiavegana/">Gastronomía Vegana</a> tienes multitud de información para dar los primeros pasos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¡Ánimo!</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13802} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaranimales4.jpg" alt="" class="wp-image-13802"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>5. Siendo un activo colaborador de organizaciones de protección animal en redes sociales</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Gracias a las personas que firman peticiones, comparten post con mensajes de protección animal y difunden los proyectos y campañas, la defensa de los animales llega al resto de la sociedad. <strong>A día de hoy las redes sociales tienen tanta importancia en alcanzar a la sociedad como los medios de comunicación</strong>. ¡Cada vez que compartes cuenta!</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13803} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaranimales5.jpg" alt="" class="wp-image-13803"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>6. Donando a organizaciones de protección animal cuyo trabajo se centre en animales de granja</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Recuerda: son estos animales los que en mayor número viven en condiciones extremas y mueren en los horribles mataderos. La ganadería industrial hace que más y más animales lleguen a existir con el sólo propósito de convertirse en bandejas de carne. <strong>Ningún animal merece una existencia tan llena de sufrimiento</strong>. Recuerda que Igualdad Animal es una organización centrada en defender a estos animales. Si quieres donar, <a href="http://www.igualdadanimal.org/colabora/donativos.php">puedes hacerlo aquí</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13804} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaranimales6.jpg" alt="" class="wp-image-13804"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>7. Si eres vegetariano / vegano</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Siendo amable con quien no lo sea. No juzgues a los demás por su manera de alimentarse. Recuerda que muy pocos han sido vegetarianos toda su vida. Muchos estuvimos en el punto en el que ahora están otras personas que no son vegetarianas. <strong>Tu manera de relacionarte y comunicarte debe animar a los demás a interesarse por tus valores</strong>. No seas arrogante ni uses discursos agresivos. Eso es lo último que necesitan los animales. No pierdas el foco: eres un embajador de los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10401} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/ayudaranimales7.jpg" alt="" class="wp-image-10401"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->