<!-- wp:image {"id":9542} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/05/jirafa-lodz-polonia.jpg" alt="" class="wp-image-9542"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Dos jirafas del zoológico de Lodz, en el centro de Polonia, murieron este fin de semana tras un ataque de estrés provocado por la visita nocturna de unos vándalos, según informó la dirección del lugar.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los individuos entraron en el zoológico el sábado por la noche y rompieron bancos, carteles y estatuas, cuyos pedazos lanzaron hacia los animales allí recluidos. Una jirafa murió pocas horas después del incidente. La segunda fue encontrada muerta el lunes por la mañana.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«La autopsia de la primera jirafa, una hembra de 3 años, reveló que<strong> se le fisuró una válvula cardíaca, evidencia de que padeció un gran estrés</strong>. La segunda jirafa, una hembra de seis años, ya estaba enferma. Es posible que el estrés le haya sido fatal»</em>, declaró Wlodzimierz Stanislawski, el director del zoólogico.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«<strong>Las jirafas son muy temerosas. Frente a situación de estrés, huyen siempre</strong>. Reaccionan de la misma manera ante ruidos inusuales»</em>, subrayó Stanislawski.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los guardianes del lugar no se dieron cuenta de la visita y, dado que el zoológico de Lodz no posee cámaras de seguridad, los delincuentes no pudieron ser identificados.</p>
<!-- /wp:paragraph -->