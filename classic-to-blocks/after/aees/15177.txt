<!-- wp:image {"id":10658} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/05/notiviernes.jpg" alt="" class="wp-image-10658"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10659" src="/app/uploads/2016/05/noticiaviernes20mayo.jpg" style="float:left; margin:10px; width:400px">Una nueva encuesta llevada a cabo en el Reino Unido ha revelado que el número de personas alimentándose de forma 100% vegetariana <strong>ha subido un 360%</strong> en la última década, según informa el diario The Telegraph.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La encuesta, se realizó a más de 10.000 personas, <strong>siendo la mayor hasta la fecha</strong> en Reino Unido para cuantificar el número de personas alimentándose con <a href="https://igualdadanimal.org/noticia/2015/12/21/vegetarianos-veganos-ya-pero-quienes-son-y-que-comen/">alternativas a carne, lácteos y huevos</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La <a href="http://www.igualdadanimal.org/noticias/7477/una-dosis-de-motivacion-para-comer-mas-platos-sin-carne">motivación principal</a> en esta <strong>tendencia imparable</strong> parece ser los beneficios para la salud. Según los expertos, la alimentación con vegetales y alternativas a la carne se asocia a menores niveles de colesterol y menor incidencia de problemas cardiacos y cáncer. Los problemas medioambientales globales y el maltrato asociado a la industria ganadera son también factores que están generando el cambio de modelo alimentario.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La actitud de los británicos hacia la <strong>alimentación respetuosa con los animales y el medioambiente</strong> está cambiando rápidamente. Un creciente número de cadenas de supermercados ya tienen sus propias líneas de productos alternativos a la carne. A su vez, más y más restaurantes y cafeterías incluyen opciones veganas en su oferta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según el magazine Vegan Life, esto se debe, en parte, al <strong>positivo retrato</strong> que desde los medios de comunicación se está haciendo de las personas que consumen productos veganos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para este magazine la tendencia <a href="https://igualdadanimal.org/noticia/2016/05/04/el-desarrollo-de-alternativas-vegetales-la-carne-tendencia-mundial-numero-uno-para-el/">seguirá creciendo</a> exponencialmente según los consumidores conozcan la realidad de la industria ganadera y la implicación para la salud del consumo de carne y lácteos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
&nbsp;</p>
<!-- /wp:paragraph -->