<!-- wp:image {"id":9483} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/viviseccion_oviedo.jpg" alt="" class="wp-image-9483"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La Universidad de Oviedo estrenará en pocos días un <strong>nuevo laboratorio de pruebas de diagnóstico por imagen sobre animales</strong>, como escáneres y resonancias. El equipamiento ha costado 1 millón de euros, procedente de los fondos “FEDER”, y estará ubicado en el bioterio del campus del Cristo, próximo al Hospital Central de Asturias.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los principales equipos adquiridos son: una resonancia magnética, S.P.E.T. (Single Photom Emission Tomography), PET CT (Positrom Emission Tomography Computed Tomography) y un Micro CT, todas ellas máquinas de diagnóstico por imagen adaptadas al tamaño y tejidos de los animales utilizados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Actualmente, alrededor de medio centenar de grupos de investigación de la Universidad de Oviedo utilizan las instalaciones del Bioterio para desarrollar sus investigaciones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En el siguiente vídeo puedes ver cómo son utilizados los animales en esta área de diagnóstico:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/7wmKdZExBQE" width="560"></iframe></p>
<!-- /wp:paragraph -->