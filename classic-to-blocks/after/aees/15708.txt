<!-- wp:html -->
<div class="center">&nbsp;</div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
El chef británico, y también celebridad, Jamie Oliver dice que ama a los veganos y por eso sus contenidos incluyen muchísimas recetas de platos libres de carne.

Estos comentarios los hizo recientemente durante un programa que compartió con el atleta vegano de alto rendimiento Timothy Shieff, y que se centró exclusivamente en la alimentación vegana. El popular chef declaró que, a pesar de que muchas de las cosas <span style="color: #000000;">que hace no siempre gustan a algunas personas que siguen una alimentación vegetariana o vegana, él los ama y por eso entre el 60 y el 70% de su producción es veggie.</span>

Más allá de este afecto, Oliver está, sobre todo, muy consciente de la importancia de cambiar a una alimentación basada en vegetales. «Estamos tratando de llevar al público global por un viaje de transición, para que coman más verduras, más vegetales, más hierbas y frutas», declaró, y agregó: «creo que el mensaje para mí es que deberíamos comer más vegetales y menos o nada de carne».
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/pHpyYeHDSiM" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
Ya en 2015, Oliver había declarado que el futuro se sostendrá en una alimentación basada en proteínas vegetales. Y, a pesar de que el famoso chef aún cocina, come y vende carne en sus restaurantes, este año ha sido promotor y participante en múltiples iniciativas para promover la importancia de comer más vegetales y menos carne.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large; color: #000000;">¿Quieres recibir las mejores noticias de actualidad sobre los animales? </span></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large; color: #000000;">¡Suscríbete gratuitamente a <a href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958"><span style="color: #0000ff;">nuestro e-boletín</span></a>!</span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
En este sentido, en mayo de este año lanzó su Desafío Vegetariano de 5 días y declaró que su intención es inspirar a todos por igual, vegetarianos, veganos y amantes de la carne a conocer más y abrazar el mundo de los vegetales. «Saber cómo hacer que las verduras y las frutas sean las estrellas del espectáculo es una manera emocionante de cocinar» afirmó.

Y, consciente del impacto de nuestras decisiones añadió: «Además, comer más vegetales siempre te pondrá en un buen lugar. Es bueno para tu bolsillo, es bueno para el planeta y puede ser realmente bueno para tu salud».</p>
<!-- /wp:paragraph -->