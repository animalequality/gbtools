<!-- wp:paragraph -->
<p>Dos activistas de Igualdad Animal (Javier Moreno y Jose Valle) serán juzgados el próximo Lunes 13 de Julio a las 12:20 h. en el juzgado de instrucción nº 10 -sito en la Plaza de Castilla número 1 de Madrid- bajo el cargo de desórden público por haber saltado a la pasarela Cibeles el pasado 23 de Febrero durante un desfile con pieles con el fin de protestar contra la utilización de animales como vestimenta.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/3306450904/"><img src="http://farm4.static.flickr.com/3545/3306450904_a7824f1c85_o.jpg" alt="23/02/2009 - Madrid | Protesta contra la utilización de animales como vestimenta - Protest against the use of animals as clothing"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Los activistas de Igualdad Animal que saltaron en dos momentos diferentes con éxito burlando la seguridad del desfile, mostraron sendos carteles que leían "Piel es asesinato - Igualdad Animal" ante las cámaras de los periodistas presentes tal y como se puede ver en el <a href="https://www.youtube.com/watch?v=wqpRme-Zm7k">vídeo</a> y <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157614378324844/show">fotografías</a> de la acción.

Esta nueva acción exitosa de desobediencia civil de Igualdad Animal cumplió su objetivo de mostrar los carteles que portábamos ante las cámaras de televisión, de modo que fue posteriormente emitida en prácticamente todas las televisiones nacionales de España e incluso reflejándose en medios internacionales.

Se da la circunstancia que justo una semana antes, otros cuatro activistas habíamos irrumpido en la feria peletera IberPiel que se celebraba en ese mismo recinto ferial encadenándonos por el cuello a un stand peletero a la vez que mostrábamos los cadáveres despellejados de cuatro visones obtenidos de una granja peletera española (<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157613807758438/show">enlace a fotografías</a>)

Queremos aprovechar este juicio para defender a los animales víctimas de la moda y animar a un cambio social que lleve al fin de la utilización de animales como vestimenta. Por ello te pedimos ahora a ti que acudas al juicio en muestra de solidaridad con nuestros activistas y con los animales a quienes defendemos y que son masacrados constantemente para que otros se vistan con partes de sus cuerpos.

Si deseas apoyar a Igualdad Animal y el trabajo que estamos haciendo, participa como <a href="mailto:activismo@igualdadanimal.org">activista</a>, <a href="http://www.igualdadanimal.org/socios">hazte socio-colaborador</a> y realiza una <a href="https://igualdadanimal.org/dona/">donación</a> para poder financiar las próximas acciones.</p>
<!-- /wp:paragraph -->