<!-- wp:paragraph -->
<p>MIRANDA (VENEZUELA).- El Ministerio de Agricultura y Tierras (MAT) supervisó la llegada de 420 de los 650 bovinos tipo girolando que formarán parte del plantel de un centro genético animal en el estado Anzoátegui.

La inspección de la unidad productiva, ubicada en la vía Bare, municipio Miranda, a unos 30 minutos de El Tigre, estuvo a cargo de Yván Gil. vice ministro de Circuitos Agroproductivos del MAT.

Durante el recorrido por la antigua Finca El Paraíso, Gil señaló que la importación de los bovinos deriva del convenio entre el Instituto Nacional de Investigaciones Agrícolas (Inia) y la Empresa Brasileña de Pesquisas Agrícolas (Embrapa).

Beneficios genéticos
El plan que alcanza una inversión de 3.5 millones de dólares, permitirá consolidar un mejoramiento genético de los rebaños en el sur del estado y parte del oriente del país.

Mientras que la negociación de las bienhechurías de la finca, que, según dijo, estaba ociosa, costó 6 millones de bolívares fuertes.

El también presidente del Inia detallo que en el centro de genética se procesarán pajuelas de semen (dosis que se usa en la inseminación artificial), embriones congelados y animales propiamente dicho.

“Esta es una de las mejores importaciones de ganado de calidad que hemos hecho en los últimos años”, acotó .

El vocero ministerial refirió que ayer comenzó la cuarentena (aislamiento de animales para limitar el riesgo de una enfermedad), y esperan, en seis meses, presentar los primeros resultados con las diferentes técnicas genéticas.

Entre ellas resaltan 50 mil pajuelas de semen que servirán para producir la inseminación de vacas nacionales de menor calidad genética.

Además de los beneficios reproductivos, el Inia aspira a impulsar la fabricación de 5 mil kilos de queso diarios.

En el predio pecuario de 1.392 hectáreas, Gil consideró que luego puedan extender la incorporación de otras 600 reses de ganado en la parte productiva

Estudian el apostamiento de ganado caprino y ovino.

El plan genético animal fue visto con sorpresa por el presidente de la Asociación de Ganaderos de la zona sur (Agasa), Mauro Barrios, al alegar que desconocían sobre su aplicación en esta zona.

Aunque no está en contra de la iniciativa, al dirigente gremial le inquieta que el Gobierno nacional no tome en consideración la opinión ni participación de los productores de la zona sur en actividades que son propias de su quehacer,

“No podemos avalar ningún proyecto sino tenemos conocimiento, siempre sabemos de los planes del gobierno por medio de la Gaceta Oficial”.

Barrios recordó que ya existe una experiencia negativa con la importación del ganado girolando. “Esos animales terminaron flacos, unos robados en abigeato y otros en los estómagos de los campesinos”.

Fuente: eltiempo.com.ve</p>
<!-- /wp:paragraph -->