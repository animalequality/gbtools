<!-- wp:image {"id":10426} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/mayonesasinhuevo.jpg" alt="" class="wp-image-10426"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El gigante de la mayonesa Hellmann’s, cuyos productos se comercializan en todo el mundo, anuncia que lanza una mayonesa sin huevo hecha completamente con productos vegetales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La mayonesa sin huevo llegará a los supermercados estadounidenses a mediados de este mes. <strong>Su lanzamiento afianza aún más el imparable crecimiento de los productos hechos a base de vegetales</strong>. La carne, los huevos y la leche están dando paso a una nueva gama de productos alternativos que <a href="https://igualdadanimal.org/noticia/2015/10/15/los-sustitutos-de-la-carne-se-estan-generalizando-en-alemania/">cada vez son más demandados por los consumidores de todo el mundo</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cada vez surgen más empresas innovadoras que apuestan por <strong>un cambio de modelo alimenticio</strong> siguiendo criterios de sostenibilidad, salud y bienestar animal. Empresas como <a href="https://www.ju.st/"><em>Hampton Creek</em></a>, cuya mayonesa sin huevo Just Mayo es la pionera y predecesora de la nueva mayonesa sin huevo de Hellmann’s.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se da la curiosa circunstancia de que la empresa matriz de <em>Hellmann’s</em>, la multinacional <em>Unilever</em>, demandó en 2014 a <em>Hampton Creek</em>. La acusación de Unilever se basaba en decir que Hampton Creek engañaba a los consumidores al llamar «<em>mayonesa</em>» a un producto que no contenía huevos. <strong>Tras un agria polémica y mucha publicidad negativa hacia la multinacional, <em>Unilever</em> acabó retirando la demanda</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ahora parece que el gigante de la alimentación ha pensado que si no puedes con tu enemigo lo mejor es unirte a él.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-14131" src="/app/uploads/2016/01/mitos3.jpg" style="float:right; margin-left:10px; margin-right:10px; width:300px">Cuando se le preguntó a la directora de marketing de Hellmann’s si <strong>su nueva mayonesa sin huevo</strong> era la respuesta a la de Hampton Creek, respondió que, «<em>Hellmann’s se está limitando a satisfacer los gustos de sus consumidores</em>».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lo cierto es que los gigantes de la alimentación como Unilever <a href="https://igualdadanimal.org/noticia/2015/11/03/carrefour-lanza-una-linea-de-sustitutos-de-la-carne-en-francia/">están dando pasos hacia productos alternativos a la carne, leche y huevos</a> al ver el éxito de innovadoras empresas como Hampton Creek.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los tiempos cambian, y los consumidores demandan productos cuyos estándares en materia de <strong>bienestar animal</strong> sigan una sencilla máxima: «<em>si sabe delicioso y ningún animal ha sufrido al producirlo, lo quiero en mi nevera</em>».</p>
<!-- /wp:paragraph -->