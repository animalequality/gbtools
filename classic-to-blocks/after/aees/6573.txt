<!-- wp:paragraph -->
<p><img alt="" class="wp-image-9582" src="/app/uploads/2012/06/circo_leon.jpg" style="width: 220px; float: left; ">A través de una resolución ampliamente esperada durante años por asociaciones y organizaciones protectoras de animales, la Secretaría del Ambiente (SEAM) <strong>prohíbe </strong>desde este martes, 5 de junio, el establecimiento de <strong>espectáculos circenses</strong> con carácter temporario o permanente, que incluyan <strong>animales silvestres en cautiverio</strong>, ya sea con fines comerciales benéficos o didácticos, en todo el territorio paraguayo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El titular de la SEAM, el Ministro Oscar Rivas, al entregar la<a href="http://www.seam.gov.py/images/stories/seam/documentos/escaneado.pdf" target="_blank"> Resolución Nº2002/12</a> destacó la importancia de la misma:<br>
	<br>
	<em>«Con esta resolución se genera una prohibición del maltrato que significa utilizar animales silvestres, que son nuestra competencia, en espectáculos de divertimento público. <strong>Ni siquiera en términos educativos deberíamos permitir tales cosas</strong>. Por eso nos pareció importante que esto rija desde un día 5 de junio, Día Mundial del Medio Ambiente, porque el humano es un ser simbólico, dirigido por significados y significantes y eso refuerza el trabajo.»</em><br>
	<br>
	El Ministro Rivas también destacó la importancia de esta firma en Paraguay, ya que es un gran avance en la concienciación de la ciudadanía en cuanto a las reivindicaciones de derechos de los animales. <em>«Es fantástico poder vivir esta parte de la historia nacional, en la cual los paraguayos y paraguayas podemos encontrarnos en una multicolor convocatoria para construir este nuevo país que nos merecemos, que siempre nos merecimos y que nos habían postergado»</em>, expresó.</p>
<!-- /wp:paragraph -->