<!-- wp:paragraph -->
<p>La feria de Sant Tomàs llenó las calles de Sineu de visitantes ávidos de carne de <strong>matanzas</strong>.

La feria de Sineu huele a carne. Entre sobrasadas, lechonas, panadas y guisos, en ningún rincón del centro del pueblo emanaba otro olor. Y todo con un denominador común: el cerdo.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div>Como es tradición, las matanzas fueron protagonistas en la última gran feria del año. Con los boleros sonando de fondo, los numerosos visitantes recorrieron paradas de embutidos o repostería que despertaban el apetito.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>También hubo una demostración de <strong>cómo realizar una matanza</strong>. Con esa carne, luego se elaboraron diferentes platos típicos de la gastronomía mallorquina.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>Los animales también estuvieron muy presentes, ya que, por ejemplo, se pudieron <strong>comprar lechonas vivas</strong> para la cena de Navidad. Las sobradas y las lechonas fueron lo más demandado.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>Los más jóvenes aprovecharon para <strong>fotografiarse </strong>con los cerdos, que bramaron mientras los aguantaron en brazos.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>El evento más esperado de la jornada festiva fue el concurso de cerdos gordos, organizado por el Ayuntamiento de Sineu. Nada de concursos de belleza, lo que importaba era lo que marcara la balanza.</div>
<!-- /wp:html -->