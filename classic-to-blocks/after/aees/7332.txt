<!-- wp:image {"id":10336} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/11/matanzaperros.jpg" alt="" class="wp-image-10336"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Los investigadores de Igualdad Animal, con la ayuda de activistas locales, se infiltraron en la industria china de piel de perro y gato en distintas regiones del país y documentaron durante semanas las condiciones en las que se encuentran los animales.

Los principales hallazgos fueron los siguientes:

En una granja de perros de Jining, en Shandong, fotografiaron a animales que se encontraban encerrados en pequeñas jaulas, con cercas de metal oxidado y salientes punzantes. Los perros vivían sobre un suelo sucio y desnivelado de cemento o bien de madera, con poco más que un puñado de paja donde poder tumbarse. Algunos de los animales presentaban infecciones y heridas.

En el Mercado Dali del distrito de Nanhai, en Foshan, también en la provincia de Guangdong, grabaron como descargaban de los camiones, sin ningún cuidado a los perros y a los gatos, que se retorcían dentro de las jaulas. No se proporciona ninguna atención a los animales que sufren o que se rompen los huesos, solo importa su carne y su piel. A la mayoría de estos animales se los roban a familias y en este mercado y otros similares se venden a mataderos y a restaurantes. &nbsp;

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/sMz1UjBGFu0" width="885px" height="498px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
&nbsp;

En dos instalaciones de Jiaxing, en la provincia de Shandong, se documentó la matanza y despellejamiento de los perros, donde los animales sufren una muerte brutal y llena de agonía. Se golpea a los perros y gatos en la cabeza para dejarlos inconscientes y entonces les rajan la garganta. Muchos de ellos recuperan el sentido mientras se desangran hasta que mueren.

Las pieles de perro y gato se utilizan para fabricar bolsos, juguetes y prendas de vestir. En el mercado de Guangzhou, en la provincia de Guangdong, al sur de China, los investigadores de Igualdad Animal descubrieron que los comerciantes venden alfombras fabricadas con piel de perro y gato —algunas están hechas con nueve o más animales— y las venden en Pekín donde también se vende ropa infantil fabricada con piel de gato.

Esta es la cuarta investigación que lleva a cabo Igualdad Animal con la ayuda de activistas chinos en la industria de carne y piel de perros y gatos en China para intentar acabar con este negocio tan cruel. Igualdad Animal consiguió <a href="https://elpais.com/sociedad/2013/10/12/actualidad/1381604379_482010.html">el cierre de 33 de estos mercados y un matadero</a>. Además, los investigadores <a href="https://youtu.be/4zWfse-aP0k">rescataron a Vita</a> —una perra a la que se encontraron en un matadero, temblando y suplicando que la ayudaran—. Al cabo de poco dio a luz a ocho cachorros sanos y ella misma goza de muy buena salud. Todos juegan ya a salvo, lejos del horror del matadero.

Este trabajo que forma parte de la campaña internacional de Igualdad Animal “<a href="https://www.sinvoz.org/">Sin Voz</a>”, con la que ya ha conseguido reunir más de 500.000 firmas contra este cruel comercio. &nbsp;

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/albums/72157656180774172"><img src="https://farm1.staticflickr.com/337/19285733884_dd339c7937_c.jpg" alt="Comercio de pieles de perros en China"/></a></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="media_embed">
<script async="" charset="utf-8" height="534px" src="https://embedr.flickr.com/assets/client-code.js" width="800px"></script></div>
<!-- /wp:html -->