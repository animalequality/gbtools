<!-- wp:image {"id":9494} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/eap_photo_selection.jpg" alt="" class="wp-image-9494"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><a href="http://britishporkindustry.co.uk/es/east-anglian-pig.php">Una nueva investigación encubierta de Igualdad Animal</a> pone al descubierto a la industria cárnica británica al revelar una impactante brutalidad en las granjas de la compañía East Anglian Pig Company (EAP), tercera productora de carne de cerdo del Reino Unido y proveedora de los principales supermercados y marcas del país.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El periódico The Sunday Mirror ha dedicado esta mañana una página completa a la investigación.&nbsp;<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Un investigador infiltrado trabajó en dos granjas diferentes durante un total de 29 días:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
<strong>Unidad de Destete de Little Thorns:</strong><br>
Esta es la unidad de destete, con aproximadamente entre 8.000 y 10.000 cerdos. En Little Thorns, los cerdos son criados en seis cobertizos exteriores durante un periodo de entre 6 y 8 semanas antes de ser enviados a las granjas de engorde.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Didlington, The Piggery:</strong><br>
Esta es la unidad de cría con 3.000 cerdos y más de 700 cerdas. Los cerdos son criados en el exterior y engordados en el interior de las naves. Más de 300 cerdos son enviados al matadero cada semana desde esta granja.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Igualdad Animal prueba una vez más que, independientemente del sello de bienestar animal que tenga la granja sigue existiendo dolor, sufrimiento y explotación a gran escala.<br>
<br>
<strong><span style="color:#800000;">Advertencia: El vídeo que va a ver contiene escenas de violencia hacia los animales.</span></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="315px" width="560px"><iframe allowfullscreen="" frameborder="0" height="315px" src="https://www.youtube.com/embed/TUxg93ghBVA" width="560px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La <a href="http://britishporkindustry.co.uk/es/east-anglian-pig.php">investigación de Igualdad Animal</a> ha recibido el apoyo de expertos internacionales en comportamiento animal y veterinaria.<br>
<br>
<strong>Para leer las declaraciones de los expertos</strong>, <a href="http://britishporkindustry.co.uk/es/east-anglian-pig-expertos.php">pulsa aquí</a>.<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="http://britishporkindustry.co.uk/es/east-anglian-pig-diario-del-investigador.php">Pulsa aquí para acceder al Diario del investigador</a><br>
<br>
&nbsp;</h4>
<!-- /wp:heading --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p><strong>HALLAZGOS DE LA INVESTIGACIÓN:</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://animalequality.net/sites/default/files/imagesimages/eap_photo_selection.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Lechones enfermos que eran matados a golpes. Estos animales eran cogidos por sus extremidades traseras y brutalmente golpeados con la cabeza contra el suelo repetidas veces. En un caso en el que el lechón no había muerto tras el traumatismo craneal, un trabajador trató de asfixiar al animal agarrándole el hocico con su mano y pisándole la garganta.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Confinamiento extremo en las jaulas de gestación y parideras que impiden que los animales puedan siquiera darse la vuelta.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Estereotipias. Las cerdas fueron frecuentemente observadas mordiendo los barrotes u oscilando el cuerpo constantemente.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Cerdos que padecen graves heridas. Se observaron profundas heridas en los lomos de algunos animales y muchos lechones paralizados o parapléjicos, probablemente como resultado de un daño en su médula espinal.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Lechones con abrasiones y lesiones ulceradas en sus articulaciones. Estas heridas pueden llevar a infecciones.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Varios lechones muertos en las jaulas parideras, probablemente tras haber sido aplastados por sus madres.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Cerdos golpeados en la cabeza con una barra de hierro.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Un cerdo adulto fue observado amordazado por el hocico. El animal estaba claramente sufriendo.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Varios segmentos de colas amputadas como resultado de una mutilación que es llevada a cabo de forma rutinaria en las granjas. La amputación de las colas se realiza generalmente sin anestesia y puede ser un procedimiento altamente doloroso.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Trabajadores obligando a cerdos adultos a desplazarse dándoles patadas. Una cerda fue golpeada repetidas veces recibiendo varios puñetazos en su sensible hocico y en la cabeza.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Lechones que acaban de ser destetados y son lanzados con agresividad.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Un trabajador agarrando a un cerdo adulto por su cola para inmovilizarlo y su apuñalamiento con un cuchillo.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/animalequalityuk/albums/72157629572561783"><img src="https://farm8.staticflickr.com/7205/6989733085_e61fedcb1d_z.jpg" alt="East Anglian Pig Company - Norfolk"/></a></figure>
<!-- /wp:image -->

<!-- wp:html -->
<script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>SUPERMERCADOS Y MARCAS INVOLUCRADOS</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>•&nbsp;</strong><a href="http://www.eapigs.co.uk/">East Anglian Pig</a> es una compañía propiedad de <a href="https://cranswick.plc.uk/">Cranswick Plc </a>- un proveedor alimentario líder en el Reino Unido que produce para los principales supermercados del país tales como <a href="https://www.facebook.com/MorrisonsWeLoveFood">Morrisons</a>, <a href="https://www.facebook.com/sainsburys">Sainsbury’s</a>, <a href="https://www.facebook.com/tesco">Tesco</a>, <a href="https://www.facebook.com/TheCooperativeFood">The Cooperative Food</a>,<a href="https://www.facebook.com/Waitrose"> Waitrose</a> y <a href="https://www.facebook.com/Asda">ASDA</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>•&nbsp;</strong>La compañía produce una variedad de productos para marcas como <a href="https://www.facebook.com/jamieoliver">Jamie Oliver</a>, <a href="https://www.facebook.com/weightwatchers?ref=ts">Weight Watchers</a>, <a href="http://www.cranswick.plc.uk/bacon/bacon_richardwoodall.html">Richard Woodall</a>, <a href="https://www.facebook.com/theblackfarmer">The Black Farmer</a>, <a href="https://www.facebook.com/redlionfoods">Red Lion Foods</a>, <a href="http://www.reggae-reggae.co.uk/">Reggae Reggae</a>, <a href="http://www.simplysausages.co.uk/">Simply Sausages</a> y <a href="http://www.yorkshirebaker.co.uk/">Yorkshire Baker</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• <a href="https://cranswick.plc.uk/">Cranswick Pork </a>es también quien fabrica famosos platos que son servidos en <a href="https://www.facebook.com/McDonaldsUK">McDonald's</a> como el McMuffin de salchicha y huevo. <a href="https://cranswick.plc.uk/">Cranswick </a>fue distinguido como proveedor del año por <a href="http://www.cranswick.plc.uk/plc_awards_recognition.html">McDonald's</a> el 4 de abril de 2011.<strong>PARTICIPA</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>• ¡<a href="http://www.vivevegano.org">Elige un estilo de vida vegano</a>!<br>
La mayoría de nosotros pensamos que los animales no deben ser dañados si podemos evitarlo, sin embargo el consumo de productos animales impone a éstos una vida de miseria y sufrimiento que podríamos evitar fácilmente.<br>
<br>
• <a href="http://www.igualdadanimal.org/colabora/donativos.php">Por favor, considera hacer un donativo</a> o <a href="http://www.animalequality.net/collaborate/become-a-member.php">hacerte socio-colaborador</a> para ayudar a Igualdad Animal a continuar con un trabajo de vital importancia en defensa de los animales.<br>
<br>
• Puedes ayudar a que las imágenes tengan mayor repercusión publicando y compartiendo esta investigación en tu Facebook, Twitter o cuenta de Google +, subiendo nuestro vídeo a tu cuenta de YouTube o Vimeo, blog o sitio web.<br>
<br>
• <a href="https://igualdadanimal.org/dona/">¡Únete a nosotros! </a>Rellena el formulario para hacernos saber cómo te gustaría ayudar. Sé la voz de los sin voz.<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->