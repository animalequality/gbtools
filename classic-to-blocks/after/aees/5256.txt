<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/albums/72157623234983076"><img src="https://farm3.staticflickr.com/2791/4284384607_1d7ff01521_z.jpg" alt="18/01/2010 - Madrid - Juicio por saltar a la Pasarela Cibeles"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
Esta mañana ha quedado visto para sentencia el juicio contra los dos activistas de Igualdad Animal, Javier Moreno y José Valle, que saltaron el pasado 23 de Febrero de 2009 a la pasarela Cibeles en protesta por la utilización de animales para la fabricación de prendas de vestir.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los activistas de Igualdad Animal eran acusados de supuesto desorden público por el jefe de seguridad del IFEMA, quien presentándose como acusación particular, ha pedido a través de su abogado que se multe a cada activista con 900 euros (1.800 euros en total) así como una pena de localización permanente de once días. La fiscalía y la defensa, en cambio, han coincidido al solicitar la libre absolución.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Al juicio han acudido diversos simpatizantes de Igualdad Animal que han mostrado una pancarta y carteles contra la peletería y se han manifestado apoyando esta acción que hace un año tuvo una gran repercusión mediática y social.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta acción forma parte de la campaña "Piel es asesinato" que igualdad Animal está llevando a cabo contra la explotación animal en el ámbito de la vestimenta que incluye una investigación de más de un año recorriendo aproximadamente la mitad de las granjas de visones de España.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Web de la investigación: <a href="http://www.PielEsAsesinato.com">www.PielEsAsesinato.com</a></p>
<!-- /wp:paragraph -->