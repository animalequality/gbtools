<!-- wp:image {"id":9416} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/03/delfin_decapitado_tarragona.jpg" alt="" class="wp-image-9416"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Se investiga la aparición de una cabeza de delfín en la zona del muelle de pescadores de El Serrallo, Tarragona, el pasado martes. Se rumorea que el cetáceo podría haber sido <strong>cazado mediante un arpón</strong> para ser posteriormente <strong>decapitado con un arma afilada</strong>, posiblemente un cuchillo, ya que el corte limpio no puede ser atribuido a las hélices de ninguna embarcación.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pese a que la hipótesis inicial contemplaba la posibilidad de que la víctima hubiera quedado atrapada en las redes de algún barco, fue descartada al no encontrarse señales de rozaduras en su cara. Las heridas observadas sólo eran <em>post mortem</em>, por lo que la teoría que cobra más fuerza es que hubiese sido matado por un arpón. El personal de Salvamento Marítimo afirmó haber observado un grupo de delfines el martes por la mañana en la zona de la bocana del puerto.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se desconoce dónde fue a parar el resto del cuerpo del delfín decapitado, aunque <strong>se sospecha que su carne haya sido utilizada para consumo humano</strong>.</p>
<!-- /wp:paragraph -->