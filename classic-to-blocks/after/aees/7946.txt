<!-- wp:image {"id":12594} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/11/shutterstock_75212668.jpg" alt="" class="wp-image-12594"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Durante mucho tiempo se ha pensado en los peces como animales desprovistos de sentimientos y sensibilidad. Allí, en las profundidades de un medio que no es el nuestro, <strong>permanecen olvidados, incluso por quienes rechazamos el maltrato hacia los animales. </strong>Todos sabemos que los peces existen, pero en cuanto a su maltrato, no les ponemos en la misma categoría que a vacas, cerdos u otros animales.<br>
<br>
La pesca industrial es responsable del mayor número de muertes de animales en el planeta y las piscifactorías los someten a una crueldad extrema. Además, <strong>no existe ley alguna que proteja a los peces, y por eso <a href="https://igualdadanimal.mx/blog/los-peces-sufren-igual-que-los-mamiferos-y-las-aves/" target="_blank">su sufrimiento es inimaginable</a>.</strong> Y sí, los peces sienten y sufren las más dolorosas de las muertes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2016/07/22/los-peces-son-los-animales-mas-olvidados-y-nos-necesitan/" target="_blank">Los peces son los animales más olvidados y nos necesitan</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
Diversos estudios científicos han arrojado luces a favor de estos sensibles e inteligentes animales. <a href="https://www.nature.com/articles/s41598-017-13173-x" target="_blank">El más reciente</a>, publicado en Scientific Sports, reveló que <strong>investigadores portugueses demostraron por primera vez que los peces tienen estados emocionales</strong> desencadenados por la forma en que perciben los estímulos ambientales.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12595} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/11/shutterstock_120698047.jpg" alt="" class="wp-image-12595"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El equipo dirigido por Rui Oliveira, investigador de la Universidad de Algarve, entre otras, evaluó que esto no sería tarea fácil. La razón era que a diferencia de los humanos que sienten conscientemente y verbalizan las emociones, no creían que existiese una forma de comprobar si los animales sienten emociones. Pero debido a que un estado emocional es más que un sentimiento, <strong>realizaron pruebas que permitieron deducir si la respuesta a un determinado estímulo estaba asociada con un estado emocional. </strong>&nbsp;<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</p></font>

<p><br>
Los investigadores sometieron a besugos a condiciones que podrían desencadenar en un estado emocional. Los peces respondieron de manera diferente al mismo estímulo según la forma en que eran evaluados. Se evaluó la interacción y conductas de escape entre los peces además de sus niveles de cortisol (hormona del estrés) y las áreas del cerebro asociadas con estados emocionales positivos y negativos.<br>
<br>
«Es la primera vez que se demuestra que los peces pueden activar respuestas fisiológicas y neuromoleculares en el sistema nervioso central como respuesta a estímulos emocionales basados en el significado que tiene para el pez», afirmó Rui Oliveira. Y también explicó que la capacidad de evaluar los estímulos emocionales en los peces puede tener una base neurológica más simple de la esperada y que pudo haber evolucionado hace 375 millones de años.</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://www.europapress.es/ciencia/laboratorio/noticia-peces-tambien-sienten-emocionalmente-20171027183008.html" target="_blank">https://www.europapress.es/ciencia/laboratorio/noticia-peces-tambien-sienten-emocionalmente-20171027183008.html</a></p></font>
<!-- /wp:html -->