<!-- wp:image {"id":9627} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/07/foie_francia.jpg" alt="" class="wp-image-9627"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Durante una breve audiencia ante un Tribunal Federal en Los Angeles, <strong>el juez Stephen V. Wilson rechazó los argumentos</strong> que un sindicato de productores canadienses de foie gras, un grupo de restaurantes y un productor de Nueva York alegaban para derogar la prohibición que recae sobre la producción y comercialización de foie gras en California.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El abogado de los demandantes afirmó que llevará el caso ante la Corte de Apelaciones del Oeste de Estados Unidos, en San Francisco. Según él, la prohibición es «<em>anticonstitucional, vaga y contraria a las leyes del libre comercio»</em>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La multa que actualmente debe afrontar un restaurante por ofrecer foie gras en su menú llega a alcanzar los 1.000 dólares.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->