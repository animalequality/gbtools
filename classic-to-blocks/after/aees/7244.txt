<!-- wp:image {"id":13828} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/03/cabeceratheghosts.jpg" alt="" class="wp-image-13828"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14030} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/03/ghosts1.jpg" alt="" class="wp-image-14030"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El próximo <strong>miércoles 25 de marzo a las 19 horas en la Facultad de Filosofía de Santiago de Compostela</strong>, Igualdad Animal proyectará el aclamado documental "<a href="https://www.theghostsinourmachine.com/"><strong>The Ghosts In Our Machine</strong></a>". El documental, <strong>considerado como uno de los mejores de temática animalista realizados hasta la fecha</strong>, está dirigido por Liz Marshall y protagonizado por la prestigiosa fotógrafa y activista Jo-Anne McArthur, y nos hace reflexionar sobre nuestra relación con los animales en un mundo donde son condenados a una vida de miseria y desolación.<br>
&nbsp;<br>
Jo-Anne recorre el mundo con su cámara documentando la situación de los animales en granjas peleteras, mataderos, circos o zoos. También documenta la esperanza a través de sus trabajos en refugios y santuarios donde los animales pueden vivir una vida lejos del maltrato.<br>
&nbsp;<br>
El documental <a href="http://www.theghostsinourmachine.com/awards/">ha ganado varios premios</a> y ha recibido buenas críticas en Canadá y Estados Unidos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/u5vYH4KlHBs" width="885px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->