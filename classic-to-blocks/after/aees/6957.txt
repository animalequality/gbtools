<!-- wp:image {"id":9947} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/03/perros_pakistan_peleas.jpg" alt="" class="wp-image-9947"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Las peleas de perros son algo habitual en Punjab, Pakistán, durante la temporada de invierno. Las reglas del combate son sencillas: la pelea termina cuando uno de los perros <strong>muere desangrado,</strong> huye o su dueño decide retirarlo, convirtiendo al otro perro en "vencedor" del encuentro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El destino de los animales heridos durante los combates es incierto: <em>«Cada uno se ocupa de su perro. Aquí los veterinarios sólo curan animales grandes. <strong>Nadie se preocupa por los perros</strong>. Quizás en Islamabad sí, puede haber clínicas, pero <strong>aquí en la zona rural no hay nada para curar a los perros.</strong>»</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/se0KGneiQug" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«Estos combates se realizan en invierno, el resto del año entrenamos a los perros. Para cada perro hay una persona que se ocupa de sus ejercicios:&nbsp;<strong>sacarlo a pasear por la mañana y por la noche</strong>»</em>, explica uno de los&nbsp;<a href="http://www.igualdadanimal.org/articulos/gary-francione/animales-como-propiedad" target="_blank">propietarios&nbsp;</a>de estos perros, que afirma que los perros son alimentados y cuidados con mucho esmero.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A pesar de tratarse de una <strong>práctica&nbsp;ilegal </strong>según las leyes de Pakistán, que puede suponer 6 meses de cárcel y multas de 11 dólares, <strong>la policía pasa por alto</strong> estos encuentros y los aficionados siguen&nbsp;realizando las peleas con total impunidad.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«<strong>Es un entretenimiento para los hombres</strong>. Nuestros perros son mejorados especialmente para las batallas</em>», afirma uno de los asistentes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->