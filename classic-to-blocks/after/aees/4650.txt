<!-- wp:paragraph -->
<p>El próximo Martes&nbsp; 20 de Enero a las 00.30 horas de la madrugada,&nbsp;el programa "Diario de..." de Telecinco incluirá un reportaje sobre Igualdad Animal y dos de sus acciones más impactantes del año 2008; el salto al ruedo y el descuelgue de una pancarta de la plaza de toros "La misericordia" de Zaragoza&nbsp;realizado el pasado 19 de Octubre:</p>
<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://igualdadanimal.org/noticia/2008/10/29/igualdad-animal-salta-al-ruedo-y-se-descuelga-de-la-plaza-de-toros-de-zaragoza/"} -->
<figure class="wp-block-embed"><div class="wp-block-embed__wrapper">
https://igualdadanimal.org/noticia/2008/10/29/igualdad-animal-salta-al-ruedo-y-se-descuelga-de-la-plaza-de-toros-de-zaragoza/
</div></figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->
<p>y el rescate de 10 gallinas rpresentado con motivo del Día Internacional de los Derechos Animales:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="http://www.rescateabierto.org/igualdad-animal-rescata-a-diez-gallinas">http://www.rescateabierto.org/igualdad-animal-rescata-a-diez-gallinas</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>el programa&nbsp;que tratará sobre&nbsp;desobediencia civil y acción directa no violenta será presentado por&nbsp;por Merecedes Mila. Es posible ver un avance de 2 minutos en:&nbsp;<a href="http://mitele.telecinco.es/programas/diario/61810.shtml">http://mitele.telecinco.es/programas/diario/61810.shtml</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Durante varios días, reporteros del programa nos acompañaron en la preparación previa, durante y después de las dos acciones pudiendo presenciar de primera mano la organización y de la mismas. Durante la preparación del reportaje nuestros activistas se infiltraron en la plaza de toros con cámara oculta, mostraron delante de las cámaras todo el material y la preparación necesaria para realizar descuelgues, saltos al ruedo y rescates, realizando además&nbsp;varias entrevistas a camara durante las acciones.</p>
<!-- /wp:paragraph -->