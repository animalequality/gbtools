<!-- wp:paragraph -->
<p>Addis Abeba – Un “zoológico” de Etiopía está envenenando bebés de león y vendiendo después los cadáveres para ser disecados. Los responsables alegan que el “zoológico” carece de fondos suficientes para hacerse cargo de más leones. Los animales no humanos muertos son vendidos a taxidermistas por 170 dólares.

"Estos animales son el orgullo de nuestro país, pero en este momento nuestra única alternativa es enviarlos al taxidermista" dijo Muhedin Abdulaziz, del Zoológico de Leones, a la Associated Press. También declaró que desde que él trabaja allí, se han asesinado seis bebés, pero que desconoce si la cifra puede ser mayor. 

Es demasiado evidente que existe una alternativa a tener que matar a estos bebés, y es cerrar esa cárcel y liberar a los leones allí donde puedan vivir sus vidas en libertad. Pero, por desgracia, para los visitantes y responsables de esta prisión, esa sería la última opción a elegir. Aunque haya que llevarse por delante la vida de más animales no humanos, preferirán mantenerlos en sus jaulas para que los visitantes puedan disfrutar a su costa.</p>
<!-- /wp:paragraph -->