<!-- wp:paragraph -->
<p>España - La aparición de dos perros muertos y despellejados y de otro herido en la perrera de Cambados (Pontevedra), ocurrida el jueves de la semana pasada, podría estar relacionada con la actuación de personas desconocidas que 'metieron otros perros para realizar peleas' entre los animales, según indicó a Europa Press, la gerente del citado centro Olga Costa.

La gerente de la perrera de Cambados (Pontevedra) explicó cómo, en la mañana del jueves de la semana pasada, al llegar al centro 'se encontró el primer perro muerto en el centro de la pista, y que por su estado parecía que había sido atacado por otro perro en el cuello'.

Añadió que, durante el recuento de los perros, encontraron otro perro muerto en la parte trasera de la perrera. A la hora de describir el estado en el que se encontraba el segundo animal no humano, Olga no pudo evitar un gesto de horror porque, según declaró, 'en la parte de arriba le faltaba toda de la piel'. La responsable de la perrera de Cambados descubrió también una herida profunda en otro de los perros.

Según Olga Costa, todo apunta a que detrás del macabro hallazgo hay 'personas sin sentimientos' que 'metieron sus perros dentro del recinto para realizar peleas de perros'. Una sospecha que se ve acrecentada por el hecho de que es 'la tercera vez que sucede' y porque los ataques a los perros se produjeron 'precisamente en los dos huecos no cubiertos por las cámaras de seguridad'.

La responsable de la perrera descarta la posibilidad de que fuera una pelea entre ellos, ya que los perros que fueron víctimas del suceso llevaban mucho tiempo en la perrera y que nunca habían causado ningún problema. 'Los perros atacados eran dos de los cuatro de raza pequeña que por falta de espacio estaban fuera de las jaulas, y los perros pequeños no se hacen eso y los otros es imposible que salgan de las jaulas', aseveró.

Fuente - Terra</p>
<!-- /wp:paragraph -->