<!-- wp:paragraph -->
<p>Una llamada anónima alertaba sobre la existencia de un vídeo subido a YouTube, y en el que un perro era quemado vivo aparentemente por alumnos de la Escuela Baquedano de Coyhaique (Chile).

En el citado vídeo se puede apreciar cómo en el interior de una vivienda un grupo de menores se jacta de la acción, antes de asesinarle prendiéndole fuego, después de rociarle con parafina, según se puede escuchar en el audio de la grabación.

Fuente: El Diario de Aysén</p>
<!-- /wp:paragraph -->