<!-- wp:paragraph -->
<p>Las regiones italianas de Lombardía y Emilia- Romagna continúan manteniendo una de las tasas más altas de Europa en el corte de cola a los cerdos destinados a la industria cárnica. De hecho, el 98% de los ganaderos de esta zona del país sigue realizando actualmente esta práctica.

“Es un caso emblemático, porque estos productos italianos se asocian con la excelencia”, afirma Elena Nalón, asesora veterinaria para animales de granja, para The Guardian. “Los productores hacen gala de los requisitos legales mínimos en materia de bienestar animal” en el país, donde la industria generó el año pasado hasta 7,98 mil millones de euros.

Como ya hemos demostrado en varias <a href="https://www.youtube.com/watch?v=hkDTQ5RhrHk&amp;feature=youtu.be" target="_blank" rel="noopener">investigaciones de Igualdad Animal,</a><strong>los cerdos son sometidos al corte de la cola con apenas dos o tres días de vida</strong>, y se realiza sin ningún tipo de anestesia. Además, varios estudios confirman que los cerdos sienten el dolor y sufren un trauma agudo por ello, desarrollando estrés y pudiendo llegar a sufrir infecciones duraderas en la zona.

Te puede interesar: <a href="https://igualdadanimal.org/blog/4-crueles-practicas-de-la-industria-porcina-con-los-cerditos-recien-nacidos/" target="_blank" rel="noopener">4 crueles prácticas de la industria porcina con los cerditos recién nacidos</a>

Lo que se busca con esta práctica es prevenir lesiones graves entre estos animales. Pero un informe de la UE de 2014 señala que estas agresiones entre cerdos se producen por las condiciones estresantes en las que viven normalmente en las granjas industriales. Los cerdos no disponen del espacio suficiente para desarrollar su comportamiento natural, lo que puede desencadenar en moderaduras u otro tipo de agresiones entre ellos.

De hecho, <strong>el corte de cola en los cerdos es ilegal, según la directiva europea</strong>, y aunque esta legislación no se aplica en todo el continente, su práctica sí es ilegal en Italia (y también en España). Lo que nos lleva a preguntarnos por qué se sigue ignorando este problema.

“Para que se pueda cortar la cola de un cerdo, tienen que haberse dado lesiones previas en zonas de otros cerdos como las orejas”, explica Enrico Moriconi, exveterinario y defensor de los animales. “Pero esta práctica se realiza cuando los lechones tienen pocos días de edad. Es imposible saber en ese momento si el grupo se comportará de esa manera. <strong>Simplemente se asume que este tipo de cría conduce a la mordedura de la cola, por eso las cortan</strong>”.

Pero Italia no es el único país europeo en altas tasas de corte de cola en cerdos. <a href="http://www.igualdadanimal.org/noticias/8027/nueva-investigacion-en-granja-proveedora-de-el-pozo-en-colaboracion-con-el-programa" target="_blank" rel="noopener">En Igualdad Animal hemos realizado numerosas investigaciones en granjas industriales españolas,</a> donde las condiciones de vida de los cerdos son difíciles de creer. No solo se produce este tipo de práctica, también les recortan los dientes con alicates sin ningún tipo de anestesia, para evitar que las crías luchen entre ellas por mamar. Además, estos animales son castrados y maltratados desde que nacen.

El Gobierno italiano reconoce el problema de las granjas industriales del país y este año ha establecido un grupo de trabajo para elaborar un plan de acción a implementar en las 3.000 granjas de cría en Italia para mejorar las condiciones de los animales en un periodo de tres años.

Aun así, la industria toma como defensa el clima de la zona, ya que el calor afecta a los cerdos porque tienen problemas para regular su propia temperatura. <strong>“Los animales se ponen nerviosos”</strong>, dice Stefano Salvarani, representante de los criadores de cerdos de ‘Confragricultora’, una de las principales asociaciones de agricultores. “Los problemas comienzan con el verano. Cuando se alteran, no pueden hacer nada más que morder a sus vecinos”.

Te puede interesar:&nbsp;<a href="https://igualdadanimal.org/noticia/2017/04/11/industria-porcina-espanola-un-desastre-medioambiental-y-de-maltrato-animal/" target="_blank" rel="noopener">Industria porcina española,&nbsp;un desastre medioambiental y de maltrato animal</a>

Sin embargo, Pietro Pizzagali, de la granja de alto bienestar Fumagalli, en Senna Lodigiana, discrepa. Allí los animales tienen espacio para moverse, “ponemos menos cerdos por metro cuadrado que lo que exige la ley. Obtenemos menos cantidad de carne, pero creemos que merece la pena”. Aunque puntualiza que <strong>criar a los animales de una forma óptima “es más caro”.</strong> “Con la paja el cerdo se mueve más y crece más lentamente”.

<strong>¿Puede Italia solucionar este problema si continúa sin aplicar estrictamente esta ley?</strong>

Según el veterinario Giovanni Aborali, que encabeza el grupo de trabajo creado por el Gobierno de Italia, es el momento del cambio. “Los criadores entienden lo importante que es la tendencia del bienestar animal”.

“Cambiar esta situación requerirá una revisión del sistema, un cambio en la mentalidad de los agricultores para poner las necesidades de los animales en el centro del modelo de producción de cerdos” dice Elena Nalón. “<strong>Algunos agricultores ya están haciendo esto, demostrando que no es imposible</strong>. Tenemos que convertirlo en la corriente principal”.</p>
<!-- /wp:paragraph -->