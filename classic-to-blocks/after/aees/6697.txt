<!-- wp:image {"id":9687} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/09/ecoducto_paises_bajos_p.jpg" alt="" class="wp-image-9687"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>En Argentina, el reciente atropello de un jaguar en una ruta del norte del país planteó la controversia de cómo deben convivir los humanos con los animales en libertad, dado que decenas de miles de animales mueren todos los años en las rutas argentinas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«Debido al desarrollo humano muchas de las áreas naturales se fueron achicando y quedaron aisladas entre sí. Los animales deben salir e ir hacia zonas más amplias para sobrevivir, pero corren peligro en el camino»</em>, explicó Diego Varela, de la ONG Conservación Argentina.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para evitar que los animales mueran haciendo este recorrido, Varela trabaja con el departamento de vialidad de Misiones creando un <strong>"corredor biológico</strong>": un área segura de conexión entre dos parques. El recurso más utilizado es el "<strong>pasafauna</strong>", un túnel que pasa por debajo del asfalto y gracias al cual los animales pueden transitar al otro lado sin peligro. <strong>Argentina comenzó a utilizarlos en 1994 y en la actualidad hay 12 en Misiones</strong>, esperando que este número número se triplique en los próximos años.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13929} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/09/ecoducto_paises_bajos.jpg" alt="" class="wp-image-13929" title="Ecoducto &quot;El Borkeld&quot;, en Países Bajos, uno de los primeros países en implementar una red de pasos de fauna"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Otra alternativa son los "<strong>ecoductos</strong>", unos puentes que se elevan sobre las vías y que permiten igualmente un cruce seguro. <strong>Argentina tiene el único ecoducto de América Latina</strong>, siendo un recurso poco utilizado debido a su alto costo de construcción.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pero más allá de la infraestructura, los especialistas coinciden en que <strong>para reducir las muertes de animales en las vías es necesaria la concienciación de la población</strong> sobre la importancia de reducir la velocidad y respetar la señalización, tomando conciencia de que compartimos el espacio con animals de otras especies.<br>
	<br>
	Después del incidente con el jaguar, las autoridades también contemplan aumentar las multas para quienes atropellen un animal tras infringir el límite de velocidad.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14076} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/09/jaguar.jpg" alt="" class="wp-image-14076" title="Jaguar"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
	Debido quizás a que el número de jaguares que viven en Argentina no supera los 250 individuos, la muerte de este animal generó una movilización importante por parte de ONG's ambientalistas. Varios medios se hicieron eco del problema. Incluso el diario La Nación, el segundo más importante del país, publicó un editorial <strong>alertando sobre la necesidad de tomar medidas para solucionar este tema.</strong></p>
<!-- /wp:paragraph -->