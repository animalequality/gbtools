<!-- wp:paragraph -->
<p>Europa - Sony Europa organizó hace unos días una fiesta para celebrar el lanzamiento de God of War II en la región y ¿qué mejor manera de hacerlo que mostrar una cabra desmembrada acompañada de una chica en topless con bodypaint? Los reportes indican que los asistentes fueron invitados a tocar las víceras del animal, las cuales aún estaban tibias.</p>
<!-- /wp:paragraph -->

<!-- wp:shortcode -->
[img_assist|nid=2547|title=|desc=|link=none|align=right|width=200|height=187]
<!-- /wp:shortcode -->

<!-- wp:paragraph -->
<p>No contentos con el aberrante espectaculo ideado por las personas más degeneradas del universo, la gente de Sony tomó fotografías del evento, las agregó a su revista oficial y puso en circulación 80,000 ejemplares con imágenes bizarras que le llegaron tanto a padres de familia y sus hijos como a los adolescentes que repudiaron las imágenes.

Claro, el grito de “¿qué rayos te sucede Sony?” fue unísono y la empresa se vio obligada a ofrecer una disculpa pública por su nada brillante idea y a retirar las revistas del mercado y pedir las ya vendidas. Pésimo por Sony, de verdad, ni ganas de hacer comentarios sarcásticos.

Fuente: Unratedgames.com</p>
<!-- /wp:paragraph -->