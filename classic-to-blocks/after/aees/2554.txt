<!-- wp:paragraph -->
<p>Navarra - El Servicio de Protección de la Naturaleza (Seprona) de la Guardia Civil puso en marcha una investigación tras recibir varias denuncias sobre la sustracción de perros en diferentes localidades de Navarra.
Las investigaciones dieron su resultado el pasado 14 de abril al descubrir que un vecino de Lodosa había recibido dos perros robados de raza Podanco Portugués procedentes de la localidad riojana de Calahorra a través de una agencia de transportes.

Tras comprobar que los números de chip de los canes no correspondían con los de su cartilla sanitaria, los agentes registraron el domicilio del sospechoso, donde se localizaron 45 perros, los cuales presentaban lesiones cutáneas debido a parásitos y riñas entre ellos. El estado de delgadez de algunos de los animales era extrema y ocho de ellos habían estado atados permanentemente con cadenas cortas.

Los agentes detuvieron al sospechoso, un vecino de Lodosa de 34 años, J.L.J.J, así como a su hermano de 29 años, A.J.K, por los delitos de maltrato de animales domésticos, robo, receptación y falsificación de documentos públicos. Una tercera persona, F.A.B., vecino de Verín (Orense), fue detenida por haber remitido los animales.

Los 45 perros han quedado a cargo de la sociedad protectora de Echauri.

Fuente: deia.com</p>
<!-- /wp:paragraph -->