<!-- wp:paragraph -->
<p>El Congo - El virus del Ébola es la principal causa de mortalidad de las poblaciones de grandes simios, principalmente gorilas y chimpancés, en el santuario de fauna de Lossi, situado en el nordeste de la República del Congo y su alrededor, según un estudio internacional que publica la revista Science.

El trabajo, en el que han intervenido expertos de la Universidad de Barcelona (UB ), revela que de un censo de nidos que cubría una extensión de 5.000 kilómetros, el ébola ha matado a un total de 5.000 gorilas.

La transmisión de grupo a grupo ha amplificado la mortandad los autores del estudio han comprobado que el desfase del inicio de la mortalidad entre los diferentes grupos de gorilas vecinos estudiados es muy cercano a la longitud del ciclo de la enfermedad del ébola (doce días), lo que evidencia que la transmisión de grupo a grupo ha amplificado la mortandad.

La investigación concluye que la muerte de más de 5.000 gorilas en el área de estudio es un caso único de pérdida de ejemplares en una población animal en tan poco tiempo, a la vez que evidencia que la transmisión entre familias amplifica los episodios de mortalidad.

El virus Ébola es el responsable de una enfermedad febril aguda, muy severa y a menudo muy mortal que afecta a los primates.

El reconocimiento del virus se hizo por primera vez en 1976 durante una epidemia simultánea en Zaire y Sudán. Del total de 550 casos, fallecieron 470 personas.

El virus se transmite por contacto directo con líquidos corporales infectados como la sangre, la saliva, el sudor, la orina o los vómitos. 

Fuente - 20 minutos</p>
<!-- /wp:paragraph -->