<!-- wp:image {"id":12470} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/shutterstock_411499069.jpg" alt="" class="wp-image-12470"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>¿Qué está pasando cuando ves que la mayor empresa productora de lácteos en Canadá decide que podría sumarse al mercado de las alternativas vegetales? Pasa que <strong>los consumidores son quienes transforman la industria de alimentos y ellos están cada vez más conscientes de adquirir los productos más saludables</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por supuesto esto ha provocado que con más empresas produciendo leches vegetales los anaqueles de los supermercados tengan cada vez más y más opciones a escoger. <strong>Es una tendencia indetenible que tiene lugar en todo el mundo</strong>, afortunadamente para los animales y el bienestar del planeta también.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2015/09/25/8-crueles-practicas-estandar-de-la-industria-lactea-con-las-vacas/" target="_blank">8 crueles prácticas estándar de la industria láctea con las vacas</a></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Saputo, el mayor procesador de lácteos de Canadá, está considerando invertir en el mercado de las alternativas a los lácteos. De encontrar y poder concretar un acuerdo, esta compañía que ya cuenta con una excelente presencia de distribución en Norteamérica usaría su fuerte plataforma para llevar más productos a las tiendas y supermercados.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12471} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/shutterstock_304690607.jpg" alt="" class="wp-image-12471"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/04/04/tras-fabricar-lacteos-por-mas-de-90-anos-gigante-empresa-pasa-producir-unicamente/" target="_blank">Tras fabricar lácteos por más de 90 años gigante empresa pasa a producir únicamente leches vegetales</a></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El gigante, con sede en Montreal, ha hecho 25 adquisiciones desde 1997, incluyendo el negocio de productos lácteos Neilson de George Weston en 2005 por 465 millones dólares y Dairyworld por $ 407 millones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La demanda de productos lácteos ha disminuido en América del Norte debido a que <strong>los consumidores están recurriendo cada vez más a las bebidas vegetales</strong> a base de proteínas de almendras y anacardos. De acuerdo al <a href="http://www.nielsen.com/ca/en/insights/news/2017/plant-based-proteins-are-gaining-dollar-share-among-north-americans.html," target="_blank">reporte</a> de Nielsen, las ventas de estos productos aumentaron en un 14, 7% en el período de 12 meses finalizado el 8 de julio. En Canadá, el mercado de bebidas vegetales ha crecido 7% desde el año pasado; y el mercado global de productos a base de vegetales en este país vale más de $ 1,5 mil millones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Está claro que invertir en productos alternativos a los lácteos <strong>no solamente es un movimiento inteligente sino también necesario&nbsp;para todas aquellas empresas de alimentos que quieran responder a las necesidades e intereses de los consumidores</strong>. Saputo sería más que sagaz si hace estos cambios en un futuro próximo como ya lo han hecho grandes empresas como el gigante del yogur&nbsp;Danone o Elmhurst, el ex-procesador de productos lácteos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fuentes:</p>
<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://www.bnnbloomberg.ca/saputo-mulls-a-move-into-the-plant-based-drink-business-1.863879"} -->
<figure class="wp-block-embed"><div class="wp-block-embed__wrapper">
https://www.bnnbloomberg.ca/saputo-mulls-a-move-into-the-plant-based-drink-business-1.863879
</div></figure>
<!-- /wp:embed -->

<!-- wp:embed {"url":"https://www.onegreenplanet.org/news/canadas-largest-dairy-processor-looking-at-plant-based-milk/"} -->
<figure class="wp-block-embed"><div class="wp-block-embed__wrapper">
https://www.onegreenplanet.org/news/canadas-largest-dairy-processor-looking-at-plant-based-milk/
</div></figure>
<!-- /wp:embed -->