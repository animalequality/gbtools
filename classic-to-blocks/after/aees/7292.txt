<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10291" src="/app/uploads/2015/09/perro3.jpg" style="float:right; margin:10px; width:450px">Simba volaba de Tel Aviv a Toronto en la zona de cargo cuando el capitán del vuelo comprobó que había un problema con el sistema de calefacción. Sin dudarlo, el capitán desvió el vuelo, temiendo por la vida de Simba.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>“<em>Tan pronto como la tripulación se dio cuenta del problema empezaron a preocuparse”, declaraba ayer el portavoz de Air Canada Peter Fitzpatrick</em>. “<em>Con la altitud la situación podría haberse vuelto muy desagradable para Simba, y su vida habría estado en peligro si el vuelo hubiese continuado</em>”, añadió.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El piloto desvió el vuelo a Frankfurt donde Simba fue reubicado en otro vuelo, en perfecto estado de salud.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El desvió causó un retraso de 75 minutos a los pasajeros del vuelo de Air Canada. “<em>Reconocemos que esto supuso un inconveniente para nuestros pasajeros</em>”, dijo Fitzpatrick, “<em>pero la reacción mayoritaria fue que se hizo lo correcto, una vez que supieron que la vida de Simba estaba en juego</em>.”</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El dueño de Simba, German Kontorovich, declaró emocionado, ya en Toronto, “<em>es mi perro, es como mi hijo, lo significa todo para mí</em>.” Otro pasajero declaraba “<em>Sin duda desviar nuestro vuelo fue la decisión correcta</em>”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El experto en aviación &nbsp;Phyl Durby declaraba a los medios de comunicación que “<em>el desvío habrá costado a la aerolínea miles de dólares en fuel, pero sin duda fue lo correcto. El capitán es el responsable de todas las vidas a bordo</em>”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->