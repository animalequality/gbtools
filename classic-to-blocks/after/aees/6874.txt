<!-- wp:paragraph -->
<p>Agentes de la Policía Nacional han detenido a <strong>cuatro trabajadores del Circo Berlín</strong>, situado en Málaga, acusados de las <strong>agresiones </strong>que denunciamos activistas de Igualdad Animal <a href="https://igualdadanimal.org/noticia/2013/01/06/tres-activistas-de-igualdad-animal-agredidos-por-trabajadores-del-circo-berlin/" target="_blank">el pasado 6 de enero.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-13753" src="/app/uploads/2013/01/activista_agredido_circo_be.jpg" style="width: 150px; " title="Activista de Igualdad Animal agredido por miembros del Circo Berlin en Málaga">Han sido <strong>arrestados cuatro hombres</strong>, entre ellos el <strong>gerente del circo</strong>, a los que se les imputa un presunto<strong> delito de lesiones</strong>. A uno de los trabajadores también se le imputan los <strong>daños</strong> causados a parte del material de los activistas, como dos cámaras fotográficas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Los detenidos y las diligencias instruidas por la policía han sido puestos ya a disposición judicial.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los cinco activistas de Igualdad Animal fueron agredidos el pasado 6 de enero por trabajadores del Circo Berlín instalado en la ciudad de Málaga, mientras trataban de informar a los viandantes sobre la injusticia que padecen los animales en estos espectáculos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Uno de los activistas recibió un <strong>cabezazo en la nariz</strong>. Además de ser <strong>empujados y zarandeados</strong>, recibieron varios <strong>puñetazos en el rostro y golpes en la cabeza</strong>, no pudiendo evitar la rotura del megáfono de la organización y de dos cámaras fotográficas personales, así como la violenta apropiación de algunos de sus teléfonos móviles.<br>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13893} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/01/circo_berlin_malaga_protest.jpg" alt="" class="wp-image-13893"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Mañana domingo</strong> Igualdad Animal ha convocado una <strong>concentración en respuesta a estas agresiones</strong> y para reclamar el fin de los circos con animales<strong> frente al Circo Berlín, a las 15:30 horas</strong> en el recinto ferial de la ciudad de Málaga. Para participar, por favor escribe a AteneaG@IgualdadAnimal.org</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->