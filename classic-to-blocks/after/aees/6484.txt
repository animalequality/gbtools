<!-- wp:image {"id":9496} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/perros_matadero_china.jpg" alt="" class="wp-image-9496"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Cerca de 500 perros fueron salvados de ser convertidos en carne</strong> el pasado jueves en China, cuando varias personas detuvieron el camión que los transportaba, enjaulados, de camino al matadero.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>El uso de perros para alimentación es perfectamente legal en China</strong>, por lo que un empresario anónimo pagó unos 60 mil yuanes (9.500 dólares) a los transportistas para impedir que los perros fueran enviados al matadero. Los voluntarios de diferentes asociaciones protectoras de los animales recolectaron más de 50 mil yuanes para pagar los gastos de alimentación y cuidado hasta que los perros encuentren un hogar.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los perros fueron trasladados a un refugio para un examen y su posterior tratamiento médico. Sin embargo,<strong> más de 20 de ellos ya habían muerto a causa de falta de agua y alimentos, o por enfermedades</strong>, durante el largo viaje.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14174} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/perros_china.jpg" alt="Perros rescatados de camino al matadero en China" class="wp-image-14174"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Algunos residentes se han ido acercando al refugio para adoptar a estos animales. <em>“Después de terminar todos los exámenes, los perros sanos serán enviados a nuevas familias, y haremos un seguimiento para asegurarnos de que se les trata bien”</em>, dijo Yao, organizador del refugio.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>“<em><strong>Estos perros han tenido la suerte de salvarse del matadero, pero muchos otros han sido matados y servidos como alimento”</strong></em>, dijo Song Jinzhang, de la Asociación China de Protección de Animales Pequeños.</p>
<!-- /wp:paragraph -->