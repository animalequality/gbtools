<!-- wp:paragraph -->
<p>Esta mañana a las doce menos cuarto, una quincena de activistas de Igualdad Animal han irrumpido en la sección de peletería de El Corte Inglés situado en la plaza de Felipe II con visones despellejados provenientes de granjas peleteras españolas y carteles contrarios a la explotación animal.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta mañana a las doce menos cuarto, una quincena de activistas de Igualdad Animal han irrumpido en la sección de peletería de El Corte Inglés situado en la plaza de Felipe II con visones despellejados provenientes de granjas peleteras españolas y carteles contrarios a la explotación animal.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13756} -->
<figure class="wp-block-image"><img src="/app/uploads/2010/01/actovisones_madrid.jpg" alt="" class="wp-image-13756"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	La acción ha transcurrido sin incidentes y los activistas han permanecido en el interior del centro comercial durante media hora gritando consignas como "Derechos para los animales" o "Piel es Asesinato" y describiendo la vida y muerte de estos animales en las granjas peleteras.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Esta acción de protesta se enmarca dentro de la campaña "Piel es asesinato" que la organización Igualdad Animal está llevando a cabo por toda España. Esta protesta se ha realizado en Valencia, Sevilla, Madrid haciendo de este modo visibles a las víctimas de la peletería.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Cada año, más de cuatrocientos mil visones son víctimas de la industria peletera. Los visones nacen en Abril y permanecen enjaulados hasta Diciembre, mes en el que los granjeros les que les matará asfixiándoles con los gases del tubo de escape de un tractor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Durante el transcurso de su investigación, Igualdad Animal ha entrado en más de veinte de las cincuenta granjas de visones existentes en España, grabando desde el nacimiento hasta la muerte de esos animales. Hemos conseguido grabar también cómo los peleteros despellejan a los animales instalándoles una cámara oculta en sus propias granjas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Igualdad Animal es una organización internacional de derechos animales presente en España, Inglaterra, Perú, Venezuela y Colombia. Rechazamos toda forma de explotación animal, ya sea para vestimenta, alimentación, entretenimiento o experimentación y promovemos el respeto hacia los animales a través de la educación y concienciación social informando, realizando investigaciones, rescates de animales, acciones mediáticas, dando conferencias y talleres, etc.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Web de la investigación en granjas de visones de España:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<a href="http://www.pielesasesinato.com">http://www.pielesasesinato.com</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Vídeo de la investigación:</p>
<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://vimeo.com/7180004"} -->
<figure class="wp-block-embed"><div class="wp-block-embed__wrapper">
https://vimeo.com/7180004
</div></figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->
<p>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Galería fotográfica de la investigación:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<a href="https://www.flickr.com/photos/igualdadanimal/sets/72157622484756111/">http://www.flickr.com/photos/igualdadanimal/sets/72157622484756111/<br>
	<br>
	</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Otras acciones de Igualdad Animal contra la industria peletera:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	23 de Febrero de 2009 - Madrid: Salto a la pasarela Cibeles<br>
	14 de Febrero de 2009 - Madrid: Encadenamiento a un stand peletero de IberPiel con cadáveres de visones<br>
	19 de Diciembre de 2009 - Valencia: Irrupción en la sección de peletería de El Corte Inglés<br>
	9 de Enero de 2010 - Sevilla: Irrupción en la sección de peletería de El Corte Inglés</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<strong>CONTACTOS DE MEDIOS</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Sharon Núñez | Portavoz de Igualdad Animal<br>
	Teléfono: 609 980 196<br>
	Correo electrónico: <a href="mailto:sharonn@igualadanimal.org">sharonn@igualadanimal.org</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Javier Moreno | Coordinador de Activismo de Igualdad Animal<br>
	Teléfono 691 054 172<br>
	Correo electrónico: <a href="mailto:javierm@igualdadanimal.org">javierm@igualdadanimal.org</a></p>
<!-- /wp:paragraph -->