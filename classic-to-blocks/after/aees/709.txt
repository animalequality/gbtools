<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El pasado 29 de Agosto Personas por un Trato Ético a los Animales (PETA) organizaron en la India una llamativa protesta que consistía en que varias/os activistas se "empaquetaron" dentro de bandejas de carne como las que se venden en los supermercados y se exhibieron en la puerta de la Exposición Internacional sobre Ganado y Productos Lácteos de Nueva Delhi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>[img_assist|nid=712|title=Acto de las bandejas en la India|desc=|link=none|align=left|width=200|height=122]El pasado 29 de Agosto Personas por un Trato Ético a los Animales (PETA) organizaron en la India una llamativa protesta que consistía en que varias/os activistas se "empaquetaron" dentro de bandejas de carne como las que  se venden en los supermercados y se exhibieron en la puerta de la Exposición Internacional sobre Ganado y Productos Lácteos, que se realiza en Nueva Delhi. Otras/os  miembros de la organización sostenían carteles que leían: "La carne es asesinato".

Este acto ya se había realizado por la organización Igualdad Animal en la Puerta del Sol de Madrid el día Internacional sin Carne (el 20 de Marzo) con la diferencia de que Igualdad Animal pedía el fin de la utilización de los demás animales como comida, y las/os activistas de PETA reivindicaban mejoras de las condiciones de los animales no humanos antes de su asesinato. 

Desde que Igualdad Animal realizó esta protesta han sido múltiples los grupos y organizaciones que la han repetido, pero no debemos olvidar que esta propuesta se ideó por uno de los fundadores de Igualdad Animal (Javier Moreno) para denunciar la utilización de los animales no humanos como alimento y así promover la dieta 100% vegetariana, y no como una manera de pedir mejoras en la forma en la que los animales no humanos son esclavizados y asesinados ya que esta clase de reivindicaciones no cuestionan la injusticia de la explotación animal sino que confunden centrando el debate en las condiciones en las que las/os no humanas/os son esclavizadas/os. No hay mejores formas de explotar a un individuo, sólo existe una manera de pedir respeto por los animales y esta es rechazar su utilización.</p>
<!-- /wp:paragraph -->