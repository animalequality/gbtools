<!-- wp:image {"id":10227} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/02/15831439803_6d4ca89660_z.jpg" alt="" class="wp-image-10227"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-14330" src="/app/uploads/2015/02/zoopanista3.jpg" style="float:right; margin-left:5px; margin-right:5px; width:450px">La Procuraduría Federal de Protección al Ambiente (Profepa) <strong>ha encontrado cadáveres de 40 animales en congeladores del zoo "El Club de los Animalitos"</strong> del diputado panista Sergio Gómez Olivier.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El subprocurador de Recursos Naturales de la Profepa, Alejandro del Mazo Maza, señaló&nbsp;que entre los ejemplares muertos hay crías de felinos, dos felinos adultos, un lobo canadiense, un antílope, algunos reptiles y ocho aves, entre otros, <strong>lo que calificó de algo grave</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta nueva revelación se da tras la inspección efectuada a la UMA &nbsp;ubicada en Tehuacán, Puebla, y cuyo propietario es el Diputado de Acción Nacional (PAN), Sergio Emilio Gómez Olivier.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El personal de la Profepa acudió a las instalaciones de esta Unidad &nbsp;de Manejo con el propósito de verificar con los responsables de ese zoológico, las condiciones de confinamiento y la legal procedencia de los ejemplares, el registro de la UMA, así como el Plan de manejo &nbsp;de especies y su cumplimiento.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El pasado 29 de enero, la organización internacional Igualdad Animal (IA), con sede en México, <a href="https://igualdadanimal.mx/noticia/2015/02/05/asi-malviven-los-animales-en-el-zoo-del-diputado-sergio-gomez-olivier/">documentó con fotografías</a> y datos sobre la situación de más de 400 animales que habitan en el zoológico “El Club de los animalitos”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En palabras de Dulce Ramírez, coordinadora de Igualdad Animal México, "<em>estamos ante un escándalo que ya habíamos denunciado desde Igualdad Animal. La situación de los animales en el zoo del diputado Gómez Olivier es dramática. Los animales viven hacinados en condiciones deplorables, y ahora sale a luz que guardan los cadáveres de los animales en congeladores, lo que sigue poniendo de manifiesto la vergonzosa gestión del centro. El "Club de los Animalitos" debería ser clausurado. Nuestro gabinete jurídico sigue evaluando el caso para emprender nuevas acciones legales</em>."</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/DeJXTkk2oO4" width="885px"></iframe></div>
<!-- /wp:html -->