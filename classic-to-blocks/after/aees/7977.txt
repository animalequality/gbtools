<!-- wp:image {"id":12644} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/11/topcharity2017.jpg" alt="" class="wp-image-12644"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="http://www.animalcharityevaluators.org/">Animal Charity Evaluators</a> (ACE) es una ONG que evalúa distintas organizaciones de derechos animales y de bienestar animal en EEUU y Europa basándose en su eficacia e impacto sobre los animales. Este año ACE ha elegido a tres organizaciones de un total de 300 analizadas&nbsp;y las ha reconocido como “Top Charity” (Mejor ONG); lo que significa que <strong>las considera como las ONGs más eficientes y con el mayor impacto sobre los animales.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Estamos muy contentos de poder anunciar que I<strong>gualdad Animal ha sido galardonada una vez más con este honor y ha recibido la mejor recomendación posible de ACE.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según ACE,&nbsp;"<strong>Igualdad Animal consigue grandes logros con muy poco dinero, especialmente realizando investigaciones encubiertas con un menor coste en comparación a otras organizaciones. Sus esfuerzos para evaluar y mejorar su trabajo son constantes.&nbsp;Igualdad&nbsp;Animal también tiene programas internacionales particularmente sólidos; han estado activos en varios países durante muchos años y han tenido algunos logros significativos en los países en los que comenzaron a trabajar más recientemente, específicamente en India y Brasil"</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Puedes ver las consideraciones de ACE <a href="https://animalcharityevaluators.org/charity-review/animal-equality/#overview">aquí</a>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>ACE recomienda que las personas que quieran ayudar a los animales donen a Igualdad Animal. Cada donación que recibimos tiene un impacto directo sobre los animales. Si quieres marcar una diferencia en la vida de los animales, considera <a href="http://www.igualdadanimal.org/colabora/donativos.php">donar a Igualdad Animal hoy</a>.</p>
<!-- /wp:paragraph -->