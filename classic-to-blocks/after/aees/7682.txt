<!-- wp:image {"id":11625} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/foto_fb_10.jpg" alt="" class="wp-image-11625"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Harold Brown se crió en una granja familiar y pasó la mitad de su vida dedicado al negocio de la ganadería. Un día, <strong>un evento lo llevó a darle la espalda al modo de vida en que su familia había vivido y trabajado por cinco generaciones</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Por años Harold condujo ganado, hizo castraciones, descornó vacas y también les mató</strong>. Igualmente, trabajó durante tres años en la industria lechera.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Un ataque al corazón a los 18 años lo llevó a replantearse su estilo de vida y, a su vez, su conexión con los animales que por años había explotado y llamado «comida».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>El cambio más profundo que experimentó fue el comenzar a reconocer a los animales como individuos</strong>: «me dí cuenta de que tienen lazos familiares, anhelan la seguridad y experimentan la alegría y la felicidad».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-11624" src="/app/uploads/2017/02/20051207_farmer-brown.jpg" style="float:right; margin:10px; width:350px">Para Harold se trató de elegir la compasión antes de la violencia y replantearse todo aquello en lo que había creído y practicado: «Elegí la vida. Así que no, <strong>en mi experiencia, no hay tal cosa como productos animales humanos, prácticas de cultivo humanas, transporte humano o sacrificio humano»</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Desde entonces <strong>dejó de consumir carne y otros productos de origen animal</strong> y reconoce que esta decisión lo llevó a ser una persona más compasiva.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«Ahora sabía con certeza que, independientemente de las racionalizaciones que había creado, cuando maté a un animal y vi que la luz dejaba sus ojos, al extinguir esa chispa divina, había roto un fideicomiso sagrado».</p></font>

<p>&nbsp;</p>

<p>Harold <strong>es actualmente un convencido defensor de los animales, de <a href="https://igualdadanimal.org/noticia/2016/03/23/las-profundas-consecuencias-de-un-modelo-alimentario-basado-en-vegetales/" target="_blank">un nuevo y mejor modelo alimentario basado en vegetales</a> &nbsp;y de la no violencia</strong>, tal y como lo demuestra en su sitio web Farm kind .</p>

<p>Desde este proyecto anima y ayuda a los ganaderos que quieran dejar atrás el obsoleto sistema de producción de alimentos de origen animal y pasarse a la producción basada en plantas.</p>

<p>Si quieres saber cómo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación. Además, <a href="https://es.loveveg.com/" target="_blank">aquí puedes descargar gratis nuestro nuevo eBook con deliciosas recetas saludables y respetuosas con los animales</a>.</p></font>
<!-- /wp:html -->