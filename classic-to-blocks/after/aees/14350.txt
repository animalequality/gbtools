<!-- wp:paragraph -->
<p>Internacional - El pasado jueves, la Cámara de los Comunes británica aprobó una moción en la que se solicita a Londres prohibir la importación de productos derivados de focas y critica "la matanza (en el 2006) de más de 325.000 focas, la mayoría menores de tres meses de edad, sólo por su piel".

Y en Enero, Bélgica se convirtió en el primer país europeo que prohíbe la importación de todos los productos derivados de la caza de focas. Una organización ecologista dice que el 42% "probablemente estaban conscientes cuando fueron despellejados"

Tras estas decisiones, Canadá ha querido dejar claro su punto de vista, para lo que recientemente ha invitado a un grupo de periodistas europeos.

Su objetivo: dejar claro que acciones como las del Parlamento belga "son erróneas" por estar basadas en desinformación según señaló Loyola Sullivan, embajador de Canadá para la Conservación de las Pesquerías.

Sullivan explicó que Canadá ya no caza focas bebés, denominadas "whitecoats" por su pelaje blanco, desde 1987 y que los animales no son despellejados vivos como alegan algunas de las organizaciones opuestas a la caza de focas. "El 98% de las focas son cazadas de forma humanitaria", añadió Sullivan.

Las cifras no cuadran:

La cifra del 98% está basada en un estudio publicado en el 2002 en la revista de la Asociación Veterinaria de Canadá por varios veterinarios que utilizaron información propia y la recogida en años anteriores por otros grupos.

Pero el Fondo Internacional para el Bienestar de los Animales realizó en 2001 su propio estudio y concluyó que el 42% de los animales examinados "probablemente estaban conscientes cuando fueron despellejados", considera que el estudio está sesgado y que en realidad confirma "muertes inaceptables, inhumanas y crueles".

Mientras los científicos no se ponen de acuerdo, los cazadores de focas expresan su ira cuando son calificados como "bárbaros".

"Esta es nuestra forma de vida, nuestro patrimonio, nuestra forma de sobrevivir. No quiero tener que abandonar mi hogar porque no tengo trabajo y no puedo cazar focas", declaró Richard Gillet, un pescador de la pequeña localidad de Twillingate, en el norte de Terranova.

Más que nunca es el momento de entender que la oposición a la matanza de focas no debe centrarse en que la mayoría de las asesinadas sean bebes, mueran cuando todavía son conscientes o se las asesine "solo" por su piel, la oposición a semejante masacre debe nacer del rechazo a la muerte y al sufrimiento de otro, independientemente de su especie o de donde, como o porque sea asesinado.

Fuente - 20 minutos</p>
<!-- /wp:paragraph -->