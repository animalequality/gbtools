<!-- wp:paragraph -->
<p><img alt="" class="wp-image-13366" src="/app/uploads/2018/10/2279946892_06ba457446_z.jpg" style="float:left; margin-left:10px; margin-right:10px; width:258px">A lo largo de 2009 se mataron en España once millones setenta mil corderos, lo que supuso casi un millón y medio menos de animales que en 2008, según la Encuesta Mensual de Sacrificio de Ganado del Ministerio de Medio Ambiente y Medio Rural y Marino.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El descenso a nivel estatal fue de un 11,82 por ciento con respecto a 2008. Por comunidades, Asturias fue la que experimentó una mayor bajada en la matanza de corderos con un 47,25 por ciento respecto al año anterior.</p>
<!-- /wp:paragraph -->