<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10262" src="/app/uploads/2015/07/sira.jpg" style="float:right; margin:5px; width:450px">Amber Canavan, de 29 años, se declaró culpable a principios de este mes a un cargo menor de traspaso criminal en segundo grado. Todo se deriva de una noche en 2011 cuando ella y un compañero activista no identificado entró en la granja de foie gras Hudson Valley en Ferndale y grabó un video para denunciar el maltrato al que estaban sometidos los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Al hacer la declaración de culpabilidad, Canavan estaba obligada a explicar lo que hizo y vio en la granja. Eso le dio la oportunidad de testificar en una audiencia pública al respecto.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"Yo declaré en la corte, bajo juramento, las cosas horribles que se realizan a estos animales", dijo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En el video que Canavan publicó, el narrador dice que dos patos fueron rescatados, lo que hizo que se la denunciara por robo. En declaraciones posteriores, también dijo que ella entró en una granja abierta para grabar el vídeo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"Yo sabía que estaba tomando un riesgo cuando hablé sobre lo que vi", dijo a The New York Times en febrero, "Y si proporciona algún alivio para los patos que están todavía allí, habrá valido la pena".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hudson Valley Foie Gras es el principal productor del país. "Ella es un ladrón", Marcus Henley, gerente de operaciones de la compañía de foie gras, le dijo al New York Times.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los activistas que apoyan Canavan tienen una visión diferente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"Lo que sucede detrás de la puerta de esta granja se consideraría crueldad extrema por la mayoría de la gente", dijo Ashley Byrne, de PETA.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Canavan tiene una larga historia de activismo y protesta en Syracusa.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Canavan ha dicho que una vez salga de la cárcel, continuará con su activismo animalista, haciendo uso de su licenciatura en estudios ambientales. "Voy a ser un activista toda mi vida", dijo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="498px" width="885px"><iframe allowfullscreen="" frameborder="0" height="498px" src="https://www.youtube.com/embed/-qwQZXtWyeg" width="885px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->