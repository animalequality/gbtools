<!-- wp:paragraph -->
<p>En nuestra sección de Artículos hemos publicado dos nuevos artículos de la autora Joan Dunayer sobre la explotación de los peces. "Los peces: Sensibilidad más allá de la comprensión del captor" y "Los Peces y la Industria Cárnica" aportan una valiosa información y perspectiva sobre las capacidades y padecimientos de estos seres.

Esperamos que este trabajo ayude a que <a href="https://igualdadanimal.org/problematica/peces/">los peces</a> sean respetados como merecen y dejemos de utilizarles.</p>
<!-- /wp:paragraph -->