<!-- wp:image {"id":11247} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/shutterstock_127005695_-_copia.jpg" alt="" class="wp-image-11247"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Aplicar un impuesto a la carne y los lácteos salvaría medio millón de vidas al año y reduciría las emisiones de gases de efecto invernadero de forma decisiva para no alcanzar los 2ºC de calentamiento global.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Gravar con un impuesto la carne de bovino un 40% y los lácteos un 20% compensaría <strong>por el daño que su producción causa a los consumidores por motivos de dietas insalubres y de cambio climático</strong>. Así lo ha calculado <a href="http://www.nature.com/nclimate/journal/vaop/ncurrent/full/nclimate3155.html" target="_blank">un estudio</a> de la Universidad de Oxford, que proyecta que los impuestos evitarían que la población consumiese tanta cantidad de estos productos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La producción de carne y lácteos es responsable de un cuarto de las emisiones de <a href="https://igualdadanimal.org/noticia/2016/04/29/los-consumidores-tenemos-el-poder-de-ayudar-los-animales-y-al-planeta/" target="_blank">gases de efecto invernadero que están causando el cambio climático</a>. El problema sigue aumentando porque los segmentos de población mundial que alcanzan mayor poder adquisitivo elevan exponencialmente el consumo de carne global.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11248} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/11/shutterstock_306441431_0.jpg" alt="" class="wp-image-11248"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El director del estudio, Marco Springmann, del <strong>Programa Oxford Martin para el futuro de la alimentación</strong>, declara que «es algo ya sabido que si no hacemos algo para parar las emisiones que generan las industrias alimentarias, no tendremos ninguna opción de limitar el calentamiento global por debajo de los 2ºC».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Pero si tienes que pagar un 40% más por tu chuleta, puede que elijas comerla una vez a la semana en vez de dos», añadía.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los investigadores que han llevado a cabo el estudio global han tenido en cuenta las diferencias de consumo entre distintas regiones, así como las proyecciones de crecimiento de poder adquisitivo. También han tenido en cuenta las regiones con menos riqueza, <strong>para que el impuesto no supusiese carencias alimentarias en ninguna población</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«Los autores del estudio opinan que el dinero recaudado de los impuestos podría destinarse a asegurar que las poblaciones tuviesen acceso a alimentos más saludables».</p></font>

<p>&nbsp;</p>

<p>Hasta ahora, <a href="https://igualdadanimal.org/noticia/2015/11/26/un-comite-de-expertos-avisa-los-gobiernos-comer-menos-carne-es-la-unica-manera-de/" target="_blank">los cambios en la producción de los alimentos que generan más calentamiento global</a>, como la carne, han sido ignorados por los gobiernos, en gran medida <strong>debido a la sensibilidad de la población sobre sus opciones alimentarias</strong>. También por miedo a aumentar la pobreza en aquellos países menos favorecidos y la ausencia de ideas sencillas para afrontar el problema.</p>

<figure><img alt="" class="wp-image-11249" src="/app/uploads/2016/11/shutterstock_214341322_0.jpg" style="float:left; margin:10px; width:450px"></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Sin embargo, los autores del estudio opinan que el dinero recaudado de los impuestos podría destinarse <strong>a asegurar que las poblaciones tuviesen acceso a alimentos más saludables</strong>. Hay experiencias exitosas en este sentido, como en México, en el que un impuesto por exceso de azúcar a los refrescos fue destinado a un programa para abastecer de agua potable a las escuelas.</p>

<p>Además, la reducción del consumo debido al impuesto reduciría el tratamiento de <a href="http://www.cronicajalisco.com/notas/2016/68002.html" target="_blank">las enfermedades que el excesivo consumo de carne y lácteos producen</a>, como cáncer, infartos y otras enfermedades coronarias. <strong>En EE. UU., por ejemplo, la población consume tres veces la cantidad recomendada de carne</strong>.</p>

<p>Springmann, por su parte, nos recuerda a todos <strong>una realidad </strong>que está ya presente en nuestras vidas: «o incrementamos el calentamiento global y las enfermedades coronarias, obesidad y diabetes, o hacemos algo sobre nuestro sistema alimentario».</p>

<p><strong>Si quieres estar al tanto de cómo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación</strong>.</p>

<p>&nbsp;</p>

<p>Fuente: <a href="https://www.theguardian.com/environment/2016/nov/07/tax-meat-and-dairy-to-cut-emissions-and-save-lives-study-urges" target="_blank">https://www.theguardian.com/environment/2016/nov/07/tax-meat-and-dairy-to-cut-emissions-and-save-lives-study-urges</a></p></font>
<!-- /wp:html -->