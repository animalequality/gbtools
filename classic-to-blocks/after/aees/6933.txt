<!-- wp:image {"id":14274} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/02/sturup_astrazeneca2.jpg" alt="" class="wp-image-14274" title="Presencia policial en el aeropuerto de Sturup ante la protesta de los activistas | Foto: Patrick Persson"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Una veintena de activistas de la ONG&nbsp;<a href="https://djurrattsalliansen.se/" target="_blank">Djurrattsalliansen (Alliance for Animal Rights)</a>&nbsp;acudieron al aeropuerto de Sturup con la intención de impedir en la noche de ayer, martes 26 de febrero,&nbsp;el inminente <strong>envío de los perros</strong> de las instalaciones de Astra Zeneca en Örkelljunga a laboratorios de Reino Unido.<br>
	<br>
	Los perros fueron transportados desde Örkelljunga en dos vehículos que llegaron al aeropuerto escoltados por la policía y otros agentes de seguridad. Pese a que los activistas trataron de impedir el acceso de los vehículos a la zona de embarque, fueron reducidos por las fuerzas de seguridad y hubieron de presenciar impotentes la carga de los perros en dos aviones para su envío.<br>
	<br>
	<em>«Podíamos oír los perros gimiendo y ladrando de sus jaulas y sus voces mostraban ansiedad, terror y miedo.»</em></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13785} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/02/astrazeneca_avion.jpg" alt="" class="wp-image-13785" title="Los operarios cargan a los perros en el avión"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>«Los raspones que sentíamos contra nuestras mejillas tras ser arrastrados por el asfalto, el dolor que sentíamos en los brazos después de que la policía nos sujetara con fuerza... el dolor no era nada comparado con la<strong> tristeza desesperada que sentimos en nuestros corazones en el momento que los motores del avión comenzaron a sonar</strong> y el personal de tierra dio luz verde para que despegara.»</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Uno de los manifestantes fue detenido</strong> tras entrar en la zona de acceso restringido del aeropuerto mientras trataba de impedir que los aviones de <a href="http://www.aeronova-cargo.com/" target="_blank">Aeronova </a>&nbsp;y <a href="http://www.binair.eu/en_home.php" target="_blank">Binair&nbsp;</a>despegasen. A las pocas horas el activista fue puesto en libertad.<br>
	<br>
	La perrera de Örkelljunga contaba con unos 400 perros. El número de animales enviados fuera del país hasta la fecha es desconocido.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div style="color:#ccc;font-size:11px;font-family:Arial,sans-serif;margin-left:20px;">
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph -->
<p>►MÁS INFORMACIÓN SOBRE ESTA CAMPAÑA <a href="https://igualdadanimal.org/noticia/2013/02/01/campana-para-salvar-cientos-de-perros-de-un-laboratorio-tras-su-cierre-en-suecia/" target="_blank">AQUÍ</a></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:image {"id":9891} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/02/astrazeneca_beagles.jpg" alt="" class="wp-image-9891"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong style="text-align: justify;"></strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13786} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/02/astrazeneca_beagle.jpg" alt="" class="wp-image-13786"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Puedes ver fotografías actuales del criadero <a href="http://www.djurrattsalliansen.se/inrikes/lat-astrahundarna-leva.html" target="_blank">aquí</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Puedes seguir la campaña en Suecia en: &nbsp;<a href="https://www.facebook.com/Djurrattsalliansen" target="_blank">www.facebook.com/Djurrattsalliansen</a></p>
<!-- /wp:paragraph -->