<!-- wp:paragraph -->
<p>Beyoncé apuesta de nuevo por una alimentación veggie y anunció que otra vez está siguiéndola para estar más saludable y en forma. La «single lady» y su esposo Jay-Z han dado a conocer previamente su interés por llevar una alimentación basada en proteínas vegetales porque es bueno para todos y también para el planeta.

Hace unos días, la artista usó su cuenta de instagram para anunciar a sus 112 millones de seguidores que actualmente está llevando una alimentación vegana y que deberían acompañarla. Además de los múltiples beneficios que trae a la salud, Beyoncé ha sacado todos los productos de origen animal de su alimentación para poder estar en condiciones física óptimas durante y después de su preparación para sus presentaciones en el festival «Coachella» en abril de este año.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12919} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/captura_de_pantalla_2018-03-06_a_las_6.22.31_0.png" alt="" class="wp-image-12919"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

Según ella, alimentarse de forma vegana le ha hecho ser más consciente de todos los beneficios que este tipo de alimentación aporta a su salud. Y no ha estado sola en esto, ya que fue junto a su esposo Jay-Z que desde 2013 crearon un plan de comidas basadas en proteínas vegetales con la asesoría de Marcos Borges, un bestseller del New York Times, entrenador personal de las estrellas y promotor de una alimentación vegana.

Desde entonces, la pareja comenzó a hablar sobre los beneficios de una dieta vegana y por qué la promueven: «Queremos desafiarte &nbsp;en la misma medida en que nos desafiamos a nosotros mismos para avanzar hacia un estilo de vida más basado en proteínas vegetales y reconocer que estás cuidando tu salud y la del planeta».

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;</p>
<!-- /wp:paragraph -->