<!-- wp:image {"id":12407} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/36872831736_bc3824121b_z.jpg" alt="" class="wp-image-12407"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La organización internacional Igualdad Animal ha llevado al Parlamento Europeo en Bruselas su denuncia sobre la ganadería industrial con una exposición titulada "Ganadería Industrial: ¿el mayor crimen de nuestro tiempo?"</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12409} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/36872831736_bc3824121b_z_0.jpg" alt="" class="wp-image-12409"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font color="#808080">«Y si nos atrevemos a actuar desde ahora mismo, sé que viviremos para ver el fin de la ganadería industrial. Y seremos capaces de mirar a nuestros nietos a los ojos y decirles: "Sí, lo hicimos mal, pero nos esforzamos por enmendarlo".»</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font size="1"><font color="#808080">- Toni Shephard, directora de Igualdad Animal en Inglaterra.</font></font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12406} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/36664379670_e98e403f6d_z.jpg" alt="" class="wp-image-12406"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><font size="5"><font size="1"></font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font size="1">&nbsp;</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La exposición, que cuenta con imágenes de investigaciones que ha realizado la ong animalista en granjas de España, Inglaterra o Alemania, ofrece también al espectador la posibilidad de vivir en primera persona el maltrato que padecen estos animales a través del proyecto de realidad virtual "<a href="https://ianimal.es/" target="_blank">iAnimal</a>".<br>
<br>
Ayer martes 5 de septiembre, el eurodiputado Stefan Eck, del Grupo Confederal de la Izquierda Unitaria Europea / Izquierda Verde Nórdica&nbsp;(GUE/NGL), presentó la exposición en un evento en el que participaron entre otros, el eurodiputado de Equo Florent Marcellesi y la directora de Igualdad Animal en Inglaterra Toni Shephard.&nbsp;<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><font size="5"><font size="1">&nbsp;</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«La ganadería industrial no solo es la responsable del mayor maltrato animal de la historia, sino que es la principal causa de la destrucción del planeta. Es urgente que los europarlamentarios aborden esta problemática, y por eso hemos traído a Bruselas la cruda realidad que viven estos animales», indicó Javier Moreno, cofundador de Igualdad Animal</p>
<!-- /wp:paragraph -->