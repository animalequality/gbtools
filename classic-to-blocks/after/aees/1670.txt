<!-- wp:paragraph -->
<p>Tarragona- Según informa El Periódico.com, unos 18.000 pollos murieron al incendiarse una “granja” situada en Alcanar (Motsià). Una dotación de bomberos no pudo evitar que la “granja” ardiera completamente con los animales no humanos en su interior.

El fuego se declaró sobre las 6:49 en la “granja”, una construcción de unos 1200 metros cuadrados hecha con bloques de hormigón y elementos de fibra de vidrio. En ella miles de aves vivieron una vida de esclavitud para acabar en el matadero. La últimas murieron en el incendio, siendo contabilizadas como simples pérdidas económicas.</p>
<!-- /wp:paragraph -->