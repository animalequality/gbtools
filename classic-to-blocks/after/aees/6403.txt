<!-- wp:paragraph -->
<p>La muerte de una jirafa la semana pasada en el mayor <a href="http://www.igualdadanimal.org/entretenimiento/zoos" target="_blank">zoológico </a>de Indonesia puso en evidencia, una vez más, la terrible situación que padecen los animales en estos centros de reclusión. Tras la autopsia se encontró <strong>una gran bola de plástico</strong>, formada por los envoltorios de las bolsas de comida de los visitantes, <strong>en el estómago del animal fallecido</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En este <strong>zoológico de Surabaya</strong> los tigres están completamente demacrados y unos 180 pelícanos malviven amontonados en un pequeño espacio en el que ni siquiera pueden desplegar las alas. Además, los animales recluidos crían de forma incontrolada, faltan fondos para su alimentación y cuidado e incluso varios de los miembros de su personal están involucrados en el tráfico de animales en la zona.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El zoológico fue objeto de una investigación hace dos años después de que unos informes revelaran que <strong>morían, de media, 25 animales al mes</strong>. Este centro de explotación animal mantiene actualmente a <strong>4.000 animales cautivos entre sus muros</strong>.</p>
<!-- /wp:paragraph -->