<!-- wp:paragraph -->
<p>El pasado sábado, un Mozo de Escuadra (policía autonómica de Cataluña) mató a un chimpancé que escapó, junto con otros, de la reserva Fundación Mona, en Riudellots de la Selva (Gerona). Se trataba de Pancho, que fue abatido a tiros por los Mozos de Escuadra cuando se encontraba a pie de la carretera que atraviesa el municipio de Riudellots y se dirige a Cassà de la Selva. Los otros dos chimpancés que habían conseguido escaparse, Marco y Charly, fueron detenidos y devueltos a sus dormitorios.

Pancho era un chimpancé muy tranquilo, aunque su corpulencia impresionaba. Era un chimpancé que siempre colaboraba con los cuidadores, nunca se peleaba con los demás chimpancés y su interés principal era la comida. Se pasaba la mayor parte del tiempo comiendo ya que cuando fue utilizado en el circo y en el mundo publicitario le hicieron pasar mucha hambre para que así actuara de la manera que se esperaba.

Pancho llegó a Riudellots de la Selva a principios del 2001. Fue intervenido a un señor de la Comunidad Valenciana que lo había entrado de forma ilegal en el país. Pancho fue explotado en el mundo de la publicidad siendo contratado en anuncios publicitarios de marcas tan conocidas como McDonald's.


Fuente: 20minutos.es</p>
<!-- /wp:paragraph -->