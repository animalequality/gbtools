<!-- wp:paragraph -->
<p>Francia, Alemania y Reino Unido han condenado la decisión islandesa de reanudar la captura de ballenas con fines comerciales porque infringe la moratoria internacional que prohíbe la captura de estos animales para tales fines.

A pesar de esta reciente condena, lo cierto es que Islandia ya cazaba ballenas de todos modos con anterioridad ya que la Comisión Ballenera Internacional (CBI), creada en 1946 para regular la caza de ballenas, aprobó en 1986 una moratoria que impide capturarlas salvo para "fines científicos". La cuota de "caza científica" permite que se maten un máximo de 200 ballenas minke entre 2003 y 2007, lo que claramente demuestra que el motivo de dicha moratoria no es evitar el asesinato de individuos concretos sino en la posible extinción de una determinada especie.  

Desde 1985 Islandia ha capturado ballenas amparándose en la denominada caza científica, la misma excusa utilizada por otros países como Japón y Noruega para infringir las leyes internacionales que prohíben la caza comercial.</p>
<!-- /wp:paragraph -->