<!-- wp:html -->
<div>
	<span class="Apple-style-span" style="color: rgb(66, 76, 94); font-family: Arial, Helvetica, sans-serif; line-height: 14px; -webkit-border-horizontal-spacing: 1px; -webkit-border-vertical-spacing: 1px; ">El gobierno venezolano autorizó la cantidad de 10 millones 200 mil de dólares para la adquisición de 6 mil vientres bovinos doble propósito, con mestizaje pardo suizo y holstein aptos para la producción de leche y carne a través de un convenio de cooperación entre la República de Nicaragua y Venezuela.&nbsp;<br>
	<br>
	El presidente de la Confederación Nacional de Ganaderos y Agricultores de Venezuela (Confagan), José Agustín Campos, explicó que esta cantidad de "ganado" en pie fortalecerá la producción de carne y leche en el país.&nbsp;<br>
	<br>
	Aseguró que paralelamente a esta adquisición, el Gobierno Nacional está trabajando en profundizar en las líneas genéticas de éstos animales. Entre las medidas adoptadas destacan la importación de semen y de embriones, las cuales ayudarán a masificar y a refrescar el ganado existente, para así disminuir el déficit de bovinos en el país.&nbsp;<br>
	<br>
	“Es sumamente positivo la aplicación de estos programas orientados a fortalecer principalmente a los pequeños y medianos productores nacionales, para en un futuro no muy lejano ser independientes y contar con una verdadera soberanía alimentaria”, acotó Campos.&nbsp;<br>
	<br>
	El productor explicó que el "ganado" de mestizaje pardo suizo y holstein, tendrá la responsabilidad de aportar una importante cantidad de leche y carne.&nbsp;<br>
	<br>
	“Es una de las mejores razas capaces de aportar una gran cantidad de alimento”, dijo el máximo representante de Cogafan.&nbsp;<br>
	<br>
	Para tal fin, estos 6 mil vientres de bovinos deberán adaptarse al tópico venezolano, desde el punto de vista genotípico, fenotípico y físico.&nbsp;<br>
	<br>
	Indicó que en el país, existe alrededor de 13 millones 500 mil cabezas de" ganado", los cuales aportan cerca de 4 millones de litros de leche diario.&nbsp;<br>
	<br>
	Campos estima que gracias a la adquisición de estas 6 mil nuevas "reses", aunado a las políticas agrícolas y ganaderas que adelanta el Gobierno Revolucionario, esta cifra se incremente consecutivamente durante este año 2010.&nbsp;<br>
	<br>
	La adquisición de estos bovinos se ejecutará a través de la empresa mixta Albanista, y la cancelación será mediante un programa de compensación económica enmarcado en el acuerdo de cooperación energética entre ambos países.</span></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Fuente: abn.info.ve</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Para más información acerca de la vida de los animales utlizados dentro del ámbito de la alimentación visita:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<span class="Apple-style-span" style="color: rgb(17, 17, 17); font-family: 'Lucida Grande', 'Lucida Sans Unicode', verdana, geneva, sans-serif; font-size: 13px; line-height: 18px; ">&nbsp;<br>
	<a href="https://igualdadanimal.org/" style="color: rgb(0, 0, 0); text-decoration: underline; " title="http://granjasdeesclavos.com">http://granjasdeesclavos.com</a><br>
	<a href="http://investigacionesanimales.org/" style="color: rgb(0, 0, 0); text-decoration: underline; " title="http://investigacionesanimales.org">http://investigacionesanimales.org</a><br>
	<a href="http://mataderos.info/" style="color: rgb(0, 0, 0); text-decoration: underline; " title="http://mataderos.info">http://mataderos.info</a></span></p>
<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://vimeo.com/igualdadanimal"} -->
<figure class="wp-block-embed"><div class="wp-block-embed__wrapper">
https://vimeo.com/igualdadanimal
</div></figure>
<!-- /wp:embed -->

<!-- wp:embed {"url":"https://www.flickr.com/photos/igualdadanimal/collections/72157604117908835/"} -->
<figure class="wp-block-embed"><div class="wp-block-embed__wrapper">
https://www.flickr.com/photos/igualdadanimal/collections/72157604117908835/
</div></figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->
<p>
	&nbsp;</p>
<!-- /wp:paragraph -->