<!-- wp:paragraph -->
<p>Un espeluznante vídeo subido a internet muestra cómo se celebra el “novio de montería”, una broma macabra que se acostumbra a hacer a los <strong>niños y adolescentes que matan por primera vez </strong>a un animal considerado de "caza mayor".</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14145} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/01/novio_monteria.jpg" alt="" class="wp-image-14145"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En el vídeo podemos ver a <strong>un niño atado a un palo</strong> y cómo varios adultos le embadurnan la cabeza con pimentón y harina a la vez que le restriegan por la cara un trozo de piel del animal muerto. Los asistentes ríen y preguntan entusiasmados dónde está la cabeza del venado. Entonces<strong> la cabeza decapitada del ciervo es colocada sobre la del niño</strong>, entre las risas y excitación del público. Las <strong>vísceras </strong>del ciervo también son usadas para manchar de sangre la cara del niño.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Finalmente, y mientras cantan al niño el “cumpleaños feliz”, las <strong>orejas cortadas</strong> el ciervo son puestas sobre la cabeza del niño, a modo de diadema. El padre del menor posa orgulloso con su hijo ante las cámaras de los asistentes.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14146} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/01/novio_monteria_venado.jpg" alt="" class="wp-image-14146"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El vídeo fue grabado en una finca cordobesa y subido hace unos días a YouTube por uno de los presentes en la montería. Tras recibir cientos de críticas <strong>el vídeo fue eliminado</strong> por el autor de las imágenes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->