<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10408" src="/app/uploads/2016/01/circoberlin.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px">Tres trabajadores del <strong>Circo Berlín</strong>, un circo que utiliza animales en sus «<em>espectáculos</em>», <strong>han sido condenados por agredir brutalmente a cinco activistas de Igualdad&nbsp;Animal</strong>. <a href="https://igualdadanimal.org/noticia/2013/01/06/tres-activistas-de-igualdad-animal-agredidos-por-trabajadores-del-circo-berlin/">Los hechos tuvieron lugar en la ciudad de Málaga en el 2013</a>, mientras los activistas realizaban un acto informativo en las proximidades del circo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Mientras los defensores de los animales repartían información entre los viandantes, el gerente del circo, que responde a las iniciales F.B., se aproximó a la activista que portaba un megáfono <strong>agrediéndola y rompìendo el aparato violentamente</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cuando los demás compañeros acudieron para intentar socorrerla, un grupo <strong>de hasta 30 trabajadores del Circo Berlín</strong> se abalanzaron con violencia sobre ellos rompiéndoles las cámaras y robándoles los móviles con los que intentaban grabar la agresión y llamar a la policía.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Uno de los agresores <strong>propinó un cabezazo en la nariz</strong> a uno de los pacíficos activistas; otros sufrieron puñetazos y golpes en la cabeza y el cuerpo. Todos ellos tuvieron que ser atendidos en el hospital.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Además del gerente del circo, dos trabajadores más, F.D. y J.M.O. <strong>han sido igualmente condenados por faltas de lesiones</strong>. El Circo Berlín, por si fuera poco, había denunciado a los defensores de los animales por supuesta «coacción» contra los intereses del espectáculo. <strong>Todos ellos han sido absueltos</strong>, ya que, según la sentencia, «<em>estaban amparados por la libertad de expresión</em>».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El abogado de los activistas durante el procedimiento fue <a href="https://www.abogadodeanimales.com/">Daniel Dorado</a>, presidente del Centro Legal para la Defensa de los Animales. También colaboró en la defensa el abogado Daniel Amelang.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La violencia de los trabajadores del circo hacia quienes nos oponemos al uso de animales en sus denigrantes espectáculos viene siendo ya habitual. Nos complace comprobar cómo la justicia <strong>se pone del lado de la libertad de expresión</strong> y castiga a quienes están dispuestos a llegar a las actitudes más violentas para defender <a href="https://igualdadanimal.org/noticia/2015/11/06/honduras-se-une-los-paises-que-prohiben-animales-en-circos-y-peleas-de-perros/">un modelo de espectáculo que tiene los días contados</a>.</p>
<!-- /wp:paragraph -->