<!-- wp:image {"id":10110} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/01/sao_paulo.jpg" alt="" class="wp-image-10110"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>El Gobierno regional de Sao Paulo prohibió el uso de animales en pruebas de cosméticos, perfumes y productos de higiene personal. Esta decisión una se produce tras dos meses de debate en el estado por esta práctica, han informado hoy fuentes oficiales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-14257" src="/app/uploads/2014/01/saopaulo.jpg" style="width: 300px; float: left; margin: 10px;">En noviembre pasado, unos 100 activistas entraron en el Instituto Royal en Sao Paulo y rescataron a 178 beagles que estaban siendo utilizados para experimentos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Este acto tuvo repercusión internacional y generó un gran debate en la opinión pública brasileña.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>“<em>Hicimos un profundo y amplio estudio del proyecto de ley. Entendemos que la materia debería ser de ámbito nacional, eso sería lo ideal</em>”, ha afirmado Alckmin en un discurso en el Palacio de los Bandeirantes, sede del Gobierno regional en Sao Paulo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No se sabe todavía a cuantos animales va a beneficiar la nueva ley, cuya fiscalización será efectuada por la Secretaría Regional de Salud.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En caso de violación de la ley las empresas pueden ser castigadas con una multa de hasta un 1 millón de reales (unos 421.585 dólares).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/OuzZ2CpaGP4" width="420"></iframe></p>
<!-- /wp:paragraph -->