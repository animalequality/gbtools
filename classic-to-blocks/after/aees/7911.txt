<!-- wp:image {"id":12467} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/shutterstock_95001850_0.jpg" alt="" class="wp-image-12467"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Jen y Tray Federici eran dueños de una granja muy productiva dedicada básicamente a criar vacas para enviarlas al matadero. El negocio era rentable y todo parecía ir genial, hasta que un día se dieron cuenta de que no podían seguir dedicándose a hacer eso.<br>
<br>
Ocurrió que un día decidieron que solo comerían la carne de animales que ellos mismo hubiesen matado, pero llegado el momento no fueron capaces de matar a una vieja vaca que habían seleccionado de su rebaño. El hecho los sacudió de tal forma que <strong>se vieron cuestionando tanto su forma de alimentarse como su trabajo</strong>. Y a partir de &nbsp;entonces sus vidas y las de los animales que criaban cambiaron para siempre.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
<br>
<font size="5"><font color="#808080">«Decidimos seguir adelante no como consumidores de animales sino como sus embajadores».</font></font></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5">

<p><br>
<br>
«Oficialmente, comimos nuestro último jamón de navidad en 2016. Nunca nos retractamos. Juramos no volver a comer carne ni otro producto de origen animal», dice Jen Federici. Y para el verano de este año convirtieron su rancho en un refugio de animales sin fines de lucro.<br>
&nbsp;</p>

<figure><img alt="" class="wp-image-12468" src="/app/uploads/2017/09/refugio_animales_cabra.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px"></figure><p></p>

<p><br>
&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>FOTO ©The Surf and Turf Sanctuary</p>

<p>&nbsp;</p>

<p>En <a href="https://www.facebook.com/pg/thesurfandturfsanctuary/photos/?ref=page_internal" target="_blank">The Surf and Turf Sanctuary</a> conviven terneros, cerdos, cabras, pollos y cebras junto a otros animales rescatados, todos lejos del sufrimiento y la muerte de las granjas y mataderos.<br>
<br>
<img alt="" class="wp-image-12469" src="/app/uploads/2017/09/refugio_animales.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px"></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><br>
Tray y Jen aseguran que <strong>desde que llevan una alimentación basada en vegetales nunca se habían sentido mejor </strong>en toda su vida o más saludables tanto física como mentalmente. Y en cuanto a los animales que dejaron de enviar al matadero y que ahora protegen declaran: «Al igual que ningún humano, ningún animal quiere morir. Son seres increíbles y conmovedores. Creemos que cada animal merece la vida que le pertenece».<br>
&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>FOTO: ©The Surf and Turf Sanctuary</p>

<p>&nbsp;</p>

<p>Fuente: http://vegnews.com/articles/page.do?pageId=10110&amp;catId=1</p></font>
<!-- /wp:html -->