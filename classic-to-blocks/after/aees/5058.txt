<!-- wp:paragraph -->
<p>Guadalajara- Según informa 20 Minutos.es, durante este fin de semana continuó la matanza de gallinas en la zona de Almoguera, en Guadalajara, llegando a la cifra de 400.000 aves muertas.

Las autoridades decidieron matar a todas las gallinas de la zona como medida preventiva tras la muerte de 2.000 gallinas en pocos días en la granja Segura.

La ministra de agricultura, Elena Espinosa, declaró que “'Es un caso de la cepa H7, que no tiene nada que ver con gripe aviar” y que “Se trata de una enfermedad animal, que no tiene impacto sobre el consumo humano, aunque sí puede causar pérdidas económicas para la explotación”.

Estos cientos de miles de gallinas han sido matadas, algunas incineradas vivas, debido a las posibles pérdidas económicas que podrían sufrir algunos granjeros. Como siempre, las vidas de los demás animales solo son valoradas en función del beneficio que se pueda obtener de su explotación. Estas gallinas, después de pasar una vida horrible, explotadas por la industria del huevo, han tenido que sufrir además una muerte horrible, aunque no podemos evitar que desde que nacieron estaban condenadas a morir en un matadero cuando fuese el momento más rentable para su explotador.</p>
<!-- /wp:paragraph -->