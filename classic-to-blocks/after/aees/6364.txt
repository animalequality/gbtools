<!-- wp:image {"id":9384} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/03/ley_utah.jpg" alt="" class="wp-image-9384"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Un legislador de Utah ha fijado su objetivo en los defensores de los animales intentando crear una ley que castigue con <strong>duras penas </strong>el hecho de<strong> tomar fotografías, vídeos o grabaciones de sonido </strong>en granjas privadas o explotaciones agrícolas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La legislación del congresista John Mathis, R-Bernal, convierte en un delito menor de clase A "la intromisión en una explotación agrícola" si alguien "a sabiendas o intencionalmente" hace grabaciones en la propiedad sin previo consentimiento. Una infracción posterior se convierte en un delito de tercer grado según la <a href="http://le.utah.gov/~2012/bills/hbillint/hb0187.htm" target="_blank">HB187</a>.<br>
	<br>
	“Creo que esto alcanza otro nivel ya que se trata de terrorismo doméstico” afirma el congresista Lee Perry, R-Perry, que habló largo y tendido sobre la necesidad de endurecer los cargos más allá del allanamiento a aquéllos que los infrinjan.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"Uno toma estas fotografías, saca a la luz las imágenes, y durante el proceso puede dañar seriamente a las empresas afectadas", dijo Perry.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dijo que estas acciones ocurren tanto en Utah como en el resto del país y el resto del mundo. Mencionó que tras una búsqueda rápida en Youtube encontró el <a href="https://vimeo.com/21627412" target="_blank">vídeo</a>&nbsp; que el Equipo de Investigaciones de <a href="https://igualdadanimal.org/" target="_blank">Igualdad Animal</a> sacó a la luz sobre el proceso de matanza de visones, donde se muestran imágenes espeluznantes de operarios lanzando a los visones a un contenedor, así como fotografías de cadáveres de visones después de ser gaseados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"Lo que yo veo es que ellos van allí con la intención de irritar a los animales para poder obtener una grabación que haga parecer la situación peor de lo que es, o van y hacen cualquier fotografía", dijo Perry, tratando de quitar importancia a los vídeos e imágenes obtenidas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La directora ejecutiva de la Alianza de Defensa de los Animales de Utah, Anne Davis, reconoció que esta medida puede ser demasiado extrema. Davis denunció la HB187, pues le parece demasiado genérica y amplia en su aplicación. “<strong>Si un niño se va de excursión a una granja y hace fotos “intencionadamente” ¿se le procesaría? </strong>“se preguntaba Davis, citando ese ejemplo en una audiencia legislativa anterior esa semana.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Davis también dijo que el equiparar el acto de tomar fotografías en una propiedad privada con el de torturar a un animal de compañía lanza un mensaje erróneo a los defensores que trabajaron durante 13 años para conseguir que la “Ley Henry” se publicara.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>“Lo que realmente hace es tratarlo como si no fuera nada” afirma Davis. “Realmente trivializa el esfuerzo”.<br>
	<br>
	El proyecto de ley ha sido autorizado por la House Law Enforcement (Cámara de Legisladores)&nbsp; y el Comité de Justicia Criminal y está en espera ser sometido a mayor estudio en el pleno de la Cámara.</p>
<!-- /wp:paragraph -->