<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10395" src="/app/uploads/2016/01/toni.jpg" style="float:left; margin-left:10px; margin-right:10px; width:450px">En Igualdad Animal estamos muy contentos de anunciar la incorporación a nuestro equipo como directora&nbsp;en Inglaterra de&nbsp;una&nbsp;defensora de los animales brillante y experimentada: Toni Shephard.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Con Toni en nuestro equipo, los animales han ganado la profesionalidad y el trabajo incansable de una mujer que dedica su vida a ellos. Ampliamente cualificada, Toni es una inspiración para todos los que la rodean.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Toni es una amante de los animales declarada&nbsp;y siempre ha sentido la necesidad de defenderlos contra la crueldad. Durante su adolescencia, comenzó a colaborar en las campañas cuando se enteró sobre el sufrimiento causado por la industria de la piel y la vivisección. Rápidamente dejó de comer carne y comenzó una alimentación&nbsp;vegetariana, ampliando su conocimiento de las formas en que las diferentes industrias dañan animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Su primer contacto con el activismo fue cuando se unió a un grupo local que realizaba protestas fuera de las tiendas de piel y zoos - acuarios donde las ballenas y los delfines vivían encerrados. Ambas campañas lograron su objetivo, ¡varias tiendas peleteras y el zoo- acuario cerraron!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Poco después de estas protestas se unió a PETA y pasó muchos años haciendo campañas contra diferentes tipos de maltrato a los animales en todo el mundo. Durante este tiempo organizó y coordinó muchas campañas, incluyendo las primeras protestas contra el uso de pieles en Rusia, Hong Kong y Tokio. También organizó eventos para crear conciencia en las escuelas de todo el Reino Unido e Irlanda. Ella ha sido portavoz de PETA en los medios de comunicación, manifestaciones y debates.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Toni está familiarizada&nbsp;con la investigación, ha filmado en&nbsp;muchas granjas de pieles y granjas donde los animales son criados para la alimentación, documentando&nbsp;las condiciones inhumanas que estos animales tienen que&nbsp;soportar día tras día.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Toni pasó varios años con el grupo de difusión de vegetarianismo Viva!, denunciando&nbsp;los horrores de la ganadería&nbsp;industrial y ayudando&nbsp;a las personas a sustituir la carne en su alimentación. Más recientemente, se unió a la Liga Contra los Deportes Crueles, trabajando para denunciar el sufrimiento que millones de animales salvajes sufren como resultado de la caza con perros de caza, trampas y la lucrativa industria de las armas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Algunos de los espectaculares éxitos en la trayectoria&nbsp;de Toni como jefa&nbsp;de campañas y protestas han ayudado a lograr la prohibición de las granjas peleteras en el Reino Unido, la cancelación de un programa de la NASA para enviar primates al espacio, las investigaciones que consiguieron el cierre de una de las mayores granjas de cerdos en Inglaterra, así como la empresa más grande de las explotaciones porcinas en Escocia y el compromiso de miles de personas para reducir el consumo de carne o sustituirla por completo en su dieta. Impresionante, ¿verdad?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Durante su trabajo a cargo de todas estas campañas, además, ha obtenido un título en Comportamiento Animal y un doctorado en Psicología centrado en el comportamiento animal. Toni dijo que «<em>esto me ha ayudado a defender a los animales de manera más eficiente, porque ahora tengo una mejor comprensión del sufrimiento emocional y conductual que soportan&nbsp;cuando no se les permite ser libres. Pero también tengo una mejor comprensión de la psicología humana, lo que me permite entender los&nbsp;cambios en nuestro comportamiento</em>».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En las palabras de nuestra&nbsp;nueva&nbsp;directora en Inglaterra, «<em>estoy emocionada&nbsp;de unirme a Igualdad Animal,&nbsp;donde puedo combinar estas dos facetas de mi vida: trabajar para una organización que denuncia los horrores que la ganadería&nbsp;industrial nos oculta,&nbsp;y que también hace uso de las últimas investigaciones en el campo de la conducta humana&nbsp;para asegurar que sus campañas y mensajes sean tan eficientes como sea posible. Una organización líder e innovadora&nbsp;acostumbrada&nbsp;a dedicar tiempo para evaluar su&nbsp;trabajo y adaptarlo para que tenga el mayor&nbsp;impacto. Una organización que ayuda a las personas a ser compasivas con los animales y dar pasos para ayudarlos</em>».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Toni, en Igualdad Animal estamos emocionados de contar contigo. Todo el equipo de Igualdad Animal está muy entusiasmado con tu&nbsp;incorporación.</p>
<!-- /wp:paragraph -->