<!-- wp:paragraph -->
<p>Este fin de semana se ha celebrado la LXX Subasta Nacional de Ganado Selecto en el Centro de Transferencia Tecnológica de Colmenar Viejo, organizada por la Dirección General de Agricultura y Desarrollo Rural, perteneciente a la Consejería de Economía y Consumo, según informó hoy el Gobierno de Esperanza Aguirre.

El director general de Agricultura y Desarrollo Rural, Luis Sánchez, abrió la subasta pública de los animales participantes.

Se ha contado con un total de toros y vacas de las siguientes razas: Avileña-Negra Ibérica (8), Limusina (9), y Charolesa (17).

Además, a modo de exposición han participado toros y vacas de las razas Blonda de Aquitania (5), Asturiana de los Valles (4), Berrenda en Negro (4), Berrenda en Colorado (2) y Rubia Gallega (2). Por último y, en venta libre, ha habido 20 animales de raza Limusina y dos de Blonda de Aquitania.

Con objeto de fomentar la selección y mejora genética (eugenesia) de razas puras, la Comunidad de Madrid publicó la Orden 427/2007, de 26 de febrero, de la Consejería de Economía y Consumo, por la que se convocan ayudas a la participación en certámenes de ganado de raza pura, que se celebren en la Comunidad de Madrid durante el año 2007.

Los beneficiarios de estas ayudas son los titulares de cárceles ganaderas situadas en España y que formen parte del Registro General de Explotaciones Ganaderas, las asociaciones de esclavistas de razas puras o sus federaciones, las cooperativas ganaderas y las sociedades agrarias de transformación.

Las actividades sujetas a ayuda son la participación en certámenes ganaderos nacionales e internacionales que se celebren en la Comunidad de Madrid y la adquisición de ganado en subastas y concursos-subastas nacionales que se celebren en la región. La cuantía máxima de estas subvenciones es de 15.000 euros por raza y certamen y hasta 760 euros por animal.


Fuente: Servimedia</p>
<!-- /wp:paragraph -->