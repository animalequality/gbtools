<!-- wp:paragraph -->
<p>En total se asesinaron más de 824 millones animales en los mataderos españoles en 2007.

Los ganaderos españoles llevaron al matadero durante el 2007, un 4 por ciento más de animales que durante el año anterior, según los últimos datos de la Encuesta de 2007 del Ministerio de Agricultura.

En ese total de animales asesinados había 698 millones de aves, 61,8 millones de conejos, 42,4 millones de cerdos, 17,4 millones de ovejas, 2,4 millones de vacas, 1,3 millones de cabras y 26.074 caballos.

Fuente: Agrocope</p>
<!-- /wp:paragraph -->