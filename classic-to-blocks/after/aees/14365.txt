<!-- wp:paragraph -->
<p>Herencia-Según informa ABC, la Protectora de la ciudad de Herencia, Ciudad Real, se verá obligada a cerrar el próximo día 20 de junio, por orden del ayuntamiento de dicha ciudad, el cual procederá al asesinato de todos los animales que no puedan ser dados en adopción.

La humilde protectora no tiene medios materiales ni económicos suficientes para llevarse los animales abandonados que han recogido durante años en el municipio. Desde la protectora se ha pedido una reunión con el Alcalde de Herencia para intentar buscar una solución.</p>
<!-- /wp:paragraph -->