<!-- wp:paragraph -->
<p>Madrid - Transformar una casa en una galería no es un trabajo fácil. Verónica Ibarra y Carlos Thomas lo han hecho posible. Estos dos artistas veganos lo tuvieron claro: si quieres que ocurra algo, Hazlo Tú Mismo. Y así es como se han liado la manta a la cabeza con una de las propuestas más arriesgadas e innovadoras que ha visto Madrid en mucho tiempo: NI MÚSCULOS NI SECRECIONES.

Recuperan el espíritu Do It Yourself y unifican el esfuerzo y creación de 16 artistas del Reino Unido, Estados Unidos y España. ¿Qué tienen todos ellos en común? Sus obras llenan las paredes, suelos y techos de un espacio hasta ahora vacío, y además, todos ellos son veganos.

NI MÚSCULOS NI SECRECIONES es el primer encuentro internacional y multidisciplinar de artistas veganos de una magnitud importante que se celebra en todo el mundo. Y es solo el principio. Tras Madrid habrá NEITHER MUSCLES NOR SECRETIONS en ciudades como Londres, Berlín o Nueva York. 

El centro de Madrid albergará las obras de Kate Corder (Reino Unido), Allison Cole, Ray Frenden, Colin Matthes, Julie A. McConnell, Travis Nichols, Eric Redetzke, Heather May Redetzke, Maggie Suisman, Ashley Watson, Matt Page y Jt Yost (EE.UU.) y Verónica Ibarra, Javier R. Rosell, Sierra y Carlos Thomas (España), desde el día 10 de mayo, hasta el 16, desde las 12 de la mañana hasta las 10 de la noche ininterrumpidamente y totalmente gratis.

Pintura, vídeo, fotografía, escultura, impresión, dibujo e instalación bajo un mismo techo con la intención de abrir tu curiosidad y mostrarte un evento que no deberías dejar que nadie te contara.


FIESTA DE INAUGURACIÓN de la  Exposición: Ni Músculos Ni Secreciones
El Jueves 10 de mayo a partir de las 20h
Música y buffet vegano.
Entrada libre!!!

Ni Músculos Ni Secreciones es una exposición de arte colectiva internacional que que tendrá lugar en Madrid del 10 al 16 de mayo. La fiesta de inauguración  será el día 10 de mayo a las 20h.

Más información: 
<a href="http://www.nimusculosnisecreciones.com">http://www.nimusculosnisecreciones.com</a>

LUGAR
C/ San Bernardo, 51 piso 2c Madrid
Metro Noviciado / Plaza de España

HORARIO 
10-16 Mayo 2007
Horario: 12-22h todos los días 
entrada libre
CONTACTO
nmns(@)nimusculosnisecreciones.com

TIENES MYSPACE? AGREGANOS! 
myspace.com/nimusculosnisecreciones

SPONSORS

LA BOUTIQUE VEGETARIANA
VEGANVEGAN 
SHEESE</p>
<!-- /wp:paragraph -->