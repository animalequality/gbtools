<!-- wp:paragraph -->
<p><img alt="" class="wp-image-9649" src="/app/uploads/2012/08/veterinario_iva.jpg" style="width: 220px; float: left; " title="Veterinario atiende a gallinas rescatadas por Igualdad Animal">La subida del IVA (Impuesto sobre el Valor Añadido) anunciada por el Gobierno para el 1 de septiembre afectará muy especialmente al cuidado de los animales de otras especies. Los servicios veterinarios, considerados hasta la fecha como servicios básicos sujetos al tipo reducido de IVA (8%), <strong>pasarán a tributar como cualquier servicio de carácter general (21%)</strong>, lo que significa que quienes llevemos a un animal a un centro veterinario para su revisión, tratamiento, intervención quirúgica o vacunación <strong>veremos incrementada la factura en un 13% después del 31 de agosto</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se establece además, a partir de esta fecha, un <strong>“doble rasero” para la aplicación de los impuestos</strong> correspondientes a estos servicios, ya que toda intervención veterinaria realizada en favor de titulares de explotaciones agrícolas, forestales o ganaderas sólo se verá incrementado en un 2%, pasando del 8% al 10%. <strong>Explotar a otros individuos resultará, por tanto, más barato que solidarizarse con ellos y prestarles ayuda</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Es de esperar que este incremento repercuta muy negativamente sobre los santuarios de animales y asociaciones protectoras, organizaciones cuyas facturas veterinarias suponen un elevado porcentaje de sus gastos y que no cuentan con más ayuda económica que las aportaciones de socios y donativos puntuales.</p>
<!-- /wp:paragraph -->