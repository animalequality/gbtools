<!-- wp:paragraph -->
<p>Orense-Según informa La Voz de Galicia, la empresa Coren, dedicada a criar, esclavizar y asesinar a miles de pollos, ha sido premiada con la certificación “Freedom Food”, concedida por la Royal Society for Cruelty to Animals (RSPCA).

Coren ha recibido esta certificación por que sus “productos” provienen de animales recogidos, transportados y asesinados de acuerdo con los estándares de bienestar establecidos por la RSPCA. 

Desde Coren constatan que hasta hace algunos años apenas existía preocupación entre los consumidores por las condiciones de vida y alimentación de los animales que luego llegaban a la mesa, si bien en la actualidad esa visión ha cambiado. Las medidas bienestaristas como las aplicadas por Coren, consiguen calmar la conciencia de los consumidores y aseguran los beneficios de las empresas explotadoras, perpetuando la esclavitud de millones de animales no humanos.

Se suele pensar que los demás animales solo tienen interés en evitar el sufrimiento. Independientemente del hecho de que es imposible llevar a cabo una cría, transporte y asesinato que evite cualquier sufrimiento, los demás animales desean seguir vivos y ser felices, igual que cualquiera de nosotros.</p>
<!-- /wp:paragraph -->