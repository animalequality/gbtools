<!-- wp:image {"id":9567} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/06/cabra_desfiladero.jpg" alt="" class="wp-image-9567"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La Consejería de Ganadería, Pesca y Desarrollo Rural, a petición de los alcaldes de la mancomunidad de Liébana, realizó <strong>una batida que acabó con la vida de 18 cabras </strong>en el <a href="https://es.wikipedia.org/wiki/Desfiladero_de_La_Hermida" target="_blank">desfiladero de La Hermida</a>, aludiendo desprendimientos de piedras sobre la calzada.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Las autoridades justifican la matanza porque</strong> se había observado una mayor presencia de cabras a lo largo del trazado del desfiladero<em> “que <strong>están provocando movimientos de piedras y desprendimientos sobre la calzada</strong>, con el consiguiente peligro para los vehículos y personas que por allí circulan”.</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A raíz de este hecho, los ganaderos denuncian que se ha matado a varias cabras que tenían crotal identificativo que acreditaba que tenían “<a href="http://www.igualdadanimal.org/articulos/gary-francione/animales-como-propiedad" target="_blank">dueño</a>”. <em>“<strong>Es una injusticia lo que han hecho</strong>”</em>, apostilla uno de ellos.<em> “No estamos en contra de que se maten los animales de la ladera que da a la carretera, que crean un peligro por los desprendimientos de piedras, pero es que han matado también los del otro lado del río que, si tiran piedras, no hacen daño a nadie. Ha sido una masacre”</em>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13829} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/06/cabras_desfiladero.jpg" alt="" class="wp-image-13829" title="Cabras muertas al otro lado del desfiladero"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Este hecho ha sido rebatido por la Consejería aduciendo que el crotal que exhibían las cabras era del año 2009 y no había sido renovado por el vigente desde marzo de 2010, <em>“por lo que hace al menos tres años que no se ha revisado el estado sanitario de estos animales”</em>. Por otro lado, el Director General de Ganadería señaló que <em>“<strong>Es cierto que algunas cabras estaban en el lado contrario</strong>, a 1.062 metros de la carretera. Pero las cabras no son estatuas, se desplazan. En diez minutos recorren esa distancia”</em>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>El mismo día</strong> y a la misma hora en la que se dio muerte a las cabras, se autorizó al pueblo de Bejes hacer una <strong>batida de lobos</strong> <em>“que estaba preparada”</em>.&nbsp;Los ganaderos entienden esta coincidencia <em>“como que han querido decirnos, que <strong>si nosotros matamos lobos, ellos pueden matar las cabras</strong>”</em>. También se preguntan si <em>“van a hacer lo mismo con los jabalíes o los corzos que tampoco tienen crotal, que sanitariamente no están controlados, y que andan por los riscos del desfiladero”</em>.</p>
<!-- /wp:paragraph -->