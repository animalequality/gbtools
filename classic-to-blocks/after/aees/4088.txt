<!-- wp:paragraph -->
<p>Un juez condenó hoy a cinco y seis años de cárcel a dos hombres que en septiembre de 2007 mantuvieron secuestrado a un perro durante un mes y exigieron a sus dueños más de 330.000 dólares por su rescate, informaron fuentes judiciales.

La medida afecta a Ángel Egidio Cárdenas Ávila y Wilmer Orozco, hallados responsables del secuestro del perro Aldo.

La investigación estableció que Cárdenas Avila y Orozco fueron las personas que contactaron a los dueños, enviaron fotografías de Aldo, e incluso grabaron sus ladridos y los hicieron llegar como pruebas.

Aldo fue rescatado el 18 de octubre pasado en una veterinaria tras una operación de la policía, que hizo varios rastreos.

Los dos secuestradores de Aldo fueron detenidos en la misma acción de las autoridades por agentes policiales que simularon ser emisarios de los dueños para entregar el dinero del rescate, tras un intercambio de disparos en un sector del sur de Bogotá.

Fuente: Terra Actualidad - EFE</p>
<!-- /wp:paragraph -->