<!-- wp:paragraph -->
<p>Este mundo no es un buen lugar si eres un animal en una granja industrial. Toda tu vida está llena de momentos terribles. De hecho, si llegas a nacer, tu vida estará estudiada de principio a fin. Tus necesidades no importarán, sólo lo hará que des beneficios. Y sin embargo los animales de granja son seres sensibles y afectuosos. Si se les permite, nos muestran sus asombrosas formas de ser.

Queremos que los animales de granja no sean maltratados. Queremos que puedan correr, disfrutar y sentir el sol y el aire en libertad. Estas son las fotos del mundo que queremos en comparación con el mundo en el que viven. Ellos son millones y millones, repartidos por oscuras y atestadas granjas industriales. ¿Tendremos en cuenta sus sentimientos?

<strong>1. Cerditos (I)</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13868} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/10/cerdoscontraste2.jpg" alt="" class="wp-image-13868"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>Un cerdito rescatado de una granja industrial por <a href="https://igualdadanimal.org/">Igualdad Animal</a> comparado a un pobre cerdito que no tuvo su suerte.</em><strong>2. Cerditos (II)</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13867} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/10/cerdoscontraste.jpg" alt="" class="wp-image-13867"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>La placidez reflejada en un cerdito comparada con la tristeza de otro en una granja industrial.</em><strong>3. Gallinas</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14188} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/10/pollloscontraste2.jpg" alt="" class="wp-image-14188"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>Una gallina en un matadero mira la pared aterrorizada en comparación con gallinas que Igualdad Animal rescató de una granja industrial en Alemania.</em><strong>4. Pollos (I)</strong><strong><img class="wp-image-14186" style="margin: 10px; width: 885px;" src="/app/uploads/2015/10/pollitoscontraste3.jpg" alt=""></strong><em>Un pequeño y frágil pollito descansa en el calor de una mano en comparación con otro que murió en una granja industrial.</em><strong>5. Pollos (II)</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14185} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/10/pollitoscontraste1.jpg" alt="" class="wp-image-14185"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>Un niño y un pollito se observan en un momento de sus respectivas infancias. En contraste, un pequeño pollito a punto de caer en una trituradora que acabará brutalmente con su vida. [Fotografía derecha de Jan van Ijken]</em><strong>6. Terneras</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14315} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/10/vacascontraste.jpg" alt="" class="wp-image-14315"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Una curiosa ternerita olisquea a una compañera en el refugio de animales de granja Edgar’s Mission en Australia. En comparación una interminable hilera de terneras separadas de sus madres y enjauladas en una granja industrial.

<strong>7. Corderos</strong><strong><img class="wp-image-13914" style="margin: 10px; width: 885px;" src="/app/uploads/2015/10/corderoscontraste.jpg" alt=""></strong><em>Un sensible corderito rescatado por Igualdad Animal* en comparación con otro siendo forzado a entrar en el matadero. </em><em>*Fotografía Jo-Anne McArthur para Igualdad Animal </em></p>
<!-- /wp:paragraph -->