<!-- wp:image {"id":9602} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/07/granjasfoiegras.jpg" alt="" class="wp-image-9602"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La organización de derechos animales Igualdad Animal presenta una <a href="https://igualdadanimal.org/actua/foie-gras" target="_blank">nueva investigación </a>en la que <strong>pone al descubierto a la industria del foie gras en Cataluña</strong>. Este trabajo se enmarca dentro de una investigación más amplia, a nivel estatal, que la organización ha llevado a cabo durante un año y que irá mostrando durante los próximos meses.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Equipo de Investigaciones de Igualdad Animal ha realizado un pormenorizado estudio de la situación del sector, detectando que hasta la fecha los datos de producción de foie gras en registros o estadísticas oficiales estaban totalmente desactualizados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los investigadores visitaron cinco granjas en la primera etapa de la investigación, entre las cuales se encuentra la de mayor producción de toda la comunidad autónoma y que provee directamente a la empresa del presidente de Interpalm, la asociación de productores españoles de foie gras.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>► Advertencia: Este vídeo contiene imágenes de extrema violencia hacia los animales.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="435" mozallowfullscreen="" scrolling="no" src="https://player.vimeo.com/video/45216589?title=0&amp;byline=0&amp;portrait=0" webkitallowfullscreen="" width="580"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Visita: <strong><a href="https://igualdadanimal.org/actua/foie-gras" target="_blank">www.GranjasDeFoieGras.org</a></strong></h4>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Hallazgos de la investigación</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Los activistas del Equipo de Investigaciones de Igualdad Animal han sido testigos de estremecedoras escenas de sufrimiento físico y psicológico en todas las granjas visitadas. Documentaron, mediante <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157630427261184/" target="_blank">más de 350 fotografías inéditas</a> y decenas de horas de vídeo y audio, la dramática realidad que padecen los patos sometidos a alimentación forzada para la producción de foie gras en Cataluña:</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Patos que llegaban conscientes al momento de degüello, aleteando y pataleando sin cesar mientras se desangraban.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Animales encerrados en diminutas jaulas individuales, donde ni siquiera podían darse la vuelta.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Patos con claros indicios de estrés y depresión.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Animales con evidentes problemas respiratorios.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Patos debilitados a los que se dejaba morir sin atención veterinaria.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Animales muertos dentro de las jaulas individuales.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>A un operario pisando el cuello de un pato con la trampilla de una caja.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>A operarios tratando bruscamente a los patos durante su transporte.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>A un operario estresando a propósito a un pato como forma de diversión.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Patos que se movían con dificultad debido al volumen de su hígado.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">*Petición finalizada. Gracias a todos los que habéis colaborado.</h4>
<!-- /wp:heading -->

<!-- wp:html -->
<style id="badge-styles" type="text/css"><!--/*--><![CDATA[/* ><!--*/

/* You can modify these CSS styles */
        .vimeoBadge { margin: 0; padding: 0; font: normal 11px verdana,sans-serif; }
        .vimeoBadge img { border: 0; }
        .vimeoBadge a, .vimeoBadge a:link, .vimeoBadge a:visited, .vimeoBadge a:active { color: #3A75C4; text-decoration: none; cursor: pointer; }
        .vimeoBadge a:hover { color:#00CCFF; }
        .vimeoBadge #vimeo_badge_logo { margin-top:10px; width: 57px; height: 16px; }
        .vimeoBadge .credit { font: normal 11px verdana,sans-serif; }
        .vimeoBadge .clip { padding:0; float:left; margin:0 10px 10px 0; line-height:0; }
        .vimeoBadge.vertical .clip { float: none; }
        .vimeoBadge .caption { font: normal 11px verdana,sans-serif; overflow:hidden; width: auto; height: 30px; }
        .vimeoBadge .clear { display: block; clear: both; visibility: hidden; }
        .vimeoBadge .s160 { width: 160px; } .vimeoBadge .s80 { width: 80px; } .vimeoBadge .s100 { width: 100px; } .vimeoBadge .s200 { width: 200px; }	
/*--><!]]>*/
</style>
<!-- /wp:html -->