<!-- wp:image {"id":9470} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/fiona_oakes_vegan.jpg" alt="" class="wp-image-9470"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Fiona Oakes</strong>, <strong>atleta <a href="http://www.vivevegano.org" target="_blank">vegana</a></strong><a href="http://www.vivevegano.org" target="_blank"> </a>ganadora de varios maratones y responsable de un santuario en Inglaterra en el que viven cientos de animales, <strong>está corriendo en estos momentos el durísimo ultramaratón de Des Sables</strong> que tiene lugar cada año <strong>en el desierto del Sahara</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta prueba deportiva es considerada <strong>la más dura de cuantas se celebran en el mundo</strong>, y Fiona es la primera mujer vegana en afrontarla. Y lo está haciendo a pesar de las heridas, ampollas y de tener algunos dedos de los pies rotos. <strong>Lo está haciendo por los animales,</strong> corriendo con una camiseta de Vegan Runners, derribando así los mitos existentes sobre el veganismo y consiguiendo fondos para el santuario <a href="https://www.towerhillstables.org/" target="_blank">Tower Hill Stables Animal Sanctuary</a> que dirige&nbsp; y donde más de 400 animales tienen su hogar.<br>
	<br>
	“<strong>No estoy haciendo esto por mí. Estoy haciendo esto para los animales</strong>. Estoy haciendo esto <strong>para promover un estilo de vida vegano</strong> ético y sano. Estoy haciendo esto para derribar los mitos, la información incorrecta y los estereotipos que son ampliamente difundidos acerca del veganismo. Estoy haciendo esto para transmitir el mensaje vegano al público más amplio de un modo positivo que permita a las masas identificarse con él", afirma Fiona.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/9iqS4Dy9Azg" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El ultramaratón de Des Sables se realiza durante 6 días, en los cuales los participantes han de recorrer <strong>240 Km por el desierto </strong>mientras cargan con todo su equipo, llevando consigo sus objetos personales y sus propios alimentos que, dado que ninguna de las comidas concentradas habituales en estos eventos es vegana, suponen para Fiona un extra de peso en su equipo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La prueba es retransmitida en 200 países a través de más de 1.000 canales de televisión y multitud de páginas web. Puedes seguir los últimos detalles de esta prueba en su <a href="http://www.darbaroud.com/index.php" target="_blank">aquí</a>.</p>
<!-- /wp:paragraph -->