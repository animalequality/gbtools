<!-- wp:paragraph -->
<p><img alt="" class="wp-image-9413" src="/app/uploads/2012/03/incendio.jpg" style="width: 220px; float: left; ">Los bomberos del Gobierno de Navarra sofocaron en el día de ayer un incendio en una granja de cerdos del término municipal de El Mosquete, después de que éste provocara la muerte de los cerca de dos mil cerdos que se encontraban en el interior.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El fuego, cuyas causas aún se desconocen, afectó al aislante de la cubierta y provocó su derrumbe, haciendo caer consigo instalaciones de pienso y agua.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La investigación del incendio está siendo realizada por la Guardia Civil.</p>
<!-- /wp:paragraph -->