<!-- wp:paragraph -->
<p>Madrid - Con pancartas en las que aparecían fotos de animales muertos y se leían frases como "Basta de asesinatos" y "Stop especismo", el grupo Igualdad Animal protestó contra el consumo de carne en la calle.

Fuente - ADN
<a href="http://www.diarioadn.com/media/0000031500/0000031938.pdf">Link versión digital del diario ADN</a>

Igualdad Animal realiza acciones a favor del vegetarianismo cada fin de semana en Madrid y otras ciudades.Nuestra organización dedica gran parte de sus recursos a esta cuestión ya que es el ámbito de explotación donde más animales sufren y mueren. Si quieres participar escríbenos a <a href="mailto:info@igualdadanimal.org">info@igualdadanimal.org</a></p>
<!-- /wp:paragraph -->