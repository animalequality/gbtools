<!-- wp:paragraph -->
<p>El trabajo de toda una vida se calcinó en menos de media hora. A las diez de la mañana de ayer, Verín (Ourense) se paralizó ante una densa y negra humareda que cubría el cielo. Una granja de pollos de As Tuelas, entre las localidades de Queizás y Cabreiroá, estaba ardiendo en su totalidad. Un fuego, causado probablemente por un cortocircuito, que se inició en la parte trasera de la granja y se extendió con toda celeridad por el tejado de la infraestructura, de poliuretano y uralita.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Cinco minutos antes mi padre había estado dentro limpiando, vino hasta casa, y al salir ya vio el humo y el fuego», reconoce Rubén Cid, hijo del propietario. Rápidamente, hasta el lugar se desplazaron numerosos vecinos así como efectivos del parque de bomberos comarcal, Grumir de Verín, Guardia Civil, Policía Local y Policía Nacional. No se pudo hacer nada. «No pudimos salvar nada, todo fue muy rápido», asegura el joven. En el interior de la granja había aproximadamente 28.000 pollos pequeños, de unos 16 días, que perecieron al momento, solo se salvaron unos siete.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Además, de la pérdida de los animales, la granja sufrió numerosos desperfectos como consecuencia del fuego y humo, aunque no se vio afectada la estructura principal. Esta empresa verinense se dedica desde hace treinta años a la cría de pollos. «Trabajamos para Coren, que nos traen los animales, los tenemos entre 45 y 50 días aquí para criarlos y luego se los recogen», afirmaba Cid, reconociendo que «ahora no nos queda más remedio que volver a empezar».</p>
<!-- /wp:paragraph -->