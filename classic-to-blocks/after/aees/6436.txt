<!-- wp:image {"id":9448} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/04/pitbulls_filipinas_0.jpg" alt="" class="wp-image-9448"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La policía filipina encontró el pasado fin de semana a <strong>200 perros de raza pitbull</strong>, gravemente heridos, que estaban siendo utilizados en peleas retransmitidas on-line. Estaban en muy malas condiciones, <strong>atados a bidones y desnutridos</strong>, en una granja de café al sur de Manila.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En tres incursiones separadas la noche del viernes, la policía arrestó a ocho surcoreanos sospechosos de operar<strong> apuestas ilegales a través de internet</strong>, en donde jugadores de diferentes países iban apostando por los perros que participaban en las peleas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/f9H24qqs7Rk" width="420"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La falta de instalaciones para la rehabilitación de estos perros supondrá su <strong>muerte en los próximos días</strong>, según informó Ana Cabrera, de la Sociedad de Bienestar Animal de Filipinas, que detalló que los perros que sobrevivían eran "reciclados", puestos nuevamente a pelear. "Ese es un destino peor que la muerte", dijo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Algunos perros ya fueron rescatados en diciembre pasado en una instalación similar en la provincia de Cavite.</p>
<!-- /wp:paragraph -->