<!-- wp:paragraph -->
<p>Hong Kong- Una enfermedad se ha extendido en dos ciudades chinas del sur del país, infectando al menos a 1.300 cerdos y matando a más de 300 de ellos, según recogen este lunes distintos medios de comunicación de Hong Kong.

La enfermedad comenzó a matar cerdos en la ciudad de Yunfu, enclavada en la provincia de Guangdong, a principios del mes de abril. Según revela el diario Ming Pao Daily News, que cita a fuentes del Gobierno, la misteriosa enfermedad se ha cobrado ya la vida de 300 cerdos. Otro periódico, Apple Daily, asegura que este mal ha matado ya al 80 por ciento de la población de cerdos de Yunfu. Los síntomas de la enfermedad son la pérdida del apetito, altas fiebres y gran pérdida de sangre.

Fuente: Terra</p>
<!-- /wp:paragraph -->