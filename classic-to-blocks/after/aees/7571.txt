<!-- wp:paragraph -->
<p><strong>«No al maltrato animal» </strong>es la nueva campaña de Igualdad Animal en el metro de Caracas. Por primera vez se muestra en Venezuela el <a href="https://igualdadanimal.org/blog/bienvenido-al-matadero-un-viaje-al-lugar-que-la-industria-carnica-no-te-va-mostrar/" target="_blank">maltrato sistemático</a> al que son sometidos los animales en granjas industriales y mataderos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Durante 6 meses, <strong>más de 2 millones de usuarios</strong> diarios del metro de Caracas conocerán la realidad que las industrias de alimentación nos oculta. <strong>Más de 100 paneles</strong> han sido instalados a lo largo de la red de metro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>La iniciativa anima a los consumidores<strong> a incluir más opciones vegetales</strong> en sus platos.</p></font>

<p>&nbsp;</p>

<p><strong>«¿Por qué amamos a unos y nos comemos a otros?»</strong>, <a href="https://igualdadanimal.org/noticia/2016/03/29/por-que-amamos-perros-y-gatos-y-nos-comemos-vacas-cerdos-y-pollos/" target="_blank">nos cuestiona</a> una de las instalaciones que puede verse en los andenes del subterráneo y que confronta a <strong>un perro y un ternero</strong>, instando al observador a replantear su visión de ambos.</p>

<figure><img alt="" class="wp-image-11051" src="/app/uploads/2016/09/29525431705_be249c8e88_k.jpg" style="float:left; margin:10px; width:450px"></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Parte de las imágenes de <strong>la inédita campaña</strong> han sido obtenidas por Igualdad Animal <a href="https://ianimal.es/" target="_blank">en sus múltiples y extensivas investigaciones</a> alrededor del mundo durante sus 10 años de trabajo.</p>

<p>«Dentro de las granjas y mataderos el maltrato es la norma. Con esta campaña queremos que el consumidor tenga acceso a la terrible realidad que oculta la industria ganadera para que así pueda tomar decisiones compasivas con los animales. El resultado que estamos obteniendo es impresionante», declara Carolina Gil Castaldo, directora de Igualdad Animal en Venezuela.</p>

<figure><img alt="" class="wp-image-11050" src="/app/uploads/2016/09/28900207404_26928ef13f_k.jpg" style="float:left; margin:10px; width:450px"></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>La iniciativa anima a los consumidores <strong>a incluir más opciones vegetales</strong> en sus platos. Con esta simple y saludable medida podemos ayudar a disminuir nuestro impacto en el planeta. La industria cárnica es <strong>un factor principal en la emisión de gases de efecto invernadero</strong>, así como de un consumo alarmante de recursos naturales y deforestación de selvas para producir pasto para los animales.</p>

<p>Gobiernos de todo el mundo están instando a sus ciudadanos <strong>a reducir el consumo de carne</strong> debido a las causas medioambientales mencionadas. Por otra parte, los consumidores cada vez demandan más información sobre cómo es producida la comida que consumen. El maltrato animal es cada vez <a href="https://igualdadanimal.org/blog/por-que-la-ganaderia-industrial-es-la-mayor-causante-de-maltrato-animal-de-la-historia/" target="_blank">menos tolerado</a> por los consumidores.</p>

<p>Es por esto que la reducción del consumo de carne y las alimentaciones con alternativas a la carne <a href="https://igualdadanimal.org/noticia/2016/02/27/un-estudio-de-mercado-revela-que-los-productos-alternativos-la-carne-experimentaran-un/" target="_blank">son una tendencia a nivel mundial</a>.</p>

<p><strong>Si quieres estar al tanto de cómo ayudar a los animales, <a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación</strong>.</p></font>
<!-- /wp:html -->