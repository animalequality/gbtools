<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><big><b>Detenido un cazador de Toledo por ahorcar a tres galgos.</b></big></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->
<h5 class="wp-block-heading"> 
	(13/03/2011)</h5>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p> 
<a href="http://www.igualdadanimal.org/entretenimiento/caza" target="_blank"><img alt="" class="wp-image-13994" src="/app/uploads/2011/03/galgo-y-liebre.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 318px; "></a>Un cazador de Fuensalida (Toledo) ha sido detenido por la Guardia Civil por haber ahorcado a tres galgos de su <a href="http://www.igualdadanimal.org/articulos/gary-francione/animales-como-propiedad" target="_blank"><strong>propiedad</strong></a>. Los agentes recibieron el aviso del hallazgo de varios perros semienterrados a las afueras de esta localidad y tras practicarles una necropsia se determinó que habían muerto ahorcados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Gracias a que dos de ellos tenían incorporado un microchip se pudo localizar a su dueño, quien no tuvo inconveniente en reconocer durante el interrogatorio que había ahorcado a los animales porque es «costumbre» en su pueblo acabar con los <a href="http://www.igualdadanimal.org/entretenimiento/caza" target="_blank"><strong>perros de caza</strong></a> que ya no sirven por edad o por problemas físicos una vez finaliza la temporada cinegética. Incluso no tuvo reparos en añadir que era mejor matarlos que abandonarlos a su suerte.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El cazador fue inmediatamente detenido y se someterá a un juicio en el Juzgado de Torrijos (Toledo) por un delito de maltrato animal. Según afirmó ayer el portavoz de la Guardia Civil en Castilla-La Mancha, José Luis González Capilla, «afortunadamente estos casos son puntuales, aunque hace 20 años era una práctica normal ahorcar a los galgos en las vallas de los vertederos. El trabajo del Seprona y la mayor concienciación de los galgueros han erradicado este mal hábito».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Lejos de esa afirmación, según cálculos aproximados de perreras, refugios y otras protectoras, en España se abandonan unos 40.000 galgos al año, especialmente, al sur de Madrid, Castilla La-Mancha, Castilla y León y Andalucía.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> 
<img alt="Foto de la web de &quot;Hermanos Aguilera&quot; criadores de galgos. En la imagen se muestra claramente que la caza (como el especismo) es una cuestión cultural y educacional." class="wp-image-13995" src="/app/uploads/2011/03/galgos-aguilera.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: right; width: 350px; "><strong>En Igualdad&nbsp;Animal vemos esta tradición como&nbsp;<a href="http://www.igualdadanimal.org/antiespecismo">una más de las injusticias que cometemos los humanos con los animales, por la&nbsp;completa desconsideración</a> a la vida de los perros utilizados y a la de miles y miles de animales cazados.</strong> Este texto resume bien el problema de la continuidad de la caza, la educación, procedente del artículo "La caza con galgo: más que una afición, una filosofía de vida": "El galgo no es sólo un pasatiempo, sino toda una filosofía de vida. Esto es lo que se desprende de las palabras de Abraham Corpa, Pablo Salido o Félix Elche. <strong>Heredaron esta afición de sus progenitores y desde entonces no la han abandonado</strong>. “Nací en mi casa y allí ya había galgos”, relata Abraham. El bar que su padre tenía en la localidad conquense de Barajas de Melo era el punto de encuentro para muchos galgueros y el lugar donde mantenían eternas tertulias sobre todo lo relacionado con este mundo. Hoy, es él quien trata de inculcar el amor por este animal a sus hijas, Rocío y Natalia".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> 
<small> Fuentes: <a href="https://www.elcorreo.com/vizcaya/v/20110318/pvasco-espana/detenido-cazador-toledo-ahorcar-20110318.html" target="_blank">elcorreo.com</a>, <a href="http://www.larazon.es/noticia/7686-miles-de-galgos-ahorcados-o-abandonados" target="_blank">larazon.es</a>, <a href="http://webcache.googleusercontent.com/search?q=cache:http://eldiadigital.es/not/8930/la_caza_con_galgo_mas_que_una_aficion_una_filosofia_de_vida" target="_blank">eldigital.es</a></small></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:html -->
<iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FIgualdadAnimal&amp;width=400&amp;colorscheme=light&amp;show_faces=false&amp;stream=false&amp;header=false&amp;height=62" style="border:none; overflow:hidden; width:400px; height:62px;"></iframe>
<!-- /wp:html -->