<!-- wp:image {"id":9440} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/03/perra_arpon_vilagarcia.jpg" alt="" class="wp-image-9440"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Aprovechando que los propietarios pasaban la noche fuera, entre dos y tres personas accedieron a una vivienda de Sobradelo (Vilagarcía) el pasado fin de semana. Al parecer <strong>intentaron dormir a la perra que vivía en el domicilio</strong>, un cruce de Boxer, de 45 kilos de peso llamada “Playa”, pues aparecieron en el suelo del patio restos de salchichas y de unos comprimidos sedantes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sin embargo, el plan de sedar a Playa no debió dar resultado y <strong>decidieron usar un arpón</strong> de casi dos metros de longitud, y clavárselo en la garganta. La herida causada por el arpón llegó a afectarle a la espina dorsal y casi le atraviesa el costado. Posteriormente,<strong> la dejaron malherida en un pilón de piedra</strong>, con el arpón clavado, mientras accedían a las dos plantas del inmueble.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los propietarios de la vivienda se dieron cuenta de lo que había sucedido a la mañana siguiente, cuando acudieron a su casa con la intención de sacar de paseo a Playa. Primero vieron los portalones cerrados, y después se encontraron con restos de sangre y vómitos en el patio. Poco después vieron a la perra, que agonizaba dentro del pilón.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Playa fue llevada inmediatamente a un veterinario y <strong>operada de urgencia</strong>. Pese a ello, las lesiones que sufría como consecuencia del arponazo fueron tan graves que <strong>murió al día siguiente</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La familia está conmocionada y muy afectada por la muerte de Playa, a la que recogieron los voluntarios de la Protectora de Vilagarcía cuando estaba abandonada en una playa (de ahí su nombre) y en la actualidad era cuidada por una decena de jóvenes de la aldea, amigos del propietario de la vivienda.</p>
<!-- /wp:paragraph -->