<!-- wp:image {"id":9061} -->
<figure class="wp-block-image"><img src="/app/uploads/2011/06/2011-06-19_img_2011-06-12_00.31.36__5340494.jpg" alt="" class="wp-image-9061"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
	Un pequeño grupo de expertos en Medicina y Física ya había ideado una manera de comprobarlo. <strong>De momento, en animales</strong>. Desde hace nueve años y de forma experimental, estudian en un laboratorio en Santiago los efectos de la radiación del móvil. Han <strong>creado su propia cámara de radiación y trabajan concienzudamente</strong>. Los resultados ya han dado forma a artículos publicados en revistas científicas: Se vieron alteraciones en células neuronales.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div>
	Gracias a una investigación auspiciada por la Xunta y también al entusiasmo científico, llegaron a idear y fabricar incluso una cámara de radiación que emite en la misma frecuencia de la tecnología móvil. Este recipiente de acero inoxidable conectado a varias antenas les permitió descubrir <strong>cómo se comportaban las ratas luego de estar sometidas a niveles de radiación</strong> equivalentes a hablar dos horas por móvil. Los resultados no se hicieron esperar: <strong>La corteza cerebral del animal se resintió</strong>.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	<strong>Cientos de ratas, que soportaron varias horas de radiación </strong>idéntica a la que emiten los teléfonos móviles sirvieron para los resultados experimentales: La radiación del móvil provoca cambios cerebrales.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	No más de seis grupos en España tienen investigaciones en este sentido y son pocos los que se encargan de analizar los efectos a nivel biológico. Ahí es donde se sitúa la investigadora y profesora en la facultad de Medicina, Elena López Martín. Junto al catedrático de Electromagnestismo Francisco Ares Pena y un equipo de expertos, lidera los ensayos en ratas. Sin querer provocar alarmismo, éstas registraron una proliferación de células gliales a nivel de corteza cerebral, advierte. "Existe una proliferación glial en la parte más superficial del cerebro, dice enseñando muestras de la corteza cerebral de las ratas". "Se han hallado modificaciones morfológicas en el cerebro de los animales y también en el comportamiento. También encontramos modificaciones en el electroencefalograma", añade. "Trabajamos a 900 megahercios y modulación GSM, la misma frecuencia y modulación que los teléfonos móviles", explica.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	Otro de los estudios, específico sobre epilepsia, concluye que "en un modelo experimental (subconvulsivo en ratas) el efecto de la radiación aumenta la tendencia a la convulsión", explica. Los trabajos publicados se centran en el cerebro y modelos neurológicos, pero ahora están trabajando en otros tejidos como tiroides y órganos genitales.</div>
<!-- /wp:html -->