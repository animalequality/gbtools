<!-- wp:image {"id":11605} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/foto_fb_7.jpg" alt="" class="wp-image-11605"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Australia confirma la tendencia internacional a incluir cada vez más alternativas a la carne vegetarianas y veganas en la alimentación de los consumidores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De 2014-2016 el país australiano <strong>ha experimentado un espectacular crecimiento en la oferta de productos alternativos a la carne</strong>. Un estudio de mercado ha revelado que en dicho periodo las opciones veganas en los supermercados creció un 92%.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por su parte, los lanzamientos de productos vegetarianos crecieron un 8% en el mismo periodo. Los australianos, famosos por su elevado consumo de carne, parecen estar cambiando sus hábitos hacia una alimentación más consciente con el planeta y los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>


<p>&nbsp;</p>

<p>El estudio de mercado revela que las alternativas a la carne cada vez son más apreciadas por los consumidores australianos. <strong>Uno de cada ocho</strong> productos alimenticios lanzados al mercado en 2016 incluían la etiqueta de producto vegetariano.</p>

<figure><img alt="" class="wp-image-11606" src="/app/uploads/2017/02/foodworks-vegan3.jpg" style="float:left; margin:10px; width:451px"></figure><p></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>De esta forma, Australia <a href="https://igualdadanimal.org/noticia/2016/04/24/las-opciones-vegetarianas-crecen-un-633-en-alemania/" target="_blank">sigue la tendencia de aumento de alternativas a la carne marcada ya por países como Alemania</a>, en el que en el periodo 2011-2015 los productos vegetarianos experimentaron un crecimiento de un 633% y los productos veganos un espectacular 1800%. Por su parte, <a href="https://igualdadanimal.org/noticia/2016/08/08/en-dinamarca-las-alternativas-la-carne-aumentan-un-30-en-un-ano/" target="_blank">Dinamarca experimentó un crecimiento del 30% en los lanzamientos al mercado de productos alternativos a la carne</a> en tan solo un año (2014-2015).</p>

<p>A nivel internacional, la reducción del consumo de carne y el aumento del consumo de productos alternativos parece estar asociada con tres factores principales. El primero es la preocupación por el bienestar de los animales, el segundo es <a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/" target="_blank">la conciencia de los consumidores ante la relación entre la ganadería y el cambio climático</a> y, por último, una tendencia hacia hábitos de consumo más saludables y sostenibles.</p>

<p>&nbsp;</p>

<p><br>
Fuente: http://www.mintel.com/press-centre/food-and-drink/vegan-food-launches-in-australia-grew-by-92-between-2014-and-2016</p></font>
<!-- /wp:html -->