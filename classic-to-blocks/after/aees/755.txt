<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Parlamento Europeo aprobó una declaración escrita para reclamar a la Comisión Europea que prepare de inmediato una regulación con el fin de prohibir la venta, así como de las importaciones y exportaciones de pieles y otros productos de foca en todos los países de la Unión Europea.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Parlamento Europeo aprobó una declaración escrita para reclamar a la Comisión Europea que prepare de inmediato una regulación con el fin de prohibir la venta, así como las importaciones y exportaciones de pieles y otros productos de foca en todos los países de la Unión Europea. La Eurocámara argumentó que un millón y medio de crías de focas han sido "sacrificadas" en el Atlántico Noroeste en los últimos cuatro años y la mayoría de estos animales tenía menos de tres meses.

Han firmado esta demanda 373 eurodiputados, más de la mitad de los parlamentarios y fue promovida por el sueco Carl Schlyter (Los Verdes); la británica, Caroline Lucas, del mismo grupo; el portugués Paulo Casaca (socialistas) y el alemán Karl-Heinz Florenz (populares).

Los eurodiputados subrayaron que países como Bélgica, Luxemburgo e Italia han dado pasos para vetar los productos de foca, mientras que Estados Unidos, México y Croacia lo han prohibido.

El Parlamento europeo también afirmó que al 42% de los animales sacrificados se les quitó la piel cuando aún estaban conscientes.
 
Cada año bajo la fuerza de los garrotes, los garfios, y los golpes hacen que 350 mil focas seas asesinadas. Las madres verán como sus crías se desangran vivas, mientras ellas permanecen impotentes esperando su muerte o viendo a sus familias morir. Esta decisión de la Comunidad Europea es un paso adelante hacia el fin de semejante masacre.</p>
<!-- /wp:paragraph -->