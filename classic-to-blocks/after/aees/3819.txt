<!-- wp:paragraph -->
<p>Hace seis meses cayeron 9.316 barriles de crudo al río Pamplonita, en el departamento Norte de Santander (Colombia). Ello provocó la muerte de millones de peces por asfixia, debido a la falta de oxígeno en el agua contaminada de crudo, según un estudio de la Universidad Francisco de Paula Santander.

Según José Gabriel Román, subdirector de Corponor, también murieron cien aves y cientos de animales de otras especies (ardillas, faros, zorros fara, puercoespines y lagartijas).


Fuente: El Tiempo</p>
<!-- /wp:paragraph -->