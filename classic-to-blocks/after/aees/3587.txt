<!-- wp:paragraph -->
<p>Un hombre, buscado por agredir sexualmente a animales en Missouri, ha sido detenido.

La Policía del condado Clay arrestó a Matthew Roberts, de 44 años, quien tenía una orden de arresto por haber agredido sexualmente a animales. Actualmente se encuentra en la cárcel del norte de Florida sin derecho a fianza, y se espera que sea extraditado a Missouri.</p>
<!-- /wp:paragraph -->