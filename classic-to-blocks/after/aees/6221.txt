<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><img class="wp-image-13597" style="width: 250px; margin: 5px; float: left;" src="/app/uploads/2011/12/10122011comunicado.jpg" alt="Día Internacional de los Derechos Animales">Con motivo del Día Internacional de los Derechos Animales la organización Igualdad Animal convoca el próximo sábado 10 de Diciembre a las 12 del mediodía en la Puerta del Sol de Madrid una impactante protesta, en la que 400 activistas provenientes de varios países sostendrán en sus manos 400 cadáveres de animales que han sido víctimas del consumo para reivindicar los derechos animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Gatos, conejos, pollos, cerdos, gallinas, corderos… son algunos de los 400 animales que formarán parte de esta protesta. Recogidos de los contenedores de las granjas donde permanecían apilados o sacados del interior de dichas granjas donde habían muerto, estos animales pasarán de haber sido explotados en vida y finalmente desechados como basura a ser los protagonistas de una reivindicación solemne mediante la que exigimos que los derechos animales sean respetados.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Junto a otras organizaciones en todo el mundo, Igualdad Animal celebra desde 2006 el Día Internacional por los Derechos Animales coincidiendo con el Día por los Derechos Humanos para reivindicar que el mismo principio igualdad que nos lleva a respetar a otros seres humanos debe ser extendido para proteger a los demás animales. Esta protesta, que este año cuenta con la colaboración de la Fundación Equanimal, ha dado literalmente la vuelta al mundo en ediciones anteriores y se ha convertido en un acto de referencia para el movimiento de derechos animales a nivel internacional; en países como Chile, Perú o Francia están llevando a cabo actos similares para conmemorar este día.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Con esta protesta pretendemos provocar un debate en la sociedad sobre la relación de explotación y dominación que mantenemos con el resto de animales con los que compartimos este planeta. Millones de animales son explotados y matados cada día para nuestro consumo. Al igual que nosotros, tienen interés en vivir y no sufrir, y a pesar de ello, ignoramos estos intereses por el simple hecho de que no pertenecen a nuestra especie, una discriminación arbitraria denominada especismo, que es análoga a otras discriminaciones como el sexismo o el racismo. En este acto mostraremos estas víctimas a la sociedad, víctimas invisibles que han padecido toda clase de horrores en granjas, mataderos y otros centros de explotación, y que hacemos visibles para reclamar justicia para todos los animales. Porque el deseo de vivir no entiende de especie.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Igualdad Animal es una organización internacional de derechos animales actualmente presente en España, Reino Unido, India, Polonia y Venezuela.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/albums/72157625450290279"><img src="https://farm6.staticflickr.com/5209/5251779420_b8c2b11e04_z.jpg" alt="11/12/10 - Madrid - Día Internacional de los Derechos Animales | International Animal Rights Day"/></a></figure>
<!-- /wp:image -->

<!-- wp:html -->
<script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://player.vimeo.com/video/31978325?color=00c4ff&amp;title=0&amp;byline=0&amp;portrait=0" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe><a href="https://vimeo.com/31978325">Convocatoria 10 de Diciembre 2011 - Igualdad Animal / Animal Equality</a>.

&nbsp;

<strong>CONTACTO DE MEDIOS</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Javier Moreno</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Portavoz de Igualdad Animal</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Tel. 691 054 172</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Tel. 618 843 146</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Correo electrónico: <a href="mailto:javierm@igualdadanimal.org">javierm@igualdadanimal.org</a>&nbsp;&nbsp;</p>
<!-- /wp:paragraph -->