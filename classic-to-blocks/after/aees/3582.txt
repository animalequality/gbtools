<!-- wp:paragraph -->
<p>Cuatro hambrientos tigres siberianos mataron y devoraron a otro en un zoo chino enfrentado a apuros financieros que le han impedido alimentar apropiadamente a sus animales, informó hoy la agencia estatal, Xinhua.

El suceso, que ha vuelto a disparar las alarmas sobre las condiciones de la red de zoológicos del país, ocurrió este fin de semana en el Zoo Glaciar de Shenyang, capital de la provincia nororiental de Liaoning, cuando los cuatro animales atacaron a otro y comenzaron a comérselo.

El zoológico, que abrió sus puertas hace siete años, mantiene encerrados a 2.000 animales. El año pasado un crédito del Gobierno local evitó que tuviera que cerrar sus puertas.</p>
<!-- /wp:paragraph -->