<!-- wp:image {"id":12308} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/shutterstock_476293381.jpg" alt="" class="wp-image-12308"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>En Estados Unidos y Europa los productos alternativos a la carne se están haciendo cada vez más presentes y aparecen más empresas que los fabriquen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>¿Pero pasa lo mismo en Latinoamérica?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La respuesta no solo es positiva sino que esto ha ocurrido de una forma meteórica. <strong>En un gigante como Brasil, en un tiempo récord de apenas tres años, el mercado de alimentos producidos a base de vegetales se ha disparado enormemente.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De acuerdo con Gustavo Guadagnini, director de la organización Good Foods Institute, las empresa que producen este tipo de alimentos en el paÍs están creciendo a una tasa del 40% anual.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>GFI es una organización sin fines de lucro que apoya a empresas de productos vegetales y a empresas productoras de «carne limpia».<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/02/08/el-mercado-de-alternativas-la-carne-crece-un-40-cada-ano-en-brasil/" target="_blank">El mercado de alternativas a la carne crece un 40% cada año en Brasil</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En Brasil está trabajando para ayudar a estas empresas con el desarrollo de sus productos, su financiación y promoción porque <strong>creen firmemente que este mercado seguirá creciendo exponencialmente en los próximos años.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12309} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/good-food-institute-talks-plant-based-foods-beverages-in-brazil_strict_xxl.jpg" alt="" class="wp-image-12309"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Guadagnini asegura que todo esto ocurrirá porque la demanda de los consumidores es muy alta, incluyendo (y sobre todo) a aquellos no vegetarianos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Desde 2014, los productos han experimentado una drástica transformación. <strong>De existir solamente una mezcla de queso en polvo y varios productos de soya, ahora se pueden ver líneas completas de carne vegetal </strong>y alternativas lácteas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Marcas como Goshen, Vegabom, Veggy, NoMoo y Superbom presentan opciones que incluyen leches vegetales, quesos a base de papa, alternativas a la carne hechas a base de soya y mayonesa sin huevo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="1"><font color="#808080"><p>Leches vegetales de la marca A tal da castanha.</p></font>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p>«Las empresas que operan en el sector del mercado de alternativas a la carne están reportando un fuerte crecimiento positivo, y el número de minoristas interesados ​​en vender estos productos está aumentando dramáticamente», aseguró Guadagnini.</p></font>

<p>&nbsp;</p>

<p>Y según Guadagnini, <strong>hoy en día es mucho más fácil encontrar productos lácteos a base de vegetales no solo en las principales ciudades</strong>, sino también en las más pequeñas: «A partir de aquí, es solo cuestión de tiempo antes de que estos productos sean prolíficos en todo el país», asegura.</p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p><strong>La industria de la carne brasileña tiene un impacto dramático a nivel mundial. </strong>Es por eso que nos preguntamos si los consumidores de Brasil serán receptivos a la «carne limpia» o cultivada. Guadagnini asegura que sí y que como en cualquier mercado de consumo será necesario comunicar claramente sobre los beneficios y la seguridad de esta carne.</p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p>«Teniendo en cuenta el impacto medioambiental positivo, la mejora del bienestar animal y la mejor calidad esperada de estos productos (esta carne estará libre de contaminantes como la fiebre aftosa y la salmonella, que son problemas en la producción convencional de carne), no hay razón por la cual los brasileños no acogerán la carne limpia».</p></font>

<p>&nbsp;</p>

<p>Los avances que están teniendo empresas productoras de carne limpia como Memphis Meats han logrado una gran recepción positiva en las redes sociales en el país.</p>

<figure><img alt="" class="wp-image-12310" src="/app/uploads/2017/08/carne_limpia_memphis_meat.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>Actualmente, JBS (la mayor compañía de carne del mundo) y BRF, con sede en Brasil, están envueltas en un escándalo de corrupción y sanidad por conspirar con funcionarios de salud en permitir que la carne podrida que estas compañías adulteraron con ácido se vendiera al público.</p>

<p>La compañía matriz de JBS fue multada con un récord de 3,2 mil millones de dólares, una cantidad que supone un retraso en la industria de la carne de Brasil durante un tiempo en que los productos a base de plantas continúan en auge.</p>

<p>Y a la luz de estas recientes acusaciones el público está más abierto que nunca a considerar nuevas inteligentes ideas para la producción de carne.</p>

<p>&nbsp;</p>

<font size="1"><font color="#808080"><p>«Carne limpia» de la empresa Memphis Meats ©Memphis Meats</p></font>

<p>&nbsp;</p>

<p>Fuentes:</p>

<p><a href="http://vegnews.com/articles/page.do?pageId=9600&amp;catId=1" target="_blank">http://vegnews.com/articles/page.do?pageId=9600&amp;catId=1</a></p>

<p><a href="https://www.foodnavigator-latam.com/Article/2017/06/01/Good-Food-Institute-talks-plant-based-foods-beverages-in-Brazil" target="_blank">https://www.foodnavigator-latam.com/Article/2017/06/01/Good-Food-Institute-talks-plant-based-foods-beverages-in-Brazil</a></p></font></font></font></font></font>
<!-- /wp:html -->