<!-- wp:paragraph -->
<p>Herencia-Según informa La Tribuna de Ciudad Real, los 80 perros que iban a ser asesinados debido a la construcción de un campo de fútbol en el mismo terreno donde está situada la protectora que los acoge, salvarán finalmente sus vidas al llegar a un acuerdo con el ayuntamiento.

Afortunadamente el nuevo alcalde de Herencia se dio cuenta de la gravedad del asunto y buscó una solución que no implicase la muerte de los animales. Una parte de ellos serán adoptados y los demás serán trasladados a las nuevas dependencias que se han habilitado provisionalmente, hasta la construcción del emplazamiento definitivo.

Según el alcalde, Jesús Fernández, este es un acuerdo «que concilia la protección de los animales y el derecho de los herencianos a poder tener un campo de fútbol. En breve la protectora tendrá unas instalaciones acordes a sus necesidades»,

Normalmente, cuando se da un conflicto de intereses entre seres humanos y otros animales, la solución que se suele tomar, por desgracia, es el asesinato de los animales. El caso de los perros de Herencia es un buen ejemplo de cómo los conflictos de interéses con otros animales pueden ser solucionados mediante alternativas que no impliquen un desenlace fatal para una de las partes.</p>
<!-- /wp:paragraph -->