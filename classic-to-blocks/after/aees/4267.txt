<!-- wp:paragraph -->
<p>El gobierno de Gibraltar ha planeado matar al menos a 25 de los monos que viven allí. Ya se ha matado a dos de ellos.

El ministro de Turismo de Gibraltar, Ernest Britto, dijo que el asesinato de 25 monos en Bahía Catalana y Bahía Sandy ya había sido autorizado, y anunció la introducción de algún tipo de control de natalidad entre los monos.

Además, Britto confirmó que dos ejemplares ya habían sido asesinados a través de una inyección letal.

Franco Ostuni, director general del Hotel Caleta, buscando negar la categoría de ciudadanos de los gibraltareños y su libertad de expresión, ha llegado a afirmar que quienes defienden los intereses de los animales no tienen que vivir allí.</p>
<!-- /wp:paragraph -->