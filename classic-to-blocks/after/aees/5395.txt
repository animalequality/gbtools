<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font color="#000000" face="Times New Roman" size="3"><font size="3"><font color="#000000"><font face="Times New Roman"><o:p><img align="left" alt="" height="209" hspace="9" class="wp-image-13703" src="/app/uploads/2010/04/Francis-Wolf-premio.jpg" vspace="9" width="200"></o:p></font></font></font>En un impresionante ejercicio de demagogia y falta de argumentos, el filósofo francés Francis Wolf hizo un llamamiento para ir a las corridas con orgullo y comparó la matanza y tortura de toros y caballos con diferentes líneas de pensamiento filosófico, como las ideas platónicas o aristotélicas.</font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font color="#000000" face="Times New Roman" size="3">"¿Para qué hablar de filosofía a los aficionados sevillanos?", se preguntó el pregonero, que se contestó a sí mismo señalando que "yo he aprendido poco a poco que la Filosofía no tiene mucho que enseñar a los aficionados porque sus conceptos los estamos viendo día a día en los ruedos".</font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font color="#000000" face="Times New Roman" size="3">Wolff explicó que "en el toreo comprendemos a Platón sin haberlo leído nunca. Sólo la corrida de toros obra el milagro de materializar la idea platónica", precisó el pregonero que aludió a una matanza de un toro de El Cid en 2005 en la que "Manuel Jesús había desaparecido y sólo quedaba El Cid".</font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font color="#000000" face="Times New Roman" size="3">El filósofo francés también aludió a las doctrinas aristotélicas señalando que el corazón de la tauromaquia está directamente entroncado con "la esencia misma de la vida. El toro es la expresión de la propia naturaleza, de todas sus posibilidades. El toro está en libertad como oposición al buey manso. El ruedo es el territorio donde el toro libra su último combate para defender la bravura", señaló.</font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font color="#000000" face="Times New Roman" size="3">Wolff cerró el círculo de comparaciones filosóficas aludiendo al epicureismo al señalar que esta corriente filosófica sostiene que "el placer es la única buena razón para actuar. Vamos a la corrida a disfrutar”.</font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font color="#000000" face="Times New Roman" size="3">Dentro del conjunto de argumentos vacíos de Wolf, cabe destacar el siguiente. Aludiendo a su libro "50 razones para defender la Fiesta de los Toros", dijo que de todas las razones "la más importante es que los toros nos gustan y es mejor que olvidemos todas las demás para ir a la plaza orgullosamente, sin complejos y llevando de la mano a nuestros primos catalanes y gallegos".</font></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">
	<font color="#000000" face="Times New Roman" size="3">Evidentemente, que algo nos guste no es justificación para ninguna práctica que afecte a terceros. A los violadores les gusta violar, pero eso no justifica sus actos. Del mismo modo, el hecho de que a los aficionados taurinos les guste la tauromaquia no justifica esta práctica, pues la tortura y asesinato de toros y caballos no es una opción personal, ya que afecta a los intereses de otros. </font></p>
<!-- /wp:paragraph -->