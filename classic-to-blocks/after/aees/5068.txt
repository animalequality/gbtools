<!-- wp:paragraph -->
<p>SORIA- Según informa Europa Press, un madrileño de 61 años, vecino de Los Rábanos (Soria) fue imputado por un delito de maltrato a 22 perros en una perrera de la localidad de Tardajos.

El pasado día 5 de octubre, agentes de la Guardia Civil de Soria localizaron una perrera en la localidad de Tardajos en la que había 22 perros (uno de ellos muerto), enfermos y en pésimas condiciones higienico-sanitarias, sufriendo hacinamiento y falta de alimentación. Por ello, los agentes avisaron al Equipo del Seprona de la Guardia Civil de Soria, cuyos miembros iniciaron la búsqueda del responsable.

El día 16 el Seprona localizó al responsable de la perrera, al que se imputó un delito de maltrato de animales domésticos.

Durante la búsqueda del responsable murió otro de los perros.</p>
<!-- /wp:paragraph -->