<!-- wp:paragraph -->
<p>España - El Seprona dió por concluida la búsqueda de la pitón desaparecida en la barriada sevillana de las Tres Mil Viviendas, pues la denuncia fue impuesta hace ya un mes, por lo que la probabilidad de que la serpiente haya muerto es bastante alta, ya que al ser haber sido domesticada supuestamente no podría sobrevivir sola.

El General Jefe de la IN Zona de la Guardia Civil, Antonio Dichas recordó que la “tenencia” de este tipo de animales no es ilegal, siempre y cuando se cuente con todos los permisos y controles de importación necesarios.

Sin embargo, el señor Dichas no entró a valorar la injusticia que supone el hecho de reducir a cualquier animal, humano o no, con capacidad para experimentar su propia vida, a una simple propiedad que se puede tener, importar o encerrar. Al final, la serpiente pagó con su vida el precio del especismo de aquellos que le impusieron el estatus de objeto.

Fuente: terra.es</p>
<!-- /wp:paragraph -->