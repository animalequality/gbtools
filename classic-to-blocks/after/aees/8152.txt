<!-- wp:paragraph -->
<p>¿Perezosos y poco aseados? Prepárate para olvidarte de todo lo que siempre has escuchado sobre ellos. Los cerdos son animales verdaderamente sorprendentes, dotados de gran sensibilidad e inteligencia.

Acá te presentamos 5 motivos para amarlos y protegerlos:
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">1. Son super inteligentes y lo recuerdan todo</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Los cerdos son uno de los animales más inteligentes del planeta. Pero lo que la ciencia ya ha confirmado es que no solo son muy listos sino que son más listos que los perros, gatos, algunos primates y los niños de tres años.

También tienen una excelente memoria. Diversos estudios demuestran que los cerdos pueden recordar donde han encontrado comida, direcciones y encontrar el camino de vuelta a casa desde largas distancias. También son capaces de reconocer y recordar a humanos y hasta otros 30 cerdos.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">2. Son muy limpios</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
¿Cuántas veces no hemos escuchado a alguien usar equivocadamente la palabra «cerdo» para insultar a quien considera vago o sucio? Pero la realidad es muy distinta: los cerdos son animales muy limpios, tanto que pueden convivir con nosotros de la misma forma que lo hacen los perros y los gatos. Además, a los cerdos les encanta bañarse con agua y si se dan baños de barro es para mantenerse frescos y protegerse del sol ya que no tienen glándulas sudoríparas.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">3. Son conscientes de su propia existencia</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
En un estudio realizado en la Universidad de Cambridge se halló que los cerdos son seres inteligentes capaces de reconocerse en un espejo y aprender rápidamente que los objetos que les muestran son reflejos. Esta habilidad la comparten con muy &nbsp;pocos animales como los delfines, chimpancés y elefantes.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">4. Las cerdas son unas «super madres»</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Entre los cerdos existe una unidad familiar matriarcal y los lechones son cuidados por parientes femeninas. Las cerdas son excelentes madres que se preocupan y darían todo por defender a sus hijos y sufren mucho cuando son separadas de ellos.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large;">¿Quieres recibir las mejores noticias de actualidad sobre los animales? ¡Suscríbete gratuitamente a <span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958">nuestro e-boletín!</a></span></span></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">5. Aprenden velozmente</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Los cerdos son unos de los animales más rápidos de la naturaleza en aprender. Se sabe que aprenden más rápido que los perros y los primates. También pueden abrir y cerrar puertas, guiar a rebaños de ovejas, armar rompecabezas y resolver laberintos ¡e incluso jugar a videojuegos con un joystick!

&nbsp;

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/twS_COailzk" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">6. ¿Cerdos parlanchines?</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
¡Sí! Los cerdos pueden emitir más de 30 sonidos que tienen significados específicos para expresar sus intenciones, estados de ánimo, advertencias, saludos y cuándo es la hora de comer.

Las cerdas emiten un llamado especial para avisarles a los cerditos que ya es hora de amamantar y estos ya desde recién nacidos reconocen las voces de sus madres.

&nbsp;</p>
<!-- /wp:paragraph -->