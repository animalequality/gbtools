<!-- wp:image {"id":9808} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/12/caballos_g.jpg" alt="" class="wp-image-9808"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-13685" src="/app/uploads/2012/12/Caballos_G.jpg" style="width: 150px; float: left; margin: 2px 10px;">El número de caballos matados en Gesesur, el <a href="http://www.mataderos.info/" target="_blank">matadero</a> de Mercasevilla, ha crecido de forma terrible. Se estima que al finalizar el año habrán muerto en estas instalaciones entre 10.000 y 12.000 equinos. Algunas semanas se llegan a matar hasta 500 animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La Junta de Andalucía estima que en 2011 se mataron en toda la comunidad 5.189 caballos, mientras que según los últimos datos, este año la cifra puede llegar a 13.686. Este incremento se debe a que muchos ganaderos, por causa de la crisis, no consideran rentable asumir los costes de alimentación y asistencia veterinaria que genera un caballo y por tanto deciden enviarlos al matadero. La masacre ha sido tan grande que ha habido un exceso de carne de caballo acumulada y este matadero ha detenido la matanza de estos animales hasta febrero.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La carne de caballo no es muy consumida en España, salvo en algunas zonas de Cataluña y la Cornisa Cantábrica y la mayor parte se exporta a Francia, Italia y Rusia.</p>
<!-- /wp:paragraph -->