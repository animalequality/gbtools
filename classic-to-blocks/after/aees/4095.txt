<!-- wp:paragraph -->
<p>Las granjas gallegas han cursado peticiones para adquirir unas 10.000 vacas que serán explotadas por su leche. Asimismo  los ganaderos dedicarán todas las vacas jóvenes que puedan a reponer el censo. Los dirigentes del sector estiman que a finales de año se explotará por su leche a 160.000 vacas más, entre las importadas y las 'recriadas' en las propias granjas. Francia también ha instado a la compra de 100.000 nuevas vacas para incrementar la extracción de leche.

Desde su ingreso en la UE, Galicia ha aumentado la cantidad de leche extraída a las vacas desde 0,8 a 2,2 millones de toneladas anuales, a base de inversiones en compra de cuota a otras autonomías españolas.

Actualmente 450.000 vacas son explotadas por su leche en Galicia.

Fuente: El País

NOTA: La leche, al igual que el resto de lácteos, los huevos y demás productos de origen animal suponen el asesinato y explotación de animales. Puedes obtener más información sobre las consecuencias del consumo de huevo en la siguiente página web: <a href="https://igualdadanimal.org/problematica/lacteos/</a">,</a></p>
<!-- /wp:paragraph -->