<!-- wp:image {"id":12134} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/shutterstock_401388067.jpg" alt="" class="wp-image-12134"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
En todo el mundo el incremento en el número de personas que reducen o eliminan por completo su consumo de carne es cada vez mayor. <strong>En México la situación es notoria ya que este es el país con más vegetarianos en toda Latinoamérica.</strong>

Según el estudio global de Nielsen sobre Salud y Percepciones de Ingredientes un significativo número de mexicanos llevan una alimentación que excluye la carne o que solo incluye pequeñas porciones de la misma.

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><strong>Noticia relacionada: </strong><a href="https://igualdadanimal.org/noticia/2016/11/11/mexico-el-pais-con-mas-vegetarianos-en-latinoamerica/" target="_blank" rel="noopener noreferrer"><strong>México, el país con más vegetarianos en Latinoamérica</strong></a></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
&nbsp;

La encuesta realizada sobre 30.000 personas en 63 países diferentes arrojó que 19% de los mexicanos entrevistados se declaró vegetariano, el 15 % flexitariano (mínimo consumo de carne) y el 9% vegano.

Y es que <strong>en los último 9 años los mexicanos han dejado de comer 3.2 kilos menos de carne de vaca al año, pasando de 18.8 kilos en 2007 a 14.8 kilos en 2016</strong>. Y desde 2013, por cuarto año consecutivo el consumo de carne en México es menor que la producción [1].

En el resto del mundo la tendencia resulta ser la misma ya que el consumo disminuyó en promedio de un kilo durante el mismo período de tiempo.

&nbsp;

&nbsp;

<a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank" rel="noopener noreferrer">Puedes suscribirte gratuitamente a nuestro e-boletín</a> para que recibas las mejores noticias de actualidad sobre los animales y conozcas más opciones de alimentación.

&nbsp;

&nbsp;

En contraste con otros países de Latinoamérica, México es el país en el cual una mayor cantidad de personas ha declarado que dejó de comer carne debido a sus convicciones. Según el Gabinete de Comunicación Estratégica, <strong>un 36% de los mexicanos declaró haber dejado de comer carne por respeto a los animales.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12113} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/shutterstock_519758773.jpg" alt="" class="wp-image-12113"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

Además de ser mucho más saludable para ti, &nbsp;llevando una alimentación basada en vegetales contribuyes a <a href="https://igualdadanimal.org/noticia/2015/11/09/el-alto-precio-de-la-ganaderia-industrial/" target="_blank" rel="noopener noreferrer">frenar el cambio climático</a> y los animales te agradecerán enormemente que tomes esa decisión. <strong>Cada persona que decide alimentarse de forma vegetariana salva 25 animales al año.</strong>

Te animamos a que comiences a reducir tu consumo de carne sustituyéndola por una gran cantidad de deliciosas opciones vegetales ya disponibles en <a href="https://igualdadanimal.org/noticia/2017/01/10/10-deliciosos-productos-vegetales-que-puedes-encontrar-en-tiendas-de-mexico/" target="_blank" rel="noopener noreferrer">supermercados</a> y <a href="https://igualdadanimal.org/noticia/2017/04/05/10-excelentes-restaurantes-con-opciones-100-vegetales-en-ciudad-de-mexico/" target="_blank" rel="noopener noreferrer">restaurantes</a> de México.

Y si se trata de cocinar en casa, lo que sobran son recetas fáciles, rápidas y divinas. <a href="http://www.igualdadanimal.org/noticias/7643/12-recetas-para-sustituir-la-carne-que-se-preparan-entre-10-y-30-minutos" target="_blank" rel="noopener noreferrer">¡Mira nada más esta selección que tenemos para ti!</a>

&nbsp;

[1] <a class="break-all" href="https://www.gob.mx/cms/uploads/attachment/file/200639/Panorama_Agroalimentario_Carne_de_bovino_2017__1_.pdf" target="_blank" rel="noopener noreferrer">https://www.gob.mx/cms/uploads/attachment/file/200639/Panorama_Agroalimentario_Carne_de_bovino_2017__1_.pdf</a></p>
<!-- /wp:paragraph -->