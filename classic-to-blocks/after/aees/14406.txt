<!-- wp:paragraph -->
<p>Un estudio sobre los fondos marinos del Mediterráneo noroccidental, alrededor de las costas de España, Francia e Italia con redes de arrastre indicaba una muy elevada concentración de basura marina, casi 2.000 unidades por kilómetro cuadrado. El 77% de los desechos eran plásticos y, de ellos, el 92,8% eran bolsas de plástico. Éstas provocan graves impactos sobre los animales que viven en el mar, que van desde el enmallamiento de tortugas, cetáceos y focas, a la ingestión de plásticos.

En el Mediterráneo español un estudio halló que el 75% de los individuos de tortuga boba analizados presentaban ingestión por plásticos, lo que puede bloquearles el tracto digestivo e impedir que los animales se alimenten correctamente hasta provocar su muerte.

Fuente: ABC.es

NOTA: Igualdad Animal se opone a los experimentos realizados con animales. La publicación en esta página de datos obtenidos en experimentos con animales tiene un objetivo puramente informativo, sin que ello suponga un apoyo de Igualdad Animal a dichos experimentos.</p>
<!-- /wp:paragraph -->