<!-- wp:paragraph -->
<p><strong>La Policía Foral ha matado hoy a tiros a una vaca y un buey</strong> que habían accedido a la Autopista de Navarra (AP-15), a la altura de Olite y Tafalla, <strong>tras fugarse de una granja.</strong><br>
	<br>
	Hacia las 9:15 horas, patrullas de Tráfico y de Seguridad Ciudadana de la comisaría de Tafalla de la Policía Foral regularon el tráfico en la autopista y abatieron a uno de los animales.<br>
	<br>
	Posteriormente, apoyados por el “<a href="http://www.igualdadanimal.org/articulos/gary-francione/animales-como-propiedad" target="_blank">propietario</a>” de las reses, condujeron al otro bóvido hacia el corral. Sin embargo, alrededor de las 10 horas y cuando ya se encontraban en las cercanías del kilómetro 5 de la carretera NA-115 (Tafalla-Peralta), <strong>también le dispararon alegando que el animal presentaba un nerviosismo extremo.</strong><img alt="" class="wp-image-14313" src="/app/uploads/2012/05/vaca_navarra.jpg" style="width: 571px; float: right;"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
	La presencia de la vaca y el buey en la vía,<em> «a pesar del grave riesgo que ha supuesto para la circulación»</em>, no ha ocasionado ningún accidente, según ha informado el Gobierno de Navarra en un comunicado.<br>
	<br>
	Según su responsable, la vaca y el buey se habían escapado de su finca y, por el momento, se desconoce cómo accedieron a la autopista.</p>
<!-- /wp:paragraph -->