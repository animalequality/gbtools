<!-- wp:paragraph -->
<p>El pasado jueves un francotirador disparó contra una colonia de gatos en Barcelona, concretamente en el barrio de Trinitat Nova. Dos felinos murieron, y otro resultó herido y puede quedarse ciego.

Los dos gatos asesinados, Colorines y Tres Bessones, tenían siete y cuatro años, respectivamente. El gato malherido, Mercadero, sigue ingresado en una clínica veterinaria, y corre el riesgo de quedarse ciego.

Fuente: 20minutos.es</p>
<!-- /wp:paragraph -->