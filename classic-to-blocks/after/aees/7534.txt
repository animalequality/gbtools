<!-- wp:paragraph -->
<p>La decisión de la empresa es el resultado de una serie de negociaciones con la&nbsp;<a href="https://openwingalliance.org/">Open Wing Alliance</a>, una coalición internacional de la que Igualdad Animal forma parte junto a otras organizaciones de protección animal de todo el mundo.

Después de varios meses de negociación entre Open Wing Alliance&nbsp;y Sodexo, el gigante francés finalmente ha anunciado oficialmente&nbsp;su compromiso: en 2025, la totalidad de su cadena de suministro&nbsp;<strong>habrá abandonado por completo cualquier tipo de huevo o&nbsp;producto que contenga huevo&nbsp;de gallinas criadas en jaulas</strong>. La magnitud de este cambio es mucho más evidente si atendemos a&nbsp;los datos: <strong>Sodexo utiliza 250 millones de huevos de todo el mundo cada año</strong>.

<strong>¿Qué es Sodexo?</strong>

Es una de las compañías más grandes de&nbsp;servicios de comida&nbsp;del mundo,&nbsp;presente en 34.000&nbsp;sitios en 80&nbsp;países. A nivel internacional, son 75 millones de personas las que utilizan los servicios de Sodexo todos los días.

Esta decisión contribuye&nbsp;al proceso de la abolición de las jaulas en la cría de gallinas,&nbsp;ya que al tratarse de uno de los actores más importantes en el sector, condiciona a&nbsp;otros distribuidores, restaurantes,&nbsp;tiendas de comestibles y a otras empresas para que adopten políticas similares en el futuro próximo.

El proceso de negociación con Sodexo se inició en febrero de 2015 y desde entonces numerosas empresas y multinacionales han seguido su ejemplo mediante la adopción de la política "sin jaulas". El anuncio es, sin duda, un progreso&nbsp;histórico&nbsp;que <strong>abre el camino a la abolición definitiva de la cruel práctica&nbsp;de las jaulas</strong>.

<strong>¿Qué&nbsp;es </strong>Open Wing Alliance<strong>?</strong>

Open Wing Alliance es una coalición internacional de organizaciones de protección animal&nbsp;que tiene como objetivo acabar con las&nbsp;jaulas en el sector avícola&nbsp;.

Fundada por <a href="https://thehumaneleague.org/">The Humane League</a>, en la alianza cooperan 13 organizaciones de protección animal de todo el mundo, entre ellas Igualdad Animal, con el objetivo común de hacer que la crueldad de la cría de gallinas en jaulas pase a formar parte del pasado.

«<em>En Igualdad Animal hemos unido fuerzas con algunas de las organizaciones de protección animal más importantes del mundo con un objetivo común: acabar con las jaulas y todo el sufrimiento que esta inhumana práctica conlleva para millones de gallinas. La cría de estos animales en jaulas es una de las prácticas más crueles y que más sufrimiento les provoca. Este anuncio de Sodexo supone un punto de inflexión en el sector que&nbsp;no solo evita&nbsp;la extrema crueldad ocasionada por esta&nbsp;práctica&nbsp;para&nbsp;millones de animales cada año, sino que marca el camino a toda la industria hacia el fin de las jaulas</em>», indicó Javier Moreno, director internacional de Igualdad Animal.</p>
<!-- /wp:paragraph -->