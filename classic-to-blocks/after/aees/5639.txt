<!-- wp:paragraph -->
<p>
	El viaje de una niña a través de un bosque mágico en donde se encontrará con "todo tipo de animales mágicos" se refleja en el espectáculo "Alma de China" del Circo Acrobático Nacional Chino, que se presentará del 25 al 29 de agosto en el teatro Lope de Vega de Madrid.<br>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	El espectáculo, dirigido por Fei Guang Sheng, regresa -ya estuvo en Madrid el pasado año- con la intención de demostrar al público español que para este grupo de artistas "nada es imposible"; además presentan un par de números "novedosos y espectaculares", ha dicho hoy Elisa Zhao, portavoz del Circo, en conferencia de prensa.<br>
	&nbsp;&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	La trama de "Alma de China" gira en torno a la historia de una niña que en sus sueños se adentra en un bosque donde es conducida por el "amable abuelo árbol"; en este lugar, se encontrará con muchos animales fantásticos -representados por los propios acróbatas,<strong> pues en este circo no hay animales</strong>- en medio de un ambiente mágico.<br>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<strong>"Queremos ofrecer un mensaje acerca de la paz para convivir con los animales y la naturaleza; combinamos la música con la luz, el sonido y el telón, el cual hace un efecto de 3D, con sombras al fondo. La intención es que la gente que vaya, vea un espectáculo total para que después reflexione sobre él", ha explicado Zhao.</strong><br>
El Circo Acrobático Nacional Chino se encuentra de gira por España, y tras pasar por Galicia, Cataluña y Valencia; después de las cinco funciones que dará en Madrid, se presentará en León y volverá a Valencia.<span _fck_bookmark="1" style="display: none">&nbsp;</span></p>
<!-- /wp:paragraph -->