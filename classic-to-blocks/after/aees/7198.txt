<!-- wp:image {"id":10199} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/12/transportecorderos_2.jpg" alt="" class="wp-image-10199"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-14310" src="/app/uploads/2014/12/transportecorderos.jpg" style="float:right; margin:5px; width:400px">Hacinados, sin agua ni comida, recorriendo miles de kilómetros hasta llegar al matadero en Italia. Así es el transporte de corderos documentado por Igualdad Animal en su nueva investigación, imágenes que han sido publicadas en primicia en la <a href="https://www.rai.it/dl/RaiTV/programmi/media/ContentItem-81665b27-873f-431e-8b27-a56c0fea5988-tg1.html">televisión italiana TG1</a> y en el medio <a href="https://video.corriere.it/trasporto-agnelli-macelli-italiani/30390c7e-89d8-11e4-a99b-e824d44ec40b">Corriere Della Sera.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta investigación forma parte de la exitosa campaña de Igualdad Animal en Italia "Salva un Cordero", con la que se ha conseguido <a href="https://igualdadanimal.org/noticia/2013/04/11/la-investigacion-de-igualdad-animal-en-italia-reduce-las-ventas-de-cordero-en-el-pais/">reducir el consumo de carne de cordero un 48% durante la Semana Santa</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En las imágenes obtenidas por los investigadores de Igualdad Animal se puede ver a los animales hacinados en los camiones, entre sus propios excrementos, sedientos y exhaustos. También se ha documentado la separación de las crías de las madres nada más nacer y la violencia de los trabajadores hacia los animales en las cargas y descargas de los camiones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"<em>Queremos mostrar el sufrimiento de los corderos durante el transporte hacia el matadero. Un viaje infernal en el que recorren miles de kilómetros, hacinados, entre sus propios excrementos, completamente exhaustos. Publicamos ahora estas imágenes coincidiendo con estas fechas navideñas para mostrar la historia que hay detrás del producto que llega a los supermercados</em>". Indicó Fabrizia Angelini, portavoz de Igualdad Animal en Italia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/albums/72157649873862162"><img src="https://farm8.staticflickr.com/7511/15461677143_8b20678615_c.jpg" alt="El último viaje: nueva investigación sobre el transporte de corderos"/></a></figure>
<!-- /wp:image -->

<!-- wp:html -->
<script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe framespacing="0" frameborder="no" scrolling="no" src="https://video.corriere.it/video-embed/30390c7e-89d8-11e4-a99b-e824d44ec40b?playerType=embed" width="540" height="340" allowfullscreen=""></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed" height="495px" width="880px"><iframe allowfullscreen="" frameborder="0" height="495px" src="https://www.youtube.com/embed/G1ywj5Z4Dac" width="880px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->