<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Testimonio de Jose Valle, cofundador y director de investigaciones de Igualdad&nbsp;Animal</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cuando tenía 8 años presencié la matanza de una cerda, la cual había estado encerrada en un pequeño recinto muchos años en un pueblo donde solía veranear cuando era pequeño.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Noticia relacionada: <a href="https://igualdadanimal.org/investigaciones/" target="_blank">5 investigaciones de Igualdad Animal que te dejarán sin aliento</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Mis vecinos y algunos miembros de mi familia la mataron delante de mí. Vi cómo luchaba por su vida, a pesar de que esta había sido miserable. Escuché los gritos y a continuación vi la sangre. <strong>Quería que pararan pero no hice nada</strong>. Me sentí impotente y trate de justificar en mi mente lo que había ocurrido.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tenía que tratarse de una especie de mal necesario que por aquel entonces no podía entender.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p>«Gracias a toda persona simpatizante por no mirar a otro lado y ser la esperanza para los animales.»</p></font>

<p>&nbsp;</p>

<p>Mucha gente me pregunta cómo puedo infiltrarme en los mataderos y grabar todo el horror que presencio. Normalmente respondo que he desarrollado la capacidad de bloquear mis emociones <strong>para que esa violencia no me impida actuar</strong>, fingir que no me importan los animales y reírme falsamente con los matarifes cuando tengo que hacerlo para poder seguir documentando esos momentos.</p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>Creo que debo esa habilidad en parte a esa cerda que vi morir hace 29 años.</p>

<p>Ahora, cuando estoy en estos lugares, intento obtener el máximo número de ese tipo de imágenes, como si eso diera un sentido al sinsentido, como <strong>una forma de que florezca algo bueno de toda esa oscuridad</strong>. Esas imágenes son lo único que queda de esos animales y es una forma de que no desaparezcan por completo y estén presentes en ellas. <strong>La luz que reflejan</strong> queda captada y mantenida en mi cámara y en mi recuerdo. Y con suerte más tarde también residirán en el recuerdo de otras personas.</p>

<figure><img alt="" class="wp-image-11906" src="/app/uploads/2017/04/29825144320_c38c7e6292_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>No puedo olvidar la desesperación que reflejan los ojos de los animales en sus últimos momentos en el suelo del matadero; y cómo continúan luchando a pesar de no tener ninguna oportunidad, intentando buscar una salida de la zona de aturdimiento, aunque no haya ninguna, aleteando en vano las alas mientras están colgados por las patas atrapados en los ganchos y <strong>gritando y llorando mientras los matarifes ríen</strong>. Hacen todo lo que pueden y no puedo olvidarlo. Ni tampoco quiero.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<font size="1"><font color="#808080"><p>Jose Valle en una granja industrial de gallinas enjauladas.</p></font>

<p>&nbsp;</p>

<strong>Noticia relacionada: <a href="https://igualdadanimal.org/blog/9-fotos-para-explicar-por-que-amamos-nuestros-investigadores/" target="_blank">9 fotos para explicar por qué amamos a nuestros investigadores</a></strong>


<p>La magnitud del problema y el hecho de que según pasa el tiempo, más y más vidas son brutalmente aniquiladas, puede llevar a la desesperación a cualquiera.</p>

<p>Pero <strong>no desespero</strong>, precisamente porque he visto a todos esos animales luchando hasta el final; y también porque <strong>he conocido a muchos de vosotros y vosotras</strong> que lucháis día tras día incansablemente por ellos.</p>

<p>Gracias a toda persona simpatizante por no mirar a otro lado y ser la esperanza para los animales.</p>

<p>Cada paso, cada animal salvado de este horror, es una victoria.</p></font></font></font>
<!-- /wp:html -->