<!-- wp:paragraph -->
<p><strong>Mueren más de 600 cerdos en incendio en granja de Torregrossa</strong></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->
<h5 class="wp-block-heading">(Vie, 11/03/2011)</h5>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><br>
Los Bomberos de la Generalitat han recibido, a las 22:17 h de este jueves&nbsp;(10/03/2011), el aviso de un incendio en una granja de cerdos situada junto al kilómetro 80'5 de la carretera N-240 en Torregrossa (Pla d'Urgell). El fuego ha quemado totalmente la granja de 600 metros cuadrados y como consecuencia han muerto todos los cerdos que había dentro (56 cerdas y 600 lechones). Cinco dotaciones de los Bomberos han estado trabajando en el lugar. Su tarea ha consistido en confinar el fuego en el interior de la nave afectada y evitar su propagación a las naves vecinas. Hacia las 23.00 h el fuego se ha dado por controlado, y una hora más tarde se ha dado por extinguido.<br>
<br>
<br>
<img alt="Imagen de archivo, granja de cerdos incendiada" class="wp-image-14037" src="/app/uploads/2011/03/granjaquemada.jpg"><br>
<br>
<br>
<strong>¿Alguien es capaz de imaginar el inmenso sufrimiento y escuchar sus gritos desesperados? Estos hechos no son infrecuentes, cada cierto tiempo se suceden catástrofes similares en estos centros de explotación, donde los animales no tienen ninguna vía de escape posible. Si no existiesen las granjas, si no hay demanda de productos de origen animal estas tragedias no existirían. El sufrimiento y la muerte de los animales es evitable y la solución está en nuestra mano <a href="http://www.vivevegano.org">llevando un estilo de vida vegano.</a><br>
<br>
Al margen de los incendios esta es la realidad de las granjas de cerdos documentada por el Equipo de Investigaciones de Igualdad Animal, infórmate:</strong><br>
<br>
<a href="http://www.granjasdecerdos.org"><b><big>GranjasdeCerdos.org</big></b></a><br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Trailer del documental "Granjas de Cerdos: Una investigación de Igualdad&nbsp;Animal":</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://player.vimeo.com/video/11853053?color=00c4ff&amp;title=0&amp;byline=0&amp;portrait=0" width="640" height="512" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><a href="https://vimeo.com/11853053">Trailer del documental "Granjas de Cerdos: Una investigación de Igualdad Animal"</a> from <a href="https://vimeo.com/igualdadanimal">IgualdadAnimal | AnimalEquality</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
<!-- /wp:paragraph -->