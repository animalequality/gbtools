<!-- wp:image {"id":13670} -->
<figure class="wp-block-image"><img src="/app/uploads/2010/11/75878_1722407864112_1355816453_1857707_4487776_n.jpg" alt="" class="wp-image-13670"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Veintiséis animales murieron la pasada noche del 13 de noviembre tras declararse un incendio en las instalaciones del zoológico de la ciudad alemana de Karlsruhe. Se desconocen las causas del suceso. El fuego se originó en un área dónde diferentes animales como cabras, ovejas, ponys y burros están expuestos al contacto con el público. El fuego se extendió a la zona de los elefantes e hipopótamos, pero estos animales pudieron ser rescatados de las llamas en el último momento. Los zoológicos son centros de reclusión de animales dónde se les priva de libertad y se ignora sus intereses a favor del deseo de la gente de entretenerse observándoles. La desaparición de estos lugares depende de nosotros mismos. Por favor, no financies estos lugares. Más información sobre zoológicos: http://www.igualdadanimal.org/entretenimiento/zoos</p>
<!-- /wp:paragraph -->