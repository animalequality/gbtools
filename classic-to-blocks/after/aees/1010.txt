<!-- wp:paragraph -->
<p>El jurado del Concurso de Ideas para la Creación de Empresas de Base Tecnológica de la Universidad de Alcalá de Henares, ha concedido hoy los premios de su primera edición. 

El primer premio ha recaido en "Evaluación No Invasiva de Animales de Laboratorio (ENISVAL)". Una iniciativa presentada por Pedro de la Villa Polo del Departamento de Fisiología que ofrece la posibilidad de evaluar funcionalmente y de forma "no invasiva" las capacidades sensoriales visuales de los animales no-humanos utilizados como objetos de experimentación. Se trata de una iniciativa que no pretende acabar con la experimentación animal, sino regular la forma en la que ésta se lleva a cabo. 

Las instituciones públicas como universidades o centros de investigaciones científicas, y las empresas farmacológicas e instituciones privadas con orientación a la investigación animal serán los principales usuarios de esta iniciativa.</p>
<!-- /wp:paragraph -->