<!-- wp:image {"id":11629} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/shutterstock_251926435.jpg" alt="" class="wp-image-11629"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>En España no solo la tauromaquia está de capa caída, el maltrato a los animales de granja está teniendo una consecuencia histórica: el descenso del consumo de carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Una encuesta de la consultora Lantern sobre tendencias de consumo arroja el dato: <strong>3,6 millones de consumidores</strong> han reducido su consumo de carne y solo la comen en contadas excepciones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Parte de estos millones de personas, de hecho, han pasado a alimentarse con alternativas en su día a día. Ni rastro de carne en su cesta de la compra.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En el mismo estudio se preguntó a los consumidores cuáles eran sus motivaciones para haber reducido el consumo de carne o haberla sustituido por completo. La mayoría de ellos (57%), declaraba que <a href="https://igualdadanimal.org/noticia/2016/12/20/industria-carnica-y-maltrato-animal/" target="_blank">el maltrato de los animales por parte de la industria cárnica</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<figure><img alt="" class="wp-image-11631" src="/app/uploads/2017/02/shutterstock_70279201_0.jpg" style="float:right; margin:10px; width:450px"></figure><p></p>

<p>Y no es de extrañar, el abuso y la crueldad en la producción de carne es cada vez más visible para los consumidores. <strong>Si antes era la tauromaquia, ahora son los abusos en las granjas industriales lo que está captando la atención y el rechazo de los españoles</strong>.</p>

<p>Investigaciones con cámaras ocultas como las que lleva a cabo habitualmente Igualdad Animal acaban siendo mostradas en los medios de comunicación, llegando a millones de consumidores. La sensibilidad del consumidor medio está aumentando. ¿Son los animales de granja los nuevos toros a ojos de los consumidores?</p>

<p>En las granjas industriales millones de animales viven en condiciones de hacinamiento <strong>con escasa o nula protección legal</strong>. Las prácticas aberrantes son comunes. &nbsp;A los cerdos recién nacidos se les corta la cola, dientes y testículos sin anestesia. Sus madres ha de darles de mamar encerradas en jaulas tan minúsculas que no pueden ni darse la vuelta.</p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p>«¿Son los animales de granja los nuevos toros a ojos de los consumidores?»</p></font>

<p>&nbsp;</p>

<p>Los terneros son separados de sus madres al poco de nacer y encerrados en minúsculos recintos en los que no podrán alimentarse de leche materna ni disfrutar de la protección familiar. Cuando alcanzan el peso adecuado son enviados al matadero siendo aún bebés.</p>

<figure><img alt="" class="wp-image-11630" src="/app/uploads/2017/02/shutterstock_548409364.jpg" style="float:left; margin:10px; width:450px"></figure><p></p>

<p>Los pollos sufren la peor de las suertes, viven en naves industriales abarrotadas en condiciones tan extremas que millones de ellos ni quiera llegarán al matadero: mueren agonizando en las naves. Pero la industria cárnica cuenta con este porcentaje de «bajas». Para su desgracia, la carne de pollo es con diferencia la más consumida a nivel mundial, lo que multiplica el maltrato.</p>

<p>Ante semejante panorama, cada vez más consumidores está optando por empoderarse rechazando los productos cárnicos o consumiéndolos en contadas ocasiones. Buenas noticias para los animales y malas para la industria cárnica. Son estos consumidores los que están generando un nuevo modelo alimentario más compasivo y sostenible.</p>

<p>Y para certificar la tendencia, <a href="https://igualdadanimal.org/noticia/2017/02/09/espana-mas-vegetariana-que-nunca/" target="_blank">el número de restaurantes vegetarianos en España se ha duplicado en el periodo de 2011 a 2016</a>, pasando a ser de 703 establecimientos en todo el territorio. <strong>Un crecimiento que promete seguir en aumento</strong>.</p>

<p>&nbsp;</p></font></font>
<!-- /wp:html -->