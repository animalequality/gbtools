<!-- wp:image {"id":12297} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/fcebook.png" alt="" class="wp-image-12297"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><a href="http://foodsfortomorrow.com/" target="_blank">Foods For Tomorrow</a> es la startup barcelonesa productora de la proteína vegetal Heura. Su fundador, Marc Coloma, es un visionario emprendedor que tiene como objetivo cambiar a la industria alimentaria.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según nos dicen en su website, «Nuestra manera de promover mejores alimentos es hacerlos irresistibles». Y, en boca de quienes han probado Heura, lo han conseguido.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hemos entrevistado a Marc Coloma para que nos dé su visión sobre Heura, la industria alimentaria y el futuro de la alimentación.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>¿Qué tipo de alimentos produce Foods For Tomorrow?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En Foods For Tomorrow apostamos por alimentos que hagan salivar, sostenibles, nutritivos y que tengan un impacto positivo en el mundo.   Creemos que la manera de que estos alimentos tengan más presencia en nuestras dietas es que sean irresistibles y fáciles de cocinar. Es por eso que hemos creado Heura, un ingrediente que forma parte de una nueva generación de proteínas vegetales con texturas fibrosas y melosas que está seduciendo incluso a los paladares más exigentes.  </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Queremos impulsar un modelo productivo que sea sostenible y respetuoso con los recursos actuales del planeta con tal afrontar el reto de alimentar a una población mundial en aumento con una demanda cada vez mayor de alimentos. Es por ello que nos centramos en impulsar las proteínas vegetales para afrontar nuestros retos alimentarios.<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2017/06/19/nace-foods-tomorrow-una-empresa-dispuesta-conquistar-los-supermercados-con/" target="_blank">Nace Foods For Tomorrow, una empresa dispuesta a conquistar los supermercados con alternativas a la carne</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tenemos claro que la mejor manera de solucionar un sistema alimentario fallido es creando un nuevo modelo que haga obsoleto el existente, es por ello que trabajamos día a día para seguir desarrollando alimentos revolucionarios. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>¿Qué problemas genera nuestro actual sistema alimentario?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El modelo alimentario actual para producir proteínas es una enorme fuente de desigualdad e insostenibilidad.  </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Usar a los animales como intermediarios para producir alimentos es el principal motivo de injusticia alimentaria que existe en nuestro planeta. Se emplean millones de toneladas de legumbres y cereales producidas en países del sur para engordar a los animales en las granjas de países del norte, obteniendo en ese proceso mucho menos alimento del que se les da. Por ejemplo, para producir 1kg de carne de ternera se necesitan 20kg de cereales mientras que se necesita solo 0.5kg de soja para producir 1kg de Heura. Con la misma cantidad de legumbres y cereales podemos elegir entre conseguir 10 raciones de ternera o 400 de Heura, la diferencia es entre alimentar a una familia o a todo un vecindario. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12298} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/foto_1.png" alt="" class="wp-image-12298"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La cantidad de animales en granjas es 9 veces mayor que el número de personas viviendo en el planeta (66 vs 7,5 mil millones). Esta magnitud tiene un impacto ambiental brutal ya que, por ejemplo, el metano que emiten las vacas es entre 25 y 100 veces más contaminante que el CO2. Además se tienen que sumar los efectos de la ganadería en la deforestación, la pérdida de fauna y flora y el derroche de agua, entre otros. Es por todo esto que Worldwatch Institute señala la práctica ganadera como el principal responsable del cambio climático.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por otro lado, la primera causa de muerte en los países occidentales están relacionadas con las enfermedades cardiovasculares, las cuales están vinculadas con la abundante ingesta de proteínas animales.  El enorme impacto que tienen unos productos tan arraigados en nuestro día a día es sobrecogedor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La ganadería sigue un modelo en el que unos pocos se llenan los bolsillos a costa de que la mayoría perdamos lo que más nos importa. Es por ello que nuestra misión es impulsar un nuevo modelo productivo en el que todos nos podamos sumar y que no deje a nadie de lado. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>¿Por qué las alternativas a la carne de los supermercados están tan lejos de la calidad de los productos de Foods For Tomorrow si ellos cuentan con mucho más presupuesto para producirlos?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La principal diferencia es que las alternativas a la carne de los supermercados se han creado para satisfacer principalmente a un público vegano y vegetariano. Estas empresas veían a las proteínas vegetales como sucedáneos de la carne, pero nosotros creemos en impulsar sus sucesores. Este enfoque lo cambia todo.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12299} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/foto2.png" alt="" class="wp-image-12299"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No nos conformamos con los productos y las soluciones que habían actualmente en el mercado y por eso nos esforzamos para llevar a las proteínas vegetales un paso más adelante. Dedicamos un gran esfuerzo a investigar y desarrollar un proceso que nos permita conseguir un mordisco fibroso, meloso y único en cada una de nuestras piezas, siempre usando materias primas de calidad y 100% vegetales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>  Heura es el fruto de estar convencidos de que la clave para el éxito de las proteínas vegetales es que sean tan buenas y atractivas que nadie pueda evitar escogerlas. Adaptamos algo tan tradicional como las legumbres a los tiempos actuales; fáciles de cocinar, completas a nivel nutricional, muy versátiles, y sobre todo deliciosas.  </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Heura es la primera línea que ha lanzado Foods For Tomorrow, háblanos de ella. ¿Qué la diferencia de otras alternativas a la carne ya existentes?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La primera gran diferencia es que es un producto concebido desde el emprendimiento social y como consecuencia, nuestra misión es la de acelerar el cambio hacia un sistema alimentario más justo.  </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Respecto al resto de proteínas vegetales, la diferencia más importante es que formamos parte de la tercera generación de proteínas vegetales y es por ello que aportamos una experiencia organoléptica única. De hecho, cuando carniceros y reconocidos chefs prueban Heura, muchos piensan que están comiendo carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La primera generación de proteínas vegetales está representada por los coagulados y fermentados de la soja como el tofu o el tempeh. La segunda, son las legumbres extrusionadas en seco (como la soja texturizada). La tercera generación está formada por aquellos productos que se producen mediante un proceso de extrusión con cocción, permitiendo conseguir unas texturas muy distintas a las que conocíamos hasta ahora.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Una vez creada la estructura, le añadimos nuestros marinados para que solo haga falta dorar Heura en la sartén. También seguimos un proceso productivo que hace que Heura sea apto para celíacos, y enriquecemos Heura con vitamina B12, consiguiendo un ingrediente mucho más interesante nutricionalmente que cualquier otra proteína animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://www.elespanol.com/omicrono/" target="_blank">La hamburguesa que sangra y sabe a carne sin tener que matar animales</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Además, desde Foods for Tomorrow tenemos un proyecto paralelo llamado “Movimiento Foods for Tomorrow” que va a funcionar como base social con el objetivo de destinar una parte importante de nuestros esfuerzos a acelerar el progreso hacia un sistema alimentario más justo y saludable. Dentro de este “movimiento” y de forma anual abriremos una convocatoria para destinar un porcentaje de nuestros beneficios a proyectos sociales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Buena parte de los consumidores creen que es necesario comer carne a menudo, pero, ¿nuestros hábitos alimentarios moldean a la industria alimentaria o la industria alimentaria moldea nuestros hábitos alimentarios?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Durante los últimos 50 años las campañas publicitarias de grandes empresas alimentarias han potenciado la idea de que la carne es indispensable en nuestra dieta, sobretodo para conseguir proteínas. Pero hemos llegado a un punto en el que las asociaciones sanitarias más importantes y reconocidas del mundo como la OMS están pidiendo la reducción del consumo de carne y alarmando a la población de sus terribles consecuencias para la salud y el medio ambiente. La ciencia nos está apuntando hacia la dirección contraria a la que la industria cárnica quiere que vayamos.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12300} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/foto3.png" alt="" class="wp-image-12300"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Bajo mi punto de vista, actualmente está siendo la sociedad quien está moldeando la industria alimentaria ya que estamos moviéndonos hacia unos nuevos hábitos alimentarios más saludables y sostenibles. Durante los últimos años, en España, está habiendo una disminución del consumo de carne y la sociedad española demanda más alimentos basados en proteínas vegetales, despertando así el interés de las empresas cárnicas en producir y comercializar alimentos basados en proteínas vegetales.   Esta situación significa progreso, ya que sobre la mesa está el hecho de que podemos elegir de qué origen queremos que sea la fuente de nuestras proteínas y las empresas que quieran evitar desaparecer del mercado alimentario tendrán que adaptar su oferta a las necesidades actuales. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>¿Cómo ves el futuro de la industria alimentaria?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Creo que en las próximas décadas se viene una gran revolución alimentaria a la altura de la revolución tecnológica que hemos vivido estos últimos 20 años.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12301} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/foto4.png" alt="" class="wp-image-12301"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Habrá cambios estructurales importantes en las materias primas usadas, en la manera de producir, distribuir y adquirir alimentos. Junto a otras tecnologías e innovaciones, las proteínas vegetales de nueva generación y la clean-meat van a formar parte de la actualidad informativa y de nuestro día a día.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El modelo productivo que usamos para alimentar a una población en aumento exponencial y en un clima cada vez más inestable, hace que sea ineludible afrontar que la ganadería está generando los mayores problemas y retos a afrontar como humanidad en este siglo XXI. Tenemos la necesidad de cambio hacia un sistema mucho más eficiente, y Foods for Tomorrow nace para acelerar este proceso.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>¿Para cuándo Heura en Carrefour, Lidl, Alcampo…?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En octubre entraremos en una cadena de gran distribución nacional y estamos trabajando muy duro para que la revolución que supone Heura se extienda y sea cada vez más accesible.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para nosotros es clave que en los grandes centros de distribución haya opciones de calidad basadas en proteínas vegetales, ya que eso ofrece más libertad de elección a las personas. Si no podemos escoger, no hay libertad.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>¿Cuáles son los próximos productos que tiene pensado lanzar Foods For Tomorrow?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nuestro objetivo es desarrollar productos con diferentes formatos y basados en diferentes tipos de legumbres y vegetales que sorprendan por sus texturas y sabores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> En noviembre lanzaremos una nueva marca con alimentos muy interesante en formatos que la población española consume día a día. Por otro lado, también estamos trabajando para aportar Heura en nuevos formatos y sabores que vamos a presentar a finales de este año.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Seguiremos avanzando día a día y aportando nuestro granito de arena en crear un sistema alimentario más justo, que tenga en cuenta al planeta y a todos los que vivimos en él.</p>
<!-- /wp:paragraph -->