<!-- wp:paragraph -->
<p>El próximo Martes 20 de Marzo se celebra en todo el mundo el Día Internacional Sin Carne, día en el que diferentes colectivos, organizaciones e indivíduos promueven el vegetarianismo. En Igualdad Animal y al igual que hicimos el año pasado queremos aprovechar el día para llamar la atención sobre la terrible injusticia que padecen los animales en granjas y mataderos de todo el mundo, y lo sencillo que resulta acabar con tantos padecimientos
Por ello, hemos convocado actos de protesta en diversas ciudades del mundo. Actividades que incluyen entrevistas, charlas, proyecciones, concentraciones etc.
!Ayuda a que estas convocatorias tengan máxima difusión!

<strong>Madrid, España:</strong>
Fecha - Martes, 20 de Marzo
11 horas - Entrevista en el programa de Cadena Ser "Hoy por Hoy".
12 horas - Acto protesta y concentración en la Puerta del Sol. Varios activistas de Igualdad Animal se "enjaularán" desnudos en jaulas reales de batería extraidas por Igualdad Animal de una granja de cría intensiva. Se proyectará además el documental "Earthings" en la calle y realizará una concentración en el mismo lugar.
19 horas - Proyección del documental "Reino Apacible / Peaceable Kingdom" en la Librería Traficantes de Sueños. Dirección: Embajadores 35, Local 6
Barrio Lavapiés / Metro: Embajadores / Lavapies

Si quieres participar en cualquiera de estas activiades escribe a:
info@igualdadanimal.org
Tel - 675 737 459

<strong>Lima, Perú:</strong>
Fecha - Domingo 18 de Marzo.
17 horas.
Igualdad Animal realizará una concentración y protesta el Domingo 18 de Marzo de 17 a 20 horas con motivo del Día Internacional sin carne en el Parque Kennedy de Miraflores en Lima, Perú. Durante el acto varios actvistas de Igualdad Animal se "plastificarán" dentro de unas enormes bandejas similares a las que se encuentrasn en los supermercados.

Fecha - Martes 20 de Marzo.
11 horas.
El Martes 20 de Marzo con motivo del día internacional sin carne Igualdad Animal realizará un acto simbólico en la Universidad Ricardo Palma de Lima a partir de las 11 a.m. Se mostrarán carteles alusivos al tema y se colocará una mesa informativa.
Si quieres participar:
peru@igualdadanimal.org

<strong>Caracas, Venezuela:</strong>
Fecha - martes, 20 de Marzo.
De 12 a 14 horas.

Lugar: Plaza Brion de Chacaito, Municipio Chacao, Miranda, Caracas.
Con este evento Igualdad Animal estrena con ilusión su grupo de trabajo en Caracas (Venezuela), grupo que estará bajo la coordinación del activista por los derechos de los animales Andres Lizardo. Durante el acto dos activistas de Igualdad Animal en Venezuela se introducirán en unas bandejas similares a las que encontramos en los supermercados para promover el vegetarianismo. Se repartirá además información sobre nutrición, dieta y ética vegetariana.

Más información:
andresl@igualdadanimal.org
venezuela@igualdadanimal.org</p>
<!-- /wp:paragraph -->