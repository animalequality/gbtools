<!-- wp:paragraph -->
<p>Nuestro equipo en Italia acaba de lanzar una nueva investigación llevada a cabo en colaboración con el programa de noticias de TG2 que muestra la condición real de las gallinas dentro de la industria del huevo mantenidas en jaulas en Italia.

A finales de abril de 2018, nuestro equipo de investigación se reunió con Piergiorgio Giacovazzo, enviado y presentador de Tg2, en una granja de gallinas en Mantua. La granja ya había sido investigada por Igualdad Animal, pero, aunque las condiciones ya eran preocupantes y ya habían sido reportadas, la situación no mejoró: los investigadores se <a href="https://www.flickr.com/photos/animalequalityitalia/sets/72157670744572018/with/43102812132/">enfrentaron con escenas</a>, de ser posible, aún más desconcertantes.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/8RLNkWphOmQ" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
En total contraste con todo lo que se concibe como bienestar animal, los investigadores han encontrado decenas de miles de gallinas apiladas en jaulas en números a menudo más altos que los permitidos, cubiertas de tierra y rodeadas de cadáveres de ratones y gallinas podridas en el piso del establecimiento.

La granja también estaba infestada de ácaros rojos, de los cuales las gallinas y los huevos estaban completamente cubiertos. Una infestación tan extensa implica numerosos riesgos para la salud de los animales, obligados a someterse a la invasión de los parásitos sin posibilidad alguna de deshacerse de ellos.

Hemos consultado al doctor Enrico Moriconi, veterinario y garante de los Derechos de los Animales de la Región de Piamonte, quien destacó que «la presencia de estos parásitos, si no se tratan rápidamente, también puede conducir a la explosión de casos reales de salmonella. Además, la cáscara de huevo es porosa y las malas condiciones de higiene pueden conducir a la entrada de bacterias en los huevos para el consumo de alimentos ».
Además de todo esto, muchos animales estaban atrapados en las jaulas sin piso, con los cuerpos demacrados sin plumas y las crestas bajas y pálidas debido a la anemia.

Algunos de los hallazgos documentados en el video muestran:
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent1"} -->
<p class="rteindent1">- Gallinas y huevos completamente infestados de ácaros rojos
-&nbsp;Cadáveres de ratones y gallinas sobre el piso del establecimiento
- Gallinas en jaulas caminando sobre los cadáveres en descomposición de sus compañeras
- Gallinas con cuerpos sin plumas y con uñas crecidas fuera de toda proporción
- Suciedad en todas partes y granja cubierta de telarañas y polvo
- Gallinas con piernas rotas atrapadas en las jaulas
Hemos documentado la situación con videos y <a href="https://www.flickr.com/photos/animalequalityitalia/sets/72157670744572018/with/43102812132/">fotografías</a>, y los hemos entregado a los &nbsp;Carabinieri Forestali dentro de una queja formal contra los propietarios de la granja. Las condiciones de estos animales son, de hecho, inaceptables para cualquiera, y peligrosas para los consumidores, que tienen derecho a conocer la realidad detrás de estas granjas.

En Italia hay alrededor de 42 millones de gallinas que se crían cada año en granjas intensivas, con un porcentaje de gallinas en jaulas que cubren más del 65% del total. Todo esto sucede en un contexto en el que Italia juega un papel muy importante. De hecho, este país es uno de los mayores productores de huevos en Europa.

Desafortunadamente, muchas investigaciones y escándalos han demostrado que a menudo el manejo de estos lugares es altamente problemático y perjudicial para el bienestar de las gallinas, y que la jaula es una metodología que no respeta las necesidades naturales de la especie.

En conjunción con la investigación, pusimos en marcha una petición dirigida a Assoavi, Unaitalia y al ministro de agricultura, Gian Marco Centinaio, con el fin de poner fin a la utilización de jaulas, un método cruel rechazado por la mayoría de los consumidores italianos y europeos.

«Las jaulas son un sistema de reproducción que inflige terribles sufrimientos a los animales. Pedimos que en Italia se tome nota de esta situación y se escuche a los muchos ciudadanos y empresas del sector agroalimentario que ya se han alineado contra esta práctica extremadamente cruel », dice Matteo Cupi, nuestro director ejecutivo en Italia.

«Gracias a nuestra investigación y a la Tg2, todos los horrores de estas granjas claramente emergen, y es por eso que las asociaciones comerciales y también el Ministerio deben asumir la responsabilidad de responder adecuadamente. Debemos avanzar rápidamente para resolver estos graves problemas y poner fin a estos increíbles sufrimientos a los que están sujetos los animales, forzados a estas sórdidas jaulas», concluye Cupi.

En españa el 88 % de la gallinas dentro de la industria del huevo, es decir, más de 41 millones de gallinas, permanecen encerradas durante toda su vida en jaulas de alambre donde nunca ven la luz del sol ni pueden estirar las alas. Todo esto ocurre a pesar de que la Encuesta del Consumidor 2017 realizada por KantarWorldPanel revela que el 70% de los españoles prefieren consumir huevos de gallinas en libertad.

«La cría de gallinas en jaulas es uno de los sistemas que más sufrimiento genera a los animales en la ganadería industrial. Los consumidores tienen derecho a conocer esta información, y en Igualdad Animal vamos a trabajar sin descanso para poner fin a este sistema cruel», asegura Javier Moreno, director de Igualdad Animal.

En España tenemos en estos momentos una campaña pidiendo a Supermercados Día &nbsp;que adopte un compromiso para dejar de vender huevos de gallinas enjauladas e insta a esta empresa a unirse al resto de supermercados como Mercadona, Carrefour, Eroski, Lidl, Aldi, El Corte Inglés y más de 400 empresas en todo el mundo, adoptando una política para poner fin a esta cruel práctica.

Por favor,<span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.org/noticias/"> firma la petición</a></span> ahora pidiendo a Supermercados Dia en España que dejen vender huevos de gallinas que pasan toda su vida en jaulas.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<script async="" src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!-- /wp:html -->