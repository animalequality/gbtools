<!-- wp:image {"id":12337} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/vegan_cheese_farmer_pearce_julian.jpg" alt="" class="wp-image-12337"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>«Vamos a tratar a los animales como a ellos le gustaría que los trataran».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Así lo vieron todo Carol y Julian Pearce luego de dedicar <strong>20 años de su vida a la producción de leche</strong> y queso de cabra.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A diferencia de lo que ocurre con las prácticas estándar de la industria láctea estándar, la pequeña granja de los Pearce tenía una estricta política de No-Matar.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Noticia relacionada: <a href="https://igualdadanimal.org/noticia/2015/09/25/8-crueles-practicas-estandar-de-la-industria-lactea-con-las-vacas/" target="_blank">8 crueles prácticas estándar de la industria láctea con las vacas</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Jamás enviaron al matadero a cabras cuyo nivel de producción de leche había descendido y trataban a todos los animales con «amor y dulzura», según ellos mismos afirman.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Además <strong>rescataban animales como vacas, cerdos, gallinas y pollos</strong> que habían sido maltratados o para salvarlos de ir al matadero.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>Entonces, un día tuvieron que tomar una decisión muy importante: si querían seguir siendo un refugio de animales de granja <strong>no podían seguir criando animales para producir leche.</strong></p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p>«Ser una granja de productos lácteos todavía era un problema. &nbsp;Al no criar, podemos salvar a los animales de la crueldad en lugar de apoyarla trayendo más al mundo».</p></font>

<p>&nbsp;</p>

<figure><img alt="" class="wp-image-12338" src="/app/uploads/2017/08/vegan_cheese_farm_goat.png" style="margin-left: 10px; margin-right: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>Fue así como pasaron de ser los galardonados fabricantes de queso de cabra a producir queso a partir de leche de anacardo y nueces.</p>

<p>Para que den leche, las vacas, cabras y otras madres de la industria deben estar embarazadas. <strong>Luego del parto sufren inmensamente cuando sus hijos les son arrebatados.</strong> Lloran por días y los llaman desesperadamente y nunca los vuelven a ver.</p>

<p>A las hembras las incorporan a este ciclo de crueldad y maltrato, y a los machos (no rentables para la industria) <strong>los matan siendo aún unos bebés.</strong></p>

<p>&nbsp;</p>

<p>Los Pearce comprendieron que no solo podemos vivir sin comer animales sino que ser compasivos y respetuosos es lo mínimo que podemos hacer por todos aquellos con quienes compartimos este mundo.</p>

<p>&nbsp;</p>

<p>Fuente:</p>

<p><a href="https://freefromharm.org/animal-products-and-ethics/former-meat-dairy-farmers-became-vegan-activists/" target="_blank">https://freefromharm.org/animal-products-and-ethics/former-meat-dairy-farmers-became-vegan-activists/</a></p>

<p><a href="https://www.sanctuaryatsoledad.org/our-story.html" target="_blank">https://www.sanctuaryatsoledad.org/our-story.html</a></p></font></font>
<!-- /wp:html -->