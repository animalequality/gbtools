<!-- wp:image {"id":12023} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/shutterstock_253873549.jpg" alt="" class="wp-image-12023"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La organización Igualdad Animal trabaja incansablemente para lograr cambios que permitan erradicar el maltrato hacia los animales en las granjas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Conoce cuales son los logros que en en estos primeros 4 meses de 2017 la organización ha logrado, y que contribuirán a reducir el sufrimiento de los animales en las granjas industriales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>1.En Italia también Carrefour dice “no” a los huevos de gallinas enjauladas.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Francia fue el primer país de este grupo multinacional en comprometerse a ser libre de jaulas. Tras conversaciones con Igualdad Animal y una campaña de Compassion in World Farming, Carrefour Italia se suma al compromiso de abandonar los huevos de jaula este mismo año.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>2. Bunge deja atrás a los huevos de jaulas en Brasil.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12024} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/28768099774_792bf23804_z.jpg" alt="" class="wp-image-12024"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Conocida por su producción de mayonesa, Bunge es una de las principales empresas de alimentos en el mundo que se une a Unilever y Cargill para abandonar los huevos provenientes de jaulas. <strong>A partir del 2025 la empresa hará efectivo dicho compromiso.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las gallinas que son obligadas a vivir en jaulas son víctimas de una crueldad extrema. Están confinadas en espacios mínimos donde no tienen otra opción más que pisarse unas a otras. No pueden extender sus alas y sus frágiles patas siempre pisan el alambre de las jaulas, que las lacera y les provoca graves heridas y cortes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>3. La cadena de supermercados Auchan ya no venderá más huevos de gallinas en jaulas en Italia.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Uno de los principales supermercados en Italia, Auchan, cambiará la vida de cientos de miles de gallinas luego de que deje de vender huevos provenientes de jaula para 2022. La decisión ha sido el resultado de conversaciones con Igualdad Animal y otras organizaciones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Puedes suscribirte gratuitamente a nuestro e-boletín</a> para que recibas las mejores noticias de actualidad sobre los animales y conozcas más opciones de alimentación.</p></font>

<p>&nbsp;</p>

<strong>4. La empresa Esselunga ya no comprará más huevos de gallinas enjauladas.</strong>

<p>Es el tercer mayor supermercado de Italia <strong>ha anunciado una nueva política que elimina de su cadena de producción los huevos provenientes de gallinas enjauladas y la cual se hará efectiva este mismo año.</strong> La empresa ha extendido dicha política a la mayoría de los productos de su marca que contienen huevo.</p>

<p>La impactante investigación sobre gallinas en jaulas que lanzó Igualdad Animal en Italia en marzo de este año, expuso el sufrimiento al que son sometidas las gallinas en una granja de Lombardía, la mayor productora de huevos del país.</p>

<p>La investigación logró un significativo impacto mediático y que muchas empresas como Giovanni Rana, GEMOS y Dussmann también se distanciarán públicamente de la cruel práctica.</p>

<p>A continuación el video de la investigación:&nbsp;</p>

<p><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/krJYhEtDW7w" width="854"></iframe></p>

<p><br>
&nbsp;</p>

<strong>5. La empresa Dussmann y GEMOS dejarán de proveerse de huevos de jaula.</strong>

<p>Estas dos empresas de catering son las más recientemente comprometidas a dejar de proveerse de huevos que provengan de granjas de gallinas enjauladas.</p>

<p><strong>Dunsmann, la quinta empresa de catering más grande en Italia, se comprometió a hacerlo para el mismo mes de marzo</strong> en el cual fue lanzada la investigación de Igualdad Animal en la mayor productora de huevos del país.</p>

<p>Gracias a dicha decisión, fruto del intenso trabajo del Departamento de Responsabilidad Social y Corporativa de Igualdad Animal en Italia, se reducirá de forma inmediata el sufrimiento de 12.000 aves al año.</p>

<p>&nbsp;</p>

<font size="5"><font color="#808080"><p>Puedes hacer mucho más para ayudar a las gallinas si sustituyes los huevos en tu alimentación por alternativas a su consumo. Es muy fácil hacerlo. <a href="http://www.igualdadanimal.org/noticias/7713/6-recetas-para-ayudarte-sustituir-los-huevos-en-tu-alimentacion" target="_blank">¡Aquí te explicamos cómo!</a></p></font>

<p>&nbsp;</p>

<strong>6. General Mills no usará más huevos provenientes de jaulas en sus productos.</strong>

<figure><img alt="" class="wp-image-12025" src="/app/uploads/2017/05/33026712356_558c03d0b9_z.jpg" style="margin-left: 10px; margin-right: 10px; float: left; width: 449px; "></figure><p></p>

<p>Como resultado de la campaña de <a href="https://openwingalliance.org/es/" target="_blank">Open Wing Alliance</a>, una coalición de la cual forma parte Igualdad Animal, General Mills, una de las mayores multinacionales de alimentos, se comprometió a dejar de comprar huevos que provengan del cruel sistema de jaulas.</p>

<p>El compromiso <strong>se hará efectivo en 2025 y afectará a reconocidas marcas como Häagen-Dazs, Cheerios, Trix, Old el Paso, Nature Valley y Yoplait</strong>, que tienen presencia en más de 100 países.</p>

<p>Con esto, la empresa se suma a otras grandes internacionales como McDonald's, Burger King, Lidl y Sodexo, que ya se han comprometido a dejar de proveerse de huevos de gallinas enjauladas.</p>

<p>Parte de la campaña de Open Wing Alliance implicó una petición online en más de 12 idiomas firmada por más de 28.000 personas, emails a Directores Generales de General Mills y miles de comentarios de consumidores en redes sociales. &nbsp;</p>

<p>&nbsp;</p>

<strong>7. El gigante de la pasta Barilla no comprará huevos de jaulas a sus proveedores en Brasil.</strong>

<p>2020 será el año para el cual Barilla, la marca de pasta líder a nivel mundial, eliminará las jaulas en su cadena de proveedores de huevos en Brasil.</p>

<p>Dicho compromiso surge tras conversaciones entre la empresa e Igualdad Animal Brasil, Humane Society International y Forum Animal.</p>

<p>Las gallinas confinadas en jaulas soportan un terrible sufrimiento. Gracias a estas decisiones <strong>podrán desarrollar comportamientos básicos como extender sus alas, algo que no es posible dentro del cruel sistema de jaulas.</strong></p>

<p>El 3 de mayo de este año Igualdad Animal lanzó su primera investigación en la industria del huevo en Brasil, haciendo públicos los abusos que sufren las gallinas enjauladas en las granjas de producción de huevos del país.</p>

<p>A continuación, el video de la investigación:</p>

<p><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/C1xjYqppnOQ" width="854"></iframe></p>

<p>&nbsp;</p>

<strong>8. International Meal Company se compromete a que su suministro de huevos no proceda de gallinas enjauladas en Brasil.</strong>

<p>A principios de 2017, la International Meal Company se sumó a McDonalds, Burguer King y otras empresas en su compromiso para poner fin al confinamiento en jaula de las gallinas en Brasil. Dicha decisión se dio a conocer luego de las negociaciones mantenidas con Igualdad Animal y otras organizaciones.</p>

<p><strong>Esta empresa dirige más de 380 restaurantes de Brasil, Colombia, México, Panamá, Puerto Rico y la República Dominicana.</strong> Entre las cadenas de restaurante de la compañía se incluyen Olive Garden, Carl’s Jr., Viena, Frango Assado, Red Lobster, Wraps, Go Fresh, Brunella, Gino’s y Airports Concepts/Airport Shoppes.</p></font></font>
<!-- /wp:html -->