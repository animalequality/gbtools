<!-- wp:paragraph -->
<p>
	EFE - Rusia inauguró hoy en su capital un monumento a los perros con los que se realizaron experimentos e investigaciones médicas especiales que permitieron el primer vuelo del hombre al espacio, hazaña protagonizada por el cosmonauta soviético Yuri Gagarin hace 50 años.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	<br>
	"Para que pudiera tener lugar el vuelo del hombre al espacio se llevaron a cabo numerosos experimentos, en los que participaron un total de 42 perros", indicó a la agencia Interfax Ígor Bujtiyárov, jefe del Instituto de Medicina Militar del Ministerio de Defensa de Rusia.<br>
	<br>
	<strong> <img alt="" class="wp-image-13576" src="/app/uploads/2011/04/000estatualaika.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 296px; "> </strong></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>
	<strong><iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FIgualdadAnimal&amp;width=400&amp;colorscheme=light&amp;show_faces=false&amp;stream=false&amp;header=false&amp;height=62" style="border: medium none; overflow: hidden; width: 400px; height: 62px;"></iframe></strong></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>
	Bujtiyárov, quien rechazó responder a la pregunta de cuántos canes murieron en los experimentos, subrayó que "el perro, que siempre fue amigo del médico, fue sometido a ensayos muy complicados y muy importantes para la medicina."<br>
	<br>
	Por eso, agregó, se ha inaugurado hoy un monumento a uno de ellos frente al edificio del Instituto de Medicina Militar.<br>
	<br>
	"Los perros soportan <i>bien</i> las sobrecargas, la altura, las vibraciones, los mareos y demás factores", señaló Bujtiyárov.<br>
	<br>
	<a href="https://www.facebook.com/album.php?aid=353478&amp;id=116189118344" target="_blank"><img alt="Fotos de los 36 beagles rescatados por simpatizantes de Igualdad Animal de un centro de cría para vivisección" class="wp-image-13815" src="/app/uploads/2011/04/beagles-rescatados.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: right; width: 200px; "></a>Lo más difícil era enseñarles a permanecer en la cápsula. En eso se invertía gran parte del tiempo, a veces entre tres y cinco meses, y aún así, algunos perros no podían adaptarse a ese estado de aislamiento e inmovilidad y se les retiraba de los experimentos, precisó.<br>
	<br>
	Las perras Belka y Strelka fueron en 1960 los primeros <a href="http://www.igualdadanimal.org/antiespecismo">ejemplares de su especie</a> en realizar con éxito un vuelo orbital que les llevó de vuelta a la Tierra sanos y salvos.<br>
	<br>
	En 1983, la Unión Soviética envió al cosmos a los primeros monos, Abrek y Bion, y a los dos años a otra pareja de simios, Verni y Gordi, después a Drema y Yerosha (1987), a Zhakonia y Zabiyaka (1989), a Krosh e Ivasha (1992) y a Lapik y Multik (1996).<br>
	<br>
	El año pasado se anunció que los científicos rusos renunciaban a enviar más perros y monos al espacio para llevar a cabo experimentos.<br>
	<br>
	"Desde el punto de vista de las estadísticas y de la realización de investigaciones, es mejor trabajar con animales pequeños que con grandes", indicó entonces Yevgueni Ilin, investigador jefe del Instituto de Problemas Biomédicos de la Academia de Ciencias de Rusia.<br>
	<br>
	Por lo tanto, agregó, "al menos en los próximos diez años, si no más, no volverá a haber monos, ni una Belka ni una Strelka en el espacio".<br>
	<br>
	"Nuestra tarea principal es estudiar los cambios a nivel celular, molecular y genético. Para ello son <i>necesarios</i> animales de líneas consanguíneas, preferiblemente ratones, con los que experimenta todo el mundo científico y para los que ya se han elaborado métodos de investigación", precisó.<br>
	<br>
	Agregó que los animales más grandes, para los que además se necesita un mayor espacio, sirven para hacer investigaciones fisiológicas, las mismas que se pueden llevar a cabo con los cosmonautas de la Estación Espacial Internacional. EFE (Fuente: ABC)<br>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>
	<a href="https://es.wikipedia.org/wiki/Animales_en_el_espacio" target="_blank"><img alt="Animales en el Espacio" class="wp-image-13644" src="/app/uploads/2011/04/300px-Baker.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 200px; "></a><br>
	<br>
	<a href="https://es.wikipedia.org/wiki/Animales_en_el_espacio">Todo sobre las víctimas: Animales en el Espacio.</a> Los experimentos con animales en el espacio sirvieron originalmente para probar la supervivencia en los vuelos espaciales antes de intentar misiones espaciales tripuladas. En numerosas ocasiones los animales tripularon vehículos espaciales para investigar diferentes procesos biológicos y los posibles efectos que la microgravedad pudiera tener. A fecha de 2004, sólo seis países han llevado animales al espacio: los Estados Unidos, la Unión Soviética, Francia, Japón, China y Argentina. <a href="https://es.wikipedia.org/wiki/Animales_en_el_espacio">(Continúa)</a><br>
	<br>
	Laika, muerte en el espacio.<br>
	<strong> <a href="https://es.wikipedia.org/wiki/Laika" target="_blank"><img alt="Laika, una muerte en el espacio" class="wp-image-13745" src="/app/uploads/2011/04/38393381_laika300.jpg" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 10px; float: left; width: 200px; "></a><br>
	</strong></p>
<!-- /wp:paragraph -->