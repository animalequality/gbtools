<!-- wp:image {"id":13634} -->
<figure class="wp-block-image"><img src="/app/uploads/2010/11/2279946892_06ba457446.jpg" alt="" class="wp-image-13634"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>
Los musulmanes celebraron ayer su tradicional «Fiesta del cordero», día en el que se conmemora que Alá perdonase la vida del hijo de Ibrahim, a cambio de la vida de un cordero. Se estima que durante todo el día se sacrificaron 400.000 corderos en toda España como parte de la celebración. Solo en Ceuta y Melilla se mataron 10,000 corderos durante el día de ayer.<br><br>
Para matar al animal un matarife le coloca con las patas atadas mirando hacia La Meca -en recuerdo al sacrificio de Abraham-, y se le degüella con un cuchillo afilado que corta la vena yugular y la arteria carotida de ambos lados dejando intacta la espina dorsal. Un tercio de la carne se destinará a las familias que no tengan suficiente dinero para realizar esta ofrenda a Alá. La matanza debe realizarse bajo la supervisión de un veterinario.<br><br>
Los animales no son aturdidos ni reciben ningún tipo de descarga eléctrica, si no que se desangran estando plenamente conscientes. Debido al derecho constitucional de la libertad de culta, la normativa actual permite la ausencia de aturdimiento previo al sacrificio de animales de abasto por motivo de sacrificio ritual, encontrándose amparado por el artículo 5.2 del real Decreto 54-1925. Esta situación no es muy diferente a la que ocurre en los mataderos españoles según hemos podido documentar activistas de Igualdad Animal en una investigación realizada durante el año 2008, durante la misma comprobamos como la mayoría de los corderos permanecían todavía consciente mientras se desangraban.<br>
https://vimeo.com/5632402<br><br>
Este rito religioso no es más que otra muestra del sufrimiento que se les ocasiona a los animales en nombre de la tradición.<br><br>
Desde Igualdad Animal rechazamos y denunciamos la muerte de los corderos durante la celebración de este rito pero también su sufrimiento y muerte en las miles de mataderos que hay en España. Se calcula que mueren aproximadamente 20 millones de estos animales en nuestro país para consumo humano al año. Los corderos consumidos para carne son los llamados "lechal" o "pascual", animales de 3 o 4 meses de edad que todavía están en periodo de lactancia.<br><br>
Los animales tienen interés en vivir, por ello, la muerte es un perjuicio para ellos, al igual que para nosotros, independientemente de que esta se realice con o sin aturdimiento. Desde Igualdad Animal promovemos un estilo de vida vegano basado en el respeto a los animales y que excluye los productos de origen animal.<br><br>
Contacto de medios:<br>
Sharon Núñez<br>
91 522 22 18 / 609 980 196<br>
sharonn@igualdadanimal.org<br></p>
<!-- /wp:paragraph -->