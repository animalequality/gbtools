<!-- wp:paragraph -->
<p>«Si comes carne tienes que aceptar que el animal tiene que morir», afirmaba el fotógrafo <a href="http://www.everettmeissner.com/vermont-packing/" target="_blank">Everett Meissner</a> tras su visita al matadero Vermont Packinghouse, cuya política es que si lo quieres, puedas ver <strong>cómo mueren los animales</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«La muerte es parte de la vida», afirmaba por su parte el propietario del matadero. Los animales, sin embargo, no pudieron <a href="https://igualdadanimal.org/noticia/2016/03/08/que-nos-dirian-los-animales-de-granja-si-pudieran-hablar/" target="_blank">dar su opinión</a> al respecto <a href="https://www.huffpost.com/entry/slaughterhouse-photos-transparency-vermont_n_57bde785e4b085c1ff270085?ir=Green" target="_blank">en el artículo</a> de Huffington Post. Suele pasar: <strong>a las víctimas rara vez se les pide opinión</strong>; menos aún si son animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Este matadero en Estados Unidos permite visitas y hasta acepta a niños y niñas en visitas escolares programadas. Meissner coincidió con <a href="http://www.everettmeissner.com/vermont-packing/flqrgw7ccynes8lp3k841c8qf6x4rp" target="_blank">un grupo de adolescentes</a> que observan <strong>entre horrorizados y perdidos</strong> el proceso de la carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10997} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/57bdc360170000aa0fc752b9.jpeg" alt="" class="wp-image-10997"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Fotografía <a href="http://www.everettmeissner.com/vermont-packing/" target="_blank">Everett Meissner</a></em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El contraste del blanco quirúrgico de las paredes y los delantales de los trabajadores y <strong>el rojo de la sangre aún caliente de los animales</strong> resulta estremecedor. Pero, si comes carne, esta es la realidad del producto por el que pagas en el supermercado.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En su política de puertas abiertas, este matadero con paredes de cristal puede estar consiguiendo <strong>todo lo contrario a lo que se propone</strong>. Porque lo que sucede dentro de un matadero, con o sin paredes de cristal, va profundamente <a href="https://igualdadanimal.org/noticia/2016/07/29/9-alternativas-la-carne-mas-saludables-y-llenas-de-proteinas/" target="_blank">en contra de los valores</a> de cada vez más consumidores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El maltrato animal no puede ser decorado; la muerte no es un producto que los consumidores compren gustosamente. En todo caso, <strong>la carne se compra a pesar de la violencia de los mataderos</strong> y no por ella.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://igualdadanimal.org/noticia/2016/07/14/angela-23-anos-deje-de-comer-carne-cuando-vi-videos-de-mataderos/" target="_blank">Cada vez más personas</a> reducen su consumo de carne. Otras deciden sustituirla por completo en su alimentación. Los consumidores saben que los animales no quieren morir.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La muerte es parte de la vida, efectivamente, <a href="https://igualdadanimal.org/blog/imagina-un-modelo-alimentario-que-salve-el-mundo-y-no-nos-haga-renunciar-nada/" target="_blank">pero la carne solo es una elección</a> y, como toda elección, <strong>podemos decir no</strong>. Así daremos voz a los animales que no pueden dar su opinión sobre morir en mataderos con paredes de cristal (o de hormigón).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Si quieres unirte a las personas que han decidido marcar la diferencia a través de su alimentación, por favor considera consumir alternativas vegetales a la carne. Tienes toda la información y recetas que necesitas en las fantásticas websites <a href="https://www.gastronomiavegana.org/" target="_blank">Gastronomía Vegana</a> y <a href="https://danzadefogones.com/" target="_blank">Danza de Fogones</a></strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
Fuente: <a href="https://www.huffpost.com/entry/slaughterhouse-photos-transparency-vermont_n_57bde785e4b085c1ff270085?ir=Green" target="_blank">https://www.huffpost.com/entry/slaughterhouse-photos-transparency-vermont_n_57bde785e4b085c1ff270085?ir=Green</a></p>
<!-- /wp:paragraph -->