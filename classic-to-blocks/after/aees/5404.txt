<!-- wp:paragraph -->
<p>
	<img align="left" alt="" height="194" hspace="9" class="wp-image-13647" src="/app/uploads/2010/04/36461211.jpg" vspace="9" width="300">Agentes de la Guardia Civil han detenido en Pedrajas de San Esteban (Valladolid) a diez jóvenes implicados en la matanza a golpes y patadas de unas cien gallinas el pasado 16 de febrero.<br>
	<br>
	Los jóvenes, cuatro de ellos menores de edad, entraron por la fuerza durante la noche en una granja donde se explotan gallinas para la producción de huevos. Según se han defendido, habían tomado unas copas de más y cumplían una tradición del pueblo, consistente en coger varios animales de esa granja para torturarles y matarles.<br>
	<br>
	El dueño de la granja, donde viven confinadas 100.000 aves, se ha lamentado diciendo que esta vez ha sido demasiado, no como en ocasiones anteriores en que no llegó a denunciar porque el número de animales masacrados fue mucho menor. El empresario ha calculado unas pérdidas de unos 2.000 euros y se ha quejado también de que “Además de los destrozos en las instalaciones y las bajas habidas, lo peor es los efectos en la producción ya que se somete a un estrés a las gallinas y eso repercute también en la puesta”.<br>
	<br>
	Los jóvenes que llevaron a cabo la masacre están a la espera de juicio por maltrato animal y daños, aunque puede que se les acuse también de robo con fuerza.<br>
	<br>
	Este tipo de noticias hacen que nos sorprendamos y horroricemos al ver hasta qué punto puede llegar la crueldad de algunas personas. Sin embargo, no podemos olvidar que cada año, sólo en España, unos 51 millones de gallinas son enviadas al matadero porque ya no es rentable seguir explotándolas para la puesta de huevos; que la mayoría de ellas pasa una vida corta y miserable en una jaula, casi sin poder moverse, muchas de ellas con huesos rotos por la descalcificación; que por cada gallina “ponedora” que está ahora mismo explotada, un pollito macho fue arrojado a una trituradora; Y que todo esto ocurre porque demandamos un producto que no necesitamos: el huevo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Conoce cómo son las gallinas pinchando <a href="http://www.granjasdeesclavos.com/gallinas/como-son">aquí.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>
	Puedes ver el rescate de 10 gallinas que hizo Igualdad Animal pinchando <a href="https://vimeo.com/2482885">aquí.</a></p>
<!-- /wp:paragraph -->