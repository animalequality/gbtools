<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal">Hoy viernes 12 de Marzo a las 11 horas, Igualdad Animal ha descolgado una espectacular pancarta de 35 metros cuadrados en la que se leía "<strong>Tauromaquia Abolición - Derechos para todos los Animales - Igualdad&nbsp;Animal</strong>" del Puente de Segovia de Madrid.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal"><span lang="EN-US">Durante esta acción, el activista de Igualdad Animal Tomás Eugenio García-Navas, se ha descolgado en rapel del puente de varias decenas de metros de altura<span>&nbsp;</span>con otra pancarta en la que se leía "<strong>Nuestra Esperanza: La Abolición</strong>". Una acción impactante en<span>&nbsp;</span>respuesta a la intención de la presidenta de la Comunidad de Madrid, Esperanza Aguirre, de considerar la tauromaquia un Bien de Interés Cultural.</span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal"><span lang="EN-US">El activista ha permanecido descolgado aproximadamente unos treinta minutos hasta finalizar la acción, momento en el que ha sido detenido por la policía —sin oponer ningún tipo de resistencia— y trasladado a dependencias policiales. Actualmente, el activista se encuentra en libertad tras haber sido puesto a disposición judicial esta tarde con el cargo de alteración grave del orden público.</span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal"><span lang="EN-US">Desde Igualdad Animal queremos manifestar nuestro más profundo rechazo al hecho de que la presidenta de la Comunidad de Madrid haya decidido&nbsp;establecer que el sufrimiento y la muerte de los animales es algo a considerar digno de interés cultural. Nuestra"esperanza", como podía leerse en la pancarta que hemos desplegado, es que este espectáculo, al igual que otras formas de explotación animal, sea abolido.</span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal"><span lang="EN-US">Igualdad Animal es una organización internacional de derechos animales presente en España, Inglaterra, Venezuela y Colombia que considera que todos los animales, independientemente de su raza, sexo o especie, merecen ser respetados y protegidos. Por ese motivo, defendemos el fin de toda forma de explotación animal educando a la sociedad sobre la vida y muerte de los animales y promoviendo el <a href="http://www.igualdadanimal.org/veganismo">veganismo</a>.</span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal"><span lang="EN-US">&nbsp;<o p=""></o></span></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13657} -->
<figure class="wp-block-image"><img src="/app/uploads/2010/03/4426330303_3d22bb5587_m.jpg" alt="" class="wp-image-13657"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal"><span lang="EN-US">&nbsp;<o p=""></o></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal"><strong><span lang="EN-US">CONTACTO DE MEDIOS</span></strong><span lang="EN-US"><o p=""></o></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"MsoNormal"} -->
<p class="MsoNormal"><span lang="EN-US">&nbsp;Sharon Núñez | Portavoz<br>
	Correo electrónico: <a href="http://www.igualdadanimal.org:2095/3rdparty/squirrelmail/src/compose.php?send_to=sharonn%40igualdadanimal.org"><span>sharonn@igualdadanimal.org</span></a></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
	<br>
	<iframe align="center" frameborder="0" height="500" scrolling="no" src="https://www.flickr.com/slideShow/index.gne?set_id=72157623604919928" width="500"></iframe></p>
<!-- /wp:paragraph -->