<!-- wp:paragraph -->
<p>Más de 500 perros y gatos podrían haber muerto asfixiados durante 2007 en la perrera de Puerto Real (Cádiz).

Entre el 1 de enero y el 31 de agosto de este año se han asesinado supuestamente con el paralizante muscular mioflex un total de 566 animales, y sólo en el mes de agosto 136 perros y gatos. Estos datos son solo los oficiales, porque los reales podrían ser mucho mayores.

Cuatro de los responsables del centro han sido imputados por un Juzgado por un presunto delito tras la investigación del Servicio de Protección de la Naturaleza de la Guardia Civil (Seprona).

Fuente: Terra Actualidad - EFE</p>
<!-- /wp:paragraph -->