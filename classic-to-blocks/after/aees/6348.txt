<!-- wp:image {"id":9364} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/02/granja_el_escobar_murcia.jpg" alt="" class="wp-image-9364"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Desde Igualdad Animal hacemos público hoy un estremecedor vídeo con imágenes grabadas en la <strong>granja de cerdos de El Escobar</strong>, situada en Fuente Álamo de Murcia. En él mostramos algunas de las <strong>escenas de violencia</strong> hacia los animales criados para alimentación más brutales que nuestro Equipo de Investigaciones ha tenido la oportunidad de contemplar, y debido a las cuales ya hemos interpuesto la correspondiente denuncia por vía penal por un delito de maltrato hacia los animales.

El vídeo en cuestión muestra al personal de la granja —incluyendo responsables y operarios—:

• Golpeando fuertemente en la cabeza a las cerdas con <strong>barras de hierro</strong>.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14035,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157629363051319/" target="_blank" rel="noopener"><img src="/app/uploads/2012/02/granja_el_escobar_murcia_cerdos.jpg" alt="Granja de cerdos El Escobar de Murcia" class="wp-image-14035"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

• Atacando a los animales con <strong>grandes espadas</strong> que clavan repetidas veces en sus costados hasta causarles la muerte, entre estertores, después de varios minutos de agonía. También se puede apreciar cómo los operarios mueven las espadas clavadas en el cuerpo de las cerdas con el fin de agravar sus heridas internas, burlándose de su sufrimiento ante la cámara, así como dándoles patadas y saltando encima de ellas para golpearlas con el codo.

• <strong>Abriendo el abdomen y útero de las cerdas con una cuchilla</strong> para extraerles, en vivo, los lechones que iban a dar a luz. También les son arrancados los intestinos, el hígado y otros órganos, tras varios minutos de hurgar en el interior de sus cuerpos con las manos y mientras las víctimas se retuercen y gritan de dolor. Esta práctica se realiza sobre cerdas embarazadas que no pueden ser enviadas al matadero por estar cojas o encontrarse incapaces de caminar. Las cerdas víctimas de esta brutalidad son después abandonadas agonizando, con sus órganos extendidos sobre el suelo, hasta que finalmente mueren.

Resulta terriblemente irónico el hecho de que esta granja fuera premiada en 2008 por la industria cárnica con el premio Porc d'Or por lechones nacidos vivos.

<strong><em>Advertencia: este vídeo contiene imágenes que pueden herir su sensibilidad</em></strong></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://player.vimeo.com/video/37123360?title=0&amp;%3Bbyline=0&amp;%3Bportrait=0" width="500" height="400" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p><strong>Miguel Rodríguez Castaño, anestesista</strong> del Hospital Veterinario de la Universidad Complutense de Madrid, declara respecto a estos hechos:

<em>«Es lo más horrible que he visto en mi carrera respecto al trato hacia los animales. El sufrimiento al que se ven sometidos es comparable al que sufriríamos nosotros en esa situación. Los animales han padecido un sufrimiento extremo y una brutalidad incomparable.»</em></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Métodos habituales de matanza en la granja El Escobar</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Según hemos podido averiguar a través del testimonio de Marcos Vinicio Verduga Moreira, uno de los operarios de la granja, todos <strong>estos hechos tienen lugar de forma constante y rutinaria</strong> y <strong>con el conocimiento de los responsables</strong> de la explotación ganadera. Son precisamente los propietarios de la granja quienes ordenan en ocasiones estas brutales agresiones, participando en ellas, y quienes las han establecido como métodos habituales de matanza de estos animales criados, según nos cuenta, para la marca alimenticia El Pozo, aunque la empresa niega esto.

En un estremecedor y único testimonio ofrecido a Igualdad Animal en una entrevista de vídeo, este operario asegura haber presenciado en más de cincuenta ocasiones la <strong>matanza con espadas de cerdas adultas</strong>, algo que según él sucede de forma habitual cada semana desde que en 2009 entrase a trabajar en la granja.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14036,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/sets/72157629363051319/" target="_blank" rel="noopener"><img src="/app/uploads/2012/02/granja_el_escobar_murcia_cerdos_espada.jpg" alt="Brutalidad en Granja El Escobar de Murcia" class="wp-image-14036"/></a></figure>
<!-- /wp:image -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>

El reconocido <strong>veterinario </strong>y <strong>experto en bioética</strong> del Centro de Oxford para la Ética Animal del Reino Unido <strong>Andrew Knight</strong> ha declarado a propósito de las imágenes publicadas por Igualdad Animal:

<em>«El trato hacia estos cerdos es el abuso más brutal de animales de granja que he visto en mi vida. Los trabajadores aparecen disfrutando claramente de algunos de sus comportamientos. El trato hacia estos cerdos es impactantemente cruel y es, de hecho, sádico.»</em></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>

La empresa ganadera registrada en 1997 bajo la denominación Agropecuaria El Escobar S.A. —y cuyo administrador según el Registro Mercantil es Francisco Vera Sánchez— se halla situada en el término municipal de Fuente Álamo de Murcia.

</p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p><strong>¡Ponte en acción!</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br><h4>¡Hazte vegano!</h4><br></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><a href="https://es.loveveg.com/veganizando-tradiciones/">¡Elige un estilo de vida vegano hoy mismo!</a>

La mayoría de la gente cree que los animales no deberían ser dañados innecesariamente, pero el consumo de productos animales impone a los animales una vida de miseria y una muerte prematura. Negándonos a participar en esta explotación estamos evitando que miles de animales sean dañados y matados por nuestro consumo.

Elige una vida basada en el respeto y la justicia.
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br><h4>Haz una donación</h4><br></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>
Por favor, <a href="https://igualdadanimal.org/ayuda/hazte-socio/" target="_blank" rel="noopener">considera hacer una donación</a> para ayudar a Igualdad Animal a continuar con este trabajo vital para desvelar y poner fin a la explotación animal.
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br><h4>¡Únete a nosotros!</h4><br></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><a href="https://igualdadanimal.org/" target="_blank" rel="noopener">Igualdad Animal</a> realiza una gran variedad de actividades en defensa de los animales y tu ayuda es siempre bienvenida. Puedes contribuir como voluntario de muchas formas: en protestas, mesas informativas, repartiendo folletos, trabajando en nuestra oficina, organizando o ayudando en eventos benéficos, escribiendo, sacando fotografías…

<a href="https://igualdadanimal.org/defensores-animales/" target="_blank" rel="noopener">Rellena nuestro formulario </a>y haznos saber cómo te gustaría participar. Sé la voz de los sin voz.</p>
<!-- /wp:paragraph -->