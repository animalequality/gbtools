<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10789" src="/app/uploads/2016/06/china.jpg" style="float:left; margin:10px; width:450px">El Gobierno chino ha diseñado un plan para <strong>reducir el consumo de carne a la mitad</strong>. La tendencia al alza de su consumo en el país ha llevado al Gobierno a tomar esta medida siguiendo las recomendaciones de la Sociedad China para la Nutrición, el organismo oficial de <a href="http://dg.en.cnsoc.org/">recomendaciones dietéticas</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La medida del Gobierno chino es la última de una oleada de iniciativas semejantes alrededor del mundo. Impulsados por motivaciones de salud pública y para <a href="https://igualdadanimal.org/noticia/2016/05/28/naciones-unidas-nuestro-actual-sistema-alimentario-es-insostenible/">atenuar el calentamiento global</a>, del que el consumo de carne es factor principal, <strong>los gobiernos de otros países también han recomendado reducir el consumo de carne</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Así, las nuevas directrices dietéticas de los gobiernos de <strong>Estados Unidos, Suecia, Reino Unido y Holanda</strong> proponen una significativa reducción en el excesivo consumo nacional de carne. A esto se suma la medida del Gobierno de Dinamarca de establecer <a href="https://igualdadanimal.org/noticia/2016/04/28/dinamarca-proyecta-un-impuesto-las-carnes-rojas-para-combatir-el-calentamiento-global/">un impuesto a la producción de carne</a> en base a su devastadora huella de carbono.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A nivel mundial, un <strong>14,5% de las emisiones de gases de efecto invernadero</strong> provienen de la cría y el consumo de vacas, cerdos, pollos y otros animales de granja. <strong>Este porcentaje es mayor que el de toda la industria de transporte mundial junta, coches, aviones y barcos incluidos</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Li Junfeng, director general del Centro de cooperación internacional chino para la estrategia ante el cambio climático señalaba que, «a través de un cambio en nuestro estilo de vida es de esperar que haya una transformación en la ganadería industrial y las emisiones de gases de efecto invernadero se reduzcan».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>China consume el 28% de la carne a nivel global, incluyendo la mitad de la carne de cerdo. Sin embargo una docena de países se sitúan por delante del país asiático en cuanto a su consumo. <strong>Una persona promedio en Estados Unidos o Australia consume el doble de carne que una persona en China</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según expertos científicos, la reducción a nivel internacional del consumo de carne <a href="https://igualdadanimal.org/noticia/2015/11/26/un-comite-de-expertos-avisa-los-gobiernos-comer-menos-carne-es-la-unica-manera-de/">es un factor necesario y decisivo</a> para alcanzar los objetivos alcanzados en la cumbre de París para detener el calentamiento global.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
&nbsp;</p>
<!-- /wp:paragraph -->