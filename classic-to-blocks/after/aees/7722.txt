<!-- wp:image {"id":11783} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/shutterstock_18847306.jpg" alt="" class="wp-image-11783"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Escuelas, universidades, hospitales y cualquier otra institución pública portuguesa tendrá que servir opciones 100% vegetarianas en sus menús.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La medida afecta a todo edificio público en el que se sirvan menús. La ley fue discutida en el Parlamento portugués a principios de 2016 y <strong>aprobada por una amplia mayoría</strong> el pasado 3 de marzo. Se hará efectiva dentro de los próximos seis meses.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La iniciativa partió de la <a href="https://www.avp.org.pt/" target="_blank">Asociación vegetariana de Portugal</a>, que hizo circular una petición pública que acabó recogiendo 15.000 firmas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<font size="5"><font color="#808080"><p><a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank">Suscríbete ahora a nuestro e-boletín</a> y recibirás de forma gratuita las mejores noticias de actualidad sobre los animales y las mejores opciones de alimentación.</p></font>

<p>&nbsp;</p>

<p>Nuno Alvim, portavoz de la Asociación, ha declarado al respecto que «es un gran avance en Portugal porque es la primera vez que tendremos una ley que mencione el vegetarianismo específicamente.»</p>

<p>«La medida promocionará la diversidad de hábitos alimenticios y animará a más personas a probar opciones veggie al estar disponible en más lugares.»</p>

<figure><img alt="" class="wp-image-11784" src="/app/uploads/2017/03/shutterstock_177713687_0.jpg" style="margin: 10px; float: left; width: 450px; "></figure><p></p>

<p>&nbsp;</p>

<p>«La ley, ante todo, tendrá un impacto significativo en la salud de la población, pero, a largo plazo, también en los animales y el medioambiente.»</p>

<p>La aprobación de esta ley histórica en Portugal ha sentado <strong>un importante precedente en Europa</strong>. Existe otra campaña similar en Reino Unido que está recogiendo firmas en la actualidad.</p>

<p>La Vegan Society de Reino Unido ha declarado al respecto, «hospitales, prisiones y centros educativos deberían ofrecer opciones que respeten la igualdad y diversidad.»</p></font>
<!-- /wp:html -->