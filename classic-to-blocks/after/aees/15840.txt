<!-- wp:paragraph -->
<p>Todos sabemos bien que los perros, gatos y otros animales que viven con nosotros <strong>poseen personalidades únicas, saben cómo comunicarnos sus deseos y también su felicidad o tristeza. </strong>También reconocemos que están dotados de inteligencia y que comprenden y adivinan nuestras emociones incluso más de lo que nosotros las de ellos.

Pero debido a que sabemos poco sobre cómo son los animales maltratados en las granjas, desconocemos que los cerdos poseen personalidades complejas, un mundo emocional profundísimo y también gran inteligencia. Si nos diéramos la oportunidad de conocerlos mejor, nos sorprendería comprobar que<strong> son tan o más adorables e inteligentes que nuestros queridos perros y gatos.</strong>

Hoy te mostramos 5 motivos por los cuales los cerdos son tan maravillosos:

<span style="color: #000000;"><strong>1. Todo lo aprenden muy rápido</strong></span>

Son <strong>unos de los animales más rápidos de la naturaleza en aprender.</strong> La inteligencia de los cerdos supera a la de un niño de tres años, está a la par de delfines y elefantes, y aprenden más rápido que los perros y los primates. Pueden abrir y cerrar puertas, guiar a rebaños de ovejas, armar rompecabezas y resolver laberintos ¡e incluso jugar a videojuegos con un joystick!

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/twS_COailzk" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe><span style="color: #000000;">2. Son muy limpios</span>

Olvídate de todo lo que siempre has escuchado sobre que los cerdos son animales sucios y flojos porque la realidad es muy distinta: los cerdos son animales muy limpios,<strong> tanto que pueden convivir con nosotros de la misma forma que lo hacen los perros y los gatos.</strong> Además, a los cerdos les encanta bañarse con agua y si se dan baños de barro es para mantenerse frescos y protegerse del sol ya que no tienen glándulas sudoríparas.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">&nbsp;</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales de granja?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener noreferrer"> ¡Suscríbete gratuitamente a nuestro e-boletín!</a></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong><span style="color: #000000;">3. Las cerdas son unas verdaderas «super mamás»</span></strong>

Al vivir en condiciones naturales muy diferentes al confinamiento, privaciones y maltratos de las granjas, los cerdos pueden conformar una unidad familiar matriarcal y los lechones son cuidados por parientes femeninas. Las cerdas son excelentes madres que se preocupan y <strong>darían todo por defender a sus hijos y sufren mucho cuando son separadas de ellos.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13502} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/12/5-motivos-por-los-cuales-los-cerdos-son-tan-geniales-igualdadanimal.org-2.jpg" alt="" class="wp-image-13502"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong><span style="color: #000000;">4. Son animales plenamente conscientes de su propia existencia</span></strong>

Un estudio realizado en la Universidad de Cambridge reveló que si a los cerdos los colocan delante de un espejo <strong>son capaces de reconocerse a sí mismos</strong> y, también, aprenden que los objetos que muestra el espejo son solo reflejos de los reales. Esta es una habilidad que comparten con muy pocos animales como los delfines, chimpancés y elefantes.

<span style="color: #000000;"><strong>5. Poseen un lenguaje complejo</strong></span>

Los cerdos<strong> pueden emitir más de 30 sonidos que tienen significados específicos </strong>para expresar sus intenciones, estados de ánimo, advertencias, saludos y cuándo es la hora de comer.

Las cerdas emiten un llamado especial para avisarles a los cerditos que ya es hora de amamantar y estos ya desde recién nacidos reconocen las voces de sus madres.</p>
<!-- /wp:paragraph -->