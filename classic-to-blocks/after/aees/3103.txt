<!-- wp:paragraph -->
<p>El caballo Gradino ha sufrido un accidente mientras era obligado a participar en pruebas de preparación para el campeonato de Europa de "salto de obstáculos". 

Grandino ha sufrido una torcedura al realizar una caída. A consecuencia de dicho accidente no será obligado a viajar y a participar en torneos durante mes y medio.

Al margen del empleo de productos de origen animal en varios deportes, miles de animales son obligados actualmente a participar en determinadas modalidades "deportivas": "equitación", polo, carreras de caballos y perros, etc.</p>
<!-- /wp:paragraph -->