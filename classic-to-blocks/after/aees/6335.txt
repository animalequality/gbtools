<!-- wp:image {"id":9353} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/02/fb120000_mini.jpg" alt="" class="wp-image-9353"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:html -->
<div id="fb-root">
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=400961260542";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /wp:html -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Gracias a tu apoyo, en <strong><a href="https://www.facebook.com/igualdadanimal" target="_blank">Facebook </a></strong>ya somos:</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":14053,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="http://www.igualdadanimal.org/files/multimedia/120000fb/120000fb.html" target="_blank"><img src="/app/uploads/2012/02/igualdad_animal_fb120000.jpg" alt="Click en la foto para ampliar y navegar por nuestra difusión en Facebook" class="wp-image-14053" title="Haz click en la imagen y navega por nuestro trabajo de difusión en Facebook"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>(<strong>Haz click en la imagen</strong> y navega por nuestro trabajo de difusión en Facebook)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta semana celebramos, además, los 3 años de Igualdad Animal en <strong><a href="https://twitter.com/igualdadanimal" target="_blank">Twitter</a></strong>, que también ve crecer su número de seguidores de forma exponencial.<br>
	<br>
	Y es que las redes sociales se han convertido en una herramienta imprescindible para promover el respeto hacia los animales, difundir noticias, generar debate y hacer que las convocatorias en su defensa llegue a un público mucho mayor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para que nuestro trabajo siga llegando a más y más gente cada día,<strong> te invitamos a conocernos a través de estos medios</strong> y a que invites a tus amigos a formar parte de este proyecto de difusión de los derechos animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>► Visita nuestra página:&nbsp; <a href="https://www.facebook.com/IgualdadAnimalIgualdad Animal en Twitter" target="_blank">www.facebook.com/IgualdadAnimal</a><a href="https://twitter.com/IgualdadAnimal" target="_blank"><img alt="Igualdad Animal en Twitter" src="http://a2.twimg.com/profile_images/1818336825/ia-twitterbird.jpg" style="float: right; width: 100px; "></a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>► Para seguir nuestro trabajo desde <strong><a href="https://twitter.com/igualdadanimal" target="_blank">Twitter</a></strong>, visita: &nbsp;&nbsp;<a href="https://twitter.com/igualdadanimal" target="_blank">www.twitter.com/igualdadanimal</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div id="banner_facebook">
	<div class="fb-like" data-font="arial" data-href="https://www.facebook.com/igualdadanimal" data-send="false" data-show-faces="false" data-width="300" style="top:88px; left:27px; height:30px; overflow:hidden;">
		&nbsp;</div>
</div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Y si quieres estar al corriente de nuestras actividades en otros países, pásate por las páginas de:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br>		<a href="https://www.facebook.com/AnimalEquality" target="_blank">Animal Equality</a> (Reino Unido)</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		<a href="https://www.facebook.com/AnimalEqualityItalia" target="_blank">Animal Equality Italia </a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		<a href="https://www.facebook.com/pages/Animal-Equality-Polska/122612284433590" target="_blank">Animal Equality Polska</a></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->