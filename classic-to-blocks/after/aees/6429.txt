<!-- wp:image {"id":9443} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/03/plumas_carton_compost.jpg" alt="" class="wp-image-9443"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Un ingeniero de Terrassa ha conseguido obtener un nuevo material mezclando plumas de pollo con un plástico biodegradable. El director del proyecto considera que gracias a este nuevo material se pueden obtener beneficios de los restos de los pollos enviados al <a href="http://www.mataderos.info" target="_blank">matadero</a>, ya que en España estos lugares desechan unas 100.000 toneladas de plumas anualmente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Los investigadores apuestan por el futuro de este material de compostaje, que podría tener aplicaciones industriales. Debido a la ligereza de las plumas, “el material se podría utilizar en la obtención de paneles térmicos para la construcción y para el packaging, como substituto del cartón”, señalan.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Este proyecto está siendo financiado por el Ministerio de Ciencia e Innovación y ya se está tramitando una propuesta a nivel europeo, donde al parecer hay un consorcio de empresas interesadas en dicho material.</p>
<!-- /wp:paragraph -->