<!-- wp:image {"id":10154} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/06/inglaterra.jpg" alt="" class="wp-image-10154"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":13709} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/06/Inglaterra.jpg" alt="" class="wp-image-13709"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Responsables del Teatro Nacional de Londres han confirmado a Igualdad Animal que han dejado de servir carne de conejo en su restaurante tras conocer la investigación realizada por la organización en granjas de conejos en España y que han suspendido su relación con el distribuidor C&amp;D Wines.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dos de las granjas investigadas por Igualdad Animal tenían relación con distribuidores ingleses y restaurantes y uno de los platos servidos en la terraza del teatro incluía carne proveniente de estas granjas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/UN_gr4vPy7c" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La responsable de prensa del Teatro Nacional Lucinda Morrison manifestó a Igualdad Animal que “<em>estamos completamente impactados por las prácticas en ciertas granjas de conejos en España desveladas por la investigación de Igualdad Animal. Uno de nuestros distribuidores en Inglaterra ha sido relacionado con algunas de estas granjas, por lo que hemos eliminado inmediatamente la carne de conejo de nuestro menú y no la volveremos a incorporar. También hemos suspendido nuestra relación con el distribuidor en cuestión mientras investigamos lo sucedido</em>”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Investigadores de Igualdad Animal se infiltraron durante 2 años en la industria de la cría de conejos española, en un recorrido por 14 comunidades en que documentaron 70 granjas, algunas de ellas premiadas por la industria, y 4 mataderos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El gabinete jurídico de Igualdad Animal ha interpuesto un total de 74 denuncias por maltrato animal, irregularidades en las condiciones de bioseguridad e incumplimiento de la normativa higiénico sanitaria contra 72 granjas de conejos y 2 mataderos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las sanciones a las que pueden enfrentarse los responsables de las explotaciones oscilan entre 60.000 y 1.200.000 euros de multa, de 3 meses a 1 año de prisión e inhabilitación especial para trabajar con animales durante un período de 1 a 3 años para las infracciones muy graves.También podría proceder como medida accesoria, el cierre o clausura de las instalaciones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>“<em>Nos alegra oir que una respetada institución como el Teatro Nacional de Londres toma medidas contra el maltrato animal decidiendo dejar de vender carne de conejo y que haya acabado sus relaciones con los distribuidores españoles</em>” indicó Laura Gough, portavoz de Igualdad Animal en Inglaterra.</p>
<!-- /wp:paragraph -->