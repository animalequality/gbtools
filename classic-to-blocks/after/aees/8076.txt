<!-- wp:paragraph -->
<p>Grupo Eroski ha <span style="color: #0000ff;"><a style="color: #0000ff;" href="https://corporativo.eroski.es/sala-prensa/" target="_blank" rel="noopener">anunciado</a></span> que dejará de vender huevos de gallinas enjauladas en 2024.

La medida se ha enmarcado en uno de los 10 puntos del ambicioso plan de RSC&nbsp; (responsabilidad social corporativa) que Eroski acaba de presentar. Entre una de las políticas estrella, el Grupo Eroski, que ya vende un 30% libre de jaula, se ha comprometido a que en 2024 el 100% de los huevos que comercialice en sus más de 2000 establecimientos sean libres de jaula.

Este compromiso disminuirá el sufrimiento de más de un millón de gallinas.

Estamos ante una tendencia imparable y como siempre, debemos subrayar que aunque libre de jaula no implica libre de maltrato, es indispensable que las empresas adopten un papel relevante en reducir el sufrimiento extremo que padecen millones de animales y que tanto solicitan los consumidores.

Desde Igualdad Animal se valora muy positivamente que Eroski se sume así a las más de 400 empresas en todo el mundo que ya han asumido el compromiso de dejar de usar huevos de jaulas antes de 2025. Entre ellas se encuentran Kraft Heinz, Sodexo, Aldi, Mondelez Internacional o las españolas Mercadona, Carrefour, El Corte Inglés o Dulcesol.

&nbsp;</p>
<!-- /wp:paragraph -->