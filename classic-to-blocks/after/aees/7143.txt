<!-- wp:image {"id":10150} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/05/maltratoengranjas.jpg" alt="" class="wp-image-10150"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:image {"id":13734} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/05/SPA-Cover-photo-rabbit-inv.jpg" alt="" class="wp-image-13734"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La organización internacional Igualdad Animal ha presentado hoy en rueda de prensa <a href="https://www.youtube.com/watch?v=UN_gr4vPy7c"><strong>los hallazgos de su nueva investigación</strong></a>.<br>
<br>
Investigadores de Igualdad Animal se han infiltrado durante 2 años en la industria de la cría de conejos española. En un recorrido por 14 comunidades, han documentado 70 granjas, algunas de ellas premiadas por la industria, y 4 mataderos, y han sido testigos de graves irregularidades en materias de bienestar animal e higiénico-sanitarias:<br>
<br>
<strong>- Granjeros que matan a los conejos estampándoles contra el suelo.<br>
<br>
- Conejos arrojados vivos a los contenedores.<br>
<br>
- Animales con heridas abiertas e infecciones sin recibir ningún tipo de atención veterinaria.<br>
<br>
- Animales muertos en estado de putrefacción dentro de las jaulas.<br>
<br>
- Agresiones entre los conejos debido a las condiciones de hacinamiento.<br>
<br>
- Una Veterinaria estampando a un conejo contra el suelo reconociendo que es maltrato animal.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>El gabinete jurídico de Igualdad&nbsp;Animal ha interpuesto un total de 72 denuncias por maltrato animal, irregularidades en las condiciones de bioseguridad e incumplimiento de la normativa higiénico sanitaria contra 70 granjas de conejos y 2 mataderos.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Las sanciones a las que pueden enfrentarse los responsables de las explotaciones oscilan entre 60.000 y 1.200.000 euros de multa, de 3 meses a 1 año de prisión e inhabilitación especial para trabajar con animales durante un período de 1 a 3 años para las infracciones muy graves.También podría proceder como medida accesoria, el cierre o clausura de las instalaciones.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>“Exigimos a los responsables políticos en cada Comunidad Autónoma una respuesta inmediata ante los hechos denunciados dada la gravedad de los mismos y la falta de control sobre esta industria que hemos podido constatar”</em> indicó Amanda Romero, coordinadora de Igualdad Animal en España.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Igualdad Animal informó que daba comienzo su campaña de sensibilización social, considerando que la ciudadanía tiene derecho a conocer las condiciones en las que se encuentran los animales, que esta información es de interés público y que por ello hoy a través de sus plataformas, presentarán un vídeo reportaje en el que cuentan con la colaboración del actor Pablo Puyol.<br>
<br>
“<em>Los ciudadanos y ciudadanas tienen derecho a conocer lo que está ocurriendo en estos lugares, algo que estas industrias ocultan. Agradecemos enormemente al actor Pablo Puyol, conocido defensor de los animales, su apoyo y colaboración con Igualdad Animal, que sin duda hará que esta investigación llegue al máximo número de personas</em>”, indicó Javier Moreno, cofundador de Igualdad Animal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->