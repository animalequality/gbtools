<!-- wp:paragraph -->
<p>Alcampo ha confirmado a Igualdad Animal su compromiso de eliminar de sus establecimientos los huevos de gallinas criadas en jaulas. A partir de 2021 la cadena de supermercados sólo comercializará huevos de marca propia ‘Auchan’ que no procedan de gallinas enjauladas. Será en 2025 cuando este compromiso se extienda al resto de las marcas que ofertan.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esta decisión mejorará la calidad de vida de cientos de miles de gallinas. Para Javier Moreno, Director de Igualdad Animal, «El compromiso de Alcampo es un avance muy importante que servirá para reducir el sufrimiento que padecen millones de estas aves en España. Mantener a las gallinas en jaulas es una de las prácticas más crueles que lleva a cabo la industria ya que les impide desarrollar sus comportamientos naturales más básicos. Aunque libre de jaula no implica libre de maltrato, es indispensable que las empresas adopten un papel relevante para reducir el sufrimiento extremo que padecen millones de animales».</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Una tendencia liderada por los consumidores</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Las principales cadenas de supermercados en España; Mercadona, Carrefour, Eroski, Aldi, Lidl, AhorraMas, Condis o El Corte Inglés ya se han sumado también a este compromiso. Actualmente son más de 500 empresas a nivel mundial las que rechazan la utilización o distribución de huevos procedentes de gallinas enjauladas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El 88% de las gallinas en España, 41 millones, viven enjauladas. Javier Moreno destaca que cada vez más consumidores valoran el bienestar animal a la hora de elegir los productos que compran; «El 94% de las personas en Europa piensan que proteger el bienestar de los animales en las granjas es importante, y el 82% cree que los animales en las granjas deberían estar mejor protegidos».</p>
<!-- /wp:paragraph -->