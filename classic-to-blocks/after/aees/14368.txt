<!-- wp:paragraph -->
<p>Igualdad Animal organiza un curso de cocina vegetariana en Madrid de una duración de 12 horas. El próximo <strong>Sábado 14 y Domingo 15 </strong>de Julio se realizará la primera sesión y dos fines de semana después, es decir el <strong>Sábado 28 y el Domingo 29 </strong>de Julio se realizará la segunda. El curso se realizará en pleno centro de Madrid importido por el excelente chef de cocina vegana Javier Guarascio, aprenderás de manera facil y sencilla a preparar los platos más deliciosos, además de las propiedades, nutrientes y muchas cosas más de la dieta vegana. El horario del curso será de <strong>18 a 21 horas</strong> y se realizarán en la <strong>Calle San Bernardo, 51 - 2c Madrid (Metro - Noviciado / Plaza de España)</strong>. Durante el mismo se regalarán recetarios veganos, chapas, piramides de nutrición etc. a cada uno de los asistentes.

El precio será de 5 euros por día (20 euros por el curso completo), excepto para quienes se apuntaron al curso en Marzo y no pudieron realizarlo debido a que la organización se vió obligada a cancelarlo que recibirán el curso gratis. Todos los beneficio irán destinados al trabajo que Igualdad Animal realiza en defensa de los animales.

Si quieres participar apuntate llamando al:
Tel - 91 522 22 18 / 675 737 459
o escribiendo un mail a <a href="mailto:cocina@igualdadanimal.org">cocina@igualdadanimal.org</a>

Ayúdanos a difundir esta convocatoria.

<strong>Seminario de cocina básica vegana</strong><strong>Clase 1</strong>

Teórico
Cereales y legumbres, características, diferentes tipos, métodos de preparación.
Utensilios de cocina. La mejor manera de mantenerlos desde el punto de vista de la cocina vegana.
Métodos acido/alcalino
Práctica
Tabule de quínoa
Milanesas de soja
Tortilla de patatas
Budín de frutas y mijo

<strong>Clase 2</strong>

Teórico
El gluten: ventajas y desventajas. Composición nutricional, formas de utilización
Algas Marinas: Importancia en la cocina, métodos de cocción y utilización.
Práctica:
Sopa de wakame y miso
Método clásico en la elaboración de Seitán y método rápido
Bifes de seitan a la criolla
Peras glaseadas al vino tinto

<strong>Clase 3</strong>

Teórico:
Soja y sus derivados: El tofu, la importancia de consumir tofu, propiedades, composición.
Práctico:
Pastel de patatas y carne de soja texturizada
Tofu a la plancha
Natillas de chocolate, de fresa y de vainilla

<strong>Clase 4</strong>

Teórico:
Masas integrales, la importancia de utilizar harina integral en la cocina vegana, diferencias entre las harinas blancas refinadas y las harinas integrales.

Práctico:
Pan integral básico
Focaccia integral de olivas, romero y ajo
Pizzas veganas:
1- Rellena de crema de berenjenas
2-Margarita (con albahaca, muzzarella de tofu y tomate natural).
Tarta de chocolate y nueces.</p>
<!-- /wp:paragraph -->