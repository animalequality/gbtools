<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Seguir un alimentación basada en proteínas vegetales es cada vez más común también entre atletas y celebridades. Ya sea por motivos de salud, por consideración al sufrimiento al que la industria somete a miles de millones de animales o por el riesgo en que esta misma tiene al planeta, lo cierto es que la tendencia es creciente e imparable.

El centrocampista colombiano del equipo Boca Juniors de la primera división argentina, Sebastián Pérez, decidió cambiar por completo sus hábitos alimenticios, sustituyendo la carne y otros productos animales por alternativas veggie. Y lo hizo por la compasión y el respeto que siente por los animales.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading"><span style="font-size: x-large;"><span style="color: #808080;">«Están los que dicen que los animales están para comerlos, para mí el mundo es de todos los seres vivos. A eso se debe mi decisión y el respeto que tengo hacia los animales».</span></span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><span style="font-size: x-large;">&nbsp;</span>Mientras se recuperaba de una rotura de ligamentos cruzados en abril de 2017, Pérez decidió alimentarse de manera vegana. Cuando comentó su decisión al cuerpo técnico y también al departamento médico del club nadie objetó y solo se preocuparon porque pudieran mantenerlo con las misma energía en alto rendimiento. «Solo me dijeron que en eso no se metían. Que si yo me sentía bien, que lo hiciera, que lo más importante era eso», contó.

«En el club me han apoyado; me prepararon un menú muy bueno en la parte profesional con una nutricionista (Karina Gavini) para cumplir con lo que necesito», manifestó Pérez. Las comidas que debe llevar adelante incluyen arroz, papa, setas, ensaladas, guiso de lentejas y pastas. Los hidratos de carbono son sumamente importantes ya que son «el combustible principal para el ejercicio», según el mismo club. En cuanto a los controles y mediciones que se hacen en el club, explicó que no modificó su rendimiento: «Dan excelente. Lo único que bajé es grasa, todas las mediciones me dan perfecto».</p>
<!-- /wp:paragraph -->