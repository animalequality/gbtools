<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Vaya año 2015. Hemos trabajado tanto que si tuviésemos que usar palabras para resumirlo este artículo sería tan largo que tendrías que pasarte el 2016 leyendo. Y no vamos a hacerte eso, claro.

Así que hemos pensado que mejor resumirlo con 12 imágenes que hablen por sí solas. Ya se sabe que una imagen vale más que mil palabras. Pues imagina 12.

<strong>1. Tecnología virtual al servicio de los animales: una experiencia que cambia vidas</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review1.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

2015 ha sido el año en el que hemos introducido la tecnología virtual en la defensa de los animales.

Este proyecto pionero permite tener una experiencia como jamás antes se había podido tener: convierte virtualmente a las personas en los animales de granja. Muy pronto podrás experimentar lo que se siente.

<strong>2. Conferencia internacional de protección animal en Washington DC</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review2.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Los Directores Internacionales y cofundadores de Igualdad Animal, Sharon Nuñez y Jose Valle impartieron sendas conferencias en uno de los encuentros de protección animal más importantes del mundo, la Conferencia Nacional de Derechos Animales en Washington DC.

<strong>3. ¡Alcanzamos 2 millones de seguidores en Facebook!</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review3.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

2 millones de seguidores en la red social más famosa del mundo, ¡no veas! <strong>Somos la ONG con más simpatizantes en España</strong>. Hemos convertido la protección animal en un asunto de primer orden en España y en Latinoamérica. ¡<a href="https://www.facebook.com/IgualdadAnimal">Gracias por seguirnos</a>!

<strong>4. 580.000 firmas contra el cruel comercio de carne y pieles de perros y gatos</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review4.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

En una de nuestras mayores recogidas de firmas hasta la fecha, conseguimos un apoyo masivo de los ciudadanos para pedir al Gobierno chino que cree una ley de protección animal que evite este brutal comercio.

En <a href="https://www.sinvoz.org/">esta campaña</a> Igualdad Animal está teniendo la colaboración de activistas chinos que también están alzando la voz por los perros y gatos del país.

<strong>5. Igualdad&nbsp;Animal y Veganuary unen fuerzas y trabajarán juntos</strong><strong><img class="attachment-temp-unmanaged-f970093c2b872658f34cadd58bfa3858 wp-image-0000000" style="float: left; margin-left: 10px; margin-right: 10px; width: 450px;" src="/boletin_files/images/review5.jpg" alt=""></strong><a href="http://www.veganuary.com/es/about/">Veganuary</a> es una campaña global animando a la gente a probar una alimentación vegana durante el mes de enero. Con participantes procedentes de 115 países diferentes, que ofrecen información y apoyo para hacer que el veganismo sea accesible, fácil y agradable.

Imagina lo que podemos hacer por los animales trabajando juntos. De momento más de 10.000 personas ya se han comprometido a probar a alimentarse de forma vegana en enero de 2016.<a href="https://es.loveveg.com/"> ¿Te animas a probar?</a><strong>6. Somos elegidas como una de las organizaciones de protección animal más eficientes del mundo</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review6.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>La organización estadounidense Animal Charity Evaluators (ACE) realiza evaluaciones anuales de organizaciones de protección animal de todo el mundo.

En 2015 fuimos ya elegidos y en 2016 repetimos. Queremos salvar tantas vidas de animales como nos sea posible. Tras su evaluación ACE concluye que por cada euro donado a Igualdad Animal se salva a 14 animales de una vida de miseria y sufrimiento.

<strong>7. Mostramos el maltrato animal en incubadoras de pollos españolas</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review7.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Nuestros investigadores trabajaron muy duro para sacar a la luz una información que la industria de la carne de pollo se cuida muy bien de ocultar a los consumidores.

El vídeo de esta investigación se llama <a href="https://www.youtube.com/watch?v=VGKQKGOyPtU">Su Primer Día</a> y cuenta con la participación de la actriz Beatriz Ros. Ha sido visto por más de 205.000 personas ya en Youtube y la investigación llegó a más de 15 millones de personas a través de los medios de comunicación. <strong>Era la primera vez en España que se conseguían imágenes de esta industria</strong>.

<strong>8. Cuarta investigación en China sobre el comercio de carne y pieles de perro y gato</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review8.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Los investigadores de Igualdad Animal, con ayuda de activistas locales, se infiltraron en la industria china de pieles de perro y gato en varias regiones del país y destaparon la crueldad que hay detrás de la matanza de perros y gatos para obtener sus pieles.

Es la cuarta investigación que la organización lleva a cabo en esta industria. Millones de personas de todo el mundo han podido <a href="https://www.youtube.com/watch?v=S1KYkeXuKRI&amp;list=PLwYnSvpmB9cdwLPWIGVA1TMYJCkBaG5La">ver la investigación</a> a través de los medios de comunicación, poniendo presión en el Gobierno chino para su prohibición.

<strong>9. La industria peletera ve frustrado su intento de frenar al movimiento de protección animal</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review9.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

La Audiencia Provincial sección 6 de A Coruña <a href="https://igualdadanimal.org/noticia/2015/06/22/animalistas-ganan-el-caso-la-industria-peletera/">puso fin a la causa</a> contra varios activistas del movimiento de defensa de los animales en la que el anterior juez titular del Juzgado de Instrucción Nº 2 de Santiago de Compostela, José Antonio Vázquez Taín, había llegado a imputar a 25 personas (incluyendo entre ellos a los fundadores de lgualdad Animal y de Equanimal) &nbsp;de varios delitos relacionados con las sueltas de visones producidas en Galicia entre los años 2007 y 2010.

El auto de la Audiencia Provincial con fecha del 15 de junio concluye: "<strong>No se ha acreditado la participación de ninguno de los imputados, ni de la organización a la que se refiere el recurrente, en la comisión de algún acto delictivo concreto</strong>". <a href="https://www.youtube.com/watch?v=tG41EB5LatY">Aquí puedes ver un completo reportaje explicando todo el caso de principio a fin.</a><sub><em>Fotografía: Karol Orzechowski</em></sub><strong>10. Investigación sobre los festejos taurinos populares en España</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review10.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Nos recorrimos parte de la geografía para mostrar a la sociedad la brutalidad de los festejos taurinos en España. La campaña Festejos Crueles fue ampliamente cubierta por los medios de comunicación y el vídeo ha sido visto por más de <strong>33.000 personas</strong>.

Y las buenas noticias son que el <a href="https://igualdadanimal.org/noticia/2015/10/28/europa-prohibe-subvencionar-la-tauromaquia/">Parlamento Europeo llevó a cabo una votación</a> para contemplar la posibilidad de <strong>parar de subvencionar la tauromaquia en España.</strong><strong>11. Investigación en dos granjas alemanas de producción de huevos “ecológicos”</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review11.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Nuestros investigadores visitaron dos granjas al norte de Alemania que albergaban hasta <strong>30.000 gallinas ponedoras</strong>.

Una de las granjas suministra los huevos a ‘Deutsche Frühstücksei GmbH’, el mayor productor de Alemania y uno de los mayores de Europa. Suministra a los principales supermercados y cadenas al por menor. <strong>La investigación llegó a millones de personas a través de los medios de comunicación alemanes</strong>.

<strong>12. Hemos mostrado a pie de calle de distintas ciudades del mundo la realidad que se nos está ocultando</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":0} -->
<figure class="wp-block-image"><img src="/boletin_files/images/review12.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Llegando al corazón de las personas por todo el mundo. Llevando la voz de los animales cada vez más lejos. Informando y ayudando a las personas a dar pasos <a href="_wp_link_placeholder" data-wplink-edit="true">hacia una alimentación más compasiva y sostenible.</a>

Preparándonos para un 2016 en el que la protección de los animales llegue <strong>hasta el último rincón donde un animal esté sufriendo</strong>.</p>
<!-- /wp:paragraph -->