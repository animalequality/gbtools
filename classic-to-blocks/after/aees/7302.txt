<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10302" src="/app/uploads/2015/09/cheesevegan.jpg" style="float:right; margin:10px; width:450px">“Creemos que los días de usar a las vacas como máquinas de producción de alimentos se han acabado”, dicen los científicos de <a href="https://www.realvegancheese.org/">Real Vegan Cheese</a> (Queso vegano auténtico). Desde sus dos laboratorios en San Francisco, estas dos docenas de biólogos moleculares, microbiólogos y otros entusiastas científicos pretenden que su queso llegue a comercializarse en un futuro no lejano. Todos ellos comparten su pasión por la ciencia sostenible y la protección del medioambiente. Pero no olvidan el sabor y las propiedades nutricionales, “¡creemos que la gente que no quiere queso de origen animal tiene derecho al mejor sabor!”</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El proyecto ofrece un queso auténtico para veganos, además de para <a href="https://es.wikipedia.org/wiki/Intolerancia_a_la_lactosa">intolerantes a la lactosa</a> (la leche generada carece de este azúcar) y alérgicos a alimentos con proteínas de la leche derivada de animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El proceso es relativamente sencillo. Sintetizan genes de proteína láctea y los introducen en la levadura de cerveza. La levadura convierte esos genes en proteína láctea, sin ningún ingrediente animal en el proceso. Luego se recogen esas proteínas lácteas y con ellas se hace la leche, añadiendo agua y aceites de origen vegetal. Y ya con la leche, ¡sólo queda hacer el queso de la manera tradicional! “Este proceso es más sostenible que el proceso tradicional del queso de origen animal”, afirman en Real Vegan Cheese.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En la website del proyecto informan, “Incluso la U.N. Food and Agriculture Organization (Organización de las Naciones Unidas para la Alimentación y la Agricultura) reconoce que la industria ganadera es responsable de la emisión enormes cantidades de gases de efecto invernadero a la atmósfera (según diversas fuentes incluso más que todas las industrias de transporte juntas, incluídos coches, aviones, trenes y barcos a nivel mundial).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Toda la información y licencias de la tecnología implicada en el proceso están registradas bajo licencias libres y abiertas al público. Las patentes tecnológicas serán públicas para ser usadas por particulares o empresas.</p>
<!-- /wp:paragraph -->