<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-14085" src="/app/uploads/2012/11/kristinn_hrafnsson.jpeg" style="width: 200px;">Los periodistas Kristinn Hrafnsson y Joseph Farrell de Wikileaks han confirmado su participación en el “Congreso Internacional Contra La Represión - Miradas críticas ante la criminalización del activismo” que se celebrará en el Ateneo de Madrid los próximos días 6 y 7 de Diciembre. Kristinn Hrafnsson es además portavoz de Wikileaks. Desde la organización del congreso nos parece una buena noticia poder contar con la participación de Wikileaks, ya que representan claramente la apuesta por la libertad de información y la represión a la que se pueden ver sometidos los periodistas cuando ponen en jaque los intereses de grandes poderes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Su ponencia tendrá lugar el Viernes 7 de diciembre a las 9 horas en el Salón de Actos del Ateneo de Madrid. En el siguiente enlace se puede consultar el programa completo:<br>
	<a href="http://www.congresocontralarepresion.org/programa-congreso-represion-derechos-civiles.php">http://www.congresocontralarepresion.org/programa-congreso-represion-derechos-civiles.php</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Congreso Internacional Contra la Represión surge de la urgente necesidad que tienen los colectivos afectados por la represión de compartir su experiencia y buscar vías de actuación ante ella. Durante dos días se analizará en profundidad la oleada de represión que a nivel internacional amenaza el avance de los derechos civiles.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>El Congreso Internacional Contra la Represión es una iniciativa de Igualdad Animal. Igualdad Animal es una organización internacional de defensa de los animales presente en España, Alemania, Inglaterra, Italia, India, México y Venezuela que realiza investigaciones, rescates de animales y actos de desobediencia civil con el fin de concienciar a la sociedad y ayudar a los animales.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
	SITIO WEB: http://www.CongresoContraLaRepresion.org<br>
	TWITTER: @antirepresion&nbsp;&nbsp; |&nbsp;&nbsp; HASHTAG: #congresoantirepresion</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>VÍDEO PROMOCIONAL: https://vimeo.com/53680195</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>PROGRAMA: http://www.congresocontralarepresion.org/programa-congreso-represion-derechos-civiles.php</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Acreditación para periodistas: http://www.congresocontralarepresion.org/prensa-congreso-contra-la-represion.php</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
	CONTACTO DE MEDIOS<br>
	Correo electrónico: prensa@congresocontralarepresion.org</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Javier Moreno<br>
	Tel. 691 054 172</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sharon Núñez<br>
	Tel. 674 216 012</p>
<!-- /wp:paragraph -->