<!-- wp:paragraph -->
<p>Dejaba morir de hambre y de sed a los perros que compraba por internet o que adoptaba en perreras municipales.

El caso ha llegado ya a los Mossos (policía autonómica de Cataluña) y en unos días está previsto que los animales que tiene a su cargo este vecino de Badalona pasen a manos de una protectora.

Por ahora han sido seis los animales que han muerto a manos de este presunto asesino de animales, según la clínica veterinaria que los atendía y una protectora de animales.

Todos ellos presentaban los efectos evidentes de la desnutrición y de las torturas prolongadas. Algunos incluso estaban mutilados.

La última alarma saltó cuando el presunto asesino llevó a un perro a la consulta del veterinario con los mismos síntomas y con una profunda herida en el tórax producida por el constreñimiento de una correa de un perro joven demasiado ajustada.

Una vez presentadas sendas denuncias a los Mossos, al Ayuntamiento de Badalona y al Colegio de Veterinarios, la policía inspeccionará el domicilio del presunto asesino..

El denunciado tenía, además, una iguana, un hurón y un gato en casa.


[img_assist|nid=3916|link=none|align=left|width=596|height=400]</p>
<!-- /wp:paragraph -->