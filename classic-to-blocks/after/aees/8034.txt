<!-- wp:paragraph -->
<p><span style="color: #0000ff;"><a style="color: #0000ff;" href="http://foodsfortomorrow.com/" target="_blank" rel="noopener">Foods For Tomorrow</a>,</span> empresa dedicada a las proteínas vegetales, se ha llevado el premio más prestigioso en innovación alimentaria concedido por el Basque Culinary Center.

La empresa, afincada en Barcelona, ha conseguido el premio con su línea de proteínas vegetales «Heura», tan sabrosa que incluso carniceros creyeron estar probando carne convencional al probarla.

&nbsp;

El concurso, organizado por la prestigiosa institución gastronómica del País Vasco este 5 y 6 de febrero estaba enmarcado en el V Foro Internacional Emprendedores donde se fusionan iniciativas emprendedoras y gastronomía. Más de 50 proyectos se habían presentado a concurso.

«Utilizar los animales para producir proteínas es un sistema obsoleto e insostenible».

Marc Coloma, director ejecutivo y fundador de Foods For Tomorrow, remarcaba la misión de la empresa ante público e inversores: «En Foods For Tomorrow queremos dar respuesta al reto de cómo alimentar de manera más sostenible una población mundial en aumento que demanda más proteínas»; y añadía, «Utilizar los animales para producir proteínas es un sistema obsoleto e insostenible».

La línea de proteínas vegetales Heura está ideada para superar las barreras que las personas consumidoras perciben actualmente ante las carnes vegetales. El equipo técnico de la empresa empleó dos años desarrollando una proteína vegetal que revolucionara el mercado de los productos 100% vegetales.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¿Quieres recibir las mejores noticias de actualidad sobre los animales?</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4,"align":"center"} -->
<h4 class="wp-block-heading">¡Suscríbete gratuitamente a<span style="color: #0000ff;"><a style="color: #0000ff;" href="https://igualdadanimal.us10.list-manage.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=8563b98958" target="_blank" rel="noopener"> nuestro e-boletín!</a> &nbsp;</span></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Ya disponible en más de 200 puntos de venta en España, la empresa mira más lejos. En los próximos años, Coloma y su equipo se centrarán en llegar a la gran distribución y la internacionalización de sus productos, principalmente a mercados Europeos.

La empresa anunció recientemente que está trabajando en nuevas líneas de productos que satisfagan la creciente demanda de proteínas vegetales. Para lograr acceder a los mercados internacionales, Foods For Tomorrow apuesta por la I+D, ya que, según Coloma, «los consumidores no rechazan la experiencia de comer carne sino los problemas que genera». De ahí el desarrollo de alimentos análogos en sabor y textura a la carne convencional, pero muy superiores en cuanto a su huella de carbono y ahorro de agua y energía.</p>
<!-- /wp:paragraph -->