<!-- wp:image {"id":9662} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/08/cerco_policial_gijon.jpg" alt="" class="wp-image-9662"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Unas 400 personas participaron este domingo 12 de agosto en Gijón en una manifestación antitaurina que se saldó con la detención de dos personas.</strong> La policía cargó durante la manifestación contra una pareja de activistas, uno de los cuales fue conducido hasta un furgón policial.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según testigos, dentro del vehículo uno de los agentes propinó un golpe al detenido. Fue entonces cuando varios manifestantes solicitaron que se abriese el furgón policial para asegurarse de que el joven se encontraba en buenas condiciones, ante lo cual la policía hizo caso omiso. Los manifestantes siguieron reclamando ver al joven detenido, ante lo cual los agentes decidieron volver a cargar. El resultado: <strong>varias personas golpeadas y una nueva detención, en este caso de un periodista.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Ambos detenidos fueron trasladados a la comisaría de la Policía Nacional </strong>(Moreda), donde permanecieron incomunicados <strong>hasta la mañana del lunes, que pasaron a disposición judicial.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Según informa su abogado, que no pudo hablar con ellos hasta el mismo lunes por la mañana, ambos detenidos pasaron previamente por el centro de salud para ser inspeccionados por un médico, ya que uno de ellos presentaba una brecha en la cabeza provocada por los golpes que recibió durante su detención.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tras prestar declaración, los dos detenidos fueron puestos en libertad a la espera de juicio, ya que <strong>se les imputan cargos de atentado contra la autoridad.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13927} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/08/detencionGijon1.jpg" alt="" class="wp-image-13927"/></figure>
<!-- /wp:image -->

<!-- wp:image {"id":13928} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/08/detencionGijon2.jpg" alt="" class="wp-image-13928"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->