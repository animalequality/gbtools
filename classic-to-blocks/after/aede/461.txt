<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Diese Fotos, die unser Rechercheteam aufgenommen hat, zeigen den Horror, den Schweine während ihrer Fahrt zum Schlachter erleiden müssen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Die Tiere bekommen oft <strong>weder Wasser noch Futter </strong>und sind für viele Stunden im Transporter eingesperrt. Aktivistinnen und Aktivisten haben schon häufig berichtet, <a href="https://www.facebook.com/AnimalEqualityGermany/videos/vb.259770724096761/514767265263771/?type=2&amp;theater" target="_blank" rel="noopener noreferrer">wie dringend die Tiere Wasser benötigen</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12089} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/slauughterhouse2.jpg" alt="" class="wp-image-12089"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Mehr als eine Million Schweine sterben jedes Jahr <strong>an den Folgen der schlechten Bedingungen</strong> während solcher Transporte.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12086} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/slaughterhouse3.jpg" alt="" class="wp-image-12086"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12087} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/slaughterhouse4.jpg" alt="" class="wp-image-12087"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Im Sommer sterben aufgrund <strong>extremer Temperaturen und Mangel an Wasser</strong> immer wieder Schweine während des Transports an einem Hitzekollaps. Im Winter hingegen sind viele bereits tot, bevor sie den Schlachtbetrieb erreichen, da sie vorher <strong>erfrieren</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12088} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/slaughterhoyse5.jpg" alt="" class="wp-image-12088"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Die Enge und der Stress setzen den Schweinen zu</strong>; einige fallen auf den Boden und brechen sich dabei die Knochen. Unfähig aufzustehen, werden sie unter ihren Artgenossen begraben, <strong>wo sie qualvoll ersticken</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Wenn sie im Schlachtbetrieb ankommen, werden die Tiere von den Arbeitern vor Ort oft <strong>getreten und geschlagen</strong>, um sie aus den Transportern zu scheuchen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Und das ist es, was dann im Schlachtbetrieb passiert.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe allowfullscreen="" mozallowfullscreen="" scrolling="no" src="https://player.vimeo.com/video/1321076" webkitallowfullscreen="" width="580" height="464" frameborder="0"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Bitte tragen Sie nicht zu diesem Leiden bei!</strong> Ersetzen Sie Fleisch und andere tierische Produkte <a href="http://www.v-versprechen.de" target="_blank" rel="noopener noreferrer">durch pflanzliche Alternativen</a>.</p>
<!-- /wp:paragraph -->