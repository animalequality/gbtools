<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Die Delfine verfangen sich in den Fischernetzen oder den Stellschrauben der Fischerboote, ziehen sich schmerzhafte Verletzungen zu und müssen einen qualvollen Tod sterben, weil sie nicht mehr an die Wasseroberfläche schwimmen und Luft holen können.</strong> In den meisten Fällen versinken ihre toten Körper oder werden von anderen Meerestieren verspeist. Die Dunkelziffer der auf diesem Wege durch die Fischereiindustrie ums Leben gekommenen Tiere ist somit vermutlich viel höher einzuschätzen. Grund für das massenhafte Sterben der Delfine könnte eine vor allem beim Fang von Seehechten genutzte Vorgehensweise sein, bei der die Fischernetze zwischen zwei parallel fahrenden Booten ausgeworfen werden. Nach einer längeren Schonfrist, wurde die Jagd auf diese Fischart nämlich erst vor circa drei Jahren wieder aufgenommen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Die französische Regierung will nun einschreiten, indem Fischerboote dazu verpflichtet werden, bestimmte Geräte zu nutzen, die akustische Warnsignale an die Tiere senden und so vertreiben sollen. Meeresschützer bewerten derartige Geräte jedoch als ungenügend, besonders, da die Fischer sie, aus Angst auch ihre Fangfische zu vertreiben, sowieso nur während der zuständigen Kontrollen einschalten würden. Zudem könnten die Signale zu einer zusätzlichen Belastung für die Meerestiere werden, da sie für weitere unnatürliche Störungen sorgen und so zum Lärm und der akustischen Meeresverschmutzung beitragen.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">
	<strong>Entscheiden Sie sich gegen das Massensterben in unseren Weltmeeren!</strong></h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Als Verbraucherinnen und Verbraucher haben wir die Möglichkeit uns aktiv gegen das Massensterben und die Ausbeutung der Meere zu entscheiden, denn unser Konsum wirkt sich unmittelbar auf das Schicksal der Meeresbewohner aus.</strong> Fische sind die weltweit am meisten konsumierten Tiere überhaupt. Sie werden in so hohen Stückzahlen gefangen, dass die Zahl der getöteten Individuen in Tonnen zusammengefasst wird. Für den industriellen Fischfang werden extrem grausame Methoden genutzt und die Tiere häufig auf barbarische Art und Weise geschlachtet. In den meisten Ländern gibt es keine Gesetze, die den Schutz und die Rechte der Fische regulieren.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Indem wir auf den Konsum von Fisch verzichten, können wir nicht nur das Leben der Fische, sondern auch das unzähliger anderer Meerestiere und Seevögel retten, die jährlich als Beifang durch die Fischernetze qualvoll ums Leben kommen und unter dem Lärm, der Verschmutzung und dem Stress leiden, der durch die extensive Fischfangindustrie entsteht.</strong> Denn die Ökosysteme der Ozeane leiden enorm unter diesen extremen Belastungen: Prognosen zufolge, könnten unsere Weltmeere aufgrund der systematischen Vernichtung der Meerestiere bereits bis Mitte des Jahrhunderts leergefischt sein. Innerhalb weniger Jahrzehnte, hat der Einfluss der Menschen die globalen Fischbestände bereits um 80 % reduziert.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Darüber hinaus bringt die Ausbeutung der Meere auch schlimme Folgen für viele Menschen mit sich, die, beispielsweise in Afrika, als Küstenbewohner auf den Fischfang angewiesen sind</strong>. Denn die hohe Nachfrage nach Fisch und Fischprodukten in den Industriestaaten führt dazu, dass die Überfischung der Gewässer in fast allen Gebieten der Erde drastisch fortschreitet und den Menschen in den ärmeren Ländern eine wichtige Existenzgrundlage entzieht.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Durch die Wahl einer pflanzlichen Lebensweise können wir uns jeden Tag für einen ethisch vertretbaren und nachhaltigen Konsum entscheiden. <strong>Und heutzutage ist es überhaupt nicht schwer, auf Fisch zu verzichten und diesen durch <a href="https://loveveg.de/5-vegane-alternativen-zu-fisch/" target="_blank" rel="noopener noreferrer">leckere pflanzliche Fisch-Alternativen</a> zu ersetzen.</strong> Entdecken Sie mit <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">Love Veg</a> die kulinarische Vielfalt der pflanzenbasierten Küche und nützliche Tipps, die den Umstieg zu einer ausgewogenen, veganen Ernährung erleichtern!</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sub>Quellen:</sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sub>ORF. (31.03.2019): <a href="https://orf.at/stories/3117150/" target="_blank" rel="noopener noreferrer">“Tote Delfine häufen sich an Frankreichs Küste”</a>.</sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sub>Greenpeace: <a href="https://www.greenpeace.de/biodiversitaet/meere/fischerei" target="_blank" rel="noopener noreferrer">“Restlos überfischt”</a>.</sub></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->