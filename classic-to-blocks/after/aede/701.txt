<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Foto: ©ARIWA</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Freispruch für Undercover-Ermittler! Heute morgen wurde in Magdeburg (Sachsen-Anhalt) in zweiter Instanz über einen Fall von Hausfriedensbruch im Zusammenhang mit einer Undercover-Recherche verhandelt. Der Vorsitzende Richter am Landgericht, Ulf Majstrak, fand deutliche Worte: "<em>Ihr Handeln ist als positiv zu bewerten. [...]</em><em> Sie haben genau das getan, was nötig war und was als mildestes Mittel zur Verfügung stand.</em>" Wenn staatliche Organe ihre Arbeit nicht so machten, wie es sein sollte, sei das Eingreifen der Bürger nötig, so der Richter.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Animal Rights Watch e.V. (ARIWA) hatte 2013 Filmmaterial aus der van Gennip Tierzuchtanlagen GmbH in Sandbeiendorf (Sachsen-Anhalt) veröffentlicht. <a href="https://www.flickr.com/photos/animalrightswatch/sets/72157671354550422" target="_blank" rel="noopener noreferrer">Die Aufnahmen dokumentierten massive Tierquälerei und schwere Verstöße gegen das Tierschutzgesetz</a> in einem der größten Schweinezuchtbetriebe Deutschlands. Während das Strafverfahren gegen Verantwortliche der van Gennip Tierzuchtanlagen GmbH eingestellt wurde, standen drei Undercover-Ermittler bereits im September 2016 wegen Hausfriedensbruchs am Amtsgericht Haldensleben vor Gericht. Jetzt wurden sie erneut freigesprochen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Das Urteil beweist einmal mehr: Mitgefühl ist keine Straftat. Tierquälerei muss weiterhin aufgedeckt werden.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sup>Quelle: n-tv (11.10.2017). "<a href="https://www.n-tv.de/panorama/Richter-billigt-Tierschuetzer-Aktion-im-Stall-article20077859.html" target="_blank" rel="noopener noreferrer">Richter billigt Tierschützer-Aktion im Stall</a>".</sup></p>
<!-- /wp:paragraph -->