<!-- wp:image {"id":1556} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/header_wbs_groß_860.png" alt="" class="wp-image-1556"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">"<em>Der Film, den wir euch gleich zeigen werden, ist nur schwer zu ertragen...</em>" so beginnt <a href="https://www.facebook.com/annemendenofficial" target="_blank" rel="noopener noreferrer">Schauspielerin Anne Menden</a> ihren Prolog für Factory Farm, einer 360°-Dokumentation: Zuschauer begleiten Animal Equalitys Rechercheleiter, Jose Valle, auf eine Reise hinter die Kulissen der Fleischindustrie. "<em>Wenn Sie erst einmal drin sind, gibt es kein Zurück. Dies ist Ihre letzte Chance zu gehen</em>", warnt dieser die Zuschauer zu Beginn.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Warum also nicht einfach umdrehen? Warum nicht einfach wegsehen? Weil wir verstehen müssen, welches Leid diese Tiere in der Massentierhaltung durchstehen müssen<em>.</em> Und weil wir alle die Macht haben, Zustände wie diese zu ändern. Doch dafür müssen wir uns ihrer erst bewusst werden:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/y-QHLBhvOT0" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Bitte teilen Sie dieses Video in Ihrem Freundeskreis, mit Ihrer Familie und Bekannten! Nur gemeinsam können wir eine bessere Welt für Tiere erreichen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">______________</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Tieren helfen: <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">www.LoveVeg.de</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Mehr über unser 360°-Projekt: <a href="https://animalequality.de/" target="_blank" rel="noopener noreferrer">www.iAnimal360.de</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Unsere Arbeit für Tiere unterstützen: <a href="http://www.ae-mitglied.de" target="_blank" rel="noopener noreferrer">www.ae-mitglied.de</a></p>
<!-- /wp:paragraph -->