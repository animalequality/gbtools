<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Unsere Ermittlerteams haben schon viel Leid und Elend gesehen, doch über die Zustände, die sie in einem Betrieb in Devon, Großbritannien, vorfanden, waren selbst sie schockiert: Riesige Container bis oben hin voll mit den leblosen Körpern hunderter Küken, entsorgt wie Abfall. Und mitten unter ihnen ein noch lebendes Küken, verletzt und schwach.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Das ist die Geschichte von Gloria:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11947} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/abfall-1.jpg" alt="" class="wp-image-11947"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11948} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/abfall2.jpg" alt="" class="wp-image-11948"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sub>Hunderte tote Tiere in überquellenden Containern, entsorgt wie Abfall. Sie haben die brutalen Zustände in der Fleischindustrie nicht überlebt.</sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Bereits bei unserem ersten Besuch in dem Betrieb Ende März 2016 – zu diesem Zeitpunkt waren die Küken eine Woche alt – hatten wir tote Tiere in den Containern und Ställen des Betriebs vorgefunden. Aber es war während unseres dritten Besuchs, Ende April, als unsere Ermittler einen der Container öffneten und darin ein noch lebendes, verletztes und schwaches Küken vorfanden: Gloria.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11992} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/gloria_abfall.jpg" alt="" class="wp-image-11992"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11989} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/gloria1-1.jpg" alt="" class="wp-image-11989"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sub>Zwischen den verwesenden Körpern ihrer toten Artgenossen kauert Gloria. Verletzt und schwach, aber lebendig! Unsere Ermittler beschließen, sie mitzunehmen.</sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Unsere Ermittler zögern keine Sekunde und nehmen das völlig entkräftete Huhn mit. Sie ist jetzt an einem sicheren Ort, wo sie dank fürsorglicher Pflege in den letzten Monaten zu einem glücklichen, aufgeweckten Huhn herangewachsen ist. Sie kann jetzt ausgedehnte Sonnen- und Staubbäder nehmen, in der Erde nach Futter picken, tun und lassen was sie will – so, wie es Hühner lieben.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11990} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/gloria2.jpg" alt="" class="wp-image-11990"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11991} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/08/gloria_3-1.jpg" alt="" class="wp-image-11991"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sub>Dank fürsorglicher Pflege geht es Gloria heute gut. Sie hat das Grauen der Fleischindustrie überlebt und ist jetzt ein glückliches, aufgewecktes Huhn.</sub></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Gloria hatte Glück. Sie hat die Fleischindustrie überlebt. Millionen andere liebenswerte Hühner wie sie, können dieser Hölle nicht entkommen. In der Fleischindustrie werden Hühner gezielt so gezüchtet, dass sie in kurzer Zeit so viel Fleisch wie möglich ansetzen. Das steigert den Profit der Industrie, doch den Preis müssen die Tiere zahlen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Warnung: Das Video enthält Szenen von Tierleid.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/S2XH-Ue69Gc" width="560"></iframe></p>
<!-- /wp:paragraph -->