<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Mexiko ist einer der größten Eierproduzenten und -konsumenten der Welt. Über 185 Millionen Hennen werden in dem Land zur Produktion von Eiern gehalten – <strong>ein überwältigender Großteil von ihnen in Käfighaltung.</strong> Bereits 2016 hatte Animal Equality als erste Organisation Aufnahmen aus Legehennenbetrieben im mexikanischen Bundestaat Jalisco veröffentlicht. Nun, zwei Jahre später, haben Tierschutz-Aktivist*innen erneut die Zustände in einem Betrieb dokumentiert. Was sie dort vorfanden, ist schockierend.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Warnung: Das Video enthält Szenen extremen Tierleids</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><iframe src="https://www.youtube.com/embed/ehguT4zl4Ew" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe>
In der Eierindustrie in Mexiko leben Hennen noch immer in engen Käfigen – unfähig dort auch nur ihre Flügel auszubreiten oder ein Nest zu bauen. Extrem unhygienische Bedingungen, wie in diesem Betrieb, sind keine Seltenheit in Käfighaltungssystemen der Eierindustrie. Überall in dem Betrieb tummeln sich Vogelmilben, die die Körper der Hennen befallen, ihr Blut saugen und Krankheiten übertragen. <strong>Eine der grausamsten Praktiken der Eierindustrie ist die „induzierte Mauser“.</strong> Der mehrtägige Wasser- und Nahrungsentzug soll die Legeleistung steigern. Für die Hennen bedeutet diese Praktik furchtbares Leid. Schwache Tiere werden von Artgenossen erdrückt und sterben qualvoll in den Käfigen.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Sobald die Legeleistung nachlässt, erwartet alle Hennen dasselbe Schicksal: der Schlachthof. Hennen sind soziale, intelligente und sensible Lebewesen. Ein solch qualvolles Leben haben sie nicht verdient. <strong>In der Eierindustrie durchleben Millionen Hennen diese Tortur – jedes Jahr.</strong> Animal Equality arbeitet in verschiedenen Ländern daran, die grausame Käfighaltung endlich abzuschaffen. Auch wenn „käfigfrei“ nicht „tierleidfrei“ bedeutet, so ist es dennoch ein erster wichtiger Schritt, der das Leid von Millionen Tieren verringern wird. Wir alle können den Hennen helfen, <a href="https://loveveg.de/bessere-optionen/eier/" target="_blank" rel="noopener noreferrer">indem wir Eier durch pflanzliche Alternativen ersetzen.</a></p>
<!-- /wp:paragraph -->