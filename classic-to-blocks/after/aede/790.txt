<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Nur etwas mehr als zehn Prozent ihres Einkommens geben Menschen in Deutschland durchschnittlich für ihre Nahrungsmittel aus. Die aktuell so niedrigen Preise der konventionellen Lebensmittel können nur aufgrund von staatlichen Subventionen gehalten werden. Sie stehen in erheblichem Ungleichgewicht zu den Umweltbelastungen, die bei ihrer Produktion entstehen. <strong><a href="https://www.presse.uni-augsburg.de/unipressedienst/2018/jul-sep/2018_097/" target="_blank" rel="noopener noreferrer" data-wplink-edit="true">Eine Studie der Universität Augsburg</a> zeigt nun: würden wir die Folgeschäden für Menschen, Tiere und Umwelt in die Kosten der Produkte einbeziehen, müssten vor allem Fleisch und andere Tierprodukte deutlich teurer auf dem Markt angeboten werden.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Treibhausgasemissionen, Stickstoffüberschüsse, hoher Energieverbrauch – zur Berechnung der ‘verborgenen Kosten’ der Lebensmittelproduktion berücksichtigen die Wirtschaftswissenschaftler Faktoren, die maßgeblich zur Belastung unserer Umwelt beitragen und unterscheiden dabei zwischen konventioneller und ökologischer Landwirtschaft. Diese ‘externen’ Kosten werden aktuell nicht von den Konsumenten bezahlt, sondern von der Allgemeinheit getragen, wie etwa bei der aufgrund von zu hoher Nitrat-Konzentration in unseren Böden notwendigen Aufbereitung unseres Trinkwassers.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"className":"rtejustify"} -->
<h4 class="wp-block-heading rtejustify"><strong>Die wahren Kosten der Massentierhaltung</strong></h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Der größte Unterschied besteht dabei zwischen dem Marktpreis von Fleischprodukten und den ‘wahren’ Kosten, welche die konventionelle Produktion nach sich zieht. Den Berechnungen der Wissenschaftler zufolge, müssten diese im Vergleich zu der aktuellen Bepreisung dreimal so teuer sein. Für konventionell hergestellte Milchprodukte sollten die Verbraucher doppelt so viel, für Obst und Gemüse knapp ein Drittel mehr bezahlen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Bei Bio-Lebensmitteln würden die Preise unter Berücksichtigung ihrer externen Kosten weniger ansteigen, unter anderem, da beim biologischen Pflanzenanbau auf Stickstoffdünger verzichtet und in der Tierhaltung weniger konventionelles Kraftfutter verwendet wird. Bei biologisch produziertem Fleisch lägen die Kosten demnach ca. 80%, bei Bio-Milch 35% und bei biologischem Obst und Gemüse ca. 6 % über dem aktuellen Preisdurchschnitt.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Grund für die immensen Preisaufschläge, die angesichts ihrer Folgen mit der Produktion tierischer Lebensmittel einhergehen, ist vor allem die energieintensive Aufzucht der Tiere.</strong> Die Belüftung und Beheizung der Ställe, der Anbau der Futtermittel und das durch den Stoffwechsel der Tiere freigelassene Methan führen unter anderem zu einer höheren Austragung von Stickstoff, einem höheren Energieverbrauch und dem Ausstoß größerer Mengen von Treibhausgasen, als es der Herstellung von pflanzlichen Produkten der Fall ist.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Andere komplexe Faktoren, die unser Ökosystem negativ beeinflussen, konnten bei der Untersuchung zudem gar nicht berücksichtigt werden. Fehlende Daten zu Antibiotikaresistenzen und Pestiziden, die durch die Produktion tierischer Lebensmittel in unsere Gewässer gelangen, lassen sich beispielsweise nur schwer in die Rechnung mit einbeziehen.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Verbraucher freuen sich über die niedrigen Lebensmittelpreise, denn den meisten Menschen sind die tatsächlichen ökologischen und sozialen Kosten dieser Preis- und Marktverzerrung gar nicht bewusst. Mit politischen Maßnahmen zur einer korrekten Bepreisung und der Aufklärung zu den wahren Folgen der Lebensmittelproduktion wäre es jedoch möglich, die Grundlagen für ein nachhaltiges Konsumverhalten in unserer Gesellschaft zu schaffen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Als Verbraucher können wir vor allem durch den Verzicht auf tierische Produkte zur Reduktion der produktionsbezogenen Umweltschäden beitragen und mit dem Umstieg auf eine rein pflanzliche Lebensweise eine Welt mitgestalten, in der die Tiere, unsere Umwelt und nachfolgende Generationen die nötige Achtung erfahren.</strong> Auf <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">LoveVeg.de</a> finden Sie leckere vegane Rezepte und viele zahlreiche Tipps zur Umstellung auf eine pflanzliche Lebensweise. <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">Wenn Sie sich für den kostenlosen Newsletter anmelden, erhalten Sie das LoveVeg- Kochbuch gratis dazu!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sup>Quellen:</sup></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sup>Focus Online (20.12.2018). "<a href="https://www.focus.de/gesundheit/gesundleben/fitness/studie-unsere-lebensmittel-sind-zu-billig_id_10096669.html" target="_blank" rel="noopener noreferrer">Unser Essen ist zu billig! Warum Fleisch eigentlich dreimal so teuer sein müsste</a>".</sup></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sup>Universität Augsburg (18.09.2018). "<a href="https://www.presse.uni-augsburg.de/unipressedienst/2018/jul-sep/2018_097/" target="_blank" rel="noopener noreferrer">Ladenpreis – wahrer Preis? Oder: Was kosten uns Lebensmittel wirklich?</a>".</sup></p>
<!-- /wp:paragraph -->