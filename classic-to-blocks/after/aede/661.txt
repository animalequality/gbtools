<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Die EU hat heute Geschichte geschrieben – für die Tiere. Mit einer deutlichen Mehrheit von 410 zu 205 Stimmen hat sich das Europäische Parlament in Straßburg für gesetzliche Maßnahmen ausgesprochen, um die Zustände in der Kaninchenzucht zu verbessern. Dazu gehört auch ein EU-weites Verbot von Käfighaltung. Über 340 Millionen Kaninchen leiden jedes Jahr in den winzigen Käfigen, wie sie – bislang in europäischen Zucht- und Mastbetrieben üblich sind.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Als Teil einer internationalen Kampagne gelang es Animal Equality mithilfe von engagierten Unterstützer*innen auf Abgeordnete aus ganz Europa auf unglaubliches Tierleid aufmerksam zu machen und zum Handeln zu bewegen. In mehr als 120.000 Protest-Mails appellierten die Menschen an die Abgeordneten für eine Initiative zu stimmen, die Stefan Eck, MdEP, angestoßen hatte. Stefan Eck forderte seit Langem gesetzliche Maßnahmen, um gegen das erschreckende Tierleid in der europäischen Kaninchenindustrie vorzugehen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Die internationale Protest-Mail-Kampagne von Animal Equality begleitete die Tage vor der Abstimmung im EU-Parlament. <a href="https://www.youtube.com/watch?v=5yN44m-Dh4A&amp;t=1s" target="_blank" rel="noopener noreferrer">Erschreckendes Video-Material</a>, das Animal Equality aus europäischen Kaninchenfarmen veröffentlicht hatte, sorgte für Aufmerksamkeit in den Medien. Auch zahlreiche Prominente, darunter Harry-Potter-Darstellerin Evanna Lynch, Forensiker und Autor Dr. Mark Benecke sowie Model und Bloggerin Victoria van Violence, machten sich für die Kampagne stark und teilten den Aufruf #BanRabbitCages in den sozialen Netzwerken.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1644} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/17016910_1334659719888059_1055324117865887939_o_0.jpg" alt="" class="wp-image-1644"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Erster Schritt für mehr Schutz für Kaninchen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Die enorme Unterstützung zeigt, wie sehr sich die Menschen in Europa mehr Schutz für Kaninchen und andere Nutztiere wünschen. Umso größer ist die Freude über das Ergebnis der Abstimmung: "Das EU-Parlament hat heute einen ersten, aber wichtigen Schritt unternommen, um das Leid der Kaninchen zu verringern", erklärt Ria Rehberg, Ko-Vorsitzende von Animal Equality Germany. Doch nun ist es an der EU-Kommission, einen Gesetzesvorschlag zu machen, um die Mindeststandards in der Kaninchenhaltung und das Käfigverbot festzulegen: ein Vorgang, der Monate dauern und von Verzögerungen begleitet werden kann. Animal Equality wird die Entwicklungen deswegen weiterhin genau beobachten und den Druck aufrechterhalten. "Mithilfe unserer Unterstützer*innen können wir so verhindern, dass die Maßnahmen zum Kaninchenschutz durch die EU-Kommission verwässert werden", so Ria Rehberg.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Während der letzten Jahre hat Animal Equality Videomaterial aus Kaninchenfarmen in Spanien und Italien veröffentlicht. Die <a href="https://www.youtube.com/watch?v=iT_f9GQ9rAk&amp;t=1s" target="_blank" rel="noopener noreferrer">Video-Aufnahmen aus über 75 Betrieben</a> bringen das Leid der Tiere an die Öffentlichkeit: Kaninchen mit offenen und infizierten Wunden, tote Tiere, die inmitten der lebenden in den Käfigen verwesen, und sogar Kannibalismus, ausgelöst durch den extremen Stress, den die Kaninchen erleiden.</p>
<!-- /wp:paragraph -->