<!-- wp:image {"id":1248} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/07/webseiteneintrag_cover.jpg" alt="" class="wp-image-1248"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Edeka bewirbt sein eigenes Markenfleischprogramm „Gutfleisch“ mit Transparenz, Regionalität und hohen Qualitäts- und Hygienestandards, die die Gesundheit der Tiere gewährleisten sollen. Gutfleisch wurde mehrfach ausgezeichnet und geht nach eigenen Aussagen über die von der Industrie vorgeschriebenen Standards hinaus. Eine eigene Webseite soll Transparenz schaffen, indem Verbraucherinnen und Verbraucher nachprüfen können, aus welchem Stall die Tiere stammen, die hinterher als Fleisch in den Regalen der Edeka-Filialen verkauft werden. <strong>Doch diese scheinbare Transparenz endet an der Stalltür. Denn was sich tatsächlich in Betrieben abspielt, die Edeka Gutfleisch beliefern, befindet sich jenseits jeglicher Vorstellungskraft.</strong> Die wirklichen Vorgänge hinter den Stalltüren der Schweinezüchter und -mäster möchte Edeka lieber im Verborgenen halten.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">In allen Betrieben, die das Ermittlerteam von Animal Equality besuchte, wurde es Zeuge erschreckender Zustände, grober Hygienemängel und unbeschreiblichen Tierleids. Aufnahmen aus sieben Schweinemast- und -zuchtbetrieben dokumentieren die Grausamkeiten, denen die Schweine und Ferkel tagtäglich in dieser Industrie ausgesetzt sind.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Schweine sind intelligente Lebewesen, die sich ihrer Existenz bewusst sind und ihr Leben genießen, wenn sie die Chance dazu haben. Sie können stundenlang miteinander spielen, am Boden herumstöbern, in der Sonne liegen und ihre Umgebung mit ihrem außerordentlichen Geruchssinn erkunden. Sie erkennen sich sogar selbst im Spiegel, sind lernfähiger als Hunde und intelligenter als dreijährige Kleinkinder. Schweine hören auf ihren Namen und können, genau wie Hunde, Befehle erlernen und Aufforderungen folgen. In verschiedenen Experimenten lernten Schweine Videospiele zu spielen, indem sie einen Joystick mit ihrem Mund betätigten. Schweine sind außerdem sehr liebevolle und kontaktbedürftige Tiere. Nähe und gegenseitige Körperpflege sind ihnen sehr wichtig, wobei sie ihr Gegenüber mit dem Rüssel abtasten und massieren. Die gegenseitige Körperpflege trägt erheblich zu ihrem Wohlbefinden bei. Ihr natürlicher Lebensraum sind Wälder mit Büschen und sumpfigen Plätzen. In Freiheit würden Schweine die meiste Zeit des Tages mit gemeinsamer Nahrungssuche verbringen und die nähere Umgebung erkunden.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Allein schon die alltägliche Umgebung in einem Stall verursacht für diese intelligenten, geruchsempfindlichen Tiere unglaubliches Leid: <strong>enorme Hitze, ätzende ammoniakbeladene Luft, keine Ausweichmöglichkeiten vor ihrem eigenen Kot, keinerlei Anregung oder Beschäftigung und keine Möglichkeit soziale Strukturen, Körperkontakt und -pflege auszuleben.</strong> Was es darüber hinaus bedeutet, ein Leben lang den systembedingten Grausamkeiten der Tiernutzungsindustrie ausgesetzt zu sein, ist kaum vorstellbar. Die weiblichen Schweine, die zur Zucht herhalten müssen, werden im Laufe ihres Lebens immer wieder künstlich befruchtet und fristen ihr Dasein oft auf engstem Raum. Sie leiden sowohl physisch als auch psychisch so unter diesen Bedingungen, wie auch wir leiden würden, wenn wir wochen- oder monatelang in kleinen Metallboxen fixiert wären, ohne uns umdrehen oder vorwärtsbewegen zu können.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Das Leben dieser Tiere und ihre Kapazität zur Reproduktion werden lediglich als Weg zur Produktion von mehr und mehr „Ware“ angesehen. Weibliche Schweine haben 12 bis 16 Zitzen, gebären durch Überzüchtung pro Schwangerschaft jedoch deutlich mehr Ferkel. <strong>Diese „überzähligen“, schwächeren Ferkel werden systematisch aussortiert und brutal getötet.</strong> Die Mütter, die nicht mehr die erwünschte Ferkelanzahl gebären können, werden umgehend umgebracht.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">►Warnung: Das folgende Video enthält Szenen, die Gewalt gegen Tiere zeigen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><iframe allowfullscreen="" src="https://www.youtube.com/embed/IWjp70rA4JU" width="560" height="315" frameborder="0"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Zusätzlich zu den systembedingten Grausamkeiten der Schweineindustrie hat das Ermittlerteam von Animal Equality in jedem der untersuchten Betriebe unbeschreibliches Tierleid, grobe Hygienemängel und zahlreiche Verstöße gegen die geltenden Richtlinien festgestellt und dokumentiert.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Dies sind die Rechercheergebnisse:</p>
<!-- /wp:paragraph -->

<!-- wp:list {"className":"style-basic-disc"} -->
<ul class="style-basic-disc"><!-- wp:list-item -->
<li><br>		sterbende, deutlich leidende Tiere ohne tierärztliche Behandlung in Boxen zusammen mit den anderen Tieren</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		Tiere mit starken Atembeschwerden, stark hustende Tiere (zu viele Tiere auf zu engem Raum bei ungenügender Lüftung führen oft zu chronischen Atemwegserkrankungen)</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		unbehandelte große Nabelbrüche, vermutlich mit Darmvorfall, die sehr schmerzhaft und bewegungseinschränkend wirken</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		Tiere, die so schwach sind, dass sie nicht mehr aufrecht stehen oder sitzen können und beim Versuch aufzustehen immer wieder umfallen</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		schreiende Tiere, die aufgrund der extremen Engesituation zwischen anderen Tieren eingeklemmt sind</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		Sauen stehen illegal zu lange fixiert in zu schmalen Kastenständen</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		Verletzungen infolge der Haltungsbedingungen wie Geschwüre, Klauenverletzungen, blutige Wunden an Schultern und Gliedmaßen, unbehandelte Othämatome (sogenannte Blutohren)</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		Kannibalismus (bedingt durch den enormen Platzmangel, zu viel Stress, evtl. auch noch durch andere Faktoren): angebissene Schwänze und Ohren mit blutenden und teils nekrotischen Veränderungen, die sicherlich extrem schmerzhaft sind</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		extreme Hygienemängel in allen untersuchten Betrieben, teilweise systematisch kein sauberes Trinkwasser, oftmals keine sauberen Liegemöglichkeiten für die Tiere</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>		stressbedingte Kämpfe und Rangeleien zwischen den Tieren, die zu Verletzungen führen</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Recherchematerial der Organisation Ariwa zeigt weiterhin, wie in zwei der größten Betriebe, die an Gutfleisch liefern, Ferkel systematisch auf brutalste Art und Weise behandelt werden. Die Neugeborenen werden an den Hinterbeinen kopfüber aus ihren Buchten gerissen, und schwächere Tiere werden ohne Betäubung so lange mit dem Kopf gegen Boden oder Boxenwände geschlagen, bis sie tot sind.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Auch lebendige Tiere werden wissentlich in Mülltonnen geworfen, wo sie einen langsamen Erstickungs- oder Hungertod finden oder den Verletzungen, die ihnen zugefügt wurden, erliegen. <strong>Animal Equality fordert Edeka dazu auf, das Markenfleischprogramm Gutfleisch einzustellen, weil die Zustände offensichtliche Tierqual, katastrophale hygienische Zustände und eine Täuschung der Verbraucherinnen und Verbraucher bedeuten. Weiterhin fordert Animal Equality Edeka dazu auf, das Sortiment um weitere pflanzliche Produkte aufzustocken, um den Konsum von mehr tierfreundlichen Alternativen zu ermöglichen.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11902,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/animalequalityde/sets/72157645232536310/" target="_blank" rel="noopener noreferrer"><img src="/app/uploads/2014/08/12-07-2014_collage_gutfleisch_recherche.jpg" alt="" class="wp-image-11902"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Die Bilder belegen eindeutig, dass Tiere auch auf prämierten Betrieben unter Transparenzlabeln an ihrer Gefangenhaltung leiden. Lebende und fühlende Individuen zu Produktionsmaschinen zu degradieren, kann nur zu Lasten der Tiere geschehen und ist in unserer Gesellschaft ethisch nicht mehr zu rechtfertigen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Es gibt eine einfache Lösung, wie wir dieses Leid gemeinsam beenden können. Unsere Entscheidungen bedeuten den Unterschied zwischen Leben und Tod für diese sensiblen, fühlenden Lebewesen. Wir können tausende Leben retten, indem wir uns dazu entscheiden, Fleisch durch tierfreundliche Alternativen zu ersetzen. <strong>Millionen Menschen weltweit haben sich bereits für pflanzliche, tierfreundliche Alternativen entschieden, die geschmacklich überzeugen. </strong>Helfen Sie und geben auch Sie noch heute das V-Versprechen ab und tragen Sie somit dazu bei, das Leid der Schweine nicht länger zu finanzieren.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11904,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="http://www.animalequality.de/v-versprechen" target="_blank" rel="noopener noreferrer"><img src="/app/uploads/2014/08/16-07-2014-vversprechen_gf.jpg" alt="" class="wp-image-11904"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Weitere Informationen über die Hintergründe der <a href="http://www.animalequality.de/essen/fleisch">Fleischindustrie</a> und wie Fleisch ganz leicht durch tierfreundliche Alternativen zu ersetzen ist, finden Sie auf unserer <a href="http://www.animalequality.de/rezepte/tierfreundliche-alternativprodukte">Webseite</a>.</p>
<!-- /wp:paragraph -->