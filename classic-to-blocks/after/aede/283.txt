<!-- wp:paragraph -->
<p>Animal Equality hat in Zusammenarbeit mit verschiedenen AktivistInnen und Rechtsanwälten vor dem Obersten Gerichtshof in Indien ein Verbot von Rennen mit Ochsenkarren durchgesetzt. Das Gericht nahm in seiner Begründung Bezug auf die im Mai 2002 erfolgte Aufnahme des Tierschutzes in das deutsche Grundgesetz. Das Urteil bezieht sich auch auf Sportarten wie Stierkampf und das sogenannte “Jallikattu”, ein jahrhundertealter Sport bei dem Preise von den Hörnern der Stiere gerissen werden. Organisationen wie Peta und das Animal Welfare Board of India hatten Klage gegen die Durchführung des Jallikattu Festes erhoben und die Verfahren wurden gemeinsam behandelt. Das Verbot gilt für ganz Indien und stellt einen historischen Meilenstein für die Rechte von Tieren dar.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Amruta Ubale, Koordinatorin des indischen Animal Equality Teams hatte in Zusammenarbeit mit den AktivistInnen Gargi Gogoi, Ajay Marathe und Anil Katariya und einer Reihe von Rechtsanwälten unermüdlich dafür gekämpft, die grausamen Rennen zu stoppen. Das Team präsentierte dem Gericht ausführliches Beweismaterial, welches den Richtern zeigte, dass die gesetzlichen Richtlinien bei den Rennen nicht eingehalten wurden.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Bei den grausamen Rennen wurden die Tiere geschlagen, ausgepeitscht, mit Elektroschockern gequält, mit Alkohol zwangsernährt und mit Nägeln und spitzen Gegenständen drangsaliert. Ihre Schwänze wurden verdreht, ihre Hoden gequetscht, durch ihre Nasen wurden Seile gezogen und daran gerissen. Diese Massenfolter wird dank des Obersten Gerichtshofes nun beendet.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fotos der Ochsenkarren Rennen in Indien:</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11911,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/animalequalityde/sets/72157644618310243/" target="_blank" rel="noopener noreferrer"><img src="/app/uploads/2014/05/2014-05-08_ochsenkarren.jpg" alt="" class="wp-image-11911"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Des Weiteren hat das Spitzengericht das Parlament angewiesen, die Rechte von Tieren in der Verfassung zu verankern und die Tierschutzgesetze in Indien durch höhere Strafen zu verbessern. Das Gericht nahm in seiner Begründung Bezug auf die im Mai 2002 erfolgte Aufnahme des Tierschutzes in das deutsche Grundgesetz. In Artikel 20a GG, in dem bereits die natürlichen Lebensgrundlagen geschützt sind, wurden die Worte „und die Tiere“ eingefügt. Artikel 20a lautet nun: „Der Staat schützt auch in Verantwortung für die künftigen Generationen die natürlichen Lebensgrundlagen und die Tiere im Rahmen der verfassungsmäßigen Ordnung durch die Gesetzgebung und nach Maßgabe von Gesetz und Recht durch die vollziehende Gewalt und die Rechtsprechung.“</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div align="justify">In den Worten Amruta Ubales, Koordinatorin von Animal Equality in Indien: „Dieses Urteil stellt einen Meilenstein in der Geschichte der Tierrechte dar und wir hoffen, dass es wegweisend für weitere Gerichtsverhandlungen im Name der Tiere sein wird.“</div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Wir begrüßen die Entscheidungen des Obersten Gerichtshofes in Indien. Durch das Urteil können unzählige Tiere vor grausamer Misshandlung bewahrt werden. Die jahrelange Arbeit von unserem <a href="https://www.facebook.com/AnimalEqualityIndia?fref=ts" target="_blank" rel="noopener noreferrer">indischen Animal Equality Team</a> hat sich wieder einmal ausgezahlt. Die Rechte von Tieren in Indien werden gestärkt und es wird Bewusstsein für das Leiden der Tiere geschaffen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Um solche bahnbrechenden Erfolge für Tiere erreichen zu können, sind wir auf Unterstützung angewiesen. Mit nur einem kleinen monatlichen Beitrag kann man mit uns Tieren in dieser Gesellschaft eine Stimme geben. Jeder Euro hilft uns, Tieren zu helfen:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>www.ae-mitglied.de</p>
<!-- /wp:paragraph -->