<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Animal Equality veröffentlicht erstmals Bilder, die die <strong>Standardpraktiken in der Milchindustrie in Mexiko</strong> zeigen. Kühe und Kälber sind diesen entsetztlichen Methoden ihr Leben lang ausgesetzt. Keiner anderen Organisation ist es bislang gelungen, Aufnahmen in der Milchindustrie in Mexiko zu machen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><a href="https://www.flickr.com/photos/animalequalityde/albums/72157678018057784" target="_blank" rel="noopener noreferrer">Das Video- und Bildmaterial</a> zeigt <strong>die nie endende Gewalt und das Leid der Tiere</strong>:</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br>			Kühe werden immer wieder <strong>zwangsgeschwängert</strong>. Wie jedes Säugetier geben sie nur dann Milch, wenn sie ein Kind geboren haben.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Kälber werden <strong>von ihren Müttern getrennt</strong>, wenn sie nur wenige Stunden oder wenige Tage alt sind. Ihnen wird die Muttermilch verweigert – denn das ist die Milch, die die Milchindustrie verkauft.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Kälber werden in enge, dreckige <strong>Einzelboxen</strong> gesperrt.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Kühe werden <strong>enthornt</strong> – eine Praktik, die in anderen Ländern bereits verboten ist.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Kühen werden die <strong>Schwänze abgeschnitten</strong>, was in vielen anderen Ländern ebenfalls verboten ist.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Die Tiere werden mit <strong>brennenden Metallstäben</strong> “markiert”.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Beim maschinellen Melken werden die Kühe immer wieder <strong>am Euter verletzt</strong>. Infektionen und Blutungen sind die Folge.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Infektionen und Verletzungen werden <strong>nicht tierärztlich behandelt.</strong></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			<strong>Sobald die Milchproduktion nachlässt</strong>, <a href="https://animalequality.de/neuigkeiten/2016/12/02/schlachthof-recherche-in-mexiko/" target="_blank" rel="noopener noreferrer">werden die Kühe im Schlachthaus getötet.</a></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:image {"id":1651,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/animalequalityde/albums/72157678018057784" target="_blank" rel="noopener noreferrer"><img src="/app/uploads/2017/03/5.jpg" alt="" class="wp-image-1651"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Derzeit sieht das mexikanische Recht <strong>keine Strafen</strong> für diejenigen vor, die sogenannte Nutztiere missbrauchen und grausam behandeln. Dulce Ramírez, Vorsitzende von Animal Equality in Mexiko, erklärt:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">“<em>Es ist Zeit, die bestehenden Gesetze in Mexiko zu verstärken, um diesem Missbrauch entgegen zu treten. Tierquälerei muss strafrechtlich verfolgt werden können. Die Tiere in der Nutztierindustrie werden wie Maschinen behandelt, geschlagen und in dreckige Buchten eingesperrt. Ihr Leid wird völlig ignoriert</em>”.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Hier finden Sie alle Bilder der Recherche: <a href="https://www.flickr.com/photos/animalequalityde/albums/72157678018057784" target="_blank" rel="noopener noreferrer">Brutale Aufnahmen aus der Milchindustrie in Mexiko.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Der einfachste und effektivste Weg den Schweinen, Kühen und Hühnern in den Zucht-, Mast- und Schlachtbetrieben dieser Welt zu helfen, ist diese Tiere von der eigenen Speisekarte zu streichen. <strong>Helfen Sie Tieren</strong> und probieren Sie öfter pflanzliche Gerichte: <a href="https://LoveVeg.de/" target="_blank" rel="noopener noreferrer">www.LoveVeg.de</a></p>
<!-- /wp:paragraph -->