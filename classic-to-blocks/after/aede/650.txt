<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Die Haltung von Hennen in sogenannten Legebatterien ist in der EU seit dem 1. Januar 2012 verboten. Die Legehennenhaltung erfolgt seither in Kleingruppen-, Boden-, Freiland- sowie Biohaltung. Eine neue Kampagne von Animal Equality in Spanien macht deutlich, dass die Kleingruppenhaltung als Nachfolgemodell zur alten Käfighaltung keine Verbesserung für die Hennen bedeutet – <strong>die Hennen leiden auch hierbei in Käfigen.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Das im Zuge der Kampagne veröffentlichte Video- und <a href="https://www.flickr.com/photos/animalequalityde/albums/72157678747834141" target="_blank" rel="noopener noreferrer">Bildmaterial</a> zeigt:</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br>			Hennen, die stressbedingt fast alle Federn verloren haben.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			tote und sterbende Tiere inmitten von noch lebenden Artgenossen.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			überquellende Leichencontainer.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Hennen mit gekürzten Schnäbeln (noch immer eine gängige Praxis in vielen Betrieben).</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			kranke Hennen, die zu schwach sind, um Nahrung oder Trinken zu erreichen.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Zahlreiche Tiere haben vom ununterbrochenen Stehen auf Gitterboden wunde, gebrochene und deformierte Füße. <strong>Nicht selten bleiben die Tiere mit ihren Füßen zwischen den Gitterstäben hängen.</strong> Ohne Wasser und Nahrung erreichen zu können, sterben die Hennen so langsam und qualvoll.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><iframe allowfullscreen="" class="giphy-embed" frameborder="0" height="270" src="https://giphy.com/embed/5lnfwsnkczpzW" width="480"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11940,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/animalequalityde/albums/72157678747834141" target="_blank" rel="noopener noreferrer"><img src="/app/uploads/2017/02/Collage_Spain.jpg" alt="" class="wp-image-11940"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">«<em>Die Haltung von Hühnern in Käfigen zählt zu den grausamsten Praktiken der Tierindustrie. Die Verbraucher haben ein Recht darauf zu erfahren, was Kleingruppenhaltung wirklich bedeutet und wie diese Tiere dort leben. Animal Equality wird alles dafür tun diese Haltungsform zu beenden</em>», so Javier Moreno, Vorsitzender von Animal Equality in Spanien.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">In Deutschland soll die Haltung von Hennen in Kleingruppen zwar bis 2025 abgeschafft werden, doch immer wieder haben <a href="https://www.youtube.com/watch?v=DYPWO0NEug4" target="_blank" rel="noopener noreferrer">Recherchen von Animal Equality</a> gezeigt, dass die Tiere auch in alternativen Haltungsformen – wie Boden-, Freiland- oder Biohaltung – leiden. <strong>Keine Haltungsform wird den natürlichen Bedürfnissen der sensiblen, intelligenten Vögel gerecht.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Am einfachsten und sichersten können wir Tierleid mindern oder verhindern, wenn wir tierische Produkte durch pflanzliche ersetzen. Haben Sie <a href="http://www.animalequality.de/neuigkeiten/kochen-und-backen-ohne-eier" target="_blank" rel="noopener noreferrer">diese tollen Alternativen</a> zu herkömmlichen Eierspeisen und Eiprodukten schon probiert?</p>
<!-- /wp:paragraph -->