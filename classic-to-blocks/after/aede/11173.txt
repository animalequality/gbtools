<!-- wp:paragraph -->
<p>Im Dezember 2016 veröffentlichten wir <a href="https://www.flickr.com/photos/igualdadanimal/albums/72157673090582753" target="_blank" rel="noopener noreferrer">unvorstellbar grausames Foto- und Videomaterial</a> aus 21 Schlachthöfen in Mexiko. Die umfangreiche Schlachthof-Recherche erschütterte die Öffentlichkeit. In Mexiko gibt es bislang keine Gesetze, die sogenannte Nutztiere vor Misshandlung schützen. Die aktuellen Tierschutzgesetze, die für Hunde und Katzen gelten, enthalten keine Regelungen für die Tiere in den Zucht-, Mast- und Schlachtbetrieben der Nahrungsmittelindustrie. <strong>Eine von Animal&nbsp;Equality in Mexiko unterstützte Initiative hat dies nun, zumindest in dem mexikanischen Bundesstaat Jalisco, geändert.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/albums/72157673090582753"><img src="https://live.staticflickr.com/5469/31096940852_cb4f625e17_z.jpg" alt="Rastros de México"/></a></figure>
<!-- /wp:image -->

<!-- wp:html -->
<script async="" src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!-- /wp:html -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Die Auswirkungen der Initiative</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Wer in Zukunft sogenannte Nutztiere misshandelt, begeht damit eine Straftat. Das hat der Kongress des mexikanischen Bundesstaates Jalisco im April 2019 mit überwältigender Mehrheit beschlossen. <strong>Die Gesetzesänderung hat direkte Auswirkung auf jährlich 211 Millionen Tiere: </strong>so viele werden alleine im Bundesstaat Jalisco jedes Jahr in Schlachthöfen getötet. Grausame Misshandlungen wie jene, die unsere Veröffentlichung aus dem Jahr 2016 ans Tageslicht brachte, können künftig strafrechtlich verfolgt werden. Dies ist ein bedeutsamer Fortschritt für den Tierschutz in Mexiko: <strong>Zum ersten Mal gilt damit die Misshandlung von sogenannten Nutztieren in einem mexikanischen Bundesstaat als Straftat.</strong> Ob die Betriebe sich in Zukunft auch an die Vorschriften halten, werden unsere Kolleg*innen vor Ort selbstverständlich genau beobachten.</p>
<!-- /wp:paragraph -->