<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><a href="https://animalequality.de/kampagnen/schweinehoelle-in-spanien" target="_blank" rel="noopener noreferrer">Diese schrecklichen Bilder</a> aus einem Schweinemastbetrieb in Spanien haben wir Anfang diesen Jahres veröffentlicht. Tierschutz-Aktivist*innen hatten sie heimlich aufgenommen. Der Tierschutz-Skandal erstreckte sich bis nach Deutschland, denn: Der Betrieb beliefert den großen spanischen Fleischhersteller El Pozo, dessen Wurstwaren über Amazon, Edeka und Rewe auch hierzulande erhältlich sind. Durch unsere Petitionen, die bislang allein in Deutschland mehr als 97.000 Unterschriften umfassen, forderten wir diese Unternehmen auf, den Verkauf der tierquälerischen El-Pozo-Produkte umgehend zu stoppen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Dank unserer Petitionen hatte El Pozo sich daraufhin zunächst zu einem Gespräch mit Animal Equality über das Umsetzen und Einhalten höherer Tierschutzstandards bereit erklärt. Doch jüngst machte der Präsident des Unternehmens in einem Interview gegenüber diesen Tierschutz-Plänen einen Rückzieher.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Jetzt gibt es neue Entwicklungen im Fall El Pozo und Verstrickungen, die bis zu einer Veröffentlichung von Animal Equality im Jahr 2012 zurückreichen. Entwicklungen, durch die wir die Verantwortungslosigkeit des Unternehmens einmal mehr an die Öffentlichkeit bringen:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><iframe src="https://www.youtube.com/embed/Tn3Q2OhFzLc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<!-- /wp:paragraph -->