<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Animal Equality veröffentlicht heute schockierende Aufnahmen aus dem Bio-Milch-Betrieb Coombe Farm, der die britische Supermarktkette Waitrose beliefert. Tierschutz-Aktivist*innen hatten den Betrieb nach einem anonymen Hinweis besucht. Das Foto- und Videomaterial zeigt den brutalen Umgang mit Kälbern: <strong>Traumatische Trennung, Zwangsernährung, Schläge und Beschimpfungen</strong> – all das müssen die Tiere über sich ergehen lassen. Über die erschreckenden Zustände berichteten unter anderem die bekannten britischen Medienplattformen <a href="http://www.dailymail.co.uk/news/article-6097539/Footage-shows-workers-organic-farm-used-supply-Waitrose-throwing-calf-ground.html" target="_blank" rel="noopener noreferrer">Daily Mail</a>, <a href="https://www.mirror.co.uk/news/uk-news/newborn-calves-organic-dairy-farm-13133491" target="_blank" rel="noopener noreferrer">Mirror</a> und <a href="https://www.independent.co.uk/climate-change/news/organic-milk-video-footage-animal-cruelty-farm-equality-uk-a8508956.html" target="_blank" rel="noopener noreferrer">The Independent</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Arbeiter, die neugeborene Kälbern mit Plastikschläuchen brutal zwangsernähren</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>einen Arbeiter, der ein Kalb während der Zwangsfütterung zu Boden wirft und dem Tier ins Gesicht schlägt</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>einen Arbeiter, der mit seinem ganzen Gewicht auf einem Kalb steht und dabei “<em>you f**king. . .sh*t</em>” schreit</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>verängstigte Kühe, die ihre Kälber zu beschützen versuchen</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Kälber, die bis zu 29 Stunden keinen Zugang zu Trinkwasser haben</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>ein neugeborenes Kalb, das an seinen Hinterläufen über den Boden geschleift wird</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Kälber, die routinemäßig innerhalb von 24 Stunden nach ihrer Geburt von ihren Müttern getrennt werden</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>zahlreiche Kühe, deren Hinterbeine aneinander gekettet sind</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><iframe src="https://www.youtube.com/embed/_bci5ZAiEac" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Dieser brutale Umgang ist weit von dem idyllischen Bild entfernt, das die Milchindustrie den Konsument*innen vor allem hinsichtlich Bio-Produkten vorgaukelt. Die traumatische Trennung von Kuh und Kalb ist jedoch traurige Realität – auch in bio-zertifizierten Betrieben. Die Aufnahmen der Tierschutz-Aktivist*innen haben darüber hinaus gezeigt, dass Coombe Farm noch <strong>nicht einmal die Mindestanforderungen im Umgang mit Kälbern</strong> einhält. Animal Equality hat das gesammelte Foto- und Videomaterial an die zuständige Tierschutzbehörde RSPCA sowie an die Supermarktkette Waitrose weitergeleitet. Auch Bio-Labels und Zertifikate können Kühe und Kälber nicht vor Leid bewahren. Wir schon. Entscheiden Sie sich noch heute für <strong>leckere, pflanzliche Alternativen zu Kuhmilch</strong> – <a href="https://loveveg.de/bessere-optionen/milchprodukte/" target="_blank" rel="noopener noreferrer">die Auswahl war noch nie so groß!</a></p>
<!-- /wp:paragraph -->