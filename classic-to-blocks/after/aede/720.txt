<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Erneut hat sich unser Ermittlerteam in die italienische Lammfleischindustrie eingeschleust. Es recherchierte in einem der rund 200 Schlachthöfe in Italien, in denen das Schlachten ohne vorherige Betäubung erlaubt ist. Aufnahmen mit versteckten Kameras zeigen, wie die Schlachthof-Mitarbeiter systematisch gegen Gesundheits-, Hygiene- und vor allem Tierschutzvorschriften verstoßen. Die Lämmer sind im Schlachthof sowohl körperlichem als auch seelischem Leid ausgesetzt:</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br>			Ausnahmslos alle Tiere werden ohne vorherige Betäubung bei vollem Bewusstsein geschlachtet.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Die Lämmer werden wiederholt von Schlachthof-Mitarbeitern geschlagen und getreten.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Die Tiere werden gewaltsam geschoben oder am empfindlichen Schwanz gezogen.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="315" src="https://www.youtube.com/embed/H5mtp4YIDsQ" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Das betäubungslose Schlachten ist laut EU-Vorschriften nach wie vor erlaubt, wenn es um religiöse Schlachtriten geht. Zwar brauchen die Schlachthöfe dafür spezielle Genehmigungen, regelmäßige Kontrollen finden jedoch kaum statt. Unsere Kolleginnen und Kollegen in Italien haben eine Kampagne gestartet, mit der sie die italienische Regierung dazu auffordern, Tierquälerei in Schlachthöfen unter Strafe zu stellen. Außerdem fordern sie eine weitflächige Videoüberwachung in Schlachthöfen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Matteo Cupi, Geschäftsführer von Animal Equality in Italien sagt: „<em>Selbst in Schlachthöfen, die sich an die Vorschriften halten, leiden die Tiere unvorstellbare Qualen. In einer modernen Gesellschaft wie der unseren ist es unverständlich, dass es kein Gesetz gibt, das die Tiere vor dieser unnötigen, rohen Gewalt schützt. Es ist an der Zeit, das zu ändern.</em>"</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Der einfachste und beste Weg diesen Tieren zu helfen ist, sie <a href="https://loveveg.de/">von der Speisekarte zu streichen</a>.</p>
<!-- /wp:paragraph -->