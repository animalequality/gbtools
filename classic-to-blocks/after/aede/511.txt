<!-- wp:image {"id":1477} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/fisch1.jpg" alt="" class="wp-image-1477"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Vielleicht ist es etwas, worüber Sie sich noch nie Gedanken gemacht haben, aber Fische sind die in ihrer Anzahl am meisten konsumierten Tiere weltweit. Die Zahlen sind astronomisch hoch. So hoch, dass es sogar schwer ist, sie zu visualisieren: Es wird geschätzt, dass <strong>jährlich etwa ein bis drei Milliarden Fische gefangen werden.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><a href="https://www.youtube.com/watch?v=wmtudnEDLmA" target="_blank" rel="noopener noreferrer">Die Tiere werden in großen Netzen gefangen und sterben einen qualvollen Tod</a>, der sich über mehrere Minuten bis Stunden ziehen kann. Viele von ihnen werden in den Netzen zu Tode erdrückt, während der Rest aufgrund von <strong>Ersticken, Erfrieren, Erschlagen oder Entbluten </strong>sein Leben lassen muss. Die Zahlen beinhalten auch jene Tiere, die in Fischfarmen gezüchtet werden. Die Methoden, mit denen sie getötet werden, sind grausam und unmenschlich.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":11978} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/01/fish_cute.jpg" alt="" class="wp-image-11978"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Die Wissenschaft bekräftigt nicht nur die Annahme, dass Fische Schmerzen, <strong>sondern auch Angst empfinden </strong>können. Können Sie sich die Angst dieser Tiere vorstellen, wenn sie gewaltsam durch Fischnetze gezwungen werden ihre vertraute Umwelt zu verlassen?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Professor Donald Broom, Biologe an der Universität Cambrigde, fasst dies wie folgt zusammen: „<em>Es gibt sinneseindrückliche Unterschiede zwischen Fischen und Säugetieren weil Fische im Wasser leben, aber <strong>Fische empfinden Schmerz sehr ähnlich wie es Säugetiere und Vögel auch tun</strong></em>“.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Professor John Webster, Forscher für Tierschutz an der Universität Bristol, hält fest, dass „<em>die Annahme, dass Fische keine Schmerzen empfinden, von keinem Wissenschaftler verteidigt werden kann</em>“.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Was würden Tierfreunde sagen?</strong> Sind wir auf der Seite dieser intelligenten und sensiblen Tiere? Hören wir auf ihre leisen Schreie nach Hilfe? Diese Schreie sind genauso verzweifelt wie jene unserer geliebten Hunde und Katzen, wenn sie Schmerzen haben oder verstoßen werden.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Bitte streichen Sie Fisch, Fleisch und andere tierische Produkte von Ihrer Speisekarte und ersetzen Sie sie durch bessere Optionen. Das geht ganz leicht! <strong><a href="https://LoveVeg.de/" target="_blank" rel="noopener noreferrer">Hier zeigen wir Ihnen, wie das geht</a>.</strong></h4>
<!-- /wp:heading -->