<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Um die enorme Nachfrage zu stillen, beziehen die beliebtesten Fast-Food-Konzerne der Welt ihr Fleisch aus industrieller Massentierhaltung. Zur Qualitätsmessung der dortigen Haltungsbedingungen wurden im Rahmen der Studie <em>„<a href="https://www.worldanimalprotection.org/sites/default/files/media/int_files/the_pecking_order_full_report.pdf" target="_blank" rel="noopener noreferrer">The Pecking Order</a></em>“ (dt. Hackordnung) öffentlich zugängliche Informationen genutzt und nach drei Kriterien beurteilt:</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list {"ordered":true} -->
<ol><!-- wp:list-item -->
<li><strong>Wie wird Tierschutz in den Betrieben praktisch umgesetzt?</strong></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><strong>Welche Ziele verfolgen die Unternehmen, um das Leben der Hühner zukünftig zu verbessern?</strong></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><strong>Wie werden die Fortschritte im Bereich des Tierwohls dokumentiert und der Öffentlichkeit zugänglich gemacht?</strong></li>
<!-- /wp:list-item --></ol>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Abhängig von der Punktzahl in den jeweiligen Kategorien beurteilte die britische Organisation <em>World Animal Protection</em> die Konzerne mit einer abschließenden Gesamtnote. Keiner von ihnen erhielt in dem Schlussergebnis eine bessere Durchschnittsbewertung als <em>„</em><em>poor</em>“ (schlecht), der größte Anteil sogar <em>„</em><em>very poor</em>“ (sehr schlecht), und zwei Unternehmen das schlechtmöglichste Urteil <em>„failing“</em> (durchgefallen).</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"className":"rtejustify"} -->
<h4 class="wp-block-heading rtejustify"><strong>Versagen im Bereich Tierwohl</strong></h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Erschreckenderweise besitzt keine der Fast-Food-Ketten effektiv umgesetzte, strategische Richtlinien zur Verbesserung des Wohlbefindens der Hühner.</strong> Aufgrund fehlender Regelungen sind häufig sogar besonders qualvolle Praktiken, wie die Tötung ohne ausreichende Betäubung Routine. Während zudem nur drei der Riesenkonzerne Interesse geäußert haben, die Hauptursachen für das Leid der Tiere bekämpfen zu wollen, geben alle Unternehmen <strong>nur wenig oder gar keine Informationen</strong> zu ihren Leistungen im Bereich des Tierwohls preis. In Fällen in denen bereits Tierschutzrichtlinien entwickelt wurden, sind sie unzureichend und werden nicht in greifbare Ziele umgesetzt.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"className":"rtejustify"} -->
<h4 class="wp-block-heading rtejustify"><strong>Entwürdigende Zustände</strong></h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Etwa 60 Milliarden Hühner werden jedes Jahr weltweit in derartiger Intensivhaltung getötet. Vor ihrer Schlachtung leben sie nur etwa 42 Tage – unter katastrophalen Haltungsbedingungen. In der Massentierhaltung werden sie häufig mit zehntausenden Artgenossen in überfüllte Hallen gesperrt. Durchschnittlich hat jedes von ihnen weniger als die Fläche eines Din A4 Blattes Platz zum Leben. Um ein schnelles Wachstum zu fördern, werden sie hier fast permanent unnatürlichem Licht ausgesetzt, wodurch es ihnen an Phasen der Dunkelheit und somit der Möglichkeit zum Ausruhen mangelt. <strong>Weil ihre Körper so schnell wie möglich an Größe und Gewicht zunehmen sollen, sind die Tiere heute völlig überzüchtet.</strong> So leiden sie meist ihr gesamtes Leben unter chronischen Schmerzen, Lahmheit oder Herzinsuffizienz.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2175} -->
<figure class="wp-block-image"><img src="/app/uploads/2019/01/masthuehner-klein.jpg" alt="Huhn mit Entzündungen" class="wp-image-2175"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">In den kargen Hallen werden ihnen <strong>keine Beschäftigungsmöglichkeiten</strong> zur Verfügung gestellt und sie haben keine Chance, nach Futter zu suchen, die Gegend zu erkunden oder in Staub zu baden, um ihr Gefieder zu putzen. Dabei sind diese natürlichen Verhaltensweisen enorm wichtig für die Gesundheit der Hühner: zum Vergnügen, zum Stressabbau und um körperlichen Schäden vorzubeugen. Das Streu, auf dem sie leben müssen, ist zudem häufig von schlechter Qualität.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Nass und von den ammoniakhaltigen Ausscheidungen der Tiere durchtränkt, ruft es bei ihnen <strong>schmerzhafte Ausschläge, Lungenschäden und Augenprobleme</strong> hervor, weshalb sie auch das natürlichen Scharren unterlassen. Unter diesen Umständen ist es für die Hühner unmöglich, ein aktives, gesundes Leben zu führen. Sie sind ständigem Stress und Schmerzen ausgesetzt.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Um das Leben der Hühner in derartigen Großhaltungsbetrieben zu verbessern, werden weltweit bessere Standards benötigt. <strong>Gerade die größten Fast-Food-Unternehmen der Welt haben die Macht und die Verantwortung, das Leid von Millionen von Hühnern zu verringern.</strong> Deshalb arbeitet unser Team in den USA zum Beispiel momentan daran, <a href="https://mcchickencruelty.com/" target="_blank" rel="noopener noreferrer">McDonald’s</a> davon zu überzeugen, besonders tierquälerische Praktiken in der Hähnchenmast abzuschaffen und endlich nachhaltige Tierschutzvorschriften einzuführen. Diese würden das Leid der Tiere zwar nicht beenden, aber zumindest verringern.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Mit der Wahl unserer Lebensmittel tragen auch wir große Verantwortung, denn unser Konsum wirkt sich unmittelbar auf das Schicksal anderer Lebewesen aus. Probiere es deswegen mit einer tierleidfreien Lebensweise! Auf <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">LoveVeg.de</a> findest du viele tolle Rezepte und nützliche Tipps zu einer ausgewogenen, pflanzlichen Ernährung.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">*<em>McDonald’s, Subway, Starbucks, Domino’s North America, Domino’s UK &amp; EU, Nando’s, KFC,Pizza Hut, Burger King</em></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><sup>Quelle: World Animal Protection (2018). <em><em>„</em><a href="https://www.worldanimalprotection.org/sites/default/files/media/int_files/the_pecking_order_full_report.pdf" target="_blank" rel="noopener noreferrer">The Pecking Order</a>“</em>.</sup></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->