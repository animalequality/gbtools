<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Im Juni 2017 sorgten Undercover-Aufnahmen aus zwei niedersächsichen Hähnchenmastbetrieben für öffentliche Empörung: <a href="http://www.animalequality.de/haehnchenmast/" target="_blank" rel="noopener noreferrer">Wie unsere Aufnahmen zeigen</a>, werden Masthühner in den Betrieben <strong>regelmäßig lebendig entsorgt.</strong> Einer der Betriebe beliefert den Marktriesen Wiesenhof, der andere Rothkötter. Dank versteckter Kamera ist zum Beispiel auch zu sehen, wie in dem Wiesenhof-Zulieferbetrieb Arbeiter die Hühner <strong>brutal treten,</strong> an den Gliedmaßen packen und in Transportboxen schleudern. Ohne Rücksicht auf das Verletzungsrisiko wurden auch in dem Rothkötter-Zulieferer Küken auf Haufen geschüttet.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Mehrere Vorgänge in dem veröffentlichten Videomaterial stellen für Rechtsexperten und Veterinäre <strong>gravierende Verstöße gegen das Tierschutzgesetz</strong> dar. Sie sehen es als erwiesen an, dass hier Straftaten vorliegen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Animal Equality hatte bei der zuständigen Staatsanwaltschaft <strong>Strafanzeigen gestellt:</strong> wegen mutmaßlicher Verstöße gegen das Tierschutzgesetz und gegen zahlreiche weitere Tierschutzvorschriften, wie die Tierschutz-Nutztierhaltungsverordnung, die Tierschutztransportverordnung, die Tierschutz-Schlachtverordnung, die EU-Tiertransport-Verordnung sowie weitere verbindliche Vorgaben aus dem europäischen Tierschutzrecht.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Doch erneut stellen sich die Behörden gegenüber dem dokumentierten Tierleid blind. Die Staatsanwaltschaft Oldenburg hat jüngst <strong>die betreffenden Verfahren eingestellt.</strong> In Zusammenarbeit mit Dr. Christoph Maisack, Richter am Amtsgericht und Erster Vorsitzender der deutschen Juristischen Gesellschaft für Tierschutzrecht (DJGT) e. V., hat Animal Equality nun <strong>Beschwerde gegen die Verfahrenseinstellungen</strong> eingelegt.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Bitte schauen Sie sich die Stellungnahme unserer Pressesprecherin Dr. Katharina Weiss an und erfahren Sie was Sie und wir jetzt tun können:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><iframe src="https://www.youtube.com/embed/E0OJFUN28E4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">„<em>Es ist empörend, dass die zuständige Staatsanwaltschaft die Vorfälle nicht strafrechtlich verfolgt. Außerdem herrscht bei den zuständigen Behörden anscheinend in bestürzendem Ausmaß Unkenntnis über Tierschutz-Vorschriften</em>”, so Dr. Christoph Maisack. Unsere Aufnahmen zeigen zum Beispiel, wie die eingesammelten Küken in den Eimern minutenlang unter den Körpern ihrer toten Artgenossen <strong>erstickt und erdrückt </strong>werden. Dennoch sehen die Behörden hierin keinen Nachweis, dass die Tiere länger anhaltendem Leiden ausgesetzt waren - und damit keinen Straftatbestand laut §17 Nr. 2b des Tierschutzgesetzes erfüllt.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">„<em>Diese Zurückhaltung durch das deutsche Rechtssystem bei Verstößen gegen das Tierschutzgesetz ist kein Einzelfall</em>”, so Dr. Christoph Maisack. Im Mai 2017 hatten Videoaufnahmen aus einem weiteren niedersächsischen Hähnchenmastbetrieb ähnliche schockierende Zustände gezeigt: Auch hier wurden Küken lebendig entsorgt. Doch auch hier wurde das Verfahren wegen mutmaßlicher Straftat von den zuständigen Behörden eingestellt.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Doch <strong>wenn die Justiz versagt,</strong> ist es umso wichtiger, dass wir den Tieren unsere Stimme leihen: <a href="http://www.animalequality.de/haehnchenmast/" target="_blank" rel="noopener noreferrer">Bitte unterzeichnen Sie die Petition</a> und wenden Sie sich mit uns an das niedersächsische Landwirtschaftsministerium, endlich gegen diese Tierqual vorzugehen.</p>
<!-- /wp:paragraph -->