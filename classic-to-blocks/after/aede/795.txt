<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Dem wissenschaftlichen Ausschuss für Tiergesundheit und Tierschutz (AHAW) der Europäischen Behörde für Lebensmittelsicherheit (EFSA) zufolge, gehen zahlreiche Probleme und Gefahren mit Lebendtiertransporten einher. Vor allem die ungewohnte Situation des Be- und Entladens ist für die Tiere extrem stressig. Viele von ihnen ziehen sich auf dem Weg in die Fahrzeuge schmerzhafte Verletzungen zu. <strong>Unzählige Videoaufnahmen aus verschiedenen Schlachthöfen zeigen, wie die Tiere getrieben, getreten oder geworfen werden.</strong> Rutschige Böden, scharfe Kanten und Spalten führen zudem oft zu schmerzhaften Prellungen, Schnittwunden oder Knochenbrüchen, wenn die Tiere sich stoßen oder ausrutschen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Größere Tiere wie Rinder oder Schweine werden standardmäßig auf den Flächen von ein bis zweistöckigen LKWs verteilt, kleinere Tiere wie Hühner oder Enten werden eingefangen und in gestapelten Käfigen oder Boxen transportiert. Abgeordnete des Europaparlaments kritisierten im vergangenen Jahr bereits die mangelhafte Umsetzung der EU-Transportrichtlinie. <strong>Die Ladekapazität wird oft so stark ausgereizt, dass den Tieren nicht einmal genügend Platz zur Verfügung steht, um aufrecht zu stehen oder ohne Verletzungsrisiko zu liegen.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Eingesperrt neben oftmals unbekannten Artgenossen fehlt es ihnen aufgrund der schlechten Luftzirkulation an Sauerstoff. Trotz EU-Verordnung, die angemessenes Füttern und Tränken vorschreibt, finden auch lange Transporte häufig ohne ausreichende Nährstoffversorgung statt. <strong>Hungrig und dehydriert müssen besonders alte, kranke und schwangere Tiere unter den Transportbedingungen unvorstellbare Qualen erleiden.</strong> Obwohl sie, wie besonders junge Tiere, den Regelungen zufolge als nicht transportfähig gelten, werden viele von ihnen den lebensgefährlichen Strapazen ausgesetzt.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Gerade in besonders heißen oder kalten Monaten wird die Fahrt zum Schlachthof für die Tiere zur schlimmen Tortur. Nach gesetzlicher Vorschrift müssen die Temperaturen innerhalb des Transportmittels kontrolliert und zwischen 5 und 30 Grad gehalten werden. <strong>Während es in anderen EU-Ländern wie Frankreich oder den Niederlanden deshalb temporäre Transportverbote gibt, finden in Deutschland jedoch viele Transporte auch bei extremer Hitze oder eisiger Kälte statt.</strong> Die Folgen für die Tiere: Überhitzung, Kälteschäden und im schlimmsten Falle sogar der Tod.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Eine große Anzahl von Tieren erkrankt unter diesen Belastungen, vor allem bei sehr langen Transporten, die sich oft über Stunden, manchmal sogar Tage hinausziehen. Ihr Wohlbefinden verschlechtert sich mit zunehmender Transportdauer und dennoch gibt es hier keine absolute Zeitbegrenzung. <strong>Viele Tiere sterben bereits auf dem Weg zum Schlachthof.</strong> Bei Hühnern in der Fleischmast wird unter diesen Umständen deshalb sogar eine gewisse Sterberate mit einkalkuliert. Angesichts der aufgrund von Überzüchtung kranken und geschwächten Körper ist das Sterberisiko bei ihnen, genau wie bei Schweinen, nämlich besonders hoch.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Durch die zunehmende Monopolisierung und Zentralisierung europäischer Großschlachtbetriebe werden die Strecken zwischen den Schlachthöfen und Haltungsbetrieben immer größer. Über lange Transportwege exportiert Deutschland jährlich Millionen von Tieren in andere EU-Länder. <strong>Besonders besorgniserregend sind zudem Transporte, die über EU-Grenzen hinausgehen, zum Beispiel in Länder des nahen Ostens oder Nordafrika.</strong> Hier werden die in Europa geltenden Gesetze kaum eingehalten und besonders viele Tierschutz- Verstöße begangen. Abgeordnete von FDP und den Grünen hatten 2018 deswegen schärfere Kontrollen der Lebendtiertransporte in Drittländer und, im Antrag der Grünen, sogar den Stopp der Transporte gefordert, sollten die Regelungen nicht einzuhalten sein. Vom Agrarausschusses des Bundestags wurden die Anträge mit den Stimmen von CDU/CSU, SPD und AfD jedoch Ende September abgelehnt.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Unser Konsum wirkt sich unmittelbar auf das Schicksal anderer Lebewesen aus. <strong>Mit der Wahl pflanzlicher Alternativen können wir uns täglich gegen die unnötige Quälerei der Tiere entscheiden und ihr Leid verringern.</strong> Probieren Sie es im neuen Jahr mit einer veganen Ernährung! Auf <a href="https://loveveg.de/" target="_blank" rel="noopener noreferrer">LoveVeg.de</a> finden Sie viele hilfreiche Tipps zu einer ausgewogenen pflanzlichen Ernährung und leckere Rezepte zum Nachkochen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Quellen:</strong></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br>			Agrarzeitung (04. Dezember, 2018): <a href="https://www.agrarzeitung.de/nachrichten/politik/europaparlament-insbesondere-alte-tiere-leiden-unter-transport-85366?crefresh=1" target="_blank" rel="noopener noreferrer">Studie: Alte Tiere leiden unter Transport</a>.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			ZDF (20. November, 2018): <a href="https://www.zdf.de/politik/frontal/qualvolle-tiertransporte-100.html" target="_blank" rel="noopener noreferrer">Qualvolle Tiertransporte – Das Leiden der Rinder</a>.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>			Albert Schweitzer Stiftung für unsere Mitwelt: <a href="https://albert-schweitzer-stiftung.de/massentierhaltung/tiertransporte-zahlen-fakten" target="_blank" rel="noopener noreferrer">Tiertransporte: Zahlen und Fakten</a>.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->