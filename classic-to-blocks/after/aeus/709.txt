<!-- wp:html -->
<div id="petition">

Every year the European Union provides a large subsidy of over £110 million via the Common Agricultural Policy (CAP) to farmers who breed bulls for bullfights and other blood fiestas.

British taxpayers help fund bullfighting in Spain with over £13.5 million a year.<sup>1</sup>

This support towards bullfighting conflicts with Article 13<sup>2</sup>&nbsp;of the Treaty on the Functioning of the European Union, which states:&nbsp;<em>‘That the Union [...] and the Member States shall, since animals are sentient beings, pay full regard to the welfare requirements of animals.’</em>

Bullfighting constitutes cruelty to animals, and without the backing of the European Commission and Spanish Administrations funds, the industry would be on the verge of collapse.
<p style="text-align: center;"></p>

</div>
<!-- /wp:html -->