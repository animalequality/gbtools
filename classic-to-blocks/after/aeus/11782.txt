<!-- wp:paragraph -->
<p>One million European Union (EU) citizens have signed the #EndTheCageAge European Citizens Initiative (ECI) petition, the biggest ever political push for farmed animal welfare.

<strong>THE DETAILS:</strong> In Europe, hundreds of millions of animals spend their entire lives in cruel and unnecessary cages. The #EndTheCageAge ECI was launched in September 2018 by Compassion In World Farming in partnership with more than 170 EU organizations, including Animal Equality, to end the use of cages for farmed animals across Europe.

The campaign has held events throughout the EU, obtained media coverage in international newspapers, and offered a simple way for all EU citizens to set up individual pages encouraging others to add their name to the cause. Now, after just eight months, more than one million signatures to abolish the use of cages in livestock farming have been gathered - a first in the history of farmed animal welfare.

<strong>THE NUMBERS:</strong> In Europe, there are over 300 million farmed animals that spend their entire lives behind bars, with Spain as the country with the largest number of animals caged - 92 million. Nearly 100% of all rabbits, 82% of chickens and 98% of female cows used to breed spend their lives confined. This same cruelty happens to newborn calves, as well as ducks and geese subjected to forced feeding to produce foie gras. These cages are typically barren, often overcrowded, and deny these animals space to move freely.

<strong>WHY THIS MATTERS:</strong> The ECI will bring about a seismic shift in food and farming systems, with the potential to benefit hundreds of millions of farmed animals each year in Europe by sparing them enormous suffering. The ending of cages across the continent would be a monumental step on the path towards ending factory farming.

<strong>WHAT WE’RE SAYING:</strong> "Animals deserve a life free from the cruelty of cages and that’s why Animal Equality is supporting the European Citizen Initiative, End the Cage Age," explained Javier Moreno, Executive Director of Animal Equality in Spain. "By joining forces with so many other organizations, we have the power to create a future free of cages for farmed animals."

<strong>WHAT COMES NEXT:</strong> Despite having reached one million signatures, the campaign to gather support will continue until September 2019 to demonstrate to the European Parliament the overwhelming support of the ECI and ensure its success. The remarkable milestone reached so far shows that EU citizens no longer accept the horrible caging of farmed animals and are, en masse, demanding change.

<strong><a href="https://igualdad-animal.endthecageage.eu/en-INT/live">LEARN MORE ABOUT THE #ENDTHECAGEAGE INITIATIVE</a></strong></p>
<!-- /wp:paragraph -->