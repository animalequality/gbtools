<!-- wp:paragraph -->
<p>Whether you’re a lifelong vegetarian, an out and proud vegan, or a curious omnivore it’s safe to say we can all agree that there are things that happen on factory farms that are totally unacceptable.

The chicken industry is rife with abuse and cruelty and these are just a few of the main reasons why we refuse to ever eat chicken again:

<strong>Many die as babies before they even reach the farm</strong>

Bred in chicken hatcheries by the millions baby chicks are processed like cargo rather than delicate little animals. Many fall through assembly lines, get crushed my machines, fall to the floor, or get smashed in shipping crates.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe class="giphy-embed" src="https://giphy.com/embed/YHjzjA6FHnr0Y" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Bred to grow so fast they can barely function</strong>

Through years of selective breeding chickens raised for their meat now grow so fast and so large that they suffer crippling deformities. Many are incapable of standing up to access food and water dishes causing them to die of hunger or dehydration. Others succumb to breathing difficulties, organ failure, and even heart attacks.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe class="giphy-embed" src="https://giphy.com/embed/YqWeLdQEH8SWI" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Injured and sick birds are left to suffer and die</strong>

Birds are viewed as mere property and are worth just a few cents to each farmer so even those who suffer painful injuries and illness do not receive any veterinary care. Even severe injuries will go untreated leaving the animal to die an agonizing death.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe class="giphy-embed" src="https://giphy.com/embed/11pJZZ42xnIpK8" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Raised in filthy conditions</strong>

The conditions inside the crowded and dark sheds these animals are raised in are filthy and on many farms the litter is only changed every few months - after several flocks have used the same litter. Sick and dying animals are often left to decompose among the living. The unsanitary conditions are hotbeds for disease.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe class="giphy-embed" src="https://giphy.com/embed/Akzv8PjblkuB2" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Cruel slaughter</strong>

Chickens are obviously killed for their meat but few people realize the systemic cruelty that occurs in the slaughter process. After a harrowing journey in packed crates they arrive at the slaughterhouse, are hung upside down on the processing line, are shocked in an electrified pool of water, and then have their throats slit causing them to bleed out and die a horrific death.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe class="giphy-embed" src="https://giphy.com/embed/ivuGwo8YvU6Kk" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

With so many plant-based chicken alternatives from brands like Gardein and Beyond Meat, <strong>it’s never been easier to leave this cruelty off your plate</strong>.</p>
<!-- /wp:paragraph -->