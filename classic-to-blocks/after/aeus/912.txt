<!-- wp:image {"id":4267} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/10/360_pigs_truck_animalequality-crop-TOpigsave-web.jpg" alt="" class="wp-image-4267"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>Kindness and compassion are on trial today in Ontario, Canada.</strong>

In a case that is gaining international attention, Anita Krajnc is back on the witness stand today because she chose to show compassion to pigs in need.

"When you see a creature suffering, you have a duty or an obligation to come close and try to help,” she says.

We agree. That is why&nbsp;<strong>Animal&nbsp;Equality extends its support to Anita by sharing exclusive 360º VR footage</strong>&nbsp;of pigs trapped inside transport trucks, who were also likely on their way to slaughter. Her lawyers will submit this groundbreaking virtual reality footage to the judge.

Anita was charged with criminal mischief because she gave water to pigs who were cramped in a hot and dirty truck.&nbsp;She could face up to six months in jail or a $5,000 fine if convicted.

Toronto Pig Save&nbsp;is also making <a href="https://ianimal360.com/" target="_blank" rel="noopener">Animal Equality's&nbsp;iAnimal 360º</a> virtual reality&nbsp;experience available outside the Burlington courthouse from 7:30 a.m. onwards on October 3.&nbsp;<strong>The iAnimal 360 experience is so powerful</strong> that a majority of&nbsp;the people who watch&nbsp;the video say they will stop eating pig flesh or other animals and many are willing to try and go vegan.

Though we hope for an end to baseless trials like this one, we look forward to the spotlight this court case is shining on the horrific conditions that farmed animals suffer.

You can help further that mission&nbsp;by <a href="https://animalequality.org/donate/" target="_blank" rel="noopener">donating to Animal Equality</a>&nbsp;and <strong>becoming a monthly&nbsp;supporter</strong>.

&nbsp;

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Photo:&nbsp;Elli Garlin&nbsp;</p>
<!-- /wp:paragraph -->