<!-- wp:paragraph -->
<p>Imagine Earth in 2050, the human population is now 9.6&nbsp;million and the ocean temperature has risen several alarming degrees. Coral reefs no longer exist. Along with them, a great portion&nbsp;of life in the seas has disappeared. Large marine&nbsp;areas are completely empty. All of the&nbsp;wonderful sea creatures have either been&nbsp;caught or killed by the degradation of the seas. The oceans are dead or about to die. Scary, right?

This is not a horror movie, it is&nbsp;a forecast that the “Living blue planet” (from the international organization <a href="https://www.worldwildlife.org/">WWF</a>) report projects. In it, the organization is alert to&nbsp;the impact that fishing and global warming,&nbsp;among other factors, are having on the oceans.

The updated study of marine mammals, birds, reptiles, and fish shows that populations have declined, on average, almost 50 percent worldwide in the past four decades, and in some cases, several fish have&nbsp;declined&nbsp;by 75 percent.

In that report, they warn us that “in a single generation, human activity has seriously damaged the oceans by capturing fish faster than they can reproduce, and while this happens, their feeding zones are being destroyed. Profound changes are needed to ensure an abundant marine life."

Along with the crisis of declining fish stocks, the report shows significant declines in coral reefs, mangroves, and seagrass beds. The study exclaims that coral reefs and seagrass beds could be lost worldwide by&nbsp;2050 as a result of climate change. Over 25% of all marine species live in coral reefs.

The study emphasizes that climate change is causing the ocean to change faster than any other time in a lapse of millions of years. The rising temperatures and acidification caused by carbon dioxide only aggravate the already negative impacts of fishing.

In this regard, we recall that factory farming is causing more greenhouse gases than the entire transportation industry put together,&nbsp;boats, cars, and planes, combined.

It is time to make a decision. How about leaving fish out of your next meal? We want oceans to thrive with marine life, as well as to slow&nbsp;climate change. We can help achieve this!&nbsp;There are already <a href="https://loveveg.com">delicious and healthy alternatives</a>.</p>
<!-- /wp:paragraph -->