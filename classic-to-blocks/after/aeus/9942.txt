<!-- wp:paragraph -->
<p>ACE analyzes charities <strong>on the basis of their effectiveness, efficiency and real impact on animals</strong>, and this year, out of a total of 300 analyzed, ACE has chosen three organizations to recognize the title "Top Charity".

As part of its review, ACE specifically highlighted our ability to use our resources effectively, particularly in regard to undercover investigations, and to continually monitor our programs for their effectiveness and impact.

According to ACE:

<em>“We think that Animal Equality does an exceptional job with the resources they have. They are able to produce and market undercover investigation videos at a low cost relative to other organizations, and their efforts to evaluate and improve their work are strong.”</em>

ACE also focused on our strong international programs and collaborations with other organization as particular strengths.

In their review, ACE states:

<em>“Animal Equality is firmly established in eight countries, and because the exploitation of nonhuman animals is a global problem, international work seems likely to be an essential part to a global solution. Animal Equality is also highly collaborative with other organizations and plans to continue working with other organizations. Collaboration and sharing resources across the animal advocacy movement may increase its impact overall.”</em>

Animal Equality is making a broad and significant impact on the lives of farmed animals around the world. You are a part of that! And because we are a Top Charity, you can rest assured that your donations help even more animals. <a href="https://animalequality.org/donate/">Donate today</a> to continue our vital work into the next year. Thank you!
</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":9987,"align":"center"} -->
<figure class="wp-block-image aligncenter"><img src="https://animalequality.org/app/uploads/2018/11/1024x512_Tw_post.jpg" alt="" class="wp-image-9987"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"></p>
<!-- /wp:paragraph -->