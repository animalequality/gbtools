<!-- wp:paragraph -->
<p>Whistleblowers have long been the courageous ones to expose wrongdoings. In fact, the history dates back as far as 1906 with the release of Upton Sinclair’s <a href="http://www.amazon.com/gp/product/1503331865/ref=pd_lpo_sbs_dp_ss_1?pf_rd_p=1944687522&amp;pf_rd_s=lpo-top-stripe-1&amp;pf_rd_t=201&amp;pf_rd_i=0486419231&amp;pf_rd_m=ATVPDKIKX0DER&amp;pf_rd_r=115YWKNVGT4WNNAVV2Y7"><em>The Jungle</em></a>, which exposed the horrific conditions inside American meatpacking plants. This ultimately led to the passage of the federal Meat Inspection Act, the Pure Food and Drug Act, and the formation of the Food and Drug Administration.

Having an unbiased eye inside an operation, such as a factory farm, is exactly what has led to the anti-whistleblower <a href="http://www.nytimes.com/2013/04/07/us/taping-of-farm-cruelty-is-becoming-the-crime.html?_r=0">ag-gag bills</a>. When recording cruelty inside farms and slaughterhouses started becoming a common method for animal protection groups in order to expose the truth behind closed doors, the meat and dairy industry started to worry. Instead of attempting to correct the <a href="http://www.animalequality.net/node/834">malicious acts</a> by their own employees, they pushed for laws to make undercover filming and photography illegal, and furthermore, to punish those trying to expose the truth.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4568} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/crueltyaggag.jpg" alt="" class="wp-image-4568"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Thanks to a widespread opposition by consumers, many of the ag-gag laws have failed, although measures have still been enacted in eight states. However, on January 1, a new law took into effect in North Carolina where it is now not only illegal for whistleblowers to record at factory farms and slaughterhouses but at all workplaces in the state. This law comes on the heels of the failed attempt at only including agricultural facilities, as the finalized law was able to survive by covering everyone equally.

This new law attempting to silence those who seek truth and justice is a clear violation of freedom of speech and a direct threat to animal welfare. With <a href="https://www.youtube.com/user/animalequality/playlists">countless investigations</a> Animal Equality has done inside farms and slaughterhouses, it is clear that the only way to help prevent acts of animal cruelty is to expose them when they occur.

Not only is this an issue of animal welfare, but a food safety issue as well. People are becoming more conscious about their food choices and where their food is coming from. Ag-gag laws increase the risk of foodborne illness and take away our right to know the conditions under which our food was produced. Ag-gag will not stop us from fighting for the truth and the protection of both humans and animals.</p>
<!-- /wp:paragraph -->