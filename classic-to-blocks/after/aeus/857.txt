<!-- wp:paragraph -->
<p><a href="http://izq.mx/noticias/29/01/2016/las-conoces-estas-son-algunas-organizaciones-animalistas-mas-destacadas-de-latinoamerica/">According to IZQ Noticias</a>, a Mexican newspaper that just considered Animal Equality one of the most important organizations in Latin America stated;&nbsp;"Animal Equality is characterized by its powerful investigations, and inspires&nbsp;people in order to promote changes in society and laws in favor for the animals."

This is wonderful news as it&nbsp;adds to the recognition we received last December by <a href="http://www.animalcharityevaluators.org/recommendations/top-charities/animal-equality/">Animal Charity Evaluators</a> who considered Animal Equality a top charity – one of the three most effective animal charities in the world!

Here is some of the most important work we are carrying out in Latin America:

<strong>Investigations</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4250} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/2612373101_f99c9ae07e_z.jpg" alt="" class="wp-image-4250"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Our&nbsp;investigators are working in Mexico and Venezuela <strong>at this very moment,</strong> putting themselves at risk, to expose the cruelties behind abusive animal facilities.

<strong>Street Screenings</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4209} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/12688038_605309842951743_7964551788128717246_n.jpg" alt="" class="wp-image-4209"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

In Guadalajara, for example, Animal Equality is continuing to work with the campaign #DescubrirLaComida&nbsp;(Discover The Food) – an innovative educational project that inspiring people on the streets and online to try vegetarian. Every weekend in Guadalajara, Tijuana and Mexico City (Distrito&nbsp;Federal), the street screenings allow people to become aware of animal cruelty. <a href="https://descubrirlacomida.com/">Online, 6,963 people have pledged to try vegetarian.</a><strong>Leafleting&nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4210} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/12733356_604516603031067_4740268069028408829_n.jpg" alt="" class="wp-image-4210"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Leafleting is another very effective way to educate people. Throughout Mexico City, Caracas,&nbsp;Tijuana, and Guadalajara we have animal activists deliver thousands&nbsp;of guides that provide&nbsp;information on how and why we should help animals. Information is power for change! The more people are informed on the cruelties&nbsp;these animals face, the more likely they will be willing to help. This year, over 15,000 leaflets have been handed out already!

<strong>Media</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4297} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/61651c64-f265-40d7-ba72-c8cc66ab87fa.jpg" alt="" class="wp-image-4297"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Over the years, we&nbsp;have developed a fantastic relationship with the media. Thanks to that and our work, Animal Equality is appearing in Latin American media<strong> weekly </strong>with full page stories on animal cruelty in SinEmbargo, Izq Mexicana, and Cronica Jalisco. <a href="http://www.sinembargo.mx/18-02-2016/1624347">Check out this weeks story on SinEmbargo</a>!</p>
<!-- /wp:paragraph -->