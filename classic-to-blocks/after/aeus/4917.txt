<!-- wp:paragraph -->
<p>These disciplinary proceedings follow an expose of the <strong>foie gras farm by Animal&nbsp;Equality in July 2012.</strong>

The Health and Consumer Affairs Department has also ordered "<em>the immediate end to all activities on the farm, and an end to the processing and sale of all foie gras products - both in public and in private businesses</em>".

The evidence presented by Animal Equality includes voice recordings at the Momotegi farm, in which the Olga Posse Oliveira admits there are irregularities, and the veterinarians and health inspectors turn a "blind eye" to them.

Besides multiple violations of basic health and hygiene standards, Mrs. Posse and her staff kill the ducks by stabbing them with a knife in their necks whilst fully conscious, which invariably must cause tremendous pain until they finally bleed to death.

Livers obtained on the farm were then sold illegally to the <strong>chef Andoni Luis Aduriz</strong>, who served the foie gras in what is considered to be the <strong>third best restaurant in the world, Mugaritz.</strong> He and other members of Mugaritz were aware of the irregularities, and the cruelty that was taking place on the farm and defended the cruel production of foie gras.

Health and Consumer Affairs Department of the Basque Country has authenticated the material obtained by Animal Equality.

These disciplinary proceedings follow other successes in Animal Equality’s campaign against the foie gras industry. <strong>The COOP in Italy - one of the largest chains of supermarkets</strong> - with over 1,470 outlets, agreed to end the sale of foie gras in October following the investigation. In addition, <strong>eight MEPs held a conference in Brussels </strong>calling for a ban on foie gras, referencing the Animal Equality investigation and petition.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed"><iframe src="https://player.vimeo.com/video/45679233?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=c0e7f8" width="560px" height="315px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div><strong>You can help us continue the progress by supporting a ban of foie gras in the United States: <a href="https://animalequality.org/action/foie-gras">Sign the petition now</a>!</strong></div>
<!-- /wp:html -->