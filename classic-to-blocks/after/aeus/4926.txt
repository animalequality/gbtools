<!-- wp:image {"id":4459} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/12/article-2253325-16A7F55F000005DC-352_634x422.jpg" alt="" class="wp-image-4459"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Nine employees have been charged, including two managers, at Wheatland, Wyo.-based Wyoming Premium Farms with cruelty to animals following an HSUS undercover investigation. The investigation documented workers kicking, punching, and swinging pigs and piglets. At the time of the investigation, which was released in May, Wyoming Premium Farms was supplying pigs to Tyson Foods—the world's second-largest processor and marketer of chicken, beef, and pork.

Wyoming Premium Farms’ Assistant Manager Shawn Colson was charged with seven counts of animal cruelty. Others charged with cruelty include David Brian Bienz, Jr. (two counts); Kali E. Oseland (four counts); Edward Raymond (Jake) Pritekel (three counts); Richard Pritekel (four counts); Kyla Erin Adams (two counts); Steve Perry (three counts); Jarrod Barney Juarez (two counts), and Patrick D. Rukavina (three counts). An animal cruelty conviction in Wyoming carries a maximum penalty of up to two years in prison and/or a $5,000 fine.

The investigation and information provided to law enforcement had documented rampant animal abuse at Wyoming Premium and showed workers kicking live piglets like soccer balls, swinging sick piglets in circles by their hind legs, striking mother pigs with their fists and repeatedly and forcefully kicking them as they resisted leaving their young.

The undercover video also shined a bright light on the day-to-day miseries endured by mother pigs who are forced to spend months on end nearly immobilized inside narrow gestation crates. Unable to even turn around – these smart and social animals are treated like mere piglet-producing machines.
<strong>Warning: The video you are about to see, contains graphic footage</strong></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed"><iframe src="https://www.youtube.com/embed/bNY4Fjsdft4?rel=0" width="640px" height="360px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>What can you do?</strong> Each one of us, can make the difference for pigs and all animals every time we sit down to eat, simply by leaving them off our plates. <strong>Start today</strong>: visit www.bit.ly/changeinyourhands</p>
<!-- /wp:paragraph -->