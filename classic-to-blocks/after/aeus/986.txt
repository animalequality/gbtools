<!-- wp:paragraph -->
<p>Following dialogue with Animal Equality and other organizations, Esselunga has <a href="https://www.esselunga.it/cms/azienda/impegno-per-il-benessere-animale/benessere-animale.html">announced</a> a <strong>commitment to stop selling eggs from hens crammed in cruel cages</strong>. In addition, Esselunga has extended the policy to include most of its own branded products containing liquid eggs. The policy goes into effect immediately and will <strong>reduce the suffering of almost 900,000 birds</strong> each year.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4074} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/Esselunga-blog.jpg" alt="" class="wp-image-4074"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Animal Equality recently released a <a href="https://animalequality.it/news/2017/02/28/il-vero-prezzo-delle-uova-la-nuova-scioccante-investigazione-di-animal-equality-italia/">shocking investigation</a> of caged hens in the Italian egg industry. The images show how hens trapped in cages suffer as they are forced to lay an unnatural amount of eggs for human consumption. The investigation was widely covered in the media and following its release, several companies, such as <a href="http://www.animalequality.net/node/969">Giovanni Rana</a>, <a href="http://www.animalequality.net/node/973">Gemos</a>, and <a href="http://www.animalequality.net/node/976">Dussmann</a>, made commitments to banning cages.

Esselunga’s competitors Coop and Lidl have already publicly committed to banning cages. <strong>These three policies send a strong signal to Italy’s retail sector that cages have got to go.</strong>

While cage-free doesn’t mean cruelty-free, these commitments are an important step toward reducing animal suffering. However, the best way to help hens is by simply leaving eggs off your plate. <strong>Get started with <a href="http://www.animalequality.net/node/915">these handy egg replacers</a>!</strong></p>
<!-- /wp:paragraph -->