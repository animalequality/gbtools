<!-- wp:paragraph -->
<p>Due to increasing public concern regarding animal treatment, in an unexpected report today, Ringling Bros. and Barnum &amp; Bailey Circus will be ceasing the use of elephants by 2018. CEO Kenneth Feld stated, <em>"The decision was not easy, but it is in the best interest of our company, our elephants, and our customers."</em>

It’s certainly the end of an era, but the beginning of hope and a new sense of freedom for the 13 elephants who are currently in the Ringling Bros. circus. These animals will be transported to a rescue for these mammals, the Center for Elephant Conservation in Florida, where over 40 other elephants remain retired.

Sadly, other animals will remain enslaved under the power of the 145-year-old circus which states that it will continue to showcase, horses, dogs, tigers and other animals in its performances.</p>
<!-- /wp:paragraph -->