<!-- wp:paragraph -->
<p>We’ve all heard the horror stories...<a href="https://www.bbc.co.uk/news/newsbeat-35302029">woman finds worm in chicken nugget</a>...<a href="https://www.ajc.com/news/national/women-find-worms-food-mcdonald-restaurants/BiauZqc1t2QmgD6RrV69pJ/">women finds worm in sandwich</a>… (cue gag reflex). Worms are usually transmitted through the fecal oral route. The worm eggs are excreted in droppings and then ingested by birds that scratch and peck at the soil, litter or droppings.

<strong>SO MANY PARASITES:</strong> While thoughts of parasites in your food may give you the heebie jeebies, <strong>imagine having to live with them constantly attacking your body.</strong> Here’s a list of some known chicken parasites that live <em>on</em> their host:

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center><img class="alignnone wp-image-37867 size-full" src="https://animalequality.org/app/uploads/2023/08/2023-Chicken-inside-a-factory-farm-looking-at-the-camera-600x400-1.jpg" alt="" width="601" height="400"></center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>MITES:</strong> Chicken mites are known carriers of encephalitis (an infection that causes inflammation of the brain) and may also cause fowl mite dermatitis and acariasis (aka mange). Chicken mites primarily feed on the blood of birds, but will often bite humans.

<strong>FLEAS:</strong> Fleas spend little time on bird hosts, but often infest nests and can easily transfer onto pets or humans. Bird fleas are a concern in hen houses and battery cages, and can rapidly reproduce.

<strong>TICKS:</strong> Ticks which bite and embed themselves onto humans, including larvae, nymphs and adults. Besides being pesky, the risk of Lyme disease contraction is reason for concern.

<strong>LICE:</strong> Lice of all kinds enjoy the barrier that feathers create, some feeding on the feathers themselves. A variety of such lice also enjoy human skin and hair.

<strong>FLIES:</strong> Many species of flies, such as mosquitoes, horse-flies, blow-flies and warble-flies, cause direct parasitic disease to domestic animals, and transmit organisms that cause diseases.

<strong>PARASITIC FUNGI:</strong> Fungi grows on decomposing skin and feathers of birds themselves. Fungi also grows on bird nests and in bird droppings, and are associated with many inhalation-contracted diseases. This fungi can produce disease by invading, harming and destroying body tissues of the host or by producing mycotoxins.

<strong>THE SAD TRUTH:</strong> Unlike if these parasites were on you or your dog, many chickens never receive treatment or relief during their short and miserable lives. They are often being eaten alive by parasites in massive filthy sheds with up to 50,000 birds in each one. The best way to reduce this is by avoiding overcrowding and by maintaining clean living spaces. These are things many companies have committed to improving, but <strong>McDonald’s still refuses to publicly make these changes.</strong><strong><a href="https://mcchickencruelty.com/?utm_source=Blog&amp;utm_medium=CTA&amp;utm_campaign=McDs&amp;utm_content=082219">Join us in asking McDonald’s to do better for chickens.</a></strong>

Kuldeep Dhama, Sandip Chakraborty, Amit Kumar Verma, Ruchi Tiwari, Rajamani Barathidasan, Amit Kumar and Shambhu Dayal Singh, 2013. Fungal/Mycotic Diseases of Poultry-diagnosis, Treatment and Control: A Review. Pakistan Journal of Biological Sciences, 16: 1626-1640. <a href="https://scialert.net/fulltextmobile/?doi=pjbs.2013.1626.1640#.XVx7EyqX-UU.link">https://scialert.net/fulltextmobile/?doi=pjbs.2013.1626.1640#.XVx7EyqX-UU.link</a>

Parkhurst C.R., Mountney G.J. (1988) Diseases and Parasites of Poultry. In: Poultry Meat and Egg Production. Springer, Dordrecht <a href="https://link.springer.com/chapter/10.1007/978-94-011-7053-6_9">https://link.springer.com/chapter/10.1007/978-94-011-7053-6_9</a></p>
<!-- /wp:paragraph -->