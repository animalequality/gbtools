<!-- wp:paragraph -->
<p>On&nbsp;October 13, a&nbsp;livestock carrier bound for Venezuela&nbsp;capsized in the port of Bacarena, Brazil. In addition to the 5000 cows who drowned, hundreds of gallons of diesel fuel were spilled into the Amazon River.

Minerva Foods meat company said the cause&nbsp;of the wreck is still&nbsp;unknown. An investigation has been initiated.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed"><iframe src="https://www.youtube.com/embed/B5GpDZNT7nQ" allowfullscreen="allowfullscreen" width="560px" height="315px" frameborder="0"></iframe></div>
<!-- /wp:html -->