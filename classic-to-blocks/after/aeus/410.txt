<!-- wp:paragraph -->
<p><strong><a href="https://www.beyondmeat.com/en-US">Beyond Meat</a></strong>, a startup that makes vegan meat analogs, is manufacturing products that purportedly taste and feel like real meat, have a better nutrition profile, and will eventually be available at a lower price point than meat.

The company has some surprising backers, including the Obvious Corporation - a company founded by Twitter co-founders Evan Williams and Biz Stone and former Twitter VP of Product Jason Goldman--and venture capitalist powerhouse Kleiner Perkins Caufield Byers.

Biz Stone has been a vegan for over a decade. Stone first learned about Beyond Meat when one of the partners at Kleiner Perkins mentioned that they were considering an investment in the company.

Obvious announced its involvement in Beyond Meat last week (they will help with marketing in addition to funding), but the meat analog company’s beginnings can be traced back to 2009 when it began collaborating with researchers at the University of Missouri who were working on a product with the taste and texture of chicken.

Beyond Meat’s first product is Veggie Chicken Strips after that will come beef crumbles.

Right now, Beyond Meat ships wholesale for less than the cost of natural meat. It’s still more expensive than factory-farmed meat, but that will change as the company scales up. Stone is optimistic that if Beyond Meat takes off, it can have a real impact on meat consumption - and, in turn, our health and the environment.</p>
<!-- /wp:paragraph -->