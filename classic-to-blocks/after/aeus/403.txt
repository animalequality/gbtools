<!-- wp:paragraph -->
<p>A new Animal Equality <strong><a href="https://thekillingoftuna.org/index.php">undercover investigation</a></strong> reveals the tunas’ massacre, which takes place each year, between late May and early June, just off the coast of the most populated area of the island of San Pietro, south-west Sardinia.

<strong>Hundreds of bluefin tunas</strong> are caught in traps while they migrate to their breeding grounds and are later slaughtered. During a careful and painstaking investigation, Animal Equality recorded and photographed both the natural behavior of tunas underwater and the plight of the bluefin tunas who are brutally killed every Spring in Italy.

The investigation has been featured in <strong>The Times</strong> this morning:
<em><strong>Katherine van Ekert, </strong>president of Sentient, the Australian Institute for animal ethics said the tuna could feel pain. “The suspension of the tuna’s body weight is expected to cause pain and stress to the animals, as too would the tearing of their tissues as a result of gravity working against the hook."</em><em>“Pain and stress can be witnessed through the struggling and thrashing movements of the tuna while suspended in the air.”</em>

Other national papers such as <a href="http://video.repubblica.it/cronaca/tonno-rosso-sangue-la-mattanza-in-sardegna/96871?video=&amp;ref=HRESS-11"><strong>La Repubblica</strong></a>, in Italy and the Spanish paper <a href="https://www.publico.es/internacional/igualdad-animal-denuncia-matanza-atunes.html"><strong>Público</strong></a> have also published this brutal and cruel tradition.

This old and cruel ritual takes place traditionally during this time of the year. Hundreds of these gigantic animals are forced to swim through a system of fixed nets which leads them into the ‘killing zone’. The tonnara - mobile nets - slowly push the tunas towards the water surface, leaving the terrified animals stacked on top of each other. Once crowded together, the animals injure themselves as they thrash violently.

The sea turns red and they begin to run out of oxygen - that’s when the killing starts. The heavy tunas are hooked and lifted from the water surface to waiting boats, where are then violently struck with harpoons - one after another - whilst struggling, choking and bleeding to death.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed"><object width="640px" height="360px"><param name="movie" value="https://www.youtube.com/v/GPlDrCOJUCg?version=3&amp;hl=en_US"><param name="allowFullScreen" value="true"><param name="allowscriptaccess" value="always"><embed allowfullscreen="allowfullscreen" allowscriptaccess="always" src="https://www.youtube.com/v/GPlDrCOJUCg?version=3&amp;hl=en_US" type="application/x-shockwave-flash" width="640px" height="360px"></object></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Animal Equality’s investigation has been reviewed by independent <strong><a href="https://thekillingoftuna.org/experts-killing-of-tuna.php">veterinary experts</a>&nbsp;</strong>who have analyzed the behavior of the fish in the footage, and have confirmed these sentient individuals experienced painful and stressful experiences during the catching and killing process.

<strong>INVESTIGATIONS FINDINGS:</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Unnaturally high densities of tuna fish at the catching stage presented a significant stressor to individual animals.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>The fish were torn from the ocean with giant sharp metal hooks and brought on board ships.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Extensive tissue damage was caused by the piercing, blunt hooks, and this is likely to have inflicted acute pain on the fish, who were still alive and conscious.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Pick-hooks dragged tuna whilst alive - an additional source of pain.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Suspension of the tunas’ body weight caused the further tearing of tissues as a result of gravity working against the hook.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>The struggling and thrashing movements of the tuna whilst suspended in the air indicated that the fish were in obvious pain and stress.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Fish were observed being repeatedly stabbed with knives in the thoracic (chest) region and major arteries, causing death via exsanguination.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Animals were slaughtered in the presence of conspecifics - likely to cause additional stress.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>
Our high-quality <a href="https://www.flickr.com/photos/animalequalityuk/albums/72157629990167744"><strong>photo </strong></a><strong><a href="https://www.flickr.com/photos/animalequalityuk/sets/72157629906326910/">gallery</a></strong>&nbsp;reveals the suffering that tuna endure during this tradition.</p>
<!-- /wp:paragraph -->