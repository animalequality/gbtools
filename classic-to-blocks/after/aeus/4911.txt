<!-- wp:paragraph -->
<p>The images obtained by the organisation, <a href="http://www.ukaztetovlade.cz/">Zvirat Svoboda</a> (Freedom for Animals), reveals once again the cruelty of the fur industry. Critically ill animals are easily confused with corpses on the farms. Animals have been observed with gaping wounds and apparent infections. They lay anguished and depressed inside the cages.

Activists from the organisation claim that the images were taken in six, randomly selected farms within the Czech Republic. The activists were shocked by the conditions in which the animals were being held, and are particularly concerned about the grave health of&nbsp;&nbsp; the health of animals. "You have to see the animals, who have not been provided with any veterinary care, even basic veterinary care, which undoubtedly is torturous for them", said Lucie Moravcová Zvirat, Svoboda's Spokesperson.

"There are animals living in cages filled with feces, and with a lack of water ... " says Lucie Moravcová.
The broadcast, Nova, visited one of the documented farms in Syrovice, near Brno, but the owner of the establishment refused to speak to the reporter and show the mink cages. "I have no interest in talking with you. I do not want you to take any pictures here" was the reaction of John Drobílka, the farm owner.&nbsp;The evidence found on these farms have been made available to the State Veterinary Administration – which is responsible for auditing the farms.

"We take suspicions of animal cruelty very seriously," said the State Veterinary Administration's Spokesperson, Josef Duben. Duben has now stated that the farms will be investigated by the Administration, and, subject to the outcome of these investigations, the farms could be fined or even closed.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed"><strong><iframe src="https://www.youtube.com/embed/dsJksyhrOAM" width="853px" height="480px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></strong></div>
<!-- /wp:html -->