<!-- wp:paragraph -->
<p>All at Animal Equality are troubled and saddened by allegations of sexual harassment and assault in the animal protection movement — it is unconscionable those working tirelessly for a world in which animals are respected and protected should themselves be victimized or silenced.

Animal Equality is committed to maintaining a work environment that promotes equality and is free of harassment, discrimination, and retaliation. In addition to operating a zero-tolerance policy, staff regularly undertake mandatory anti-harassment and discrimination training and our directors, managers, and supervisors are required to complete a more comprehensive annual training on preventing, identifying, and resolving instances of harassment and discrimination. This <a href="https://drive.google.com/file/d/1fnYYsVhUrRABXi7DVnr0jaFH4wgO2In1/view">policy</a> applies equally to staff at all levels, whether a junior officer or president of the organization.

We also realize that training alone is not enough: enforcement of policies is equally important. To this end, Animal Equality fosters a culture of safety and open communication and provides official avenues for confidential reporting.

In addition to being a female-led international farmed animal protection organization, nine of our 14 directors are female, empowered to lead and make decisions for the organization. Animal Equality is also committed to a diverse working environment, illustrated by the fact that its directors are from nine different countries around the world.

Creating a safer, more just and compassionate world is impossible while tolerating abuse and inequality of any kind. Without a safe space from which to fight the oppression of farmed animals, innocent people suffer and it takes exponentially longer to achieve our shared mission.

Our thoughts are with those who have been silenced or bravely spoken out in all sectors of society to ensure a safer world for all. We are grateful for and stand with you.</p>
<!-- /wp:paragraph -->