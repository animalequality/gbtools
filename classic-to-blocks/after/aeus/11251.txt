<!-- wp:html -->
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/7x4NbVdsY6c" allowfullscreen="allowfullscreen" width="100%" height="100%" frameborder="0"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
A recent investigation by Animal Equality reveals cruel and illegal practices at egg farms across India.

<strong>WHAT WE FOUND</strong>: In February of 2019, our investigators visited several egg facilities in the Indian states of Maharashtra, Gujarat, Andhra Pradesh and Telangana, all of which are known for their egg production. Here is what we found:
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Four to eight hens crammed in a cage no bigger than two letter size (8.5” x 11”) sheets of paper, unable to comfortably move.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Hens with sore, cracked and deformed feet, injured from the wire floor of cages.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Overcrowded cages stacked on top of each other, causing urine and feces to fall onto birds in the lower cages.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Hens missing their feathers and suffering from abrasions and skin irritations.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Sick hens left to die slow agonising deaths with little to no veterinary care.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Litter collected in huge piles underneath the stacked cages, only disposed of once every few weeks, creating high concentrations of ammonia.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>IGNORING PUBLIC OPINION</strong>: More and more consumers are becoming aware of the cruelties inflicted on hens and are looking for cage-free eggs or alternative sources of protein. In a <a href="https://drive.google.com/file/d/1ecR7Is1J3dafi2LTuso_6dV-B6HJT4my/view?usp=sharing">survey</a> in India conducted by IPSOS, 78% of respondents stated that the government should make stronger laws so that animals used for food are not abused.

<strong>WHAT WE’RE SAYING:</strong> “The use of any sized cage, in essence, violates what are known as the <a href="https://www.animalhumanesociety.org/health/five-freedoms-animals">Five Freedoms</a>, as they don’t enable the animals to express natural behaviours and make the animals uncomfortable, fearful and distressed, as well as more susceptible to diseases. The Supreme Court in its order dated 7th May 2014 embedded ‘Five Freedoms’ in the Prevention of Cruelty to Animals Act, 1960 thereby making it mandatory,” explains Amruta Ubale, Executive Director of Animal Equality in India.

<strong>NOT JUST BAD FOR HENS:</strong> The conditions we found are horrible for the birds on these egg farms, but they’re also just as bad for the workers. “We have seen workers and their children working for long hours at the farm and sometimes they live at the farm. Due to the harmful environment at egg farms, workers are known to contract respiratory diseases like asthma and chronic bronchitis,” says Ubale.

<strong>BACKGROUND</strong>: This is Animal Equality India’s second investigation into egg farms. The first, in 2017, also exposed cruel and illegal practices and the findings were presented to the Indian government along with a list of recommendations to reduce the suffering of hens. The recommendations, such as the phasing out of cages, are still in discussion.

<strong>WHAT COMES NEXT</strong>: Even though <a href="https://www.washingtonpost.com/news/animalia/wp/2016/07/25/the-movement-to-free-hens-from-cages-may-be-going-global/?noredirect=on&amp;utm_term=.6ca669e3fbd0">countries across the world</a> are banning cages, about 80% of Indian eggs are still produced in commercial egg farms that use them. It only makes sense for India to do so, too.

<strong><a href="https://animalequality.org/donate/?utm_source=Website&amp;%3Butm_medium=CTA&amp;%3Butm_campaign=India_Investigation">DONATE NOW TO HELP US END CRUELTY TO HENS IN INDIA</a></strong></p>
<!-- /wp:paragraph -->