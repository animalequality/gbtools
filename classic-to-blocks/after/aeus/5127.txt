<!-- wp:paragraph -->
<p>The 2014 report was prepared by Michael Renner for the World Watch Institute, in which generally unknown data for consumers are exposed. Global production and consumption of meat has quadrupled since 1960. Looking further back in time, production has increased 25-fold since 1800.

The production and consumption of meat are identified as a cause of global environmental problems. More and more scientists are suggesting reducing meat consumption and replacing it with plant-based products. This simple solution would stop&nbsp;a large amount of forest deforestation and&nbsp;the waste of fresh water, among others.

Here are some data showing how the meat devours precious resources on the planet:
</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><!-- wp:list-item -->
<li>China is the country that produces more meat, with 28% of the world total. China is still a country that has few environmental regulations; this adds a higher risk to the highly polluting meat industry.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>In industrialized countries like Spain, the average meat consumption is 75.9 kg per person per year. In industrializing countries it is about 33.7 kg per year. These numbers represent a personal footprint waste of resources, as discussed below.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>In 2011, 13,600 tons of antibiotics were sold to supply for the animals in farms (nearly four times the amount provided to sick people) in the United States. In Europe, 8,500 tons were fed to animals; meanwhile, 100,000 tons were supplied to animals in China. The production of this huge amount of antibiotics consumes valuable resources worldwide.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>70% of the planet's agricultural land is intended for pasture that will be used to feed the animals we eat. These grasses are obtained mainly by cutting down precious forests.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>10% of the planet's agricultural land is used to produce grain to feed the animals we eat. However, that amount could be fed directly to the millions of hungry people around the world.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>20% of all freshwater used worldwide is used for growing cereals and grains with which farmers feed the animals. All water intended for this purpose may supply water to large populations in places ravaged by drought.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>15,415 liters of water are needed to produce one kg of beef. It takes nearly 9,000 liters of water to produce a kg of pork, and 4,325 to produce one kg of chicken. Energy waste in meat production is horrifying. More and more environmental research organizations are recommending supplying the world population with much more efficient and sustainable plant-based products.</li>
<!-- /wp:list-item --></ol>
<!-- /wp:list -->