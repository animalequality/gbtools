<!-- wp:paragraph -->
<p>The Federal Attorney for Environmental Protection (PROFEPA) announced yesterday,&nbsp;February 23, that it would confiscate a total of 101 animals from the Wildlife Conservation, Management, and Sustainable Utilization Units (UMA) named “Club de los Animalitos” for animal mistreatment. The zoo is located in Tehuacan, very close to&nbsp;Mexico City. Sergio Gomez, a member of Congress for the National Action Party, owns the zoo.

<strong>The authorities visited the zoo to check the conditions, the origin of the animals, and the&nbsp;enforcement of animal welfare&nbsp;laws only a few weeks after Animal&nbsp;Equality's report on the zoo reached Mexican national media.&nbsp;</strong>

On January 29th, Animal Equality Mexico documented the situation of over 400 animals living at the zoo "El Club de los Animalitos."&nbsp;The organization documented deplorable conditions of shocking mistreatment and neglect; animals were housed in exhibits that were extremely small and, in some cases, severely restricting. Such small spaces prevented animals from engaging in natural movements and behaviors, which are essential for their well-being, causing physical and psychological problems. In some cases, the organization&nbsp;documented how animals lived&nbsp;surrounded by their excrement.

The Natural Resources Assistant Attorney General of Profepa will seek locations for the animals that are&nbsp;appropriate for their recovery and&nbsp;protection

The news has once again appeared in several national Mexican media and<a href="https://www.bbc.com/news/world-latin-america-31603669"> international media such as the BBC.</a></p>
<!-- /wp:paragraph -->