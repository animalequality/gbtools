<!-- wp:html -->
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/-utL8CU3g7w" width="100%" height="100%" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>

Animal Equality UK has released a new investigation with footage of horrifying conditions and extreme animal suffering on three chicken farms in Lincolnshire in the UK.

<strong>THE DETAILS:</strong> Our investigators visited Red Tractor-certified farms Saltbox, Ladywath and Mount multiple times between February and April 2019 and <a href="https://www.flickr.com/photos/animalequalityuk/sets/72157709245835518/">found shocking scenes</a>, including:
<ul>
 	<li>Gigantic double-decker chicken sheds, with over 30,000 birds crowded on each floor</li>
 	<li>Many chickens suffering from severe leg injuries, some unable to stand</li>
 	<li>Carcasses left to rot for days amongst the flocks of living birds</li>
 	<li>Chicks struggling to breathe at just two days old, with many already dead</li>
</ul>
<strong>THE BACKGROUND:</strong> All three farms raise chickens for <a href="https://moypark.com/">Moy Park</a>, the UK’s second biggest chicken company which supplies major retailers, including <a href="https://www.tesco.com/">Tesco</a>, <a href="https://www.sainsburys.co.uk/">Sainsbury’s</a>, <a href="https://www.coop.co.uk/">Co-op</a> and high-end online supermarket <a href="https://www.ocado.com/webshop/startWebshop.do">Ocado</a>. Ladywath Farm is comprised of three double-decker sheds - one of the first of this type in Britain - where birds are stacked on two floors within the same building. Each multi-storey shed houses 63,000 birds and the farm has recently applied for permission to build a fourth double-decker shed.

<strong>WHAT WE’RE SAYING:</strong> “As our appetite for chicken meat has grown, so has the size of Britain’s chicken farms. Images of distressed birds in giant double-decker sheds will be a shock to many consumers who buy British, Red Tractor-certified meat thinking they can trust its animal welfare standards. Yet the truth is, the unnatural conditions chickens are forced to endure in these vast sheds are utterly dismal. One meal for us equals a lifetime of misery for them,” commented Dr. Toni Vernelli, Executive Director of Animal Equality UK, after viewing the footage.

<strong>WHAT COMES NEXT:</strong> While labels and certificates can’t protect animals in the meat industry, consumers can. It’s easy to make the compassionate choice of leaving chickens off your plate and opting for <a href="https://loveveg.com/compassionate-options/">delicious meat-free alternatives</a>, which are now widely available in supermarkets and restaurants across the world. Our investigations are crucial to exposing the cruelty on farms that use animals for food. Help us continue to make a difference by supporting our important work.

<strong><a href="https://animalequality.org/donate/?utm_source=Investigation&amp;utm_medium=News&amp;utm_campaign=UK_Chicken_Farms&amp;utm_content=062619">DONATE TODAY ENSURE MORE ANIMALS ARE SPARED FROM SUFFERING</a></strong>

</div>
<!-- /wp:html -->