<!-- wp:paragraph -->
<p>Yesterday (September 20), the last 170 beagles who were still inside the facilities of Green Hill were finally freed, meaning the breeding center is completely empty!

At a ceremony celebrating the adoptions, Senate Speaker Renato Schifani said that he hoped<em> "animal experimentation comes to an end soon".</em>

Green Hill, situated in the north of Italy, was breeding beagles to then be sent to laboratories for scientific experiments. Following numerous protests and wide-spread public outcry, the police seized 2,500 dogs during inspections of the complex,&nbsp;and three company managers have been probed for animal cruelty.

A mother and her two puppies were the last ones freed. They were delivered to their new home on Thursday after a month of rehabilitation in a canine center. The puppies had health issues, and the mothers had never seen sunlight and could not drink normally since they were hydrated through a forced-drip system when in captivity. But today they have a new home a will never have to go back to Green Hill.</p>
<!-- /wp:paragraph -->