<!-- wp:paragraph -->
<p>Animal Equality infiltrated the Italian pig industry exposing&nbsp;shocking footage&nbsp;that shows&nbsp;the misery these innocent animals endure. Here are 7 photos taken in Italy but that summarize what happens in all pig farms around the world.

<strong>1. Baby pigs tails are cut while fully conscious</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4226} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/17518620563_f416346d4f_z.jpg" alt="" class="wp-image-4226"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>2. 10% of baby piglets die in&nbsp;farms</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4229} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/17952960719_d1578cb355_z.jpg" alt="" class="wp-image-4229"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>3. Extremely painful infections and diseases go untreated</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4227} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/17951278058_3699811c0e_z.jpg" alt="" class="wp-image-4227"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>4. Rectal prolapses,&nbsp;a condition in which the rectum&nbsp;exits the anus, are common</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4225} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/17516632564_9d6e29c318_z-1.jpg" alt="" class="wp-image-4225"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>5. Mother pigs are kept in gestation crates where&nbsp;they can't even turn around</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4224} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/17516614194_2d87efd164_z.jpg" alt="" class="wp-image-4224"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>6. The dead lie amongst the living</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4231} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/18112725486_48f49e729b_z.jpg" alt="" class="wp-image-4231"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>7. This is where it all ends</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4230} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/18112681086_84d876e9af_z.jpg" alt="" class="wp-image-4230"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Choose compassion. Please <a href="https://essentialvegan.uk/">try going meat-free today.&nbsp;</a></p>
<!-- /wp:paragraph -->