<!-- wp:paragraph -->
<p>Animal Equality’s latest investigation brings to light the cruel practices that are common in India's chicken meat industry.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/NSwopBXsSfI" allowfullscreen="allowfullscreen" width="700" height="394" frameborder="0"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Animal Equality investigated several chicken farms and meat markets across India. The investigation shows the cruel cycle that birds are forced to suffer through, from the day they’re born until the day they die. Our investigators documented:
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Day old chicks who were roughly handled and stuffed into boxes.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Journeys that last for hours and sometimes even days. Many chicks die before they even arrive at the farm.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Food that is laced with antibiotics to stimulate unnatural growth rates.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Chickens who were crippled under the weight of their own bodies.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Many would fall ill or die of starvation, others died of heart attacks and respiratory infections.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Once they've reached ideal weight for slaughter, they're forced into crates and taken on long journeys without food or water.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>At markets, chickens are forced to live in tiny cages sometimes for days at a time.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>During slaughter, their throats are slit and they are thrown into drain bins where they languish in pain for several minutes before they die.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Carcasses are de-feathered and then hot torched or boiled in water in the filthiest conditions.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>
Animal Equality has also conducted investigations into India's egg industry, documenting the abuse and terrible conditions endured by hens. The findings of both these investigations, along with a list of recommendations, have been presented to the Indian government. Most of our recommendations have been included in the government’s proposed rules for the welfare of chickens. However, some of our recommendations like banning of cages for hens and introducing in ovo sexing to avoid the deaths of male chicks, are not introduced.

<strong>But you can help.</strong>

Please <a href="https://animalequality.org/indianchickenfarms/">sign our petition</a> asking the Indian government to act today to protect chickens and hens from abuse and cruelty.</p>
<!-- /wp:paragraph -->