<!-- wp:paragraph -->
<p><strong>THE DETAILS:</strong><a href="https://olis.leg.state.or.us/liz/2019R1/Downloads/MeasureDocument/SB1019">The new law</a> mandates that commercial farms with 3,000 or more chickens provide chickens room to move around and the ability to stretch their wings. By 2024, all eggs produced or purchased in the state must be produced from cage-free hens.

<strong>NATURAL BEHAVIORS:</strong> The farms will also be required to provide hens with enrichments that allow them to engage in natural behavior, which includes, at a minimum, “scratch areas, perches, nest boxes and dust bathing areas.”

<strong>90% OF HENS:</strong> Did you know that in the U.S., more than 90% of hens are packed into wire cages? On average, each hen has less living space than a standard piece of printer paper. Inside these cages, they are forced to stand or crouch on the cages’ hard wires, which cut their feet. Because of the living conditions, hens often die in their cages. They are sometimes left to rot in the same space alongside living birds. Cage-free laws like this new one in Oregon provide a significant improvement for the lives of chickens.

<strong>PROGRESS:</strong> Oregon’s forward-thinking law will give 4-million chickens more space to live and crucial avenues for enrichment. The state joins California, Washington and Massachusetts, who have all passed similar laws.

<strong>WHAT COMES NEXT:</strong> While this was an amazing victory for chickens in Oregon, hundreds of millions more all across the country are still suffering in horrible conditions. Please share this post and let people know that chickens are suffering every day for egg production. If you haven’t already, please also consider ditching eggs - the chickens will thank you.

<strong><a href="https://loveveg.com/compassionate-options/eggs/?utm_source=Blog&amp;utm_medium=CTA&amp;utm_campaign=Oregon&amp;utm_content=083019">CHECK OUT THESE EGG-FREE OPTIONS!</a></strong></p>
<!-- /wp:paragraph -->