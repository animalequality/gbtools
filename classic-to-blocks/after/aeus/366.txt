<!-- wp:paragraph -->
<p>In a <a href="http://www.vrg.org/blog/2011/12/05/how-many-adults-are-vegan-in-the-u-s/">recent national poll</a> commissioned by the Vegetarian Resource Group, the results show that about one-third of Americans eat vegetarian food a significant amount of the time. Not only that, but <strong>about 7.5 million people in the United States are vegan! </strong>

That is more than double the number of vegans in the U.S.A. since 2009. Also, according to <a href="http://www.wattagnet.com/25516.html">industry reports</a>, per capita meat consumption in the U.S.A. has been steadily declining for the last 6 years.

Veganism is growing in popularity and this means fewer animals ending up on dinner plates!</p>
<!-- /wp:paragraph -->