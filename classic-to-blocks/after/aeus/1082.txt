<!-- wp:paragraph -->
<p>In 2015, Animal Equality pioneered the use of virtual reality as a tool for educational outreach. The <a href="https://ianimal360.com/">iAnimal</a> project, which now includes three films inside <a href="https://www.youtube.com/watch?v=OKwVDZ4YLBs">pig</a>, <a href="https://www.youtube.com/watch?v=tX7FHeLaLqg&amp;t=37s">chicken</a> and <a href="https://www.youtube.com/watch?v=YZADGGwVKj8&amp;t=1s">dairy</a> farms, uses cutting-edge virtual reality technology to give viewers an immersive look at the life cycle of factory farmed animals from birth to death. Animal Equality has been taking iAnimal to prestigious college and university campuses throughout the world, as well as major festivals and conferences. The <a href="https://www.youtube.com/watch?v=kPsu3aV9VUU">responses have been overwhelming</a>.

As an effective altruist organization, Animal Equality wants to make sure we are using our resources to be the most effective for animals. So with help from Faunalytics and additional support from Statistics Without Borders, Animal Equality <a href="https://drive.google.com/file/d/1ovZ0xvJPKLGayAX-uCyGuZtJKCFcY0TP/view?usp=sharing">conducted a research study</a> to determine two things:
</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><!-- wp:list-item -->
<li>Which of two video media (360-degree virtual reality or a 2D experience) results in greater change in self-reported pork consumption and, secondarily, attitudes toward pork and pigs?</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Do these video media result in greater change in self-reported diet and, secondarily, attitudes toward pork and pigs than a control condition?</li>
<!-- /wp:list-item --></ol>
<!-- /wp:list -->

<!-- wp:image {"id":4114} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/01/32861783394_82c1a67515_z.jpg" alt="" class="wp-image-4114"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Animal Equality hired four staff members to run our Spring 2017 US university iAnimal tours that were essential to our research study. These four staff managed the outreach booths and over 65 volunteers, as well as engaged with research study participants at distinguished university campuses including USC, UCLA, Stanford, UC Berkeley, Princeton, Harvard, and Columbia. Initial data was obtained from 3,068 participants, with follow-up responses of people participating a month later from 1,782 people that were used in the final analysis. Both the 360 and 2D groups watched the same video on factory farmed pigs and completed a survey.&nbsp;The control group completed the survey only. All groups took an additional survey one month after intervention.

Relative to the control condition (students who received no intervention), both the 360 and 2D videos improved participants’ pork-related attitudes immediately after watching and, most importantly, one month later - meaning they felt it was important to reduce their consumption of pork and/or that eating pork directly contributes to pig suffering immediately after, and a month after the intervention.

Most crucially, watching a video (in either medium) reduced participants’ pork consumption relative to the control condition. For example, the probability that a participant in the control condition will have eaten pork one to three times a month or less was 59%, whereas the probability that a participant who watched a video will have eaten pork one to three times a month or less was 65%. That said, while the 360 condition showed a very slight advantage in changing people’s pork consumption, the differences between 360 and 2D were not statistically significant.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4104} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/01/32861784014_4d3997a8bc_z.jpg" alt="" class="wp-image-4104"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

It’s important to take the limitations of this study into account. This includes the possibility of bias resulting from some people being more likely than others to complete the follow-up survey. Specifically, people with lower baseline pork consumption were more likely to complete the follow-up survey, as were people in either video condition (vs. control). Another possibility is secondary effects of the intervention, such as the chance that participants compensated with other meats or animal products. Other limitations include an overrepresentation of vegans and vegetarians in the sample and the possibility of inaccurate self-reporting.

It’s also important to take into account&nbsp;the novelty of the 360 video, which draws in more people than the 2D video, giving it more potential effectiveness. In our outreach programs we have identified,&nbsp;that more people are intrigued by the virtual reality headsets and are more likely to watch a video about factory farming versus a conventional 2D video on a tablet. There is also a higher chance that people who are immersed in a 360 video will see more of the video than those who turn away or get distracted during a 2D video, causing them to miss significant content that could potentially lead to a change in their attitudes and behavior.

Animal Equality recognizes that 360 video is a more expensive tool than 2D video. For that reason, we have used it selectively, to reach some of the most influential people possible. We have targeted reputable university campuses such as Oxford, Cambridge, Yale, Harvard and others, internationally acclaimed&nbsp;film and musical festivals such as Sundance Film Festival, SXSW and others,&nbsp;and political conferences. These people and venues might not be persuaded to watch a 2D video. Thanks to the novelty of the 360 medium, however, Animal Equality has been successful in getting reporters, politicians, celebrities, and many more influencers to watch and share iAnimal videos.

Animal Equality has also used the novelty of the 360-degree video format to reach more media. In 2017 alone, Animal Equality’s iAnimal videos had a reach of more <strong>than 312 million people through the media,</strong> including an article in <a href="https://www.nytimes.com/2017/07/06/dining/animal-welfare-virtual-reality-video-meat-industry.html">The New York Times.</a> This kind of media impact would not have been possible with traditional 2D videos alone.

Overall, the results of this study support the usefulness of Animal Equality videos—in either medium—as an outreach tool. The results demonstrate their ability to improve attitudes and reduce pork consumption behavior, thereby potentially reducing the number of pigs raised and killed for food. While the current study and video focused on pigs, it seems plausible that a similar approach could also be effective to reduce the consumption of other animal species.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4089} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/01/iAnimal-UG-2.jpg" alt="" class="wp-image-4089"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>Conclusions for Animal&nbsp;Equality:</strong>

Animal Equality considers that this initial study may indicate that 2D videos and 360 videos are as effective. However, taking into account the possibility of impact and reaching influencers with 360 videos, the organization plans to continue&nbsp;bringing iAnimal to important universities, music and film festivals and political conferences. As a way of reducing the costs of the program.&nbsp;Animal Equality is launching in 2018 the <em>iAnimal Student Outreach Program</em>&nbsp;where student groups at colleges and universities across the U.S. will be provided with iAnimal virtual reality headsets and the training needed to do outreach on their campuses. This will enable Animal Equality to expand their reach at a lower cost. This along with the over 100 organizations and individuals making use of iAnimal around the globe will ensure we are able to educate thousands of people in the hopes that they will begin to make more compassionate food choices.</p>
<!-- /wp:paragraph -->