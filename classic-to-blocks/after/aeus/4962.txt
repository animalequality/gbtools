<!-- wp:paragraph -->
<p>Today, <strong>Animal&nbsp;Equality</strong>&nbsp;presents&nbsp;unedited images from the&nbsp;investigation that we carried out in the dog and cat markets in Wuhan, Taiping, Cong Hua and Nanhai, as well as images from two dog breeding and fattening farms, located in Jiaxiang and Jining, and a slaughterhouse in Zhanjiang.
This new investigation forms part of our <a href="https://Voicelessfriends.org"><strong>Voiceless Friends campaign</strong></a>, which started in <strong>April this year 2013,</strong> and with which we aim to put an end to the dog and cat meat trade in China.

Despite the fact that the security guards followed us and tried to stop us from seeing the unloading of live animals at the markets, we <strong>witnessed how thousands of animals arrived in huge trucks, crammed into tiny cages.</strong>

The dogs, cats, and rabbits are unloaded during the night and the cages they are locked in are thrown from the top of the trucks. Many of these animals have<strong> their bones broken when their cages</strong> fall against the floor. Some of the <strong>pregnant cats give birth during their journey to the markets</strong> -which can last several days. They <strong>do not eat or drink and their kittens usually die crushed</strong> as they are being unloaded from the trucks.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed"><iframe src="https://www.youtube.com/embed/6XOfDKH3OAM" width="600px" height="450px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong><span style="color: #3399ff;">Visit to Shandong puppy farm:</span></strong>
In order to get an in-depth knowledge of how the dog meat industry works in China, we visited the Shandong puppy farm (in Jining) where dogs are bred for their meat and fur. We were able to visit two warehouses at the Shandong farm: one destined for adult dogs and another full of puppies.

The first warehouse was equipped with various kennels, inside of which were about 10 large adult dogs (Alaskan Malamutes, Greyhounds, and other breeds) destined for breeding.

In the breeding warehouse, we were able to count about 150 puppies of around a couple of weeks of age, crowded into small wire cages on the floor. These cages were approximately 19 inches wide and 15 inches tall, and we counted up to 13 puppies per cage. The dogs could barely move inside their cages.

According to the information we received at the farm, the puppies are sold at three weeks of age to another company where they are fattened until they reach the appropriate weight. Once they are fattened, the puppies have their throats slit at the farm or are sent to the slaughterhouse. Each puppy is sold for 200 Yuans (around 20 pounds).

<strong><span style="color: #3399ff;">Jiaxiang dog farm (嘉祥县):</span></strong>

When we arrived at this fattening farm we were able to observe a cage which had a dozen of puppies, there were also kennels, where 60 large dogs were kept inside. The dogs are fattened in Jiaxiang until they reach an average of 198 pounds and are then sold for their meat.

<strong style="color: #3399ff;">Zhanjiang dog slaughterhouse:</strong>

During the course of our investigation, we visited a slaughterhouse located in the Zhanjiang neighborhood, in a residential area. Around 15 dogs are killed here every day. The dogs are locked up in a room with no ventilation and in absolute darkness, without water or food, until the moment of their death. They are beaten over the head and stabbed, after which their fur is removed to send the clean meat to local restaurants.

<a href="https://www.flickr.com/photos/animalequalityuk/sets/72157635439511911/"><strong style="color: #3399ff;">Wuhan Seafood Market:</strong></a>

The eating of dog meat is not typical in Wuhan. However, we discovered a market with an establishment full of cages, inside of which we found a large number of live animals, all kept for their meat: 15 dogs distributed amongst various cages, as well as deer, alligators, rabbits, raccoons, hedgehogs, a porcupine, quail, donkeys, and geese. That morning at least one dog had been killed; we know this because its skin was laying over one of the cages. In another cage, we were even able to see a dog’s skull.

<strong style="color: #3399ff;">Cat meat trade at FuXing market (Taiping):</strong>

At the FuXing market, we were able to witness how cats are also victims of the meat trade. At this market, cats are captured directly off the streets and killed at restaurants through a blow to the head. Afterward they are put inside enormous pots of boiling water and served as part of the famous “Dragon, Tiger, and Phoenix” dish, which according to tradition “helps fortify the body”.

At this market, there were also dogs, raccoons, and other wild animals for sale for their meat as well. It is estimated that around 4 million cats are killed each year in China for their meat.</p>
<!-- /wp:paragraph -->