<!-- wp:paragraph -->
<p>The owner of the mink farm has been reported to the police for trying to run down an activist during the protest.

On the video, you can see how the owner swings the car, in an attempt to hit one of the protesters. Luckily enough none of them were injured.

<em>"He opened the gate, jumped into the car and began to accelerate. He drove down and aimed straight at me to try to run me down"</em>&nbsp;Mr. Andersson, the activist who had been attacked, explained.

The owner has been called in for questioning by the police and left no comments to the journalists.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="media_embed"><iframe src="https://player.vimeo.com/video/50028319?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" allowfullscreen="allowfullscreen" width="800px" height="450px" frameborder="0"></iframe></div>
<!-- /wp:html -->