<!-- wp:paragraph -->
<p>Represented by Animal Equality and Richman Law Group, Food &amp; Water Watch and Organic Consumers Association (OCA) have sued Tyson Foods, Inc. in D.C. Superior Court under the D.C. Consumer Protection Procedures Act, for deceptive marketing and advertising of chicken products sold under the Tyson brand and other brand names. Tyson’s allegedly false claims of “humane treatment” and “environmental stewardship” mislead consumers, the nonprofits say.

<strong>THE DETAILS:</strong> The nonprofits allege that Tyson makes false marketing and advertising representations intended to make consumers think that Tyson chicken products are produced in an environmentally responsible way and that Tyson birds are healthy and treated humanely, when in fact, Tyson’s practices include:
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>routine use of anti-parasitic drugs;</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>injection of chicken eggs with formaldehyde;</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>production of chickens contaminated with antibiotic-resistant superbugs;</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>washing of the Tyson products with hazardous chemical disinfectants; and</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>crowding of birds by the tens of thousands into massive industrial warehouses with no access to the outdoors.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>THE BACKGROUND:</strong> According to <a href="https://www.foodandwaterwatch.org/">Food &amp; Water Watch</a>, Tyson’s inhumane chicken operations take a tremendous toll on the environment. In 2014, Tyson dumped more than 20 million pounds of toxic pollutants into US waterways. From 2013 to 2015, the company was guilty of more than 300 wastewater Clean Water Act permit exceedances. The nonprofit plaintiffs also allege that nearly a dozen undercover investigations in as many years have uncovered systematic cruelty inflicted intentionally upon chickens raised for Tyson products.

<strong>WHAT PEOPLE ARE SAYING:</strong> Ronnie Cummins, co-founder and international director of the <a href="https://www.organicconsumers.org/">Organic Consumers Association</a>, said: “Consumer surveys confirm that shoppers are paying greater attention to not only what’s in their food, but to how their food is produced. They often conduct online research, relying heavily on marketing claims posted on company websites. Tyson should live up to the claims it makes, or stop making them.”

<strong>HOW YOU CAN HELP:</strong> Our legal team works tirelessly to ensure farmed animals are being spared from unnecessary cruelty, using the law bring companies to compliance and proposing legislation to create a more humane world for all animals. To continue to make a difference, we need your support.

<strong><a href="https://animalequality.org/donate/?utm_source=Website_News&amp;%3Butm_medium=CTA&amp;%3Butm_campaign=Tyson">PLEASE CONSIDER A DONATION TODAY TO HELP FARMED ANIMALS</a></strong></p>
<!-- /wp:paragraph -->