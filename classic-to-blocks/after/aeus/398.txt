<!-- wp:paragraph -->
<p>A crocodile farmer in South Africa is facing possible charges for animal abuse.

There were around 15,000 crocodiles on Coen Labuschagne's farm Metcroc Boerdery. Three months prior to slaughter, to prevent damage to their skins, crocodiles were held in isolation in so-called ‘finishing pens’ on farms. The crocodiles at this farm had grown over two meters in length and were forced into pens measuring less than two meters long and a meter wide.

Their heads were bent and they could not lie or rest straight. The animals had to ensure this for three months before being killed.</p>
<!-- /wp:paragraph -->