<!-- wp:paragraph -->
<p>This beautiful elephant was killed in a train accident on 5th March 2013 by a speeding Somporkkranti Express in a forest at the Buxa Tiger Reserve. The impact of the collision was such, that the elephant died instantly. The brutality does not stop here; some train passengers tried to take away the tusks of the dead elephant.

Animal Equality had suggested installation of radar sensors to detect animals on tracks. The suggestions were accepted by both, the Ministry of Railways and Environment and Forests and the project of developing such device has been assigned to IIT, New Delhi.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4289} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/03/549459_617781888238635_1573136961_n1.jpg" alt="" class="wp-image-4289"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

The sensor device will be ready for testing in 2016 but we dread to imagine the number of animals which will be killed till then, as 13 elephants have been killed in last four months alone.

Please, send a polite email to Smt. Jayanthi Natarajan, Minister of Environment and Forests mosefgoi@nic.in and Shri Pawan Kumar Bansal, Minister of Railways pkbhansal@sansad.nic.in, urging them to address this matter immediately and implement the sensor device, ‘the Wild Animal Protection System’ at the earliest.</p>
<!-- /wp:paragraph -->