<!-- wp:paragraph -->
<p>French animal protection organization "L214 Ethique et Animaux"&nbsp;presented yesterday, December 21,&nbsp;an investigation in the heart of the French&nbsp;foie gras industry.

This new investigation exposes two common industry practices; the first one&nbsp;being that&nbsp;all of the female ducks and geese&nbsp;are thrown into shredders,&nbsp;many of them&nbsp;while still alive.&nbsp;The reason for this is that the liver of the female ducks is not big enough and it wouldn't gain&nbsp;weight during the horrific force-feeding.&nbsp;The second&nbsp;method revealed by&nbsp;the investigation is the artificial&nbsp;insemination&nbsp;of female ducks. This practice causes infections on some of the animals. The&nbsp;ducks that get infections&nbsp;are killed by twisting and breaking their necks.

<span style="line-height: 1.6;">"</span>L214 Ethique<span style="line-height: 1.6;"> et </span>Animaux"<span style="line-height: 1.6;">&nbsp;had already carried out</span><span style="line-height: 1.6;">&nbsp;an </span>investigation<span style="line-height: 1.6;"> into this cruel industry in 2012 on the practice of force-feeding. A practice in which workers shove pipes down the throats of male ducks several times a day, pumping up to 2.2 pounds of grain into their stomachs. This is done in order to enlarge the livers of the ducks and geese that will then be sold as foie gras. <a href="https://animalequality.org/blog/2022/02/08/what-is-foie-gras/">Foie gras</a> is french for "fatty liver."</span><span style="line-height: 1.6;">According to Brigitte&nbsp;Gothière, spokesperson&nbsp;for&nbsp;L214.</span> Thanks to awareness campaigns and investigations approximately&nbsp;51% of people in France&nbsp;are in favor of banning the inhumane practice of force-feeding.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/ql3CtnriSTw" allowfullscreen="allowfullscreen" width="560px" height="315px" frameborder="0"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Animal Equality has also carried out several investigations into the foie gras industry in France and Spain exposing&nbsp;the cruelty behind some of the most important foie gras producers in the world.

The production of foie gras has been banned in Argentina, Austria, Czech Republic, Denmark, England, Finland, Germany, Holland, Italy, Ireland, Israel, Luxembourg, Norway, Poland, Sweden, Switzerland and Turkey. The only European Union countries where it is permitted are Belgium, Bulgaria, France, Hungary and Spain.

<strong>Animal&nbsp;Equality is committed to ending force-feeding everywhere. Take action today by <a href="https://animalequality.org/action/foie-gras">signing our petition to ban foie gras in the United States</a>.</strong></p>
<!-- /wp:paragraph -->