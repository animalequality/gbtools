<!-- wp:paragraph -->
<p><strong>March 17, 2017</strong>—Animal Equality released images documenting the <strong>standard practices</strong> used in milk production in Mexico, revealing that it inflicts the worst type of abuse on cows and calves raised in the dairy industry. This is the <strong>first time</strong> an animal advocacy organization has ever gained access to Mexican dairy farms and revealed what happens inside this secretive industry.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/iGfu9ISomXc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

What the investigation reveals is an endless cycle of abuse evident in images of:
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Calves being born and cruelly taken away to avoid having them drink their mother’s milk.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Forced artificial insemination of cows so they continue to produce milk.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Newborn calves brutally <strong>locked in cold and filthy cages</strong>.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Dehorning of adult cows, a practice that has been banned in other countries.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Tail docking, also <strong>banned in other countries</strong>.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Branding of cows with hot iron and without anesthesia, which causes serious and painful injuries to the animals.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Cows being <strong>forced to swallow magnets</strong> to pick up the metals in their stomachs from the consumption of contaminated food.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Injured, infected, and bleeding udders caused by the mechanical milking.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Cows suffering from injuries and infections on different parts of their bodies, which receive <strong>no veterinary care</strong>.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>The killing of cows with low milk production levels in <a href="https://animalequality.org/action/mexican-slaughterhouses">government-owned slaughterhouses</a>.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:image {"id":4061} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/collage-dairy.jpg" alt="" class="wp-image-4061"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Dulce Ramírez, Executive Director of Animal Equality Mexico, said, “It’s time to strengthen the law in Mexico to prevent abuses against farmed animals. <strong>Animal cruelty must be treated as a crime</strong>.”

She added, “Animals used in factory farming are treated like machines - beaten, electrocuted, brutally stabbed, locked inside filthy cages - while the industry completely ignores their suffering.”

These&nbsp;images prove&nbsp;that Mexico seriously lags behind other countries in terms of farmed animal protection laws. Until there is a law that prohibits the use of cruel practices on factory farms, animal abuse will continue to happen.

With this new investigation, Animal Equality adds momentum to our&nbsp;petition that was delivered to the <strong>Federal Government and to the Senate of Mexico</strong> in December to modify the federal legislation and make the abuse of farmed animals a crime.

Please help us put an end to this cruelty by <a href="https://loveveg.com">replacing dairy products in your diet</a>.</p>
<!-- /wp:paragraph -->