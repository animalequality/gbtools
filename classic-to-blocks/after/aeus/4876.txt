<!-- wp:paragraph -->
<p>The airline, which was just awarded ‘World’s Best Regional Airline’ by a British based airline service, was carrying two live dolphins on board a flight to Jakarta from Bali over the weekend.

“<em>On Saturday, two dolphins used in the widely protested travel show in Kuta . . . left Denpasar on passenger Flight GA 411,</em>” Femke den Haas, spokesman for the Jakarta Animal Aid Network, said on Sunday. “<em>The dolphins traveled in small crates together with sea-lions and otters, all animals exploited in the travel show</em>.”

Den Haas said Garuda should be responsible for the suffering and exploitation of what she called “blood dolphins,” because they transported them in small crates from Japan to Bali.

“<em>These transports are extremely bad for the dolphins</em>,” den Haas said. “<em>Dolphins also suffer from stress during transport, the extreme sounds and the high chlorine content used in the small plastic pools and transport boxes in which they stay. This chlorine also blinds them</em>.”

The petition was initiated by Barbara Napoles from Florida — besides calling for a boycott of Garuda, the petition also calls on the Indonesian government to put an end to dolphin shows.

Such a petition, which collected more than 6,500 signatures, successfully stopped Hong Kong Airlines from transporting dolphins after the airline admitted they shuttled five of the animals from Japan to Vietnam.

Dolphins are protected animals in Indonesia, under the 1990 Law on the Protection of Biodiversity.</p>
<!-- /wp:paragraph -->