<!-- wp:paragraph -->
<p>Virtual reality is changing the way people view farmed animals.

Our award-winning film series, iAnimal, is on the front lines of this change. An enthusiastic team of activists has taken iAnimal on the road to colleges and universities across the world. They’ve visited more than 40 cities helping educate our future generation on the issues that farmed animals face and the positive effects that reducing or eliminating animal products from their diet would have.

Celebrities, such as Joaquin Phoenix, Mena Suvari, Evanna Lynch, Kat Von D, and Marco Antonio Regil, have lent their powerful&nbsp;voices to support this project.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4156,"align":"left"} -->
<figure class="wp-block-image alignleft"><img src="/app/uploads/2017/11/23737926_10214585107806097_56205147548991362_o.jpg" alt="" class="wp-image-4156"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Thanks in part to our collaboration with other groups, iAnimal has been viewed by more than 130,000 people at over 1,000 different events around the globe.

In China, our outreach with iAnimal continues to grow and is inspiring positive behavior change. Recently, Grace Han and her team have shared the experience with students at Tsinghua University, a premier institute of technology. iAnimal has also been featured at ChinaFit, a vegan conference in Beijing; Go Plantopia, an eco-design fair in Shanghai; and at Shanghai’s first-ever vegan and animal rights art exhibition.

iAnimal was also nominated for an award at Raindance, the UK’s leading independent film festival, and at New York’s Chelsea Film Festival where Animal Equality’s President, Sharon Nunez, spoke on a panel to discuss the effects of factory farming on climate change.

Without your support, we would not be able to reach tens of thousands of people worldwide with this unique educational outreach tool.</p>
<!-- /wp:paragraph -->