<!-- wp:paragraph -->
<p>It is of no surprise that 1 in 10 Swedes follow a meat-free diet as plant-based alternatives increase greatly throughout the country, encouraging a new wave of young vegetarians. Most Swedish vegetarians and vegans fall in the 15-24 age group.

These statistics emerge from a national poll conducted by Animal Rights Sweden (Djurens Rätt), along side Demoskop, to reveal the data behind this significant increase of vegans and vegetarians. The survey was carried out throughout February and March 2014 with a solid 1,000 participants.

Statistically, there has been an increase of 4% in the number of Swedish vegetarians over the last five years. Moreover, the figures are encouraging! 6% of the survey's participants specified they were vegetarian whilst 4% identified as vegan. Meanwhile, 37% of non-vegetarian respondents stated their appeal and interest in buying vegetarian products had grown greatly over the past years.

The Swedish population is also noticing a great increase in veggie restaurants and supermarkets which cater for a plant-based diet, ensuring they meet the demands of the ever-growing Swedish vegetarian community.

Will the world be vegan in 50 years? Nobody knows really, but what we definitely can conclude is that the statistics are significantly promising as more and more people decide to embrace a plant-based diet. Cruelty-free alternatives are increasing throughout restaurants, supermarkets, and stores worldwide!</p>
<!-- /wp:paragraph -->