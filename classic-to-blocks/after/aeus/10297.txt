<!-- wp:paragraph -->
<p>Animal Equality has released harrowing footage showing extreme suffering and cannibalism on an award-winning UK farm, Grove Smith Turkeys Ltd, which supplies turkeys to high-end UK retailers, local butcher shops and pubs.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="ae-video-container"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/pSfMY4cSLg0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>WHAT WE SAW:</strong> Filmed by our investigators in late 2018 at Hubbard’s Farm in Essex - Grove Smith Turkeys’ main site - the shocking footage shows:
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Birds who are unable to walk being pecked and eaten alive by their flockmates.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Multiple birds with severely infected wounds on their heads and eyes that were left untreated. Some had gone blind.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Crowded sheds without adequate enrichment for these inquisitive animals, causing the birds to peck each other out of boredom and frustration.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Birds who had their beak tip cut off with a hot blade, a painful mutilation performed without anesthetic.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Dozens of dead birds left to rot among the living, some for so long they were reduced to just skeletons.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>WHY IT MATTERS:</strong> Marketed as ‘prestigious’ and ‘high-welfare’, the farm supplies birds branded as ‘English Rose’ turkeys to high-end retailers such as <a href="https://www.cookfood.net/">COOK</a> and <a href="https://greatbritishmeat.com/">The Great British Meat Company</a>, as well as many local butcher shops and pubs including the Young’s chain.

Hidden cameras left running for eight days inside one shed showed that the workers were not checking the birds every day – as required by law – resulting in some birds enduring prolonged suffering and a slow death from untreated injuries.

<strong>WHAT WE’VE DONE:</strong> We have passed our footage on to the <a href="https://www.rspca.org.uk/">RSPCA</a>, Essex Trading Standards and Defra’s Animal and Plant Health Agency, who are now investigating.

<strong>WHAT YOU CAN DO:</strong> Awards and accolades don’t prevent animals from suffering in the meat industry, but you can. With delicious meat-free options widely available in stores and restaurants across the US, <a href="https://loveveg.com/compassionate-options/meat-fish/">there’s never been a better time to leave turkeys off your plate!</a><strong><a href="https://animalequality.org/donate/">DONATE NOW AND HELP OUR INVESTIGATIONS CONTINUE!</a></strong></p>
<!-- /wp:paragraph -->