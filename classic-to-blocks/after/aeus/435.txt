<!-- wp:paragraph -->
<p>The South Korean belief that eating dog helps beat the heat in the summer months has angered animal rights activists around the world.

Known as ‘Bok-Nal’, the dog eating days, it is a ritual celebrated by South Koreans where dog meat is eaten to increase stamina during the hottest days of the year.

To coincide with the tradition, animal rights activists staged protests around the world today, packing into wired cages in various locations including in front of the South Korean embassies in Seoul, London, and America.

While dog meat is consumed throughout the year, during Bok-Nal South Koreans consume more to combat the heat.

Dog meat restaurants in Seoul say they served more customers than usual today.

According to Yonhap news agency, Seoul is experiencing the longest period of time with temperatures above 35 degrees Celsius since 1994.</p>
<!-- /wp:paragraph -->