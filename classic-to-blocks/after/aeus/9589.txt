<!-- wp:paragraph -->
<p>We had multiple protests throughout the month, as well as great participation from our Animal Protectors taking part in hundreds of online actions.

Our first protest of the month fell on the same weekend as the Animal Law Conference, Midwest March for Animals and VeganMania - three high profile events located in Chicago that generated a ton of publicity and on-the-ground activist participation.

The protest took place after the March, assembling over two dozen silent protesters in front of a McDonald’s restaurant in downtown Chicago. At the protest, we held posters that stated: “Learn the Truth” and depicted the horrible conditions chickens live in on farms like those used by McDonald’s.

At the March, we were given the chance to address its hundreds of attendees, giving a rousing speech on our McDonald’s campaign and work across the country. At VeganMania, an event boasting thousands of attendees, we tabled with McDonald’s collateral and iAnimal, signing up dozens of people to become Animal Protectors.

In Orlando, we liaised with an animal rights student group, Knights for Animal Rights, and provided consulting and logistical resources to assist the group in putting together a large McDonald’s protest during the weekend of Central Florida VegFest. The protest welcomed almost 40 protesters who held Animal Equality campaign posters and passed out leaflets. The volunteer-only led protest was the first of its kind in the U.S. for Animal Equality’s McDonald’s campaign.

During the week of Halloween, we commissioned a mobile billboard depicting chickens in distress with the slogan “What Unhappy Meals Are Made Of” and “McDonald’s: Ban Chicken Abuse”, which circled McDonald’s Headquarters and flagship Rock N Roll McDonald’s (that displayed McDonald’s special Halloween exhibit). We joined coalition partner The Humane League in protesting the headquarters and adjacent restaurant, bringing in almost a dozen protesters. At the event was a creepy Ronald on stilts, Abby the Chicken and an Unhappy Meal installation looping footage of chickens in distress. We capped the week of actions with leafleting Chicago neighborhoods and participating in a lighted sign demonstration on Chicago’s Riverwalk, displaying the hashtag #ImNotLovinIt to hundreds of passersby.

Our new U.S. Corporate Outreach Manager, Katie Arth, spent October working with coalition partners to share resources and work to make a difference for chickens beyond the McDonald’s campaign.

To achieve this, our primary goal with U.S. Corporate Outreach is to contact restaurant chains and encourage them to create better chicken protection policies. We believe that more restaurants adopting these policies will help urge McDonald’s to change by creating a groundswell of corporate momentum on this issue. To that end, we’ve connected with 88 companies in October and were able to have introductory calls with at least eight of those companies.

It is beyond time for McDonald’s to ban the cruelty in its supply chain. Please use your voice for good and sign our petition and join our Animal Protectors in asking McDonald’s to finally make a change.</p>
<!-- /wp:paragraph -->