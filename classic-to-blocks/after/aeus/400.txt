<!-- wp:paragraph -->
<p>A nine-year-old chimp at Dublin Zoo has rescued three baby ducklings from possible danger.   Bossou spotted the three-day-old ducks in a nest on the edge of Chimp Island in the zoo. He rescued them from the water and cradled them until zoo authorities became aware of what was going on.

The ducklings have now set up home in the shelter. Spokesperson Miriam Kerins said the story was unusual.  "This large chimpanzee, who was a male, cradled the ducklings, looked after them, and minded them, thinking they were in danger…I think it's so touching," she said.</p>
<!-- /wp:paragraph -->