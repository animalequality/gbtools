<!-- wp:paragraph -->
<p>The 34-year-old England striker, Jermain Defoe, credits his recent comeback to having <strong>adopted a plant-based diet</strong>. Despite his advancing age, he is a star player who is eager to keep his body in <strong>peak condition</strong>.

Like many other star athletes, the switch to a vegan diet has had incredible results for him. He is seeing his performance soar and his recovery periods plummet. According to Defoe, “<strong>I don’t find anything hard to give up</strong>...because I know the feeling scoring goals gives me.”

In a sport like a soccer, energy and nutrition are everything. Defoe is just the last in a growing list of athletes to recognize that you can get both (and more) from a 100% animal-free diet.

Take for example David Carter, the self-proclaimed “300-pound vegan”, an NFL defensive lineman. <strong>He’s credited his diet with leaving him less sore, sluggish and recovering a lot faster</strong>. He told the Chicago-Sun Times in a recent interview: “I was shocked. When I first started, I was, ‘What the hell? I have more energy. I’m a lot stronger than I was before.”

Tennis superstars Venus and Serena Williams also eat a plant-based diet. Venus, in particular, credits the diet for her being able to get back on the courts so quickly after being diagnosed with an autoimmune condition.

Superstar athletes, like Defoe, make headlines when they go vegan but the science is clear: <strong>a plant-based diet&nbsp;is healthy for everyone</strong>. Studies prove that fruits, vegetables, grains, and nuts are full of #plantpower!

&nbsp;

&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Photo:&nbsp;James Boyes</p>
<!-- /wp:paragraph -->