<!-- wp:paragraph -->
<p>For several months, Animal Equality has worked&nbsp;closely and intensively&nbsp;with the&nbsp;Italian program 'AnnoUno' to launch a never-seen-before&nbsp;investigation into the Italian pig industry. The program broadcasted on Thursday, May 21 on channel 'La7' and <strong>was watched&nbsp;live by 1.020.936&nbsp;people.</strong>

Following this presentation, Animal Equality launched an exclusive report <a href="https://www.corriere.it/animali/15_maggio_24/dalla-nascita-macellazione-vita-maiali-italiani-allevamenti-lager-747db8ba-0234-11e5-8422-8b98effcf6d2.shtml">with Italian&nbsp;newspaper 'Corriere della Sera' </a>showing new&nbsp;distressing&nbsp;footage of&nbsp;the reality experienced by millions of pigs in&nbsp;factory farms and slaughterhouses in Italy.

The footage obtained by our undercover investigators shows:

• Sows inseminated artificially in tiny crates where they will be forced to live throughout the pregnancy, and most of their lives.
• Piglets&nbsp;crushed and thrown into the&nbsp;trash.
• Pigs crammed&nbsp;in the dirt, living amongst urine and feces.
• Diseases and severe injuries that went untreated (such as&nbsp;serious rectal prolapses and hernias).

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4228} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/17951514280_bb31893131_z.jpg" alt="" class="wp-image-4228"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Over&nbsp;10 million pigs are slaughtered yearly in Italy for their meat in conditions similar to the ones exposed by&nbsp;this investigation.

"The investigation team has obtained undercover footage of what happens to pigs during&nbsp;slaughter as well as&nbsp;pictures of how these animals are treated in pig&nbsp;farms. These animals life begins with mother pigs being&nbsp;artificially inseminated in gloomy sheds that they never leave. Their life end on&nbsp;a&nbsp;hook&nbsp;in a slaughterhouse." Matteo Cupi, Executive Director of Animal Equality in Italy.</p>
<!-- /wp:paragraph -->