<!-- wp:paragraph -->
<p>Livestock is a major factor of <a href="http://www.animalequality.net/node/775">global warming</a> based on a well-known fact: growing vegetables to feed billions of animals is a disastrous process, much less efficient than humans directly eating plants and their derivatives.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":4686,"align":"left"} -->
<figure class="wp-block-image alignleft"><img src="/app/uploads/2015/12/maniclima1.jpg" alt="" class="wp-image-4686"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Because of its disastrous energy deficiency, livestock emits 15% of greenhouse gases into the atmosphere, an amount greater than the entire transportation industry together - planes, trains, cars, and boats, included.

Livestock&nbsp;is also a major factor in water and air pollution, water scarcity, soil degradation, deforestation, the loss of biodiversity, and ocean pollution.

So far, livestock and meat production have been&nbsp;outside of the agenda of governments and environmental organizations, however, things are&nbsp;changing, and changing fast.

The documentary, <a href="https://www.cowspiracy.com/"><em>Cowspiracy: The Sustainability Secret</em></a>, is playing an important role in the change. In the film, the highly destructive power of raising livestock, as well as the suspicious silence of major environmental organizations on the matter are exposed.

The United Nations has issued a <a href="https://www.theguardian.com/environment/2010/jun/02/un-report-meat-free-diet">report</a> calling for a change in&nbsp;the food model in which the population would mainly feed&nbsp;on a diversity of plants. This change is necessary in order to combat world hunger, the energy crisis, and the impact of climate change.

The international committee of experts at the independent policy institute, Chatham House, has issued a widely documented <a href="https://www.chathamhouse.org/2014/12/livestock-climate-changes-forgotten-sector-global-public-opinion-meat-and-dairy-consumption">study</a> that tells governments that drastically reducing consumption of meat and milk will be a key factor to keep global warming below the dangerous two-degree rise in temperature.

In this context, and with the Paris Summit underway, demonstrations have been held in cities around the world calling on the participating countries to incorporate the disasters of animal agriculture among the factors to be discussed. It is known that the Summit does not usually take this topic into consideration, even though they are aware of the impact of meat production on the climate.

In these demonstrations, the message of veganism has been noticed. This is a growing movement showing that millions of people around the world are turning to a more responsible lifestyle with the animals and the planet in mind.

As Anna Pippus of The Huffington Post columnist said in her <a href="http://www.huffingtonpost.com/anna-pippus/vegan-signs-dominate-clim_b_8692480.html">article</a>, "Killing animals is killing the planet."

If you want to help stop climate change, in addition to saving the lives of numerous animals, try choosing <a href="https://essentialvegan.uk/">a more compassionate diet</a>. Millions of people already are changing their eating habits, helping countless animals and the planet. Try new <a href="https://loveveg.com">recipes</a> today!</p>
<!-- /wp:paragraph -->