<!-- wp:paragraph -->
<p><strong>L'Occitane, Yves Rocher</strong>, and<strong> Caudalie</strong> are among the high-end brands that can no longer use the internationally-recognized official Leaping Bunny logo to show their cosmetics are free from animal testing.

It comes after the firms decided to start selling to China where animal testing on beauty products for human use is still required by law.

Cosmetic sales in China increased by 18 per cent to £10billion last year - making it an attractive financial prospect.

But many companies selling their products in the country have been asked to fund animal testing of their products in Chinese laboratories in order for them to be sold to the public.

Cruelty Free International chief executive Michelle Thew said: '<em>Following discussion with L’Occitane, its certification was retracted in mid-December</em>.'&nbsp;<em>'Some companies wish to bring ethical beauty to China, however, this is not currently possible until China changes its current policy which requires animal testing.</em><em>'I am disappointed that certain companies have fallen prey to the lure of the Chinese market and are letting animals pay the price. Consumer pressure can make a difference. '</em><em>We certify over 400 companies around the world that refuse to allow animal testing into their products.'</em><em>'The only way that you can avoid animal testing in your toiletries and beauty products is by looking for the <strong>Leaping Bunny logo</strong>, or checking <a href="http://www.GoCrueltyFree.org/"><strong>www.GoCrueltyFree.org.</strong></a></em>'

Britain banned animal testing in 1998 and several large cosmetics companies including Paul Mitchell, Sainsbury's, The Co-operative, Superdrug, Marks &amp; Spencer all have Leaping Bunny certification meaning they are cruelty-free.

<strong>Hair-care </strong>giant <strong>John Paul Mitchell Systems</strong> pulled out of China after being informed that the company would have to pay for animal tests in order to continue selling its products there.

Paul Mitchell CEO and co-founder John Paul DeJoria put sales in China on hold last year and confirmed they will not sell products in that country in order to remain committed to the company's cruelty-free policy.

Urban Decay has also recently decided to cancel its plans to enter the Chinese market after being informed of the animal testing requirements.

But the revelation that some large name brands are giving up their cruelty-free status to sell to China will come as a surprise to many shoppers.

Even more surprisingly, there are many huge international brands including <strong>Chanel, Yves Saint Laurent, </strong>and <strong>Revlon</strong>, w<strong>hich have never been able to use the Leaping Bunny logo because of their animal testing policies.</strong>

And while an EU-wide ban on the marketing of animal-tested cosmetics is due to come into force next year, campaigners warn that the European Commission is now contemplating compromises or even delays to the legislation.</p>
<!-- /wp:paragraph -->