<!-- wp:paragraph -->
<p><strong>Information on actions that were taken against some of the most egregious violators of the Animal Welfare Act (AWA)</strong>, will now only be available via Freedom of Information Act requests -- requests that can take years to be approved.

For example, Paul Solotaroff of <em>Rolling Stone</em> was able to write about a dog breeder who threatened to stab a USDA inspector with a syringe, after he cited him for killing a dog.

And officials in Tennessee revoked plans for a drive-thru safari park when they discovered that the exhibitor was previously responsible for several AWA violations.

Public records, such as these, are a <strong>vital tool for animal advocacy groups</strong>, as well as journalists looking to check records of zoos, aquariums, circuses, research labs, puppy mills, and other facilities regulated under the AWA and Horse Protection Act.

While the AWA doesn’t provide protections for farmed animals, a lack of transparency of this magnitude raises serious concerns that the next thing to be purged will be the <strong>Humane Methods of Slaughter Act,</strong> which covers farmed animals.

Any attempt by the agency to shield itself - or animal abusers - from public scrutiny <strong>allows animal cruelty to go unchecked</strong>.

Animal Equality and other animal protection groups believe that public access to this information is key to preventing animal abuse.

As of February 6, the Humane Society of the United States has <strong>initiated legal action against the USDA,</strong> claiming that the removal of this information breaches a 2009 legal settlement concerning access to information collected under the AWA.

<strong>Please help ensure the USDA is holding animal abusers accountable </strong><strong>by</strong><strong><a href="https://secure.humanesociety.org/site/Advocacy?cmd=display&amp;page=UserAction&amp;id=7303&amp;autologin=true&amp;s_src=sh_fb"> taking action now</a>.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Photo:&nbsp;Ratikova / Shutterstock</p>
<!-- /wp:paragraph -->