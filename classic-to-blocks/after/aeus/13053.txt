<!-- wp:paragraph -->
<p><strong>1. THEY PURR</strong>

Both cats and chickens purr when they’re happy. That’s right! Give a chicken a good scratch and they’ll purr like a cat!

<strong>2. THEY ARE SOCIAL</strong>

Chickens take the win on this one. While roosters tend to keep to themselves, hens and chicks like to spend their days together, scratching for food, dust bathing, roosting in trees, and lying in the sun. And the “pecking order” is real!

<strong>3. THEY SHOW EMPATHY</strong>

It’s been observed that mother hens worry when they think their chicks are in danger. If their chicks get separated from them, the mother will rush to the chicks’ aid and bring them back to the nest. Researchers believe that <a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.699.4540&amp;rep=rep1&amp;type=pdf">this shows empathy</a>. While cats can show emotion, I know my cats wouldn’t win any awards for being empathetic.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center><img class="aligncenter wp-image-13070" src="https://animalequality.org/app/uploads/2019/09/sleepycat_chick_blog-500x0-c-default.jpg" alt="" width="572" height="391"></center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>4. THEY ARE CHATTY</strong>

It’s unclear if chickens and cats have deep conversations with their own kind, but they are definitely both very vocal! Researchers say they have found up to 30 different vocalizations for chickens, from cackling alarm calls to the ‘laying of the egg’ ceremony song. Communication even begins before hatching! While still inside the egg, mother hens chirp to their young, and <a href="https://www.sciencedirect.com/science/article/pii/S0168169910002589">they peep back</a>.

<strong>5. THEY ARE INTELLIGENT</strong>

If you have a cat, you already know how intelligent (and sneaky) they can be. But did you know that chickens show many complex skills as well? They have demonstrated their ability to problem solve, be creative, and even do some basic arithmetic! They can also recognize up to 100 individuals in a group.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center><img class=" wp-image-13071 aligncenter" src="https://animalequality.org/app/uploads/2019/09/cat_chick_blog.jpg" alt="" width="574" height="392"></center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>6. THEY ARE CHERISHED IN MANY HOMES</strong>

According to <a href="https://www.statista.com/statistics/198095/pets-in-the-united-states-by-type-in-2008/">Statista</a>, 94.2 million cats live in U.S. homes - even more than dogs! And while some people have chickens as companion animals, most chickens live short, miserable lives. An estimated 9 billions chickens are slaughtered for their meat each year. The majority of these birds are bred to grow at an abnormally fast rate, leading to intense physical suffering over the course of their shortened lives.

While traits such as intelligence should not be reason enough to treat an individual a certain way, it sure makes you wonder why we favor some animals over others. And the way we treat chickens raised for meat on modern farms is absolutely appalling.

<strong>While they should have access to the sun and open air, they’re in giant sheds with tens of thousands of other birds. While they should be digging curiously in the dirt and grass, their bellies are being burned by the ammonia of the overly soiled substrate. While they should be playing, they are struggling to stand. While they should live to be 8-10 years old, they are normally killed at only 42 days.</strong>

We are calling on some of the largest companies in the world to eliminate the worst abuses in their supply chains for chickens. McDonald’s is one of those companies still refusing to do so. <strong><a href="https://mcchickencruelty.com/">Demand they take action to help these innocent baby chickens!</a></strong></p>
<!-- /wp:paragraph -->