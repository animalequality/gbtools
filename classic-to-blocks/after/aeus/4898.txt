<!-- wp:paragraph -->
<p>These dogs, as many other animals, are seen only as a useful tool that can be replaced and discarded when no longer needed.

By Spanish law, it is forbidden to use physical abuse, maime dogs, keep dogs on short chains and abandon dogs. But the Spanish government has turned a blind eye to the plight of Galgos and Podencos.

Many of them live only until they are 2 to 3 years old, then they are replaced.

The Galgos are brutally murdered at the end of hunting season. Tens of thousands of these dogs are abandoned and starve to death or are tied around the neck and hanged in such a way to a tree, that the animal barely reaches the ground. Being murdered in this way is a slow death. The dog has time to feel immense panic, anxiety, and suffering. This suffocation can take hours and sometimes even days</p>
<!-- /wp:paragraph -->