<!-- wp:paragraph -->
<p><strong>An extensive undercover investigation by Animal&nbsp;Equality inside 21 slaughterhouses in seven different states in Mexico reveals extreme cruelty against animals.</strong><strong>In order to protect farmed animals from the abuse they are currently subjected to, we need legislation&nbsp;that protects them from suffering.</strong><a href="https://www.youtube.com/watch?v=Q2qJIDOw-cU">Our investigations team documented the horrible practices</a> that are commonly used in slaughterhouses across Mexico, including in government-owned slaughterhouses in the states of Jalisco, Nuevo León, Aguascalientes, San Luis Potosí, Colima, Zacatecas, and Nayarit.

The images they captured show how these facilities constantly violate the Federal Animal Health Law without any legal repercussions.

Our investigators documented:
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>100% of the pigs are fully conscious while they are being stabbed. In most of the slaughterhouses, pigs are stabbed more than twice. As a result, pigs bleed to death on the ground.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Workers violently forcing pigs to move by beating and electrocuting them.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Workers killing more than three pigs at a time. Some pigs step on others trying to escape the killing pen while workers are killing the other pigs.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>None of the documented slaughterhouses used stunning methods for sheep and goats. These animals were violently slaughtered, with their legs tied up, dragged from one of their limbs across the floor, and beheaded.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Pigs being beaten with axes.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Cows being forced to move with electric prods, causing them excruciating pain and suffering.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Cows attempting to stand up while bleeding after waking up from stunning.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Underage children inside the slaughterhouses witnessing how animals are killed.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>A worker violently tying a lamb, slitting his throat and ripping off his head while the animal is fully conscious.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>
This is what animals&nbsp;raised for human consumption face in Mexico every day.

The animal welfare laws that protect dogs and cats don’t include animals in farms and slaughterhouses. If someone did to a dog or a cat what factory farms do to pigs, cows, sheep, chickens, hens or goats, they would be imprisoned.

Unfortunately, there aren’t any laws that protect farmed animals from abuse. Although there are some regulations in Mexico, the law is not enforced and workers don’t face any legal consequences for torturing animals.

Currently, <strong>the Mexican Federal Criminal Code doesn’t include penalties for those who abuse or are cruel to animals</strong>, and the Federal Animal Health Law only applies for violations, comparable to a parking ticket, for slaughterhouses that don’t comply with the regulations. However, these laws don’t punish the person who commits acts of cruelty.

Please consider helping farmed animals by leaving them off your plate, and by <a href="https://animalequality.org/action/mexican-slaughterhouses">signing our petition demanding to punish those who abuse them</a>.</p>
<!-- /wp:paragraph -->