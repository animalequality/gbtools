<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>On Wednesday June 26, <strong>Javier Moreno,</strong> co-founder of Animal Equality, spoke at the <strong>European Parliament</strong> in Brussels in collaboration with the&nbsp;<a href="http://www.StopVivisection.eu/en">Stop Vivisection campaign,</a> campaign. An initiative that could result in the end of animal testing in Europe.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Below you can read Javier's speech. Support this initiative by signing the petition&nbsp;at&nbsp;<a href="http://www.StopVivisection.eu/en">www.StopVivisection.eu/en</a></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div>
<p align="justify"><em>“Good afternoon, mi name is Javier Moreno, and I´m here to speak on behalf of Animal Equality, an international animal rights organisation. I would like to thank the committee for inviting me here today, along with the other NGO representatives, scientists and politicians.</em></p>
Stop Vivisection is an initiative which shows how citizens, are participating to show politicians that they want changes. Politicians and decision makers who sit here today, should be aware that they represent society and their concerns. But this does not happen. I feel that big institutions with huge economical power, are the ones who are deciding for us, and that politicians are mere puppets who vote what they are told. This is not democracy.
<p align="justify"><em>The first step to change this situation is becoming aware of it.&nbsp;At Animal Equality we are working to gather a million signatures,&nbsp;before November the 1st&nbsp;2013, and encouraging society to believe that they can make changes for animals.</em></p>
<p align="justify"><em>All the surveys show us, that our society´s concern towards the situation of animals is increasing, and there are more and more people who wish to put an end to the dramatic situation animals suffer behind closed doors.</em></p>
<p align="justify"><em>We have turned this world into hell for animals. The numbers speak for themselves. Only in Europe, animal testing causes 12 million deaths each year. Animals suffer when they are hit, mutilated, poisoned, or forced to swallow toxic substances, in the same way we do. We allow ourselves to live in a great paradox, as they tell us that animals are just like us, and that the results obtained in animal experimentation can be extrapolated to humans. But at the same time, they make us believe that we can make them suffer because they are&nbsp;different from us, meaning that they don't deserve the same&nbsp;consideration.</em></p>
Today there are several scientific studies that prove the ability of animals to experience suffering. Most animals that are being exploited, experience fear, hunger, stress, they get depressed, and they will do anything to fight for their lives. But in spite of all the evidence, the paradigm remains unchanged.
<p align="justify"><em>We are talking about moral fraud --there are also more and more voices from the scientific community, like the ones gathered here today, that talk about scientific fraud and provide evidence of it. I would like to point out the ethical crossroads that animal experimentation is facing. We all want science to progress and we all want a better world but not at any cost. The protection we have established for humans must spread to protect the rest of animals we share this planet with, for the simple reason that we share the same interest in not being harmed and in enjoying our life.</em></p>
When speaking about animals that cannot speak for themselves, it is&nbsp; important to recover our role in transforming society, but it is even more urgent and determining that we get involved. That is the driving force of the animal rights movement, which is the same for the rest of the animal freedom movements: defending the weak from the powerful, helping those who suffer oppression to be released from it. Empathy, solidarity and justice.
<p align="justify"><em>I would like to finish with, what was for me, a historical moment in our recent struggle against animal testing. Everyone here will probably remember the images of april the 28th of 2012 when antivivisection activists in Italy jumped over the fence of Green Hill to rescue the beagles who were forced to stay in this facility. All those hands raised, catching the dogs and releasing them through the fences will always be remembered. Those are the hands that represent our daily fight. Hands for freedom, hands that claim justice for every animal.</em></p>
Let that moment be our inspiration to never give up and to feed the new world we are fighting for. And let this Stop Vivisection initiative be one more step towards it.
<p align="justify"><em>Thank you.”</em></p>

</div>
<!-- /wp:html -->