<!-- wp:image {"id":8381} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/happyrabbit.jpg" alt="" class="wp-image-8381"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
The European Parliament today voted 410 to 205 in favour of measures to improve farmed rabbit welfare, which includes the drafting of legislation to end the use of battery cages. This historic result has paved the way for legislation to help more than 340 million rabbits every year who currently experience extreme suffering in battery cages.

Animal Equality and its supporters lobbied MEPs across Europe, sending more than 120,000&nbsp;emails urging MEPs to support this long-fought initiative by German MEP Stefan Eck&nbsp;to protect farmed rabbits. Compassionate celebrities such as Evanna Lynch, Victoria Summer, Peter Egan and Dave Spikey also backed the campaign.

Toni Shephard, Executive Director of Animal Equality UK, said: “Today history has been made! One of the cruelest farming practices ever invented, confining animals in tiny barren cages for their entire lives, could soon be obsolete in Europe. This is amazing progress for hundreds of millions of rabbits who currently endure extreme suffering in battery cages on European farms. Animal Equality will keep working with European and national politicians until rabbit cages are confined to history.”

<a href="https://www.youtube.com/watch?v=E2UHnOsfxhw">Footage taken by Animal Equality</a> investigators on rabbit farms in Spain and Italy was instrumental in this landmark decision. Investigations on more than 75 farms revealed the pain and suffering that caged rabbits endure, including rabbits left with open and infected wounds and countless dead rabbits left to rot in cages alongside living rabbits, and even cases of cannibalism due to the unnatural and stressful conditions.

Animal Equality is&nbsp;so thankful to everyone that&nbsp;gave rabbits a voice by&nbsp;contacting&nbsp;their MEP. Thanks to you we&nbsp;are one step closer to freeing rabbits from their cages!</p>
<!-- /wp:paragraph -->