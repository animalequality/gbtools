<!-- wp:paragraph -->
<p>Caging animals causes some of the most extreme suffering that takes place on factory farms. Animal Equality has teamed up with other animal protection groups in the UK to urge the government to end this inhumane practice and ban cages for all farmed animals, which would improve millions of lives every year.

We have launched an <a href="http://bit.ly/2u20nzG">official UK government petition demanding a complete ban on cages for farmed animals in the UK</a>. This includes keeping mother pigs in farrowing crates, hens used for eggs in cages, and calves in individual pens in the dairy industry.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8886} -->
<figure class="wp-block-image"><img src="https://animalequality.org.uk/app/uploads/2018/11/44213827281_37c5fbc9c6_k-e1543579489335.jpg" alt="" class="wp-image-8886"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Encouragingly, in just over a week from the date the petition launched, over 50,000 compassionate people had already signed it. Our goal, however, is double this figure and it is critical we reach 100,000 signatures as soon as possible to push a Parliamentary debate about this important issue in the House of Commons.

It only takes a few minutes to add your name, pressuring the government to make a real difference for the innocent animals trapped on factory farms. <a href="http://bit.ly/2u20nzG">Please, sign the petition here – animals are counting on us.</a></p>
<!-- /wp:paragraph -->