<!-- wp:image {"id":8421} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/01/Land_crab_3.jpg" alt="" class="wp-image-8421"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Animal Equality is a signatory of this letter as we believe that decapod crustaceans are just as deserving of protection from harm as are any other animals. Further, decapod welfare is currently completely disregarded during farming, processing, storage at restaurants and supermarkets and during killing. Death is often preceded by breaking off legs or tails while the animals are fully conscious, before they are often boiled alive. This method of killing means they suffer for at least 3 minutes before losing consciousness.

The Animal Welfare Act does however state that the act can be changed to include any invertebrate if the body charged with determining animal sentience is satisfied that this has been proven. In the case of decapod crustaceans, we believe there is overwhelming evidence showing sentience, and as such these animals must no longer be allowed to suffer with no protection for their welfare.

The UK must join countries such as Norway, Switzerland and New Zealand in providing these animals the protection they deserve. Inclusion in the imminent Animal Welfare Bill (Sentencing and Recognition of Sentience) and existing Anima Welfare Act would be a timely and appropriate step for the Government to take.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8200} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/01/crab.jpg" alt="" class="wp-image-8200"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

If you agree, you can help <a href="https://www.crustaceancompassion.org/">Crustacean Compassion</a> achieve its goal of having decapod crustaceans reclassified as animals and provided the protection they deserve under UK law. <strong>Sign the <a href="https://www.change.org/p/george-eustice-mp-protect-crabs-and-lobsters-under-the-animal-welfare-act-england-and-wales-by-crustacean-compassion">petition</a> today, and be a voice for these vulnerable animals!</strong></p>
<!-- /wp:paragraph -->