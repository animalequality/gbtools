<!-- wp:paragraph -->
<p>Animal Equality investigators visited Walston Poultry Farm (East Down site), near Blandford Forum in Dorset, four times between January and March 2018 and found:

· &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;80,000 hens in each giant shed, all locked in ‘colony cages’ stacked seven tiers high
· &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Many birds suffering from severe feather loss - some nearly bald - with red, raw skin
· &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dead birds left in cages with the living, some being cannibalised
· &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Birds with visible wounds from being pecked by cage mates
· &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dead birds left lying on walkways next to cages with the living
· &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tubs holding dozens of dead birds left in the sheds overnight
· &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Failure to inspect all birds daily, in violation of welfare regulations

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/IDfHeZmGOAs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

&nbsp;

Around half a million birds are housed in eight windowless sheds on this site, laying around 140 million eggs a year for the UK's largest egg producer – Noble Foods – which operates multiple brands including Big &amp; Fresh (eggs from caged hens) and the free-range Happy Egg Company. Noble Foods is the subject of an ongoing campaign by The Humane League UK because it claims to put the welfare of its ‘girls’ first for Happy Eggs while simultaneously forcing 4.3m hens to suffer and die in cages for its Big &amp; Fresh brand.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8164} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/two-bald_0.jpg" alt="" class="wp-image-8164"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>Take Action</strong>

The best way to end the suffering of these hens is to leave eggs off your plate. Discover delicious egg-free foods and recipes at <strong><a href="https://loveveg.uk/">www.loveveg.uk</a></strong>

You can also help end the use of these cruel cages by <strong><a href="https://www.change.org/p/dale-burnett-noble-foods-stop-farming-hens-in-cages">signing the petition</a></strong> demanding that Noble Foods goes cage-free across all of its brands.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8172} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/dead-floor-brighter_2.jpg" alt="" class="wp-image-8172"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

We have passed all of our footage, photographs and written evidence to Dorset Trading Standards which is charged with investigating on-farm welfare, as well as to the British Egg Industry Council which runs the Lion Code. Both have said they are investigating the farm.</p>
<!-- /wp:paragraph -->