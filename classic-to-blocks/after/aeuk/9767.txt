<!-- wp:paragraph -->
<p><i><span style="font-weight: 400;">Extreme suffering and cannibalism has been revealed on an Essex farm which supplies ‘English Rose’ turkeys to high-end retailers, local butcher shops and pubs.</span></i><span style="font-weight: 400;">Harrowing scenes of suffering on an award-winning farm, Grove Smith Turkeys Ltd, have been released by Animal Equality today. Our investigators found injured, sick and crippled birds tightly packed into crowded sheds, with those who didn’t survive left to rot amongst the living.</span></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="ae-video-container"><center><iframe src="https://www.youtube.com/embed/pSfMY4cSLg0" width="750" height="400" frameborder="0" allowfullscreen="allowfullscreen"></iframe></center></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><span style="font-weight: 400;">Filmed in the last four weeks at Hubbard’s Farm in Essex – Grove Smith Turkeys’ main site – the shocking footage shows:</span></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<li style="text-align: left;"><span style="font-weight: 400;">Birds who are unable to walk being pecked and eaten alive by their flock mates.</span></li>
<!-- /wp:html -->

<!-- wp:html -->
<li style="font-weight: 400; text-align: left;"><span style="font-weight: 400;">Multiple birds with severely infected wounds on their heads and eyes that were left untreated. Some had gone blind.</span></li>
<!-- /wp:html -->

<!-- wp:html -->
<li style="font-weight: 400; text-align: left;"><span style="font-weight: 400;">Crowded sheds without adequate enrichment for these inquisitive animals, causing the birds to peck each other out of boredom and frustration.</span></li>
<!-- /wp:html -->

<!-- wp:html -->
<li style="font-weight: 400; text-align: left;"><span style="font-weight: 400;">Birds who had their beak tip cut off with a hot blade, a painful mutilation performed without anaesthetic.</span></li>
<!-- /wp:html -->

<!-- wp:html -->
<li style="font-weight: 400; text-align: left;"><span style="font-weight: 400;">Dozens of dead birds left to rot among the living, some for so long they were reduced to just skeletons.</span><span style="font-weight: 400;">Marketed as ‘prestigious’ and ‘high-welfare’, the farm supplies birds branded as ‘English Rose’ turkeys to high-end retailers such as COOK and The Great British Meat Company, as well as many local butcher shops and pubs including the Young's chain. </span>

<img class="wp-image-9772 size-full alignleft" src="https://animalequality.org.uk/app/uploads/2018/12/news_turkey_grovesmith-1.jpg" alt="English Rose Turkeys at Grove Smith Turkeys Ltd Farm in Essex" width="4000" height="2400">

<span style="font-weight: 400;">Hidden cameras left running for eight days inside one shed showed that the workers were not checking the birds every day – as required by law – resulting in some birds enduring prolonged suffering and a slow death from untreated injuries. </span><span style="font-weight: 400;">
</span><span style="font-weight: 400;">
</span><span style="font-weight: 400;">We have passed our footage on to the RSPCA, Essex Trading Standards and Defra’s Animal and Plant Health Agency, who are now investigating.</span>

<img class="alignleft wp-image-9774 size-full" src="https://animalequality.org.uk/app/uploads/2018/12/news_turkey2_grovesmith.jpg" alt="English Rose Turkeys at Grove Smith Turkeys Ltd Farm in Essex" width="4000" height="2400">

<span style="font-weight: 400;">Awards and accolades don’t prevent animals from suffering in the meat industry, but you can. With delicious meat-free festive meals now widely available in shops and restaurants across the UK, </span><a href="https://loveveg.uk/"><span style="font-weight: 400;">there’s never been a better time to enjoy a cruelty-free Christmas</span></a><span style="font-weight: 400;">! </span></li>
<!-- /wp:html -->