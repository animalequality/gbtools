<!-- wp:image {"id":2680} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/rabbitvoteA4.jpg" alt="" class="wp-image-2680"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
The submission of this 'alternative resolution'&nbsp;is an unusual step, especially since the report by MEP Stefan Eck was&nbsp;already supported by a majority of votes in the Agricultural Committee&nbsp;(including votes from the EPP) back in January.

Eck's report aims to put an end to cruel rabbit cages and to introduce mandatory minimum standards in rabbit farming. The alternative motion by the EEP&nbsp;merely provides a recommendation for improved rabbit welfare without any binding legislation for the industry.

With this move the EPP is trying to prevent effective animal protection measures and make Eck's report obsolete. It is a desperate reaction to our campaign! Now more than ever we must show the MEPs that we demand&nbsp;serious animal protection and a ban on rabbit cages! <a href="http://www.animalequality.net/banrabbitcages">Please sign and share our petition TODAY</a>.</p>
<!-- /wp:paragraph -->