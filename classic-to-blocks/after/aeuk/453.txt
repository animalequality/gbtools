<!-- wp:image {"id":2118} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/09/fvfg01092012.jpg" alt="" class="wp-image-2118"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Did you know that the 1st of September was the International Bacon Day?
And that it is celebrated around the World by bacon lovers in all sorts of food-eating events?

We believe, that this day of awareness should be centred on the industry itself, and what it really means to the billions of animals slaughtered every year.
For this reason Animal Equality was out on the street in Covent Garden (London), showing and handing out information about the realities of the pig industry, as well as giving out free vegan samples.

Over 400 sandwiches filled with <a href="http://shop.redwoodfoods.eu/cheatin-streaky-style-rashers-115g.html">"vegan bacon"</a> were given out to a very interested crowd and 2.000 <a href="https://issuu.com:443/animalequality/docs/pig_dog_leaftlet">"Something to eat or someone to respect" leaflets</a> were distributed, explaining that animal exploitation exists because of consumers demand, and that we can stop this by choosing a diet free of animal products.

We want to thank <a href="http://shop.redwoodfoods.eu/">The Redwood Co.</a> for supporting this event, as they donated their amazing and yummy products. A special thank you as well, to all the dedicated activists who took part at the event, inspiring people that a world without animal suffering is possible.
<strong>Photo Gallery:</strong></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div id="flickrembed_56"></div>
<!-- /wp:html -->

<!-- wp:html -->
<div style="position: absolute; top: -70px; display: block; text-align: center; z-index: -1;"></div>
<!-- /wp:html -->

<!-- wp:html -->
<script src="https://flickrembed.com/embed_v2.js.rand.php?container=flickrembed_56&amp;source=flickr&amp;layout=responsive&amp;input=www.flickr.com/photos/animalequalityuk/sets/72157631365922122&amp;sort=0&amp;by=album&amp;theme=default&amp;scale=fill&amp;limit=10&amp;skin=default&amp;autoplay=true"></script>
<!-- /wp:html -->

<!-- wp:html -->
<script type="text/javascript">function showpics(){var a=$("#box").val();$.getJSON("http://api.flickr.com/services/feeds/photos_public.gne?tags="+a+"&tagmode=any&format=json&jsoncallback=?",function(a){$("#images").hide().html(a).fadeIn("fast"),$.each(a.items,function(a,e){$("<img/>").attr("src",e.media.m).appendTo("#images")})})}</script>
<!-- /wp:html -->