<!-- wp:image {"id":8408} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/08/Amanda_Abbington_Animal_Equality_04.jpg" alt="" class="wp-image-8408"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
We are thrilled to announce that Raindance, the UK's largest independent film festival, has shortlisted our virtual reality film 'iAnimal: 42 days in the life of a chicken', for the Best Social Impact Experience award.

The five-minute film, which we released in December 2016, is narrated by Amanda Abbington - star of Sherlock and Mr Selfridge - and features 360° footage filmed by Animal Equality investigators inside intensive chicken farms and slaughterhouses in several countries including the UK.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/fgRqM5SNXbo" width="560px" height="315px" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

“This is so horrible, it’s just awful. How can you treat any living thing with such a lack of respect and disregard? People need to be aware, and they’re not.” said Amanda Abbington, who was moved to tears after experiencing iAnimal.

She added: “You should watch this before you eat meat, because I don’t think you would eat it.”

The award winners will be announced during the <a href="https://raindance.org/festival/">Raindance Film Festival 2017</a> in London, September 20th to October 1st, where our iAnimal film will also be showcased.

For lots of ideas on replacing chicken in your diet, check out <a href="https://loveveg.uk/">www.loveveg.uk</a></p>
<!-- /wp:paragraph -->