<!-- wp:image {"id":8410} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/stop_live_transport_logo_cropped.jpg" alt="" class="wp-image-8410"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Every year, millions of live sheep, cattle and pigs are transported thousands of miles in trucks and on ships from Europe, to be slaughtered in other countries around the world. These animals endure excessively long journeys without enough food, water or rest. Crammed in together, some with no space to sit down, they can be in transit for days on end, in extreme temperatures. The stress caused to the animals by this experience leads to lowered immune systems and rapid spread of disease.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8183} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/live-transport-sheepweb.jpg" alt="" class="wp-image-8183"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

When the trucks are emptied at their destination, many, many animals are dead or dying.

Shockingly, the UK and Ireland send thousands of sheep and cattle to Europe every year, from where, they go on to the Middle East, and North Africa. Pigs are sent even further, from the UK directly to China.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2930} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/live-transport-cattleweb.jpg" alt="" class="wp-image-2930"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

Many animals will be exported from Europe to countries that have lower animal welfare standards, or none at all to protect them. Their transport and slaughter would often be considered illegal under UK and European law.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2931} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/live-transport-pigsweb.jpg" alt="" class="wp-image-2931"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

Animal Equality is supporting Compassion in World Farming’s Stop Live Transport campaign and so can you. Visit <a href="https://stoplivetransport.org/">www.stoplivetransport.org</a> to find resources, local groups taking action and a list of events taking place in the run up and on the Stop Live Transport International Awareness Day on the 13<sup>th</sup> September. Sign and share the <a href="https://www.animalsinternational.org/take_action/live-export-global/?ua_s=AI.org">petition</a>, share a message of support, and shout about how abhorrent live transport is, on social media with the hashtag #StopLiveTransport.

We want an end to the barbaric trade and transport of live animals and if you do too, join us on September 13<sup>th</sup>! And remember, live transport and trade wouldn’t exist if nobody ate animals. The number 1 way to help them is to leave them off your plate. Visit <a href="https://loveveg.uk/">Love Veg</a> today to find out how to replace meat with cruelty free alternatives.</p>
<!-- /wp:paragraph -->