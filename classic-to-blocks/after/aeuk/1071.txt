<!-- wp:paragraph -->
<p>Animal Equality investigations have led to several cruelty convictions in the 11 years since we were founded. The longest was handed down to a farm worker we filmed killing pigs by beating them with a metal bar, throwing pigs to the ground and picking them up by their ears on a UK farm in 2012. The man was convicted on five charges of causing unnecessary suffering and one charge of failing to protect the animals from suffering.

Despite his crimes, and clear lack of empathy or care for the animals he was charged with protecting, the man received just 18 weeks in prison. With his conspirator receiving just a suspended 8-week sentence.

Although both men were banned from working in animal husbandry for 10 and 5 years respectively, we believe their punishment did not fit the crime.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8193} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/10/harling-farm.jpg" alt="" class="wp-image-8193"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

Similarly, a young man we filmed beating and kicking cows and their calves on a UK dairy farm last December also escaped a prison sentence, despite the judge commenting that it was the worst example of abusive behaviour he had seen in 35 years in his job.

The local Somerset MP, Rebecca Pow, commented on this case in Parliament. "An example that recently arose in my constituency involved not a dog or a cat, but a dairy farm. The dairy farmer is in the top group for animal welfare standards among dairy farmers, but unbeknown to him, a lad he had taken on as an apprentice—this was secretly filmed by Animal Equality—was going in and kicking the nursing cows in the face, kicking the calves, pressing them up against metal gates, and slamming the gates on them and abusing them verbally. It was absolutely horrific… The lad’s sentence is being considered at the moment, but it will probably not fit the crime."

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8197} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/10/pyrland-farm.jpg" alt="" class="wp-image-8197"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

However, this is set to change with the advent of tougher sentences available to law enforcers. With the maximum penalty now raised from a mere 6 months to 5 years in prison, we hope this works as a deterrent to would-be animal abusers, with those that are convicted punished to the full extent of the law.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8180} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/10/harling-pigs.jpg" alt="" class="wp-image-8180"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

Although cruelty comes as standard legal practice on intensive farms, we are pleased that those convicted of causing suffering to the animals in their care will now be subject to tough penalties which are in line with other EU countries.</p>
<!-- /wp:paragraph -->