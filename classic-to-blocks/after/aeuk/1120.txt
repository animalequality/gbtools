<!-- wp:paragraph -->
<p>In just 12 months we have amassed <a href="https://animalequality.org.uk/act/ban-force-feeding" target="_blank" rel="noopener">over 88,000 signatures on our petition</a> calling for a foie gras import ban post-Brexit. This is largely thanks to the compassionate celebrities who have supported us and shared the petition, including Ricky Gervais, Evanna Lynch, Chris Packham, Amanda Abbington and Peter Egan.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":3050} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/06/evanna_fg.jpg" alt="" class="wp-image-3050"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

We have made great political progress as well. Soon after we launched the campaign, Kate Hoey MP put forward an Early Day Motion calling on Parliament to ban foie gras imports. This helped put the issue on politicians’ radars. In the following months we worked with animal-friendly MPs from all parties to push it up the political agenda.

In February 2018 Labour announced that it would ban foie gras imports as part of its Animal Welfare Plan, and just a few days later Michael Gove – Secretary of State for the Environment – said that he would also consider it after we leave the EU. As encouraging as Mr Gove’s statement was, we are calling for an import ban to be passed now and enacted on the day that we leave the EU. This disgustingly cruel trade should not continue even one day longer than necessary!

So we persisted. On 29th March, marking one year until our departure from the EU, we delivered 70,000 signatures to Defra calling for a foie gras-free Great Britain&nbsp;– with the help of Bill Oddie, Britain’s best-known birder. Despite torrential rain, we were also joined by MPs Andrea Jenkyns, Sandy Martin, Sir David Amess, Kerry McCarthy and Baroness Jenny Jones.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":3053} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/06/andrea_bill_fg.jpg" alt="" class="wp-image-3053"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

In May we brought animal welfare experts Professor Donald Broom and vet Emma Milne, who lives in France herself, to Parliament to outline the severe welfare problems inherent in foie gras production and why Britain must end its import.

Two weeks ago Henry Smith MP led an adjournment debate in Parliament where he made a passionate and powerful speech calling for an import ban, which you can watch below. He received strong support from Heidi Allen and Kerry McCarthy, who both highlighted our current double standards for allowing the import and sale of a product that causes such great animal suffering that its production is banned under our own welfare laws.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/Qd7gow0lpXo?rel=0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

We were extremely encouraged by the response from the Minister George Eustice who confirmed that our departure from the EU does present an opportunity for a foie gras import ban and that we would face no barriers under World Trade Organisation laws. Sadly, he stopped short of committing to a ban, so our campaign continues!

Please <a href="https://animalequality.org.uk/act/ban-force-feeding" target="_blank" rel="noopener">add your name to the petition</a>&nbsp;and <a href="https://twitter.com/intent/tweet?text=Help%20make%20Britain%20foie%20gras-free%21%20Sign%20%40Animal_Equality%27s%20petition%20calling%20for%20an%20import%20ban%3A%20www.animalequality.org.uk%2Ffoie-gras%20%23FoieGrasFreeGB&amp;source=webclient" target="_blank" rel="noopener">share it on social media</a> if you haven't done so already.&nbsp;A #FoieGrasFreeGB is within sight, and together we will make it a reality!</p>
<!-- /wp:paragraph -->