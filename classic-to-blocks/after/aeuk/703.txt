<!-- wp:paragraph -->
<p><strong>An amendment&nbsp;to New Zealand law on behalf of the&nbsp;The Animal Welfare Amendment Bill, which was passed on Tuesday, states that animals, like humans, are "sentient" beings.</strong>

"To say that animals are sentient is to state explicitly that they can experience both positive and negative emotions, including pain and distress," said Dr Virginia Williams, chair of the National Animal Ethics Advisory Committee.

"The explicitness is what is new and marks another step along the animal welfare journey."

The recent bill has also included a ban on the use of animals for the testing of cosmetic products.&nbsp;Dr Williams said the legal recognition of animal sentience provided a stronger underpinning of the requirements of the Animal Welfare Act.

Nelson SPCA manager Donna Walzl said the changes were "wonderful" and that&nbsp;"it's great to finally see it brought into legislation. It's awesome."

She went on to explain that&nbsp;"you can see that they do have separation anxiety and that's showing emotion.&nbsp;It's the same with the animals that we see that are neglected and have real, true animal welfare issues. They suffer for it. You can see it in their eyes."

The SPCA Auckland said a declaration of sentience was required considering "most New Zealand law treats animals as 'things' and 'objects' rather than as living creatures".

Walzl said she hoped that this new change in law by recognizing animals are the sentient beings that they are would add&nbsp;"more weight" to abuse and neglect cases in court.&nbsp;"Hopefully there will be some sterner penalties out there and that obviously creates a bigger deterrent for people to do those things."

The bill also provides for a penalty scheme to enable low-to-medium level offending to be dealt with more effectively, and gives animal welfare inspectors the power to issue compliance notices, among other measures.

"Expectations on animal welfare have been rapidly changing.&nbsp;The bill brings legislation in line with our nation's changing attitude on the status of animals in society." said New Zealand Veterinary Association president Dr Steve Merchant.

The bill was introduce to parliament by primary industries minister Nathan Guy in May, 2013.

Read the Animal Welfare Amendment Bill&nbsp;<a href="http://www.legislation.govt.nz/bill/government/2013/0107/latest/DLM5174807.html?path=bill%2fgovernment%2f2013%2f0107%2flatest&amp;col=bill&amp;fid=DLM5174807&amp;search=sw_096be8ed81047b83_sentient_25_se&amp;p=1">here.</a></p>
<!-- /wp:paragraph -->