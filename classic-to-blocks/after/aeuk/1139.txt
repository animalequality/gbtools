<!-- wp:paragraph -->
<p>Over the last year, Animal Equality’s campaign for a UK import ban on foie gras has gained widespread public support and this week we delivered a milestone 100,000 petition signatures directly to Number 10, calling on the government to commit to a ban.

Actor and animal advocate Peter Egan joined campaigners outside Downing Street to hand in the names, showing his support and saying “<em>We banned its production in the UK because it’s cruel. Now let's ban the importation of this cruel product. Make the UK Foie Gras Free!</em>”

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8145} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/09/fg-hn-3.jpg" alt="" class="wp-image-8145"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

Animal Equality has documented the suffering of ducks and geese on foie gras farms across France and Spain where birds are force-fed to produce engorged fatty livers. This process is so cruel that it is illegal in the UK, yet nearly 200 tonnes of foie gras is still imported here every year. Leaving the European Union gives us a unique opportunity to rid Britain of this extremely cruel product.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8144} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/09/fg-hn-1.jpg" alt="" class="wp-image-8144"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

We’re keeping the pressure on the government to act now and ensure a ban on the importation of this cruel ‘delicacy’ will be enacted on 29th March 2019, the day we leave the European Union. Join us and <a href="https://animalequality.org.uk/act/ban-force-feeding">add your name in support</a> of a foie gras-free Great Britain!</p>
<!-- /wp:paragraph -->