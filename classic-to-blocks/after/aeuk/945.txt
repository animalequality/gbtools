<!-- wp:paragraph -->
<p>And it’s not just the USA getting all the plant based action; we Brits have an amazing selection of dairy free alternatives at our fingertips.

<strong>“Milks”</strong>

Soy, Almond, Cashew, Coconut, Macadamia, Oat, Rice, Hemp, the list goes on and the choice is yours! But, here are a few of our favourites: &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8280} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/01/bonsoy4.jpg" alt="" class="wp-image-8280"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

&nbsp;

For coffee you can’t beat <a href="https://www.ocado.com/webshop/getSearchProducts.do?clearTabs=yes&amp;isFreshSearch=true&amp;chosenSuggestionPosition=0&amp;entry=bonsoy">Bonsoy</a>. A little pricier than most, but designed especially for hot drinks, Australian born Bonsoy is worth every penny. Available from Ocado. A very close second is <a href="http://www.waitrose.com/shop/DisplayProductFlyout?productId=512984&amp;source=sho_&amp;utm_source=google%2Bshopping&amp;utm_medium=organic%2Bgs&amp;utm_campaign=google%2Bshopping&amp;gclid=Cj0KEQiAh4fEBRCZhriIjLfArrQBEiQArzzDAQ64XOy_vWIgYQG7kOFKLzvbrx8SD-skY0hjJOyEFx">Oatly Foam-able</a> and <a href="http://www.healthyfoods-online.com/alpro-coconut-for-professionals-1l.html">Alpro Coconut for professionals</a>. Both are widely available at all the major supermarkets and health food shops too. And for the tea leaves out there we recommend <a href="http://www.waitrose.com/shop/DisplayProductFlyout?productId=18074&amp;source=sho_&amp;utm_source=google%2Bshopping&amp;utm_medium=organic%2Bgs&amp;utm_campaign=google%2Bshopping&amp;tsrc=vdna&amp;gclid=Cj0KEQiAh4fEBRCZhriIjLfArrQBEiQArzzDAXEneX1o7pB_PHhg9u9i5T-84ky4I26Jz">Alpro’s organic soya milk</a>.

<strong>Top tip!</strong> Is there anything worse than brewing some coffee, adding your favourite milk and experiencing the dreaded curdle effect? After extensive trials, the Animal Equality UK team have worked out the trick to avoiding this. Warm your milk on the hob or in the microwave before adding the coffee. Simples!

If you’re baking or cooking almost any milk will work, but <a href="http://www.waitrose.com/shop/HeaderSearchCmd?searchTerm=rude+health+almond+milk&amp;defaultSearch=GR">Rude Health’s organic almond milk</a> is particularly good for sweet bakes. Found in all supermarkets in the long life milk or health food section, Rude health use organic Italian almonds and no yucky additives, with delicious results. Also great for cereals and porridge are their <a href="http://www.tesco.com/groceries/product/details/?id=288500532">rice</a>, <a href="http://www.tesco.com/groceries/product/details/?id=285490967">oat</a> and <a href="http://www.tesco.com/groceries/product/details/?id=288500463">coconut</a> milks.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8259} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/01/Rude_Health_milks_wide_large.jpg" alt="" class="wp-image-8259"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Of course, everyone has their own preferences and taste buds but for the best products, avoid those with a huge list of ingredients. Often those with the simplest ingredients are the best tasting and better for you!

Other brands to look out for include <a href="http://www.tesco.com/groceries/product/details/?id=252098146">Rice Dream</a>, <a href="https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/koko-long-life-coconut-milk-alternative-1l?storeId=10151&amp;langId=44&amp;krypto=fb0Y4AwQHI82fpfxCjuN5t6CwtqyJT5UOG5Pd54k3bCBm42gCG%2BGSjT09jmNs%2FnDLz8sY6DfRkryrYadKAZA5mptoSEBdpr7BKkpEzKQ70VWgoVzFyERBRVMe28SvxMYOd79P5qafcVYEy6qpVTD355c%2BQEe4H9sva5l%2Fb56cm4%3D&amp;ddkey=https%3Agb%2Fgroceries%2Fkoko-long-life-coconut-milk-alternative-1l">Koko</a>, <a href="https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/blue-diamond-long-life-almond-milk--unsweetened-1l?storeId=10151&amp;langId=44&amp;krypto=X60r%2FL2zWWLqRCfAd5CswLhoK6sdfO8sRgCFsMzbZjPKmNRfYOQVNfyOp8Mw7gvFVnblI0jrShLRXS5iV01PSUJJAqgxOCLW8mpsxGDFW194gW0KrO6xWZ9O9dBSdhiK53orzUm2H0pvNOrwmeta6EyblGwGX7PysS2nFWxXTJs%3D&amp;ddkey=https%3Agb%2Fgroceries%2Fblue-diamond-long-life-almond-milk--unsweetened-1l">Almond breeze</a>, <a href="https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/braham---murray-good-hemp-milk--original-1l?storeId=10151&amp;langId=44&amp;krypto=v0T0i8w2wHU5%2FpOhF%2BPDEMsxH685mnZ0IGtxvhOp6hl1TcDN5C4Tsle%2FV2S6Vwatx4FgH2r8MTEIQNBhbJ4pYrEnv9uPC6VARuqN%2FV3SyeqUQWYiaLcOiUc0HpeF3DBb1%2FQJRRok3KIw2YuGi4nv5l2TpEepov2eiXhgkVLk1Ao%3D&amp;ddkey=https%3Agb%2Fgroceries%2Fbraham---murray-good-hemp-milk--original-1l">Good Hemp</a> and <a href="http://www.hollandandbarrett.com/shop/product/provamel-organic-almond-milk-60007226?skuid=007226&amp;&amp;utm_medium=cpc&amp;gclid=Cj0KEQiAh4fEBRCZhriIjLfArrQBEiQArzzDAQI6Bt7gpiRRwM5PgPWpvopY4Cpjyjf2BWjLlqdXQKEaAsyY8P8HAQ">Provamel</a>.

<strong>“Cheese” please</strong>

A more recent foray into dairy free eating is the world of plant based cheeses. And with the new addition of Sainsbury’s very own <a href="http://www.sainsburys.co.uk/shop/gb/groceries/dairy/dairy-free-cheese#langId=44&amp;storeId=10151&amp;catalogId=10241&amp;categoryId=273761&amp;parent_category_rn=267396&amp;top_category=267396&amp;pageSize=30&amp;orderBy=FAVOURITES_FIRST&amp;searchTerm=&amp;beginIndex=0&amp;hideFilters">“Gary” range</a>, it’s a world we want to live in. Try the caramelised onion “chedder” or the feta-esque “Greek style” for a delicious coconut based cheese board.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8329} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/01/vegan_free_from_sainsburys_cheese1.jpg" alt="" class="wp-image-8329"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

And Sainsbury’s aren’t the only ones with their very own line of free from cheese, try Tesco soya based <a href="http://www.tesco.com/groceries/product/details/?id=277073874">“smoked soya”</a> for an alternative to hard smoked cheese.

Our other favs include <a href="http://www.violifefoods.com/violife-prosociano-with-parmesan-flavour/?gclid=Cj0KEQiAh4fEBRCZhriIjLfArrQBEiQArzzDAfQEdU-DmWLSsdg46xqBf-kUpL3004I4wOMpPJFftvIaAqbd8P8HAQ">Violife’s parmesan</a> and <a href="https://vegusto.co.uk/no-moo-piquant.html">Vegusto’s No-Moo Piquant</a> for incredible pizzas and cheese toasties. And for a simple cashew nut “cream cheese” you can make at home, try <a href="https://www.thefullhelping.com/go-to-cashew-cheese-recipe/">this little gem.</a><strong>Yogurt, butter, ice-cream and more!</strong>

How can you replace your favourites? Well, pretty easily actually.

Try the <a href="https://coconutco.co.uk/">Coconut Collaborative</a>’s selection of dairy free yogurts and desserts. Their <a href="http://www.tesco.com/groceries/product/details/?id=293624597">little chocolate pots</a> are particularly good, as are their ice-creams! Co-yo’s <a href="http://www.tesco.com/groceries/product/details/?id=284210639">raw chocolate yogurt</a> is a close competitor, and their selection of <a href="http://www.waitrose.com/shop/DisplayProductFlyout?productId=481387&amp;source=sho_&amp;utm_source=google%2Bshopping&amp;utm_medium=organic%2Bgs&amp;utm_campaign=google%2Bshopping&amp;gclid=Cj0KEQiAh4fEBRCZhriIjLfArrQBEiQArzzDAWOHpGq9yKGirDzAw5Pec_BV6iDS6v97FeuCaIg51B">coconut milk yoghurts</a> are so versatile and delicious. You can easily swap these in any recipe that requires natural yoghurt.

Perfect alternatives to butter include <a href="http://www.tesco.com/groceries/product/details/?id=253250771">Vitalite’s dairy free spread</a>, <a href="http://www.tesco.com/groceries/product/details/?id=291597297">Flora dairy free</a> and <a href="https://purefreefrom.co.uk/">Pure</a>’s range. Great for cooking, baking and spreading. Own brand options can also be found in Sainsburys, Waitrose and Tesco. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8281} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/01/booja-booja-2.jpg" alt="" class="wp-image-8281"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><a href="http://www.waitrose.com/shop/DisplayProductFlyout?productId=336438">Booja Booja ice-cream</a> is perfect for an indulgent treat. With less pricey but still delicious options including Almond Dream’s <a href="http://www.waitrose.com/shop/HeaderSearchCmd?searchTerm=almond+dream&amp;defaultSearch=GR">mint choc or salted caramel</a> flavours, <a href="http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?catalogId=10241&amp;langId=44&amp;storeId=10151&amp;krypto=m8Da%2BmtVPtFl4vYACSRhlxSRk8QbcXLGsfkQWVNqWulaoluMHrY4HizMuCNxXwDa8SUlGkXN%2Bldj84cGDeLszyRByd7iVhQ3zL57A5L4cHedA0P3i6mHv38HC0Hm">Swedish Glace</a> and <a href="https://www.alpro.com/uk/products/ice-cream?gclid=Cj0KEQiAh4fEBRCZhriIjLfArrQBEiQArzzDAZqehalUaxMmVMk3qjd0nexFy4kwJMEDsZEt6hHDppEaAs2G8P8HAQ">Alpro’s new range</a>. We also hear that our friends over at Fry’s are about to launch some fantastic new frozen treats, so watch this space!

And any card carrying vegan or vegetarian must try Oatly’s new range of <a href="http://www.tesco.com/groceries/product/details/?id=293836605">cream</a>, <a href="http://www.tesco.com/groceries/product/details/?id=293836628">crème fraiche</a> and <a href="http://www.tesco.com/groceries/product/details/?id=293836640">custard</a>. Available at Tesco’s and health food stores across the country, the crème fraiche can be used in sauces, instead of soured cream in Mexican recipes and as a side with pancakes or desserts. Oaty deliciousness. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":8275} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/01/Wow-no-cow.jpg" alt="" class="wp-image-8275"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

We can’t name them all, but look out for Alpro’s <a href="http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?msg=&amp;catalogId=10241&amp;langId=44&amp;storeId=10151&amp;krypto=U7oUdn%2FmmKNqdd0KsRuTInQWiM3g7CI%2BN98j9JWHXUPUHAarIjvMxM8S7PZZyHb1r3LhYMDXPVhHuiE7zsNhwVmBXViAjUrbltZ58zt%2Bcj49PkbdRatoI">custards</a> and <a href="http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?msg=&amp;catalogId=10241&amp;langId=44&amp;storeId=10151&amp;krypto=hJH9NSTdphkt395gb1ZEzw4iCFGS0hArwgYwRxJsPEAZIiQ2M6Tq6pZ5uaE1jrwHY6dKchfvV3ZHtU3lN24HYABF0fJDgYCkDjVMocni95EAf9yy3xISjq%2Fif">creams</a>, <a href="http://www.kokodairyfree.com/news/newproduct_-_koko_dairy_free_yogurts/">Koko’s yoghurt </a>and own brand ranges in the major supermarkets.

With the demand for plant based products on the rise, and big brands catching on, we can’t wait for even more cruelty free options hitting the supermarket shelves this year. Let us know what your favourites are on our <a href="https://www.facebook.com/animalequality">facebook page</a>, or tweet us <a href="https://twitter.com/animalequality">@AnimalEquality</a>

Bon Appetite!</p>
<!-- /wp:paragraph -->