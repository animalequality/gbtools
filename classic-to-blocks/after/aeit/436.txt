<!-- wp:image {"id":1338} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/09/3526196847_6e7a439834_b.jpg" alt="" class="wp-image-1338"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Molte delle crudeltà inflitte agli animali negli allevamenti intensivi derivano dal gran numero di animali allevati in ognuno di essi. Maiali e mucche vivono confinati a migliaia in grandi capannoni mentre i polli a centinaia di migliaia. Queste condizioni di sovraffollamento aumentano lo stress degli animali, abbassano le loro difese immunitarie e consentono alle malattie di diffondersi rapidamente, come la recente epidemia di influenza aviaria ci ha dimostrato. Gli antibiotici che di routine vengono somministrati agli animali, attraverso il mangime e l'acqua, possono fare ben poco per prevenire le malattie; infatti, ora le malattie diventano sempre più virulente e sviluppano una resistenza agli antibiotici pericolosissima sia per la salute umana che per quella animale.

Negli allevamenti intensivi, gli animali vivono quasi tutta la loro vita senza mai sentire il sole sulla pelle o camminare sull'erba. Passano invece mesi e mesi stipati dentro capannoni senza finestre, tra le loro feci e urine, delle quali respirano le esalazioni tossiche. Gli animali meno fortunati - galline ovaiole, scrofe e vitelli trascorrono la maggior parte della loro vita dentro gabbie così piccole che riescono a malapena a muoversi.

<strong>La terribile vita degli animali da allevamento</strong><strong><strong>Galline Ovaiole</strong>
<img class="wp-image-10545" style="width: 800px;" src="/app/uploads/2015/09/5613840726_bec88fe959_o.jpg" alt=""></strong>
Negli allevamenti in batteria di galline ovaiole, ogni gabbia contiene diverse galline, offrendo ad ogni animale uno spazio poco più grande di un iPad. In tale situazione le galline non riescono ad esprimere i propri comportamenti naturali di base, come aprire le ali o spostarsi senza dover salire sopra i corpi degli altri uccelli. Cercano anche di muoversi il meno possibile, perché il reticolo tagliente delle gabbie procura loro innumerevoli ferite alle zampe. Spesso le ali gli rimangono impigliate in qualche punto e quindi, impossibilitate a raggiungere il cibo, muoiono di fame

<strong>Le scrofe riproduttrici</strong>

Le scrofe riproduttrici negli allevamenti intensivi sono condannate a passare quasi tutta la loro vita in gabbie di gestazione &nbsp;poco più grandi del loro corpo, che impediscono loro di stare in piedi, girare su se stesse o addirittura di sdraiarsi completamente. Questi animali, che numerosi studi citano come più intelligenti e socievoli dei cani, provano grandissima sofferenza fisica e psicologica per questa prigionia. Molte scrofe soffrono di ferite aperte non trattate, piaghe da decubito e ascessi. La reclusione provoca loro una profonda depressione e gli animali rosicchiano ripetutamente le sbarre delle gabbie nel vano tentativo di sfuggire a questa condizione di prigionia.

<strong><b>Vitelli
<img class="wp-image-10538" style="width: 800px;" src="/app/uploads/2015/09/4274255385_b1ea730bc6_b.jpg" alt=""></b></strong>

Nel settore lattiero-caseario i vitelli maschi sono indesiderati e inutili. Allontanati dalle loro madri alla nascita, vivono anch’essi imprigionati in condizioni infernali per la totalità della loro breve vita. Non soltanto vivono confinati in minuscole gabbie, ma vengono anche immobilizzati con una catena al collo &nbsp;per impedire loro qualsiasi movimento e fare in modo che la carne si mantenga più tenera per mancanza di muscoli. Molti di loro non riescono neanche a sollevare completamente la testa a causa della brevità della catena.

<strong><b>Come gli allevamenti intensivi risolvono le problematiche legate al sovraffollamento
</b></strong>

La reclusione e il sovraffollamento negli allevamenti intensivi provocano negli animali comportamenti aggressivi non presenti in natura. Tuttavia, invece di fornire agli animali più spazio e arricchimenti ambientali, gli allevatori mutilano gli animali per impedire loro di provocarsi ferite l’uno con l’altro; non per compassione, ma per semplice limitazione delle perdite. Ad esempio, i suini costretti a vivere ammassati gli uni sugli altri possono mordersi la coda a vicenda, provocando infezioni e nei casi più gravi atti di cannibalismo. Cosa fanno gli allevatori per limitare questo rischio? Sostituiscono un male con un altro: ai maialini, pochi giorni dopo la nascita, vengono tagliate le code ed estirpati i denti, il tutto senza anestesia. Allo stesso modo, i becchi morbidi e sensibili dei pulcini vengono tagliati e cauterizzati senza antidolorifici per evitare che gli animali si becchino gli uni con gli altri. Questa operazione viene chiamata “debeccamento” - noi la chiamiamo tortura.

Mentre il pubblico condannerebbe la castrazione di un cane senza anestesia, gli operai degli allevamenti intensivi possono estirpare senza anestesia i testicoli ai suinetti. Perché? Per rimuovere l’intenso odore di ormone sessuale, che conferisce alle carni dei suini maschi interi un gusto sgradevole. Le mucche da latte non se la passano meglio – secondo una pratica arcaica ritenuta inutile nel settore lattiero caseario, gli operai tagliano loro le code ricche di terminazioni nervose, spesso con strumenti non sterilizzati come rudimentali forbici. Gli allevatori bruciano loro anche le corna usando dei ferri roventi, il tutto sempre senza anestesia. Il profitto prima della compassione; questo è il mantra del settore.

<strong><b>Al di là della crudeltà istituzionale
<img class="wp-image-10546" style="width: 800px;" src="/app/uploads/2015/09/6208936482_d25932dc77_b.jpg" alt=""></b></strong>

Incredibilmente, queste procedure strazianti sono considerate prassi normale negli allevamenti intensivi, ma, come se non bastasse, gli animali sono soggetti anche ad altri inimmaginabili abusi. Gli operai impiegati in questi luoghi soffrono spesso di danni psicologici procurati loro dall’ambiente e dalla sofferenza che sono costretti ad infliggere ogni giorno ad altri esseri viventi. I lavoratori diventano insensibili alla miseria degli animali e il risultato inevitabile è la brutalità quotidiana messa in atto e documentata da quasi ogni video sotto copertura girato in un allevamento. Di volta in volta, gli operai sono stati ripresi durante atti di percosse, pugni, calci verso gli animali indifesi; rotture della coda, ferite inflitte con i forconi o sollevamento con carrelli elevatori per costringerli a stare in piedi; animali lanciati in aria o sbattuti a terra, sono pratiche comuni di abuso e uccisione.

Dopo l'uscita dei video girati durante le indagini sotto copertura, che espongono i crudeli abusi ai quali gli animali sono sottoposti negli allevamenti intensivi, le aziende sono pronte a licenziare quelli che definiscono "cattivi lavoratori" ma la verità è ben altra.

Il licenziamento dei lavoratori o un’assemblea interna non possono arginare problemi insiti nel sistema: pratiche standard troppo crudeli e mancanza di vigilanza atta ad individuare e scoraggiare gli abusi sugli animali.

<strong><b>Speranza per il cambiamento
<img class="wp-image-10577" style="width: 800px;" src="/app/uploads/2015/09/Free_Range_Chickens.jpg" alt=""></b></strong>

Fortunatamente, molti dei più grandi attori del settore alimentare hanno preso in considerazione, pena gravi conseguenze economiche, la crescente preoccupazione del pubblico per il benessere degli animali da allevamento e la necessità di trasparenza nella produzione alimentare. Qualche&nbsp;mese fa, Walmart - la più grande società al mondo e la più grande catena di generi alimentari degli Stati Uniti - ha annunciato una politica di benessere degli animali che ridurrà la sofferenza di milioni di polli, maiali e mucche da parte dei suoi fornitori. Anche la Nestlé, la più grande azienda alimentare del mondo, Starbucks e Aramark, hanno recentemente adottato politiche globali di benessere degli animali.

Questi cambiamenti politici importanti conseguiti negli Stati Uniti, molti dei quali provocati dalle indagini e dalle campagne delle associazioni per la difesa degli animali, sono il segno di una svolta nella responsabilità d'impresa - non solo per queste aziende particolari, ma per l'industria alimentare nel suo complesso.

Ma anche quando gli allevamenti intensivi saranno finalmente aboliti, rimarrà la pratica più tragicamente crudele e inutile alla quale sottoponiamo gli animali da allevamento, quella che conclude la loro vita: la macellazione.

Tutti i giorni abbiamo la possibilità di scegliere alimenti di origine vegetale che risparmiano agli animali una vita di sofferenza e la morte.

Considera questa scelta e <a href="https://it.loveveg.com/">inizia dal prossimo pasto</a>!

<strong>&nbsp;</strong></p>
<!-- /wp:paragraph -->