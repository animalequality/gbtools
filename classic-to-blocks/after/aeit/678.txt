<!-- wp:image {"id":2125} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/pellegrini_1.jpg" alt="" class="wp-image-2125"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Gruppo Pellegrini ha <a href="https://www.gruppopellegrini.it/news/l-impegno-di-pellegrini-per-il-benessere-animale/">annunciato</a> che nell’arco di cinque anni smetterà di utilizzare uova provenienti da allevamenti in gabbia. Questa decisione, giunta a seguito del dialogo con il dipartimento di sensibilizzazione aziendale di Animal Equality Italia, avrà un impatto positivo su decine di migliaia di galline ovaiole ogni anno.&nbsp;<img alt="" class="wp-image-2126" src="/app/uploads/2017/05/pellegrini.jpg" style="width: 0px; "></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Quarta azienda del settore, Gruppo Pellegrini conferma così il percorso intrapreso dalle altre grandi realtà di ristorazione collettiva che, non solo in Italia ma anche all’estero, si stanno progressivamente allontanando dalla crudeltà delle gabbie per le galline ovaiole. L’azienda ha deciso di allontanarsi da «un sistema che compromette seriamente il benessere degli animali e che si trova in totale contrasto con i principi di responsabilità ambientale e sociale alla base delle politiche e decisioni aziendali», come si legge sul loro annuncio.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2124} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/pellegrini_0.jpg" alt="" class="wp-image-2124"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Come documentato dalla nostra <a href="https://animalequality.it/news/2017/02/28/il-vero-prezzo-delle-uova-la-nuova-scioccante-investigazione-di-animal-equality-italia/">scioccante investigazione</a> sulla realtà degli allevamenti in gabbia di ovaiole in Italia, le galline rinchiuse in gabbia sono private della quasi totalità dei loro comportamenti naturali, tra cui spiegare completamente le ali. Fratture alle zampe, caduta delle piume e della cresta sono solo alcuni dei problemi di salute comuni in questo tipo di allevamenti, dove le galline sono inoltre sottoposte a fortissimo stress fisico e psicologico.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Così come tante altre aziende del settore alimentare italiano, Pellegrini sta contribuendo a mettere la parola fine alle gabbie per le galline ovaiole. Purtroppo, però, altre compagnie si rifiutano di migliorare le condizioni di vita degli animali negli allevamenti da cui si riforniscono, tra cui Eurospin: se vuoi aiutare a spingere questa azienda a schierarsi dalla parte delle galline, firma la nostra petizione su <a href="https://animalequality.it/news/">www.crudeltaanimale.it/eurospin</a>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 class="wp-block-heading">Nonostante eliminare le gabbie non significhi eliminare la sofferenza, queste politiche aziendali costituiscono un grande primo passo verso la riduzione della sofferenza degli animali. Tuttavia, il modo più veloce ed efficace per aiutare le galline è lasciare le uova fuori dal tuo piatto: <a href="https://www.vegolosi.it/glossario/come-sostituire-le-uova/">scopri</a> come è semplice!<br>
&nbsp;</h2>
<!-- /wp:heading -->