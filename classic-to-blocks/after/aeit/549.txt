<!-- wp:image {"id":1558} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/preview_6.jpg" alt="" class="wp-image-1558"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Ma spetta a noi consegnare l'industria della carne al passato. Abbiamo un sacco di motivi per farlo, ed in questo articolo abbiamo deciso di parlarvi delle motivazioni ambientali.

Tu da che parte vuoi stare? Dalla parte del pianeta e degli ecosistemi, o dalla parte dell'industria della carne e di chi continuerà a farti credere che la nostra dieta non abbia un impatto sul mondo?

Se hai una sensibilità ecologica, prova a prendere in considerazione anche questi 7 punti.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">1. Emissioni di Gas a Effetto Serra</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":1538} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/effetto_serra_industria_carne.jpg" alt="" class="wp-image-1538"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div>Una riduzione del 50% nel consumo di carne e latte nei paesi sviluppati potrebbe portare a un <strong>risparmio del 40%</strong> della perdita complessiva di sostanze nutritive e nell'emissione di gas ad effetto serra.</div>
<!-- /wp:html -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">2. Una scadente gestione delle risorse</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":1545} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/mangimi-animaili_0.jpg" alt="" class="wp-image-1545"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Circa il 35% delle colture di tutto il mondo viene utilizzato per nutrire gli animali che mangiamo, invece di alimentare direttamente con l'umanità.

Il tutto con un rapporto veramente sproporzionato. Lo sapevi che <strong>per produrre 1kg di carne occorrono circa 30kg di grano</strong>?
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">3. Anidride Carbonica</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":1546} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/anidride_carbonica_0.jpg" alt="" class="wp-image-1546"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Entro il 2050, la sola anidride carbonica prodotta dall'industria della carne supererà la soglia limite. Oggi è già un terzo del totale di tutta l'anidride carbonica prodotta. L'allevamento industriale ne è il <strong>primo responsabile</strong>.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">4 Spreco d'acqua</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":1547} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/acqua_2_0.jpg" alt="" class="wp-image-1547"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Produrre <strong>1 kg di carne di pollo</strong> richiede la stessa quantità di acqua che spendiamo per farci <strong>82 docce</strong>.

Nel caso di maiale, 1 kg è equivalente a 115 docce. Infine 1 kg di carne bovina è uguale a 288 docce con acqua.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">5. Deforestazione</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":1542} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/deforestazione.jpg" alt="" class="wp-image-1542"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Indovinate per cosa vengono utilizzati i milioni di tonnellate di soia prodotti ogni anno a spese delle foreste pluviali e della biodiversità?

Si, <a href="http://d2ouvy59p0dg6k.cloudfront.net/downloads/wwf_soy_scorecard_2016_r6.pdf"><strong>secondo un rapporto del WWF</strong></a>, circa il <strong>75% della produzione di soia del mondo</strong> è destinato all'<strong>alimentazione degli animali da allevamento</strong>.

In Europa la percentuale è ancora più alta: si stima che il 93% della soia entra in Europa è destinato ad alimentare gli animali finiranno poi sui nostri piatti.

Molti europei ancora non sanno che in media consumano 61 kg all'anno di soia, a causa del consumo di carne e latte di origine animale, o che questo consumo ha un grave impatto sulle foreste pluviali del Sud America.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">6. La scomparsa delle Falde Acquifere</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":1543} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/inaridimento.jpg" alt="" class="wp-image-1543"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Secondo la FAO, entro il 2025, <strong>due miliardi di persone non avranno più accesso all'acqua</strong>. Fiumi in secca, bacini prosciugati e falde esaurite. La situazione è diventata peggiorare potenzialmente catastrofico nel 2050.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">7. Un modello alimentare completamente&nbsp;insostenibile</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Secondo invece <a href="http://www.futureoffood.ox.ac.uk/news/plant-based-diets-could-save-millions-lives-and-dramatically-cut-greenhouse-gas-emissions"><strong>uno studio condotto dall'Università di Oxford</strong></a>, entro il 2050 la metà dei gas serra che il mondo può permettersi deriverà direttamente dal nostro <strong>attuale modello alimentare</strong>.

Una dieta onnivora equilibrata, secondo le linee guida attuali, ridurrebbe le emissioni del 29%

Con una dieta vegetariana sarebbero ridotte del 63%.

Una dieta vegan infine, le ridurrebbe del 70%.

Vuoi fare del bene al pianeta? <strong>Lascia la carne fuori dal tuo piatto</strong>.

Questa rimane una delle scelte più rivoluzionarie che tu possa fare per affrontare alcuni dei più gravi problemi del nostro tempo.</p>
<!-- /wp:paragraph -->