<!-- wp:html -->
<div>
	Mercoledì 7 maggio la Corte Suprema Indiana ha messo al bando tutte le corse con carri trainati da buoi, il controverso festival “Jallikattu” e tutti gli eventi di intrattenimento sul territorio nazionale che prevedono l’uso di questi animali. La sentenza si basa sulle evidenti violazioni delle leggi nazionali sul benessere animale.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	<br>
	<strong><img alt="" class="wp-image-1244" src="/app/uploads/2014/05/One-driver-gives-electric-shock-while-the-other-squeezing-testicles-of-the-bull_web.jpg" style="width: 409px; border-width: 0px; border-style: solid; margin: 10px 5px; float: right;">La coordinatrice in India di Animal Equality</strong>, <strong>Amruta Ubale</strong>, in collaborazione con alcune associazioni e una squadra di avvocati molto preparata, ha lavorato con impegno negli ultimi anni per porre fine a queste competizioni crudeli e violente.<br>
	<br>
	La svolta, che ha portato a questa decisione storica per la difesa degli animali in India, è stata la <strong>presentazione di quattro casi denunciati nel 2012 per violenze e crudeltà sui buoi usati nelle corse</strong>. Parallelamente le organizzazioni Peta e Animal Walfare India hanno lavorato per denunciare il controverso festival “Jallikattu”, in cui dei tori molto giovani vengono terrorizzati e rincorsi dalla folla che tenta violentemente di afferrarli per la coda e le corna.<br>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	<strong>Nelle parole di Amruta Ubale</strong>, coordinatrice di Animal Equality in India, <strong>il significato di &nbsp;un risultato così importante</strong>:<br>
	"<em>Questo divieto è una pietra miliare per i diritti degli animali in India. Indica che stiamo facendo significativi passi in avanti nella nostra società, e che alcune tradizioni non possono essere considerate più importanti della sofferenza inflitta agli animali </em>".</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	<br>
	&nbsp;</div>
<!-- /wp:html -->