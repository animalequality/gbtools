<!-- wp:image {"id":1241} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/04/5690976031_c796f3f749_z.jpg" alt="" class="wp-image-1241"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Anche quest'anno sarà possibile donare il 5xMILLE ad Animal Equality!&nbsp;A te non costa nulla e per gli animali può fare la differenza.<br>
<br>
Come fare? Basta apporre la propria firma in uno dei 4 riquadri (il primo in alto a sinistra) adibiti alla donazione del 5xMille che compaiono sui modelli di dichiarazione (<strong>CUD,&nbsp; modello 730/1 - Mod. Unico</strong>) ed indicare il <strong>codice fiscale </strong>di Animal Equality che è <em>976 81 66 05 81</em>.<br>
<br>
<strong>Come utilizzeremo il tuo 5xMILLE?</strong><br>
<br>
➥ Ci impegneremo sempre di più a livello nazionale ed internazionale per esporre e rendere sempre più effettive le nostre indagini su chi maltratta e sfrutta gli animali.<br>
<br>
➥ Avvieremo campagne pubbliche ed educative per sensibilizzare l'opinione pubblica e per chiedere a gran voce dei cambiamenti concreti per tutti gli animali.<br>
<br>
Aiutateci a crescere, per gli animali!</p>
<!-- /wp:paragraph -->