<!-- wp:image {"id":1142} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/04/7187802001_8b447ef0c4_c.jpg" alt="" class="wp-image-1142"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>I nostri investigatori, che hanno rischiato in prima persona infiltrandosi negli allevamenti e nei macelli di cani a Jiangmen e Zhanjiang, hanno ottenuto immagini e filmati mai visti prima che rivelano gli orrori perpetrati ogni giorno a migliaia di cani per soddisfare la richiesta di carne in Cina. Il materiale scioccante mostra cani terrorizzati, tenuti in condizioni terribili nei macelli prima di essere brutalmente colpiti con mazze di legno e pugnalati a morte, di fronte agli altri cani vigili e in attesa di subire lo stesso destino.<br>
	<br>
	<em>"Questo video nei macelli di cani è il peggiore, il più spietato e orribile che abbia mai visto in vita mia"</em>. Bernard Rollin, docente di Scienze Animali e Biomediche alla State University of Colorado.<br>
	<br>
	Alcuni dei cani incontrati nei macelli visitati da Animal Equality sono stati probabilmente rubati dalle famiglie in cui vivevano. Secondo un sondaggio realizzato dalla ONG 'Guo' nel maggio del 2011, oltre l'80% delle famiglie di Jinan (Shandong) hanno riferito di aver avuto un cane che poi è stato rubato. Secondo l'ONG questi animali sono probabilmente finiti all'interno dei mattatoi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Attenzione: questo video contiene scene di violenza esplicita</strong><br>
	<iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/DcEJfACChD8" width="640"></iframe>L'investigazione di Animal Equality ha rivelato che la maggior parte dei cani proviene da allevamenti situati nella province di Peixian e in altre provincie settentrionali, dove possono trascorrere anche oltre quattro mesi in piccole gabbie metalliche. Quando gli animali raggiungono il peso desiderato, vengono trasportati nei macelli di Guangzhi, Guangdon e Zhanjiang.<br>
	<br>
	Si stima che ogni giorni circa 30.000 cani vengono uccisi in Cina per la loro carne o per la pelliccia, un commercio che vale più di dieci milioni di euro l'anno.&nbsp; Mangiare carne di cane è particolarmente popolare nel nord est del paese, al confine con la Corea, e anche nelle regioni meridionali, come Guizhou, Guangxi e il Guandong. L'unica zona della Cina dove è vietato il consumo di carne di cane è Hong Kong. Al momento non ci sono leggi a livello nazionale di protezione degli animali in Cina.<br>
	<br>
	Con questa investigazione, presentata contemporaneamente in Gran Bretagna, Italia, Germania, Francia, Messico e India, Animal Equality lancia una campagna internazionale, in collaborazione con le organizzazioni cinesi di protezione degli animali per porre fine al commercio crudele di carne di cane in Cina.</p>
<!-- /wp:paragraph -->