<!-- wp:paragraph -->
<p>Animal Equality rende pubblica un’investigazione sotto copertura all’interno di una delle circa 200 strutture italiane in cui è permessa la macellazione rituale. Gli investigatori hanno avuto l’opportunità di riprendere cosa è accaduto durante le fasi di carico e scarico di centinaia di animali, esponendo successivamente il terribile trattamento riservato durante la macellazione rituale in una delle circa 200 strutture italiane in cui è permessa. <strong>Le immagini raccolte denunciano la violenza inferta nelle fasi del rito ‘halal’ a centinaia di montoni, durante la Festa del Sacrificio o di Aid al Ahda, mostrando il cruento trattamento rivolto agli animali, continuamente percossi, sbattuti contro delle grate, legati e costretti a vedere i propri simili barbaramente uccisi.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div>
	<img alt="Macellazione rituale Halal - macelli.info/halal" class="wp-image-1260" src="/app/uploads/2014/10/halal_rituale.jpg" style="width: 378px; border-width: 0px; border-style: solid; margin: 5px 10px; float: right;">I responsabili delle strutture come quella investigata acquistano gli animali dagli allevamenti, li contrassegnano con un numero progressivo messo sull’orecchio che corrisponde ad un talloncino rilasciato all’acquirente di fede musulmana, che si presenterà poi il giorno della macellazione per ritirare la sua carcassa. Questo tipo di prassi avviene in Italia da alcuni anni, rispondendo all’esigenza di evitare spargimenti di sangue di fatto illegali, poiché tradizionalmente la Festa del Sacrificio viene celebrata collettivamente con l’uccisione diretta dell’animale da parte dei fedeli e il successivo consumo della carne. Pertanto la violenza che si consuma ogni anno, anche in Italia, in un’occasione del genere, viene ‘istituzionalizzata’ e permessa all’interno dei circa 200 macelli che hanno ottenuto la deroga per effettuare macellazioni rituali.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	<strong>Ciò che il nostro team investigativo ha raccolto supera ogni immaginazione e ci porta all’interno di una realtà mai documentata prima in Italia, dove gli animali, in questo caso centinaia di montoni, vengono uccisi barbaramente dopo ripetute violenze</strong> e dove è stato riscontrato quanto segue:</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	• <em>Nel momento in cui è effettuato il carico dai recinti dove sono radunati in attesa del trasporto al macello, gli animali vengono terrorizzati e colpiti violentemente.</em></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	• <em>Per forzarli a salire sul camion, gli animali sono calpestati e presi a calci, in ripetuti casi vengono maneggiati con assurda violenza.</em></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	• <em>I montoni vengono lasciati in un cortile completamente legati per diversi minuti, in preda al terrore e forzati a vedere i simili che vengono sgozzati.</em></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	• <em>Gli operatori del macello gestiscono in maniera difficoltosa gli animali che nel frattempo, terrorizzati, si dimenano in mezzo al sangue e ai resti dei compagni.</em></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	• <em>Diversi montoni aspirano sangue nei loro polmoni durante la macellazione.&nbsp;</em></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	• <em>Risulta evidente dai filmati raccolti che il taglio della gola e dell'esofago lascia gli animali in agonia e dolore per diversi minuti, con una sofferenza inaudita e percepibile a chiunque veda le immagini.</em></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	• <em>Quando vengono effettuate le operazioni della separazione delle pelli realizzata con un compressore, gli animali sono ancora vivi. Il tessuto connettivo che ne previene la separazione è molto innervato, quindi una pressione del genere per separarlo mentre si è ancora vivi crea un intenso e insopportabile dolore fisico. &nbsp;</em></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	&nbsp;</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>
	<strong>Paura, angoscia, sofferenza, dolore: quello che prova un animale durante la macellazione va oltre ogni immaginazione. L’investigazione di Animal&nbsp;Equality dimostra quanta brutalità si cela dietro l’uccisione di ogni singolo individuo. Le immagini rese pubbliche mostrano come l’assenza di stordimento possa amplificare il dolore provato dall’animale e prolungarne, anche di molto, l’agonia.</strong> Questo è permesso da una deroga alle cosiddette leggi sul benessere animale, che invece prevederebbero, appunto, lo stordimento prima della macellazione vera e propria. L’Unione Europea lascia facoltà ad ogni singolo paese membro di decidere se eliminare o meno la possibilità di derogare a quest’obbligo. Così alcuni Stati, come Polonia e Danimarca, hanno già scelto di vietare la macellazione rituale ed è possibile che ciò avvenga anche in Italia. Per quanto un risultato del genere non possa considerarsi neanche lontanamente risolutivo, significherebbe evitare che i piccoli passi in avanti fatti nel percorso per il riconoscimento dei diritti animali siano vanificati da altrettanti passi indietro. <strong>Per questo ti chiediamo di attivarti e di fare la tua parte, diffondendo la campagna “Fermiamo la crudeltà rituale” e firmando la petizione!</strong></div>
<!-- /wp:html -->