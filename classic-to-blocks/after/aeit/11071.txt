<!-- wp:paragraph -->
<p>Siamo orgogliosi di comunicarvi che Animal Equality è stata premiata ancora una volta con il titolo di <strong>Top Charity</strong> e ha ricevuto la migliore raccomandazione possibile da parte di <a href="http://animalcharityevaluators.org"><strong>Animal Charity Evaluators</strong></a> (ACE).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>ACE è un ente indipendente che valuta l’operato delle diverse organizzazioni per i diritti degli animali negli Stati Uniti e in Europa <strong>sulla base della loro efficacia, efficienza ed impatto reale sugli animali</strong> ed anche quest'anno, su un totale di 300 analizzate, ACE ha scelto tre organizzazioni a cui riconoscere il titolo "Top Charity". Per “Top Charity” (letteralmente miglior non profit) ACE intende quelle<strong> tre organizzazioni che in base ai dati, si sono dimostrate più efficienti ed hanno apportato un miglioramento reale e misurabile sulla vita degli animali</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2274,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://campaigns.animalequality.it/io-aiuto/"><img src="/app/uploads/2017/11/Web-site_TW_topcharity2017_0.jpg" alt="" class="wp-image-2274"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
Secondo ACE:</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><em>"Animal Equality ottiene grandi risultati investendo poche risorse economiche, in particolare <strong>realizza indagini sotto copertura ad un costo inferiore rispetto ad altre organizzazioni</strong>. Monitora costantemente i propri successi e fallimenti e definisce in maniera continuativa nuovi obiettivi per i<strong>ncrementare le proprie performance ed aumentare il proprio impatto</strong>. </em></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><em>[...] Animal Equality ha inoltre un forte coordinamento internazionale; l’organizzazione è attiva da diversi anni in molti paesi, ed ha <strong>ottenuto significanti risultati anche nei paesi dove la presenza si è concretizzata più recentemente, ossia in India e Brasile</strong>”. </em></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><em>Puoi leggere l’<a href="https://animalcharityevaluators.org/charity-review/animal-equality/"><strong>intero rapporto ACE su Animal&nbsp;Equality cliccando qui</strong></a>. </em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Siamo contenti che alla luce di queste considerazioni, Animal Charity Evaluators <strong>raccomandi per il quarto anno consecutivo Animal&nbsp;Equality a tutte le persone che vogliono aiutare gli animali</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<h4><a href="https://campaigns.animalequality.it/io-aiuto/"><img alt="" class="wp-image-2273" src="/app/uploads/2017/11/cta_button.jpg" style="width: 500px; "></a></h4>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Ogni donazione che riceviamo ha un impatto diretto sugli animali.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se vuoi fare la differenza nella vita degli animali, questo è il momento migliore dell’anno per farlo, poiché <a href="https://campaigns.animalequality.it/io-aiuto/"><strong>fino al 31 dicembre, ogni donazione ricevuta da Animal&nbsp;Equality sarà raddoppiata</strong></a>, euro su euro, da un generoso donatore.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://campaigns.animalequality.it/io-aiuto/">Supportandoci mensilmente ci permetterai di programmare ed ottimizzare al meglio tutto il nostro lavoro. </a><strong><a href="https://campaigns.animalequality.it/io-aiuto/">Aiutaci ora</a>!</strong></p>
<!-- /wp:paragraph -->