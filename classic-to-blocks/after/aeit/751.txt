<!-- wp:paragraph -->
<p>Ormai è dal 2013 che alcuni consumi di carne sono in costante diminuzione, in particolare quelli di carne di agnello.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Gli italiani insomma sembrano aver sviluppato una maggiore empatia verso questi giovani animali, che però continuano ad essere vittime di grandi sofferenze sia durante il <a href="https://campaigns.animalequality.it/destinazione-macello-italia/">trasporto</a><b> </b>in Italia (quando vengono allevati all’estero), sia durante la <a href="https://animalequality.it/video-macello-agnelli/">macellazione</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ma gli agnelli non sono gli unici cuccioli a cui viene tolta la vita per il consumo della loro carne.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ci sono tanti altri piccoli che vengono strappati alle loro mamme a pochissimi giorni o settimane dalla loro nascita, senza contare quelli che giovanissimi finiscono al macello, come i polli.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Purtroppo infatti è una triste realtà: l’aspettativa di vita di tutti gli animali da reddito è nettamente inferiore rispetto a quella che avrebbero se fossero liberi di vivere al di fuori dei terribili allevamenti intensivi.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><span style="font-size:18px;"><b>I vitelli, vittime dell’industria del latte &nbsp;</b></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L’esempio principale di questo ciclo perverso che comporta la perdita di tantissime vite in tempi molto rapidi sono i neonati maschi nell’industria del latte.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I vitelli sono ritenuti inutili perché non producono reddito, e vengono quindi sottratti alle proprie madri poco dopo il parto per evitare che bevano il latte destinato al mercato.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2410} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/05/33382563746_d769e75546_z.jpg" alt="" class="wp-image-2410"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In Italia ogni anno vengono uccisi <a href="https://animalequality.it/problema/latte/">circa un milione di vitelli</a> a poche settimane dalla nascita, finendo direttamente nel ciclo produttivo della carne. È per questo che anche l’industria dei prodotti lattiero-caseari è direttamente collegata con l’industria di carne bovina e con il destino crudele riservato a questi animali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ma i vitelli, come gli agnelli, non sono i soli ad avere un ciclo di vita molto ridotto a causa dei sistemi di produzione ideati dall’industria.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><span style="font-size:18px;"><b>La triste fine dei giovani maiali&nbsp;</b></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://www.livescience.com/50623-pigs-facts.html">In condizioni naturali</a> i maiali sono in grado di vivere fino a 20 anni.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nell’industria della carne invece vengono fatti ingrassare il più velocemente possibile in modo da raggiungere il peso di macellazione in breve tempo. Ciò permette all’impresa di avere un ritorno economico rapido, ovviamente a discapito della qualità della vita di questi animali.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2411} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/05/4298317109_f120a1d46c_z.jpg" alt="" class="wp-image-2411"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cosa significa questo? Significa che questi animali di oltre 100 kg altro non sono che maialini, uccisi quando hanno appena 6 mesi di vita.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><span style="font-size:18px;"><b>I pulcini maschi e la crudele industria delle uova&nbsp;</b></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://animalequality.it/blog/5-motivi-cui-mangiare-uova-non-e-una-buona-idea/">L’industria delle uova è crudele </a>con i pulcini che hanno la sfortuna di nascere al suo interno.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2412} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/05/22525327724_f1c25cc2e9_z.jpg" alt="" class="wp-image-2412"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per avere un continuo ricambio di galline ovaiole, da sostituire ogni volta a quelle diventate meno produttive, l’industria ha bisogno di far nascere continuamente nuovi pulcini.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Poiché solo le femmine possono deporre uova, i nascituri maschi sono considerati dei veri e propri scarti per l’industria, e quindi vengono uccisi nel modi più economici possibili. Uno dei metodi più comuni per disfarsene è gettarli vivi in un tritacarne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="http://ricerca.repubblica.it/repubblica/archivio/repubblica/2018/01/28/dai-pulcini-ai-capretti-lappello-dei-veterinari-salvate-i-maschi19.html?ref=search">Anche la Federazione Europea dei veterinari ha denunciato queste crudeltà</a>: la fine terribile a cui sono costretti dei piccoli animali è considerata solamente un danno collaterale dall’industria delle uova.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><span style="font-size:18px;"><b>L’industria della carne di pollo, 530 milioni di cuccioli macellati</b></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Poco dopo la schiusa delle uova i pulcini vengono gettati su di un nastro trasportatore che li porta tra le mani degli operai addetti alla vaccinazione. Tale operazione viene condotta in modo molto rapido e brusco, stringendo i pulcini per il collo e schiacciandogli la testa contro l’ago.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
Succede di frequente che alcuni pulcini cadano fuori dal nastro, finendo per morire lentamente di fame o di sete, oppure schiacciati da qualche operatore.<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2413} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/05/40866206654_4691c7da0f_z.jpg" alt="" class="wp-image-2413"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I pulcini sopravvissuti verranno trasportati negli allevamenti intensivi, dove dopo solo 40 giorni di vita andranno incontro al loro triste destino, la macellazione.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://campaigns.animalequality.it/pollo-100-italiano/">Come vi abbiamo già raccontato</a>, in Italia sono circa 530 milioni i polli che ogni anno vengono crudelmente macellati.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Anche in questo caso, quelli che troviamo nei banchi frigo dei supermercati non sono altro che petti e cosce di animali uccisi a pochi mesi di vita.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><span style="font-size:18px;"><b>Che cosa puoi fare tu per aiutarli&nbsp;</b></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Purtroppo l’industria della carne è piena di queste crudeltà, che sono accettate solamente con lo scopo di mantenere i ritmi elevatissimi con cui produce latte, uova e carne.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ma qualcosa si può fare.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Scegli di non mangiare carne e derivati animali, è molto più facile di quanto possa sembrare!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><span style="font-size:22px;"><strong>Vuoi saperne di più?&nbsp;</strong></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter">&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ecco le nostre ultime investigazioni:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Scopri <a href="https://animalequality.it/news/2017/02/28/il-vero-prezzo-delle-uova-la-nuova-scioccante-investigazione-di-animal-equality-italia/">qui</a> tutto quello che l’industria delle uova ti sta nascondendo!</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Continua ad informarti sull’industria della carne di pollo, <a href="https://campaigns.animalequality.it/pollo-100-italiano/">le nostre investigazioni </a>hanno smascherato le atrocità delle pratiche utilizzate negli allevamenti e macelli italiani di polli.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Sai cosa hanno in comune prosciutto crudo, pancetta e cotechino? Sono tutti prodotti derivati dalla sofferenza di milioni di maiali ogni anno.<br><br>	I nostri investigatori sono entrati all’interno di un macello di maiali italiano per mostrarvi <a href="https://animalequality.it/video-macello-maiali/">le terribili pratiche</a> che segnano il destino di questi animali.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Lo sapevi che l’Italia è il secondo produttore mondiale di carne di coniglio, seconda solo alla Cina?<br><br>	Una nostra investigazione in un macello di conigli italiano ha divulgato le tremende immagini di come questi animali vengono uccisi, spesso lasciati completamente coscienti al momento dello sgozzamento.<br><br>	Scopri <a href="https://animalequality.it/video-macello-conigli/">cosa puoi fare tu</a> per i conigli!</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><span style="font-size:22px;"><strong>Vuoi fare ancora di più?</strong></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Conosci i Difensori degli Animali?&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sono un gruppo di web attivisti che combattono per un mondo più giusto anche per gli animali. Tramite semplici azioni, come inviare una e-mail o fare una telefonata, sono in grado di avere un enorme impatto sulle scelte delle aziende e di conseguenza sulle condizioni di vita di milioni di animali allevati.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><span style="font-size:26px;"><strong><a href="https://animalequality.it/difensori-animali/">Diventa oggi un Difensore degli Animali!</a></strong></span></p>
<!-- /wp:paragraph -->