<!-- wp:image {"id":1501} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/05/Veganfood_12001200120200_0.jpg" alt="" class="wp-image-1501"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Nel 2006 erano 150.000, oggi i britannici dai 15 anni in su che hanno scelto un’alimentazione a base vegetale sono <strong>oltre mezzo milione</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A nutrire questa tendenza in crescita sembrano essere i <strong>vantaggi dal punto di vista della salute</strong>. Chi sceglie questo tipo di alimentazione ha generalmente pressione sanguigna e colesterolo più bassi della media, con un minor rischio di tumori e malattie cardiache. Anche la <strong>questione etica e quella ambientale</strong> hanno un peso importante: <strong>sempre più persone scoprono modalità e&nbsp;conseguenze della produzione di derivati animali e decidono di non contribuirvi</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://upload.wikimedia.org/wikipedia/commons/f/f3/Veganfood_12001200120200.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Anche le <strong>celebrità veg</strong> come Ellie Goulding, Jennifer Lopez e Liam Hemsworth sembrano avere una certa influenza. Quasi <strong>metà della popolazione veg inglese ha tra i 15 e i 34 anni</strong> (42%). La stragrande maggioranza proviene dalle <strong>aree urbane</strong> (88%), oltre un quarto (22%) da <strong>Londra</strong>. Il sondaggio rivela inoltre che <strong>gli inglesi che hanno deciso di eliminare carne e pesce</strong> dalla propria alimentazione sono <strong>oltre un milione</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se fino a poco tempo fa scegliere un’alimentazione 100% veg era spesso considerato un estremismo, oggi l’atteggiamento sta senz’altro cambiando. Molti supermercati hanno la propria linea di prodotti vegetali e i principali locali e ristoranti offrono opzioni veg. La catena di ristorazione&nbsp;Pret A Manger aprirà prossimamente un punto vendita senza carne o pesce, forse il primo di una lunga serie.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://upload.wikimedia.org/wikipedia/commons/8/83/Veganz,_Schivelbeiner_Stra%C3%9Fe_34,_Berlin,_June_2012.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Scegliere uno stile alimentare più rispettoso nei confronti degli animali è sempre più facile, per farsi un’idea basta entrare in qualsiasi supermercato e dare un’occhiata a tutti i tipi di latte vegetale che ci sono sugli scaffali. <strong>Con l’aumento delle persone che chiedono alternative vegetali, non potrà che aumentarne anche l’offerta: scegliere veg sarà un’opzione sempre più accessibile</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I consumatori diventano via via più consapevoli della realtà degli allevamenti, come del peso dei prodotti animali sulla nostra salute e sull’ambiente. <strong>Quanti più decideranno di opporvisi, tanti più animali verranno risparmiati da una vita di sofferenze.</strong></p>
<!-- /wp:paragraph -->