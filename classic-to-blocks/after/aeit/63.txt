<!-- wp:image {"id":1048} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/07/2278288057_1a22ccd3c0_b.jpg" alt="" class="wp-image-1048"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Il progetto iniziale, presentato dall'azienda 'Bresaole Pini', una delle maggiori produttrici di carne, prevedeva la costruzione di una struttura dove fossero macellati 12.000 maiali al giorno, 4.380.000 ogni anno.<br>
	<br>
	<img alt="" src="https://farm3.staticflickr.com/2096/2278288057_1a22ccd3c0_b.jpg" style="width: 260px; margin: 10px 5px; float: right;">Dopo mesi di campagna e pressione da parte del Coordinamento Contro il Mega Macello, realtà nata sul luogo per opporsi alla costruzione di questo lager, arriva finalmente una notizia che apre la strada alla speranza. La ASL di Brescia ha bloccato il progetto; il principale motivo della contestazione risiede nell'utilizzo delle risorse idriche, che sarebbero pesantemente coinvolte per soddisfare il fabbisogno della struttura. Tra i rappresentanti delle varie municipalità presenti sono emerse perplessità: c'è chi attende valutazioni più approfondite e chi non è affatto convinto del progetto. Un solo primo cittadino si è espresso favorevolmente basandosi sulla posizione strategica dell'impianto. Unico sindaco che abbraccia il progetto Pini senza riserve è Cesare Meletti, di Manerbio.<br>
	<br>
	Questo è stato il primo di una lunga serie di incontri in cui i vari enti competenti esporranno le loro relazioni in merito all’impianto di macellazione, sicuramente i referti di ASL e ARPA saranno determinanti sulla valutazione finale. Seguiremo quello che accadrà e comunicheremo ciò che sarà stabilito.<br>
	<br>
	Rimane tuttavia fondamentale sottolineare che esiste una motivazione fondamentale che basta da sola a porre fine per sempre alla costruzione del Mega Macello, ed è quella etica: gli animali sono individui da rispettare e per questo la costruzione di un nuovo luogo di sfruttamento non può che incontrare una ferma opposizione.<br>
	&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/YOW401QHht0" width="480"></iframe></p>
<!-- /wp:paragraph -->