<!-- wp:image {"id":1535} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/06/ria_eu_0.jpg" alt="" class="wp-image-1535"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Martedì 21 giugno, <strong>Animal&nbsp;Equality ha visitato il Parlamento europeo a Bruxelles per parlare della terribile condizione dei conigli negli allevamenti</strong>, documentata con le investigazioni condotte in Italia e in Spagna.

Ogni anno, <strong>nell’Unione Europea vengono macellati 326 milioni di conigli. Il 99% di essi vive rinchiuso in minuscole gabbie metalliche</strong>. I maltrattamenti da parte dei lavoratori, le ferite aperte senza cure veterinarie, il cannibalismo e le infezioni non sono eccezioni, bensì la norma.

</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://animalequality.de/sites/default/files/eu_parl1.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Negli ultimi anni, le investigazioni di Animal Equality in Italia e in Spagna hanno rivelato le <strong>pessime condizioni di vita dei conigli nelle aziende europee</strong>. Abbiamo avuto l’opportunità di presentare le nostre osservazioni nell’ambito dell’iniziativa di Compassion in World Farming e della campagna dell’eurodeputato Stefan Eck, che chiedono l’abolizione delle gabbie in batteria per l’allevamento dei conigli.

<strong>Ria Rehberg</strong>, co-direttrice di Animal Equality in Germania, ha lanciato un <strong>appello a tutti gli eurodeputati</strong> presenti:

<em><img style="margin-left: 10px; margin-right: 10px; float: left; width: 500px;" src="https://animalequality.de/sites/all/files/images/ria_eu.jpg" alt="">«Buon pomeriggio, vorrei innanzitutto esprimere il mio più sincero apprezzamento per l'opportunità di parlare qui oggi al Parlamento europeo a favore degli animali. Il mio nome è Ria Rehberg e sono co-direttrice di Animal Equality in Germania. Animal Equality è un'organizzazione internazionale in difesa degli animali d’allevamento e si dedica a proteggerli promuovendo il cambiamento sociale, politico e aziendale verso scelte compassionevoli nei confronti degli animali.&nbsp;</em><em><strong>Negli ultimi anni, il team investigativo di Animal&nbsp;Equality ha esposto l’abuso sistematico ed i maltrattamenti che avvengono negli allevamenti europei di conigli. Più del 60% della produzione di carne di coniglio nell’Unione Europea fa capo a Spagna e Italia</strong>. Animal Equality ha documentato le terribili condizioni in cui vivono i conigli in batteria in entrambi i paesi. <strong>Solo in Spagna abbiamo filmato 70 allevamenti di conigli e 4 macelli. Abbiamo riscontrato prove di maltrattamento in ognuno di questi luoghi.</strong></em><em>I nostri investigatori hanno visto conigli con <strong>ferite aperte, infezioni dolorose agli occhi </strong>e conigli le cui <strong>orecchie erano state morsicate </strong>dai loro compagni di gabbia a causa dello stress del confinamento. Un coniglio di un allevamento europeo ha alte probabilità di vivere in una <strong>gabbia talmente piccola da non potersi nemmeno muovere</strong>. Un coniglio di un allevamento europeo ha <strong>dolori cronici e ulcere alle zampe</strong> a causa del pavimento in fil di ferro. Un coniglio di un&nbsp;allevamento europeo vive in una <strong>gabbia delle dimensioni di un foglio di carta</strong>.</em><em>Date queste condizioni deplorevoli, <strong>fino al 30% di questi animali sensibili muore o viene sacrificato&nbsp;anche prima di raggiungere il macello</strong> - una percentuale superiore a qualsiasi altro animale d’allevamento. Le nostre investigazioni negli allevamenti di conigli mostrano che <strong>gli animali feriti o malati vengono spesso colpiti alla testa con una sbarra di metallo o percossi a morte sul pavimento</strong>. A volte vengono <strong>gettati via ancora vivi</strong>. Quando mostriamo queste immagini alle cariche pubbliche per fare pressione contro gli allevamenti, i cittadini di tutta Europa rimangono indignati. Il popolo europeo si preoccupa per il benessere degli animali e vuole proteggerli da qualsiasi maltrattamento.</em><em>Come dimostrato da un sondaggio, il 94% dei cittadini dell’Unione Europea ritiene che la tutela del benessere degli animali da allevamento sia importante. Per questo oggi <strong>chiedo ai rappresentanti di milioni di cittadini europei di porre fine a uno dei peggiori abusi subiti dagli animali d’allevamento</strong>. Vi chiedo di sostenere l'iniziativa sul divieto delle gabbie in batteria. <strong>Non vi è alcuna differenza tra fare del male ad un cane, a un coniglio o a un gatto. Tutti soffrono in ugual misura.</strong> Ora è il momento di porre fine ad alcuni dei peggiori maltrattamenti subiti da questi animali in Europa.</em><em><strong>Gli animali non hanno voce nella nostra società. È nostra responsabilità dargliene una.</strong> Grazie»</em>

In Italia, le tragiche condizioni di questi teneri animali sono state documentate nell'ambito della <a href="http://www.coraggioconiglio.it">campagna&nbsp;Coraggio Coniglio</a>, portata avanti da Animal Equality e LAV per chiedere che i conigli vengano considerati, come in altri paesi europei, animali d'affezione. Ecco l'ultimo video:
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/sly8b0Ezd9A" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
In Spagna invece, a raccontare la triste vita dei conigli è stato Pablo Puyol, noto attore e cantante che condivide con noi il desiderio di un mondo migliore per gli animali.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/S8mMhApOREc" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<!-- /wp:paragraph -->