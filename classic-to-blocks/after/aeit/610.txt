<!-- wp:image {"id":1873} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI-MAIALE_2_1.jpg" alt="" class="wp-image-1873"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Abbiamo deciso di raccogliere alcuni degli <strong>sguardi</strong> che hanno incrociato i nostri durante questi anni di investigazioni.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sono sguardi che si spiegano da soli,&nbsp;sono occhi che <strong>non hanno bisogno di nessun tipo di interprete</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per questo motivo ci limiteremo semplicemente ad invitarvi a guardare tutti questi animali negli occhi, senza aggiungere nulla perché&nbsp;quasi sicuramente&nbsp;sarebbe superfluo.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se poi dentro di voi sentirete qualcosa, ci auguriamo non abbiate paura di posare il vostro sguardo verso orizzonti nuovi.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1851} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_4_MAIALE.jpg" alt="" class="wp-image-1851"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1857} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_3_MUCCA.jpg" alt="" class="wp-image-1857"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1852} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_15_MAIALI.jpg" alt="" class="wp-image-1852"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1856} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_13_MAIALI.jpg" alt="" class="wp-image-1856"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1853} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_9_MUCCA.jpg" alt="" class="wp-image-1853"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1912} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_8_MAIALE_0.jpg" alt="" class="wp-image-1912"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1855} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_17_ANATRA.jpg" alt="" class="wp-image-1855"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1859} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_1_MUCCA.jpg" alt="" class="wp-image-1859"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1860} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_20_MAIALI.jpg" alt="" class="wp-image-1860"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1858} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_22_CONIGLIO.jpg" alt="" class="wp-image-1858"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1911} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_7_MAIALE_0.jpg" alt="" class="wp-image-1911"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1862} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_10_MUCCA.jpg" alt="" class="wp-image-1862"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1863} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_17_ANATROCCOLI.jpg" alt="" class="wp-image-1863"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1864} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_12_MAIALI.jpg" alt="" class="wp-image-1864"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1865} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_21_CONIGLIO.jpg" alt="" class="wp-image-1865"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1866} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_19_MAIALINO.jpg" alt="" class="wp-image-1866"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1867} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_14_MAIALI.jpg" alt="" class="wp-image-1867"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1868} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_6_MAIALE.jpg" alt="" class="wp-image-1868"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1869} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_16_MAIALI.jpg" alt="" class="wp-image-1869"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1870} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/12/OCCHI_5_MAIALE.jpg" alt="" class="wp-image-1870"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->