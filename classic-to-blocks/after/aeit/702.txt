<!-- wp:image {"id":2215} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/RESIZED-INS-MERCATO_1.jpg" alt="" class="wp-image-2215"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>L’insegna iN’s Mercato del Gruppo Pam inganna i propri consumatori, <strong>comunicando il falso riguardo la propria politica relativa alle uova da allevamento in gabbia.</strong> A seguito della <a href="https://animalequality.it/news/2017/09/28/gruppo-pam-cede-e-dichiara-la-fine-della-vendita-di-uova-di-galline-gabbia/">campagna di Animal Equality</a>, lanciata lo lo scorso 21 giugno al fine di informare i consumatori italiani riguardo al rifiuto da parte dell'azienda di cessare la vendita di uova provenienti da galline allevate in gabbia, in data 22 giugno 2017 l'azienda ha pubblicato il proprio "<a href="https://www.insmercato.it/vivi-ins-post/limpegno-ins-mercato-nei-confronti-degli-animali-eliminare-le-uova-provenienti-allevamenti-galline-gabbia/"><u>impegno nei confronti degli animali</u></a>". Nel comunicato, visionabile sul sito, l’azienda afferma “<em>Le uova presenti nei punti vendita &nbsp;In’s Mercato, provengono già per lo più da allevamenti a terra o biologici, in cui le galline razzolano e si muovono liberamente. Le uova vengono controllate durante tutta la filiera di produzione in modo da garantire al consumatore requisiti igienici e di sicurezza alimentare elevati. Alcune referenze che non rispondono a questo standard sono in fase di smaltimento e non saranno più riassortite</em>” manifestando quindi chiaramente l’intenzione di non approvvigionarsi più di uova di galline allevate in gabbia una volta esaurite le scorte, senza però comunicare una data precisa per l'implementazione di questa politica.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2218} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/RESIZED-INS-MERCATO_3.jpg" alt="" class="wp-image-2218"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Secondo il Regolamento (CE) N. 853/2004 del Parlamento europeo e del Consiglio del 29 aprile 2004, il quale stabilisce norme specifiche in materia di igiene per gli alimenti di origine animale, "Le uova devono essere consegnate al consumatore entro un termine di ventun giorni dalla data di deposizione". <strong>Più di due mesi sono passati dall'emissione del comunicato ufficiale di iN's Mercato e l'azienda continua a vendere uova provenienti da galline allevate in gabbia, dimostrando la falsità dello stesso.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La palese inattendibilità della dichiarazione emessa da iN’s Mercato ha provocato l’indignazione di migliaia di consumatori italiani e dei&nbsp;<a href="https://animalequality.it/news/2017/06/06/6-motivi-diventare-un-difensore-degli-animali/">Difensori degli Animali</a> &nbsp;che, già attivi nel chiedere una politica di abbandono delle suddette uova, hanno&nbsp;prontamente intensificato la pressione sull’azienda con centinaia di azioni come mailbombing e telefonate. Nonostante tutto ciò, e nonostante le migliaia di firme raccolte sulla <a href="https://www.change.org/p/chiediamo-a-gruppo-pam-di-eliminare-le-gabbie-per-le-galline-ovaiole">petizione</a>,&nbsp;&nbsp;l’insegna si dimostra indifferente e tutt'oggi continua ad approvvigionarsi di uova di galline allevate in gabbia e a mantenere sul proprio sito il comunicato falso.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pubblicare una dichiarazione falsa è estremamente grave e, sebbene fino ad oggi l’azienda non si sia ancora dovuta confrontare con le possibili conseguenze legali di questa azione, è chiaro a tutti come un gesto del genere dimostri la poca affidabilità dell’insegna e provochi seri dubbi sulla serietà della stessa.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dopo il recente impegno da parte di Pam Panorama, Pam Local e Pam Franchising a non approvvigionarsi più di uova di galline allevate in gabbia, iN’s Mercato rimane l’unica insegna del Gruppo Pam a non essersi ancora adeguata alla nuova tendenza del settore della grande distribuzione organizzata in Italia.&nbsp;Molte altre grandi aziende fra cui Coop, Lidl, <a href="https://animalequality.it/news/2017/03/30/esselunga-abbandona-le-uova-da-allevamenti-gabbia/">Esselunga</a>, &nbsp;<a href="https://animalequality.it/news/2017/04/18/carrefour-fa-chiarezza-niente-piu-galline-gabbia-tutti-i-punti-vendita/">Carrefour</a> ed <a href="https://animalequality.it/news/2017/04/04/anche-auchan-italia-si-impegna-ad-abbandonare-le-gabbie-le-ovaiole/">Auchan</a>, infatti, hanno già reso pubblico il loro impegno a cessare la vendita di uova provenienti da galline allevate in gabbia, dimostrando la crescente attenzione del settore ai temi riguardanti il benessere degli animali.</p>
<!-- /wp:paragraph -->