<!-- wp:image {"id":1102} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/10/7505365568_556719a7fc_c.jpg" alt="" class="wp-image-1102"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Andrea Zanoni e Nadja Hirsch hanno presentato un'interrogazione alla Commissione europea, in cui è stata menzionata la nostra ultima investigazione: sotto accusa le deroghe che permettono ancora la produzione del foie gras in alcuni Paesi Ue.

<em>"Si tratta di una pratica immonda e barbara, un'autentica tortura di milioni di povere oche ed anatre che vivono indicibili sofferenze quotidiane per permettere il consumo del rinomato fois gras</em> - commenta Zanoni -<em> Il punto è che la maggior parte delle persone che acquista questo prodotto lo fa in buona fede, ignorando il processo che c'è dietro per ottenerlo"</em>.

Il video della conferenza è visionabile a questo <a href="https://www.youtube.com/watch?v=8Yae9sx_hX8&amp;feature=g-user-u"> link</a>.

Ovviamente l'interrogazione alla Commissione Europea ha generato delle polemiche tra i difensori dell'industria del foie gras, gli allevatori e i distributori.
In Bulgaria, il presidente degli allevatori D. Belarechkov (la Bulgaria è il secondo maggior &nbsp;produttore di fegato grasso in EU) ha dichiarato che il bando è improponibile dal momento che gli animali non sono obbligati a rimanere in gabbia.

Le immagini raccolte nella nostra ultima investigazione volta a smascherare l'industria del foie gras, mostrano una realtà scioccante, in cui milioni di anatre e oche sono costrette a trascorrere immerse nell'agonia quotidiana della reclusione e dell'alimentazione forzata. Questi animali, sottoposti continuamente a violenze sia fisiche che psicologiche, vivrebbero 10/15 anni se lasciati liberi in natura; tuttavia,a causa di questa produzione vengono uccisi a soli 4 mesi d'età, ormai distrutti e provati per quello che hanno dovuto subire.

Attualmente il bando di produzione europeo riguarda 22 paesi ad eccezione di Belgio, Bulgaria, Francia, Ungheria e Spagna.

In Inghilterra la polemica si allarga al colosso Fortnum &amp; Mason che vergognosamente paga gli allevatori francesi per poi importare il foie gras in UK.

La crudeltà della pratica dell'alimentazione forzata è&nbsp;una vera e propria tortura, oltraggiosa e inaccettabile, è l'accusa delle numerose associazioni a sostegno del bando totale.

Intanto in Francia anche il mondo dell'arte si unisce alla protesta contro il foie gras e lo fa con un commovente contributo realizzato dall'organizzazione L214. Il corto, realizzato dall'artista David Myriam con la tecnica, rimarca la sofferenza degli animali costretti negli allevamenti e finisce lanciando un messaggio di speranza e libertà.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://player.vimeo.com/video/7291668?badge=0&amp;color=ffffff" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->