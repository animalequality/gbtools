<!-- wp:image {"id":2054} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/rabbit_fb.jpg" alt="" class="wp-image-2054"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Questo risultato storico allevierà notevolmente la sofferenza a cui sono sottoposti &nbsp;<strong>340 milioni di conigli</strong>&nbsp;ogni anno<strong>&nbsp;</strong>negli<strong> allevamenti europei</strong>.

Animal Equality e i suoi sostenitori <strong>hanno esercitato&nbsp;pressione&nbsp;sul Parlamento europeo</strong>&nbsp;tramite l'invio di oltre 120,000 email a tutti gli eurodeputati, invitandoli a votare a favore di questa iniziativa volta a proteggere i conigli d'allevamento. A livello internazionale, volti noti come <strong>Evanna Lynch</strong>, <strong>Victoria Estate</strong>, <strong>Peter Egan</strong> e <strong>Dave Spikey</strong> hanno appoggiato la campagna della nostra organizzazione.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2051} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/rabbit_news.jpg" alt="" class="wp-image-2051"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

In Europa ogni anno vengono macellati circa<strong> 340 milioni di conigli</strong>, la quasi totalità dei quali&nbsp;vengono allevati in <strong>gabbie di batteria</strong>. Un numero spaventoso, soprattutto se si considera che il tasso di mortalità nelle gabbie è estremamente elevato, tra il <strong>15-30%</strong>. Questo dato, superiore a quello di qualsiasi altro animale d'allevamento, è dovuto al fatto che&nbsp;i conigli sono animali molto delicati e dunque particolarmente sensibili ai maltrattamenti a cui sono sottoposti&nbsp;negli allevamenti. Inoltre, nonostante il coniglio sia il secondo animale più allevato in Europa, ad oggi non esistono requisiti minimi obbligatori per la loro protezione.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2052} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/eu_parliament_rabbit_cages.jpg" alt="" class="wp-image-2052"/></figure>
<!-- /wp:image -->

<!-- wp:heading -->
<h2 class="wp-block-heading">«<em>Quella di oggi è una svolta storica. Confinare gli animali in gabbie minuscole per tutta la vita è una&nbsp;</em><i><strong>delle pratiche più crudeli dell'industria alimentare</strong>, ma presto,&nbsp;per i 340 milioni di&nbsp;conigli allevati all'interno dell'Unione Europea, potrebbe essere solo un ricordo»</i></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
ha dichiarato <strong>Matteo Cupi</strong>, direttore esecutivo di Animal Equality Italia, a seguito della votazione.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/N6bmFvAypC0" width="850" height="478" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Le immagini delle investigazioni di Animal Equality&nbsp;svolte all’interno degli allevamenti di conigli in <strong>Spagna</strong> e in <strong>Italia</strong>&nbsp;sono state determinanti per&nbsp;questa storica&nbsp;decisione. Le indagini, realizzate all’interno di <strong>oltre 75 allevamenti</strong>, hanno svelato il dolore e la sofferenza a cui sono costretti&nbsp;i conigli in gabbia: ferite aperte e infette, corpi senza vita&nbsp;lasciati&nbsp;a decomporsi&nbsp;nelle stesse&nbsp;gabbie dei compagni vivi, e episodi di cannibalismo causati dalle condizioni innaturali e di stress.

Con questo voto, il Parlamento dell’Unione Europea ha incaricato la Commissione Agricola&nbsp;di elaborare <strong>norme minime per la protezione dei conigli d'allevamento</strong>. Questo processo richiederà probabilmente diversi mesi in termini di tempo,&nbsp;ma Animal Equality si impegnerà a mantenere alta la pressione&nbsp;sulle istituzioni competenti, fino a che la volontà del Parlamento&nbsp;e&nbsp;dei cittadini diverrà realtà.</p>
<!-- /wp:paragraph -->