<!-- wp:image {"id":2374} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/39301319880_a14fa93acc_m.jpg" alt="" class="wp-image-2374"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Questa storia comincia in Est Europa. È da qui che, in questi giorni, sono partiti molti camion carichi di giovani agnelli destinati al mercato italiano.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La maggior parte di loro proviene dalla Romania ed è collegato all’industria casearia: vivono in luoghi dove il business principale è il latte di pecora e in quanto maschi non sono considerati utili al ciclo produttivo.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per questo nascono già con una destinazione: il macello.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2369} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/20180318-19_Lambs-trailing-March-RO-IT_AWF_Photo-44.jpg" alt="" class="wp-image-2369"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Come se non bastasse però, però, la fine terribile a cui sono destinati non è l’unica crudeltà a cui sono sottoposti questi animali. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per arrivare - a basso prezzo - in Italia, gli agnelli sono sottoposti a viaggi terribili e interminabili, ancora permessi in Europa e poco controllati.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>È per questo che Animal Equality Italia e Animal Welfare Foundation (AWF) hanno deciso di collaborare per mostrare quello che si nasconde dietro a questi trasporti, rilasciando foto e video di quanto documentato durante questi viaggi.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allow="autoplay; encrypted-media" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/DVe-jYgDvyU" width="560"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Anche quest’anno, il team di Animal Equality Italia e AWF ha riscontrato le seguenti violazioni:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li><b>Sovraffollamento</b>: animali stipati e schiacciati, senza alcuna possibilità di riposare o sdraiarsi.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><b>Sistemi di abbeveraggio non funzionanti</b>, che non erano mai stati aperti durante il tragitto e impossibili da raggiungere a causa del sovraffollamento.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><b>Camion non a norma</b>, vecchi e con parti interne rotte e non funzionanti, quindi non idonei al trasporto di agnelli. </li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><b>Agnelli feriti, esanimi, svenuti e schiacciati</b>, che non sono mai stati soccorsi o controllati durante l’intero tragitto.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><b>Lettiera non a norma</b>, sporca e non idonea ad assorbire le deiezioni degli animali.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2371} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/Screen-Shot-2018-03-28-at-16.47.23.png" alt="" class="wp-image-2371"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Durante il trasporto di agnelli in occasione della Pasqua abbiamo notato continue violazioni del regolamento europeo, come un forte sovraffollamento e animali feriti» spiega Matteo Cupi, direttore esecutivo di Animal Equality Italia. «Questi lunghissimi viaggi comportano enormi sofferenze per animali che non dovrebbero nemmeno essere sottoposti alla violenza del macello, ed è per questo che vogliamo far luce su un sistema che non funziona e svelare quello che avviene nell’industria della carne,» conclude Cupi.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2372} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/Screen-Shot-2018-03-28-at-16.46.28.png" alt="" class="wp-image-2372"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Ogni anno, milioni di animali vengono trasportati via mare e via terra per andare al macello, sia all’interno dei confini europei sia dall’Europa verso paesi terzi come la Turchia,» spiega Maria Boada di Animal Welfare Foundation. «Tutto questo comporta gravissime sofferenze per questi giovani animali, ed è per questo che è importante continuare a monitorare questo fenomeno,» conclude Boada.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per fortuna, proprio al Parlamento Europeo qualcosa si sta muovendo,<a href="http://www.efdd-m5seuropa.com/2017/09/una-commissione-dinc-1.html"> con 223 parlamentari europei che hanno chiesto con forza l’introduzione di una commissione di inchiesta sui trasporti di animali vivi in Europa</a>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L’istituzione di questa commissione permetterebbe di capire perché molti paesi accettano ancora tutto questo, perché i controlli vengano fatti in modo sommario e spesso le sanzioni siano sempre troppo basse.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Non c’è ancora una data per il lancio di questa commissione e anzi,<a href="http://www.efdd-m5seuropa.com/2018/03/bocciata-la-commissi.html"> la sua istituzione potrebbe essere a rischio</a>, ma ovviamente ci auguriamo che la pressione crescente di associazioni e cittadini aiuti a fare in modo che anche questo tassello venga messo a posto.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tuttavia, per ora sono i consumatori che possono davvero fare la differenza.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2373} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/Screen-Shot-2018-03-28-at-16.48.07.png" alt="" class="wp-image-2373"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Proprio quest’anno, <a href="https://codacons.it/pasqua-codacons-prezzi-stabili-per-uova-e-colombe-ma-irrompono-prodotti-biologici-e-vegani/">il Codacons ha pubblicato dei nuovi dati sui consumi di carne di agnello</a>, in costante diminuzione: ormai siamo al - 10% rispetto al 2016 e i numeri continuano a scendere.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tutto questo grazie al lavoro di attivisti, sostenitori e persone che ogni giorno scelgono di cambiare il proprio modo di consumare cibo e si orientano verso scelte più etiche e rispettose degli animali.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nel frattempo, vogliamo che si faccia giustizia anche su quello che avviene all’interno dei macelli, destinazione finale di tutti questi animali e luoghi nei quali mancano totalmente controlli trasparenti.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>È per questo che abbiamo lanciato una petizione per chiedere l’introduzione di nuove modifiche di legge, come l’introduzione di telecamere di sorveglianza e l’aumento delle pene previste per i maltrattamenti.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://animalequality.it/video-macello-agnelli/">Sono già più di 70.000 le persone che hanno scelto di sostenerci in questa battaglia, aiutaci anche tu a mettere fine a tutto questo!</a></p>
<!-- /wp:paragraph -->