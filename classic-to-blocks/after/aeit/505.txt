<!-- wp:paragraph -->
<p>Per un’Europa unita dovremmo puntare sul benessere animale. Ciò che emerge dalle ricerche dell’Eurobarometro è una <strong>popolazione europea schierata a favore del benessere degli animali</strong>, un tema che trova supporto in tutti e 28 i paesi interpellati. Ma quali sono le idee del <strong>pubblico italiano</strong> su questo tema?

</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://upload.wikimedia.org/wikipedia/commons/7/70/Gestation_crate_(Farm_Sanctuary).jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Innanzitutto, <strong>l’80% degli italiani vorrebbe avere più informazioni</strong> sulle condizioni degli animali d’allevamento. Sono notizie ottime per <strong>Animal&nbsp;Equality, che si pone proprio quest’obiettivo</strong>. Nei suoi 10 anni di <strong>investigazioni</strong> negli allevamenti e nei mattatoi, la nostra organizzazione ha cercato di <strong>informare quante più persone possibile</strong> sull’orribile trattamento riservato agli animali da reddito.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1445} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/24605500550_67fa4f2118_k.jpg" alt="" class="wp-image-1445"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

I connazionali <strong>i</strong>nsoddisfatti delle condizioni attuali degli animali sono ben l’86% - significa che <strong>la stragrande maggioranza vorrebbe una maggior tutela del benessere animale</strong>.

<strong>Per quasi la metà degli italiani questo tema è “molto importante”</strong> (47%), ma questo dato è più basso della media europea (57%).

Se <strong>i cittadini del nostro paese si dimostrano inclini all’informazione</strong>, lo sono <strong>meno per quanto riguarda l’azione</strong>: poco meno della metà degli italiani (43%) sarebbe disposto a pagare di più per prodotti animal-friendly, mentre il 47% vorrebbe un’etichetta che garantisse un maggior benessere agli animali contro il 52% degli altri paesi dell’Unione Europea.

L’Eurobarometro è lo strumento di ricerca con cui la Commissione europea analizza le tendenze dell’opinione pubblica in tutti gli Stati membri e nei paesi candidati. <strong>Gli orientamenti delle persone sono importanti per preparare proposte legislative in accordo con la popolazione, prendere decisioni e valutare il proprio operato</strong>.

A questo punto è chiaro: per la Commissione europea è arrivato il momento di muoversi. Nel frattempo, <strong>Animal&nbsp;Equality continuerà a fare investigazioni e a dare informazioni, a quanto pare ben gradite.</strong></p>
<!-- /wp:paragraph -->