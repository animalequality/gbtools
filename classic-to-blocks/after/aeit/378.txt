<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"p1"} -->
<p class="p1">Un nuovo emendamento alla legge sul benessere animale in Nuova Zelanda riconosce agli animali il loro stato di “Esseri Senzienti” ciò che chi si prende cura&nbsp;degli&nbsp;'animali domestici' e scienziati ha&nbsp;riconosciuto ormai da anni.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">L'emendamento, che è passato in votazione lo scorso martedì, afferma che gli animali, come gli esseri umani, sono esseri "senzienti".</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"p2"} -->
<p class="p2"><em>"Dire che gli animali sono “senzienti” vuol dire affermare esplicitamente che possono vivere emozioni sia positive che negative, compresi il dolore e l'angoscia"</em>, ha dichiarato la <strong>dott.ssa&nbsp; Virginia Williams, presidente del Comitato Nazionale Animal Ethics Advisory</strong>.</p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"p1"} -->
<p class="p1"><em>"Questo importantissimo riconoscimento segna un altro passo nel cammino del benessere degli animali."</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"p1"} -->
<p class="p1">Il disegno di legge vieta anche l'utilizzo degli animali per la sperimentazione di prodotti cosmetici.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10694} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/shutterstock_134362790.jpg" alt="" class="wp-image-10694"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">La Dott.ssa Williams ha dichiarato inoltre che il riconoscimento giuridico della sensibilità animale è stato determinante per la nuova legge sul benessere degli animali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">Anche la direttrice del Nelson SPCA - l'ente protezione animali neozelandese - Donna Walzl, ha dichiarato che questo cambiamento di ottica è formidabile e il fatto che sia inserito in una legge è un segno di portata epocale.</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"p2"} -->
<p class="p2"><em>"Che gli animali abbiano emozioni simili alle nostre lo vediamo tutti i giorni. Vivono l'ansia da separazione e mostrano sofferenza. E questo è un sentimento quasi umano"</em>, ha detto.</p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"p1"} -->
<p class="p1"><em>"La stessa cosa accade quando vediamo animali trascurati e che hanno problemi di benessere. Soffrono molto per questo. Lo si può vedere nei loro occhi. E' molto triste, davvero."</em></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"p1"} -->
<p class="p1">Questa legge era necessaria perché la sensibilità verso gli animali è aumentata <em>"Purtroppo la maggior parte delle leggi neozelandesi considerava ancora gli animali come "cose" e "oggetti" piuttosto che come creature viventi".</em></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"p1"} -->
<p class="p1"><em>"Speriamo che ora si possano comminare sanzioni più severe per i maltrattamenti e che queste creino un deterrente maggiore contro i comportamenti di alcune persone"</em></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p1"} -->
<p class="p1">Il disegno di legge prevede anche un sistema di classificazione delle offese con conseguenti richieste di pena più o meno severe dando agli ispettori che controllano il benessere animale la possibilità di presentare rapporti di conformità o meno con la legge.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">Il Presidente dell'Associazione Veterinari della Nuova Zelanda, il dottor Steve Merchant, ha dichiarato che il disegno di legge è uno strumento che fornisce maggiore chiarezza, trasparenza e migliore applicabilità delle leggi sul benessere degli animali.</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph {"className":"p2"} -->
<p class="p2"><em>"La percezione del benessere degli animali sta cambiando rapidamente e le pratiche che un tempo erano all'ordine del giorno per gli animali domestici e di allevamento non sono più accettabili o tollerate. Il disegno di legge porta la legislazione in linea con il mutato atteggiamento della nostra nazione sullo stato degli animali nella società."</em></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">L'emendamento alla legge era stata presentato dal ministro dell'Industria Nathan Guy a maggio del 2013.</p>
<!-- /wp:paragraph -->