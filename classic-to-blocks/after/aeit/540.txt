<!-- wp:image {"id":1537} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/ridurre_sostituire_carne_vegan.jpg" alt="" class="wp-image-1537"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Vuoi&nbsp;dare il <strong>tuo contributo</strong> per porre fine agli abusi sugli animali e contemporaneamente fermare il riscaldamento globale?
E magari vuoi farlo mangiando cose deliziose e salutari. Si??

Bene, lo sapevi&nbsp;che una dieta a base vegetale è il modo <strong>più pratico </strong>e <strong>più semplice </strong>per fare tutto questo?
&nbsp;

Oggi ti daremo alcuni consigli per aiutarti a ridurre o sostituire la carne nella tua dieta... e non stiamo scherzando: con ogni nuovo piatto senza carne starai seriamente contribuendo a cambiare il mondo!
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">1. Un passo per volta.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Inizia con piccoli cambiamenti nelle tue abitudini alimentari. Ogni settimana, durante i pasti, cerca di introdurre un <strong>nuovo cambiamento</strong> che non pesi troppo nella tua routine.

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">2. Iniziare con il pollo, per esempio</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Potresti iniziare concentrandoti sulla riduzione o la sostituzione della carne di pollo durante la prima settimana.

Potrai utilizzare alternative saporite come ceci, lenticchie, fagioli, avena, tofu ... Ci sono mille differenti gusti a portata di mano!

Eccoti due meravigliosi siti pieni di consigli, trucchi e ricette per una dieta a base vegetale: <a href="http://www.kitchenbloodykitchen.com/"><strong>Kitchen Bloody Kitchen</strong></a>&nbsp;e <strong><a href="https://www.vegancucinafelice.it/">Vegan Cucina Facile</a></strong>.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">3. Mini auto-promemoria.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Sì, fidati! Dei piccoli promemoria dei cambiamenti che stai introducendo nella tua dieta ti saranno molto utili.
Immaginati&nbsp;di mettere sullo sportello del tuo frigorifero o come sfondo del telefono delle foto dei tuoi animali preferiti o del pianeta Terra visto dallo spazio...
Potrà sembrarti banale, ma fidati: ti aiuterà a <strong>tenere a mente</strong> il motivo per cui hai intrapreso questo percorso di cambiamento!
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">4. Domande?</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Calcio? Ferro? Proteine?
Hai domande su come sostituire certe sostanze e come bilanciarle al meglio?
La <strong><a href="https://www.scienzavegetariana.it/">Società Scientifica di Nutrizione Vegetariana</a>&nbsp;</strong>è un'associazione non-profit fondata nel 2000 e costituita da professionisti, studiosi e ricercatori in diversi settori che ha una risposta per <strong>qualsiasi tua domanda</strong>. &nbsp;

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">5. Lasciati sorprendere!</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Esistono un sacco di<strong> alternative alimentari </strong>alla carne che probabilmente non conosci ed i banchi dei supermercati iniziano ad esserne sempre più pieni!
Fatti un giro fra quegli scaffali che fino ad ora hai evitato e scopri un sacco di nuovi cibi deliziosi, versatili e ricchi di sostanze nutritive...
Di cosa stiamo parlando? Tofu, burger e salsicce vegetali, hummus...
<strong>Il segreto è lasciarsi stupire!</strong></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">6. E per mangiare fuori?</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Oggi ti va di mangiare fuori?
Oppure sei di fretta e vuoi mangiare qualcosa di veloce ma non sai dove recuperare qualcosa senza carne?
Semplice! Scarica l'app <a href="https://www.happycow.net"><strong>HappyCow</strong></a> e magicamente scoprirai i punti di ristoro vegetariani e vegani più vicini a te.&nbsp;
Funziona&nbsp;in qualsiasi parte del mondo!
&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">7. Non sentirti solo!</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Stai prendendo una scelta molto importante per il <strong>pianeta</strong>, per gli <strong>animali</strong> e per la<strong> tua salute</strong>.
Non sentirti solo, ci sono molte persone che hanno già intrapreso questo percorso o ci si stanno approcciando, proprio <strong>come te</strong>!&nbsp;

Prova a conoscere queste persone, ti sentirai parte di un gruppo&nbsp;in costante crescita... e che sta cambiando il mondo grazie alle proprie <strong>piccole scelte quotidiane</strong>.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">8. Cosa stai ottenendo?</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Quello che stai facendo è veramente&nbsp;<strong>incredibile</strong>.
Ogni volta che mangi un piatto senza carne stai:
- salvando l'equivalente di <strong>8 giorni di consumo personale d'acqua</strong>
- risparmiando al pianeta circa <strong>2,5kg di CO2</strong>
- riducendo la <strong>sofferenza degli animali</strong> negli allevamenti intensivi e nei macelli
- riducendo le <strong>emissioni di gas serra</strong> che causano il riscaldamento globale
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">9. A volte è difficile, non fartene una colpa!</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
A volte può veramente risultare difficile trovare un'alternativa vegetale da mettere sul proprio piatto. E' normale, non scoraggiarti e <strong>non sentirti in colpa</strong>.
Sapere che anche un piccolo gesto può avere un grande impatto potrà aiutarti a continuare a fare del tuo meglio per sostenere questo cambiamento.

<strong>Coraggio</strong>!</p>
<!-- /wp:paragraph -->