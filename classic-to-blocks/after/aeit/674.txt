<!-- wp:image {"id":2104} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/04/effetto_serra_industria_carne.jpg" alt="" class="wp-image-2104"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Il nostro pianeta non sta bene e la ragione principale di questo pessimo stato di salute è, ormai è chiaro a tutti, la cattiva condotta dell'uomo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Il paradigma per cui le risorse sono infinite e gli effetti delle nostre azioni su questa terra saranno visibili solo&nbsp;fra centinaia di anni ormai non regge più: sappiamo che stiamo costantemente<strong> distruggendo il nostro habitat </strong>e ne abbiamo la riprova <strong>ogni giorno</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se abbiamo a cuore il futuro del mondo in cui viviamo, è giunto il momento di&nbsp;fare delle <a href="https://animalequality.it"><strong>scelte coraggiose</strong></a> ed invertire la rotta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">C'è però una notizia positiva: la scelta più potente che tu possa fare per invertire questa rotta è a portata di "pasto"!</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Sì, hai letto bene, come scoprirai fra poco con<strong> 4 semplici dati</strong>, la tua dieta è l'arma più potente che hai in mano per <strong>salvare</strong> o uccidere la Terra.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fai la scelta giusta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2105" src="/app/uploads/2017/04/a_biodiversita_0.jpg" style="width: 700px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">L'intero settore dei trasporti produce solo il 13% dei gas ad effetto serra attuali. Agli allevamenti intensivi è imputabile una percentuale pari al 18%. In più, oltre alla CO2 ed al metano, gli allevamenti intensivi sono responsabili&nbsp;del<strong> 65% di tutte le emissioni di ossido di azoto prodotte dall'uomo</strong> - un gas serra con <strong>296 volte</strong> il potenziale di riscaldamento globale dell'anidride carbonica e che rimane nell'atmosfera <strong>per oltre 150 anni</strong>.</h4>
<!-- /wp:heading -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2101" src="/app/uploads/2017/04/acqua.jpg" style="width: 700px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">L'acqua è sinonimo di vita&nbsp;ed è proprio per questo che è un bene così prezioso. Ad oggi l'accesso all'acqua potabile diventa però sempre più difficile vista l'inarrestabile crescita della popolazione mondiale. L'allevamento intensivo è responsabile di una percentuale tra <strong>l'80 ed il 90% del consumo d'acqua mondiale</strong>.</h4>
<!-- /wp:heading -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2102" src="/app/uploads/2017/04/amazonia.jpg" style="width: 700px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Un altro patrimonio ecologico del nostro pianeta sono le foreste. Fra queste, le foreste pluviali sono considerate il vero e proprio polmone della Terra.&nbsp;Ogni secondo, <strong>tagliamo circa 1-2 acri di foresta pluviale</strong> e la causa principale di questa distruzione sono gli allevamenti intensivi: gli alberi vengono tagliati per fare spazio alle colture dei mangimi destinati agli animali.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2114" src="/app/uploads/2017/05/aria_fl.jpg" style="width: 700px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">È stato calcolato che <strong>ogni secondo perdiamo circa 137 specie di piante ed&nbsp;animali </strong>a causa della distruzione della foresta pluviale.&nbsp;Questa cosa rappresenta un problema perché è proprio la biodiversità e la conservazione degli habitat che garantisce la stabilizzazione del clima, l'assesto idrogeologico, il mantenimento delle barriere alla diffusione di agenti patogeni e parassiti, la formazione del suolo, la fotosintesi, il reciclo dei nutrienti ed il mantenimento della qualità dell'acqua.</h4>
<!-- /wp:heading -->