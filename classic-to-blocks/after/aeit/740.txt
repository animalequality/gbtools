<!-- wp:image {"id":2363} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/agenlli_fb.jpg" alt="" class="wp-image-2363"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Arriva una buona notizia per gli agnelli. Secondo l’ultimo report pubblicato da Codacons, calano infatti i consumi di questa tipologia di carne su tutto il territorio italiano, dove <a href="https://codacons.it/oltre-i-numeri-agnello-tradizione-incalo-per-il-10/">si registra un meno 10% rispetto all’anno precedente</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2361} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/19235487311_f2732b6cab_z.jpg" alt="" class="wp-image-2361"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>E si tratta di un trend che, per fortuna, prosegue da anni: nel 2013 si macellavano circa 4 milioni di agnelli, per passare nel <a href="https://animalequality.it/blog/buone-notizie-gli-agnelli/">2016 a poco più di 2 milioni di capi</a>, risultato dei cambiamenti di abitudini di 7 famiglie su 10 che hanno scelto di non includere la carne di agnello nella loro tavola in occasione della Pasqua.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Questa è la dimostrazione più tangibile che sensibilizzando le persone e diffondendo informazioni sulle reali condizioni degli agnelli e di tutti gli altri animali è possibile cambiare le cose.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nel corso di questi anni ci siamo impegnati in prima fila, con la nostra campagna <b>#Salvaunagnello </b>e con tutte le campagne precedenti, per far conoscere <b>la realtà, che spesso è molto più dolorosa e cruenta di quello che tante persone riescono a immaginare</b>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La nostra prima campagna su questo tema è partita nel 2013 con lo slogan "<b>Questa Pasqua Salva un Agnello</b>", accompagnato sui social dall'hashtag <b>#Salvaunagnello</b>, una campagna che va avanti ormai da cinque anni <a href="https://animalequality.it/news/2017/03/30/salva-un-agnello-con-tullio-solenghi/">per <b>contrastare la terribile uccisione di agnelli e capretti che caratterizza purtroppo la festività religiosa della Pasqua</b></a> e che nel 2017 è stata sostenuta dall’attore Tullio Solenghi, da sempre sensibile alla questione animale.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<p><iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="405" src="https://www.youtube.com/embed/Px1qzfz20dM" width="720"></iframe></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Negli ultimi anni, per rafforzare questa campagna e rendere più efficace la risposta dell’opinione pubblica, <b>Animal Equality Italia ha reso noti</b> i risultati sconvolgenti di <b>diverse investigazioni svolte all’interno del mercato della carne di agnello</b>, negli allevamenti e nei mattatoi, compreso il trasporto di milioni di cuccioli che si svolge in un lasso di tempo produttivo molto concentrato.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nel 2017 abbiamo mostrato immagini che sono rimaste impresse nelle menti di migliaia di persone, quelle raccolte all’interno di un macello in provincia di Viterbo.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<p><iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="405" src="https://www.youtube.com/embed/GsUMaPEYS0w" width="720"></iframe></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Qui, grazie all’installazione di telecamere nascoste, abbiamo scoperto che agnelli, pecore e capre venivano sottoposti a torture e violenze di una crudeltà insensata. <a href="https://www.corriere.it/video-articoli/2017/12/06/agnelli-macellati-ancora-coscienti-picchiati-gonfiati-col-compressore-video-denuncia-contro-macello-orrori/596398ba-da9a-11e7-97c8-2b2709c9cc49.shtml">Le immagini, riprese da tantissimi media italiani come Il Corriere della Sera, sono scioccanti</a>: operatori che sgozzano animali senza stordimento, che li gonfiano con un compressore - mentre sono ancora vivi - per separare la pelle dai muscoli e che li lasciano ad agonizzare per minuti interi.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>E non è stata l’unica investigazione e campagna che abbiamo dedicato agli agnelli.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2362} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/18611219673_eafa3c8470_z.jpg" alt="" class="wp-image-2362"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Le immagini e i video arrivati attraverso i media nazionali – e internazionali - nelle case degli italiani, hanno mostrato agnelli strappati alle loro madri al momento della nascita, il loro trasporto che spesso dura migliaia di chilometri, i maltrattamenti subiti durante il tragitto e all’arrivo al mattatoio, la loro macellazione che troppo spesso avviene con gli animali ancora coscienti.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A fronte di tutto questo, abbiamo continuato a lavorare perché sempre più persone smettano di consumare carne di agnelli e cuccioli in occasione della Pasqua e a riflettere sulle proprie abitudini alimentari, riducendo o eliminando il consumo di carne per tutto il resto dell'anno.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ma oltre ai consumi - la parte più importante e fondamentale per portare il cambiamento che vogliamo vedere nel mondo - ci sono anche altre azioni concrete che si possono intraprendere per cambiare la situazione.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<strong><a href="https://animalequality.it/video-macello-agnelli/">Firma la nostra petizione per fermare le crudeltà nei macelli e per chiedere anche tu ai Parlamentari italiani di introdurre riforme più severe per monitorare e sanzionare i reati che avvengono in questi luoghi terribili.&nbsp;</a></strong></p>
<!-- /wp:paragraph -->