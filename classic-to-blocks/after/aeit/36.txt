<!-- wp:image {"id":1021} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/05/14gennaio2012.jpg" alt="" class="wp-image-1021"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>Sabato 14 gennaio 2012</strong> si è tenuta la <strong>presentazione di Animal&nbsp;Equality in Italia</strong>. La presenza di circa un centinaio di persone e lo spirito positivo che ci ha accolto, non possono fare altro che incoraggiare ancora di più il lavoro che porteremo avanti nel nostro paese, per ogni individuo sfruttato e dimenticato.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" src="https://farm8.staticflickr.com/7043/6947592359_51d711f45f_c.jpg" style="width: 240px; margin: 5px 10px; float: left;">Durante la serata i fondatori di Animal Equality in Spagna e le persone responsabili del gruppo del Regno Unito, hanno presentato il lavoro svolto dall'organizzazione in questi anni; foto e video ci hanno accompagnato tra azioni dimostrative, proteste e investigazioni che hanno permesso di sensibilizzare l'opinione pubblica sullo specismo e sull'importanza della scelta vegan.<br>
	I coordinatori del gruppo italiano hanno quindi introdotto alcuni futuri progetti che verranno realizzati.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
	A seguire, è stato possibile quindi degustare del cibo vegan con diversi piatti inclusi alcuni prodotti di www.Vegusto.it.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><object height="300" width="400"><param name="flashvars" value="offsite=true&amp;lang=it-it&amp;page_show_url=%2Fphotos%2Fanimalequalityitalia%2Fsets%2F72157629498700089%2Fshow%2F&amp;page_show_back_url=%2Fphotos%2Fanimalequalityitalia%2Fsets%2F72157629498700089%2F&amp;set_id=72157629498700089&amp;jump_to="><param name="movie" value="https://www.flickr.com/apps/slideshow/show.swf?v=109615"><param name="allowFullScreen" value="true"><embed allowfullscreen="true" flashvars="offsite=true&amp;lang=it-it&amp;page_show_url=%2Fphotos%2Fanimalequalityitalia%2Fsets%2F72157629498700089%2Fshow%2F&amp;page_show_back_url=%2Fphotos%2Fanimalequalityitalia%2Fsets%2F72157629498700089%2F&amp;set_id=72157629498700089&amp;jump_to=" height="300" src="https://www.flickr.com/apps/slideshow/show.swf?v=109615" type="application/x-shockwave-flash" width="400"></object></p>
<!-- /wp:paragraph -->