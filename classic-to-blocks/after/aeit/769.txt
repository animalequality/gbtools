<!-- wp:paragraph -->
<p>Sembrava non dovesse accadere mai. Sembrava che questa campagna, iniziata nel marzo del 2016, non avrebbe mai avuto un epilogo. Invece è finalmente successo: <a href="https://www.facebook.com/EurospinItaliaSpa/photos/a.435103389841536/2045768302108362/?type=3&amp;theater">Eurospin ha finalmente reso pubblico sulla&nbsp;pagina Facebook -&nbsp;attraverso un post dedicato proprio alle uova -&nbsp;il proprio impegno a non vendere uova di galline allevate in gabbia</a>, dichiarando che da mesi questo genere di uova non è più disponibile nei loro punti vendita e unendosi alle altre grandi aziende del settore della grande distribuzione organizzata in Italia che hanno già preso la decisione di distanziarsi dal commercio di uova provenienti da galline allevate in gabbia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La campagna per chiedere ad Eurospin di fare questo gesto è stata lunga ed intensa, e per mesi ha raccolto l’adesione ed il supporto di decine di migliaia di persone che si sono mobilitate in tutta Italia realizzando innumerevoli azioni di protesta che si sono susseguite in modo incessante e costante per mesi. Si è trattato di una campagna molto complicata, i cui sforzi si sono visti contrastati dalla totale chiusura dell’azienda che ha per mesi ignorato le richieste dei consumatori di eliminare le uova di galline allevate in gabbia dai propri scaffali, e che, una volta eliminatele, ha per mesi mostrato la propria reticenza a rendere pubblica la propria posizione nonostante moltissime altre importanti aziende del settore lo avessero già fatto.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2481} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/11/36306523515_3e4337cfef_z.jpg" alt="" class="wp-image-2481"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La campagna rivolta ad Eurospin è stata la prima campagna mai realizzata dal dipartimento di sensibilizzazione aziendale di Animal Equality; grazie allo straordinario sforzo di tutte le persone che hanno partecipato, l’iniziativa ha ricevuto una straordinaria copertura mediatica, arrivando a milioni di persone in tutta italia. La forza e la costanza con cui migliaia di consumatori si sono pronunciati sono stati meritevoli di nota, ma un ringraziamento davvero speciale va alla squadra dei Difensori degli Animali, i quali hanno combattuto con coraggio, determinazione ed assoluta dedicazione per ottenere un risultato che per mesi sembrava impossibile da raggiungere. È a tutti loro che vogliamo dedicare questa straordinaria vittoria, che avrà un impatto su 400.000 galline ovaiole.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter">&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><a href="https://animalequality.it/difensori-animali/"><span style="font-size:24px;">UNISCITI ANCHE TU ALLA SQUADRA DEI DIFENSORI DEGLI ANIMALI</span></a></p>
<!-- /wp:paragraph -->