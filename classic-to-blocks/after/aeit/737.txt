<!-- wp:image {"id":2351} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/MAIALI_12.jpg" alt="" class="wp-image-2351"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Maiali deformati, costretti a vivere con cisti e ascessi giganti. Abbandonati alle sofferenze più terribili in nome della produzione di “pregiati” prosciutti spagnoli. Tutto questo è emerso dopo il lancio della nostra <a href="https://animalequality.it/news/2018/02/16/la-nostra-campagna-contro-lallevamento-degli-orrori-di-elpozo-non-si-ferma-qui/">campagna contro El Pozo, l’azienda spagnola accusata di maltrattamenti e abusi sui maiali</a> allevati per la produzione di carne. Ma purtroppo quello spagnolo non è un caso isolato. L’industria della carne è piena di storie come questa, eppure sono in tanti a sostenere il contrario. Ogni volta la risposta è sempre la stessa: “Si tratta di un solo allevatore che non rispetta le regole su migliaia di produttori.”

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>

<img class="wp-image-2350" style="width: 640px;" src="/app/uploads/2018/03/MAILALI_3.jpg" alt="">

</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Tutto questo non è vero.

L’industria della carne nasconde da sempre molti segreti.

<a href="https://theecologist.org/2018/feb/20/spanish-meat-scandal-making-waves-britain-no-isolated-incident">La prima persona ad aver rivelato che cosa si nasconde dietro alle porte dei macelli è stato Upton Sinclair, nel libro, diventato poi un classico, “The Jungle”</a>, pubblicato nel 1906 dopo settimane di lavoro sotto copertura nei macelli di Chicago. Grazie alla sua testimonianza, il pubblico americano scoprì che nell’industria della carne gli operai immigrati erano costretti a lavorare in condizioni di schiavitù e che la situazione sanitaria era talmente precaria da mettere a serio rischio la sicurezza alimentare americana. Già all’epoca, nessuno voleva pubblicare questo libro. Ma nonostante abbiano cercato di mettere a tacere la potenza della voce di Sinclair, il suo messaggio arrivò ai lettori, <a href="http://www.seesharppress.com/books1.html">e arriva ancora oggi grazie al lavoro di See Sharp Press</a>, casa editrice anarchica e indipendente americana.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Sono passati tantissimi anni da quella prima coraggiosa investigazione, eppure ci sono ancora gli stessi problemi. Com’è possibile? Ogni volta che esce un’investigazione, una testimonianza dai macelli e dagli allevamenti, l’industria risponde sempre allo stesso modo: sono solo casi isolati.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Ma noi sappiamo che è l’intero sistema a non funzionare. Basta guardarsi intorno e scavare nella memoria.

Nel 2012, Animal Equality svelò che in un allevamento di maiali in Spagna, nella zona della Murcia, scrofe incinte <a href="https://www.youtube.com/watch?v=GNhJ3eyz2Zc">erano costrette a subire torture e sevizie da parte di operatori che - tra le risate generali - praticavano a turno veri e propri atti di sadismo sugli animali indifesi</a>. Era il cosiddetto “caso El Escobar”.

Sono passati sei anni, e<strong> i colpevoli sono stati condannati a un anno di carcere tre anni di interdizione dal lavoro con animali</strong>. In quel caso, i colpevoli sono stati riconosciuti e arrestati, ma anche nel nostro paese l’industria dell’allevamento di maiali nasconde un volto a dir poco mostruoso.

<a href="https://animalequality.it/video-macello-maiali/">Due mesi fa abbiamo mostrato delle immagini sconvolgenti, che raccontano bene quello che ancora avviene all’interno dei macelli</a> italiani, in questo caso in una località della Lombardia. Nel video, ottenuto grazie al leak di un operatore, dopo un primo (inefficace) stordimento, si vede chiaramente come un maiale venga sgozzato e lasciato agonizzare a terra per vari minuti nell’indifferenza generale.

Non si tratta di un caso isolato. Sono più di otto milioni i maiali che vengono allevati e macellati nel nostro paese, e quell’immagine di sofferenza racconta alla perfezione tutto quello che devono subire. Ma, oltre alle immagini, ci sono molti dati concreti che non riguardano solamente i prosciutti pregiati venduti da ElPozo in Italia su Amazon, ma anche i prodotti nostrani.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>

<img class="wp-image-2346" style="width: 640px;" src="/app/uploads/2018/03/FOTO-UNO.jpg" alt="">

</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><a href="https://animalequality.it/news/2015/05/23/no-carne-linchiesta-choc-sugli-allevamenti-intensivi/">Nel 2015 - per la prima volta in Italia - abbiamo svelato quello che si nasconde dietro alla produzione dei prosciutti italiani, </a>dopo mesi di lavoro investigativo in Nord Italia in collaborazione con la giornalista Giulia Innocenzi e la squadra di giornalisti di Announo.

<a href="https://www.youtube.com/watch?v=NupGMf0z_Vs">I nostri investigatori scoprirono anche in quel caso una situazione agghiacciante, rivelando le condizioni di estrema gravità</a> in cui versa un settore che vanta di produrre carne d’eccellenza e di altissima qualità, come il Prosciutto di Parma. Nel video infatti si vede chiaramente che in alcuni allevamenti si trattava di maiali cresciuti proprio all’interno della filiera del consorzio e marchiati con i simboli che indicano la destinazione per la produzione di prosciutto certificato.

Negli allevamenti di Brescia, Mantova e Modena e nei macelli visitati dai nostri investigatori abbiamo trovato condizioni di sporcizia estrema, infestazioni di scarafaggi e topi e animali coperti da cisti, pustole e con prolassi anali gravissimi.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>

<img class="wp-image-2349" style="width: 640px;" src="/app/uploads/2018/03/17310660864_865bed5f48_z.jpg" alt="">

</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Nelle zone di ingrasso, il cibo con cui venivano nutriti i maiali era mischiato con urine e feci, che venivano lasciate nelle stesse vasche del mangime, mentre <a href="https://animalequality.it/news/2015/05/25/indagine-shock-di-animal-equality-dopo-la-puntata-di-announo/">nelle zone di gestazione le scrofe venivano costrette a passare tutta la gravidanza e l’allattamento in piedi, così provate dal loro stesso peso che le zampe anteriori e posteriori cedevano in continuazione.</a> Per non parlare delle condizioni dei piccoli, schiacciati uno sull’altro come spazzatura, abbandonati senza cure e, quando morti per gli stenti e la fatica, gettati nei secchi e lasciati per giorni a marcire.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>

<img class="wp-image-2347" style="width: 640px;" src="/app/uploads/2018/03/FOTO-TRE.jpg" alt="">

</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Nel Nord Italia viene allevato l’80 per cento dei maiali italiani e queste sono le condizioni in cui sono costretti a vivere negli oltre 100.000 allevamenti dislocati sul territorio. È per tutti questi motivi che abbiamo scritto delle richieste molto precise rivolte ai ministri della Salute e dell’Agricoltura.

È necessario che anche l’Italia come primo passo rispetti le norme europee, fornendo a questi animali un ambiente migliore, dove ricevano stimoli e possano esprimere i propri comportamenti naturali, ma soprattutto un contesto sano e sicuro.

<b>Ogni scelta del consumatore può essere fondamentale per aiutare Animal Equality</b> a fermare tutto questo e a salvare più animali possibile. Per questo motivo il primo passo è smettere di consumare carne di maiale. Ma non è l’unico. La nostra petizione sui macelli chiede anche l’introduzione di un sistema telecamere a circuito chiuso e una serie di misure che garantiscano una vita migliore a questi animali.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<h4><a href="https://animalequality.it/video-macello-maiali/"><strong>FIRMA ORA LA PETIZIONE!</strong></a></h4>
</center>
<!-- /wp:html -->