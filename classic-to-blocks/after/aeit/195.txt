<!-- wp:paragraph -->
<p>Dopo la carne di cavallo un nuovo scandalo colpisce l'opinione pubblica. La stampa internazionale e nazionale ha riportato recenti dichiarazioni rilasciate da ex dipendenti del gruppo <strong>Euralis</strong> (Primo produttore al mondo di foie gras) che riferirebbero dell'<strong>uso di farmaci antibiotici per ingrassare gli animali </strong>(una pratica vietata dalla normative vigenti) della presenza costante di molti animali malati negli allevamenti di Lescar (Sud della Francia).

Ma cosa accade veramente a questi animali all'interno degli allevamenti? <strong>Animal&nbsp;Equality ha attentamente documentato,</strong><a href="https://animalequality.it/blog/new-york-vieta-il-foie-gras/"> attraverso un'indagine sotto copertura, la vita delle anatre ed oche in diversi allevamenti del Sud-Est della Francia e della Spagna </a>.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1157} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/05/foie-gras.jpg" alt="" class="wp-image-1157"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em>“Abbiamo raccolto video e foto con scene terribili di <strong>animali confinati in minuscole gabbie</strong>, affetti da <strong>stress e depressione</strong>, <strong>feriti</strong> dal tubo che ogni giorno gli viene spinto nell'esofago per far passare il cibo, oppressi da <strong>problemi respiratori e di deambulazione</strong> per le abnormi dimensioni raggiunte dal fegato, maltrattati e <strong>lasciati morire senza cure. </strong>Ciò che viene riportato oggi dalla stampa francese ed internazionale sull'ultimo scandalo riguardante il foie gras non dovrebbe sorprendere affatto”</em>, afferma Francesca Testi, portavoce di Animal Equality in Italia.

Quest'ultimo grave fatto sta provocando scalpore e riporta ancora una volta l'attenzione su una pratica oramai definita crudele in molte parti del mondo. Diverse nazioni hanno infatti già vietato la produzione di foie gras per l'evidente crudeltà; tra questi l'<strong>Italia</strong>, dove <strong>dal 2007</strong> è stato <strong>bandito l'allevamento</strong> con un decreto legislativo che ha definito l'<strong>alimentazione forzata come una 'tortura'</strong>.

<em>“Tuttavia la distribuzione in Italia continua ancora oggi, ponendo il nostro paese in una posizione di incoerenza con quanto affermato nel decreto legislativo”</em> , afferma sempre Francesca Testi.&nbsp; Ed è per questo motivo che da settembre dello scorso anno è stata lanciata da Animal Equality, a seguito dell'indagine,&nbsp; una<strong> campagna di protesta</strong> per chiedere a diverse catene di supermercati (<strong>Auchan, Bennet, Conad, Esselunga, Sma, Super Elite</strong>) di rivedere le scelte aziendali in merito alla distribuzione di foie gras sul territorio nazionale, fornendo dichiarazioni pubbliche ed ufficiali che annuncino la cessazione immediata della vendita di un prodotto ottenuto con così tanta violenza sugli animali.

<strong>Come primo importantissimo risultato</strong>, alla fine dello scorso ottobre, <strong>la COOP</strong>, prima catena di distribuzione in Italia, <strong>ha comunicato ad Animal&nbsp;Equality di cessare la vendita di foie gras sospendendo</strong><strong>gli ordini ed andando fino ad esaurimento delle scorte presenti nei magazzini.&nbsp; Con i suoi 1470 punti vendita e circa 7 milioni di consumatori la COOP ha sicuramente fatto un passo in avanti decidendo di non distribuire più il 'fegato grasso' di anatre ed oche. Animal&nbsp;Equality auspica che anche le altre grandi catene di supermercati facciano lo stesso nel più breve tempo possibile, perché finalmente la terribile produzione di foie gras possa essere completamente bandita sia a livello nazionale che internazionale.</strong>

In Europa il 'fegato grasso' proveniente da anatre ed oche è ancora prodotto legalmente solo in 5 paesi: <strong>Francia, Bulgaria, Spagna, Ungheria e Belgio</strong>. La Francia è il maggior produttore ed esportatore, ogni anno sono infatti prodotte oltre 800.000 tonnellate; circa <strong>700.000 oche e 37 milioni di anatre vengono macellate</strong> per questo prodotto.</p>
<!-- /wp:paragraph -->