<!-- wp:paragraph -->
<p>A <strong>6 anni</strong> ha saputo che gli animali venivano uccisi per finire sui nostri piatti e che il latte delle mucche era in realtà per i vitelli. Genesis non era d’accordo e quindi <strong>ha deciso di sua spontanea volontà&nbsp;di fare a meno dei prodotti di origine animale</strong>, ovviamente con il supporto della sua famiglia.

La giovanissima attivista ha le idee molto chiare e non ci ha pensato un attimo a metterle in pratica: <strong>prima di compiere 9 anni aveva già fatto dimostrazioni fuori dai circhi, interpellato le autorità locali e detto la sua su vari canali d’informazione</strong>.

La sua passione l’ha portata a <strong>fondare la non profit “Genesis for Animals”</strong>, per sensibilizzare la popolazione sulle crudeltà subite dagli animali e fare informazione su alternative più salutari e cruelty-free. <em>“Per me gli animali sono come fratelli e sorelle. Nessuno mangerebbe i propri fratelli o le proprie sorelle, ecco perché non mangio gli animali”</em> ha detto al canale televisivo KCET.

L’anno scorso ha fatto <strong>pressione sul consiglio comunale di Long Beach perché si impegnasse a incoraggiare il <em>Meatless Monday</em></strong> (“lunedì senza carne”). Secondo <a href="http://www.huffingtonpost.com/kathy-freston/the-breathtaking-effects_b_181716.html">l’Huffington Post</a>, se <strong>ogni abitante degli Stati Uniti evitasse la carne per un solo giorno</strong> si risparmierebbero quasi <strong>4 miliardi di ettolitri d’acqua</strong>&nbsp;e <strong>più di un milione di ettari di terra</strong>. Inoltre, si eviterebbe la produzione di 4,5 milioni di tonnellate di rifiuti di origine animale.

Oltre a mettere un incredibile impegno nella sensibilizzazione delle persone, <strong>Genesis lavora nel rifugio "Farm Sanctuary"</strong> di Acton, California.
La bambina <strong>spera di essere di esempio e di contribuire ad un futuro migliore per ogni essere vivente</strong>.

Alexandra Caswell, proprietaria di Farm Sanctuary,&nbsp; ha dichiarato: <em>“Se si insegna ai bambini a comportarsi con&nbsp;compassione, loro spesso influenzano la propria famiglia a vivere allo stesso modo”</em>.

La storia di Genesis ci riempie speranza e ci insegna che <strong>tutti possono fare la differenza per un mondo migliore, a prescindere dall’età</strong>.</p>
<!-- /wp:paragraph -->