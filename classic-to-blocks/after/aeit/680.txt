<!-- wp:image {"id":2136} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/con_0_0.jpg" alt="" class="wp-image-2136"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>In quel momento ho deciso di cambiare e di fare qualcosa in prima persona. &nbsp;<strong>Sei mesi dopo partivo per la mia prima investigazione sotto copertura</strong>. &nbsp;Credevo di essere pronto. Ho scoperto col tempo che non si è mai pronti.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Questi sono alcuni appunti presi durante quella prima investigazione.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2132} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/con_0.jpg" alt="" class="wp-image-2132"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">23/03/2015</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Non possono sapere chi sono,&nbsp;mi dicevo oggi per tranquillizzarmi, è impossibile.&nbsp;Ma il veterinario mi guardava insospettito. Devo rimanere calmo per<strong> non farmi scoprire</strong>. La riuscita della missione dipende anche dal mio stato d’animo. Il padrone dello stabilimento mi si è avvicinato per chiedermi lentamente: «Non sei mica uno di quegli animalisti?». «Io animalista? No, assolutamente», gli ho risposto d’istinto con una risata. «D’accordo. Seguimi, ti mostro qualcosa».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Il proprietario della struttura mi ha spiegato minuziosamente il sistema delle gabbie. Poi siamo entrati nella stalla. Davanti a me c’erano centinaia di gabbie di ferro, una sopra l’altra con una dozzina di conigli ammassati in ognuna. «Questo sembra molto malato», gli ho detto puntando il dito verso un piccolo coniglio bianco che stava tremando a terra, incapace di alzarsi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2133} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/con_2.jpg" alt="" class="wp-image-2133"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Questo non si riprenderà più», mi ha risposto. Poi ha aperto la gabbia, ha preso il coniglio per le orecchie e<strong> lo ha scaraventato a terra con tutta la sua forza</strong>. Il sangue ha&nbsp;macchiato tutto il pavimento. Le piccole zampe si sono mosse ancora qualche secondo come se il coniglio volesse scappare, poi però è rimasto&nbsp;immobile, morto. Mi ha preso un nodo in gola e non potevo più respirare. Ero spaventato. “Cosa ha fatto?» ho balbettato. «Non sarebbe vissuto a lungo»,&nbsp;mi ha risposto bruscamente,&nbsp;«funziona così».&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Avrei voluto prenderlo per il collo, scuoterlo e urlargli «Come puoi essere così spietato?». Per la prima volta mi scontravo con una delle parti più difficili del mio lavoro: lasciare le mie emozioni fuori, metterle in pausa, reprimerle in favore di un obiettivo più grande. Non potevo farmi scoprire.&nbsp;Il mio compito è documentare scene come questa, renderle pubbliche e in questo modo contribuire affinché questa crudeltà abbia fine.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Discutere con il proprietario dell'allevamento, in quel momento, mi avrebbe messo in pericolo e, cosa ancora più importante, non avrebbe di sicuro aiutato gli animali.&nbsp;</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":2134} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/05/con_1.jpg" alt="" class="wp-image-2134"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Quel che ho vissuto in quell’allevamento mi fa venire gli incubi ancora oggi.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Ho lavorato due anni nell’industria della carne di coniglio. Insieme al mio team ho visitato 70 allevamenti. In ognuno ho vissuto delle scene inimmaginabili di sofferenza e di una violenza. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Quando abbiamo pubblicato le foto e i video moltissime televisioni e giornali ne hanno parlato, portando alla luce le condizioni in cui i conigli vivono ogni giorno.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Grazie a quelle immagini abbiamo portato la discussione al Parlamento europeo e abbiamo vinto: è stato il primo passo per eliminare le gabbie nell'Unione Europea. Un passo avanti concreto che avrà ripercussioni sulla vita di milioni di conigli.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Per risparmiare la vita degli animali negli allevamenti intensivi dobbiamo continuare con le nostre inchieste. Finché l’ultima gabbia sarà vuota.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Ti prego quindi di sostenere Animal Equality con una <a href="http://sostenitore.animalequality.it/#section4"><strong>donazione regolare</strong></a>. Con un piccolo contributo mensile ci dai la possibilità di pubblicare altre video-inchieste e di salvare tante vite animali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Alla prossima,</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Marco</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><a href="http://sostenitore.animalequality.it/#section4"><img alt="" class="wp-image-2135" src="/app/uploads/2017/05/button.png"></a></figure><p></p>
</center>
<!-- /wp:html -->