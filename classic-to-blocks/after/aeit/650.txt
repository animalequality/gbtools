<!-- wp:paragraph -->
<p><strong>General Mills</strong>, una delle più grandi aziende produttrici del settore alimentare a livello globale, <a href="https://www.generalmills.com/en/News/Issues/animal-welfare-policy">annuncia</a> che <strong>entro il 2025</strong> smetterà di utilizzare uova provenienti da allevamenti in <strong>gabbia</strong>. Questa decisione include tutti i prodotti della General Mills su <strong>scala globale</strong>, ed avrà dunque ricadute sulle operazioni<strong> in più di cento paesi</strong>, come Italia, Giappone, Thailandia, Australia, Germania, Francia, Russia, Israele, Brasile, Messico e molti altri.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L’impegno, che ridurrà la sofferenza di <strong>centinaia di migliaia di galline</strong>, è frutto di una campagna di pressione condotta dalla<strong> Open Wing Alliance</strong>, una coalizione internazionale di cui <strong>Animal&nbsp;Equality</strong> è fiera di far parte. Nel mese di Febbraio i rappresentanti di oltre <strong>17 organizzazioni </strong>all’interno della coalizione, che include <strong>22 diversi paesi nel mondo</strong>, si sono riuniti a Varsavia, in Polonia, per partecipare al <strong>Summit Mondiale per l’Abolizione della Gabbie</strong> (Global Summit to End Cages). Duranta la riunione, le diverse organizzazioni hanno lanciato la prima campagna su scala globale utilizzando petizioni e social media, c<strong>onvincendo infine General Mills ad abbandonare le uova provenienti da galline in gabbia</strong>. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2028} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/general_mills.jpg" alt="" class="wp-image-2028"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Pochi giorni fa, Animal Equality ha diffuso un’<a href="https://animalequality.it/news/2017/02/28/il-vero-prezzo-delle-uova-la-nuova-scioccante-investigazione-di-animal-equality-italia/">investigazione</a> che fa luce sulla realtà degli <b>allevamenti</b> di <b>galline ovaiole</b> in <b>gabbia</b>. Studi dimostrano che le galline sono creature dotate di grande socialità, sensibilità ed intelligenza. Nonostante ciò, in questi allevamenti le galline vivono <b>confinate in piccole gabbie</b>, costrette su un pavimento di rete metallica e senza la possibilità di compiere la maggior parte dei loro comportamenti naturali.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>General Mills, che commercializza i propri prodotti nel mondo attraverso diversi marchi come Pillsbury, Progresso, Betty Crocker, Totino’s e Häagen-Dazs, è una delle<strong> maggiori aziende del settore alimentare mondiale</strong>. Con questo impegno, General Mills si unisce alla lista di grandi aziende, tra cui <strong>Barilla</strong>, <strong>Ferrero</strong> e <a href="https://animalequality.it/blog/entro-giugno-2017-rana-completa-il-progetto-100-uova-da-allevamenti-non-gabbia/"><strong>Giovanni Rana</strong></a>, che stanno <strong>progressivamente escludendo</strong> gli allevamenti di galline ovaiole in gabbia dalla lista dei propri fornitori.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->