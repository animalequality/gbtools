<!-- wp:paragraph -->
<p>Un recente sondaggio condotto dalla Vancouver Humane Society e gestito dalla società di sondaggi Environics “ dimostra che il 33 per cento dei canadesi, o quasi 12 milioni, è vegetariano o sta&nbsp;mangiando meno carne. "Nel dettaglio l'8% degli intervistati sono già vegan o prevalentemente vegan, mentre il 25% dei canadesi dichiara che sta cercando di mangiare meno carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-1310" src="/app/uploads/2015/06/12-million-graphic.jpg" style="width: 800px; "><br>
<br>
Il sondaggio on-line, al quale hanno partecipato 1.507 adulti canadesi, ha rivelato che i canadesi più giovani (età 18-34) hanno dichiarato di essere vegan, mentre i più anziani si sono detti propensi a mangiare meno carne.</p>
<!-- /wp:paragraph -->