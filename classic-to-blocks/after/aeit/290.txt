<!-- wp:image {"id":1239} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/04/13842048955_79bc7bce95_o.jpg" alt="" class="wp-image-1239"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>Un nuovo sconvolgente video, frutto di un’investigazione sotto copertura condotta con telecamere nascoste, svela tremendi atti di crudeltà e maltrattamenti perpetrati nei confronti di agnelli e capretti negli allevamenti e nei macelli italiani a ridosso della Pasqua 2014.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div><strong><img class="wp-image-10643" style="width: 392px; border-width: 0px; border-style: solid; margin: 10px 5px; float: right;" src="/app/uploads/2014/04/claudia_zanella.png" alt="Claudia Zanella">Immagini inedite</strong>, <strong>accompagnate dalla voce narrante dell’attrice Claudia Zanella</strong>, <strong>mostrano scene mai viste prima di reali abusi inflitti a cuccioli</strong> che, uccisi ad appena un mese di vita, sono destinati a diventare cibo “tradizionale” sulle tavole del nostro Paese, soprattutto nel periodo delle festività.

<strong>Le riprese mostrano una realtà terribile che</strong>, sebbene con numeri in costante diminuzione, <strong>coinvolge circa 3 milioni di agnelli e capretti</strong>. E’ questa, infatti, la cifra dei piccoli uccisi ogni anno in Italia per il consumo umano, tra quelli importati dall’Est Europa e quelli allevati in Italia. <strong>Questa cifra cresce se prendiamo in considerazione anche pecore</strong>, agnellotti e capre, <strong>e arriva a superare i 500.000 animali nelle sole settimane precedenti la Pasqua</strong>.

</div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
"<em>Il materiale raccolto dai nostri investigatori negli allevamenti e nei macelli italiani espone una realtà diffusa e non casi isolati; la breve vita di milioni di agnelli e capretti nel nostro Paese è segnata da violenze inimmaginabili"</em>, dichiara Fabrizia Angelini, portavoce di Animal Equality in Italia.

Animali stipati in spazi ristretti, senza ripari, in mezzo a rifiuti e rottami; pecore malate, lasciate per ore legate, allontanate dal gregge, senza alcuna cura veterinaria, in attesa della macellazione; agnelli troppo presto separati dalle loro madri, spesso affette da mastite e con le mammelle in necrosi: queste scene, documentate nell’investigazione, non sono che il preludio ad una fine tremendamente dolorosa, come dimostrano le immagini realizzate dagli attivisti.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div><strong>In un allevamento prima del carico sul camion diretto al macello é stata nuovamente documentata la “pesatura”</strong>, una pratica controversa, cruenta e sulla quale la legge italiana non è molto chiara, <strong>durante la quale gli agnelli, terrorizzati, vengono legati, issati per i carpi (polsi) e pesati in gruppo</strong>. Si tratta di una modalità di contenimento molto dolorosa, che può portare lesioni come strappi muscolari e dei legamenti. Tale posizione é molto innaturale per questi animali e li induce a scalciare nel tentativo di trovare una postura meno stressante, con conseguente rischio di lesioni gravi come la lussazione della spalla o la frattura dei carpi.
<div>All’interno dei macelli le riprese sono sconcertanti: capretti e agnelli terrorizzati, intrappolati nei tunnel che li condurranno verso i loro ultimi istanti di vita. Gli investigatori hanno ripreso le inadempienze dei lavoratori incapaci di stordire gli animali, ai quali viene recisa la gola in stato di coscienza mentre sono in preda al panico e al dolore.</div>
</div>
<!-- /wp:html -->

<!-- wp:html -->
<div><img style="width: 400px; border-width: 0px; border-style: solid; margin: 10px 5px; float: left;" src="https://farm3.staticflickr.com/2836/13842048955_4783ca7bd2.jpg" alt="La campagna 'Salva Un Agnello'">→ <strong>ATTIVIAMOCI: LA CAMPAGNA VIRALE</strong></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>Quest'indagine viene affiancata per il secondo anno consecutivo dalla campagna nazionale '<strong>Salva un Agnello'</strong>: già oltre diecimila italiani hanno sottoscritto il loro impegno a non consumare carne di agnello e capretto durante la prossima Pasqua, attraverso l’apposito form sul sito http://www.SalvaUnAgnello.com. Sono oltre mille poi le persone che hanno aderito alla parallela iniziativa “virale” che li ha portati a scattarsi un selfie con l’apposita cartolina e a pubblicarlo sul proprio profilo Facebook, Twitter o Instagram con l’hashtag #SalvaUnAgnello.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div>Tra gli altri, hanno aderito all’iniziativa, postando un proprio scatto, anche alcuni volti noti dello spettacolo: il conduttore Red Ronnie, la scrittrice e dj Paola Maugeri e l’attrice Claudia Zanella.</div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>→ <strong>QUALCOSA STA CAMBIANDO</strong></div>
<!-- /wp:html -->

<!-- wp:html -->
<div>"<em>Nel 2013 il consumo della carne di agnello è calato significativamente. I dati diffusi dalle associazione di settorei parlano chiaro: durante la scorsa Pasqua la richiesta è diminuita del 40% rispetto all’anno precedente. E per il 2014 il calo si preannuncia ancora più significato: mentre in passato l’agnello era presente durante le festività sulla tavola di una famiglia su due, quest’anno un sondaggio condotto da Confesercenti-Swgii ha rilevato che meno di una famiglia su tre ha deciso di continuare questa tradizione, mentre ben il <strong>58% degli italiani si è dichiarato contrario a portare in tavola questi animali</strong>. É oramai evidente che nell’opinione pubblica il dibattito sulla sofferenza di questi cuccioli è assolutamente aperto</em>”, afferma Fabrizia Angelini.

</div>
<!-- /wp:html -->