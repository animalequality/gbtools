<!-- wp:image {"id":2315} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/02/ElPozo4-2.png" alt="" class="wp-image-2315"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Animal Equality ha recentemente pubblicato <strong>una serie di immagini sconvolgenti raccolte all’interno di un allevamento di maiali in Spagna in collaborazione con l’equipe di Jordi Évole</strong>, giornalista del noto programma televisivo Salvados. I filmati mostrano centinaia di animali deformi, malati e morenti completamente abbandonati a se stessi per mesi interi, condannati a passare tutta la loro vita in condizioni di sofferenza estrema senza mai ricevere cure veterinarie.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2313" src="/app/uploads/2018/02/40240554281_75de699df8_k.jpg" style="width: 840px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Le scene documentate sono impossibili da immaginare finché non le si vede coi propri occhi:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>animali deformati da enormi ascessi ed ernie gigantesche</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>animali che presentano gravi ulcere ricoperte di mosche e larve </li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>maiali che vivono a contatto con animali morti in avanzato stato di putrefazione</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>numerosi container colmi di cadaveri</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>maiali deboli o infermi cannibalizzati dagli altri animali</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>maialini lasciati a morire in agonia senza mai ricevere alcuna assistenza veterinaria</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>animali che passano tutta la vita tra i propri escrementi</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<p><iframe allowfullscreen="" frameborder="0" height="405" src="https://www.youtube.com/embed/8eK1O1JYd24" width="720"></iframe></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>L'allevamento in questione è uno dei principali fornitori di ElPozo</strong>, noto marchio di carne e salumi distribuito da Amazon in molti paesi europei, tra cui anche l’Italia. Il caso portato alla luce è il più grave esempio di negligenza nel campo del benessere animale mai riscontrato in Europa. Animal Equality ha immediatamente provveduto a denunciare la situazione alle autorità ed ora ElPozo sarà chiamato a rispondere di fronte alla legge.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>La notizia ha fatto il giro del web in poche ore ed è già apparsa su alcune tra le più importanti testate europee</strong>, tra cui <a href="https://elpais.com/elpais/2018/02/05/tentaciones/1517819110_839351.html">El Paìs</a> e <a href="https://www.independent.co.uk/news/uk/home-news/morrisons-amazon-uk-spanish-sausages-el-pozo-pigs-animal-equality-animal-welfare-farming-a8195571.html">The Independent</a>, suscitando l’indignazione di centinaia di migliaia di cittadini europei. La petizione, avviata da Animal Equality per chiedere a ElPozo di introdurre politiche volte a migliorare le condizioni di vita degli animali, ha già registrato oltre 150.000 firme a livello internazionale.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Al di là della crudeltà disumana documentata, <strong>si tratta di un enorme scandalo anche per la sicurezza alimentare</strong>: secondo la normativa vigente spagnola, infatti, gli animali malati possono comunque essere inviati al macello, dove le parti considerate "adatte al consumo umano" vengono separate dal resto e immesse nella catena di produzione.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2314" src="/app/uploads/2018/02/40207226792_dfcc26e8cd_k.jpg" style="width: 840px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Matteo Cupi, direttore esecutivo di Animal Equality Italia, ha dichiarato: «<strong>Queste sono alcune fra le immagini più agghiaccianti che abbia mai visto. Un simile livello di negligenza è inaccettabile, a maggior ragione nell’Unione europea. I responsabili dovranno rispondere delle proprie azioni.</strong>» E aggiunge: «ElPozo non distribuisce solo in Spagna, ma in molti paesi europei. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Animal Equality si è subito attivata per fare chiudere l'allevamento in questione e per portare i responsabili di questi maltrattamenti di fronte a un giudice, ma abbiamo bisogno dell'aiuto di tutti per aumentare la pressione su ElPozo e riuscire così a fermare le atrocità in corso.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
&nbsp;</p>
<!-- /wp:paragraph -->