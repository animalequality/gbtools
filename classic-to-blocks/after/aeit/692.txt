<!-- wp:image {"id":2184} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/difensori_animali3.png" alt="" class="wp-image-2184"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>I&nbsp;<a href="https://animalequality.it/difensori-animali/">Difensori degli Animali</a>&nbsp;</strong>sono<strong>&nbsp;</strong>un gruppo di "attivisti digitali" ossia un gruppo di persone che punta a <strong>raggiungere risultati importanti per la vita degli animali rinchiusi negli allevamenti utilizzando&nbsp;delle piccole, facili azioni online giornaliere</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Oggi email, telefonate e varie attività sui&nbsp;social network hanno un effetto molto più incisivo sulla vita degli animali rispetto ad alcune forme di azione più tradizionali ed hanno il vantaggio di poter essere eseguite da molte più persone.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Insomma, piccoli gesti per i singoli, ma dall'impatto enorme perché amplificate da un numero sempre crescente di "attivisti digitali".</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2183} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/06/difensori_animali2.png" alt="" class="wp-image-2183"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Vuoi sapere se questo gruppo fa per te?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Abbiamo deciso di darti 6 motivi per iniziare a difendere gli animali con noi ;)</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>

<h4>Perché diventare un Difensore degli Animali?</h4>
</center>
<!-- /wp:html -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><b>1. Perché puoi rendere realtà il tuo sogno di essere un eroe per gli animali</b></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Se sei una di quelle persone che da sempre guarda con ammirazione ai nostri investigatori e agli attivisti che nel corso degli anni hanno combattuto per gli animali ottenendo risultati importanti, allora questo gruppo fa per te.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><b>2. Perché tutti insieme possiamo fare davvero la differenza</b></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>L’industria dello sfruttamento animale è fra le più potenti al mondo, e le aziende che utilizzano gli animali hanno soldi, risorse umane, e protezione legale. Il maltrattamento nei confronti degli animali da reddito è talmente istituzionalizzato da essere considerato normale. Per tutti questi motivi,<strong> presi singolarmente abbiamo una capacità molto limitata di cambiare le cose</strong>. <strong>Ma tutti insieme siamo una forza inarrestabile</strong>. Un saggio anonimo disse “un gruppo di persone con un obiettivo comune può raggiungere l’impossibile”. Se anche tu ci credi, allora questo gruppo fa per te.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><b>3. Perché difendere gli&nbsp;animali dà soddisfazione ;)</b></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Se ti piace pensare di dare fastidio a chi sfrutta gli animali ed allo stesso tempo schierarti dalla parte degli animali; se l’idea di mettere i bastoni fra le ruote alle aziende che maltrattano gli animali ti sembra allettante; se il fatto di creare un disturbo a coloro che vedono gli animali come mere unità produttive ti compiace, allora questo gruppo fa per te.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><b>4. Perché il tuo sforzo è minimo ma&nbsp;l'impatto è massimo</b></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Alla fine dei conti, tutti vogliamo che i nostri sforzi abbiamo un impatto. E quando si tratta di combattere contro le grandi aziende che sfruttano gli animali, è davvero necessario che tutte le risorse disponibili vengano utilizzate nel modo più intelligente ed efficace possibile. Negli ultimi mesi abbiamo spinto dieci aziende del settore alimentare ad adottare politiche aziendali volte a ridurre la sofferenza negli allevamenti, avendo un impatto diretto su 2.230.000 animali. I fatti (ed i numeri!) parlano chiaro: se desideri che le tue azioni scaturiscano in un risultato tangibile, allora questo gruppo fa per te.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><b>5. Perché ci vuole così poco&nbsp;che non farlo sarebbe assurdo!</b></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Fare parte della squadra dei Difensori degli Animali è facile e semplicissimo. Le azioni che dovrai compiere non ti richiederanno mai più di 3 minuti, e potrai agire direttamente dal tuo computer o dal tuo cellulare. Noi facciamo il lavoro più difficile e ti mettiamo nella condizione di poter agire senza complicazioni, perché tu possa convertire la tua pausa caffè o il viaggio in ascensore in attivismo di impatto per gli animali. Se dedicare 3 minuti del tuo tempo al giorno ad aiutare gli animali ti sembra una bella idea, allora non avere dubbi: questo gruppo fa per te.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><b>6. Perché milioni di animali hanno bisogno di te, proprio adesso!</b></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Proprio in questo momento milioni di animali si trovano all’interno di allevamenti intensivi e vivono un’esistenza di dolore e disperazione. Costretti a vivere dentro le gabbie o in capannoni senza aria né luce, questi animali soffrono terribilmente e sono continuamente sottoposti a pratiche estremamente crudeli. Ognuno di questi animali è un individuo sensibile ed intelligente, che non può parlare né difendersi in nessun modo. Se quando pensi a questi animali senti dentro di te una necessità profonda di "fare qualcosa", allora devi fidarti: questo gruppo fa per te.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<strong><a href="https://animalequality.it/difensori-animali/">DIVENTA ORA&nbsp;UN DIFENSORE DEGLI ANIMALI!</a></strong>
</center>
<!-- /wp:html -->