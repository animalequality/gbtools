<!-- wp:image {"id":1026} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/06/ministeroambiente_06.jpg" alt="" class="wp-image-1026"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Alle prime luci dell'alba di giovedì 7 giugno 2012, nei pressi del <b>Ministero dell'Ambiente e della Tutela del Territorio e del Mare </b><span style="font-weight: normal">è scattata l'azione di Animal Equality</span>; davanti alla&nbsp; sede e nella zona circostante sono comparse delle insegne stradali molto particolari, tutte indicanti '<b>Carloforte 574 – www.lamattanzadeitonni.org</b>'. L'azione si inserisce nella campagna pubblica di sensibilizzazione lanciata in seguito all'investigazione presentata in data 31 maggio 2012 che ha esposto le crudeltà che avvengono durante la mattanza dei tonni rossi a Carloforte, in Sardegna. <img alt="" class="wp-image-10679" src="/app/uploads/2012/06/ministeroambiente_01.jpg" style="width: 200px; margin: 5px 10px; float: left;">Con queste provocatorie insegne stradali rivolte verso il Ministero, si è voluta sottolineare la distanza geografica tra quest'ultimo e Carloforte, evidenziando che non è certo un fatto del genere a tenere il problema celato e dimenticato.<br>
	<br>
	<strong><a href="https://www.flickr.com/photos/animalequalityitalia/sets/72157630072193918/" target="_blank" rel="noopener noreferrer">Visualizza la galleria fotografica completa di quest'azione</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Animal Equality ha voluto evidenziare provocatoriamente al Ministero le proprie responsabilità etiche, educative e legislative nel permettere la pesca del tonno rosso ed eventi tragici come la specifica mattanza esposta dall'organizzazione. Già nel 2010, tale istituzione affermava pubblicamente che la pesca di quest'animale andava fermata dichiarando di volersi impegnare in tal senso, ma nulla è mai stato fatto, tanto meno con alla base un aspetto fondamentale: i tonni (come qualsiasi altro pesce), secondo attenti studi scientifici, sono provvisti di un sistema nervoso molto sviluppato, sono individui e provano le stesse sensazioni, la stessa sofferenza di qualsiasi altro animale, essere umano incluso. La pratica della pesca e nello specifico la cruenta 'tradizione' di Carloforte andrebbero fermate immediatamente ed abolite poiché la crudeltà che viene esposta è evidente, tangibile.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Parallelamente a quest'azione che lancia ufficialmente una campagna pubblica in supporto all'investigazione presentata sulla mattanza di Carloforte, <strong>Animal&nbsp;Equality promuove una protesta via email indirizzata al Ministero</strong>, sollecitandolo ad un intervento nei confronti di pratiche che, per la loro evidente crudeltà e brutalità nei confronti degli animali, andrebbero fermate immediatamente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">----------------------<br>
	<u>Indirizzi email a cui scrivere</u>:<br>
	<br>
	Segretariato Genereale - <strong>SegretariatoGenerale@minambiente.it</strong><br>
	Direzione generale per la protezione della natura e del mare - <strong>pnm-udg@minambiente.it</strong><br>
	<br>
	<u>Email tipo</u>:<br>
	<br>
	Alla cortese attenzione<br>
	del Ministro dell'Ambiente e della Tutela del Territorio e del Mare<br>
	del Segretariato Generale<br>
	della Direzione Generale per la protezione della natura e del mare<br>
	<br>
	Salve,<br>
	è stato incredibilmente scioccante per me vedere la brutalità e la violenza inflitta ai tonni rossi durante la mattanza che avviene tutti gli anni a Carloforte (Sardegna), documentata dall'organizzazione internazionale per i diritti animali Animal Equality e visibile in questo video uscito in esclusiva per La Repubblica:<br>
	http://video.repubblica.it/cronaca/tonno-rosso-sangue-la-mattanza-in-sardegna/96871/95253.<br>
	<br>
	Le immagini dei tonni che si dimenano terrorizzati ferendosi l'uno contro l'altro durante la cattura o mentre arpionati in maniera cruenta e trascinati sulle barche circostanti, dove con un coltello gli viene forato il cuore per permettere che le carni rimangano morbide mentre muoiono soffocati, mi indignano fortemente.<br>
	<br>
	Penso che, per quanto si possa parlare di 'tradizione' e di 'cultura', non ritengo che queste parole debbano essere associate ad una pratica così cruenta ed ingiustificata. Il Ministero dell'Ambiente e della Tutela del Territorio e del Mare, poiché punto di riferimento legislativo per ciò che riguarda la fauna e l'ambiente da essa abitato, dovrebbe esprimere il suo sdegno verso una pratica che causa ogni anno la morte di migliaia di individui ed adoperarsi, in quanto organo competente, affinché questa abbia fine.<br>
	<br>
	Grazie per l'attenzione.<br>
	<br>
	Cordiali saluti,<br></p>
<!-- /wp:paragraph -->

<!-- wp:shortcode -->
[nome e cognome]
<!-- /wp:shortcode -->