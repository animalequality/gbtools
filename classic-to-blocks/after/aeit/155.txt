<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>DATA:</strong> 6 e 7 dicembre 2012

<strong>ORA:</strong> inaugurazione alle ore 9 della mattina di giovedì 6 dicembre. Chiusura alle ore 21 di venerdì 7 dicembre

<strong>LUOGO:</strong> Ateneo di Madrid

<strong>INDIRIZZO:</strong> Calle Prado, 21 • Madrid

<strong>ORGANIZZATORE:</strong> Animal Equality

<strong>SITO WEB:</strong> www.CongresoContraLaRepresion.org

<strong>TWITTER:</strong> @antirepresion

<strong>HASHTAG:</strong> #congresoantirepresion

<strong>Il 6 e 7 dicembre</strong> si terrà il "Congresso Internazionale Contro la Repressione - Critica alla criminalizzazione dell'attivismo" all'Ateneo di Madrid.

Il congresso vedrà la partecipazione di <strong>22 esponenti dei movimenti per i diritti umani, degli animali ed ecologisti, appartenenti al movimento LGBT, per la libertà di stampa e per i diritti dei lavoratori</strong>. Inoltre, sono stati invitati giornalisti, avvocati, giudici, pubblici ministeri, sociologi, politici, filosofi, psicologi provenienti da diversi paesi come Austria, Stati Uniti, Spagna, Inghilterra, Islanda, Norvegia e Siria.

I temi portanti saranno il ruolo della disobbedienza civile e la libertà di espressione, di riunione, di stampa ed il diritto all'informazione nelle società democratiche attuali.

Al Congresso parteciperanno, nel ruolo di relatori, <strong>Alberto Garzón</strong> (deputato del Congresso per la Sinistra Unita), <strong>Santiago Vidal </strong>(Giudice del Tribunale Provinciale di Barcellona, in rappresentanza dei Giudici per la Democrazia), <strong>Kristinn Hrafnsson</strong> (giornalista e rappresentate islandese di Wikileaks - TBC - ), <strong>Joseph Farrell</strong> (membro di Wikileaks, ancora da confermare), <strong>Martxelo Otamendi</strong> (ex direttore di Euskaldunon Egunkaria), <strong>Ines Herreros</strong> (procuratore e membro dell'Unione Progressista dei Procuratori), <strong>Carlos Sanchez Almeida</strong>, (avvocato e ciberattivista), <strong>Juan Lopez de Uralde</strong> (attivista ecologista e portavoce di Equo), <strong>Pilar Velasco</strong> (giornalista per Cadena Ser), <strong>Edurne Elizondo</strong> (giornalista di Berria),<strong> Andy Stepanian</strong> (attivista ed ex prigioniero politico, dagli Stati Uniti), <strong>Brendan McNally</strong> (attivista inglese), <strong>Christof Mackinger</strong> (politologo e attivista austriaco), <strong>Martin Balluch</strong> (presidente della VGT e recentemente insignito del premio etico Mishkin), <strong>Leila Nachawati</strong> (attivista per i diritti umani in Siria), <strong>Michael Loadenthal</strong> (filosofo americano ed esperto di terrorismo), <strong>Pablo Iglesias</strong> (dottore in scienze politiche e professore presso l'Universidad Complutense di Madrid), <strong>Pattrice Jones</strong> (psicologa, attivista LGBT e ecofemminista), <strong>Sharon Nunez</strong> (attivista, presidente e co-fondatrice di Animal Equality), <strong>Rune Ellefsen</strong> (sociologo norvegese e membro del Dipartimento di Criminologia presso l'Università di Oslo).

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://player.vimeo.com/video/53680195" width="400" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Il <strong>Congresso Internazionale Contro la Repressione</strong> nasce dalla necessità urgente, delle realtà colpite dalla repressione, di condividere le loro esperienze e cercare il modo per difendersi e rispondere. Per due giorni sarà analizzata, in modo approfondito, l'ondata di repressione che sta minacciando il movimento internazionale per i diritti civili.

Il Congresso Internazionale Contro la Repressione è un'iniziativa di <a href="https://animalequality.it/"> Animal Equality </a>.
Animal Equality è un'organizzazione internazionale in difesa di tutti gli animali ed è oggi presente in Spagna, Germania, Inghilterra, Italia, India, Messico e Venezuela. Realizza investigazioni, salva animali dai luoghi di sfruttamento e promuove azioni di disobbedienza civile con lo scopo di sensibilizzare l'opinione pubblica su quello che accade agli animali.

Il Congresso è sostenuto dalla campagna <a href="http://www.unidoscontralarepresion.org/"> 'Unidos Contra La Represión del Movimiento de Derechos Animales' </a> ed è sponsorizzato dall'organizzazione statunitense per i diritti degli animali <a href="http://www.lcanimal.org/"> Last Chance for Animals </a> che da oltre 30 anni lavora in loro difesa attraverso la realizzazione di investigazioni, riscatti, campagne e azioni di disobbedienza civile .</p>
<!-- /wp:paragraph -->