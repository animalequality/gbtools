<!-- wp:paragraph -->
<p>Divella si unisce alle aziende che hanno detto basta alle gabbie per le galline ovaiole. L’azienda, leader nella produzione di pasta e biscotti, <a href="https://www.divella.it/it/news/uova-fresche-da-galline-allevate-a-terra.html">pubblica</a> la propria posizione a riguardo, impegnandosi a cessare l’utilizzo di uova da allevamenti in gabbia entro dicembre 2019 per tutti i prodotti a marchio Divella. Questa comunicazione, giunta a seguito del dialogo intercorso con Animal Equality, rappresenta un passo avanti enorme per le galline ovaiole, le cui condizioni di vita dipendono dalle scelte delle aziende come Divella.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2444" src="/app/uploads/2018/06/Divella_0.jpg" style="width: 750px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Unendosi ad aziende come Garofalo, De Cecco e pasta Zara, Divella dimostra così l’attenzione dedicata alle condizioni di vita degli animali coinvolti nella sua filiera. Le aziende possono fortemente influenzare le metodologie di allevamento, e si prendono le proprie responsabilità pubblicando scelte come quella di Divella, che permetterà a migliaia di galline di uscire dalla costrizione delle gabbie.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Eliminare le gabbie non significa eliminare la crudeltà, ma per le galline costrette negli allevamenti significa avere maggiori possibilità di esprimere i propri comportamenti naturali. Ci auguriamo che questo trend, ormai segnato, si allarghi sempre più, fino a costruire un futuro libero dalle gabbie per tutti.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Gli impegni delle aziende sono di fondamentale importanza, ma tu puoi fare ancora di più e aiutare a creare un futuro libero da ogni sofferenza animale, semplicemente scegliendo di lasciare le uova fuori dal tuo piatto. Dai un’occhiata a <a href="https://campaigns.animalequality.it/cucinare-senza-uova-scarica-ora-il-ricettario-gratuito/">questa pagina</a> e scopri quante alternative esistono!&nbsp;</h4>
<!-- /wp:heading -->