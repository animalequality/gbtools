<!-- wp:image {"id":1028} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/06/7303943426_4ea08b7be2.jpg" alt="" class="wp-image-1028"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">La prima puntata del programma televisivo nazionale <strong>Lineablu</strong>, andata in onda sabato 9 giugno scorso dalle ore 14.00 su RaiUno, è stata caratterizzata da un servizio molto ampio realizzato sull'Isola di San Pietro. Nello specifico, per un totale di circa 20 minuti, la mattanza dei tonni praticata in quella zona e documentata recentemente da <strong>Animal&nbsp;Equality</strong>, è stata difesa con argomentazioni altamente diseducative e prive di qualsiasi fondamento etico.<br>
	<br>
	Le tonnare fisse da qualche anno sono sicuramente in crisi, chiaramente non per ragioni etiche inerenti la sofferenza degli animali, ma per mere ragioni economiche. Il programma Lineablu, probabilmente influenzato da una mancanza di imparzialità di cui possiamo immaginare le ragioni (Sarà un caso la realizzazione di questa puntata a 10 giorni dal rilascio della nostra investigazione?), si prodiga in un servizio dove è possibile addirittura assistere all'assurda affermazione di un biologo che ritiene le tonnare fisse un buon metodo di monitoraggio per la salvaguardia del tonno.<br>
	<br>
	<img alt="" src="https://farm9.staticflickr.com/8151/7303943426_4ea08b7be2.jpg" style="width: 200px; margin: 10px 5px; float: right;">Il servizio realizzato è altamente diseducativo e questo non è accettabile. Sull'onda dell'ipocrisia emotiva e della 'crisi economica' si giustifica un massacro di essere viventi, spacciandolo per 'tradizione culturale'; le parole 'tradizione' e 'cultura' non possono e non devono essere associate ad eventi discriminatori, irrispettosi e violenti verso qualsiasi individuo.<br>
	<br>
	<strong>Invitiamo ad esprimere il più possibile il proprio dissenso alla redazione del programma Lineablu, rimanendo cortesi ed educati nell'esporre le motivazioni</strong>. E' opportuno che programmi del genere vengano a conoscenza dell'opinione del pubblico riguardo a pratiche come la mattanza dei tonni di Carloforte. <strong>Una casella email diretta del programma in questione non sembra più esistere, pertanto vi invitiamo a postare commenti sulla pagina dove è presente il servizio tramite Facebook oppure registrandosi attraverso il servizio 'my rai.tv'</strong>:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><u>La pagina dove è presente il servizio televisivo è la seguente</u>:<br>
	<a href="https://www.rai.it/dl/RaiTV/programmi/media/ContentItem-fab01875-4f4c-448c-bd3a-faef9a177bcf.html" target="_blank" rel="noopener noreferrer"><strong>www.rai.tv/dl/RaiTV/programmi/media/ContentItem-fab01875-4f4c-448c-bd3a-faef9a177bcf.html</strong></a><br>
	<br>
	Facciamo presente la violenza delle mattanze dei tonni prima di tutto linkando:<br>
	<br>
	• Il video della mattanza di Carloforte pubblicato in esclusiva dal quotidiano La Repubblica:<br>
	<a href="https://video.repubblica.it/cronaca/tonno-rosso-sangue-la-mattanza-in-sardegna/96871/95253" target="_balnk" rel="noopener noreferrer"><strong>video.repubblica.it/cronaca/tonno-rosso-sangue-la-mattanza-in-sardegna/96871/95253</strong></a><br>
	• La versione intera del video disponibile sul nostro canale Youtube:<br>
	<strong>www.youtube.com/watch?v=7lg02isKRdc</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">• La petizione dove si richiede agli enti ed amministrazioni responsabili la totale abolizione della mattanza:<br>
	<strong>www.animalequality.it/firma-la-petizione-abolire-la-mattanza-dei-tonni-di-carloforte</strong><br>
	<br>
	<u>Linkare il video e la petizione è molto importante, perchè permette di apprendere di questa triste realtà a chi non ha avuto ancora opportunità di venirne a conoscenza</u>.<br>
	<br>
	Menzioniamo di seguito un 'commento tipo' da postare o modificare a proprio piacimento:<br>
	<br>
	Spettabile Redazione di 'Linea Blu',<br>
	ho preso visione della puntata speciale girata a Carloforte lo scorso sabato 9 giugno scorso. Tra i vari argomenti esposti, si è parlato della mattanza dei tonni e dell'importanza del fatto che le tonnare fisse riescano a 'sopravvivere' nelle località attorno all'Isola di San Pietro. E' stato incredibile per me assistere ad una puntata altamente diseducativa, ritengo che un programma che si presenti con argomenti culturali ed educativi non possa essere associato a servizi televisivi come quello a cui ho assistito. Nei giorni passati l'organizzazione internazionale per i diritti animali Animal Equality ha pubblicato, con un'esclusiva sul canale video del quotidiano La Repubblica (http://video.repubblica.it/cronaca/tonno-rosso-sangue-la-mattanza-in-sardegna/96871/95253) la violenza inflitta ai tonni rossi durante la mattanza che avviene a Carloforte, esattamente dove è stato realizzato il vostro servizio.<br>
	<br>
	Mi indignano fortemente le immagini dei tonni che si dimenano terrorizzati ferendosi l'uno contro l'altro durante la cattura o mentre vengono arpionati in maniera cruenta e trascinati sulle barche circostanti, dove con un coltello gli viene forato il cuore per permettere che le carni rimangano morbide mentre muoiono soffocati.<br>
	<br>
	Il vostro programma ha realizzato un servizio altamente diseducativo, in cui è stata ignorata completamente la realtà di una pratica cruenta e brutale che causa da tantissimi anni la morte di migliaia di animali.<br>
	Non è accettabile per me assistere a servizi dove, sull'onda dell'ipocrisia emotiva e della 'crisi economica', si giustifica un massacro di essere viventi, spacciandolo per 'tradizione culturale'. Le parole 'tradizione' e 'cultura' non possono e non devono essere associate ad eventi discriminatori, irrispettosi e violenti verso qualsiasi individuo, compresi gli animali massacrati nelle tonnare.<br>
	<br>
	Distinti Saluti.</p>
<!-- /wp:paragraph -->