<!-- wp:image {"id":2016} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/LOGHI_fb.jpg" alt="" class="wp-image-2016"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Il piano di <strong>#run4animals</strong> è semplice: ci alleniamo, ci divertiamo e raccogliamo fondi per un mondo senza gabbie partecipando alla <strong>Milano Marathon 2017 domenica 2 aprile</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Obiettivo di quest'anno è raccogliere fondi destinati alla campagna per <strong>liberare gli animali dalle gabbie</strong>. Lo faremo attraverso <strong>nuove investigazioni</strong>, <strong>pressione alle aziende attraverso petizioni pubbliche</strong>, campagne di <strong>informazione pubblica</strong>, azioni di <strong>pressione sui decisori politici</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/lIIb38jdprM" width="850"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Solo pochi giorni fa abbiamo già ottenuto questo importante risultato: <a href="https://animalequality.it/news/2017/01/26/allevamenti-di-conigli-gabbia-ue-abolizione-sempre-piu-vicina/"><strong>è sempre più vicina nell'UE l'abolizione delle gabbie negli allevamenti di conigli</strong></a>. Un passo in avanti ma certamente non l'ultimo. Per questo dobbiamo correre per gli animali!&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La squadra di Animal Equality quest'anno sarà di <strong>circa 40 persone</strong> che correranno la staffetta 4x10km: la foto di gruppo sarà molto più affollata di quella dello scorso anno :).</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 class="wp-block-heading">Abbiamo appena lanciato la campagna e abbiamo raccolto circa la metà dei <strong>Runners4Animals</strong>!<br>
<br>
Ma ci sono ancora dei posti liberi perciò se non ti spaventa correre per 10 km e <strong>hai a cuore gli animali</strong> <a href="http://run4animals.animalequality.it"><strong>contattaci attraverso la pagina della campagna: abbiamo bisogno di te</strong></a>!</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><br>
Se vuoi ti possiamo<strong> inserire una delle squadre</strong> oppure<strong> puoi coinvolgere altre tre persone e fare la tua squadra!</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2014,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="http://run4animals.animalequality.it"><img src="/app/uploads/2017/02/LOGHI_850.jpg" alt="" class="wp-image-2014"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Partecipare è facile: ti manderemo tutte le informazioni via mail.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La partecipazione alla staffetta quest'anno prevede un contributo a persona a partire da 40 euro che comprende:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li><br>	<p>l'iscrizione alla staffetta (pettorale e cronometraggio) </p><br>	</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>	<p>il pacco gara della Milano Marathon (gadget vari e i vari prodotti degli sponsor dell'evento)</p><br>	</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>	<p>il ristoro durante la corsa e a fine gara</p><br>	</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>	<p>la maglietta della Milano Marathon </p><br>	</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>	<p>la splendida maglietta di #Run4Animals (stiamo definendo il lay out in questi giorni)</p><br>	</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><br>	<p>e alcuni allenamenti insieme per prepararci al meglio<br><br>	 </p><br>	</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>La partecipazione di Animal Equality alla maratona è un'importante occasione per raccogliere fondi per la <strong>campagna contro le gabbie</strong>, per questo ognuno di noi sarà testimonial degli animali e si farà promotore della raccolta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ogni Staffetta avrà come obiettivo di raggiungere i 1000 euro, circa 250 euro a testa.<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2015} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/02/maratona2015.jpg" alt="" class="wp-image-2015"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Alcuni dei partecipanti alla Milano Maraton 2015 per Animal Equality</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Non farti spaventare dai numeri&nbsp;lo scorso anno anche chi era più restio è riuscito a raccogliere molto di più perché è stupefacente quante persone vogliano <strong>contribuire a dare un futuro diverso agli animali</strong>, un futuro <strong>libero dalle gabbie</strong>.&nbsp;</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se deciderai di partecipare a<strong> #run4animals</strong> ti accompagneremo passo passo e ti darò tutti i contenuti, i consigli e le idee più semplici ed efficaci per raggiungere insieme il nostro obiettivo: non rimarrai da solo!&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Basta aprire un account (personale o della squadra) su Retedeldono alla pagina del nostro progetto, scrivere qualche riga di presentazione e poi condividere la <strong>pagina sui social</strong>, via <strong>mail</strong> e con i mezzi in cui lo ritieni più opportuno!&nbsp;Nessuna preoccupazione: contribuire alla raccolta fondi <strong>è più facile che prepararsi alla corsa</strong> :)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
Presto ti faremo sapere i primi appuntamenti per allenarci insieme in vista della Maratona.<br>
Ovviamente il team italiano&nbsp;di Animal Equality parteciperà al completo!&nbsp;:)</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 class="wp-block-heading"><a href="http://run4animals.animalequality.it">Il 2 aprile sarà un grande giorno perché correremo insieme per dare voce agli animali!<br>
Più saremo, più persone riusciremo a coinvolgere, più forte sarà la loro voce.</a></h2>
<!-- /wp:heading -->