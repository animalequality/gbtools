<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Durante la notte degli Oscar, <strong>Leonardo DiCaprio ha approfittato del suo attesissimo discorso di ringraziamento per parlare di una tematica a lui cara da molti anni: il cambiamento climatico.</strong><em>“Il cambiamento&nbsp;climatico&nbsp;è una cosa&nbsp;reale, sta avvenendo&nbsp;in questo momento. <strong>È il pericolo più grave che la nostra intera specie si trova ad affrontare</strong>, e dobbiamo lavorare insieme e <strong>smettere di rimandare</strong>”</em> ha dichiarato.

<strong>Ma cosa può fare ognuno di noi nel suo piccolo per salvare il pianeta?</strong>

Secondo uno <a href="http://www.sciencedirect.com/science/article/pii/S0306919216000129">studio</a> appena pubblicato dal Food Policy Journal, dobbiamo <strong>dimezzare i consumi di carne e latticini</strong>.

<strong>L’Unione Europea si è impegnata a ridurre le emissioni di gas serra del 20% entro il 2020</strong>. Per raggiungere questo scopo si dovranno inevitabilmente<strong> ridurre del 75% le emissioni prodotte da bovini e ovini</strong>, dichiarano i ricercatori.

</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://static-secure.guim.co.uk/sys-images/Guardian/About/General/2014/9/30/1412094898081/Bakers-Dairy-Farm-in-Hase-014.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Uno studio che ricorda <a href="https://www.theguardian.com/news/datablog/2013/jan/10/how-much-water-food-production-waste">quello dell’IME</a> (Institution of Mechanical Engineers), che descrive nel dettaglio l’enorme spreco d’acqua dell’industria della carne con le gravi conseguenze sul bisogno mondiale di acqua potabile.

Secondo i dati dell’IME ci vogliono <strong>20.000 litri d’acqua per produrre 1kg di carne</strong>, mentre ce ne vogliono da 500 a 4.000 (un quarto al massimo!) per 1kg di grano.

Anche il latte è stato preso in considerazione dalla ricerca svedese: <em>"Il consumo di latte e derivati in Europa e Stati Uniti è tra i più alti del mondo ed ha lo stesso impatto sul clima del consumo di pollo e maiale” - </em>dichiara&nbsp; Stefan Wirsenius, uno degli autori - <em>“Se sostituissimo alcuni di questi prodotti con alternative vegetali, avremmo sicuramente maggiori possibilità di raggiungere gli obiettivi stabiliti”. </em>Secondo la ricerca dell’IME, <strong>produrre un bicchiere di latte richiede ben 255 litri d’acqua</strong>: sono circa&nbsp;<strong>5 docce</strong>!

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1435} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/shutterstock_172996022.jpg" alt="" class="wp-image-1435"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

A proposito di clima e allevamenti, non dimentichiamo che il neo premio Oscar DiCaprio è direttore esecutivo di <a href="https://www.cowspiracy.com/">Cowspiracy</a>: un documentario illuminante <strong>sul ruolo degli allevamenti intensivi nel cambiamento climatico</strong>. Se siete stanchi di aspettare e volete fare attivamente&nbsp;qualcosa per il pianeta, ne consigliamo caldamente la visione!
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/ywiufDyniyU" width="800" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->