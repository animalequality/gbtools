<!-- wp:paragraph -->
<p>Quando parliamo di diritti umani e diritti degli animali, parliamo di due facce della stessa medaglia. Entrambi i movimenti condividono gli stessi obiettivi: rispetto, empatia e giustizia per gli uomini, per gli animali e per se stessi. Proprio per questo, <a href="http://www.beadandreel.com/"><b>Bead &amp; Reel</b></a> nasce dall’idea che moda e etica non si escludono a vicenda: il brand tutto americano firmato <strong>Sica Schmitz</strong> offre alternative <i>eco-friendly</i> e totalmente <i>cruelty-free</i> per donne coscienziose di tutto il mondo.

“Per me, uno stile di vita che evita la <strong>crudeltà verso gli animali </strong>include necessariamente i <strong>diritti umani </strong>e il <strong>rispetto per tutte le forme di vita</strong>” – ha detto la giovane fondatrice del marchio in occasione della seconda annuale <i>Fair Trade Fashion Show</i> lo scorso 16 Luglio a Los Angeles - “Allo stesso modo in cui chiediamo ad un onnivoro di realizzare il fatto che mucche e gatti sono allo stesso modo meritevoli di vita e di dignità, credo che animali ed esseri umani meritino lo stesso rispettoso trattamento. Creare moda <i>cruelty-free</i> a spese di vite umane non corrisponde alla mia definizione di <strong>compassione</strong>, per questo il commercio equo-solidale è una componente chiave della mia versione di moda <i>cruelty-free</i> alla Bead &amp; Reel. Così come non voglio che un animale soffra per creare qualcosa che indosso, allo stesso modo non voglio che un uomo venga sfruttato per lo stesso scopo”.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1952} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/01/moda_vegan_2.jpg" alt="" class="wp-image-1952"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>Sica Schmitz </strong>ha fondato Bead &amp; Reel impostato sulla sua instancabile ricerca di moda etica. Anche come Costume Designer a Hollywood, ha avuto un periodo difficile mentre cercava di trovare un compromesso tra la sua idea di <strong>compassione animale e di valori umani</strong>: è così che ha creato lo store in cui le sarebbe piaciuto fare acquisti. La fondatrice crede fermamente che la moda possa essere uno strumento per migliorare sia la vita di chi fa i vestiti che quella di chi li indossa, e si dedica ad aiutare le donne ad amare se stesse attraverso quello che indossano quotidianamente e ad amare il mondo grazie a quello che comprano.

Negli ultimi anni, il marchio ha ottenuto numerosi riconoscimenti, tra cui l’approvazione da parte di <i>Peta</i>, <i>Green America Certified Business</i>, <i>Fair Trade LA</i> e <i>Vegan Trade Council</i>.

A questo punto, niente più scuse: l’evoluzione gentile è realtà, e “va di moda”.

<em>Miriam Bruno</em></p>
<!-- /wp:paragraph -->