<!-- wp:paragraph -->
<p>Sembrano immagini surreali ed è difficile credere che raccontino una storia vera, fatta di migliaia di individui che ogni giorno incontrano la morte.
Purtroppo sta accadendo a Mordano (Bologna): le galline vengono ammassate nelle gabbie, afferrate per le zampe e lanciate in grandi cassoni per essere gasate. Cinquantamila ogni giorno, per dieci lunghi giorni. Cinquecentomila animali uccisi così.

<strong>I volatili rinchiusi nello stabilimento dell'Eurovo si sono ammalati di <a href="https://bologna.repubblica.it/cronaca/2013/08/25/news/laviaria_presenta_il_conto_80_aziende_sorvegliate_speciali-65269591/">aviaria</a> e quando questo accade non c'altra soluzione che ucciderli, tutti.</strong>
Qualcuno parla di camion che non venivano disinfettati, altri si preoccupano per le galline del proprio piccolo pollaio che potrebbero contrarre il virus, addirittura si pensa alla caccia e al rischio di non poter più uccidere i fagiani che vivono nei pressi dell'azienda, potrebbero essere contaminati anche loro.
La solidarietà per le vittime di questa strage appare ancora più grottesca, tra chi sospira </p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<cite>"povere galline"</cite>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p> ricordando come veniva </p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<cite>"schiacciata la testolina"</cite>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p> dei pulcini uccisi perché nati con qualche difetto e chi invece sceglie di celebrarle mangiando frittate, tanto </p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<cite>"le uova, se cotte, non fanno alcun male"</cite>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>.

</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://bologna.repubblica.it/images/2013/08/22/173028923-4a9d06b1-0b87-4e9b-bf78-ddc95e40e01c.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Tra i tanti pensieri </p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<cite>dedicati</cite>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p> a questi animali ne manca uno, così palesemente semplice eppure evidentemente sfuggente: perché così tanti individui si trovano rinchiusi dentro un capanno? Quale soluzione potremmo proporre affinché questi poveri animali non siano più' </p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<cite>poveri</cite>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p> ma liberi?

La risposta per noi è semplice. Nessun individuo dovrebbe essere sfruttato: cinquecentomila animali che muoiono in quello che sembra un girone dantesco non sono risorse perdute ma individui ai quali è stata negata la vita, la libertà. Individui che come noi hanno un proprio sentire, dei desideri, delle necessità che vengono calpestate quotidianamente perché è più importante trasformarli in macchine per produrre uova. E così dimentichiamo chi sono, dimentichiamo tutto e riusciamo a leggere quel 500.000 senza battere ciglio, pensando magari alla perdita in termini economici e non a quante vite sono state spezzate.

Ogni giorno le nostre scelte, anche le più piccole e apparentemente insignificanti, possono diventare importanti e permetterci di modificare lo stato delle cose. I nostri consumi hanno un potere enorme e quello che a noi appare banale, per altri può fare la differenza. Una differenza che costa l'intera vita.
Scegliere di non consumare prodotti di origine animale significa restituire a tutti loro una possibilità e provare a compiere il primo passo verso un mondo diverso, fatto di solidarietà e di rispetto.</p>
<!-- /wp:paragraph -->