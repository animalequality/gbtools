<!-- wp:paragraph -->
<p>Uno studio rivela che le mucche hanno una vita sociale complessa e gerarchica; ed amano avere tutto sotto controllo. Oltretutto sono delle vere amanti della musica.
Come molti video su YouTube dimostrano,&nbsp;le mucche amano la musica suonata dal vivo e si stringono ad ascoltarla concentrate attorno al musicista di turno.
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="ae-video-container"><iframe src="https://www.youtube.com/embed/E47D_PZ2Zec" width="800px" height="450px" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
La Dott.ssa Rebecca Doyle del Centro per il benessere animale dell'Univeristà di Melbourne afferma che la curiosità è innata nelle mucche.
</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph -->
<p><em>“Spesso entrano in conflitto con loro stesse perché sono naturalmente curiose ed hanno al contempo paura dell'ignoto. Quando sono in grado di controllare la situazione, sono molto contente di fare un'esperienza positiva, ma se vengono forzate a compiere delle azioni nuove e sconosciute, il fatto di non avere il controllo di quanto accade le spaventa e le stressa fortemente”.</em></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p><b>Chi sapeva che le mucche amano avere tutto sotto controllo? </b>

Essendo delle “prede” la necessità del controllo è per loro biologica. Prendiamo come esempio il controllo del loro spazio personale. Questa è la distanza minima da una possibile minaccia entro la quale la mucca si sente tranquilla, che sia essa un cane, un leone, un umano o una moto.

Una mucca che vive in grandi spazi aperti in Australia, la quale quindi non vede molto spesso gli umani, ha necessità di avere il controllo su uno spazio di 20 – 30 metri. Mentre questa necessità per una mucca da latte munta tre volte al giorno viene di molto ridotta.

Questo è il motivo per il quale gli allevatori di mucche da carne e gli allevatori di mucche da latte usano tecniche diverse: per esempio è molto più complicato spostare una mucca da un versante all'altro del campo poiché la mucca vorrà continuamente mantenere la propria distanza di sicurezza.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<address>E' molto difficile rimanere nascosti alla vista di una mucca, sia perché sono sempre molto attente, sia perché, avendo gli occhi disposti ai lati della testa, hanno una visione più ampia e l'angolo cieco è minimo. Anche il resto della mandria funge da controllo e segnala eventuali pericoli.&nbsp;</address>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><b>Quante mucche servono per formare un branco?</b>

Gli studiosi hanno rilevato che il numero minimo per formare un branco, che dia una certa sicurezza, deve essere composto da almeno 8 individui. Sotto questo numero le mucche sono molto più vigili e nervose.

<strong>Questo vuol dire che le mucche sanno contare?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<address>Questo non lo sappiamo, poiché non siamo in grado di decifrare i loro segnali che potrebbero essere prodotti dagli zoccoli o dagli sbuffi o da una specie di codice morse. In effetti il loro modo di comunicare può essere molto sofisticato.&nbsp;</address>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><b>Cosa fa una mucca per passare il tempo? </b>

Le mucche ruminano. Rigurgitano una parte del cibo per poterla rimasticare ed aiutare il processo digestivo.

Secondo la dott.ssa Doyle, questa caratteristica consente loro di non manifestare i tipici comportamenti stereotipati di alcuni animali costretti in spazi ristretti, come il dondolio da una gamba all'altra o il dondolio della testa.

Quindi, anche se l'umano ha una relazione ormai millenaria con le mucche, avendo addirittura creato nuove razze attraverso la domesticazione e l'allevamento selettivo, la comunicazione rimane criptica. Tuttavia, la comprensione del loro comportamento è in crescita.

Le mucche hanno un'eccellente capacità di apprendimento. Ma anche il modo diverso nel quale sono state allevate può influenzare questa loro capacità. Alcuni interessanti studi condotti dall'Università British Colombia di Vancouver, Canada, hanno dimostrato che il modo nel quale vengono cresciuti i vitelli può avere un'influenza decisiva sulla loro capacità&nbsp;di apprendimento.

Per esempio, se vengono cresciuti in coppia o in compagnia di altri vitelli, la loro capacità di apprendimento è di gran lunga maggiore dei vitelli cresciuti in solitudine.&nbsp;E questo ci porta a riflettere sui sistemi di allevamento e sul benessere degli animali.</p>
<!-- /wp:paragraph -->