<!-- wp:image {"id":1240} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/04/IMG_9104-1024x682.jpg" alt="" class="wp-image-1240"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
All'interno del nostro sito (www.SalvaUnAgnello.com) quest'anno è presente un promettente menù di Pasqua, con prodotti senza alcun ingrediente di origine animale. A realizzarlo è stato Marco Bortolon, classe 1982, chef dal 2006. Per l'occasione Marco ha deciso di rispondere ad alcune nostre domande, in un'intervista che vi riportiamo qui di seguito e che vi invitiamo a leggere e condividere.

Vi suggeriamo di provare il menù proposto da Marco, basta cliccare su questo link
→ <strong><a href="https://salvaunagnello.com/il-menu-di-pasqua-cruelty-free" target="_blank" rel="noopener noreferrer">salvaunagnello.com/il-menu-di-pasqua-cruelty-free</a></strong>

Buon veg appetito!
</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div style="padding: 8px; border: 1px; background: #ededed; border-radius: 5px;"><strong>Ciao Marco, comincerei con la domanda più ovvia: come hai deciso di diventare vegan, e perché?</strong>
<em>Ero in un periodo in cui ragionavo molto sulla libertà individuale e collettiva, su alternative possibili per una vita in armonia con me stesso e con quello che mi circondava. É stata una cosa spontanea, appena scoperto che alcuni miei amici non mangiavano animali, ho intrapreso tale strada, lasciando la libertà ad altre forme di vita.</em>--&gt;&nbsp;<strong>E la seconda domanda, che fa il paio con l’altra: quando è nata la passione per la cucina? E come si è sposata con le tue scelte etiche?</strong>
<em>La passione per la cucina è sorta in maniera graduale, dovendo cucinare per me e successivamente collaborando come volontario ad eventi di beneficenza. Nel proporre piatti vegan ho trovato una mia strada per diffondere la via della non violenza, in parte la vivo come una missione.</em>

--&gt;&nbsp;<strong>L’Italia è un paese con una cultura culinaria radicata e importante, spesso molte persone non riescono a credere che si possa scegliere un’alimentazione rispettosa degli animali senza tradirla: cosa rispondi agli scettici? E quali prospettive vedi per la cucina 100% vegetariana?</strong>
<em>Seguire una dieta vegan in Italia potrebbe essere molto semplice, vista la varietà di prodotti che abbiamo. Bisogna cambiare un po' la prospettiva riguardo al proprio percorso e ovviamente nel 2014 abbiamo la fortuna che esiste internet, con mille fonti per spunti e idee. Senza dimenticare che ci sono oramai molti libri sul tema.
Sicuramente negli ultimi anni la parola 'vegan' si è diffusa abbastanza bene, basta vedere quanti ristoranti veg stanno aprendo negli ultimi tempi. Mi auguro che si radichi in un modo genuino e non 'modaiolo'. Ritengo che sia in atto un'evoluzione culturale importante, la vedo bene.</em>

<strong>Sono alle porte le festività pasquali, e tu hai voluto proporci un menù etico: tradizione o creatività? Come racconteresti il tuo pranzo di Pasqua?</strong>
<em>Il menù che ho proposto è stato pensato per chi non ha solitamente voglia di dedicare troppo tempo alla cucina. Ho voluto tenere un regime di esecuzione facile per incoraggiare chiunque a provarlo. Tra creatività e tradizione direi... c r e a z i o n e!</em>

--&gt; &nbsp;<strong>Scegliere di salvare un agnello (ma anche tutti gli altri animali) è più che una semplice scelta: vorresti salutarci con un augurio o una speranza?</strong>
<em>Un caro saluto e un grande ringraziamento a tutti coloro che leggeranno quest'intervista, e alle fantastiche persone che dedicano le loro energie al miglioramento della qualità di vita non solo umana. Mi auguro di conoscere molti di voi per divulgare, condividere e discutere la scelta vegan. A presto!</em>

</div>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"></p>
<!-- /wp:paragraph -->