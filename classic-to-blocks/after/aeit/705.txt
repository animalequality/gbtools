<!-- wp:image {"id":2231} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/morte-prematura.jpg" alt="" class="wp-image-2231"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Milano - Animal Equality pubblica oggi “<a href="https://campaigns.animalequality.it/pollo-100-italiano/"><strong>Pollo 100% Italiano</strong></a>”, un’investigazione che svela gli agghiaccianti retroscena dell’industria della carne di pollo in Italia. I filmati sono stati raccolti fra Emilia Romagna e Lombardia negli allevamenti intensivi e nei macelli che riforniscono le aziende leader del settore e mostrano una realtà ben diversa da quella che ci raccontano le pubblicità dei grandi marchi.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2226} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/37183996536_2f463bf8f7_k.jpg" alt="" class="wp-image-2226"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Le immagini, documentate anche tramite l’utilizzo di droni, rappresentano le condizioni di vita del 95% dei polli che finiscono ogni anno sulle tavole degli italiani e comprendono:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li>animali stipati a decine di migliaia in capannoni chiusi, sudici e spogli</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>animali con deformazioni alle zampe, zoppie e altri problemi locomotori</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>animali con gravi problemi respiratori</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>animali con gravi affezioni cutanee, tra cui ustioni da ammoniaca, vesciche e ulcere</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>animali con profonde piaghe dovute alla scarsa mobilità</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>animali che muoiono di attacchi cardiaci a pochi giorni di vita</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>cadaveri in avanzato stadio di decomposizione lasciati per settimane sulla lettiera in mezzo agli animali ancora vivi</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>operatori che maneggiano violentemente i polli, spesso causandogli dolorose fratture</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>animali macellati in modo approssimativo, molti dei quali ancora coscienti</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:html -->
<center>
<p><iframe allowfullscreen="" frameborder="0" height="405" src="https://www.youtube.com/embed/bBNbkWzxJt4" width="720"></iframe></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>In Italia si macella un numero di polli sconvolgente</strong>: quasi mezzo miliardo ogni anno. Per soddisfare la crescente domanda di carni bianche a prezzi sempre più bassi, negli ultimi decenni <strong>questi animali sono stati sottoposti a un’esasperata selezione genetica</strong> affinché raggiungano il peso di macellazione (2/3 kg) a sole 6 settimane di vita.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Questa crescita accelerata è la causa principale delle <strong>deformazioni e delle patologie che colpiscono i polli</strong>, condannati così a una vita breve e piena di sofferenza in nome del profitto. Le ossa, i polmoni e il cuore di questi delicati animali non riescono infatti a svilupparsi allo stesso ritmo della muscolatura, causando loro ogni sorta di deformità, difficoltà motorie, problemi cardiaci e respiratori.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2225} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/09/37231058911_b389caa3cd_k.jpg" alt="" class="wp-image-2225"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In questo gigantesco sistema intensivo,<strong> la vita di un pollo vale poco più di un centesimo</strong>. Per questo, prestare cure veterinarie agli animali malati o feriti è visto dall’industria come uno spreco di soldi: nella maggior parte dei casi essi vengono semplicemente abbandonati a una lenta agonia. Per lo stesso motivo, i polli infermi o così deboli da non riuscire a raggiungere le mangiatoie non vengono soccorsi dagli operatori, morendo di fame e sete nel giro di pochi giorni. Così,<strong> ogni anno, milioni di polli muoiono di malattia o stenti ancor prima di arrivare al macello</strong>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Una sorte peggiore attende gli animali che sopravvivono fino al giorno del macello: gli operatori li afferrano violentemente per le zampe, spesso procurandogli dolorose fratture, e li appendono a testa &nbsp;in giù su ganci di metallo. I polli, dibattendosi convulsamente nel tentativo di liberarsi, spesso sfuggono allo stordimento, che risulta così inefficace: così, <strong>ogni giorno, migliaia di polli vengono sgozzati mentre sono ancora coscienti</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Queste sono le misere condizioni in cui 500 milioni di polli sono costretti a vivere ogni anno in Italia», ha dichiarato Matteo Cupi, direttore esecutivo di Animal Equality Italia. «L’industria della carne avicola non può continuare a prendersi gioco dei consumatori con pubblicità ingannevoli, per questo abbiamo deciso di fare chiarezza. C’è bisogno di un cambiamento radicale. ed è questo che chiediamo ai produttori italiani».&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L’inchiesta promuove una petizione su www.polloitaliano.it rivolta ad Unaitalia, associazione di categoria che rappresenta il 90% dell’intera filiera avicunicola nazionale, perché intraprenda al più presto un dialogo con i maggiori produttori di carne di pollo - in particolare AIA, Amadori e Fileni - affinché adottino al più presto politiche volte a ridurre la sofferenza degli animali.<br>
&nbsp;</p>
<!-- /wp:paragraph -->