<!-- wp:image {"id":2072} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/igualdad_animal_senadora_diva_gastelum_2.jpg" alt="" class="wp-image-2072"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>A seguito della denuncia da parte di Animal Equality delle atrocità documentate negli allevamenti e nei macelli messicani,<strong> la senatrice Diva Hadamira Gastélum Bajo&nbsp;ha presentato una proposta di iniziativa a tutela degli animali</strong>. Presentata nella sessione plenaria del Senato e sostenuta da molti altri senatori, l’iniziativa richiede un emendamento della legge federale sulla salute degli animali e un emendamento nel Codice Penale Federale del Messico per il reato di maltrattamento di animali, compresi quelli allevati a scopo alimentare.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La proposta è stata sostenuta da una coalizione nazionale di 92 gruppi per la protezione animale, tra cui Animal Equality, e prevede che l’inadempienza delle normative vigenti sia considerata un vero e proprio crimine.<br>
<br>
<b><img alt="" class="wp-image-2071" src="/app/uploads/2017/03/igualdad_animal_senadora_diva_gastelum_1.jpg" style="width: 800px;">&nbsp;</b></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
La senatrice Diva Gastélum ha dichiarato: «Stiamo cercando di definire le sanzioni nella legge federale per la salute e nel codice penale federale sul maltrattamento degli animali destinati al consumo alimentare, al fine di evitare che situazioni come quelle&nbsp;documentate da Animal Equality si verifichino nuovamente e che eventuali maltrattamenti rimangano impuniti».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Come documentato durante la nostra investigazione all’interno di 31 macelli Messicani, molti dei quali di proprietà del governo, le violazioni delle normative sono gravissime: animali scuoiati vivi, picchiati, folgorati, bruciati e uccisi, tutto questo in stato di totale coscienza, senza alcun tipo di stordimento.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Le norme ufficiali redatte e approvate dal SAGARPA (Ministero dell'Agricoltura messicano) sono inutili se non c'è garanzia che siano rispettate» ha dichiarato <strong>Dulce Ramirez</strong>, direttrice esecutiva di Animal Equality Messic, aggiungendo:&nbsp;«È evidente che le sanzioni attuali non siano sufficienti per raggiungere questo obiettivo, ed è per questo che dobbiamo rafforzare la leggi e accrescere l’attenzione del pubblico verso questa tematica».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Siamo fiduciosi che grazie alla senatrice Gastélum e ai migliaia di cittadini messicani che hanno firmato la nostra petizione, il Senato si mostrerà favorevole a cambiare questa terribile realtà», dichiara Ramirez. «In questo modo stiamo dimostrando come le leggi ufficiali messicane non servano solo a soddisfare convenzioni sulla carta, accordi e mandati internazionali, ma a fare progredire l'intero Paese, in questo caso in materia di protezione animale».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br>
<iframe allowfullscreen="" frameborder="0" height="450" src="https://www.youtube.com/embed/hxv7JTEgmjs" width="800"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Se anche tu, come noi, vuoi che la crudeltà verso gli animali sia un crimine e che questi vengano protetti dalle terribili atrocità a cui sono tuttora sottoposti, per favore firma la nostra petizione all’indirizzo <a href="https://rastrosdemexico.igualdadanimal.mx/">RastrosDeMexico.com</a>, per chiedere ai senatori di approvare questo nuovo regolamento il più presto possibile.</h4>
<!-- /wp:heading -->