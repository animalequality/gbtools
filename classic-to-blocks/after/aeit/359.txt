<!-- wp:image {"id":1281} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/03/16837520292_73b96d7767_z.jpg" alt="" class="wp-image-1281"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>La campagna è lanciata da&nbsp;<a href="https://www.facebook.com/pages/Thegreenplace/411904288886549?fref=ts" target="_blank" rel="noopener noreferrer">The Green Place</a>, un rifugio per animali liberati dallo sfruttamento&nbsp;che ne accoglie oltre 100&nbsp;di&nbsp;differenti specie, inclusi alcuni agnellini salvati da&nbsp;Animal Equality a seguito dell'ultima&nbsp;investigazione realizzata sui trasporti di animali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10663" src="/app/uploads/2015/03/fotocampagnaagnelli_sacrificio2015.jpg" style="width: 237px; border-width: 1px; border-style: solid; margin: 10px 5px; float: left;"><strong>I protagonisti della campagna </strong>sono: <strong>Lina e Elvis</strong>, i due agnellini ultimi&nbsp;arrivati presso il rifugio Thegreenplace. <strong>Coprotagonisti molti&nbsp;personaggi della cultura e dello spettacolo</strong>:&nbsp;<em>Daniela Poggi, Claudia Zanella, Anna Ammirati, Daniela Martani, Loredana&nbsp;Cannata, Diana Del Bufalo, Maria Grazia Capulli, Giovanni Baglioni,&nbsp;Lodovica Mairè Rogati, Alessandra Celletti, Nunzio Fabrizio,&nbsp;Nora Lux, Cristian Stelluti</em>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10662" src="/app/uploads/2015/03/fotocampagnaagnelli_sacrificio2015-2.jpg" style="width: 237px; border-width: 1px; border-style: solid; margin: 10px 5px; float: right;"><strong>Le tante adesioni rispecchiano un forte desiderio di cambiamento</strong> e una nuova cultura in espansione, dove esseri umani e (altri) animali possono convivere&nbsp;in armonia con la natura, nel rispetto dell’altro da&nbsp;sé, senza pregiudizi di specie, senza più sostenere tradizioni crudeli&nbsp;e indicibile violenza. Noi desideriamo la libertà ed il&nbsp;rispetto per&nbsp;tutti gli individui.<br>
&nbsp;</p>
<!-- /wp:paragraph -->