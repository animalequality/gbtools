<!-- wp:paragraph -->
<p>La biologa marina Sylvia Earl una volta affermò "Non mangio mai qualcuno che conosco personalmente" riferendosi ai pesci e alle loro sorprendenti personalità. Gli scienziati concordano: I pesci sono sensibili e intelligenti; sono capaci di utilizzare degli strumenti e, contrariamente a quanto si crede, hanno una memoria a lungo termine. Questi particolari sui pesci vi sproneranno a cercare altre notizie su questi splendidi animali!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>1. Molti pesci vedono al buio meglio dei gatti.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>2. Alcune specie di pesci possono addirittura identificare il livello di stress dei propri compagni!</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1331} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/08/fish8.jpg" alt="" class="wp-image-1331"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>3. Dopo aver nuotato per migliaia di chilometri, i salmoni riescono a riconoscere il posto in cui sono nati semplicemente dall'odore dell'acqua.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>4. I pesci elaborano mappe molto complesse dei luoghi in cui vivono. Imparano dove cercare il cibo, di chi fidarsi o aver paura ed anche con chi accoppiarsi.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10660} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/08/fish9.jpg" alt="" class="wp-image-10660"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Contrariamente a quanto si crede, i pesci hanno una memoria eccezionale. Per esempio, un pesce arcobaleno ha ricordato come uscire da un contenitore 11 mesi dopo averlo fatto la prima e unica volta. E' come se un essere umano ricordasse una lezione imparata 40 anni prima.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La scienza parla chiaro. I pesci non solo sono animali senzienti, ma hanno anche delle capacità eccezionali!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>E' quindi arrivato il momento di lasciare il pesce fuori dal tuo menù, non credi? <a href="http://shop.ivegan.it/pesce-vegetale-c-155.html?osCsid=37iipdgcb6or43stvjtov5p6s1">Prova alcuni sostituti deliziosi e sani come questi!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->