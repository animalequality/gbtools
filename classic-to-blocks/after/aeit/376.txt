<!-- wp:paragraph {"className":"p1"} -->
<p class="p1"><span style="line-height: 20.7999992370605px;">Basilico è stato trovato quando aveva appena due giorni. Era debole e affamato.&nbsp;</span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p1"} -->
<p class="p1"><span style="line-height: 20.7999992370605px;">Basilico è stato trovato non molto lontano da una fattoria. Era destinato ad essere ucciso a circa un mese di età. Milioni di cuccioli come lui vengono macellati ogni giorno quando hanno appena&nbsp; poche settimane di vita. </span></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1290} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/11037568_894446307261750_3532412028238952351_n.jpg" alt="" class="wp-image-1290"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"><span style="line-height: 20.7999992370605px;">E' così che l'industria della carne tratta i cuccioli. Guadagna abusando di animali innocenti e indifesi. Agnelli che ancora prendono il latte dalla madre dalla quale vengono brutalmente strappati.&nbsp;</span></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10494} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/11017064_894446403928407_2603305315348973738_n.jpg" alt="" class="wp-image-10494"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"><span style="line-height: 20.7999992370605px;">Basilico sarebbe stato costretto a salire su un camion, sarebbe stato gettato terrorizzato e inerme sul pavimento di un macello. Avrebbe disperatamente cercato una via d'uscita, avrebbe chiamato sua madre. Gli sarebbe stata tagliata la gola mentre era ancora cosciente.</span></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<div class="ae-video-container"><iframe allowfullscreen="" frameborder="0" height="450px" src="https://www.youtube.com/embed/ndJ4jp-d2rE" width="800px"></iframe></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Ma la storia di Basilico ha avuto un lieto fine.
</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10493} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/11009893_894446570595057_1283548000076176785_n.jpg" alt="" class="wp-image-10493"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">&nbsp;
Animal Equality è intervenuta non appena lo ha visto e lo ha salvato. Basilico è stato portato in un rifugio per animali con il quale Animal Equality collabora, The Greenplace, ed è stato subito nutrito e curato. Ora potrà vivere una lunga&nbsp; e serena vita circondato da persone che lo amano. Non vivrà mai più nella paura, nella solitudine e nel terrore di essere ucciso.
</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10496} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/05/11069925_894446607261720_2026550948541635502_n.jpg" alt="" class="wp-image-10496"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">Basilico è un cucciolo molto gentile e giocoso. Ama crogiolarsi al sole e pascolare con i suoi amici umani e altri animali. Basilico merita una vita libera da crudeltà, così come tutti i suoi fratelli e gli altri animali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"><strong>Aiutaci a continuare a salvare animali come Basilico, <a href="https://animalequality.it/diventa-sostenitore">diventando oggi un sostenitore mensile di Animal Equality</a>.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":10625,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://animalequality.it/diventa-sostenitore"><img src="/app/uploads/2015/04/banner_sostegnofisso_agnello1.jpg" alt="" class="wp-image-10625"/></a></figure>
<!-- /wp:image -->