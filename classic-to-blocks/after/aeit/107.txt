<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Il 7 luglio 2012, un gruppo internazionale di autorevoli ricercatori delle neuroscienze cognitive, farmacologiche, fisiologiche, anatomiche e computazionali, si sono riuniti presso l'Università di Cambridge per rivalutare i substrati neurobiologici dell'esperienza cognitiva e i corrispondenti comportamenti di animai umani e non umani. Nonostante la ricerca comparativa su questo argomento sia stata ostacolata dalla naturale incapacità, tra animali umani e non umani, di comunicare in maniera chiara e semplice i rispettivi stati interni, è stato dichiarato quanto segue:

<em>"L'assenza di una neocorteccia non sembra precludere ad un organismo di provare stati affettivi. Prove convergenti indicano che gli animali non umani possiedono substrati neuroanatomici, neurochimici e neurofisiologici, di stati di coscienza insieme alla capacità di esibire comportamenti intenzionali. Di conseguenza, le evidenze scientifiche mostrano che gli esseri umani non sono gli unici in possesso dei substrati neurologici che generano coscienza. Anche gli animali non umani, tra cui tutti i mammiferi, gli uccelli, i polpi e molte altre specie, sono in possesso di questi substrati neurologici."</em>

Potrebbe non sembrare così importante che un gruppo di scienziati abbia affermato che molti animali non umani siano in possesso di coscienza, ma il riconoscimento pubblico ed ufficiale di questa realtà è invece un grande risultato. <strong>L'enorme quantità di evidenze scientifiche che dimostrano come la maggior parte degli animali siano consapevoli esattamente come lo siamo noi, è un fatto che non può essere più ignorato.</strong>

È molto interessante notare come nella Dichiarazione, sia stata riconosciuta la coscienza in animali molto differenti dall'essere umano, comprese tutte quelle specie che hanno seguito tracce evolutive distanti, come ad esempio gli uccelli e alcuni cefalopodi.

La Dichiarazione è stata proclamata a Cambridge - Regno Unito, il 7 luglio 2012, in occasione della Francis Crick Memorial Conference on Consciousness in Human and non-Human Animals, al Churcill College, Università di Cambridge, da Low, Edelman e Koch. La dichiarazione è stata firmata in presenza di Stephen Hawking, e include nomi come Christof Koch, David Edelman, Edward Boyden, Philip Low, Irene Pepperberg, e molti altri.</p>
<!-- /wp:paragraph -->