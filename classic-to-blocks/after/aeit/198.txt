<!-- wp:image {"id":1159} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/05/482414_10151430036494077_1353932014_n.jpg" alt="" class="wp-image-1159"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Non solo le organizzazioni animaliste, ambientaliste e dei consumatori si stanno mobilitando contro le leggi Ag-Gag, le leggi bavaglio proposte dagli allevatori statunitensi di animali per ostacolare la documentazione della loro detenzione all’interno degli stabilimenti, ma anche il governatore del Tennessee si è pronunciato contro questa legge.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In questo stato sono state raccolte in poco tempo oltre 36.000 firme, mentre al governatore&nbsp;Bill Haslam&nbsp;sono arrivate ben oltre 5.000 telefonate e 16.000 mail di protesta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Le prime leggi Ag-Gag sono state introdotte nel 2011 negli USA e prevedono pene pecuniarie e detentive per tutti coloro che vengono scoperti a riprendere, fotografare e registrare attività all’interno degli allevamenti e dei macelli.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lo scopo di questi provvedimenti è palese: ostacolare nella maniera più restrittiva possibile l’attività di persone, attivisti ma non solo (vedi il caso di <a href="https://animalequality.it/blog/primo-giudizio-della-legge-ag-gag-aver-filmato-un-macello-da-una-pubblica/"> Amy Meyer </a>) che sempre più vogliono portare a conoscenza le crudeltà che ogni giorno, in tutto il mondo, vengono perpetrate sugli animali d’allevamento.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Il governatore lancia così un importante segnale che ci auguriamo vivamente possa fare da apripista a tutti gli altri stati degli Stati Uniti.</p>
<!-- /wp:paragraph -->