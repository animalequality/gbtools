<!-- wp:image {"id":2266} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/11/Markasweb.jpg" alt="" class="wp-image-2266"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Otto tra le dieci maggiori aziende del settore della ristorazione collettiva in Italia si sono impegnate ad abbandonare le uova prodotte da galline rinchiuse in gabbia. Questa volta è Markas, dopo dialogo intrattenuto con Animal Equality, a pubblicare la sua <a href="http://www.markas.it/fileadmin/user_upload/Markas%20International/Dokumente/Markas_Politica_benessere_animale_.pdf">politica “cage-free”</a>, nella quale si impegna ad incrementare sempre più la percentuale di fornitura di uova da allevamenti privi di gabbie, fino a raggiungere il 100% entro la fine del 2024, consacrando la leadership del settore di ristorazione collettiva in Italia come il più attento alle problematiche legate all’uso delle gabbie per le galline ovaiole.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Markas aderisce alla filosofia del pensiero etico che considera il confinamento in gabbia delle galline una pratica negativa per il benessere degli animali e in contrasto con i principi di responsabilità ambientale e sociale alla base delle politiche aziendali», si legge nel comunicato. Le galline allevate in gabbia soffrono enormemente, come documentato dalla nostra scioccante <a href="http://ilveroprezzodelleuova.it/">investigazione</a> negli allevamenti italiani di galline ovaiole, le cui immagini mostrano animali incapacitati di esprimere i loro più basilari comportamenti naturali. &nbsp;&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2267} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/11/Markasweb_0.jpg" alt="" class="wp-image-2267"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La decisione di Markas giunge dopo l’impegno di altre aziende del settore: tra le prime dieci, spiccano gli impegni di aziende come CIR food, Dussmann, Pellegrini e Camst accanto ai giganti internazionali Elior e Sodexo, mentre Serenissima rimane l’unica azienda nella top ten a rifiutarsi di aprire un dialogo in merito e ignorando i nostri reiterati tentativi di contatto.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Le aziende hanno una grande responsabilità e devono fare la loro parte. Ma tu puoi fare di più: il modo più efficace per aiutare le galline è escludere le uova dalla tua alimentazione. Ed è più facile di quello che pensi, guarda <a href="https://campaigns.animalequality.it/cucinare-senza-uova-scarica-ora-il-ricettario-gratuito/">qui</a>: c’è un ricettario in regalo per te!</p>
<!-- /wp:paragraph -->