<!-- wp:paragraph -->
<p>Giunti oramai al terzo anno di campagna, dopo diverse settimane di lavoro intenso e significativo, possiamo fare il punto della situazione grazie a diversi dati in nostro possesso. In tre anni Animal Equality è stata protagonista di una campagna che sta trovando un appoggio sempre più crescente nell’opinione pubblica. <strong>Sebbene la tragedia continui ogni giorno e senza sosta a colpire tantissimi animali</strong>, <strong>la sensibilità è in crescita</strong>, <strong>grazie&nbsp;al coinvolgimento di sempre più persone</strong> e&nbsp;ad un utilizzo&nbsp;più centrato dei mezzi comunicativi che abbiamo a nostra disposizione. La strada è difficile e in salita (l'abbiamo sempre saputo e non l'abbiamo mai negato), ma siamo consapevoli&nbsp;di contare su un numero sempre più crescente di sostenitori&nbsp;che sta iniziando a chiedere a gran voce una considerazione più rilevante per gli animali e&nbsp;un rispetto concreto per la loro capacità di provare quella sofferenza&nbsp;che non possiamo più ignorare.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Il <u><strong>2015</strong></u> ha segnato il raggiungimento di traguardi&nbsp;importantissimi per la <strong>campagna #salvaunagnello</strong>&nbsp;che speravamo con tutto il cuore di ottenere. <strong>L’intera copertura mediatica</strong> ci ha permesso di arrivare ad oltre <strong>13 milioni di persone</strong>, mentre <strong>sui nostri social networks abbiamo raggiunto oltre 10 milioni di persone</strong>, un incremento davvero consistente rispetto al <strong>2014</strong>, quando abbiamo raggiunto <strong>oltre 2 milioni di persone</strong>. <a href="https://www.youtube.com/watch?v=ndJ4jp-d2rE">Il nostro <strong>video</strong> che racconta la vita degli agnelli e capretti&nbsp;in 60 secondi è stato </a><strong><a href="https://www.youtube.com/watch?v=ndJ4jp-d2rE">visualizzato da oltre 540.000 persone</a> </strong>sui nostri canali mediatici;&nbsp;a questi&nbsp;vanno&nbsp;aggiunti i passaggi avuti sui principali siti di informazione nazionali&nbsp;e noti programmi televisivi&nbsp;come <a href="https://business.facebook.com/AnimalEqualityItalia/videos/vb.113071918732530/900921513280896/?type=2&amp;theater">Cronache Animali (Raidue)</a> e <a href="https://business.facebook.com/AnimalEqualityItalia/videos/905734232799624/?business_id=901012179938496&amp;theater">Animali &amp; Animali (Tv2000)</a> che ci hanno permesso&nbsp;in pochi minuti di mostrare a milioni di persone&nbsp;la situazione in cui vivono (e muoiono) questi cuccioli.&nbsp;<a href="http://salvaunagnello.com/campagna-pubblicitaria-agnelli2015/">La campagna fotografica</a> di&nbsp;quest'anno lanciata dal rifugio <a href="http://www.thegreenplace.it" target="_blank" rel="noopener noreferrer"><strong>TheGreenPlace</strong></a> (che ospita anche alcuni degli animali salvati da Animal Equality) e <strong>diffusa&nbsp;da noi in collaborazione con la LAV</strong>,&nbsp;ha avuto una risonanza senza precedenti, grazie soprattutto all'appoggio di diversi testimonials del mondo dello spettacolo:&nbsp; <em>Daniela Poggi, Claudia Zanella, Anna Ammirati, Daniela Martani, Loredana Cannata, Diana Del Bufalo, Maria Grazia Capulli, Giovanni Baglioni, Lodovica Mairè Rogati, Alessandra Celletti, Nunzio Fabrizio, Nora Lux, Cristian Stelluti.</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img alt="" class="wp-image-10670" src="/app/uploads/2015/04/grafico_consumo_agnello.png" style="width: 550px; border-width: 0px; border-style: solid; margin: 10px 5px; float: right;">Attraverso il sito <strong><a href="http://www.SalvaUnAgnello.com">SalvaUnAgnello.com</a></strong>, dal 2013, diffondiamo la nostra richiesta di&nbsp;impegno&nbsp;semplice e concisa: cambiare il destino di questi animali è possibile e la scelta può diventare uno strumento fondamentale nelle nostre mani per evitare che il loro massacro continui. La scelta di&nbsp;non mangiarli può diventare un aiuto concreto. Grazie al nostro&nbsp;sito&nbsp;fino ad ora <strong>oltre 20.000 persone</strong> hanno deciso di impegnarsi concretamente con nome e cognome. Sta crescendo&nbsp;sempre di più la sensibilità nei confronti degli animali ed è importante promuovere campagne come questa, perché&nbsp;le nostre scelte possono influenzare veramente l'andamento di simili tragedie. A dirlo non siamo&nbsp;esclusivamente noi, ma un <strong>organo autorevole come l’Istat</strong>, che&nbsp;ha diffuso i dati sugli&nbsp;<strong>agnelli e capretti macellati: da 4.7&nbsp;milioni nel 2010 a poco più di 2 milioni lo scorso anno</strong>, ovvero una <strong>diminuzione superiore al 50%</strong>, di fatto&nbsp;un cambiamento senza precedenti.&nbsp;Sebbene siamo ben coscienti&nbsp;che esprimersi&nbsp;in numeri riguardo ad&nbsp;una simile tragedia non sia affatto&nbsp;semplice, allo stesso tempo sappiamo&nbsp;che&nbsp;un dato del genere ha tutta la possibilità di&nbsp;tradursi nel&nbsp;salvataggio di milioni di animali. Gli ultimi rilevamenti hanno confermato che la maggioranza di&nbsp;italiani ha deciso di non mangiare&nbsp;più agnello e capretto, facendo ammettere anche ad&nbsp;esperti agroalimentari&nbsp;(Ismea)&nbsp;che si tratta di&nbsp;una delle cause che sta impedendo la crescita di questo specifico&nbsp;settore dell'allevamento. Se vogliamo continuare su questa strada <strong>non deve diminuire l’impegno e il sostegno alla campagna #salvaungnello</strong> e al lavoro che i nostri attivisti e investigatori stanno portando avanti per mostrare&nbsp;chi sono i cuccioli che vengono massacrati ogni anno&nbsp;all'interno dei macelli italiani.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nessuna tradizione può continuare ad essere giustificata in quanto tale, se&nbsp;fondata sullo sfruttamento e la crudeltà; è ora di iniziare a 'costruire' nuove tradizioni basate sul rispetto e&nbsp;la tolleranza verso gli animali, da tramandare con una ritrovata e profonda sincerità&nbsp;a chi verrà dopo di noi.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->
<h6 class="wp-block-heading"><img alt="" class="wp-image-1287" src="/app/uploads/2015/04/stef_bettini.jpg" style="width: 180px; border-width: 0px; border-style: solid; margin: 10px 5px; float: right;"><br>
<strong>Stef Bettini</strong><br>
<em>Responsabile Investigazioni in Italia per&nbsp;Animal Equality</em></h6>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->