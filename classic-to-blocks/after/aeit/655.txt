<!-- wp:image {"id":2050} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/03/13285813185_9dcc28a965_z_1.jpg" alt="" class="wp-image-2050"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Ieri abbiamo appreso che <strong>il Partito Popolare Europeo (PPE)</strong> - Il gruppo cristiano democratico-conservatore dell’EU ha presentato una "risoluzione di mozione alternativa" al Parlamento europeo a proposito della relazione presentata dell'<strong>eurodeputato Stefan Eck</strong> che mira a disciplinare gli <strong>allevamenti di conigli in Europa</strong> e che potrebbe raggiungere l’abolizione delle <strong>gabbie negli allevamenti</strong>.

Questo è un passo insolito, soprattutto perché la relazione del deputato europeo Stefan Eck era già stata votata positivamente da una maggioranza schiacciante (e in parte con i voti del PPE), nella commissione per l'agricoltura dello scorso gennaio.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/N6bmFvAypC0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

La relazione di Eck si propone di porre fine alle crudeltà dell’allevamento in gabbia e introdurre standard minimi obbligatori per l’allevamento di coniglio (al momento non vi è nessuna normativa per questi animali). Il movimento alternativo del PEE, invece, prevede solo una raccomandazione per migliorare il benessere dei conigli senza alcuna normativa vincolante per l'industria.
Con questo provvedimento il PPE sta cercando di impedire misure efficaci per proteggere gli animali e annullare così la relazione di Eck.

<strong>Si tratta di una reazione disperata alla nostra campagna!&nbsp;Ora più che mai dobbiamo dimostrare agli eurodeputati ciò che chiediamo: una protezione seria per gli animali in Europa!</strong></p>
<!-- /wp:paragraph -->