<!-- wp:paragraph -->
<p><strong>Natasha Brenner</strong>, un'attivista per i <strong>diritti animali</strong> di New York, ha appena festeggiato<strong> i suoi&nbsp;primi 95 anni</strong>.

Questa incredibile donna è diventata vegetariana nel <strong>1992</strong> ed ha completamente abbandonato i prodotti di origine animale nel <strong>2012</strong>.

Se hai 95 anni ed hai 24anni di attivismo alle spalle, significa che hai deciso di metterti in gioco…<strong> a 71 anni</strong>!
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">"Sono un'attivista perché è mia intenzione eliminare il più possibile la sofferenza degli animali."</h4>
<!-- /wp:heading -->

<!-- wp:html -->
<center><iframe src="https://www.youtube.com/embed/kwKl3u63yqs?rel=0&amp;showinfo=0" width="850" height="478" frameborder="0" allowfullscreen="allowfullscreen"></iframe></center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>In quasi un quarto di secolo di attivismo, Natasha ha partecipato ad un impressionante&nbsp;numero di campagne:
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">"Quando mio marito era ancora in vita, abbiamo partecipato ad ogni protesta possibile in città (New York, ndr)."</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Dopo la scomparsa del marito, le condizioni fisiche di Natasha hanno iniziato a rendere più complessa la sua attività di attivista. Questo però non l'ha di certo fermata:
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">"Ora mi metto al computer ogni giorno, per lavorare per gli animali o contribuire alle petizioni."</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Questa donna eccezionale è un esempio per ognuno di noi: è la prova vivente di come non sia <strong>mai troppo tardi per mettersi in gioco</strong> ed allo stesso tempo un esempio da seguire quando si è un <strong>po' troppo timidi nel prendere parte al cambiamento</strong>.

Non potevamo che chiudere questo articolo che con questa sua citazione:
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">"Non ho dubbi: siamo e saremo sempre di più. Le persone si renderanno conto che non solo stiamo salvando vite, ma stiamo salvando anche le <strong>nostre</strong> vite."</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Tanti auguri Natasha!</p>
<!-- /wp:paragraph -->