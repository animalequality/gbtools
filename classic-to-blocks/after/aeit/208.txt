<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">E' di qualche giorno fa la tragica notizia della morte di Clément Méric, ragazzo di 18 anni, francese, attivista vegan impegnato in diverse lotte sociali. Abbiamo deciso di raccontare cosa è accaduto a Clément perché riteniamo importante esprimere il profondo dolore e la tristezza per un ragazzo che ha lottato contro molteplici forme di ingiustizia e che per colpa di quest'ultime, è morto.<br>
	<br>
	Animal Equality cerca, ogni giorno, di lottare dando voce e visibilità agli animali sfruttati in questa società e lo fa partendo dal presupposto che i sentimenti di rispetto, uguaglianza e solidarietà siano alla base di un mondo tutto da costruire, insieme. <strong>Clément era un attivista vegan, impegnato contro lo specismo e contro tutte quelle forme di oppressione che conosciamo e vediamo spesso manifestarsi attorno a noi.</strong> Una di queste forme, il razzismo, lo ha colpito a morte mercoledì 5 giugno, per mano di alcuni individui con idee xenofobe e intolleranti. Clément è stato circondato ed aggredito, l'autopsia ha confermato che è morto per i colpi ricevuti alla testa dagli aggressori, poi tutti identificati. Tre di questi sono stati trattenuti, due rilasciati sotto sorveglianza ed uno attualmente in custodia cautelare.<br>
	<br>
	<strong>Raccontiamo ciò che è accaduto a Clément prima di tutto perché l'odio verso il suo impegno per una società più giusta per ogni individuo (così come testimoniato da tutti i suoi amici) e la sua morte ingiusta a 18 anni suonano come un campanello di allarme, un'atroce testimonianza di un mondo che vogliamo cambiare e che vogliamo privo di questa cieca violenza, frutto del razzismo.</strong> A causa dell'odio sono tante, troppe, le vittime della discriminazione che accomuna in questi casi esseri umani ed animali, un collegamento che non può essere scisso e che non possiamo negare, perché impegnarsi concretamente nel rendere liberi gli animali, nel far sì che vengano considerati con quel rispetto di cui sono degni, non può essere contornato da una società in cui la violenza razzista o di qualsiasi altra matrice discriminatoria siano valori accettati e condivisi. Da tragiche vicende del genere possono però nascere semi di speranza, uno spirito rinnovato che vuole generare sentimenti e idee diverse da quelle guidate da una cieca mano violenta, che si infrange costantemente su chi è più debole o senza difese; una speranza che vediamo espressa nelle migliaia di persone scese in strada a Parigi e in tantissime altre città della Francia, per manifestare e per ricordare che la morte di Clément Méric non verrà mai dimenticata.<br>
	<br>
	E con un'immagine di speranza, ovvero un autentico <em>'fiume di persone'</em> che ha invaso il centro di Parigi sabato 8 giugno, che chiudiamo questo breve articolo, realizzato per ricordare che mercoledì scorso, un ragazzo di 18 anni, francese, attivo in difesa di chi è oppresso, inclusi gli animali, è stato ucciso per le sue idee da chi le idee di cambiamento le vuole represse, come in questo caso, nel sangue e nella violenza.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1167} -->
<figure class="wp-block-image"><img src="/app/uploads/2013/06/protesta-per-Clément-Méric.jpg" alt="" class="wp-image-1167"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->