<!-- wp:image {"id":1465} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/15881989400_d02c2713e2_m_0.jpg" alt="" class="wp-image-1465"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><a href="http://www.cia.it/news?p_p_id=visualizzaarticoli_WAR_visualizzaarticoliportlet_INSTANCE_FiFVNpMIQJX2&amp;p_p_lifecycle=0&amp;p_p_state=normal&amp;p_p_mode=view&amp;p_p_col_id=column-2&amp;p_p_col_pos=2&amp;p_p_col_count=6&amp;_visualizzaarticoli_WAR_visualizzaarticoliportlet_INSTANCE_FiFVNpMIQJX2_articleId=975783&amp;_visualizzaarticoli_WAR_visualizzaarticoliportlet_INSTANCE_FiFVNpMIQJX2_action=getArticolo">Secondo le stime della Confederazione Italiana Agricoltori</a> <strong>il consumo di carne di agnello ha subito quest'anno un'ulteriore flessione del 10% e sono ormai 7 famiglie su 10 quelle che hanno scelto la compassione, almeno verso questi cuccioli</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Anche quest'anno abbiamo raggiunto milioni e milioni di telespettatori attraverso le nostre immagini trasmesse sul <a href="https://www.facebook.com/AnimalEqualityItalia/videos/1089501571089555/">TG1</a> e <a href="https://www.facebook.com/AnimalEqualityItalia/videos/1087342774638768/">La Gabbia</a>, senza contare <a href="https://www.facebook.com/AnimalEqualityItalia/videos/1084487748257604/">le splendide parole spese dal Capitano Paolo Sottocorona</a>, meteorologo de La7, sulla nostra campagna #SalvaunAgnello che hanno sicuramente suscitato una riflessione importante sull'argomento.<br>
&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1463} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/03/15881989400_5789bbe41f_h.jpg" alt="" class="wp-image-1463"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Anche in strada il mese di marzo è stato impegnativo ed <a href="https://www.facebook.com/media/set/?set=a.1084564588249920.1073741911.113071918732530&amp;type=3">abbiamo realizzato tre eventi di sensibilizzazione</a>. Attraverso i nostri filmati su tablet e la distribuzione di opuscoli e volantini informativi abbiamo raggiunto un numero ragguardevole di persone.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Abbiamo fatto del nostro meglio in questi 4 anni e continueremo a farlo, per gli agnelli e tutti gli animali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Il nostro ringraziamento va a chi ci segue e ci sostiene per fare meglio e di più.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Il vostro sostegno salva delle vite! Grazie di cuore.</p>
<!-- /wp:paragraph -->