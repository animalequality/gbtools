<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">In un primo momento le galline sarebbero dovute essere 50, ma alla fine se ne sono aggiunte 22 più del previsto, ed è stato così possibile salvare 72 individui da morte certa. Per l'allevatore non erano altro che mezzi da sfruttare finché produttivi, e da eliminare quando non più redditizi. <strong>Se Igualdad&nbsp;Animal / Animal&nbsp;Equality e Equanimal non si fossero preoccupate di salvarli e di dar loro la possibilità di una vita diversa, sarebbero stati tutti uccisi.</strong><br>
	<br>
	<img alt="" src="https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-ash3/545129_10151228405773345_855020316_n.jpg" style="width: 280px; margin: 1px 5px; float: left; ">Negli allevamenti biologici, così come in tutti gli altri, il valore della vita di un animale è valutato in base al profitto che può produrre; quando il guadagno di chi sfrutta diminuisce è arrivato il momento di mandare gli animali al macello.<br>
	<br>
	<img alt="" src="https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-snc6/s720x720/181148_10151228423113345_534531546_n.jpg" style="width: 250px; margin: 5px; float: right;">In Spagna, oltre 47 milioni di galline vengono sfruttate per soddisfare la domanda di uova e la realtà italiana non è poi così diversa; nel nostro paese sono oltre 50 milioni le galline ovaiole uccise ogni anno. Altrettanti pulcini maschi sono vittime, spesso dimenticate, di questo consumo: non destinati dall'uomo a diventare carne e non predisposti dalla natura a produrre uova, vengono eliminati alla nascita, macellati, schiacciati vivi o gasati.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><strong>Queste 72 galline sono state ospitate nei rifugi El Hogar de Luci e Wings of Heart</strong>: qui potranno finalmente essere libere da qualsiasi forma di sfruttamento.<br>
	<br>
	Il riscatto è stato reso possibile grazie alle donazioni di tutti coloro che hanno sostenuto le due organizzazioni. Il noleggio dei furgoni, il carburante e altre spese accessorie hanno avuto un ammontare di 695,72 euro. La raccolta fondi lanciata per fronteggiare questa emergenza ha permesso di arrivare a 1.653,02 euro; tutto il rimanente è stato equamente diviso tra i due rifugi per le spese veterinarie e quotidiane per far fronte alle esigenze degli animali salvati.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify"><img alt="" src="https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-ash3/165854_10151228442523345_503477570_n.jpg" style="width: 270px; margin: 10px 5px; float: left; ">Nessuno dei 72 animali liberati è stato comprato perché un individuo non può avere un costo, perché se lo avesse significherebbe utilizzare la logica di chi misura il valore di qualcuno in base a ciò che è capace di produrre. Pagare per un individuo non significa liberarlo perché per quello che si porta via ce ne saranno altri per i quali comincerà una vita di prigionia, e questo anche a causa del sostegno economico che abbiamo fornito all'allevatore. Un atto di liberazione porta nell'immediato la libertà di chi ce l'ha fatta ma deve essere un gesto di speranza per tutti coloro che sono ancora rinchiusi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtejustify"} -->
<p class="rtejustify">Da oggi per 72 individui inizia una nuova vita.<br>
	Grazie a tutti coloro che hanno reso questa liberazione possibile.</p>
<!-- /wp:paragraph -->