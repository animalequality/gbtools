<!-- wp:image {"id":2368} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/25116087347_59313f9ae4_m.jpg" alt="" class="wp-image-2368"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Arrivano aggiornamenti dalla Spagna sul caso ElPozo, <a href="https://animalequality.it/news/2018/02/14/scandalo-spagna-nuova-investigazione-svela-lorrore-dietro-i-prodotti-elpozo-marchio-di/">una drammatica vicenda di abusi e orrori che riguarda i maiali allevati per la produzione di salumi e carne spagnoli</a> all’interno di un allevamento della Murcia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per fortuna, dopo una visita del 25 febbraio, il nostro team in Spagna ha confermato che l’allevamento al momento non è più in attività.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Oltre a questo annuncio, i nostri investigatori hanno rilasciato un nuovo video e immagini inedite dell’allevamento della Murcia "Hermanos Carrasco", denunciato in collaborazione con la troupe del programma investigativo spagnolo Salvados.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Come dimostrato dai video, le condizioni in cui vivevano gli animali allevati da questo fornitore del marchio spagnolo ElPozo erano drammatiche e pericolosissime.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<p><iframe allow="encrypted-media" allowfullscreen="" frameborder="0" gesture="media" height="405" src="https://www.youtube.com/embed/kA5q9tjbI3w" width="720"></iframe></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«La denuncia che abbiamo depositato dinanzi alla Corte di Totana ha portato a una serie di indagini tuttora in corso. Devono accertare tutte le responsabilità, perché questo è stato, senza ombra di dubbio, uno dei più dei casi di maltrattamento animale più scandalosi nella storia recente della Spagna», dichiara Javier Moreno, direttore di Animal Equality in Spagna.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2364} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/39955127032_6d9a71236d_z-1.jpg" alt="" class="wp-image-2364"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>E aggiunge: «Oltre all'ovvia sofferenza a cui sono stati costretti, questi animali malati avrebbero potuto essere mandati al macello e le parti considerate "adatte al consumo umano" avrebbero potuto entrare nella normale filiera di produzione, finendo così sulle tavole consumatori. Ovviamente tutto questo è oggetto di indagine da parte delle autorità e verrà verificato nel tempo».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nel frattempo, in Spagna continua la campagna pubblica - che ha raccolto finora 130.000 firme -&nbsp;per chiedere a ElPozo di impegnarsi pubblicamente in una politica sul benessere degli animali.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2365} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/26114230378_3fe0dd0286_z-1.jpg" alt="" class="wp-image-2365"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In Italia invece i prodotti a marchio ElPozo continuano ad essere distribuiti su Amazon Market Place, ed è per questo che la nostra campagna non si ferma.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2366} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/39955124262_a909e654e8_z.jpg" alt="" class="wp-image-2366"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Chiediamo infatti ad Amazon Italia di prendere immediatamente le distanze da un marchio colpevole di osceni maltrattamenti sugli animali e di aver mentito ai propri clienti con pubblicità ingannevoli, mettendo a rischio la salute dei consumatori. Inoltre, chiediamo che cessino immediatamente le vendite di prodotti El Pozo sul loro sito.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->