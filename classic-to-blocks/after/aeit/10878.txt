<!-- wp:paragraph -->
<p>I protagonisti di queste storie hanno avuto la fortuna di essere amati e protetti nei rifugi per animali da allevamento. Le loro vite reali non hanno nulla a che spartire con quelle dei loro sfortunati simili, reclusi negli allevamenti e infine uccisi nei macelli. Questi adorabili animali sono ambasciatori di un messaggio universale che vale per tutti gli animali da allevamento "Per favore non farci uccidere!"

Ora vivono in due rifugi; lo scopo dei rifugi è proprio quello di farci capire quanto maiali, agnelli, vitelli, polli e altri animali da allevamento, destinati a diventare cibo, siano irresistibilmente dolci. Ringraziamo quindi Farm Sanctuary e Edgar's Mission che danno loro ogni giorno protezione e amore oltre che la possibilità a tante persone di conoscerli da vicino.

Vi presentiamo questi meravigliosi (e talvolta un po' birichini) animali. E' facile aprire il nostro cuore appena abbiamo la possibilità di conoscerli!

<strong>1. Michael, un vitello fortunato.</strong>

Michael è nato in un allevamento di mucche da latte. In questi allevamenti i vitelli vengono subito sottratti alle madri per avere il loro latte da poter vendere aisupermercati. I vitelli maschi vengono venduti per la carne e inviati al macello quando hanno appena un paio di giorni.

Ma Michael è stato fortunato: l'allevatrice vide qualcosa nei suoi occhi e decise di non venderlo subito, di aspettare il prossimo camion verso il mattatoio. Nel frattempo, un attivista per i diritti animali, la convinse però a donarglielo e lo portò al Farm Sanctuary, nello stato di New York, dove ora Michael cresce felice, libero di correre e giocare sommerso di coccole e da tanto amore.

<strong>2. Arabesque, l'agnellina che ha conquistato il cuore di chi la doveva uccidere...</strong>

Il destino di Arabesque era già scritto: morire dopo aver visto la madre colpita a morte dal macellaio. Tuttavia, qualcosa di diverso accadde quel giorno. Il belato terrorizzato di Arabesque catturò l'attenzione di un lavoratore del macello che, mosso a compassione, prima la portò a casa e poi a Edgar's Mission. Qui la piccola è riuscita a dimenticare il trauma della perdita della madre, grazie all'amore e alle cure che gli assistenti del rifugio le dedicano ogni giorno.

<strong>3. Undici fortunatissimi polli scampano alla griglia grazie al buon cuore di un acquirente!</strong>

Anche questa è una storia a lieto fine. L'acquirente di questi animali sensibili e curiosi era stato assunto come assistente presso il Farm Sanctuary, in California. Poco prima di essere assunto, la sua famiglia aveva acquistato dei pulcini allo scopo di farli crescere nel proprio giardino per poi mangiarli, poiché non volevano mangiare polli provenienti da allevamenti intensivi.

Ma a volte la realtà supera la fantasia. Quando il nuovo assistente del Farm Sanctuary vide come gli animali vivevano nel rifugio, la sua percezione di essi cominciò a cambiare. Dopo aver visto che gli animali da allevamento restituivano l'amore che gli veniva dato da chi si prendeva cura di loro, qualcosa dentro di lui è cambiato ed è nata una nuova consapevolezza. Non avrebbe mai più potuto uccidere i polli che vivevano nel suo giardino... E non solo, ha anche deciso di agire contro il maltrattamento degli animali negli allevamenti e nei macelli non mangiando più carne e adottando uno stile di vita vegetariano!

<strong>4. Little Miss Sunshine, l'esempio di come la compassione possa cambiare il cuore di ognuno di noi.</strong>

Questa storia vi farà piangere di gioia; è la storia di una piccola gallina divenuta ambasciatrice di ogni gallina sfruttata nella produzione avicola. Little Miss Sunshine viveva con altre 1.081 galline in un allevamento di galline ovaiole. Il suo destino sarebbe stato quello di tutte le galline ovaiole: vivere per tutta la vita in una piccola gabbia per poi essere inviata ad un macello, quando non poteva più deporre abbastanza uova.

Ma qualcosa di meraviglioso è accaduto; l'allevatore ha deciso di cambiare vita e di non voler più sfruttare quelle povere, sofferenti galline. Non poteva più vederle dentro quelle piccole gabbie e allora chiamò Edgar's Mission per dare loro una vita migliore. E chissà, forse è stata proprio Little Miss Sunshine a far aprire i suo cuore!

Al suo arrivo presso Edgar's Mission, Little Miss Sunshine si mise subito accoccolata al sole e una bambina di sette anni che si trovava lì in quel momento esclamò: "guarda, non ha mai visto il sole eppure le piace, chiamiamola Little Miss Sunshine!"

<strong>5. Polly, la più adorabile e dolce ambasciatrice di maiali.</strong>

Polly è veramente una grande ambasciatrice per tutti gli animali che vivono ora presso Edgar's Mission. Tra i suoi doveri di ambasciatrice quelli di essere adorabile, sorridente e giocare con i suoi compagni, come se non ci fosse un domani!

Polly è famosa per le sue birichinate e proprio come una grande superstar spesso assume atteggiamenti da diva. Durante le sue scorribande nel rifugio non disdegna di smangiucchiare&nbsp;l'ultimo GPS appena acquistato e ogni volta che vede telecamere in giro fa di tutto per catturare l'attenzione del personale del rifugio, come solo una vera star sa fare!</p>
<!-- /wp:paragraph -->