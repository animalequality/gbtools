<!-- wp:paragraph {"className":"p1"} -->
<p class="p1">Il disegno di legge determina che l'uccisione, la vendita, il consumo e il possesso di bovini è vietato. E non solo è vietato il consumo di carne ma viene introdotta una pena detentiva di cinque anni per chiunque possieda un bovino.&nbsp;In India le mucche sono considerate sacre dalla religione indù, ma l'industria della carne è in genere gestita da musulmani.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1304} -->
<figure class="wp-block-image"><img src="/app/uploads/2015/06/beef-cattle.jpg" alt="" class="wp-image-1304"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p1"} -->
<p class="p1">Il Primo ministro del Maharashtra, Devendra Fadnavis, ha twittato i suoi ringraziamenti al presidente, dichiarando che&nbsp;<em>"Il nostro sogno di vietare la macellazione delle mucche è diventato ormai una realtà."</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">&nbsp;</p>
<!-- /wp:paragraph -->