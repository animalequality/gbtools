<!-- wp:image {"id":1608} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/investigatori_animal_equality_fb.jpg" alt="" class="wp-image-1608"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">1. I nostri investigatori sono persone <strong>normali</strong>.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Gli investigatori di Animal Equality sono uomini e donne normali che hanno semplicemente deciso di fare la propria parte nella lotta alle ingiustizie.

Si sono formati professionalmente ed hanno deciso di <strong>mostrare al mondo</strong> la crudeltà dell'industria della carne.

&nbsp;

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1595} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/investigatori_animal_equality_1.jpg" alt="" class="wp-image-1595"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">2. I nostri investigatori rientrano dalle loro missioni con delle immagini di abusi e crudeltà orribili.<strong> Ogni volta</strong>.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Per una persona con una particolare attenzione alla vita animale, trovarsi di fronte a queste scene di crudeltà costante non è semplice.&nbsp;

Ma sarebbe molto più difficile voltarsi e far finta di non vedere.

&nbsp;

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1596} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/investigatori_animal_equality_5.jpg" alt="" class="wp-image-1596"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">3. Questi abusi e queste crudeltà, senza il lavoro dei nostri investigatori, sarebbero rimasti <strong>nascosti</strong>.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Vi siete mai chiesti come mai sia così difficile <strong>entrare</strong> all'interno di allevamenti e mattatoi?

Oppure perché il <strong>marketing</strong> dell'industria della carne investa così tanti soldi per farci vedere immagini che <strong>non rappresentano</strong> assolutamente la <strong>realtà</strong> degli <strong>allevamenti</strong>?

Perché non c'è nulla di più vero di questo aforisma:
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><em>"Se i macelli avessero le pareti di vetro, saremmo tutti vegetariani"</em></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><img alt="" class="wp-image-1597" src="/app/uploads/2016/07/investigatori_animal_equality_3.jpg"></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">4. Il lavoro di investigazione e denuncia <strong>paga</strong>.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Le violazioni denunciate dai nostri investigatori<strong> non rimangono impunite</strong>.

Le attività che operano in modo irregolare vengono <strong>chiuse</strong> e le persone responsabili sono chiamate a rispondere delle proprie azioni <strong>di fronte alla legge</strong>.

&nbsp;

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1598} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/investigatori_animal_equality_4.jpg" alt="" class="wp-image-1598"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">5. Il lavoro di investigazione e denuncia ha un'<strong>impatto positivo</strong> sulla società.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
La maggiore informazione portata dal lavoro degli investigatori contribuisce alla creazione di un'opinione pubblica via via sempre più consapevole e critica.

Consumatori più informati compiono scelte più accurate e, modificando la domanda, anche l'offerta si deve adeguare.

Il risultato è sotto i nostri occhi ogni volta che vediamo aumentare i prodotti cruelty free nei negozi e nei supermercati.

&nbsp;

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1599} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/07/investigatori_animal_equality_2.jpg" alt="" class="wp-image-1599"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">6. <strong>Tu</strong> hai un ruolo <strong>fondamentale</strong> nel lavoro d'investigazione.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Se il risultato delle nostre indagini contribuisce alla maggiore sensibilizzazione dell'opinione pubblica, è solo grazie a tutti quei contenuti che voi condividete con i vostri amici e parenti ogni giorno.

Grazie per supportare il lavoro di Animal Equality <strong>condividendo sempre</strong> i nostri contenuti!&nbsp;

&nbsp;

&nbsp;</p>
<!-- /wp:paragraph -->