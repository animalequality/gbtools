<!-- wp:image {"id":1434} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/02/tuttoilrestoèsoia_0.jpg" alt="" class="wp-image-1434"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>I ragazzi di <strong>Vegan Chronicles</strong> sono attivi da un paio d'anni e di sicuro molti di voi conosceranno già il loro lavoro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Si tratta di una <strong>webserie comica, che racconta l'impatto della scelta veg</strong>&nbsp;sulla quotidianità&nbsp;di Claudio, che oltre ad essere attore è co-autore della serie.<br>
Con una <a href="https://www.youtube.com/watch?v=2WJ5YIrrYAQ">puntata pilota ispirata a Full Metal Jacket</a>, la loro opera ha vinto il&nbsp;Taormina film fest 2015, ma i&nbsp;teaser <strong>avevano già iniziato a farci ridere e riflettere dal lancio del promo</strong> nel 2014.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In un clima in cui la satira sulla scelta&nbsp;veg spopola&nbsp;evidenziandone spesso&nbsp;il lato più noioso, <strong>Vegan Chronicles</strong><strong>&nbsp;dimostra&nbsp;che anche chi sceglie uno stile di vita cruelty-free è capace di (ottima) autoironia</strong>, affrontando con il sorriso tutti quei piccoli paradossi che si creano nella vita quotidiana di chi decide di vivere ogni giorno nel rispetto degli animali.<br>
Seguiteli su <a href="https://www.facebook.com/veganchronicles/">Facebook</a> e su <a href="https://www.youtube.com/channel/UC_uOMTXJZI0KBrKqbiS852A">YouTube</a>.&nbsp;Buona visione!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;<iframe allowfullscreen="" frameborder="0" height="450" src="https://www.youtube.com/embed/LWqsjkwUXn4" width="800"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->