<!-- wp:image {"id":2320} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/02/25116087347_59313f9ae4_z.jpg" alt="" class="wp-image-2320"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>A seguito delle rivelazioni da parte di Animal Equality in collaborazione con il programma investigativo spagnolo Salvados, sono diverse le aziende europee che hanno deciso di interrompere i rapporti con ElPozo.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In Belgio, <strong>due delle catene principali di supermercati, Delhaize e Colruyt, <a href="https://elpais.com/economia/2018/02/12/actualidad/1518460190_212018.html">hanno deciso di sospendere la vendita di prodotti ElPozo</a></strong>, ritirando sia i prodotti già in vendita sia quelli in stock, una scelta adottata anche dalla catena tedesca REWE. Nel Regno Unito, la catena di supermercati Morrisons ha annunciato approfondimenti sul caso.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2317" src="/app/uploads/2018/02/investigazioni_maiali.gif" style="width: 560px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Questa è la buona notizia in una vicenda ancora lontana dall’essere risolta.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per cercare di contenere i danni, <a href="http://www.elpozo.com/en/news/elpozo-alimentacion-extends-its-quality-standards-to-increase-the-animal-welfare-guarantees/">ElPozo ha rilasciato un comunicato</a> in cui ha dichiarato di aver interrotto i rapporti con l’allevamento incriminato e di volersi impegnare a migliorare le condizioni di vita degli animali, ma si tratta di una dichiarazione che non descrive chiaramente le misure che verranno adottate né specifica una data per la loro implementazione.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Questa posizione però costituisce un’importante prova di come<strong> l’azienda stia percependo la pressione delle nostre firme e della ripercussione mediatica ottenuta a seguito dell'investigazione presentata</strong>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Stiamo parlando di un’azienda che si è dimostrata più volte colpevole di non aver a cuore la condizione degli animali allevati a scopo alimentare dai suoi fornitori, come dimostrano anche casi precedenti.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Nel 2012 infatti Animal&nbsp;Equality aveva documentato i maltrattamenti agghiaccianti che avvenivano in uno degli allevamenti spagnoli fornitori del colosso della carne e dei salumi.&nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2318" src="/app/uploads/2018/02/investigazione_macello_maiali_1.jpg" style="width: 600px; "></figure><p><strong></strong></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>L’inchiesta - divenuta celebre sulla stampa internazionale come “<a href="https://www.youtube.com/watch?v=GNhJ3eyz2Zc">il caso El Escobar</a>” - aveva mostrato episodi di vere e proprie torture nei confronti di scrofe, sezionate ancora coscienti con i cuccioli in grembo e animali malati uccisi a colpi di spada tra le risate degli operatori.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per tutti questi motivi, non possiamo fermarci proprio ora.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center>
<figure><img alt="" class="wp-image-2316" src="/app/uploads/2018/02/40207225002_b701cbbc43_k.jpg" style="width: 840px; "></figure><p></p>
</center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Grazie alla nostra investigazione e al <a href="https://www.independent.co.uk/news/uk/home-news/morrisons-amazon-uk-spanish-sausages-el-pozo-pigs-animal-equality-animal-welfare-farming-a8195571.html">supporto dei media internazionali</a>, siamo riusciti a raccogliere più di 150,000 firme fra Italia, Spagna, Regno Unito e Germania: vogliamo aumentare ancora di più la pressione affinché tutti i rivenditori di prodotti targati ElPozo rispondano al nostro appello.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->