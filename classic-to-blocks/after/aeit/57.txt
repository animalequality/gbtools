<!-- wp:image {"id":1042} -->
<figure class="wp-block-image"><img src="/app/uploads/2012/07/granjasfoiegras.jpg" alt="" class="wp-image-1042"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Animal Equality ha documentato l'intero processo di produzione del foie gras negli allevamenti catalani; le anatre arrivano dagli incubatoi spagnoli e francesi quando hanno ancora poche ore di vita, vengono immediatamente sottoposte all'alimentazione forzata e infine uccise.<br>
	<br>Il team investigativo di Animal Equality ha contattato durante l'anno passato, tutti i produttori di foie gras, in Catalogna così come nel resto del paese, riuscendo a realizzare un quadro complessivo dettagliato di questo settore dell'industria. I dati che sono stati raccolti superano ampiamente le statistiche ufficiali disponibili sulla produzione del foie gras.<br>
	<br>
	I nostri investigatori hanno visitato cinque allevamenti in Catalogna, tra cui uno dei più produttivi, fornitore della compagnia 'Interpalm' di proprietà del presidente dell'associazione spagnola dei produttori di foie gras.<br>
	<br>
	Gli attivisti che hanno realizzato questa investigazione hanno assistito a scene di sconvolgente sofferenza fisica e psicologica inflitta agli animali, in tutti gli allevamenti visitati. Oltre <a href="https://www.flickr.com/photos/igualdadanimal/sets/72157630427261184/"> 350 fotografie </a> inedite e decine di ore di video, documentano la vita drammatica delle anatre alimentate forzatamente per la produzione di foie gras in Catalogna, e permettono di fare le seguenti osservazioni:<br>
	<br>
	• gli animali sono confinati singolarmente in piccole gabbie in cui non riescono nemmeno a girarsi<br>
	• le anatre danno dei chiari segnali di stress e depressione<br>
	• gli animali hanno evidenti problemi di respirazione<br>
	• le anatre, indebolite e malate, vengono lasciate sole a morire senza alcuna cura veterinaria<br>
	• molti animali muoiono all'interno delle gabbie<br>
	• le anatre arrivano coscienti al momento della macellazione, continuano a sbattere incessantemente le ali mentre muoiono dissanguate.<br>
	• un operaio pestava il collo di un'anatra col il coperchio di una scatola<br>
	• i lavoratori trattano violentemente le anatre durante il trasporto verso il macello<br>
	• un operaio infastidiva un'anatra come se fosse una forma di intrattenimento, provocandole forte stress<br>
	• gli animali si muovono con difficoltà a causa delle abnormi dimensioni del fegato<br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://player.vimeo.com/video/45216589" width="420"></iframe></p>
<!-- /wp:paragraph -->