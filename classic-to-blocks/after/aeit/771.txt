<!-- wp:image {"id":2484} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/11/rsz_214.jpg" alt="" class="wp-image-2484"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>“Entro il 1° luglio 2019 Conad eliminerà da tutto il suo assortimento le uova da galline allevate in gabbia”. Con questo <a href="https://www.conad.it/news/2018/11/19/Conadentroluglio2019addioatutteleuovadaallevamentoingabbia.html">impegno</a>, che giunge a seguito del lavoro di Animal Equality e altre organizzazioni operanti sul territorio, le cinque maggiori insegne della grande distribuzione in Italia hanno detto basta alla crudeltà delle gabbie per le galline.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Questa decisione di Conad da sola cambierà le condizioni di vita di oltre un milione di galline allevate per le loro uova.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2483} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/11/4_4.jpg" alt="" class="wp-image-2483"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L’allevamento in gabbia provoca enormi sofferenze agli animali. Le galline infatti sono costrette a passare la vita intera in condizioni che compromettono i loro bisogni naturali e non permettono loro di esprimere comportamenti come stendere completamente le ali o beccare il terreno. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Queste terribili condizioni di vita sono state documentate più volte da Animal Equality attraverso investigazioni e inchieste negli allevamenti. Ultima in ordine temporale è l’inchiesta realizzata in esclusiva con la redazione del Tg2 <a href="https://animalequality.it/una-vita-in-gabbia/">Una vita in gabbia</a>, un reportage che ha riscosso grande attenzione da parte del pubblico e nel settore alimentare in Italia.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«La politica appena rilasciata da Conad giunge a seguito della pubblicazione di un impegno parziale, che includeva solo le uova commercializzate nei propri punti vendita a marchio dell’insegna. Ora invece la scelta di Conad comprende tutte le uova vendute nei punti vendita dell’azienda, ed è questo tipo di impegni che dà un vero e chiaro segnale ai produttori» spiega Matteo Cupi, Direttore Esecutivo di Animal Equality Italia.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>«Per questi motivi speriamo che anche altre insegne che al momento hanno preso impegni purtroppo parziali, come MD e Despar Italia, seguano al più presto al trend del settore, che si dimostra ormai inarrestabile» conclude Cupi.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nonostante eliminare le gabbie non significhi eliminare la sofferenza, queste politiche aziendali costituiscono un grande primo passo verso la riduzione della sofferenza degli animali. Ma questo genere di decisioni non riguarda esclusivamente i consigli di amministrazione delle aziende. A partire da settembre di quest’anno infatti Animal Equality - insieme a più di 100 organizzazioni in tutta Europa, 20 solo in Italia - ha promosso End the Cage Age (“mettiamo fine all’era delle gabbie”), una campagna internazionale lanciata con lo scopo di raccogliere un milione di firme in tutta Europa e chiedere alla Commissione europea di porre fine all’uso delle gabbie negli allevamenti.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rtecenter"} -->
<p class="rtecenter"><span style="font-size:20px;"><a href="https://campaigns.animalequality.it/firma-contro-gabbie/">Firma anche tu l’iniziativa dei cittadini europei per mettere fine all’uso delle gabbie in tutta Europa!</a></span></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><span style="font-size:16px;">Ma puoi fare ancora di più per aiutare le galline e agli altri animali costretti a vivere in gabbia:&nbsp;<strong><a href="https://animalequality.it/difensori-animali/">Unisciti alla squadra dei Difensori degli Animali, il nostro gruppo di attivisti digitali che lotta per un mondo migliore per gli animali</a>!&nbsp;</strong></span></p>
<!-- /wp:paragraph -->