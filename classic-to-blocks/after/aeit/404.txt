<!-- wp:paragraph -->
<p>Emma è l'ennesima vittima dello sfruttamento degli animali per il consumo di carne ma la sua storia è molto significativa.

Emma è un'altra vittima dello sfruttamento animale per il consumo di carne. Aveva solo pochi mesi quando venne investita da un auto nello Utah, Usa, proprio fuori dal ranch dal quale stava per essere trasportata al macello. La lasciarono sulla strada, in agonia, per giorni, la cosa più semplice e meno costosa da fare. Purtroppo l'ingorda industria della carne tiene soltanto al profitto e un animale ferito non merita neanche una morte più dolce. A loro non importava più nulla di lei, ormai era solo un rifiuto e sarebbe morta di dolore e di stenti.

Era il 7 aprile quando qualcuno chiamò il Farm Sanctuary, un rifugio per animali, per riferire di questa cucciola abbandonata in mezzo alla strada, ferita ed esposta alle intemperie. Emma venne soccorsa e subito ricoverata in una clinica della California dove finalmente le furono date le cure e le attenzioni di cui ogni essere vivente ha bisogno.

Emma ci ricorda che anche gli animali hanno tanta voglia di vivere, nonostante le sofferenze che sono costretti a sopportare a causa di abitudini alimentari che li rendono schiavi, codici a barre, profitto. L'industria della carne considerava la dolce Emma come un rifiuto e se non fosse stato per un eroe di passaggio, che ha avvisato il Farm Sanctuary, lei sarebbe stata l'ennesima vittima dell'industria della carne. Scartata e abbandonata a morire in solitudine.

Vorremmo cogliere l'occasione per dire ai nostri lettori che le mucche sono animali calmi e molto dolci, in grado di provare dolore ed emozioni come tutti gli esseri viventi e che la vita di Emma conta tanto quanto quella di chiunque altro. Il futuro degli animali è nelle nostre mani e nelle nostre scelte perché vengano al più presto considerati amici e compagni di viaggio sullo stesso pianeta.</p>
<!-- /wp:paragraph -->