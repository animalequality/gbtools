<!-- wp:image {"id":1498} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/05/15b2d55456031d40665dace3acf42380_XL.jpg" alt="" class="wp-image-1498"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
I <strong>liquami</strong> prodotti in un anno da questo allevamento&nbsp;avrebbero potuto riempire <strong>50 piscine olimpioniche</strong>, invece venivano <strong>riversati nel territorio circostante</strong>&nbsp;senza alcun tipo di autorizzazione per la disposizione dei rifiuti. 20mila metri cubi di rifiuti.

Le indagini per scoprire la fonte dell'<strong>inquinamento del fiume abruzzese Aterno</strong> hanno condotto al sequestro di&nbsp;un allevamento intensivo che arrivava a contenere&nbsp;4.500 suini. Molti di questi <strong>animali&nbsp;erano destinati alla produzione di salumi per prestigiose etichette</strong> del nord Italia,&nbsp;i prodotti di questo stabilimento si trovano quindi&nbsp;<strong>nei frigoriferi di milioni di italiani</strong>.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1499} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/05/17747173149_a2465a6f72_k.jpg" alt="" class="wp-image-1499"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Oltre alla gestione illegale ed incontrollata dei liquami, sono stati trovati&nbsp;circa <strong>30 maiali morti&nbsp;all’area aperta, in&nbsp;due fosse comuni</strong> senza nessun presidio di protezione sanitaria ed ambientale e senza&nbsp;certificazioni veterinarie a stabilire le cause dei decessi e le istruzioni per lo smaltimento dei corpi.

Si stima che <strong>in Italia vengano allevati ogni anno circa 10 milioni di maiali</strong>. Una quantità altissima, raggiungibile soltanto con gli allevamenti di tipo intensivo. Sono strutture che basano la propria attività sulla produzione di massa <strong>per ottenere quanto più profitto possibile, senza curarsi del benessere degli animali</strong>, un aspetto che viene a malapena&nbsp;preso in considerazione quando ha delle conseguenze sulla qualità dei prodotti.

</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://news-town.it/media/k2/items/cache/15b2d55456031d40665dace3acf42380_XL.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

La nostra <a href="https://animalequality.it/carne-di-maiale">investigazione sull'industria suinicola italiana</a> ha denunciato realtà altrettanto crudeli:
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><!-- wp:list-item -->
<li><strong>Scrofe inseminate artificialmente</strong> all’interno di piccolissime gabbie di gestazione dove saranno costrette a vivere tutta la gravidanza e gran parte della loro vita;</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><strong>Cuccioli abbandonati senza cure</strong>, schiacciati o gettati uno sull’altro come spazzatura, spesso all’aperto e per diverse ore;</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><strong>Zone di ingrasso stracolme</strong>, riempite all’inverosimile con maiali costretti a vivere <strong>tra urine e feci</strong>;</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><strong>Malattie e ferite gravi</strong> non curate (con prolassi all’ano di estrema gravità).</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/NupGMf0z_Vs" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<!-- /wp:paragraph -->