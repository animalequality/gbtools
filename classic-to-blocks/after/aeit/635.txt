<!-- wp:paragraph -->
<p><strong>Domani</strong>, <strong>25 gennaio</strong>, i membri della <strong>Commissione Agricoltura</strong> del <strong>Parlamento Europeo</strong> saranno chiamati a votare in merito ad proposta di Stefan Eck che potrebbe aprire la strada al <strong>bando gli allevamenti in batteria di conigli</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In questo post ti spiegheremo che aiuto concreto puoi dare tu oggi, prima della votazione di domani.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I conigli hanno bisogno che tu sia la loro voce.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Ogni anno, in Europa,<strong> uccidiamo più di 320 milioni di conigli</strong>. Il 99% di questi viene allevato in <strong>batteria</strong>.&nbsp;</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><img alt="" class="wp-image-1966" src="/app/uploads/2017/01/conigli_animal_equality_commissione_europea.jpg" style="width: 850px; "></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Immaginatevi che ogni coniglio spenda la propria, intera vita all'interno di uno <strong>spazio grosso circa quanto un foglio A4</strong> (21x29.7cm). È esattamente quello che succede.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Animal Equality ha più volte documentato le incredibili sofferenze a cui sono sottoposti i conigli sia in Italia che in Spagna: il 60% dei conigli allevati in Europa si trova proprio all'interno di queste due nazioni.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/sly8b0Ezd9A?rel=0&amp;showinfo=0" width="850"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I nostri investigatori hanno filmato di tutto:<strong> ferite non curate</strong>, <strong>infezioni agli occhi</strong> e perfino <strong>episodi di cannibalismo</strong> fra i conigli stessi. L'incredibile <strong>sovraffollamento</strong> delle gabbie e le <strong>condizioni di stress atroce</strong> fanno si che il <strong>30% di questi animali muoia o venga ucciso ancora prima di raggiungere il macello</strong>. È la percentuale più alta fra tutti gli animali allevati dall'industria della carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nel 2016 abbiamo presentato i risultati delle nostre ricerche proprio al Parlamento Europeo ed ora i membri della Commissione Agricoltura stanno per discutere una proposta che potrebbe mettere al bando gli allevamenti in batteria di conigli in tutta Europa. Se la proposta passa, il prossimo passo sarà il voto dell'intero Parlamento Europeo già quest'anno.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Oggi, però, TU puoi fare la differenza.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>C'è la necessità che i rappresentati più importanti rappresentanti di ogni paese all'interno della commissione&nbsp;sappiano da che parte siamo.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Per favore, prenditi un minuto per scrivere <strong>un'email</strong> o un <strong>tweet</strong> ai rappresentanti italiani chiedendogli di approvare la proposta per il divieto della gabbie in tutta Europa!</h4>
<!-- /wp:heading -->

<!-- wp:html -->
<center>
<p>&nbsp;</p>

<h4>Paolo De Castro: <a href="https://twitter.com/home?status=%40paolodecastro%20600.000%20cittadini%20europei%20hanno%20detto%20%23bastagabbie%20per%20i%20%23conigli.%20Ascolatali%20il%2025/01!%20Vota%20%23EndTheCageAge">Tweet</a>&nbsp;| <a href="mailto:paolo.decastro@europarl.europa.eu?subject=Basta%20Gabbie%20per%20i%20Conigli!">Email</a></h4>

<h4>Herbert Dorfmann: <a href="https://twitter.com/home?status=%40HerbertDorfmann%20600.000%20cittadini%20europei%20hanno%20detto%20%23bastagabbie%20per%20i%20%23conigli.%20Li%20ascolterai%20il%2025/01?%20Vota%20%23EndTheCageAge">Tweet</a> | <a href="mailto:herbert.dorfmann@europarl.europa.eu?subject=Basta%20gabbie%20per%20i%20Conigli!">Email</a></h4>
</center>
<!-- /wp:html -->