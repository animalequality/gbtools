<!-- wp:image {"id":1958} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/01/francia_animali_macelli.jpg" alt="" class="wp-image-1958"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Sembrerebbe che gli sforzi delle associazioni di animalisti e privati cittadini d'oltralpe inizino a dare alcuni risultati: con l'<a href="https://www2.assemblee-nationale.fr/scrutins/detail/(legislature)/14/(num)/1368">approvazione dell'emendamento n°22 all'articolo 4 del progetto di legge per il rispetto degli animali</a>, dal 1 gennaio 2018, ogni struttura adibita a trasporto, alloggio, immobilizzazione, stordimento, macellazione e abbattimento degli animali dovrà essere <strong>munita di un sistema di video sorveglianza interno</strong>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fino a qui tutto bene, verrebbe da dire. La notizia, però, sta dando adito a diverse critiche, anche da parte delle organizzazioni animali francesi che in questi anni hanno portato avanti le più importanti battaglie per i diritti animali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":1957} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/01/francia_animali_macelli_animal_equality.jpg" alt="" class="wp-image-1957"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>L214</strong>, una fra le più attive sul territorio francese, ha infatti espresso alcune perplessità: alla votazione hanno partecipato <strong>solo 32 deputati su 577</strong> ed in questa camera praticamente deserta si è deciso comunque di non approvare altri punti importanti come il divieto di <strong>macellazione di mucche gravide</strong>, il divieto di macellazione <strong>senza stordimento</strong> o il diritto di <strong>ispezione non programmata</strong> nei macelli da parte dei parlamentari.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<center><iframe allowtransparency="true" frameborder="0" height="734" scrolling="no" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fl214.animaux%2Fposts%2F10154836878204757%3A0&amp;width=500" style="border:none;overflow:hidden" width="500"></iframe></center>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>In più, lamenta sempre L214, il sistema di video sorveglianza sarà <strong>accessibile solo a persone già attualmente presenti all'interno di questi impianti</strong>.<br>
I controlli del Ministero dell'Agricoltura hanno rivelato come l'<strong>80% dei macelli francesi non rispettino nemmeno le norme minime</strong>, ed in questo senso, viene automatico chiedersi che senso abbia obbligare queste strutture a dotarsi di sistemi di video sorveglianza se poi le associazioni animaliste ed i cittadini stessi non abbiano il diritto di accedervi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>30 Millions d'Amis</strong>, altra organizzazione francese rincara la dose: se gli unici a poter accedere a questi filmati sono figure che già operano all'interno di queste strutture, chi darà l'allarme in caso di abuso? Perché le organizzazioni per la protezioni degli animali sono state escluse?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">C'è comunque una notizia positiva in tutto questo: <strong>il maltrattamento degli animali è diventato in Francia un crimine a tutti gli effetti</strong>.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Grazie proprio al provvedimento passato alla camera, le pene per questo reato andranno <strong>dai 6 ai 12 mesi di reclusione</strong> e potranno implicare <strong>sanzioni fino a 7500€</strong>.</p>
<!-- /wp:paragraph -->