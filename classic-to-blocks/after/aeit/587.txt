<!-- wp:image {"id":1706} -->
<figure class="wp-block-image"><img src="/app/uploads/2016/10/docu.jpg" alt="" class="wp-image-1706"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Se amate gli animali, o se semplicemente amate scoprire come stanno realmente le cose al giorno d'oggi, assicuratevi di aver visto i seguenti documentari, perchè sono essenziali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Abbiamo infatti selezionato i quattro migliori documentari usciti nell'ultimo decennio.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Speriamo vi piacciano almeno quanto sono piaciuti a noi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">1. Earthlings (2005)</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Narrato da <strong>Joaquine Phoenix</strong> e vincitore di diversi premi, Earthlings è considerato da molti la pellicola che ha innescato il proprio cambiamento personale ed acceso la propria voglia di aiutare gli animali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Un lavoro potente ed onesto, che unito alla voce dell'attore celebre per diverse pellicole come Il Gladiatore, Walk the Line o Lei, lascia ogni conclusione in mano allo spettatore.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/Lvuroa6m3i8?list=PL7e9bllia7wOgmmRpgormO_rHijsvrBOA&amp;showinfo=0" width="850"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><em>"Se avessi il potere di mostrare un documentario ad ogni persona sulla terra, quel documentario sarebbe Earthlings"</em></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Peter Singer, autore del classico 'Animal Liberation'</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">2. Cowspiracy&nbsp;(2014)</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Una pellicola ipnotica che esplora le conseguenze&nbsp;dell'allevamento intensivo sull'ambiente come mai nessuno aveva fatto fino ad ora.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Vengono affrontati molti argomenti taboo per l'industria della carne (ed anche per le maggiori organizzazioni ambientaliste) con un'onesta ed un rigore statistico sorprendenti.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La versione per Netflix è stata prodotta da Leonardo DiCaprio.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/7p0Vs_4VDsY?rel=0&amp;showinfo=0" width="850"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><em>"Cowspiracy è probabilmente il più importante documentario prodotto fino ad ora per ispirarci a salvare il pianeta"</em></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Louie Psihoyos, vincitore dell'Oscar per il documentario "The Cove".</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">3. Forks Over Knives&nbsp;(2011)</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Accompagnato&nbsp;da due rinomati esperty di cibo, salute e benessere, lo spettatore condurrà questo percorso attraverso le abitudini alimentari della società Occidentale.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Facendovi&nbsp;incontrare&nbsp;un'ampia&nbsp;selezione di nutrizionisti professionisti e pazienti, questa pellicola vi metterà di fronte alla relazione fra cibo e salute.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Difficilmente ne rimarrete indifferenti.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/hCktxJtb_3Q?rel=0&amp;showinfo=0" width="850"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><em>"Vi consiglio vivamente la visione, è una pellicola incredibile sulle conseguenze del mangiare animali"</em></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Moby, musicista.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">4. The Ghosts in Our Machine&nbsp;(2011)</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Un bellissimo documentario mozzafiato che, attraverso l'esperienza del foto-giornalista ed attivista Jo-Anne McArthur racconta le storie degli animali che vivevano ma sono stati salvati dall'industria moderna.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L'incredibile storia di una donna che, con il proprio lavoro, ha deciso di aiutare i più indifesi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><iframe allowfullscreen="" frameborder="0" height="478" src="https://www.youtube.com/embed/-sTQpZxqHKU?rel=0&amp;showinfo=0" width="850"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><em>"Un must. Andrebbe visto da tutti"</em></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>James Cromwell, attore.</p>
<!-- /wp:paragraph -->