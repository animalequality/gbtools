<!-- wp:image {"id":1232} -->
<figure class="wp-block-image"><img src="/app/uploads/2014/02/vita.jpg" alt="" class="wp-image-1232"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph {"className":"p1"} -->
<p class="p1">Animal Equality ha lanciato da un anno la campagna Senza Voce per chiedere la fine del commercio di carne di cane e gatto in Cina; gli attivisti della nostra organizzazione si sono infiltrati in questo settore dell’idustria carnea riuscendo a portare alla luce le terribili nefandezze compiute ai danni di questi animali. Iniziata nell’aprile del 2013, oggi la petizione sul sito dedicato a questo lavoro ha superato le 300.000 firme. L’impegno portato avanti insieme ad organizzazioni e attivisti cinesi ha dato i suoi frutti: <strong>33 rivenditori nel mercato dei Tre Uccelli a Dali</strong>, e <strong>un macello</strong> nella medesima zona, <strong>sono stati chiusi</strong> grazie alle denunce presentate sulla base delle immagini raccolte. Stimiamo che oltre un milione e mezzo di animali tra cani e gatti trovassero, ogni anno, la morte proprio in questi luoghi.&nbsp;Ma non è tutto.<strong>Un altro individuo ha visto cambiare la propria vita a seguito di questa investigazione.&nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">Durante il lavoro investigativo <strong>gli attivisti di Animal&nbsp;Equality</strong> <strong>sono riusciti a portare via</strong>, grazie ad un espediente, <strong>un cane da un macello a Zhanjiang</strong>. Ingannando i lavoratori del posto Vita, questo è il suo nome, è stata tratta in salvo e portato fino a Madrid, per tenerla al sicuro, lontano dalla violenza e dai crimini commessi in Cina per la produzione della carne di cane e gatto.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p1"} -->
<p class="p1">Vita non è arrivata sola in Spagna ma insieme ai cuccioli che ha dato alla luce pochi giorni dopo; nessuno sapeva al momento del salvataggio che fosse incinta. Ora vivono tutti felici, amati e rispettati, ambasciatori per tutti i cani che continuano a morire a causa del consumo della loro carne.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p2"} -->
<p class="p2">La storia di Vita e le immagini dell’investigazione che abbiamo realizzato insieme ai tanti attivisti cinesi che hanno supportato il nostro lavoro, ha raggiunto milione di persone in occidente ma anche in Cina, sensibilizzando l’opinione pubblica sulle violenze nascoste perpetrate ai danni di cani e gatti.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"p1"} -->
<p class="p1">In molti non hanno avuto la sua fortuna ma la nostra campagna non si ferma, fino a che la terribile realtà con cui siamo entrati in contatto non diverrà soltanto un ricordo.
Continuiamo ad impegnarci costantemente per fare la differenza per tutti gli animali, come in questo caso.
<strong>Sostenerci</strong> può aiutare il nostro lavoro concretamente, grazie! &nbsp;→ <strong><a href="https://animalequality.it/dona-per-aiutare-gli-animali/">https://animalequality.it/dona-per-aiutare-gli-animali/</a></strong></p>
<!-- /wp:paragraph -->