<!-- wp:html -->
<div class="center">&nbsp;</div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Hace apenas dos días, hicimos públicas imágenes de violencia y negligencia en Rosebury Farm, una granja de cerdos con certificación «Red Tractor» en Bedfordshire, Inglaterra.

El «Red Tractor» <strong>es el mayor esquema de aseguramiento de alimentos en el Reino Unido</strong>, que mira a normas de seguridad, higiene, la producción de alimentos y el bienestar animal.

Las imágenes obtenidas tras tres meses de investigación revelaron:

- Un trabajador balanceando pequeños lechones por la pierna trasera y golpeando su cabeza contra la pared. Uno de ellos continúa dando patadas por, al menos, 10 segundos después de esto.

- Lechones que gritan de dolor cuando les cortan las puntas de sus diminutos dientes sin anestesia, una mutilación restringida «únicamente a circunstancias extremas», según la ley del Reino Unido.

- Cerdos aterrorizados que son electrocutados repetidamente con una picana eléctrica para forzarlos a subir al camión de matanza. Muchos de ellos son pinchados en el costado y el cuello en violación de la ley.

- Un pequeño lechón con espuma en la boca que fue arrojado a una pila de lechones muertos y abandonado a morir horas antes.

- Docenas de lechones muertos ensuciando el piso de la caseta de parto, así como lechones atrapados en jaulas junto a sus hermanos muertos.

- Cerdos más grandes metidos en corrales de metal tan pequeños que se ven obligados a acostarse uno encima del otro con temperaturas extremadamente altas.

- Otros cerdos encerrados en sucios compartimientos de madera con tapas que los dejan en total oscuridad.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/qRYMG_ttiCc" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13241} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/08/barren_dark_pens_-_web.jpg" alt="" class="wp-image-13241"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Rosebury Farm, en Dunstable, es la sexta granja de cerdos certificada por «Red Tractor» donde, en menos de un año, hemos expuesto el sufrimiento de estos animales. Le siguen <strong>investigaciones en varias granjas donde documentamos a cerdos en graves dificultades y violaciones de la ley del Reino Unido</strong>, así como los abusos impactantes que descubrimos en Fir Tree Farm en Lincolnshire, hace apenas dos meses.

Se sabe que la granja suministra a Cheale Meats en Essex, que a su vez proporciona carne de cerdo a los principales mayoristas del Reino Unido, y suministra a Evans &amp; Sons, un carnicero «tradicional» en Bedfordshire que se jacta de vender únicamente carne local.

El experto veterinario, profesor Andrew Knight, confirmó que las imágenes muestran «el manejo y la matanza inhumanos de lechones», así como «el uso excesivo e inapropiado de una picana eléctrica que puede causar dolor y miedo».

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13242} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/08/piglet_foaming_at_mout_-_web_1.jpg" alt="" class="wp-image-13242"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Después de recibir la evidencia que le suministramos, «Red Tractor» retiró la certificación de aseguramiento de la granja. Si bien estamos complacidos de que hayan actuado de acuerdo con la evidencia que proporcionamos, esto pone en tela de juicio sus propias inspecciones, que afirman llevar a cabo cinco veces al año en cada granja de cerdos de su plan. La RSPC, Real Sociedad para la Prevención de la Crueldad hacia los Animales, también ha sido informada.

<strong>Te podría interesar: <a href="https://igualdadanimal.mx/noticia/2018/07/25/impactante-filmamos-el-brutal-maltrato-cerdos-en-granjas-italianas/">IMPACTANTE: filmamos el brutal maltrato a cerdos en granjas italianas</a></strong></p>
<!-- /wp:paragraph -->