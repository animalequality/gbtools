<!-- wp:paragraph -->
<p>Los expertos insisten: además de ser responsable del mayor maltrato animal de la historia, la ganadería industrial es el factor principal del calentamiento climático y por eso reducir nuestro consumo de carne es la única forma de salvar al planeta.

Es así como lo ves: los efectos de esta industria son tan nocivos que solo ella es responsable del 14,5% de las emisiones de gases de efecto invernadero en el mundo.

El 30% de la superficie de la tierra está siendo ocupado por la ganadería industrial y tan solo en Sudamérica el 71% de la deforestación se debe a la demanda de los productos derivados de su actividad.

Un estudio presentado por la Coalición Mundial por los Bosques evidenció la relación entre la deforestación y la expansión del pastoreo intensivo en siete países de América del Sur.

Según dicho estudio, entre 1990 y 2005, el 71% de la deforestación en Argentina, Colombia, Bolivia, Brasil, Paraguay, Perú y Venezuela se debió al aumento de la demanda de pastos para ganado. Tan solo el 14% fue destinado a cultivos comerciales y apenas un 2% a la infraestructura y expansión humana.

La expansión de los bosques causó la pérdida de al menos un tercio de los bosques en seis de los países analizados.

<strong>MÉXICO</strong>

En México las cifras son también alarmantes, ya que la mitad del territorio nacional son ocupadas por granjas, mataderos, pastizales y cultivos para los animales.

En ese país mientras que el uso doméstico del agua representa el 14% y la producción de alimentos vegetales para consumo humano el 20%,&nbsp;<strong>la ganadería industrial consume la alarmante cifra del 50% del uso de agua potable. A nivel mundial esta cifra es del 20 al 33%</strong>.

Resulta más eficiente producir proteínas de origen vegetal y las cifras que revelan esta rentabilidad son realmente contundentes:&nbsp;<strong>para producir un kilo de carne de se necesitan 15 mil litros de agua mientras que producir un kilo de maíz o soya requiere 900 litros</strong>.

Con ese kilo de carne comen 8 personas y con los cereales que se invierten para alimentar a los animales para producirla podrían comer hasta 150 personas.

<strong>Con la disminución del consumo de carne se ahorraría más del 70% de agua, se reduciría la deforestación también en un 70%</strong>, se recuperarían miles de millones de hectáreas en todo el mundo y se reducirían hasta en un 63% las emisiones de gases de efecto invernadero.

Te podría interesar: <a href="https://igualdadanimal.mx/blog/el-cambio-climatico-implica-grandes-cambios-de-habitos-que-beneficiaran-todos/">https://igualdadanimal.mx/blog/el-cambio-climatico-implica-grandes-cambios-de-habitos-que-beneficiaran-todos/</a></p>
<!-- /wp:paragraph -->