<!-- wp:paragraph -->
<p>El día de ayer&nbsp;Igualdad Animal presentó&nbsp;en el Senado de la República el proyecto de realidad inmersiva iAnimal y una exposición fotográfica de la <a href="https://rastrosdemexico.igualdadanimal.mx/">investigación que realizó en rastros de México</a> en un evento para impulsar la iniciativa de Ley que busca que la crueldad animal sea considerada un delito. Claro, se han asegurado de que los animales considerados de granja sean incluídos en la protección que brindará la modificación propuesta.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom","align":"center"} -->
<figure class="wp-block-image aligncenter"><a href="https://www.flickr.com/photos/igualdadanimal/37750285836/in/album-72157688230263883/"><img src="https://farm5.staticflickr.com/4473/37750285836_6d458fafab_z.jpg" alt="iAnimal en el Senado de México"/></a></figure>
<!-- /wp:image -->

<!-- wp:html -->
<script async="" src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Nunca antes la Cámara de&nbsp;Senadores&nbsp;del Honorable Congreso de la Unión había sido sede de un evento enfocado en los animales de granja. Ninguna organización había acudido a representar a los animales que más sufren y más mueren en cantidad en nuestro país.&nbsp;¡Se ha&nbsp;hecho historia!

En la inauguración del evento, el presentador y activista Marco Antonio Regil, la senadora Diva Gastélum, promotora de dicha iniciativa y la doctora Giuliana Miguel-Pacheco, maestra en comportamiento y bienestar animal aplicado, dieron su apoyo a la iniciativa, destacando la importancia de acabar con los actos crueles contra los animales e invitando a los senadores a votar positivamente la iniciativa.
Como parte de su discurso, Dulce Ramírez, directora de Igualdad Animal en México puntualizó que es urgente y necesario que los mexicanos ampliemos nuestro marco de indignación en el actuar político y social de la consideración que hacemos hacia los animales. Aseguró que las futuras generaciones lo agradecerán y recordarán el día en que la crueldad animal sea un delito federal como el fin de una era y el comienzo de otra mejor.

Destacó el apoyo de los grupos y asociaciones civiles de protección animal en México; Vegan Outreach, Proyecto Gran Simio, Animal Sonoro, Movimiento Ecológico por los Derechos Animales, Proyecto ARPA, Xalapa por la Vida Digna, A.C., Comunidad Gaia, Callejeritos de los Tuxtlas A.C., Justicia y Dignidad Animal, A.C., Naturaleza Animal, A.C., y Apasdem, A.C., una coalición integrada por 92 grupos y la presencia de representantes de los tres últimos durante el magno evento.

Por su parte, los asistentes al evento pudieron conocer no sólo las investigaciones que ha presentado la organización sino adentrarse a la realidad que se vive en los rastros mexicanos.

“Nosotros hemos hecho el trabajo, les entregamos las pruebas y una solución, que no es total pero que supone un gran avance. Les corresponde ahora a ustedes senadoras y senadores darle un voto a los animales y así sumar en la construcción de una sociedad más justa, libre de violencia y consciente de que compartimos este planeta con otros animales que deben ser respetados y protegidos”, aseguró Dulce Ramírez.

El evento inició a las 11 horas y estuvo abierto al público en general hasta las 17 horas.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/la-importancia-de-legislar-en-favor-de-los-animales-en-el-senado-de-la-republica/">La importancia de legislar en favor de los animales en el Senado de la República Mexicana</a></strong></p>
<!-- /wp:paragraph -->