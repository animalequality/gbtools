<!-- wp:paragraph -->
<p>Transcurrido ya un tiempo desde que lanzamos nuestra investigación en la industria de la carne de pollo en México, en la cual mostramos las terribles condiciones en las que viven miles de millones de animales, hoy quisiéramos compartir <strong>una información importante que la ganadería industrial insiste en mantener oculta</strong>.

La industria de la carne, incluida la del pollo, no solo es responsable de someter a estos sensibles e inteligentes animales a una vida de inmenso sufrimiento, sino que también <strong>amenaza la vida de miles de millones de personas</strong>. Entre sus procedimientos estándar, desde hace décadas se encuentra el uso de antibióticos de manera rutinaria, y que no son aplicados para tratar a animales enfermos sino para prevenir brotes de enfermedades y para acelerar el crecimiento.

<strong>Noticia relacionada: &nbsp;<a href="https://igualdadanimal.org/noticia/2017/10/27/espana-usa-mas-antibioticos-en-granjas-que-cualquier-otro-pais-de-la-ue/">España usa más antibióticos en granjas que cualquier otro país de la UE</a></strong>

La cría de animales a nivel industrial es posible gracias a esto, pero también ha provocado que se desate una de las mayores amenazas tanto para animales como humanos: la multiplicación de cepas de bacterias resistentes. Estas «superbacterias», como también se les conoce, <strong>provocan anualmente la muerte de 25 mil personas tan solo en Europa</strong>.

Y a pesar de que en México existe un reglamento para el uso de antibióticos, <strong>no hay una vigilancia estricta en la aplicación de leyes</strong>, como sucedió recientemente con <a href="https://igualdadanimal.mx/noticia/2018/04/12/escandalo-en-mexico-se-vende-carne-contaminada-con-clembuterol/">el escándalo por uso de clembuterol</a>. De acuerdo a una investigación periodística de la organización independiente «Bureau of Investigative Journalism» (BIJ), tan solo en 2016, México, junto a otros países, compró 2.800 toneladas de colistina, uno de lo antimicrobianos más perjudiciales y cuyo uso se recomienda sólo como último recurso.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13184,"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="//www.igualdadanimal.org/sites/default/files/images/pollosmexico1.jpg"><img src="/app/uploads/2018/07/pollosmexico1.jpg" alt="" class="wp-image-13184"/></a></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Una inmensa parte de la carne de pollo y otros animales que se vende en México <strong>está atiborrada de antibióticos</strong>. Desde hace tiempo, la comunidad científica comenzó a alertar sobre este riesgo porque las bacterias resistentes de los animales pueden llegar a los humanos a través del contacto directo o la dispersión medioambiental en el aire o el agua.

Un informe del gobierno británico publicado en 2016, afirma que de no tomarse las medidas necesarias, en 2050 <strong>morirán más personas a causa de las superbacterias que por cáncer</strong> (8,2 millones de muertes) o accidentes de tráfico (1,2 millones). A pesar de que esto puso en alerta a las autoridades europeas, aún no existe un control sobre el uso real de antimicrobianos en países como España, donde cada año llegan a morir hasta 2.500 personas debido a la resistencia bacteriana desarrollada por el consumo de los mismos.</p>
<!-- /wp:paragraph -->