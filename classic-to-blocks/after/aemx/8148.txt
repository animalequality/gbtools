<!-- wp:paragraph -->
<p>Recientemente hemos presentado una <a href="https://www.youtube.com/watch?v=hkDTQ5RhrHk&amp;feature=youtu.be">nueva investigación</a> que ha revelado &nbsp;la tortura sistemática a la que son sometidos los cerdos en dos granjas de cerdos de Piamonte y Lombardía, al norte de Italia.

En abril de 2018, logramos infiltrar por primera vez en Italia a un investigador que trabajó de forma encubierta en dos granjas en las provincias de Cuneo y Mantua. &nbsp;En los vídeos se demuestra la negligencia y el maltrato sistemático al que son sometidos los cerdos, incluso aquellos certificados para la famosa marca mundial, Parma Ham.

Algunos hallazgos de la más reciente investigación revelaron :

• Operadores que manejan los cerdos sin cuidado.

• Cerdos abandonados a morir agónicamente en los corredores.

• Operadores que golpean violentamente a los cerdos en la cara y la cabeza.

• Cerdas enfermas y cubiertas de heridas.

• Operadores agarrando cerdos por sus patas y arrojándolos violentamente.

• Animales muertos dentro de las granjas de cría.

Otro hecho relevante que fue registrado por nuestro investigador infiltrado dentro de las instalaciones concierne a la práctica del corte de cola. El mismo es practicado como un procedimiento de rutina a pesar de estar prohibido por las normas de la Unión Europea (2008/120/CE), que ya ha recordado a Italia que casi todas sus granjas (hablamos del 98 %) violan dichas normas.
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"ae-video-container"} -->
<p class="ae-video-container"><iframe src="https://www.youtube.com/embed/hkDTQ5RhrHk" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
Ante los terribles abusos ya mencionados y la violación de la normativa, hemos &nbsp;reportado la situación a las autoridades y a los propietarios de la empresa localizada en la provincia de Cuneo, que suministra al Consorcio Jamón de Parma. El reclamo ha sido firmado también por el Supervisor de Bienestar Animal, el doctor Enrico Moriconi y el Concejal de la Región de Piamonte, Francesca Frediani.

Según los últimos datos publicados por ISMEA (Instituto de Servicios para el Mercado Agrícola de alimentos), en 2016, Italia conquistó a los líderes mundiales para las exportaciones de preparados de carne de cerdo y conservas, superando a Alemania con un valor total de casi 1.38 billones de euros. En general, en los últimos cinco años, las exportaciones italianas de estos productos han aumentado un 27 %, y más de la mitad de estos son jamones sazonados, por 692 millones de euros en 2016.

Hemos lanzado una petición dirigida expresamente a los ministros Gian Marco Centinaio (Políticas Agrícolas, Alimentarias y Forestales) y Giulia Grillo (Salud) para lograr, entre otras cosas que exigimos, un control mayor en las condiciones de cría de los animales, la suspensión de las prácticas que no cumplen con las normas europeas y una actualización de la legislación sobre el empleo de jaulas para cerdas. Puedes&nbsp;firmar y compartir&nbsp;la petición <a href="https://animalequality.it/allevamenti-maiali/" target="_blank" rel="noopener noreferrer">aquí</a>.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2018/08/01/ultima-hora-destapamos-la-violencia-y-negligencia-en-una-granja-de-cerdos-en/">¡Última hora! Destapamos la violencia y negligencia en una granja de cerdos en Inglaterra</a></strong></p>
<!-- /wp:paragraph -->