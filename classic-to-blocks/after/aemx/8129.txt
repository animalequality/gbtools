<!-- wp:paragraph -->
<p><strong>Pastelería Ok</strong>, una de las más grandes en el occidente de México, ha hecho hoy público su compromiso para eliminar al 100% las jaulas de su cadena de suministro de huevo para el año 2025.

Al ser empresa líder en su sector que se preocupa por sus productos y sus clientes, en su comunicado aseguran que “para proteger a los animales de las malas prácticas en los procesos de producción de huevo, Pastelería OK colaborará en conjunto con todos sus proveedores con la meta de eliminar por completo el consumo de productos proveniente de gallinas criadas dentro de jaulas”.

Con este <a href="https://www.pasteleriaok.com.mx/">nuevo compromiso</a>, serán más de 15.400 gallinas al año las que ya no sufrirán a causa del cruel sistema de jaulas. Pastelería Ok es la tercera pastelería que se suma a más de 40 empresas en México que han optado por eliminar las jaulas, y esperamos que, pronto, más empresas se sumen, como la galletera Mac’Ma o Soriana, con lo que serían millones de gallinas las que dejarían de pasar sus vidas&nbsp;enjauladas.

En Igualdad Animal México sabemos que libre de jaula no significa libre de sufrimiento, pero consideramos que este compromiso es un avance hacia un mundo más compasivo. Seguiremos trabajando para eliminar este sistema cruel e innecesario.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2018/01/30/pasteleria-fina-neufeld-anuncia-que-eliminara-las-jaulas-de-su-suministro-de-huevo/">Pastelería fina Neufeld anuncia que eliminará las jaulas de su suministro de huevo</a></strong></p>
<!-- /wp:paragraph -->