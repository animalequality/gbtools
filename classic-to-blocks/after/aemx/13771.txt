<!-- wp:paragraph -->
<p>La cadena de hoteles Marriott, la segunda más grande del mundo, ha anunciado <a href="http://serve360.marriott.com/wp-content/uploads/2018/12/Marriott_International_Cage-Free_Egg_Statement.pdf">su compromiso</a> para eliminar las jaulas de su suministro de huevo a nivel global para el año 2025.

Con este compromiso las dos cadenas de hoteles más grandes del mundo, Wyndham y Marriott, se suman a la tendencia por terminar con el sistema de jaulas que tanto maltrato causa a las gallinas. Gracias a estos compromisos serán cientos de miles de gallinas las que ya no tendrán que vivir hacinadas en jaulas.

El acuerdo &nbsp;se logró gracias a una campaña global por parte de la Open Wing Alliance (OWA), coalición internacional de organizaciones que trabajan para terminar con el maltrato animal, incluida Igualdad Animal, específicamente con el uso de las jaulas para las gallinas que es &nbsp;uno de los sistemas más crueles.

<strong>Te podría interesar: <a href="https://igualdadanimal.mx/noticia/2019/06/05/karisma-hotels-resorts-anuncia-su-compromiso-para-sumarse-al-movimiento-100-huevo-libre-de-jaula/">Karisma Hotel &amp; Resorts anuncia su compromiso para sumarse al movimiento 100% huevo libre de jaula</a></strong>

En tan solo dos días las organizaciones que conforman la OWA realizaron protestas en ciudades como Berlín, Nueva York, Ciudad de México, entre otras, y voluntarios de todo el mundo realizaron acciones en línea pidiéndole a Marriott que hiciera su compromiso, logrando así el compromiso global.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13482} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/12/marriot_progreso_2.jpg" alt="" class="wp-image-13482"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Cada vez son más las empresas que están mostrando interés en no ser partícipes del maltrato animal que implican las jaulas y escuchan a sus clientes que les piden políticas a favor de los animales. La tendencia por eliminar este sistema crece año cada año y con cada nuevo compromiso nos queda claro que es cuestión de tiempo para que más empresas anuncien su compromiso y sean más las gallinas que ya no vivan enjauladas.</p>
<!-- /wp:paragraph -->