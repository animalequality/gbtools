<!-- wp:paragraph -->
<p>Hoy Día Mundial de la Tierra es ocasión de reflexionar sobre la crisis medioambiental sin precedentes que enfrenta nuestro planeta. Recientemente, la ONU alertó a través de su informe ‘<a href="https://www.unep.org/resources/global-environment-outlook-6" target="_blank" rel="noopener">Perspectivas del Medio Ambiente Mundial</a>’ sobre las amenazas que tendrá que enfrentar la humanidad de no actuar a tiempo y <a href="https://www.unep.org/es/noticias-y-reportajes/comunicado-de-prensa/la-salud-humana-enfrenta-graves-amenazas-si-no-se-toman" target="_blank" rel="noopener">recomendó&nbsp;“adoptar dietas menos intensivas en carne”</a> que tienen un impacto menor en el planeta y evitan el sufrimiento de millones de animales.

&nbsp;

<strong>Emisiones de gases de efecto invernadero</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"left"} -->
<p class="has-text-align-left">La industria ganadera produce el 15% de los gases de efecto invernadero a nivel mundial, pero de acuerdo con la organización WorldWatch si se suman todas las emisiones indirectas (respiración del ganado, uso de tierra y emisión de metano) la cifra asciende al 51%. Emite el 65% de emisiones del gas óxido nitroso, un gas de efecto invernadero con un potencial 296 mayor que el dióxido de carbono.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":15761} -->
<figure class="wp-block-image"><img src="https://igualdadanimal.mx/app/uploads/2019/04/noticia-día-mundial-de-la-tierra-celebrar-salvando-animales-mx-3.jpg" alt="Día Mundial de la tierra: celebrar salvando animales " class="wp-image-15761"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>Derroche de agua</strong>

En México la producción de alimentos vegetales para consumo humano utiliza el 20% del agua mientras que la ganadería industrial consume la alarmante cifra del 50% &nbsp;del agua potable. Y en todo el mundo sus actividades consumen entre el 20 y el 33% del agua. Para producir medio kilo de carne de vaca son necesarios 9.400 litros de agua.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":15762} -->
<figure class="wp-block-image"><img src="https://igualdadanimal.mx/app/uploads/2019/04/noticia-día-mundial-de-la-tierra-celebrar-salvando-animales-mx-2.jpg" alt="Día Mundial de la tierra: celebrar salvando animales " class="wp-image-15762"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>Los océanos están muriendo</strong>

En los últimos 40 años la pesca industrial ha capturado peces con mayor rapidez con la cual pueden reproducirse. &nbsp;Esta actividad provoca mucho sufrimiento a los peces y otras especies marinas y, junto con los efectos del cambio climático del cual la actividad &nbsp;ganadera es responsable en gran parte, las poblaciones de mamíferos marinos, aves, reptiles y peces se han reducido en un 50% a nivel mundial. En el caso de algunos peces ha disminuido un 75%.

De no tomarse acciones, cada uno de estos impactos solo empeorará en los próximos años. <a href="https://loveveg.mx/10-recetas-veganas-faciles-y-llenas-de-proteinas/" target="_blank" rel="noopener">Reducir el consumo de carne</a> es la forma más efectiva de salvar animales, detener los efectos del calentamiento global y la devastación del planeta y construir un mundo mejor para todos.</p>
<!-- /wp:paragraph -->