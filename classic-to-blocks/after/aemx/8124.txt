<!-- wp:paragraph -->
<p>Podemos pasar horas viendo videos en internet. Nos maravillamos ante las habilidades físicas de animales como tigres, gacelas, monos o delfines y de los perros y gatos que viven en nuestros hogares. <strong>Admiramos su inteligencia, su sensibilidad</strong> y su empatía nos enternece y cautiva.

Pero algo ha cambiado recientemente, y es que cada vez hay más videos en la web en donde los protagonistas son otros animales. <strong>Los cerdos, vacas, cabras y gallinas llegaron para demostrar que son tan inteligentes como cualquiera</strong> de los otros animales que nos cautivan y amamos.

Todos ellos, aunque están dotados de inteligencia, destrezas y alta sensibilidad, son llamados «animales de granja» porque lamentablemente pasan la mayor parte de su vida dentro de ellas.

¡Estamos seguros que te encantará saber lo increíbles que son!
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"align":"center"} -->
<h3 class="wp-block-heading"><strong>1. Su inteligencia puede ser superior a la de un niño, delfines, primates y otros animales considerados como los más inteligentes</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
¿Sabías que los cerdos son super inteligentes? ¡Sí! Los cerdos son unos de los animales más inteligentes que habitan el planeta. Su inteligencia que supera a la de un niño de tres años está a la par de delfines y elefantes, y aprenden más rápido que los perros y los primates.

Las vacas disfrutan resolviendo problemas de lógica. Tan notable es su inteligencia que son muy comunes las historias de vacas que se han valido de ella para recorrer muchas millas hasta reunirse con sus terneros o para escapar del matadero.

Incluso cuando se les quita un objeto y se les oculta, los pollos son capaces de comprender que éste todavía existe. No son muchos los animales que tienen la capacidad de hacer esto, ni siquiera las niñas y niños humanos.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":14372,"align":"center"} -->
<figure class="wp-block-image aligncenter"><img src="https://igualdadanimal.mx/app/uploads/2018/11/Documental-granjero-73-vacas-premiado-igualdadanimal-2.jpg" alt="Documental de granjero que libró de la muerte a 73 vacas fue premiado" class="wp-image-14372"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->
<h3 class="wp-block-heading"></h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"align":"center"} -->
<h3 class="wp-block-heading"><strong>2.Poseen una memoria realmente sorprendente</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Los peces (al igual que los pollos) pueden reconocer a más de 100 individuos diferentes durante meses así como el rango social de cada individuo. Elaboran y retienen complejos mapas mentales de su entorno. Los salmones tras nadar miles de kilómetros pueden reconocer el olor del agua donde nacieron.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":3590,"align":"center"} -->
<figure class="wp-block-image aligncenter"><img src="https://igualdadanimal.mx/app/uploads/2018/03/half_fish_closeup.jpg" alt="" class="wp-image-3590"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Las vacas tienen una impresionante memoria espacial que les permite recordar rutas migratorias, lugares donde encontrar agua, las mejores zonas para pastar y el lugar dónde dejaron a su ternero para hacerlo.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"align":"center"} -->
<h3 class="wp-block-heading"><strong>3.Forman complejas organizaciones sociales y sólidas amistades &nbsp;&nbsp;</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Multitud de estudios en comportamiento animal aseguran que las vacas tienen complejas vidas sociales. Debido a su impresionante memoria son capaces de tener amistades durante toda su vida o rechazar a otras vacas que les hayan hecho daño a ellas o a los suyos.

En libertad, los pollos pueden establecer complejas jerarquías sociales. Cada uno de ellos conoce su rango en la pirámide social y esto les permite vivir en armonía. Al momento de realizar una tarea siguen las instrucciones del miembro dominante del grupo.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":3190,"align":"center"} -->
<figure class="wp-block-image aligncenter"><img src="https://igualdadanimal.mx/app/uploads/2018/03/hero_milla_pollitos_chile.jpg" alt="" class="wp-image-3190"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->
<h3 class="wp-block-heading"></h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3,"align":"center"} -->
<h3 class="wp-block-heading"><strong>4.Pueden razonar para formar opiniones y planificar su futuro</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Las vacas se preocupan por su futuro y lo que les pueda ocurrir a ellas y a sus hijos. Al igual que otros animales, aprenden a protegerse y se mantienen alejadas de aquello que les ha causado un daño en el pasado, como las vallas eléctricas, humanos y otros animales.

Un estudio de la Universidad de Macquarie en Australia sobre el comportamiento de un grupo de gallinas demostró que estas usan la lógica y la razón para elegir a los gallos con quienes desean aparearse luego de observarlos y formarse una opinión sobre ellos.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":2927,"align":"center"} -->
<figure class="wp-block-image aligncenter"><img src="https://igualdadanimal.mx/app/uploads/2018/03/half_cow_calf_bn_jmc.jpg" alt="" class="wp-image-2927"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3,"align":"center"} -->
<h3 class="wp-block-heading"><strong>5. Se comunican a través de lenguajes complejos</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Los cerdos pueden emitir más de 30 sonidos para expresar sus necesidades y estados de ánimo, y las gallinas emiten ruidos a los que sus hijos aún no nacidos responden dentro de sus huevos. ¿Te sorprende? ¡Seguro que sí!

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":3504,"align":"center"} -->
<figure class="wp-block-image aligncenter"><img src="https://igualdadanimal.mx/app/uploads/2018/03/half_cute_piglet.jpg" alt="" class="wp-image-3504"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Por su parte, los pollos también tienen 30 tipos de vocalizaciones para advertir a los miembros de su grupo sobre la presencia de depredadores, comida, alarma y miedo.</p>
<!-- /wp:paragraph -->