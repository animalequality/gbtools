<!-- wp:paragraph -->
<p>El pollo es el animal terrestre más consumido. En todo el mundo <strong>9 de cada 10 animales consumidos son pollos</strong>. Y ese altísimo consumo, por supuesto, implica un inmenso número de animales que son matados y que durante toda su vida sufren las peores formas de maltrato que podamos imaginar.

Al ser mucho más pequeños que los cerdos y las vacas, para poder satisfacer la demanda de los consumidores, lógicamente, deben ser matados muchos más pollos que cualquier otro animal. Para producir la misma cantidad de carne que se obtiene de una sola vaca <strong>son necesarios 200 pollos</strong>. Sí, ¡200!

Solo después de los peces, cuyo número es tan grande que se mide por peso, los pollos son los animales más matados y maltratados del planeta. Desde su primer día de vida soportan una brutal violencia en sus frágiles cuerpos. Apenas nacen, los más débiles <strong>son triturados vivos o desechados</strong> como basura en contenedores donde se asfixian o se les aplasta vivos con mazos.

Debido a que ha sido seleccionado genéticamente, el pollo broiler crece en las granjas de engorde a un ritmo antinatural, tanto que <strong>si fuera un bebé humano pesaría 300 kilos a los dos meses de vida</strong>. Su sufrimiento es inimaginable: al no poder soportar su propio peso sufren terribles dolores y dificultad para caminar. Muchos agonizan echados en el piso por días antes de morir, entre sus excrementos y sin poder alcanzar el alimento o el agua.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13284} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/08/mx_pollos_igualdad_animal_34_1.jpg" alt="" class="wp-image-13284"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->
<h5 class="wp-block-heading"><span style="font-size: 10px;">Foto: Nuestra investigación en granjas de pollo en México.</span></h5>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
La media de vida de los pollos broiler es de 42 días durante los cuales solo conocen la violencia y el sufrimiento. Sí, la carne de pollo que es consumida es la de animales muy jóvenes, ¡prácticamente son bebés!

<strong>Hay 3 pollos por cada ser humano en el planeta, todos obligados a vivir un verdadero infierno</strong> para que se produzca y venda una carne que no necesitamos consumir ya que podemos sustituirla por muchas opciones vegetales que son deliciosas, nutritivas y sanas.

Si comienzas a reducir tu consumo de pollo estarías también incrementando tu impacto en ayudar a más animales. Y para ayudarte a hacerlo te invitamos a probar las recetas de este fabuloso recetario para sustituir la carne de pollo en tus comidas que <a href="https://pollohechoenmexico.igualdadanimal.mx/">puedes descargar al firmar nuestra petición</a>. ¡Todas muy fáciles y deliciosas!

Te podría interesar: <a href="https://igualdadanimal.mx/noticia/2018/05/16/lo-volvimos-hacer-nos-infiltramos-en-la-industria-del-pollo-mexicana/">Lo volvimos a hacer, ¡nos infiltramos en la industria del pollo mexicana!</a></p>
<!-- /wp:paragraph -->