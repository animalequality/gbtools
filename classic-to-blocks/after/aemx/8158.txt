<!-- wp:image {"id":13256} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/08/34963172_463340597429181_6745888266043523072_n.jpg" alt="" class="wp-image-13256"/></figure>
<!-- /wp:image -->

<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Para poder mantener su granja, Mike Lanigan <strong>tenía que enviar constantemente a a vacas y terneros al matadero</strong>. Esa era la parte del trabajo que no le gustaba, pero como él mismo dice «siendo un granjero, tú simplemente miras para otro lado».

&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Noticia relacionada: <a href="https://igualdadanimal.mx/blog/5-motivos-para-amar-los-cerdos/">5 motivos para amar a los cerdos</a></h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
&nbsp;

Pero el punto es que Lanigan no pudo hacer la vista gorda por mucho más tiempo. Todo cambió para él un día, mientras ayudaba a un ternero recién nacido a tomar su primera leche. «Lo estaba haciendo con tanto amor, hablándole al ternero y limpiándole el sucio de la cara y tratando de llevárselo a su mamá», recuerda Lanigan.
&nbsp;

De pronto, se dio cuenta de que a ese inocente e indefenso animal que despertaba en él tanta ternura y a quien trataba con tanto cariño, pronto le darían un golpe en la cabeza y lo cortarían en pedazos para ser comido. Esto lo hizo reflexionar profundamente: «pensé cuán hipócrita estaba siendo al amar tanto a alguien para que todo al final terminara siendo tan distinto a ese amor».

Y fue así como luego de dedicarse por mucho tiempo al mismo trabajo que realizó su padre, Mike decidió que no enviaría más nunca en su vida a una vaca o a un ternero al matadero y que convertiría su granja en un refugio donde todos ellos podrían vivir sus vidas en paz. &nbsp;

&nbsp;

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe allow="autoplay; encrypted-media" allowfullscreen="" frameborder="0" height="450" src="https://www.youtube.com/embed/1tLeO2pg1CI" width="800"></iframe>
<!-- /wp:html -->

<!-- wp:html -->
<font size="5"><font color="#808080">Quieres recibir las mejores noticias de actualidad sobre los animales y opciones de alimentación? ¡Suscríbete gratuitamente a nuestro<a href="http://igualdadanimal.us10.list-manage2.com/subscribe?u=e092aae79a181fb16ccb742e7&amp;id=3982ab3bc4" target="_blank" rel="noopener noreferrer"> e-boletín</a>!</font></font>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

&nbsp;

Actualmente, Mike Laningan está al frente de Farmhouse Garden Animal Home con 21 vacas, gansos, caballos y un asno de guardia llamado «Buckwheat». El granjero que una vez vendió carne de vaca y de ternera para su beneficio, ahora cuida de todos estos animales vendiendo vegetales y jarabe de arce. Y todo es gracias a un cambio en su corazón que le permitió entender que todos los animales quieren vivir sus vidas en paz, y que es mucho lo que cada uno de nosotros puede hacer para que eso sea una realidad. &nbsp;</p>
<!-- /wp:paragraph -->