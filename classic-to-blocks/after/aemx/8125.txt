<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Desde que hace dos años presentamos en México nuestro proyecto de realidad virtual iAnimal, hemos avanzado cada vez más en nuestro objetivo de influir positivamente en quienes viven <strong>la experiencia de ponerse en la piel de un animal de granja.</strong>

Los tres videos dentro del proyecto iAnimal muestran las condiciones en las que cerdos, vacas y pollos viven en granjas y mataderos de México y otros países como Reino Unido, Alemania y España. Además, cada uno de ellos cuenta con una celebridad diferente: en México han sido el popular presentador de televisión, Marco Antonio Regil, la afamada artista del tatuaje Kat Von D y la talentosa actriz Sofía Sisniega.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13150} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/06/30621215394_8349fe4a8a_z.jpg" alt="" class="wp-image-13150"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Tan solo en 2017, y gracias al apoyo de nuestro maravilloso grupo de voluntariado, <strong>iAnimal fue presentado a 4.450 espectadores en más de 25 eventos</strong> multitudinarios. Algunos de ellos incluyendo al Cosquín Rock México, el Punk Rock Fest, el Corona Wuf y el Festival Roxy.

En lo que va de 2018, ya hemos acudido a 12 eventos en los que 1.760 personas pudieron probar la impresionante experiencia inmersiva. Uno de ellos fue la cuarta conferencia internacional Minding Animals, uno de los mayores eventos internacionales enfocados en la protección de los animales.

«Nuestro proyecto de realidad virtual convierte a los espectadores en testigos. Testigos de la cotidianidad dentro de una granja, de los procesos más brutales a los que son sometidos los animales para acelerar la producción y abastecer la gran demanda de carne. iAnimal es una herramienta poderosa para sensibilizar y lograr que las personas creen un lazo de empatía con los animales y decidan sacarlos de sus platos», afirmó la coordinadora de proyectos educativos de la organización, Katya Ramírez.

<strong>Nadie ha podido quedar indiferente ante lo que implica ponerse en el lugar de los animales de granja</strong> y conocer el inmenso sufrimiento que deben soportar a lo largo de sus vidas. Luego de vivir la experiencia, muchos espectadores se han sumado al compromiso de reducir y eliminar su consumo de carne, huevos y lácteos.

<a href="https://descubrirlacomida.com/" target="_blank" rel="noopener noreferrer">Consigue GRATIS</a> un fantástico recetario con deliciosos platillos veggie, además de una guía con consejos prácticos para una alimentación sostenible.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13151} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/06/39757356650_1dfc568bb8_z.jpg" alt="" class="wp-image-13151"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

«Tenemos el compromiso de informar a los consumidores sobre lo que hay detrás de los productos para que puedan tomar decisiones respecto a su alimentación. Ponerse en el lugar de los animales funciona. Esta nueva perspectiva que ofrece iAnimal acerca a todos los espectadores a la devastadora realidad que la ganadería industrial les ha ocultado sistemáticamente», comenta Dulce Ramírez, directora de la organización en México.

A finales de este año, iAnimal estará presente por segundo año consecutivo en el Campus Party, <strong>la mayor experiencia de tecnología, ciencia, innovación</strong> y entretenimiento digital que reúne a la mayor cantidad de estudiantes universitarios en México.

Hemos sido la primera organización en usar esta tecnología para mostrar la vida de los animales de granja y, <strong>hasta hoy, iAnimal ha sido visto por más de 70 millones de personas en todo el mundo. </strong>Cada día son más las organizaciones que están utilizando el proyecto para concienciar a la sociedad sobre el sufrimiento de los animales que son criados y matados para convertirse en comida. Tan solo en México ya son seis grupos y asociaciones las que están presentando iAnimal en eventos por todo el país.

Te podría interesar: <a href="https://igualdadanimal.mx/blog/realidad-virtual-para-acabar-con-la-crueldad-hacia-los-animales/">iAnimal: la realidad virtual que está cambiando la vida de los animales en México</a></p>
<!-- /wp:paragraph -->