<!-- wp:paragraph -->
<p>El próximo sábado 2 de febrero, el autor belga <strong>Tobias Leenaert hará la presentación oficial en México de su libro <em>Hacia un futuro vegano</em></strong>, publicado por Plaza y Valdés Editores en colaboración con Igualdad Animal. Lo acompañarán Dulce Ramírez, directora en México y representantes de la editorial.

Durante el evento, el autor <strong>ofrecerá también la conferencia <em>Cómo crear un mundo vegano</em></strong> para compartir con los asistentes las estrategias comunicacionales que ha creado a fin de ayudar a los defensores de animales a lograr un mayor impacto en sus acciones por promover la alimentación vegana.

«El foco principal es siempre el impacto. Necesitamos preguntarnos constantemente: ¿lo que estoy comunicando tiene el impacto que quiero? Repasaremos buenos y malos argumentos y prácticas de comunicación», ha adelantado el autor sobre lo que pueden esperar encontrar los asistentes en la conferencia.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13561} -->
<figure class="wp-block-image"><img src="/app/uploads/2019/01/tobias_leenaert_presenta_hacia_un_futuro_vegano_en_mexico_igualdad_animal-2.jpg" alt="" class="wp-image-13561"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><a href="https://loveveg.mx/" target="_blank" rel="noopener noreferrer">Consigue GRATIS un fantástico recetario con deliciosos platillos veggie</a>, además de una guía con consejos prácticos para una alimentación sostenible.

Por su parte, Dulce Ramírez ha destacado la importancia de que la obra se presenté en el país “Sin duda, este libro resultará muy significativo para el activismo en México. Tobias Leenaert es un gran comunicador de estrategias que serán de gran impacto y utilidad para el trabajo que hoy se hace en el país para promover el veganismo”.

<strong>La presentación tendrá lugar el día sábado 2 de febrero, en la Casa Rafael Galván de la Universidad Autónoma Metropolitana, ubicada en Zacatecas 94, Col. Roma Norte, C.P. 6700, México, Ciudad de México, a las 17:00 horas. La entrada es gratuita pero se requiere previo registro e invitación.</strong><strong>Sobre el libro:</strong>

Peter Singer, filósofo y autor del clásico animalista Liberación Animal, considera a Leenaert como una de las pocas personas calificadas para tratar estos temas y recomienda ampliamente Hacia un futuro vegano como «un lugar excelente por donde comenzar» para quienes quieran contribuir al movimiento vegano y a construir el mundo mejor que todos soñamos.

“Tobias Leenaert es uno de los escritores actuales más importantes en lo que se refiere a asuntos relacionados con animales”. —Matt Ball, autor de The Accidental Activist y coautor de The Animal Activist’s Handbook.

"Un libro indispensable para todos aquellos que busquen maximizar su influencia para hacer del mundo un lugar mejor para los animales”. —Melanie Joy, doctora en Psicología Social, autora del best seller Por qué amamos a los perros, nos comemos a los cerdos y nos vestimos con las vacas.

<strong>Sobre el autor:</strong>

Cofundador y antiguo director en Bélgica de la organización EVA (Ethical Vegetarian Alternative), la primera organización vegetariana que ha recibido apoyo financiero de un gobierno nacional. Bajo la dirección de Tobias, EVA lanzó una campaña muy exitosa gracias a la que Gante se convirtió en la primera ciudad que apoyó oficialmente un día vegetariano a la semana.

Tobias Leenaert imparte talleres de defensa animal en todo el mundo, junto a Melanie Joy (autora del best seller Por qué amamos a los perros, nos comemos a los cerdos y nos vestimos con las vacas), para el Center of Effective Vegan Advocacy. También es cofundador de ProVeg, una organización internacional para la promoción de un nuevo modelo alimentario, cuya misión es reducir el consumo mundial de animales al cincuenta por ciento para el año 2040.</p>
<!-- /wp:paragraph -->