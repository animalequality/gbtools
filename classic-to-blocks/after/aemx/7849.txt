<!-- wp:paragraph -->
<p>Durante cinco días <a href="https://www.facebook.com/igualdadanimalmexico">Igualdad Animal México </a>acudió a uno de los eventos de tecnología e innovación más importantes de Latinoamérica con nuestro proyecto de realidad virtual iAnimal.
</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:paragraph -->
<p>iAnimal es una experiencia inmersiva en 360 grados que te transporta virtualmente a las granjas y mataderos que abastecen a los supermercados donde compramos la carne proveniente de los animales.</p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p><a href="https://mexico.campus-party.org/">Campus Party</a> reúne al talento joven del país, conformado en su mayoría por universitarios, la comunidad de tecnología y emprendimiento más grande de México, con un enfoque de innovación, creatividad, ciencia y entretenimiento digital experimentaron iAnimal impactando a los campuseros&nbsp;ya que la tecnología nos ha permitido trasladar al espectador a la realidad de los animales y generar una experiencia de gran impacto en sus vidas y en las vidas de los animales.

Más de 1,000 campuseros probaron nuestro proyecto de realidad virtual <a href="https://ianimal.es/mx/">iAnimal</a>, 8,000 universitarios se suscribieron a nuestros boletines nutricionales y se repartieron 6,000 guías vegetarianas.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="https://www.flickr.com/photos/igualdadanimal/35004873434/in/album-72157683298795124/"><img src="https://farm5.staticflickr.com/4261/35004873434_491474c25e_z.jpg" alt="iAnimal en Campus Party 2017"/></a></figure>
<!-- /wp:image -->

<!-- wp:html -->
<script async="" src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Eventos como el Campus Party, que buscan “Inspirar a los campuseros y a las ciudades para el gran reto de ser parte fundamental de los cambios que tendrá la humanidad en los años siguientes”, por esa razón dimos voz a los animales de granja.

Estamos convencidos que cambiar la realidad que padecen los animales tiene que formar parte de ese gran cambio social que se requiere para generar sociedades justas y compasivas.

<strong>Tú, como los campuseros que vieron el iAnimal, puedes iniciar hoy mismo un viaje para conocer opciones de alimentación compasiva <a href="https://loveveg.mx/">suscribiéndote a nuestro e-boletín</a> que incluye un recetario vegetariano gratuito.&nbsp;</strong></p>
<!-- /wp:paragraph -->