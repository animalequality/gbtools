<!-- wp:paragraph -->
<p>Nuestro equipo en Italia acaba de presentar una nueva investigación en mataderos de conejos. La matanza de estos animales a menudo se efectúa sin aturdimiento previo, sometiendo a los animales a un sufrimiento completamente innecesario.

La investigación es acompañada de <a href="https://animalequality.it/video-macello-conigli/" target="_blank" rel="noopener noreferrer">una petición</a> para poner fin a estas prácticas ilegales aumentando los controles.

Desde diciembre del año pasado hemos mostrado videos que evidencian las condiciones de los animales en Italia, ya sean <a href="https://www.youtube.com/watch?v=GsUMaPEYS0w&amp;feature=youtu.be" target="_blank" rel="noopener noreferrer">pequeños corderos matados mientras están conscientes</a> o <a href="https://www.youtube.com/watch?v=Me6Fvnhf8Hc&amp;feature=youtu.be" target="_blank" rel="noopener noreferrer">cerdos que no son aturdidos</a> y tratan desesperadamente de escapar a este destino.

Pero esta vez quisimos adentrarnos en un matadero de conejos, gracias al trabajo de nuestro investigador que, actuando de forma encubierta, logró registrar las condiciones de crianza y final de todos estos animales en un matadero cercano a Brescia, Italia.

Los conejos pasan toda su vida en una jaula y, <strong>con tan solo 12 semanas de vida, son enviados al matadero</strong>, a menudo sin que se cumpla el respeto de las normas, en particular las relacionadas con el aturdimiento.

En el video se puede apreciar claramente que:
</p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><!-- wp:list -->
<ul><!-- wp:list-item -->
<li>Los operadores no aturden adecuadamente a los animales.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Los operadores degollan sistemáticamente a conejos conscientes.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Los operadores rompen el cuello de los animales con sus manos.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Los animales se ven obligados a esperar su propio final amontonados en jaulas.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Los conejos gritan de miedo y terror cuando son sacados de las jaulas.</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Los animales se ven obligados a vivir y ser transportados en jaulas donde no pueden desarrollar un comportamiento natural.</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></blockquote>
<!-- /wp:quote -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/02T8XEVwMqs" width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><strong>El aturdimiento es obligatorio para todos los animales antes del sacrificio</strong>, pero, de acuerdo con la legislación italiana, solo están contempladas sanciones administrativas para quienes violan los requisitos del procedimiento de sacrificio, sanciones que en los casos más graves establecen una multa de 6.000 euros.

Esta falta de consecuencias penales severas, junto con la ausencia total de controles estrictos, implica una mayor ligereza en el manejo de esta fase por parte de los operadores que trabajan en los mataderos y, como muestran nuestras investigaciones, <strong>a menudo obliga a los animales a sufrir.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12947} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/03/40762805422_bcf07bfe3d_k_1_0.jpg" alt="" class="wp-image-12947"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

«Esta investigación muestra que es necesario introducir normas específicas para la protección de los animales sacrificados en nuestro país», explica Matteo Cupi, director ejecutivo de Igualdad Animal en Italia.

«Europa ya se ha expresado a favor de una mayor protección hacia los conejos criados para carne, que ya no se mantendrán en una jaula gracias a una votación del Parlamento Europeo en marzo de 2017, un resultado histórico obtenido gracias a una intensa campaña de presión política. Obviamente, sin embargo, todo esto no es la solución al sufrimiento que los animales viven en los mataderos. De hecho, los consumidores pueden ayudar a los conejos primero haciendo elecciones específicas de alimentos, cómo reducir o sustituir el consumo de carne».
Desde hace años hemos estado trabajando para reducir el sufrimiento de los animales durante la matanza. Es por eso que hemos lanzado una petición dirigida al Parlamento para obtener, entre las diversas solicitudes, la instalación de cámaras de vigilancia en CCTV en mataderos y el respeto por el aturdimiento sin excepciones.
Sabemos que no es la solución definitiva que quisiéramos, pero estamos dispuestos a hacer todo lo posible para mejorar las vidas de los animales, con la esperanza de que la situación cambie pronto.

Todo esto es posible también porque hay ciudadanos involucrados en esta batalla a nuestro lado. <a href="https://animalequality.it/video-macello-conigli/" target="_blank" rel="noopener noreferrer">Más de 60,000 personas ya han firmado nuestra petición, ¡únete a ellas!</a></p>
<!-- /wp:paragraph -->