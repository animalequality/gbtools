<!-- wp:paragraph -->
<p>Hace varios meses filmamos a trabajadores de Fir Tree Farm en Lincolnshire, Inglaterra, pateando a cerdos en la cara, pinchándolos con horcas y cerrando puertas con sus cabezas. Hoy en el Tribunal de Magistrados de Grimsby,<strong> tres hombres recibieron condenas totalmente inadecuadas por estos actos deliberados de crueldad</strong>. Estamos extremadamente decepcionados de que estos abusadores de animales no hayan sido enviados a prisión por sus ataques contra los cerdos que estaban a su cuidado.

Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2018/08/01/ultima-hora-destapamos-la-violencia-y-negligencia-en-una-granja-de-cerdos-en/" target="_blank" rel="noopener noreferrer">Última hora: destapamos la violencia y negligencia en una granja de cerdos en Inglaterra</a>

Troy Wagstaff, un supervisor y el administrador de bienestar animal en la granja, que fue filmado pateando violentamente a los cerdos al menos 20 veces en unos pocos días, Gavin Hardy y Artis Grogprkevs <strong>recibieron una sentencia de prisión de 8 semanas y una orden comunitaria de 100 horas de trabajo no remunerado.</strong> También se les ordenó pagar los cargos de la corte.

</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/zs8E9CwVRWg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>

Nuestros investigadores encubiertos visitaron la granja en abril y mayo de 2018 después de un aviso anónimo y descubrieron violencia repetida y deliberada por parte de varios trabajadores. Sin el coraje del denunciante y de nuestros investigadores, estos trabajadores seguirían abusando de los cerdos en la granja de abetos. <strong>Este caso demuestra que las leyes y las etiquetas no protegen a los animales de los abusos en las granjas</strong>; la única forma de detener este sufrimiento es mediante <a href="https://loveveg.mx/" target="_blank" rel="noopener noreferrer">la elección de opciones sin carne.</a>

Fir Tree Farm <strong>es propiedad de Elsham Linc, uno de los productores de cerdos más grandes de Gran Bretaña</strong>, está certificada por el sistema de garantía Red Tractor y suministra a grandes minoristas, incluyendo Tesco.

Como resultado de nuestras investigaciones en el Reino Unido, seis trabajadores agrícolas han sido condenados por crueldad hacia los animales. <strong>En mayo de 2017, un trabajador de una granja lechera en Somerset recibió una sentencia de prisión de 12 semanas</strong> después de que nuestros investigadores lo filmaron violentamente abusando de sus terneros recién nacidos en una granja familiar que proporcionaba Muller Milk, uno de los principales procesadores utilizados por Marks and Spencer, Tesco. , Sainsbury's y Co-op para sus propias marcas de leches.

En 2012, dos trabajadores fueron filmados matando a los lechones con una barra de metal en Harling Farm en Norfolk; ambos hombres se declararon culpables de cargos de crueldad y uno fue encarcelado durante 18 semanas.</p>
<!-- /wp:paragraph -->