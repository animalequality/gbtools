<!-- wp:paragraph -->
<p>Tobias Leenaert se ha dedicado a desarrollar estrategias comunicativas que favorezcan el cambio hacia una alimentación vegana y a ofrecer presentaciones alrededor del mundo para comunicarlas. <strong>Su enfoque apunta a la efectividad y el pragmatismo</strong> como estrategia, y no podría ser de otra forma si va dirigido a un movimiento que pretende transformar a un sistema de producción de alimentos basado en una de las industrias más poderosas del mundo: la ganadera.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13448} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/11/estreno_mexico_hacia_un_futuro_vegano_igualdadanimal.org_2.jpg" alt="" class="wp-image-13448"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

A mediados de este año Leenart presentó la edición en español de su libro <em>Hacia un futuro vegano</em>, <strong>una obra en la que sorprende al tomar caminos alternativos</strong> para abordar las herramientas comunicativas y los objetivos del movimiento vegano con un enfoque alejado de los convencionalismos.

De acuerdo con el autor,<strong> ser efectivos es la única forma en la que podemos lograr disminuir el sufrimiento de los animales</strong> en el menor tiempo posible: “los veganos deben darse cuenta de que los argumentos morales no son suficientes. Tenemos que hacer que comer vegano sea más fácil para la gente proporcionando más alternativas, y entonces será mucho más fácil para la gente cambiar sus actitudes hacia los animales”.
</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"></h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading"><a href="https://loveveg.mx/">Consigue GRATIS un fantástico recetario con deliciosos platillos veggie</a>, además de una guía con consejos prácticos para una alimentación sostenible.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>
Por su parte, Peter Singer, filósofo y autor del clásico animalista Liberación Animal, considera a Leenaert como una de las pocas personas calificadas para tratar estos temas y recomienda ampliamente <em>Hacia un futuro vegano</em> como «un lugar excelente por donde comenzar» para quienes quieran contribuir al movimiento vegano y a construir el mundo mejor que todos soñamos.

Dulce Ramírez, directora de Igualdad Animal México indicó que la presentación del libro en México se realizará en febrero del 2019 con la presencia del autor: "sin duda este libro resultará muy significativo para el activismo en México. Tobias Leenaert es un gran comunicador de estrategias que serán de gran impacto y utilidad para el trabajo que hoy se hace en el país para promover el veganismo”.

<em>Hacia un futuro vegano</em> está ya disponible <a href="https://www.amazon.es/dp/8417121129/ref=cm_sw_r_cp_ep_dp_Y7y2BbNAR554D">a la venta </a>en español a través de Amazon España y pronto lo estará también en México, luego de su presentación oficial en el país.</p>
<!-- /wp:paragraph -->