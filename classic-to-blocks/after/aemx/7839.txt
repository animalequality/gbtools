<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Desde que su padre murió en 2011 a Jay Wilde le tocó hacerse cargo de la granja familiar. Pero <strong>la historia de este granjero de 59 años es bastante peculiar porque además de preocuparse por el bienestar de esas vacas y del impacto ecológico de su granja, Jay ha sido vegetariano desde hace 25 años</strong>.

«Estaba resultando muy difícil cuidarlas por dos o tres años, llegar a conocerlas muy bien y luego enviarlas al matadero. Sientes como si las has traicionado», declara Wilde.

En lugar de eso, Jay, que pudo haber obtenido 45.000 libras esterlinas a cambio de este rebaño, prefirió abandonar el negocio y liberarlas del sufrimiento y la muerte enviando un total de 59 vacas al refugio Hillside Animal Sanctuary ubicado en Norfolk, Inglaterra.

Este refugio es el hogar de otras 1.900 vacas, toros, caballos, burros y ponis que escaparon del terrible final que la industria tenía preparado para ellos.

<a href="https://igualdadanimal.mx/suscripcion/" target="_blank" rel="noopener noreferrer">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.

30 de las vacas que Jay envió al refugio están embarazadas y podrán vivir junto a sus hijos sin sufrir ni una vez más la dolorosa separación a la cual la granja las sometía.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/6-crueles-practicas-estandar-que-la-industria-lactea-prefiere-que-no-conozcas/" target="_blank" rel="noopener noreferrer">6 crueles prácticas estándar de la industria láctea con las vacas</a></strong>

Wendy Valentine, la fundadora del refugio, dijo que las vacas ahora <strong>podrán también disfrutar de sus 25 años de vida completos en lugar de los solo dos a tres años determinados por su envío al matadero.</strong>

Dentro de la industria láctea las vacas son forzadas a soportar embarazos continuos para producir leche. Esto, acompañado del profundo dolor de las continuas separaciones de sus hijos, deteriora tanto su salud que en solo dos o tres años su nivel de producción de leche desciende y son enviadas al matadero.

Jay dice que las vacas experimentan emociones y que saben cuándo las van a matar.

«Puedes saber cuando están felices, tristes o aburridas y puedes ver cuando algo ocurre a través de sus ojos. Tienen excelente memoria y un amplio rango emocional. Pueden formar lazos. Incluso las he visto llorar».

Jay también afirma que «Obviamente ha tenido lugar un fuerte conflicto de intereses entre el hecho de que no como carne pero estaba criando animales para producir carne». Y agrega con tristeza: «A lo largo de los años debemos de haber enviado cientos de vacas al matadero».

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12206} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/07/96459522_hi040029853.jpg" alt="" class="wp-image-12206"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Tom Kuehnel, oficial de campañas de The Vegan Society, declaró que estuvieron involucrado con el granjero desde que él los contactó luego de conocer su informe «Go Green» (Hazte Verde). Este informe alienta a los granjeros a abandonar la agricultura animal para dedicarse a producir cultivos para el consumo humano.

Y precisamente fue también The Vegan Society quien encontró el nuevo hogar para las 59 vacas y está coordinando y facilitando la transición de la granja de Jay.

Las vacas en su nuevo hogar, el refugio Hillside Animal Sanctuary en Norfolk, Inglaterra.

El granjero ahora planea cultivar verduras orgánicas libres de productos de origen animal y fertilizantes para vender en el creciente mercado de productos basado en vegetales.

«Este es el primer caso de este tipo y Jay es un verdadero pionero, esperamos que inspire a otros agricultores a avanzar hacia métodos de cultivo más compasivos y sostenibles que no involucren animales “, declaró Kuehnel.

Fuentes:

<a href="https://www.bbc.com/news/av/magazine-40448684" target="_blank" rel="noopener noreferrer">https://www.bbc.com/news/av/magazine-40448684</a><a href="http://www.dailymail.co.uk/news/article-4601956/Vegetarian-farmer-donates-cows-animal-sanctuary.html" target="_blank" rel="noopener noreferrer">http://www.dailymail.co.uk/news/article-4601956/Vegetarian-farmer-donates-cows-animal-sanctuary.html</a><a href="https://www.independent.co.uk/life-style/vegetarian-farmer-cow-herd-save-abattoir-jay-wilde-animal-sanctuary-bradley-nook-derbyshire-a7789131.html" target="_blank" rel="noopener noreferrer">https://www.independent.co.uk/life-style/vegetarian-farmer-cow-herd-save-abattoir-jay-wilde-animal-sanctuary-bradley-nook-derbyshire-a7789131.html</a></p>
<!-- /wp:paragraph -->