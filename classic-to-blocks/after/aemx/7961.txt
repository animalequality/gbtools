<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Desde agosto de este año, Igualdad animal está trabajando para lograr algo histórico para los animales en el estado de Jalisco: <strong>que los actos de crueldad cometidos hacia ellos en los rastros (mataderos) sean tipificados como delito</strong>. Para esto ha solicitado una modificación a dos de los actuales artículos dentro del Código Penal. Esto aseguraría que se cumpliesen las normas federales en el manejo de animales de granja, que son de carácter obligatorio.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":12624} -->
<figure class="wp-block-image"><img src="/app/uploads/2017/11/31204897176_4383b8675e_z.jpg" alt="" class="wp-image-12624"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

La iniciativa ha derivado de la investigación de Igualdad Animal en 31 rastros (mataderos) municipales y 7 estatales en México. Las imágenes obtenidas son pruebas contundentes de que en <strong>estos lugares los animales son víctimas de una crueldad extrema</strong>. Se les mata estando aún conscientes, se les tortura y mueren agónicamente.

<span style="font-size: xx-small;"><span style="color: #808080;">Investigación de Igualdad Animal en rastros de México.</span></span>

Todo esto está ocurriendo a pesar de que existe una normativa federal que lo prohíbe. Actualmente no existen consecuencias legales para los responsables y esto ha hecho que los actos de crueldad sean la norma en los rastros.
En México existe la llamada «Norma Oficial Mexicana», que define y estandariza cuáles son los métodos para dar muerte a los animales. Según ella es <strong>obligatorio provocar la inconsciencia previa del animal </strong>para garantizar que tenga una muerte rápida, sin sufrimiento ni ansiedad y con el mínimo nivel &nbsp;de estrés.
<span style="font-size: xx-small;"><span style="font-size: x-large;"><span style="color: #808080;"><a href="https://descubrirlacomida.com/" target="_blank" rel="noopener noreferrer">Suscríbete gratuitamente a nuestro e-boletín</a> y recibe las mejores noticias de actualidad sobre los animales y opciones de alimentación.</span></span></span>

En la actualidad, el Código Penal de Jalisco no contempla que se cumpla con el marco normativo federal. Esta iniciativa aseguraría que más de 187 millones de animales actualmente desamparados quedaran legalmente protegidos.
Con esta iniciativa <strong>Jalisco está a un paso de volver a hacer historia por los animales</strong> y de adelantarse a la posible aprobación de una legislación similar que se encuentra actualmente en el Senado de la República.
«Es muy satisfactorio encontrar voluntad política para ayudar a los animales, especialmente a los animales de granja que se encuentran totalmente invisibilizados en las legislaciones estatales. Lo que pide esta iniciativa es muy simple, que se cumpla lo que ya es obligatorio y de no cumplirse que se castigue», comentó Dulce Ramírez, Directora Ejecutiva de Igualdad Animal en México.
Conoce nuestra investigación en <a href="https://rastrosdemexico.igualdadanimal.mx/">Rastros de México</a> y apoya con tu firma las iniciativas para acabar con la crueldad animal.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/blog/5-razones-para-apoyar-la-iniciativa-contra-la-crueldad-en-rastros-de-jalisco/">5 razones para apoyar la iniciativa contra la crueldad en rastros de Jalisco</a></strong></p>
<!-- /wp:paragraph -->