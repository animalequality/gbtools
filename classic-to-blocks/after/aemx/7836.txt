<!-- wp:paragraph -->
<p>Pacific Star Foodservice anunció hoy su compromiso de eliminar de su línea de suministro el huevo proveniente de jaula de batería para 2025. Esta decisión, que reduce el sufrimiento de&nbsp;miles de aves, es resultado de negociaciones de&nbsp;Igualdad Animal con la compañía.

Pacific Star es una empresa de servicios de alimentos que opera en todo México y gracias a su alianza con Sysco ofrece a sus clientes soluciones y servicios de primera calidad. Sus clientes más reconocidos incluyen Cinépolis, el grupo Hilton, Burger King y KFC.

La <a href="http://www.pacificstar.com.mx/nosotros.html#social" target="_blank" rel="noopener noreferrer">nueva política de Pacific Star</a> se suma a una serie de medidas tomadas por diversas empresas &nbsp;en México como Compass Group y Alsea para deshacerse del uso de jaulas en respuesta a los consumidores, quienes cada día exigen una mayor transparencia así como mejoras en el trato animal dentro del sistema alimentario.

Aunque libre de jaula no significa libre de crueldad, este compromiso es un avance hacia un mundo más compasivo.

Lamentablemente, Soriana sigue sin atender los llamados de la sociedad para ayudar acabar con &nbsp;el sufrimiento de estas gallinas. Por favor, firma <a href="https://igualdadanimal.mx/actua/soriana-sin-jaulas" target="_blank" rel="noopener noreferrer">nuestra petición</a> para pedirle a Soriana que deje de proveerse de huevos de gallinas encerradas en jaulas.</p>
<!-- /wp:paragraph -->