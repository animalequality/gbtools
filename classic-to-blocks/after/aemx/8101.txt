<!-- wp:paragraph -->
<p>Cerca de veinte personas acudieron a las oficinas del corporativo de la galletera mexicana Mac’Ma para entregar, junto a representantes de Igualdad Animal, más de 15 mil firmas dirigidas al Director General, Xavier Olazábal. En las mismas, le piden que elimine las crueles jaulas para las gallinas en su suministro de huevo y exigen que escuche la opinión de sus consumidores.

La petición fue lanzada hace algunos meses y ha sido controversial la falta de respuesta de la empresa y la información poco clara que comparte con sus clientes. La industria del huevo somete a las gallinas a uno de los peores maltratos y, sin duda, los consumidores al conocer la situación se desconciertan y exigen a las empresas que hagan cambios para ayudar a los animales.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13080} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/05/39593857880_25b44bf117_z.jpg" alt="" class="wp-image-13080"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

En México, las panificadoras y galleteras más importantes ya están trabajando para poner fin a las jaulas y el director de Mac’Ma, Xavier Olazábal, se está quedando atrás en este tema tan necesario e importante. Líderes de todos los sectores de la industria alimentaria en México han visto una oportunidad y necesidad en implementar políticas empresariales para acatar los valores de la sociedad que incluyen cada vez más a los animales de granja.

<strong>Te podría interesar: <a href="https://igualdadanimal.mx/blog/como-pueden-las-grandes-empresas-cambiar-la-vida-de-millones-de-animales/">¿Cómo pueden las grandes empresas cambiar la vida de millones de animales?</a></strong>

Desde Igualdad Animal exigimos a Xavier Olazábal una respuesta inmediata a las más de 15,000 firmas entregadas el pasado 11 de abril. Cada firma, representa la voz de un cliente de Mac’Ma que le pide que no voltee la mirada y responsablemente se comprometa a ayudar a reducir el sufrimiento de las gallinas, poniendo una fecha límite para rechazar las jaulas de su suministro de huevo.

Si te gustaría apoyar la campaña entra a la página de petición en <a href="https://igualdadanimal.mx/actua/macma-sin-jaulas" target="_blank" rel="noopener noreferrer">https://igualdadanimal.mx/actua/macma-sin-jaulas</a><span style="font-size: x-large;">&nbsp;</span></p>
<!-- /wp:paragraph -->