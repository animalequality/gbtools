<!-- wp:html -->
<div class="center"></div>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Nuestro equipo de investigaciones registró escenas impactantes de <strong>terneros angustiados siendo tratados brutalmente</strong> y alimentados a la fuerza en una granja lechera orgánica que abastece a los supermercados Waitrose en Inglaterra.

<strong>Noticia relacionada: <a href="https://igualdadanimal.mx/noticia/2018/05/24/ultima-hora-filmamos-trabajadores-golpeando-brutalmente-cerdos-en-una-granja-de/">ÚLTIMA HORA: filmamos a trabajadores golpeando brutalmente a cerdos en una granja de Inglaterra</a></strong>

Las impactantes escenas que se filmaron este verano muestran:
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent2"} -->
<p class="rteindent2">* Los terneros recién nacidos luchando mientras los trabajadores les introducen cruelmente un tubo en la garganta.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent2"} -->
<p class="rteindent2">* Un ternero es tirado al suelo y abofeteado mientras lo alimentan forzadamente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent2"} -->
<p class="rteindent2">* Un trabajador parado sobre la pantorrilla de un ternero con todo su peso mientras grita «maldita m**rda».</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent2"} -->
<p class="rteindent2">* Las vacas se agitan visiblemente e intentan intervenir mientras los terneros forcejean y las llaman desesperadamente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent2"} -->
<p class="rteindent2">* A los terneros se les negó el acceso al agua por hasta 29 horas en algunos de los días más calurosos del año.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent2"} -->
<p class="rteindent2">* Un becerro recién nacido es arrastrado por sus patas traseras a un corral de separación.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent2"} -->
<p class="rteindent2">* Se acostumbra a separar a los becerros de sus madres menos de 24 horas después de su nacimiento.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"rteindent2"} -->
<p class="rteindent2">* Varias vacas con sus patas traseras encadenadas entre sí con grilletes.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<iframe src="https://www.youtube.com/embed/wfYHyIvIRXk" width="800" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>
Lejos de la imagen idílica de la producción láctea, el cruel trato que se da a los terneros recién nacidos mientras sus madres miran con impotencia, indudablemente sorprenderá a los consumidores. Sin embargo, <strong>la ruptura de este fuerte vínculo materno es una parte inherente de la industria láctea</strong>, donde las madres y los bebés son separados de manera rutinaria para que su leche pueda ser embotellada y vendida a nosotros.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13289} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/08/29277605067_eded111ace_z.jpg" alt="" class="wp-image-13289"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

Nuestra investigación revela que Coombe Farm no cumple con los requisitos legales mínimos para el cuidado de los terneros y está muy por debajo de los estándares orgánicos de la Real Sociedad para la Prevención de la Crueldad hacia los Animales (RSPCA), a pesar de contar con la certificación de Soil Association y RSPCA Assured. Hemos presentado nuestras imágenes a ambas organizaciones, así como a Waitrose.</p>
<!-- /wp:paragraph -->