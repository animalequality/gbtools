<!-- wp:paragraph -->
<p>El pasado miércoles 27 de febrero<a href="https://igualdadanimal.mx/noticia/2019/02/28/marisa-rechazo-la-peticion-de-miles-contra-la-crueldad/" target="_blank" rel="noopener noreferrer"> tuvimos un acto</a> muy emocionante y que nos llena de esperanza: <strong>fuimos a entregar las más de <a href="https://www.change.org/p/pastelesmarisa-%C3%BAnete-para-eliminar-este-maltrato-animal" target="_blank" rel="noopener noreferrer">26 mil firmas de personas que piden a Pastelerías Marisa</a> que ayude a reducir significativamente el sufrimiento de miles de gallinas</strong> mediante un compromiso público para prohibir las crueles jaulas.

Tras varios intentos, <strong>conseguimos que una representante de la empresa recibiera el oficio de las firmas pero al final nadie salió a recibir las firmas impresas</strong>. No nos fuimos con desilusión, al contrario, nos llenamos de esperanza porque sabemos que es un avance y creemos firmemente que el sufrimiento que padecen las gallinas tocará el corazón de Marisa y que, finalmente, hará que se pronuncie públicamente a favor del fin de las jaulas.
</p>
<!-- /wp:paragraph -->

<!-- wp:shortcode -->
[embed]https://flic.kr/p/2dRHSsw[/embed]
<!-- /wp:shortcode -->

<!-- wp:paragraph -->
<p>No es una tarea sencilla representar de la forma lo más profesional pero apasionada posible las voces de todas las personas que se oponen al maltrato animal al que la industria del huevo somete a millones de gallinas alrededor del mundo. La fortaleza siempre viene del trabajo en conjunto y de la esperanza compartida de que juntos podemos construir un mundo en el que los animales sean respetados y en el que los consumidores seamos quienes definamos lo que las empresas deberían hacer y cómo deberían definir sus valores y principios.
</p>
<!-- /wp:paragraph -->

<!-- wp:shortcode -->
[embed]https://flic.kr/p/24maSSZ[/embed]
<!-- /wp:shortcode -->

<!-- wp:paragraph -->
<p>Nuestra voz es la única defensa que tenemos en contra de las injusticias y es por eso que en Igualdad Animal queremos que cada voz cuente. <strong>Tú puedes ser parte de acciones sencillas para ayudar a acabar con uno de los sistemas más crueles: las jaulas. </strong>Entra hoy a <a href="https://igualdadanimal.mx/protectores-de-animales/" target="_blank" rel="noopener noreferrer">Protectores de Animales</a> y ¡únete a nosotros!</p>
<!-- /wp:paragraph -->