<!-- wp:paragraph -->
<p>Hace pocos días, el &nbsp;ministro de Agricultura y Alimentación de Francia, Didier Guillaume,<strong> ha condenado las «prácticas inaceptables» de maltrato en el matadero de Boischaut</strong> en Indre y ordenó su cierre inmediato y temporal. La decisión fue provocada por la reacción inmediata de Guillaume tras ver las imágenes publicadas por la organización L214 que muestran actos de crueldad hacia los animales.

Los hallazgos comprueban que tras los muros de los mataderos la violencia está normalizada. Los videos obtenidos con cámara oculta muestran escenas de brutal violencia con animales suspendidos de una sola pata y <strong>que se desangran mientras están plenamente conscientes. </strong>Uno de los empleados llega incluso a cortar la pata de un animal en la línea de matanza antes de que este muera.

</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":13450} -->
<figure class="wp-block-image"><img src="/app/uploads/2018/11/clausuran_matadero_francia_crueldadigualdadanimal.org_.jpg" alt="" class="wp-image-13450"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>

A pesar de que Guillaume expresó su rechazo ante estos actos y afirmó que la lucha contra el maltrato animal es una prioridad del actual gobierno, y que el ministro de Estado y de Transición Ecológica y Solidaria quiere «controlar continuamente los mataderos, la organización L214 ha denunciado que en los últimos tres años la situación sigue idéntica.

Si bien aplauden la rápida respuesta del ministro, recuerdan que existen normas para todos los mataderos y que las violaciones a las mismas causan un inmenso sufrimiento a los animales que pudiera ser evitado. Todos estos delitos pasan desapercibidos y nunca son sancionados como corresponde.

Una auditoría llevada a cabo en 2016 demostró que en el 80% de los mataderos franceses se viola la normativa europea de bienestar animal. Gracias a las imágenes obtenidas por L214 en los últimos años y que han tenido un gran impacto mediático, el ejecutivo francés ha comenzando a intentar controlar el abuso hacia los animales. La Asamblea Nacional aprobó una ley que obligó a instalar cámaras en todos los mataderos franceses a partir del 1 de enero de este año.

Particularmente en el caso del matadero de Boischaut, se tiene conocimiento de estos actos de crueldad desde hace más de dos años sin que haya habido ningún cambio o sanción, y el matadero siguió operando como si este control no hubiese tenido lugar.

La organización L214 logró que System U y Carrefour, dos de las cadenas de distribución que comercializaban con el matadero de Boischaut, dejaran de abastecerse de inmediato de la carne que producía. Ambas empresas se encuentran actualmente en espera de los resultados de la investigación para reevaluar su relación comercial con dicha instalación.

Fuentes:

https://www.francebleu.fr/infos/economie-social/abattoir-du-boischaut-ca-nous-fait-souffrir-disent-les-eleveurs-presents-au-concours-charolais-de-la-1541787789

https://www.elmundo.es/ciencia-y-salud/ciencia/2018/11/03/5bddac54268e3e2f1c8b463a.html

https://www.l214.com/communication/20181103-fermeture-temporaire-abattoir-boischaut

https://www.liberation.fr/france/2018/11/04/abattoir-du-boischaut-la-chaine-de-l-effroi_1689895</p>
<!-- /wp:paragraph -->