import os from 'os';
import mysql from 'mysql2-async';
import ini from 'node-ini';
import { debug, info } from './console-utils.js';
import {
    computeId,
    getPackagesVersions,
    getWpVersionFromDatabase,
    packagesVersionsMatch,
} from './gb-version-utils.js';
import process from "node:process";

const slugToLocale = {
    de: 'de_DE',
    uk: 'en_GB',
    us: 'en_US',
    in: 'en_GB',
    es: 'es_ES',
    mx: 'es_MX',
    it: 'it_IT',
    br: 'pt_BR',
};

class SqlProvider {
    #sources;
    #options;
    #write;
    #conn;

    constructor(sources, write, options) {
        this.type = 'sql';
        this.#sources = sources;
        this.#write = write;
        this.#options = options;
        this.connect();
    }

    async computeId() {
        return await computeId(this.#conn);
    }

    async preCheck() {
        const remote_wp_version = await getWpVersionFromDatabase(this.#conn),
              versions = await getPackagesVersions(remote_wp_version),
              match = packagesVersionsMatch(...versions);
        if (!match) {
            console.error(
                `Versions mismatch:
WP ${versions[2]} running at ${this._dsn()}
gbtools   GB version: ${JSON.stringify(versions[0])} (package.json)
WordPress GB version: ${JSON.stringify(versions[1])} (${versions[2]}'s online package.json)
Run \`wp cron event run acf_update_site_health_data\` if you feel the WP ACF DB field doesn't reflect actual WP state
`
            );

            return false;
        }
        debug('[mysql] GB version check passed');
        return true;
    }

    /**
     * When running this function, don't forget to finish SQL connection using `process.exit()`
     */
    connect() {
        const { db, host, user, port, pass } = this.#options;
        let cnf;
        if (user || port || pass) {
            process.env.MYSQL_USER = process.env.MYSQL_USER || user;
            process.env.MYSQL_PORT = process.env.MYSQL_PORT || port;
            process.env.MYSQL_PASS = process.env.MYSQL_PASS || pass;
            cnf = {};
        } else {
            cnf = ini.parseSync(os.homedir() + '/.my.cnf');
        }

        this.#conn = new mysql({
            skiptzfix: true,
            database: db, // default to MYSQL_DATABASE otherwise
            host,
            ...cnf.client,
        });
        const c = this.#conn.conn.config.connectionConfig;
        debug(
            `[mysql] Connecting as ${c.user}@${c.host}:${c.port} on ${c.database}`,
        );
    }

    _details() {
        return this.#conn.conn.config.connectionConfig;
    }

    _dsn() {
        const c = this._details();
        return `${c.user}@${c.host}:${c.port}/${c.database}`;
    }

    /** Returns an async iterator. `await yield` over the results */
    select(fields) {
        const { where, limit } = this.#options;
        let fields_map = fields;
        if (!Array.isArray(fields)) {
            fields_map = Object.entries(fields).map(([k, v]) => `${k} as ${v}`);
        }

        const _where = ['post_status = "publish"'];
        const sources = this.#sources.map((e) => parseInt(e)).filter((e) => e);
        if (sources.length > 0) {
            _where.push(`ID IN (${sources.join(',')})`);
        }
        if (where) {
            _where.push(where);
        }

        const query = `SELECT ${fields_map.join(',')}
FROM wp_posts
WHERE ${_where.join(' AND ')}
ORDER BY id ASC
${limit ? 'LIMIT ' + limit : ''}`;
        debug(`[mysql] ${query}`);

        return this.#conn.iterator(query);
    }

    it() {
        return this.select({ id: 'source', post_content: 'content' });
    }

    async update_content(post_id, content) {
        return await this.#conn.update(
            'UPDATE wp_posts SET post_content = ? WHERE ID = ?',
            [content, post_id],
        );
    }

    async write(source, blocks) {
        if (!this.#write) {
            info(`[dry-run] mysql UPDATE`);
            return null;
        }

        const markup = wp.blocks.serialize(blocks),
            rows = await this.update_content(source, markup);
        info(`|mysql] UPDATED post ${source}: ${rows} rows modified`);
        if (rows > 2) {
            throw Error(
                `ERROR ! UPDATE statement causes more than one rows (${rows}) to be updated`,
            );
        }
        return rows === 1;
    }

    /**
     * Detect locale based on database's name.
     */
    detectLocale() {
        const db = this.#options.db;
        if (
            db.match(
                /^(animaleuqality|igualdadanimal)(org|in|orguk|mx|de|orgbr|it)|ae(us|in|uk|mx|es|de|br|it)$/,
            )
        ) {
            let suffix = db.slice(-2);
            suffix = suffix = 'rg' ? (db.includes('igualdadanimal') ? 'es' : 'us') : suffix;
            return slugToLocale[suffix];
        }
        return null;
    }
}

export { SqlProvider };
