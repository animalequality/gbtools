import child_process from 'child_process';
import { debug, info, isInfo } from './console-utils.js';

/**
 * Arbitrary command
 */
class CommandProvider {
    #sources;
    #options;
    #write;
    // Eg: `ssh foo cat %d`
    #readCommand;

    // Eg: `tee %s`
    #writeCommand;

    constructor(sources, write, options) {
        this.type = 'command';
        this.#sources = sources;
        this.#write = write;
        this.#options = options;
        this.#readCommand = options.from.replace(/^cmd:\/\//, '');
        this.#writeCommand = options.to.replace(/^cmd:\/\//, '');
    }

    interpolate(str, source) {
        return str.replace(/%source%|\$?\{source\}|\[source\]|%d/, source);
    }

    *it() {
        if (!this.#readCommand) {
            throw Error(`No read command specified`);
        }

        for (let source of this.#sources) {
            const cmd = this.interpolate(this.#readCommand, source);
            const result = child_process.spawnSync(cmd, [], { shell: true });
            if (result.status != 0) {
                console.error(`Error code ${result.status} running ${cmd}`);
                console.log(result);
                throw Error(result.stderr?.toString());
            }
            yield { source: source, content: result.stdout.toString() };
        }
    }

    async write(source, blocks) {
        if (!this.#writeCommand) {
            throw Error(`No write command specified`);
        }

        const markup = wp.blocks.serialize(blocks);
        let cmd = this.interpolate(this.#writeCommand, source);

        if (!cmd.includes(source)) {
            throw Error(
                `"${cmd}" doesn't seem to include "${source}" after placeholder replacement. Likely an error.`,
            );
        }

        if (!this.#write) {
            info(`[dry-run] write command: ${cmd}`);
            return null;
        }

        info(`[cmd] Running ${cmd}`);
        const result = child_process.spawnSync(cmd, [], {
            input: markup,
            shell: true
        });
        if (result.status != 0) {
            throw result.error ? result.error : Error(result.stderr?.toString());
        }
        if (isInfo()) {
            info(`[cmd] post ${source} modified`);
            if (result.stdout) {
                debug('cmd stdout: ' + result.stdout.toString());
            }
        }

        return result.status === 0;
    }

    /**
     * Detect locale based on database's name.
     */
    detectLocale() {
    }
}

export { CommandProvider };
