import fs from 'fs';
import { debug, info } from './console-utils.js';

const slugToLocale = {
    de: 'de_DE',
    uk: 'en_GB',
    us: 'en_US',
    in: 'en_GB',
    es: 'es_ES',
    mx: 'es_MX',
    it: 'it_IT',
    br: 'pt_BR',
};

function getSource(uri) {
    if (uri === '-') {
        return fs.readFileSync('/dev/stdin').toString();
    }

    return fs.readFileSync(uri).toString();
}

class FsProvider {
    #sources;
    #options;
    #write;
    #out_file;

    constructor(sources, write, options) {
        this.type = 'fs';
        this.#sources = sources;
        this.#write = write;
        this.#options = options;
        this.#out_file = options.out_file || '';
        if (this.#options?.reader?._details) {
            this.reader_db = this.#options.reader._details().database;
            this.reader_host = this.#options.reader._details().host;
        }
    }

    *it() {
        for (const source of this.#sources) {
            yield { source, content: getSource(source) };
        }
    }

    write(source, blocks) {
        const output = wp.blocks.serialize(blocks);
        if (source === '-' && !this.#out_file) {
            console.log(output);
            return true;
        }

        let out_file = this.#out_file.replace(
            /%source%|\$?\{source\}|\[source\]|%s|%d/,
            source,
        );
        if (this.reader_db) {
            out_file = out_file.replace(/%db%|\$?\{db\}|\[db\]/, this.reader_db);
        }
        if (this.reader_host) {
            out_file = out_file.replace(
                /%host%|\$?\{host\}|\[host\]/,
                this.reader_host,
            );
        }
        out_file = out_file || source;

        if (!this.#write) {
            info(`[dry-run] fs write to ${out_file}`);
            return null;
        }

        fs.writeFileSync(out_file, output);
        info(`[fs] written to ${out_file}`);
        return true; // assume successful write
    }

    /**
     * Search for a locale based on a slug. If every <source> match "ae<slug>", it's assumed
     * to be a suitable locale
     */
    detectLocale() {
        const sources = this.#sources;

        var first_locale = false;
        for (let [slug, locale] of Object.entries(slugToLocale)) {
            for (let s of sources) {
                if (s.includes('acf.txt')) {
                    continue;
                }
                if (s.includes('ae' + slug)) {
                    if (first_locale && locale != first_locale) {
                        return null;
                    } else if (!first_locale) {
                        first_locale = locale;
                    }
                }
            }
        }

        return first_locale;
    }
}

export { FsProvider };
