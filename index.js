#!/usr/bin/env node

/**
 * Copyright (C) 2022, Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 *
 * This script bootstraps WP Gutenberg API under node.js to operate in batch
 * over blocks/blocks' markup.
 * convertStatisticBlock() and makeHistoryBlock() are two such examples.
 */

/**
 * JSDOM/MutationObserver initialization
 * @wordpress/priority-queue needs requestidlecallback which needs mutationobserver which needs jsdom
 */
import './globals.js';
import './globals-mutation.js';
import './globals-postload.js';

import { registerCoreBlocks } from '@wordpress/block-library';
import '@animalequality/gutenberg-blocks/build/node/node.js';
import { __ } from '@wordpress/i18n';
import program from './cli.js';
import process from "node:process";

let window,
    default_attrs = {},
    basedir = process.env.WP_BASE_DIR || './',
    wpincludedir = basedir + '/web/wp/wp-includes',
    plugindir = basedir + '/web/app/plugins',
    themedir = basedir + '/web/app/themes';

registerCoreBlocks();
program.parse();
