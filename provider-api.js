import fetch from 'node-fetch';
import https from 'https';
import apiFetch from '@wordpress/api-fetch';
import { addQueryArgs } from '@wordpress/url';

/**
 * Application password can be created using:
 * `wp user application-password create user-xxx rest`
 * don't forget Apache passing-on Authorization header:
 * `RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]`
 * then define environment variables:
 * - WP_USER: the regular user
 * - WP_PASS: the application password
 */
async function getRemoteSource(wpr, postType, id) {
    const agent = new https.Agent({
        rejectUnauthorized: false,
    });

    apiFetch.default.use(
        apiFetch.default.createRootURLMiddleware(`${wpr.base}/wp-json/`),
    );
    apiFetch.default.setFetchHandler((options) => {
        const { url, path, data, method } = options;

        return fetch(
            url || path,
            {
                method,
                body: data,
                agent,
                headers: {
                    authorization: `Basic ${btoa(wpr.WP_USER + ':' + wpr.WP_PASS)}`,
                },
            },
        );
    });

    const queryParams = { context: 'edit', include: [id] };
    const response = await apiFetch.default({
        path: addQueryArgs(`/wp/v2/${postType}`, queryParams),
    });
    const data = await response.json();

    if (data.data && data.data.status !== 200) {
        console.error(data.data.status, data.message);
        return null;
    }

    if (!data || !data.length) {
        console.error('No posts found');
        return null;
    }

    return data && data[0] ? data[0].content.raw : null;
}

class ApiProvider {
    #sources;
    #write;
    #options;

    constructor(sources, write, options) {
        this.type = 'api';
        this.#sources = sources;
        this.#write = write;
        this.#options = options;
    }

    *it() {
        for (const source of this.#sources) {
            yield { source, content: getRemoteSource(source, 'todo', 'todo') };
        }
    }

    write(source, blocks) {
        // ToDo
    }

    detectLocale() {
        // ToDo, based on remote URL ?
    }
}

export { ApiProvider };
