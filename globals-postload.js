import blockEditor from '@wordpress/block-editor';
import editor from '@wordpress/editor';
import element from '@wordpress/element';
import components from '@wordpress/components';
import blocks from '@wordpress/blocks';
import data from '@wordpress/data';
import coreData from '@wordpress/core-data';
import compose from '@wordpress/compose';
import domReady from '@wordpress/dom-ready';
import primitives from '@wordpress/primitives';
import viewport from '@wordpress/viewport';
import i18n from '@wordpress/i18n';
import date from '@wordpress/date';
import hooks from '@wordpress/hooks';
import React from 'react';
import ReactDOM from 'react-dom';
import lodash from 'lodash';
import jq from 'jquery';

// import {webcrypto as crypto} from 'node:crypto';
// import TestRenderer from 'react-test-renderer';

global.window.wp = global.wp = {
    element,
    components,
    blocks,
    data,
    coreData,
    compose,
    domReady,
    primitives,
    viewport,
    i18n,
    editor,
    blockEditor,
    date,
    hooks,
};

let categories = wp.blocks.getCategories();
categories.push(
    { 'slug': 'animalequality', 'title': 'Animal Equality' },
    { 'slug': 'animalequality-patterns', 'title': 'Animal Equality Patters' },
);
wp.blocks.setCategories(categories);

// Needs to be added to the global scope *before* not only loading but *importing* other blocks (like AE or cool-timeline)
global.window.React = global['React'] = React;

global.window.ReactDOM = global['ReactDOM'] = ReactDOM;
global.window.jQuery = global['jQuery'] = jq;
global.window.lodash = global['lodash'] = lodash;
// global.TestRenderer = TestRenderer;
// global.crypto = crypto;

global.window.ReactJSXRuntime = {
    jsx: React.createElement,
    jsxs: React.createElement,
};
