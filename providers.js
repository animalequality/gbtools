import process from 'process';
import { debug } from './console-utils.js';
import { SqlProvider } from './provider-mysql.js';
import { FsProvider } from './provider-fs.js';
import { StdioProvider } from './provider-stdio.js';
import { WpCliProvider } from './provider-wpcli.js';
import { CommandProvider } from './provider-command.js';
import { ApiProvider } from './provider-api.js';

const DATA_PROVIDERS = {
    wpcli: WpCliProvider,
    cmd: CommandProvider,
    mysql: SqlProvider,
    fs: FsProvider,
    stdio: StdioProvider,
    api: ApiProvider,
};

function getProviderFromUrl(direction, str) {
    try {
        let o = new URL(str);

        if (o.protocol === 'mysql:') {
            let args = {
                host: o.hostname,
                port: o.port,
                user: o.username,
                pass: o.password,
                db: o.pathname.replace(/^\/*([^/]+).*/, '$1'),
            };
            // Use `mysql://db` to rely localhost & my.cnf
            if (!o.pathname && o.host) {
                args.db = args.host;
                args.host = 'localhost';
            }
            return ['mysql', args];
        }

        if (['http:', 'https:'].includes(o.protocol)) {
            return ['api', str];
        }

        if (o.protocol === 'file:') {
            // out_file only for write-enabled provider
            return ['fs', { out_file: o.pathname }];
        }

        if (o.protocol === 'wp-cli:') {
            return ['wpcli', { alias: o.host }];
        }

        if (['stdin:', 'stdout:', 'stderr:'].includes(o.protocol)) {
            return ['stdio', { out_file: o.pathname }];
        }
    } catch (e) {
        //console.warn(e, str);
    }

    // Invalid URI cases
    if (str === 'mysql') {
        return ['mysql', null];
    }
    if (str === 'fs') {
        return ['fs', null];
    }
    if (str && str.startsWith('wp-cli')) {
        return ['wpcli', null];
    }
    if (str && str.startsWith('cmd')) {
        return ['cmd', null];
    }
    if (['-', 'stdout', 'stderr', 'stdio'].includes(str)) {
        return ['stdio', null];
    }
    if (str && (str.includes('/') || str.startsWith('./'))) {
        return ['fs', { out_file: str }];
    }
}

/**
 * If --from is omitted, the FsProvider is used and sources are expected to be regular files (or stdin)
 * Otherwise, --from could be mysql:// http:// cmd:// or wp-cli:// URL and source will be post ID
 *
 * If --to is not specified, --from is used.
 * If --to is set parse it, instantiate the corresponding class and attach its (bound) write() method to the
 * reader.
 *
 * This is dirty. A better implementation would rely on Readers() and Writers() classes.
 */
function dataProvider(sources, options) {
    let read = false,
        write = false,
        readProvider = false,
        writeProvider = false,
        readType,
        writeType,
        custom_opts = {};
    const WRITE_REQUESTED = options.write;

    // READ part
    read = getProviderFromUrl('in', options.from) || ['fs', null];
    [readType, custom_opts = {}] = read;
    if (!custom_opts) custom_opts = {};

    if (custom_opts?.out_file) {
        delete (custom_opts.out_file); // out_file is only for read-provider
    }
    readProvider = new DATA_PROVIDERS[readType](sources, false, {
        ...options,
        ...custom_opts,
    });

    // WRITE part
    write = getProviderFromUrl('out', options.to) ||
        (sources.length === 1 ? ['stdio', null] : ['fs', null]);
    [writeType, custom_opts = {}] = write;
    if (!custom_opts) custom_opts = {};

    // incompatibilities / warnings
    if (read[0] === 'wpcli' && !options.to && WRITE_REQUESTED) {
        throw Error(
            'wp-cli needs a --to command-line specifying how to update posts',
        );
    }
    if (!options.to && WRITE_REQUESTED) {
        console.warn(
            'Warning: --to passed without --write. No modification will be made',
        );
    }

    // Pass the reader to the writer to allow further coordination
    // Eg, for `fs`, it's use to infer the default output.
    if (options.to) {
        custom_opts.out_file = options.to;
        custom_opts.reader = readProvider;
    }

    // dual-mode: different in/out mechanisms
    // but could also be a different way to write within one provider, eg: read from files
    // but output output to stdout
    writeProvider = new DATA_PROVIDERS[writeType](sources, WRITE_REQUESTED, {
        ...options,
        ...custom_opts,
    });

    if (writeType === 'stdio') {
        // When writing to the terminal, always assume --write was passed
        options.write = options.forceWrite = true;
    }
    readProvider.write = writeProvider.write.bind(writeProvider);
    console.warn(
        `data provider: ${read[0]} / ${write[0]} (${
            write[0] != 'stdio' ? 'writing=' + (options.write || false) : ''
        })`,
        readProvider,
    );

    return { readProvider, writeProvider };
}

async function preCheck(ro, rw) {
    if (ro.preCheck && !await ro.preCheck()) {
        throw Error('readProvider prechecking conditions failed');
    }

    if (ro != rw && rw.preCheck && !await rw.preCheck()) {
        throw Error('writeProvider prechecking conditions failed');
    }
    debug('all prechecking conditions passed');
}

export { dataProvider, preCheck };
