import { identityPreCheck } from './utils.js';
import { dryRun, epilog } from './console-utils.js';

async function apply(provider, options) {
    // start boiler-plate data-provider iterator-code
    const it = provider.it();
    while (true) {
        const { done, value } = await it.next();
        if (done) {
            break;
        } else if (done === undefined) {
            throw Error('Itarator error. done is undefined');
        }

        const { source, content } = value;
        identityPreCheck(source, content, options);
        // end boiler-plate data-provider iterator-code

        const blocks = wp.blocks.parse(content, {
            __unstableSkipMigrationLogs: true,
        });
        if (!blocks || !blocks.length) {
            throw Error('No blocks found in source');
        }

        let modified = false;
        blocks.forEach((block) => {
            if (block.name === 'core/missing') {
                content = content.concat(wp.blocks.rawHandler(
                    { HTML: wp.blocks.getBlockContent(block) },
                ));
                modified = true;
            }
        });

        if (modified) {
            if (wp.blocks.serialize(blocks) === content) {
                throw Error(`${source}: modified but generated markup is identical`);
            }

            dryRun(source, content, blocks, options);
            await provider.write(source, blocks);
        }
    }
    epilog();
}

export default apply;
