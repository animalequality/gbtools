import local_json from './package.json' with { type: 'json' };

const packagesToCheck = [
    '@wordpress/block-library',
    '@wordpress/editor',
    '@wordpress/components',
];

function package_json_to_version(d) {
    return Object.fromEntries(
        Object.entries(d.dependencies).filter(([key, val]) => packagesToCheck.includes(key)),
    );
}

function computeIdQuery() {
    return `SELECT GROUP_CONCAT(option_value, \",\") as hash
 FROM wp_options
 WHERE option_name IN (\"WPLANG\", \"options_email\", \"timezone_string\", \"siteurl\")
 ORDER BY option_name`;
}

async function computeId(db) {
    return (await db.query(computeIdQuery()))[0].hash;
}

async function getWpVersionFromDatabase(db) {
    /*
      Run `wp @aeall  cron event run acf_update_site_health_data` to force an update.
      `acf_site_health` wp_version fields is obtained from bloginfo() which gets it from
      wp-includes/version.php `$wp_version` variable (on the host where the cron runs)

    const query = `SELECT option_value from wp_options where option_name  = 'db_version'`,
        res = (await db.query(query))[0].option_value,
        remote_wp_version = `${res[0]}.${res[1]}.${res[2]}`;
    */
    const query = `SELECT option_value from wp_options where option_name  = 'acf_site_health'`,
        res = (await db.query(query))[0].option_value,
        remote_wp_version = JSON.parse(res)['wp_version'];
    return remote_wp_version;
}

async function getGutenbergVersionFromWp(wp_version) {
    const url =
            `https://github.com/WordPress/wordpress-develop/raw/refs/tags/${wp_version}/package.json`,
        db_gb_json_versions = await fetch(url).then((res) => res.json()),
        remote_pkg_versions = package_json_to_version(db_gb_json_versions);
    return remote_pkg_versions;
}

function getGutenbergLocalVersion() {
    return package_json_to_version(local_json);
}

async function getPackagesVersions(wp_version) {
    const local_versions = getGutenbergLocalVersion(),
        remote_versions = await getGutenbergVersionFromWp(wp_version);
    return [local_versions, remote_versions, wp_version];
}

function packagesVersionsMatch(local_pkg_versions, remote_pkg_versions) {
    // Compare by suming version numbers: (^9.8.16 + ^28.8.11 + ^14.8.18) = 53445
    const sum = (obj) =>
        Object.values(obj).map((e) => parseInt(e.replaceAll(/[^0-9]/g, ''))).reduce(
            (_sum, x) => _sum + x,
        );
    return sum(local_pkg_versions) == sum(remote_pkg_versions);
}

export {
    computeId,
    computeIdQuery,
    getGutenbergLocalVersion,
    getGutenbergVersionFromWp,
    getPackagesVersions,
    getWpVersionFromDatabase,
    packagesVersionsMatch,
};
