def leaves:
  if ((type == "array" or type == "object") and ((.|has("type")|not) or .type == "section" or .type == "columns" or .type == "column" or .type == "fieldset" or .type == "tab" or .type == "tabs"))
  then .[] | leaves
  else .
  end;

{
  ($b): [
      path(leaves) as $p | {
	"key": ($p|join(".")), "value": (if (getpath($p)|type == "object") then getpath($p) else null end)
      }] |
  map(select(.value!=null)) |
  from_entries |
  with_entries(.key |= sub(".*header."; ""))
}
