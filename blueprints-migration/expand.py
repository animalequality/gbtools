#!/usr/bin/python3

import json, sys

# Loading modular
with open("blueprints-excerpt.json", "r") as f:
    j = json.load(f)

# Loading stats
with open("modular-stats.json", "r") as f:
    stats = json.load(f)

def import_(filename, key, val):
    modular = filename.replace('.yaml', '')
    try:
        ref = val["type"].split('/')[-1] + '.yaml'
    except KeyError:
        print("no type in ", val)
        return None

    if ref not in j.keys():
        print(f'# Cant import {ref} from {filename}')
        return None

    val = j[ref]
    lastpath = key.split('.')[-1]
    newkey = f"{modular}_{lastpath}"

    print(f'# {ref} imported by "{filename}" containing %s' % ', '.join(val.keys()), file=sys.stderr)
    # val = {f"{modular}_{k}": v for k, v in val.items()}
    j[filename] = {**j[filename], **val}
    del(j[filename][key])
    return newkey

def main():
    for filename, fields in j.items():
    
        modular = filename.replace('.yaml', '')
        for proppath, val in fields.items():

            if proppath in ['extends@', '@extends']:
                continue
    
            if proppath.endswith(".import@"):
                # print("a", j[filename][proppath])
                x = import_(filename, proppath, val)
                return main()

            field_name = proppath.split('.')[-1]
            usage_path = f"{modular}_{proppath}"

            # print(f"%d usage of {usage_path}" % int(stats[usage_path])) # , json.dumps(val))
            if usage_path in stats:
                val['_usage'] = int(stats[usage_path])
            else:
                print(f"NOT FOUND: {usage_path}   as    {proppath}", file=sys.stderr)

            for i in ["validate", "description", "style", "help", "size", "highlight", "options", "default",
                      "rows", "toggleable", "max", "min", "codemirror", "classes", "content", "collapsed",
                      "fields", "multiple", "required", "selectize", "id", "flavors", "destination",
                      "ordering@", "random_name", "accept", "avoid_overwriting", "format", "config-default@",
                      "data-options@", "field-default@", "field-data-options@@", "preview_images", "folder",
                      "config-placeholder@", "placeholder"]:
                try:
                    del(val[i])
                except:
                    pass
    

main()
print(json.dumps(j))
