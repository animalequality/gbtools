#!/bin/bash


for i in $(find $@ -name '*.yaml' -type f); do
    yq --arg b $(basename $i) -rf $(dirname $0)/filter.jq "$i";
done | jq -s add


# Generate statistics
# for f in $(fd ".md" user/??/pages); do sed -rn "/---/,/---/{/^ /d;s/([^:]+):.*/$(basename "$f"|sed 's/\..._..\.md//') \1/p}" "$f"; done|sort|uniq -c

# awk '{x[$2 " " $3]+=$1;}END{for (d in x) print(x[d], d)}'<  modular-stats.txt | jq -R 'split(" ")|{(.[1] + "_" + .[2]): .[0]}' |jq -s add >| modular-stats.json
