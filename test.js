import program from './cli.js';
import { dataProvider } from './providers.js';

program.command('test1')
    .addArgument('<sources...>', 'sources')
    .action(function (sources, options) {
        options = this.optsWithGlobals();
        const x = dataProvider(sources, options),
            d = x._details();

        if (d.host !== 'localhost' || d.user != 'ae'
            || typeof (x.write) !== 'function') {
            throw Error('test failed');
        }

    });

program.parse(['node', 'test.js', 'test1', '--from', 'mysql://aeus', '--to', "wp-cli://wp @prod-ae post get --field=post_content %d", '10257']);
