import process from 'process';

const [major, _, __] = process.versions.node.split('.').map(Number);
if (major <= 18) {
    global.File = class {};
}

import { JSDOM } from 'jsdom';
global.JSDOM = JSDOM;
global.dom = new JSDOM('<!doctype html><html><body></body></html>');
global.window = dom.window;
global.document = dom.window.document;

try {
    // For node 18.x
    global.navigator = global.window.navigator;
} catch (TypeError) {}

global.window.matchMedia = () => {
    return {
        matches: false,
        addEventListener: () => {},
        removeEventListener: () => {},
        dispatchEvent: () => {},
    };
};
